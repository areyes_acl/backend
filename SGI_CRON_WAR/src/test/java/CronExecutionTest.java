import junit.framework.TestCase;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.step.StepCorreoVencimiento;
import com.sbpay.sgi.cron.step.StepDownloadCPagoTbk;
import com.sbpay.sgi.cron.step.StepDownloadComisionesTbk;
import com.sbpay.sgi.cron.step.StepDownloadLogAutorizacion;
import com.sbpay.sgi.cron.step.StepDownloadOnus;
import com.sbpay.sgi.cron.step.StepDownloadOutgoingTbk;
import com.sbpay.sgi.cron.step.StepDownloadRechazos;
import com.sbpay.sgi.cron.step.StepGenerarCargoAbono;
import com.sbpay.sgi.cron.step.StepGenerarIncomingOnUs;
import com.sbpay.sgi.cron.step.StepGenerateIncoming;
import com.sbpay.sgi.cron.step.StepProcessCPagos;
import com.sbpay.sgi.cron.step.StepProcessComisiones;
import com.sbpay.sgi.cron.step.StepProcessOnus;
import com.sbpay.sgi.cron.step.StepProcessOutgoing;
import com.sbpay.sgi.cron.step.StepRechazosTbk;
import com.sbpay.sgi.cron.step.StepUploadCargoAbono;
import com.sbpay.sgi.cron.step.StepUploadICOutgoingTbk;
import com.sbpay.sgi.cron.step.StepUploadIncoming;
import com.sbpay.sgi.cron.step.StepUploadOnusTbk;

public class CronExecutionTest extends TestCase {

	/**
	 * STEP A EJECUTAR JOB1
	 */
	private StepDownloadOutgoingTbk stepDownloadOutgoingTbk;
	private StepUploadICOutgoingTbk stepUploadOutgoingIC;
	private StepDownloadComisionesTbk stepDownloadComisiones;
	private StepDownloadCPagoTbk stepDownloadCPago;

	/**
	 * STEP A EJECUTAR JOB2
	 */
	private StepDownloadRechazos stepDownloadRechazos;
	private StepDownloadOnus stepDownloadOnus;
	private StepDownloadLogAutorizacion stepDownloadLogAutorizacion;
	private StepProcessOutgoing stepProcessOutgoingTbk;
	private StepRechazosTbk stepProcessRechazosTbk;
	private StepProcessOnus stepProcessOnus;
	private StepProcessComisiones stepProcessComisiones;
	private StepProcessCPagos stepProcessCPagos;

	/**
	 * STEP A EJECUTAR JOB3
	 */
	private StepGenerateIncoming stepGenerateIncoming;
	private StepGenerarIncomingOnUs stepGenerarIncomingOnUs;
	private StepGenerarCargoAbono stepGenerarCargoAbono;
	private StepUploadIncoming stepUploadIncoming;
	private StepUploadOnusTbk stepUploadOnusTbk;
	private StepUploadCargoAbono stepUploadCargoAbono;
	private StepCorreoVencimiento stepCorreoVencimiento;

	/**
	 * INICIALIZACION DE STEPS
	 */
	protected void setUp() throws Exception {
		super.setUp();

		/**
		 * INIT STEP JOB1
		 */
		stepDownloadOutgoingTbk = new StepDownloadOutgoingTbk();
		stepUploadOutgoingIC = new StepUploadICOutgoingTbk();
		stepDownloadComisiones = new StepDownloadComisionesTbk();
		stepDownloadCPago = new StepDownloadCPagoTbk();

		/**
		 * INIT STEP JOB2
		 */
		stepDownloadRechazos = new StepDownloadRechazos();
		stepDownloadOnus = new StepDownloadOnus();
		stepDownloadLogAutorizacion = new StepDownloadLogAutorizacion();
		stepProcessOutgoingTbk = new StepProcessOutgoing();
		stepProcessRechazosTbk = new StepRechazosTbk();
		stepProcessOnus = new StepProcessOnus();
		stepProcessComisiones = new StepProcessComisiones();
		stepProcessCPagos = new StepProcessCPagos();

		/**
		 * INIT STEP JOB 3
		 */
		stepGenerateIncoming = new StepGenerateIncoming();
		stepGenerarIncomingOnUs = new StepGenerarIncomingOnUs();
		stepGenerarCargoAbono = new StepGenerarCargoAbono();
		stepUploadIncoming = new StepUploadIncoming();
		stepUploadOnusTbk = new StepUploadOnusTbk();
		stepUploadCargoAbono = new StepUploadCargoAbono();
		stepCorreoVencimiento = new StepCorreoVencimiento();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/**
	 * 
	 */
	public void testFirstJob() {

		try {
			System.out.println("===== PRIMER JOB EJECUTANDOSE  =========");
			stepDownloadOutgoingTbk.execute();
			Thread.sleep(10000);
			stepUploadOutgoingIC.execute();
			Thread.sleep(10000);
			stepDownloadComisiones.execute();
			Thread.sleep(10000);
			stepDownloadCPago.execute();

		} catch (AppException e) {
			System.out.println(e);
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	public void _testSecondJob() {
		try {
			System.out.println("===== SEGUNDO JOB EJECUTANDOSE  =========");
			stepDownloadRechazos.execute();
			Thread.sleep(10000);
			stepDownloadOnus.execute();
			Thread.sleep(10000);
			stepDownloadLogAutorizacion.execute();
			Thread.sleep(10000);
			stepProcessOutgoingTbk.execute();
			Thread.sleep(10000);
			stepProcessRechazosTbk.execute();
			Thread.sleep(10000);
			stepProcessOnus.execute();
			Thread.sleep(10000);
			stepProcessComisiones.execute();
			Thread.sleep(10000);
			stepProcessCPagos.execute();
			Thread.sleep(10000);

		} catch (AppException e) {
			System.out.println(e);
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * 
	 */
	public void thirdJob() {
		try {
			System.out.println("===== TERCER JOB EJECUTANDOSE  =========");
			stepGenerateIncoming.execute();
			Thread.sleep(10000);
			stepGenerarIncomingOnUs.execute();
			Thread.sleep(10000);
			stepGenerarCargoAbono.execute();
			Thread.sleep(10000);
			stepUploadIncoming.execute();
			Thread.sleep(10000);
			stepUploadOnusTbk.execute();
			Thread.sleep(10000);
			stepUploadCargoAbono.execute();
			Thread.sleep(10000);
			stepCorreoVencimiento.execute();
			Thread.sleep(10000);
		} catch (AppException e) {
			System.out.println(e);
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
