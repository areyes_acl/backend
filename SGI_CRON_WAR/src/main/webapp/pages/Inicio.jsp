<%@page import="java.sql.ResultSet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true"%>
<!DOCTYPE html>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Web Jobs</title>

<%@include file="templates/header.html" %>
<script type="text/javascript">

function callServlet(url) 
{ 

   $.ajax({
   url: url,
   type: 'GET'
   }).done(function(data){
   	alert(data);
   });
} 



function getParam(url) 
{ 

   $.ajax({
   url: url,
   type: 'GET',
   success: function(data){
    
 	var tbody = document.querySelector('#myTBody')
 	tbody.innerHTML = '';
 	
 	for(var i in data)
 	{
 		var codDato = data[i].codDato;
 		var sid = data[i].sid;
 		tbody.innerHTML +="<tr><td>"+data[i].codDato+
 		"</td><td><input class='form-control' type='text' id='valor2' name='valor2' value='"+data[i].valor+"' onblur='actualizar(\""+sid+"\", this.value)'/>"+
 		"</td><td>"+data[i].descripcion+
 		"</td><td>"+data[i].codGrupo+
 		"</td></tr>";
 		
 	}
 	 
 	 
   },
   error: function(){
   alert("error en la respuesta");
   }
   
   })
   
} 


function actualizar(sid, valor){
	//alert(sid + ' - ' +valor)
	
	$.ajax({
	
		url: 'updateValor.htm',
		data: {'sid' : sid, 'valor' : valor},
		type: 'POST',
		success: function(){
		},
		error: function(){
		}
	
	});
	
}


</script>


</head>
<body>



<div id = "somediv"></div>

<!-- Modal -->
<div class="modal fade " id="myModal" role="dialog" >
  <div class="modal-dialog modal-dialog-centered modal-xl modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
      
        <h5 class="modal-title" id="exampleModalLabel">Lista de Parámetros</h5>
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
      
      <div class="container" >      
        <table class="table table-bordered" >
	
  <thead class = "thead-dark" style="text-align: center; ">
    <tr>
      <th scope="col">Código del dato</th>
      <th scope="col">Valor</th>
      <th scope="col">Descripción</th>
      <th scope="col">Código de grupo</th>
    </tr>
  </thead>
  <tbody style="overflow-y:scroll; " id = "myTBody">
  

  </tbody>
  </table>
  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
	<center>

<div>




<div class = "container">

<div style = "float:right;" class = "list">

<a href = "logout.htm"><h5>Cerrar Sesión</h5></a>
</div>

	<table class="table table-bordered" >
	
  <thead class = "thead-dark" style="text-align: center;">
    <tr>
      <th scope="col">Jobs</th>
      <th scope="col">Tareas</th>
      <th scope="col">Cambiar Estado del Parámetro</th>
      <th scope="col">Ver Datos del Parámetro</th>
    </tr>
  </thead>
  
  <tbody>
  
  <tr>
		<td  class = "table-active" rowspan="8" style = "vertical-align: middle; text-align: center;">
		
		
		<!-- JOB 1 -->
		
			<a class = "btn btn-primary" type = "button" href="javascript: callServlet('Jobs.htm?nameJob=1');">Ejecutar Job1 </a>
		</td>
		<td class = "list table-active">
			<a href = "javascript: callServlet('Task.htm?task=1')">Descargar Outgoing Transbank</a>
		</td>
		<td  class = "list table-active">
			  
			 <% String var = session.getAttribute("DESCARGAR_OUTGOING").toString();
			  	if(var.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "D_Out">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="D_OUT" onclick="callServlet('updateParametro.htm?paramName=DOWNLOAD_OUTGOING&nuevoEstado=1');">
					  <label class="custom-control-label" for="D_OUT">DESCARGAR_OUTGOING</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "D_Out2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="D_OUT2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=DOWNLOAD_OUTGOING&nuevoEstado=0');">
					  <label class="custom-control-label" for="D_OUT2">DESCARGAR_OUTGOING</label>
					</div>
					</form>
			   <%} %>
		
		</td>
		<td class = "table-active" style = "vertical-align: middle; text-align: center;">
		<button type="button" class="btn btn-primary" id = "btn" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=1')">
		  Mostrar datos
		</button>
		</td>
      </tr>
      
	  <tr class = "list">
		  <td class = "table-active">
			<a href = "javascript: callServlet('Task.htm?task=2')">Descargar Comisiones Transbank</a>
		  </td>
		  
		  <td class = "table-active">
			<% String var1 = session.getAttribute("DESCARGAR_COMISIONES_TRANSBANK").toString();
			  	if(var1.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "D_C_TB">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="D_C_TB" onclick="callServlet('updateParametro.htm?paramName=DOWNLOAD_COMISSIONS&nuevoEstado=1');">
					  <label class="custom-control-label" for="D_C_TB">DESCARGAR_COMISIONES_TRANSBANK</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "D_C_TB2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="D_C_TB2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=DOWNLOAD_COMISSIONS&nuevoEstado=0');">
					  <label class="custom-control-label" for="D_C_TB2">DESCARGAR_COMISIONES_TRANSBANK</label>
					</div>
					</form>
			   <%} %>
		  </td>
		  <td class = "table-active" style = "vertical-align: middle; text-align: center;">
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=2')">
			  Mostrar datos
			</button>
			</td>
	  </tr>
	  
	  <tr class = "list">
		  <td class = "table-active">
			<a href = "javascript: callServlet('Task.htm?task=3')">Descargar Compensación Pagos</a>
		  </td>
		  
		  <td class = "table-active">
			<% String var2 = session.getAttribute("DESCARGAR_COMPENSACIONES_PAGO").toString();
			  	if(var2.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "D_C_P">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="D_C_P" onclick="callServlet('updateParametro.htm?paramName=DOWNLOAD_COMPENSATION_PAYMENT_FILE&nuevoEstado=1');">
					  <label class="custom-control-label" for="D_C_P">DESCARGAR_COMPENSACIONES_PAGO</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "D_C_P2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="D_C_P2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=DOWNLOAD_COMPENSATION_PAYMENT_FILE&nuevoEstado=0');">
					  <label class="custom-control-label" for="D_C_P2">DESCARGAR_COMPENSACIONES_PAGO</label>
					</div>
					</form>
			   <%} %>
		  </td>
		  <td class = "table-active" style = "vertical-align: middle; text-align: center;">
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=3')">
			  Mostrar datos
			</button>
		 </td>
	  </tr>
	  
	  <tr class = "list">
		  <td class = "table-active">
			<a href = "javascript: callServlet('Task.htm?task=4')">Subir Outgoing Nac IC</a>
		  </td>
		  
		  <td class = "table-active">
			<% String var3 = session.getAttribute("SUBIR_OUTGOING_NAC_IC").toString();
			  	if(var3.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "S_O_N_I">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="S_O_N_I" onclick="callServlet('updateParametro.htm?paramName=UPLOAD_OUTGOING_TO_IC&nuevoEstado=1');">
					  <label class="custom-control-label" for="S_O_N_I">SUBIR_OUTGOING_NAC_IC</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "S_O_N_I2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="S_O_N_I2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=UPLOAD_OUTGOING_TO_IC&nuevoEstado=0');">
					  <label class="custom-control-label" for="S_O_N_I2">SUBIR_OUTGOING_NAC_IC</label>
					</div>
					</form>
			   <%} %>
		  </td>
		  <td class = "table-active" style = "vertical-align: middle; text-align: center;">
		  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=4')">
			  Mostrar datos
			</button>
		  </td>
	  </tr>
	  
	  <tr class = "list">
		  <td class = "table-active">
			<a href = "javascript: callServlet('Task.htm?task=5')">Descargar Outgoing Visa Nac</a>
		  </td>
		  
		  <td class = "table-active">
			<% String var4 = session.getAttribute("DESCARGAR_OUTGOING_VISA_NAC").toString();
			  	if(var4.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "D_O_V_N">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="D_O_V_N" onclick="callServlet('updateParametro.htm?paramName=DOWNLOAD_OUTGOING_VISA_NAC&nuevoEstado=1');">
					  <label class="custom-control-label" for="D_O_V_N">DESCARGAR_OUTGOING_VISA_NAC</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "D_O_V_N2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="D_O_V_N2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=DOWNLOAD_OUTGOING_VISA_NAC&nuevoEstado=0');">
					  <label class="custom-control-label" for="D_O_V_N2">DESCARGAR_OUTGOING_VISA_NAC</label>
					</div>
					</form>
			   <%} %>
		  </td>
		  <td class = "table-active" style = "vertical-align: middle; text-align: center;">
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=5')">
			  Mostrar datos
			</button>
		  </td>
	  </tr>
  
  	<tr class = "list">
		  <td class = "table-active">
			<a href = "javascript: callServlet('Task.htm?task=6')">Subir Outgoing Visa Nac Ic</a>
		  </td>
		  
		  <td class = "table-active">
			<% String var5 = session.getAttribute("SUBIR_OUTGOING_VISA_NAC_IC").toString();
			  	if(var5.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "S_O_V_N_I">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="S_O_V_N_I" onclick="callServlet('updateParametro.htm?paramName=UPLOAD_OUTGOING_VISA_NAC_TO_IC&nuevoEstado=1');">
					  <label class="custom-control-label" for="S_O_V_N_I">SUBIR_OUTGOING_VISA_NAC_IC</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "S_O_V_N_I2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="S_O_V_N_I2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=UPLOAD_OUTGOING_VISA_NAC_TO_IC&nuevoEstado=0');">
					  <label class="custom-control-label" for="S_O_V_N_I2">SUBIR_OUTGOING_VISA_NAC_IC</label>
					</div>
					</form>
			   <%} %>
		  </td>
		  <td class = "table-active" style = "vertical-align: middle; text-align: center;">
		  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=6')">
			  Mostrar datos
			</button>
		  </td>
	  </tr>
	  
	  
	  <tr class = "list">
		  <td class = "table-active">
			<a href = "javascript: callServlet('Task.htm?task=7')">Descargar Outgoing Visa Internacional</a>
		  </td>
		  
		  <td class = "table-active">
			<% String var6 = session.getAttribute("DESCARGAR_OUTGOING_VISA").toString();
			  	if(var6.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "D_O_V">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="D_O_V" onclick="callServlet('updateParametro.htm?paramName=DOWNLOAD_OUTGOING_VISA&nuevoEstado=1');">
					  <label class="custom-control-label" for="D_O_V">DESCARGAR_OUTGOING_VISA_INT</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "D_O_V2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="D_O_V2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=DOWNLOAD_OUTGOING_VISA&nuevoEstado=0');">
					  <label class="custom-control-label" for="D_O_V2">DESCARGAR_OUTGOING_VISA_INT</label>
					</div>
					</form>
			   <%} %>
		  </td>
		  <td class = "table-active" style = "vertical-align: middle; text-align: center;">
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=7')">
			  Mostrar datos
			</button>
			</td>
	  </tr>
	  
	  <tr class = "list">
		  <td class = "table-active">
			<a href = "javascript: callServlet('Task.htm?task=8')">Subir Outgoing Visa A IC</a>
		  </td>
		  
		  <td class = "table-active">
			<% String var7 = session.getAttribute("SUBIR_OUTGOING_VISA_A_IC").toString();
			  	if(var7.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "S_O_V_AIC">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="S_O_V_AIC" onclick="callServlet('updateParametro.htm?paramName=UPLOAD_OUTGOING_VISA_TO_IC&nuevoEstado=1');">
					  <label class="custom-control-label" for="S_O_V_AIC">SUBIR_OUTGOING_VISA_A_IC</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "S_O_V_AIC2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="S_O_V_AIC2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=UPLOAD_OUTGOING_VISA_TO_IC&nuevoEstado=0');">
					  <label class="custom-control-label" for="S_O_V_AIC2">SUBIR_OUTGOING_VISA_A_IC</label>
					</div>
					</form>
			   <%} %>
		  </td>
		  <td class = "table-active" style = "vertical-align: middle; text-align: center;">
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=8')">
			  Mostrar datos
			</button>
			</td>
	  </tr>
	  
	  
	 <!-- JOB 2 -->
	  
    <tr>
      <td rowspan="16" style = "vertical-align: middle; text-align: center;">
      	<a class = "btn btn-primary" role  = "button" href="javascript: callServlet('Jobs.htm?nameJob=2');">Ejecutar Job2</a>
      </td>
     	<td  class = "list">
			<a href = "javascript: callServlet('Task.htm?task=9')">Descargar Rechazos</a>
		</td>
		<td  class = "list">
			<% String var8 = session.getAttribute("DESCARGAR_RECHAZOS").toString();
			  	if(var8.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "D_R">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="D_R" onclick="callServlet('updateParametro.htm?paramName=DOWNLOAD_REJECTIONS&nuevoEstado=1');">
					  <label class="custom-control-label" for="D_R">DESCARGAR_RECHAZOS</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "D_R2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="D_R2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=DOWNLOAD_REJECTIONS&nuevoEstado=0');">
					  <label class="custom-control-label" for="D_R2">DESCARGAR_RECHAZOS</label>
					</div>
					</form>
			   <%} %>
		</td>
		<td  class = "list" style="vertical-align: middle; text-align: center;">
		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=9')">
			  Mostrar datos
			</button>
		</td>
		
      </tr>
      
	  <tr  class = "list">
		  <td>
			<a href = "javascript: callServlet('Task.htm?task=10')">Descargar Log Autorización</a>
		  </td>
		  
		  <td>
			<% String var9 = session.getAttribute("DESCARGAR_LOG_AUTORIZACION").toString();
			  	if(var9.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "D_L_A">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="D_L_A" onclick="callServlet('updateParametro.htm?paramName=DOWNLOAD_LOG_AUTHORIZED_TRANSACTIONS&nuevoEstado=1');">
					  <label class="custom-control-label" for="D_L_A">DESCARGAR_LOG_AUTORIZACION</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "D_L_A2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="D_L_A2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=DOWNLOAD_LOG_AUTHORIZED_TRANSACTIONS&nuevoEstado=0');">
					  <label class="custom-control-label" for="D_L_A2">DESCARGAR_LOG_AUTORIZACION</label>
					</div>
					</form>
			   <%} %>
		  </td>
		  <td  class = "list" style="vertical-align: middle; text-align: center;">
		  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=10')">
			  Mostrar datos
			</button>
		  </td>
	  </tr>
	  
	  <tr  class = "list">
		  <td>
			<a href = "javascript: callServlet('Task.htm?task=11')">Procesar Log Autorización</a>
		  </td>
		  
		  <td>
			<% String var10 = session.getAttribute("PROCESAR_LOG_AUTORIZACION").toString();
			  	if(var10.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "P_L_A">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="P_L_A" onclick="callServlet('updateParametro.htm?paramName=PROCESS_LOG_AUTHORIZED_TRANSACTIONS&nuevoEstado=1');">
					  <label class="custom-control-label" for="P_L_A">PROCESAR_LOG_AUTORIZACION</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "P_L_A2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="P_L_A2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=PROCESS_LOG_AUTHORIZED_TRANSACTIONS&nuevoEstado=0');">
					  <label class="custom-control-label" for="P_L_A2">PROCESAR_LOG_AUTORIZACION</label>
					</div>
					</form>
			   <%} %>
		  </td>
		  <td  class = "list" style="vertical-align: middle; text-align: center;">
		  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=11')">
			  Mostrar datos
			</button>
		  </td>
	  </tr>
	  
	  <tr  class = "list">
		  <td>
			<a href = "javascript: callServlet('Task.htm?task=12')">Procesar Outgoing Transbank</a>
		  </td>
		  
		  <td>
			<% String var11 = session.getAttribute("PROCESAR_OUTGOING_TRANSBANK").toString();
			  	if(var11.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "P_O_TB">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="P_O_TB" onclick="callServlet('updateParametro.htm?paramName=PROCESS_OUTGOING&nuevoEstado=1');">
					  <label class="custom-control-label" for="P_O_TB">PROCESAR_OUTGOING_TRANSBANK</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "P_O_TB2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="P_O_TB2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=PROCESS_OUTGOING&nuevoEstado=0');">
					  <label class="custom-control-label" for="P_O_TB2">PROCESAR_OUTGOING_TRANSBANK</label>
					</div>
					</form>
			   <%} %>
		  </td>
		  
		  <td  class = "list" style="vertical-align: middle; text-align: center;">
		  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=12')">
			  Mostrar datos
			</button>
		  </td>
	  </tr>
	  
	  <tr  class = "list">
		  <td>
			<a href = "javascript: callServlet('Task.htm?task=13')">Procesar Outgoing Visa Nac</a>
		  </td>
		  
		  <td>
			<% String var12 = session.getAttribute("PROCESAR_OUTGOING_VISA_NAC").toString();
			  	if(var12.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "P_O_V_N">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="P_O_V_N" onclick="callServlet('updateParametro.htm?paramName=PROCESS_OUTGOING_VISA_NAC&nuevoEstado=1');">
					  <label class="custom-control-label" for="P_O_V_N">PROCESAR_OUTGOING_VISA_NAC</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "P_O_V_N2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="P_O_V_N2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=PROCESS_OUTGOING_VISA_NAC&nuevoEstado=0');">
					  <label class="custom-control-label" for="P_O_V_N2">PROCESAR_OUTGOING_VISA_NAC</label>
					</div>
					</form>
			   <%} %>
		  </td>
		  <td  class = "list" style="vertical-align: middle; text-align: center;">
		  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=13')">
			  Mostrar datos
			</button>
		  </td>
	  </tr>
	  
	  <tr  class = "list">
		  <td>
			<a href = "javascript: callServlet('Task.htm?task=14')">Procesar Outgoing Visa Int</a>
		  </td>
		  
		  <td>
			<% String var13 = session.getAttribute("PROCESAR_OUTGOING_VISA_INT").toString();
			  	if(var13.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "P_O_V_I">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="P_O_V_I" onclick="callServlet('updateParametro.htm?paramName=PROCESS_OUTGOING_VISA&nuevoEstado=1');">
					  <label class="custom-control-label" for="P_O_V_I">PROCESAR_OUTGOING_VISA_INT</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "P_O_V_I2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="P_O_V_I2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=PROCESS_OUTGOING_VISA&nuevoEstado=0');">
					  <label class="custom-control-label" for="P_O_V_I2">PROCESAR_OUTGOING_VISA_INT</label>
					</div>
					</form>
			   <%} %>
		  </td>
		  
		  <td  class = "list" style="vertical-align: middle; text-align: center;">
		  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=14')">
			  Mostrar datos
			</button>
		  </td>
	  </tr>
	  
	  <tr  class = "list">
		  <td>
			<a href = "javascript: callServlet('Task.htm?task=15')">Generar Itz Avances</a>
		  </td>
		  
		  <td>
			<% String var14 = session.getAttribute("GENERAR_ITZ_AVANCES").toString();
			  	if(var14.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "G_I_A">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="G_I_A" onclick="callServlet('updateParametro.htm?paramName=GENERATE_FILE_AVANCE&nuevoEstado=1');">
					  <label class="custom-control-label" for="G_I_A">GENERAR_ITZ_AVANCES</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "G_I_A2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="G_I_A2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=GENERATE_FILE_AVANCE&nuevoEstado=0');">
					  <label class="custom-control-label" for="G_I_A2">GENERAR_ITZ_AVANCES</label>
					</div>
					</form>
			   <%} %>
		  </td>
		  
		  <td  class = "list" style="vertical-align: middle; text-align: center;">
		  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=15')">
			  Mostrar datos
			</button>
		  </td>
	  </tr>
	  
	  <tr  class = "list">
		  <td>
			<a href = "javascript: callServlet('Task.htm?task=16')">Descargar Bice</a>
		  </td>
		  
		  <td>
			<% String var15 = session.getAttribute("DESCARGAR_BICE").toString();
			  	if(var15.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "D_B">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="D_B" onclick="callServlet('updateParametro.htm?paramName=DOWNLOAD_AVANCES_BICE&nuevoEstado=1');">
					  <label class="custom-control-label" for="D_B">DESCARGAR_BICE</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "D_B2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="D_B2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=DOWNLOAD_AVANCES_BICE&nuevoEstado=0');">
					  <label class="custom-control-label" for="D_B2">DESCARGAR_BICE</label>
					</div>
					</form>
			   <%} %>
		  </td>
		  
		  <td  class = "list" style="vertical-align: middle; text-align: center;">
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=16')">
			  Mostrar datos
			</button>
		  </td>
	  </tr>
	  
	  <tr  class = "list">
		  <td>
			<a href = "javascript: callServlet('Task.htm?task=17')">Descargar Avances</a>
		  </td>
		  
		  <td>
			<% String var16 = session.getAttribute("DESCARGAR_AVANCES").toString();
			  	if(var16.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "D_A">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="D_A" onclick="callServlet('updateParametro.htm?paramName=DOWNLOAD_AVANCES&nuevoEstado=1');">
					  <label class="custom-control-label" for="D_A">DESCARGAR_AVANCES</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "D_A2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="D_A2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=DOWNLOAD_AVANCES&nuevoEstado=0');">
					  <label class="custom-control-label" for="D_A2">DESCARGAR_AVANCES</label>
					</div>
					</form>
			   <%} %>
		  </td>
		  
		  <td  class = "list" style="vertical-align: middle; text-align: center;">
		  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=17')">
			  Mostrar datos
			</button>
		  </td>
	  </tr>
	  
	  <tr  class = "list">
		  <td>
			<a href = "javascript: callServlet('Task.htm?task=18')">Descargar OnUs</a>
		  </td>
		  
		  <td>
			<% String var17 = session.getAttribute("DESCARGAR_ONUS").toString();
			  	if(var17.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "D_O">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="D_O" onclick="callServlet('updateParametro.htm?paramName=DOWNLOAD_ONUS_FILE&nuevoEstado=1');">
					  <label class="custom-control-label" for="D_O">DESCARGAR_ONUS</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "D_O2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="D_O2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=DOWNLOAD_ONUS_FILE&nuevoEstado=0');">
					  <label class="custom-control-label" for="D_O2">DESCARGAR_ONUS</label>
					</div>
					</form>
			   <%} %>
		  </td>
		  
		  <td  class = "list" style="vertical-align: middle; text-align: center;">
		  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=18')">
			  Mostrar datos
			</button>
		  </td>
	  </tr>
	  
	  <tr  class = "list">
		  <td>
			<a href = "javascript: callServlet('Task.htm?task=19')">Procesar Bice</a>
		  </td>
		  
		  <td>
			<% String var18 = session.getAttribute("PROCESAR_BICE").toString();
			  	if(var18.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "P_B">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="P_B" onclick="callServlet('updateParametro.htm?paramName=PROCESS_BICE&nuevoEstado=1');">
					  <label class="custom-control-label" for="P_B">PROCESAR_BICE</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "P_B2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="P_B2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=PROCESS_BICE&nuevoEstado=0');">
					  <label class="custom-control-label" for="P_B2">PROCESAR_BICE</label>
					</div>
					</form>
			   <%} %>
		  </td>
		  <td  class = "list" style="vertical-align: middle; text-align: center;">
		  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=19')">
			  Mostrar datos
			</button>
		  </td>
	  </tr>
	  
	  <tr  class = "list">
		  <td>
			<a href = "javascript: callServlet('Task.htm?task=20')">Procesar Avances</a>
		  </td>
		  
		  <td>
			<% String var19 = session.getAttribute("PROCESAR_AVANCES").toString();
			  	if(var19.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "P_A">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="P_A" onclick="callServlet('updateParametro.htm?paramName=PROCESS_AVANCES&nuevoEstado=1');">
					  <label class="custom-control-label" for="P_A">PROCESAR_AVANCES</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "P_A2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="P_A2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=PROCESS_AVANCES&nuevoEstado=0');">
					  <label class="custom-control-label" for="P_A2">PROCESAR_AVANCES</label>
					</div>
					</form>
			   <%} %>
		  </td>
		  <td  class = "list" style="vertical-align: middle; text-align: center;">
		  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=20')">
			  Mostrar datos
			</button>
		  </td>
	  </tr>
	  
	  <tr  class = "list">
		  <td>
			<a href = "javascript: callServlet('Task.htm?task=21')">Procesar OnUs</a>
		  </td>
		  
		  <td>
			<% String var20 = session.getAttribute("PROCESAR_ONUS").toString();
			  	if(var20.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "P_O">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="P_O" onclick="callServlet('updateParametro.htm?paramName=PROCESS_ONUS&nuevoEstado=1');">
					  <label class="custom-control-label" for="P_O">PROCESAR_ONUS</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "P_O2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="P_O2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=PROCESS_ONUS&nuevoEstado=0');">
					  <label class="custom-control-label" for="P_O2">PROCESAR_ONUS</label>
					</div>
					</form>
			   <%} %>
		  </td>
		  
		  <td  class = "list" style="vertical-align: middle; text-align: center;">
		  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=21')">
			  Mostrar datos
			</button>
		  </td>
	  </tr>
	  
	  <tr  class = "list">
		  <td>
			<a href = "javascript: callServlet('Task.htm?task=22')">Procesar Rechazos Transbank</a>
		  </td>
		  
		  <td>
			<% String var21 = session.getAttribute("PROCESAR_RECHAZOS_TRANSBANK").toString();
			  	if(var21.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "D_R_TB">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="D_R_TB" onclick="callServlet('updateParametro.htm?paramName=PROCESS_REJECTED&nuevoEstado=1');">
					  <label class="custom-control-label" for="D_R_TB">PROCESAR_RECHAZOS_TRANSBANK</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "D_R_TB2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="D_R_TB2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=PROCESS_REJECTED&nuevoEstado=0');">
					  <label class="custom-control-label" for="D_R_TB2">PROCESAR_RECHAZOS_TRANSBANK</label>
					</div>
					</form>
			   <%} %>
		  </td>
		  
		  <td  class = "list" style="vertical-align: middle; text-align: center;">
		  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=22')">
			  Mostrar datos
			</button>
		  </td>
	  </tr>
	  
	  <tr  class = "list">
		  <td>
			<a href = "javascript: callServlet('Task.htm?task=23')">Procesar Comisiones</a>
		  </td>
		  
		  <td>
			<% String var22 = session.getAttribute("PROCESAR_COMISIONES").toString();
			  	if(var22.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "P_C">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="P_C" onclick="callServlet('updateParametro.htm?paramName=PROCESS_COMISSIONS&nuevoEstado=1');">
					  <label class="custom-control-label" for="P_C">PROCESAR_COMISIONES</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "P_C2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="P_C2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=PROCESS_COMISSIONS&nuevoEstado=0');">
					  <label class="custom-control-label" for="P_C2">PROCESAR_COMISIONES</label>
					</div>
					</form>
			   <%} %>
		  </td>
		  <td  class = "list" style="vertical-align: middle; text-align: center;">
		  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=23')">
			  Mostrar datos
			</button>
		  </td>
	  </tr>
	  
	  <tr class = "list">
		  <td>
			<a href = "javascript: callServlet('Task.htm?task=24')">Procesar CPagos</a>
		  </td>
		  
		  <td>
			<% String var23 = session.getAttribute("PROCESAR_CPAGOS").toString();
			  	if(var23.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "P_CPag">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="P_CPag" onclick="callServlet('updateParametro.htm?paramName=PROCESS_COMPENSATION_PAYMENT&nuevoEstado=1');">
					  <label class="custom-control-label" for="P_CPag">PROCESAR_CPAGOS</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "P_CPag2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="P_CPag2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=PROCESS_COMPENSATION_PAYMENT&nuevoEstado=0');">
					  <label class="custom-control-label" for="P_CPag2">PROCESAR_CPAGOS</label>
					</div>
					</form>
			   <%} %>
		  </td>
		  <td  class = "list" style="vertical-align: middle; text-align: center;">
		  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=24')">
			  Mostrar datos
			</button>
		  </td>
	  </tr>
    
    
    <!-- JOB 3 -->
    
    <tr>
		<td class = "table-active" rowspan="9" style = "vertical-align: middle; text-align: center;">
			<a class = "btn btn-primary" role  = "button" href = "javascript: callServlet('Jobs.htm?nameJob=3')">Ejecutar Job3</a>
		</td>
		<td class = "table-active list">
			<a href = "javascript: callServlet('Task.htm?task=25')">Generar Incoming</a>
		</td>
		<td class = "table-active list">
			<% String var24 = session.getAttribute("GENERAR_INCOMING").toString();
			  	if(var24.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "G_INC">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="G_INC" onclick="callServlet('updateParametro.htm?paramName=GENERATE_INCOMING&nuevoEstado=1');">
					  <label class="custom-control-label" for="G_INC">GENERAR_INCOMING</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "G_INC2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="G_INC2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=GENERATE_INCOMING&nuevoEstado=0');">
					  <label class="custom-control-label" for="G_INC2">GENERAR_INCOMING</label>
					</div>
					</form>
			   <%} %>
		</td>
		
		<td  class = "table-active" style="vertical-align: middle; text-align: center;">
		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=25')">
			  Mostrar datos
			</button>
		</td>
      </tr>
      
      <tr class = "list">
		  <td class = "table-active">
			<a href = "javascript: callServlet('Task.htm?task=26')">Generar Incoming Visa Nacional</a>
		  </td>
		  
		  <td class = "table-active">
			<% String var25 = session.getAttribute("GENERAR_INCOMING_VISA_NAC").toString();
			  	if(var25.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "G_I_V_NAC">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="G_I_V_NAC" onclick="callServlet('updateParametro.htm?paramName=GENERATE_INCOMING_VISA_NAC&nuevoEstado=1');">
					  <label class="custom-control-label" for="G_I_V_NAC">GENERAR_INCOMING_VISA_NAC</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "G_I_V_NAC2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="G_I_V_NAC2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=GENERATE_INCOMING_VISA_NAC&nuevoEstado=0');">
					  <label class="custom-control-label" for="G_I_V_NAC2">GENERAR_INCOMING_VISA_NAC</label>
					</div>
					</form>
			   <%} %>
		  </td>
		  
		  <td  class = "table-active" style="vertical-align: middle; text-align: center;">
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=26')">
			  Mostrar datos
			</button>
		  </td>
	  </tr>
      
      <tr class = "list">
		  <td class = "table-active">
			<a href = "javascript: callServlet('Task.htm?task=27')">Generar Incoming Visa Internacional</a>
		  </td>
		  
		  <td class = "table-active">
			<% String var26 = session.getAttribute("GENERAR_INCOMING_VISA_INTERNACIONAL").toString();
			  	if(var26.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "G_I_V_I">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="G_I_V_I" onclick="callServlet('updateParametro.htm?paramName=GENERATE_INCOMING_VISA_INT&nuevoEstado=1');">
					  <label class="custom-control-label" for="G_I_V_I">GENERAR_INCOMING_VISA_INTERNACIONAL</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "G_I_V_I2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="G_I_V_I2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=GENERATE_INCOMING_VISA_INT&nuevoEstado=0');">
					  <label class="custom-control-label" for="G_I_V_I2">GENERAR_INCOMING_VISA_INTERNACIONAL</label>
					</div>
					</form>
			   <%} %>
		  </td>
		  
		  <td  class = "table-active" style="vertical-align: middle; text-align: center;">
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=27')">
			  Mostrar datos
			</button>
		  </td>
	  </tr>
      
	  <tr class = "list">
		  <td class = "table-active">
			<a href = "javascript: callServlet('Task.htm?task=28')">Generar Solicitud 60</a>
		  </td>
		  
		  <td class = "table-active">
			<% String var27 = session.getAttribute("GENERAR_SOLICITUD60").toString();
			  	if(var27.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "G_S60">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="G_S60" onclick="callServlet('updateParametro.htm?paramName=GENERATE_DEBITS_AND_CREDITS&nuevoEstado=1');">
					  <label class="custom-control-label" for="G_S60">GENERAR_SOLICITUD60</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "G_S602">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="G_S602" checked="checked" onclick="callServlet('updateParametro.htm?paramName=GENERATE_DEBITS_AND_CREDITS&nuevoEstado=0');">
					  <label class="custom-control-label" for="G_S602">GENERAR_SOLICITUD60</label>
					</div>
					</form>
			   <%} %>
		  </td>
		  
		  <td  class = "table-active" style="vertical-align: middle; text-align: center;">
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=28')">
			  Mostrar datos
			</button>
		  </td>
	  </tr>
	  
	  <tr class = "list">
		  <td class = "table-active">
			<a href = "javascript: callServlet('Task.htm?task=29')">Subir Incoming</a>
		  </td>
		  
		  <td class = "table-active">
			<% String var28 = session.getAttribute("SUBIR_INCOMING").toString();
			  	if(var28.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "S_INC">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="S_INC" onclick="callServlet('updateParametro.htm?paramName=UPLOAD_INCOMING_TO_TRANSBANK&nuevoEstado=1');">
					  <label class="custom-control-label" for="S_INC">SUBIR_INCOMING</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "S_INC2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="S_INC2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=UPLOAD_INCOMING_TO_TRANSBANK&nuevoEstado=0');">
					  <label class="custom-control-label" for="S_INC2">SUBIR_INCOMING</label>
					</div>
					</form>
			   <%} %>
		  </td>
		  
		  <td  class = "table-active" style="vertical-align: middle; text-align: center;">
		  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=29')">
			  Mostrar datos
			</button>
		  </td>
	  </tr>
	  
	  <tr class = "list">
		  <td class = "table-active">
			<a href = "javascript: callServlet('Task.htm?task=30')">Subir Incoming Visa Nacional</a>
		  </td>
		  
		  <td class = "table-active">
			<% String var29 = session.getAttribute("SUBIDA_INCOMING_VISA_NAC").toString();
			  	if(var29.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "S_I_V_NAC">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="S_I_V_NAC" onclick="callServlet('updateParametro.htm?paramName=UPLOAD_INCOMING_VISA_NAC&nuevoEstado=1');">
					  <label class="custom-control-label" for="S_I_V_NAC">SUBIDA_INCOMING_VISA_NAC</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "S_I_V_NAC2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="S_I_V_NAC2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=UPLOAD_INCOMING_VISA_NAC&nuevoEstado=0');">
					  <label class="custom-control-label" for="S_I_V_NAC2">SUBIDA_INCOMING_VISA_NAC</label>
					</div>
					</form>
			   <%} %>
		  </td>
		  
		  <td  class = "table-active" style="vertical-align: middle; text-align: center;">
		  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=30')">
			  Mostrar datos
			</button>
		  </td>
	  </tr>
	  
	   <tr class = "list">
		  <td class = "table-active">
			<a href = "javascript: callServlet('Task.htm?task=31')">Subir Incoming Visa Internacional</a>
		  </td>
		  
		  <td class = "table-active">
			<% String var30 = session.getAttribute("SUBIDA_INCOMING_VISA_INTERNACIONAL").toString();
			  	if(var30.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "S_I_V_Inter">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="S_I_V_Inter" onclick="callServlet('updateParametro.htm?paramName=UPLOAD_INCOMING_VISA_INT&nuevoEstado=1');">
					  <label class="custom-control-label" for="S_I_V_Inter">SUBIDA_INCOMING_VISA_INTERNACIONAL</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "S_I_V_Inter2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="S_I_V_Inter2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=UPLOAD_INCOMING_VISA_INT&nuevoEstado=0');">
					  <label class="custom-control-label" for="S_I_V_Inter2">SUBIDA_INCOMING_VISA_INTERNACIONAL</label>
					</div>
					</form>
			   <%} %>
		  </td>
		  
		  <td  class = "table-active" style="vertical-align: middle; text-align: center;">
		  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=31')">
			  Mostrar datos
			</button>
		  </td>
	  </tr>
	  
	  <tr class = "list">
		  <td class = "table-active">
			<a href = "javascript: callServlet('Task.htm?task=32')">Subir Solicitud 60</a>
		  </td>
		  
		  <td class = "table-active">
			<% String var31 = session.getAttribute("SUBIR_SOLICITUD60").toString();
			  	if(var31.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "S_S60">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="S_S60" onclick="callServlet('updateParametro.htm?paramName=UPLOAD_CREDITS_AND_DEBITS&nuevoEstado=1');">
					  <label class="custom-control-label" for="S_S60">SUBIR_SOLICITUD60</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "S_S602">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="S_S602" checked="checked" onclick="callServlet('updateParametro.htm?paramName=UPLOAD_CREDITS_AND_DEBITS&nuevoEstado=0');">
					  <label class="custom-control-label" for="S_S602">SUBIR_SOLICITUD60</label>
					</div>
					</form>
			   <%} %>
		  </td>
		  
		  <td  class = "table-active" style="vertical-align: middle; text-align: center;">
		  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=32')">
			  Mostrar datos
			</button>
		  </td>
	  </tr>
	  
	  <tr class = "list">
		  <td class = "table-active">
			<a href = "javascript: callServlet('Task.htm?task=33')">Generar Correo Venc Transac</a>
		  </td>
		  
		  <td class = "table-active">
			<% String var32 = session.getAttribute("GENERAR_CORREO").toString();
			  	if(var32.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "G_C">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="G_C" onclick="callServlet('updateParametro.htm?paramName=GENERATE_EMAIL_EXPIRATION&nuevoEstado=1');">
					  <label class="custom-control-label" for="G_C">GENERAR_CORREO</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "G_C2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="G_C2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=GENERATE_EMAIL_EXPIRATION&nuevoEstado=0');">
					  <label class="custom-control-label" for="G_C2">GENERAR_CORREO</label>
					</div>
					</form>
			   <%} %>
		  </td>
		  
		  <td  class = "table-active" style="vertical-align: middle; text-align: center;">
		  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=33')">
			  Mostrar datos
			</button>
		  </td>
	  </tr>
    
    
     <tr>
		<td rowspan="2" style = "vertical-align: middle; text-align: center;">
			<a class = "btn btn-primary" role  = "button" href = "javascript: callServlet('Jobs.htm?nameJob=4')">Ejecutar Job4</a>
		</td>
		<td  class = "list">
			<a href = "javascript: callServlet('Task.htm?task=34')">Generar Contable</a>
		</td>
		<td  class = "list">
			<% String var33 = session.getAttribute("GENERAR_CONTABLE").toString();
			  	if(var33.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "G_CONTA">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="G_CONTA" onclick="callServlet('updateParametro.htm?paramName=GENERATE_ACCOUNTING_FILE&nuevoEstado=1');">
					  <label class="custom-control-label" for="G_CONTA">GENERAR_CONTABLE</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "G_CONTA2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="G_CONTA2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=GENERATE_ACCOUNTING_FILE&nuevoEstado=0');">
					  <label class="custom-control-label" for="G_CONTA2">GENERAR_CONTABLE</label>
					</div>
					</form>
			   <%} %>
		</td>
		
		<td  class = "list" style="vertical-align: middle; text-align: center;">
		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=34')">
			  Mostrar datos
			</button>
		</td>
      </tr>
      
	  <tr  class = "list">
		  <td>
			<a href = "javascript: callServlet('Task.htm?task=35')">Subir Asientos Contables</a>
		  </td>
		  
		  <td>
			<% String var34 = session.getAttribute("SUBIR_ASIENTOS_CONTABLES").toString();
			  	if(var34.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "S_A_C">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="S_A_C" onclick="callServlet('updateParametro.htm?paramName=UPLOAD_ACCOUNTING_FILE&nuevoEstado=1');">
					  <label class="custom-control-label" for="S_A_C">SUBIR_ASIENTOS_CONTABLES</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "S_A_C2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="S_A_C2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=UPLOAD_ACCOUNTING_FILE&nuevoEstado=0');">
					  <label class="custom-control-label" for="S_A_C2">SUBIR_ASIENTOS_CONTABLES</label>
					</div>
					</form>
			   <%} %>
		  </td>
		  <td  class = "list" style="vertical-align: middle; text-align: center;">
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=35')">
			  Mostrar datos
			</button>
		  </td>
	  </tr>
    
    
     <tr>
		<td class = "table-active" rowspan="2" style = "vertical-align: middle; text-align: center;">
			<a class = "btn btn-primary" role  = "button" href = "javascript: callServlet('Jobs.htm?nameJob=5')">Ejecutar Job5</a>
		</td>
		<td  class = "list table-active">
			<a href = "javascript: callServlet('Task.htm?task=36')">Descargar Archivo BOL</a>
		</td>
		<td  class = "list table-active">
			<% String var35 = session.getAttribute("DESCARGAR_ARCHIVO_BOL").toString();
			  	if(var35.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "D_A_B">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="D_A_B" onclick="callServlet('updateParametro.htm?paramName=DOWNLOAD_BOL&nuevoEstado=1');">
					  <label class="custom-control-label" for="D_A_B">DESCARGAR_ARCHIVO_BOL</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "D_A_B2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="D_A_B2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=DOWNLOAD_BOL&nuevoEstado=0');">
					  <label class="custom-control-label" for="D_A_B2">DESCARGAR_ARCHIVO_BOL</label>
					</div>
					</form>
			   <%} %>
		</td>
		
		<td  class = "table-active" style="vertical-align: middle; text-align: center;">
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=36')">
			  Mostrar datos
			</button>
		</td>
      </tr>
      
	  <tr  class = "list">
		  <td class = "table-active">
			<a href = "javascript: callServlet('Task.htm?task=37')">Subir Archivo BOL</a>
		  </td>
		  
		  <td class = "table-active">
			<% String var36 = session.getAttribute("SUBIR_ARCHIVO_BOL").toString();
			  	if(var36.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "S_A_B">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="S_A_B" onclick="callServlet('updateParametro.htm?paramName=UPLOAD_BOL&nuevoEstado=1');">
					  <label class="custom-control-label" for="S_A_B">SUBIR_ARCHIVO_BOL</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "S_A_B2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="S_A_B2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=UPLOAD_BOL&nuevoEstado=0');">
					  <label class="custom-control-label" for="S_A_B2">SUBIR_ARCHIVO_BOL</label>
					</div>
					</form>
			   <%} %>
		  </td>
		  <td  class = "table-active" style="vertical-align: middle; text-align: center;">
		  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=37')">
			  Mostrar datos
			</button>
		  </td>
	  </tr>
    
   
   <tr>
		<td rowspan="3" style = "vertical-align: middle; text-align: center;">
			<a class = "btn btn-primary" role  = "button" href = "javascript: callServlet('Jobs.htm?nameJob=6')">Ejecutar Job6</a></th>
		</td>
		<td class = "list">
			<a href = "javascript: callServlet('Task.htm?task=38')">Procesar Conciliación</a>
		</td>
		<td class = "list">
			<% String var37 = session.getAttribute("PROCESAR_CONCILIACION").toString();
			  	if(var37.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "P_CONCI">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="P_CONCI" onclick="callServlet('updateParametro.htm?paramName=PROCESS_AVA_CONCI&nuevoEstado=1');">
					  <label class="custom-control-label" for="P_CONCI">PROCESAR_CONCILIACION</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "P_CONCI2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="P_CONCI2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=PROCESS_AVA_CONCI&nuevoEstado=0');">
					  <label class="custom-control-label" for="P_CONCI2">PROCESAR_CONCILIACION</label>
					</div>
					</form>
			   <%} %>
		</td>
		
		<td  class = "list" style="vertical-align: middle; text-align: center;">
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=38')">
			  Mostrar datos
			</button>
		</td>
      </tr>
      
	  <tr class = "list">
		  <td>
			<a href = "javascript: callServlet('Task.htm?task=39')">Generar Contable Avances</a>
		  </td>
		  
		  <td>
			<% String var38 = session.getAttribute("GENERAR_CONTABLE_AVANCE").toString();
			  	if(var38.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "G_C_AVA">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="G_C_AVA" onclick="callServlet('updateParametro.htm?paramName=GENERATE_ACCOUNTING_FILE_AVA&nuevoEstado=1');">
					  <label class="custom-control-label" for="G_C_AVA">GENERAR_CONTABLE_AVANCE</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "G_C_AVA2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="G_C_AVA2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=GENERATE_ACCOUNTING_FILE_AVA&nuevoEstado=0');">
					  <label class="custom-control-label" for="G_C_AVA2">GENERAR_CONTABLE_AVANCE</label>
					</div>
					</form>
			   <%} %>
		  </td>
		  
		  <td  class = "list" style="vertical-align: middle; text-align: center;">
		  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=39')">
			  Mostrar datos
			</button>
		  </td>
	  </tr>
	  
	  <tr class = "list">
		  <td>
			<a href = "javascript: callServlet('Task.htm?task=40')">Subir Asientos Contables Avance</a>
		  </td>
		  
		  <td>
			<% String var39 = session.getAttribute("SUBIR_ASIENTO_CONTABLE_AVANCE").toString();
			  	if(var39.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "S_C_AVA">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="S_C_AVA" onclick="callServlet('updateParametro.htm?paramName=UPLOAD_ACCOUNTING_FILE_AVA&nuevoEstado=1');">
					  <label class="custom-control-label" for="S_C_AVA">SUBIR_ASIENTO_CONTABLE_AVANCE</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "S_C_AVA2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="S_C_AVA2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=UPLOAD_ACCOUNTING_FILE_AVA&nuevoEstado=0');">
					  <label class="custom-control-label" for="S_C_AVA2">SUBIR_ASIENTO_CONTABLE_AVANCE</label>
					</div>
					</form>
			   <%} %>
		  </td>
		  
		  <td  class = "list" style="vertical-align: middle; text-align: center;">
		  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=40')">
			  Mostrar datos
			</button>
		  </td>
	  </tr>
	  
     <tr>
		<td class = "table-active" rowspan="3" style = "vertical-align: middle; text-align: center;">
			<a class = "btn btn-primary" role  = "button" href = "javascript: callServlet('Jobs.htm?nameJob=7')">Ejecutar Job7</a>
		</td>
		<td  class = "list table-active">
			<a href = "javascript: callServlet('Task.htm?task=41')">Proceso Regenerar Contable</a>
		</td>
		<td  class = "list table-active">
			<% String var40 = session.getAttribute("PROCESO_REGENERAR_CONTABLE").toString();
			  	if(var40.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "P_R_C">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="P_R_C" onclick="callServlet('updateParametro.htm?paramName=REGENERATE_ACCOUNTING_FILE_AVA&nuevoEstado=1');">
					  <label class="custom-control-label" for="P_R_C">PROCESO_REGENERAR_CONTABLE</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "P_R_C2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="P_R_C2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=REGENERATE_ACCOUNTING_FILE_AVA&nuevoEstado=0');">
					  <label class="custom-control-label" for="P_R_C2">PROCESO_REGENERAR_CONTABLE</label>
					</div>
					</form>
			   <%} %>
		</td>
		
		<td  class = "table-active" style="vertical-align: middle; text-align: center;">
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=41')">
			  Mostrar datos
			</button>
		</td>
      </tr>
      
	  <tr  class = "list">
		  <td class = "table-active">
			<a href = "javascript: callServlet('Task.htm?task=42')">Resubir Asientos Contables Avance</a>
		  </td>
		  
		  <td class = "table-active">
			<% String var41 = session.getAttribute("RESUBIR_ASIENTOS_CONTABLES_AVANCE").toString();
			  	if(var41.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "R_A_C_A">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="R_A_C_A" onclick="callServlet('updateParametro.htm?paramName=REUPLOAD_ACCOUNTING_FILE_AVA&nuevoEstado=1');">
					  <label class="custom-control-label" for="R_A_C_A">RESUBIR_ASIENTOS_CONTABLES_AVANCE</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "R_A_C_A2">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="R_A_C_A2" checked="checked" onclick="callServlet('updateParametro.htm?paramName=REUPLOAD_ACCOUNTING_FILE_AVA&nuevoEstado=0');">
					  <label class="custom-control-label" for="R_A_C_A2">RESUBIR_ASIENTOS_CONTABLES_AVANCE</label>
					</div>
					</form>
			   <%} %>
		  </td>
		  
		  <td  class = "table-active" style="vertical-align: middle; text-align: center;">
		  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=42')">
			  Mostrar datos
			</button>
		  </td>
	  </tr>
	  <tr  class = "list">
		  <td class = "table-active">
			<a href = "javascript: callServlet('Task.htm?task=43')">Generar Reporte Detalle Contable</a>
		  </td>
		  
		  <td class = "table-active">
			<% String var42 = session.getAttribute("GENERAR_REPORTE_ARCHIVO_CONTABLE").toString();
			  	if(var42.equalsIgnoreCase("false")){
			   %>
			   <form method="post" name = "R_A_C_ARC">
			   		<div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="R_A_C_ARC" onclick="callServlet('updateParametro.htm?paramName=GENERAR_REPORTE_ARCHIVO_CONTABLE&nuevoEstado=1');">
					  <label class="custom-control-label" for="R_A_C_ARC">GENERAR_REPORTE_ARCHIVO_CONTABLE</label>
					</div>
					</form>
			   <%} else{ %>
			   <form method="post" name = "R_A_C_A2RC">
			   <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="R_A_C_A2RC" checked="checked" onclick="callServlet('updateParametro.htm?paramName=GENERAR_REPORTE_ARCHIVO_CONTABLE&nuevoEstado=0');">
					  <label class="custom-control-label" for="R_A_C_A2RC">GENERAR_REPORTE_ARCHIVO_CONTABLE</label>
					</div>
					</form>
			   <%} %>
		  </td>
		  
		  <td  class = "table-active" style="vertical-align: middle; text-align: center;">
		  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getParam('mostrarParam.htm?task=43')">
			  Mostrar datos
			</button>
		  </td>
	  </tr>
  </tbody>
   
</table>

</div>
		</center>
	
</body>

 
  </html>