<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Login Page</title>
<%@include file="templates/header.html" %>

<script>


function quitarFormato(rut)
        {
          var strRut = new String(rut);
          while( strRut.indexOf(".") != -1 )
          {
          strRut = strRut.replace(".","");
          }
          while( strRut.indexOf("-") != -1 )
          {
          strRut = strRut.replace("-","");
          }
          
          return strRut;
        }

function formato_rut(rut)
{
    var sRut1 =  quitarFormato(rut.value);      //contador para saber cuando insertar el . o la -
    var nPos = 0; //Guarda el rut invertido con los puntos y el guión agregado
    var sInvertido = ""; //Guarda el resultado final del rut como debe ser
    var sRut = "";
    for(var i = sRut1.length - 1; i >= 0; i-- )
    {
        sInvertido += sRut1.charAt(i);
        if (i == sRut1.length - 1 )
            sInvertido += "-";
        else if (nPos == 3 && sRut1[i])
        {
            sInvertido += ".";
            nPos = 0;
        }
        nPos++;
    }
    for(var j = sInvertido.length - 1; j >= 0; j-- )
    {
        if (sInvertido.charAt(sInvertido.length - 1) != ".")
            sRut += sInvertido.charAt(j);
        else if (j != sInvertido.length - 1 )
            sRut += sInvertido.charAt(j);
    }
    //Pasamos al campo el valor formateado
    rut.value = sRut.toUpperCase();
}


</script>


<style>
.error {
	padding: 15px;
	margin-bottom: 20px;
	border: 1px solid transparent;
	border-radius: 4px;
	color: #a94442;
	background-color: #f2dede;
	border-color: #ebccd1;
}

.msg {
	padding: 15px;
	margin-bottom: 20px;
	border: 1px solid transparent;
	border-radius: 4px;
	color: #31708f;
	background-color: #d9edf7;
	border-color: #bce8f1;
}

#login-box {
	width: 300px;
	padding: 20px;
	margin: 100px auto;
	background: #fff;
	-webkit-border-radius: 2px;
	-moz-border-radius: 2px;
	border: 1px solid #000;
}
</style>


</head>
<body onload='document.loginForm.username.focus();'>

	
<div class="container">

    <div class="row">
      <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
        <div class="card card-signin my-5">
        
          <div class="card-body">
          <center><div class="col-12 user-img">
                    <img src="pages/Images/user.png"/>
                </div></center>
          
            <h3 class="card-title text-center">Iniciar Sesión</h3>
            <c:if test="${not empty error}">
			<div class="error">${error}</div>
		</c:if>
		<c:if test="${not empty msg}">
			<div class="msg">${msg}</div>
		</c:if>
		
		
		
            <form class="form-signin" id = "loginForm" name='loginForm' action="<c:url value='loginform.htm' />" method='POST'>
            
              <div class="form-label-group">
                <input type="text" id="inputEmail" name='username' class="form-control" onblur="formato_rut(this);" required >
                <label for="inputEmail">RUT</label>
              </div>

              <div class="form-label-group">
                <input type="password" id="inputPassword" name='password' class="form-control" placeholder="Password" required>
                <label for="inputPassword">Contraseña</label>
              </div>

              <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Entrar</button>
              
              <input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
              
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>


</body>

</html>