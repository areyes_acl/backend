package com.sbpay.sgi.cron.config;

import java.io.Serializable;

public class CronDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4594636772831771580L;
	private String jobName;
	private String jobGroup;
	private String cronTriggerName;
	private String cronTriggerGroup;
	private String cronTriggerTimer;

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getJobGroup() {
		return jobGroup;
	}

	public void setJobGroup(String jobGroup) {
		this.jobGroup = jobGroup;
	}

	public String getCronTriggerName() {
		return cronTriggerName;
	}

	public void setCronTriggerName(String cronTriggerName) {
		this.cronTriggerName = cronTriggerName;
	}

	public String getCronTriggerGroup() {
		return cronTriggerGroup;
	}

	public void setCronTriggerGroup(String cronTriggerGroup) {
		this.cronTriggerGroup = cronTriggerGroup;
	}

	public String getCronTriggerTimer() {
		return cronTriggerTimer;
	}

	public void setCronTriggerTimer(String cronTriggerTimer) {
		this.cronTriggerTimer = cronTriggerTimer;
	}

	@Override
	public String toString() {
		return "CronDetails [jobName=" + jobName + ", jobGroup=" + jobGroup
				+ ", cronTriggerName=" + cronTriggerName
				+ ", cronTriggerGroup=" + cronTriggerGroup
				+ ", cronTriggerTimer=" + cronTriggerTimer + "]";
	}

}
