package com.sbpay.sgi.cron.servlet;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class JobInitConfig extends HttpServlet {
    
    private static final Logger LOG = Logger.getLogger( JobInitConfig.class );
    /** Constante de directorio aplicación. */
    private static final String DIRAPPCONFIG = "dirAppConfig";
    /** Constante de serverPath. */
    private static final String USERDIR = "user.dir";
    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;
    
    public JobInitConfig() {
        super();
    }
    
    public void init( final ServletConfig config ) throws ServletException {
        super.init();
        try {
            
            cargaLog4jConfig( config );
            cargaJobTransbank();
            
        }
        catch ( Exception ex ) {
            LOG.error( ex.getMessage(), ex );
            throw new ServletException( ex );
        }
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 7/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * Carga los primeros Jobs Ejecutar
     * 
     * @since 1.X
     */
    private void cargaJobTransbank() {
        LOG.info( "=======CARGANDO SCHEDULER PARA EJECUCION DE JOBS=========" );
        try {
            CronScheduler scheduler = new CronScheduler();
            scheduler.runJobs();
            
        }
        catch ( Exception e ) {
            LOG.error( "HA OCURRIDO UN ERROR AL INICIAR EL SCHEDULER", e );
        }
    }
    
    private void cargaLog4jConfig( ServletConfig config ) throws IOException {
        InputStream inStream = null;
        final String serverPath = System.getProperty( USERDIR ).replaceAll(
                "\\\\", "/" );
        try {
            final String file = config.getServletContext().getInitParameter(
                    "log4jConfig" );
            final String dirApp = config.getServletContext().getInitParameter(
                    DIRAPPCONFIG );
            inStream = new BufferedInputStream( new FileInputStream( serverPath
                    + dirApp + file ) );
            Properties p = new Properties();
            p.load( inStream );
            PropertyConfigurator.configure( p );
        }
        finally {
            if ( inStream != null ) {
                inStream.close();
            }
        }
    }
}
