package com.sbpay.sgi.cron.servlet;

import java.text.ParseException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

import com.sbpay.sgi.cron.config.CronDetails;
import com.sbpay.sgi.cron.ds.PropertiesConfiguration;
import com.sbpay.sgi.cron.enums.PropertyEntryTypes;
import com.sbpay.sgi.cron.job.FirstJob;
import com.sbpay.sgi.cron.job.FiveJob;
import com.sbpay.sgi.cron.job.FourJob;
import com.sbpay.sgi.cron.job.SecondJob;
import com.sbpay.sgi.cron.job.SevenJob;
import com.sbpay.sgi.cron.job.ThirdJob;
import com.sbpay.sgi.cron.job.SixJob;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 6/1/2016, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class CronScheduler {

	/** The Constant LOGGER. */
	private static final Logger LOG = Logger.getLogger(CronScheduler.class);

	/** Var schedulers **/
	private Scheduler scheduler1 = null;
	private Scheduler scheduler2 = null;
	private Scheduler scheduler3 = null;
	private Scheduler scheduler4 = null;
	private Scheduler scheduler5 = null;
	private Scheduler scheduler6 = null;
	private Scheduler scheduler7 = null;

	/** Var Cron Details */
	private CronDetails cronDetailFirst;
	private CronDetails cronDetailSecond;
	private CronDetails cronDetailThird;
	private CronDetails cronDetailFour;
	private CronDetails cronDetailFive;
	private CronDetails cronDetailSix;
	private CronDetails cronDetailSeven;

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 6/1/2016, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @throws Exception
	 * @since 1.X
	 */
	public CronScheduler() {
		loadCronDetails();
		LOG.debug("1)" + cronDetailFirst);
		LOG.debug("2)" + cronDetailSecond);
		LOG.debug("3)" + cronDetailThird);
		LOG.debug("4)" + cronDetailFour);
		LOG.debug("5)" + cronDetailFive);
		LOG.debug("6)" + cronDetailSix);
		LOG.debug("7)" + cronDetailSeven);
	}
	
	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 8/1/2016, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @throws SchedulerException
	 * @throws ParseException
	 * @throws InterruptedException
	 * 
	 * @since 1.X
	 */
	public void runJobs() throws ParseException, SchedulerException,
			InterruptedException {
		lanzarPrimerJob();

		lanzarSegundoJob();

		lanzarTercerJob();

		lanzarCuartoJob();

		lanzarQuintoJob();

		lanzarSextoJob();

		lanzarSeptimoJob();
		
		Thread.sleep(100000);
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 8/1/2016, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * Metodo encargado de lanzar el primer Job
	 * 
	 * @throws ParseException
	 * @throws SchedulerException
	 * @since 1.X
	 */
	private void lanzarPrimerJob() throws ParseException, SchedulerException {
		// Configura JobDetails
		JobDetail job1 = new JobDetail();
		job1.setName(cronDetailFirst.getJobName());
		job1.setGroup(cronDetailFirst.getJobGroup());
		job1.setJobClass(FirstJob.class);

		// CONFIGURAR TIEMPO DE EJECUCION (minutos)
		CronTrigger trigger1 = new CronTrigger(
				cronDetailFirst.getCronTriggerName(),
				cronDetailFirst.getCronTriggerGroup(),
				cronDetailFirst.getCronTriggerTimer());

		// CONFIGURA SCHEDULER
		scheduler1 = new StdSchedulerFactory().getScheduler();
		scheduler1.start();
		scheduler1.scheduleJob(job1, trigger1);
		LOG.info("===SE CONFIGURA CORRECTAMENTE EL JOB 1 : "
				+ cronDetailFirst.getCronTriggerTimer() + " ===");
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 8/1/2016, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo encargado de lanzar el segundo Job QUE EN AMBIENTE PRODUCTIVO DEBE
	 * LANZARSE A LAS 4 AM. TODOS LOS DIAS
	 * 
	 * @throws ParseException
	 * @throws SchedulerException
	 * @since 1.X
	 */
	private void lanzarSegundoJob() throws ParseException, SchedulerException {
		JobDetail job2 = new JobDetail();
		job2.setName("job2");
		job2.setJobClass(SecondJob.class);

		CronTrigger trigger2 = new CronTrigger(
				cronDetailSecond.getCronTriggerName(),
				cronDetailSecond.getCronTriggerGroup(),
				cronDetailSecond.getCronTriggerTimer());

		scheduler2 = new StdSchedulerFactory().getScheduler();
		scheduler2.start();
		scheduler2.scheduleJob(job2, trigger2);
		LOG.info("===SE CONFIGURA CORRECTAMENTE EL JOB 2 : "
				+ cronDetailSecond.getCronTriggerTimer() + " ===");
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 8/1/2016, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo encargado de lanzar el tercer Job El que en un ambiente productivo
	 * debe ser ejecutado a las 19 horas
	 * 
	 * @throws ParseException
	 * @throws SchedulerException
	 * @since 1.X
	 */
	private void lanzarTercerJob() throws ParseException, SchedulerException {
		JobDetail job3 = new JobDetail();
		job3.setName("job3");
		job3.setJobClass(ThirdJob.class);

		CronTrigger trigger3 = new CronTrigger(
				cronDetailThird.getCronTriggerName(),
				cronDetailThird.getCronTriggerGroup(),
				cronDetailThird.getCronTriggerTimer());

		scheduler3 = new StdSchedulerFactory().getScheduler();
		scheduler3.start();
		scheduler3.scheduleJob(job3, trigger3);
		LOG.info("===SE CONFIGURA CORRECTAMENTE EL JOB 3 :  "
				+ cronDetailThird.getCronTriggerTimer() + " ===");
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 8/1/2016, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo encargado de lanzar el cuarto Job El que en un ambiente productivo
	 * debe ser ejecutado a las 13 horas
	 * 
	 * @throws ParseException
	 * @throws SchedulerException
	 * @since 1.X
	 */
	private void lanzarCuartoJob() throws ParseException, SchedulerException {
		JobDetail job4 = new JobDetail();
		job4.setName("job4");
		job4.setJobClass(FourJob.class);

		CronTrigger trigger4 = new CronTrigger(
				cronDetailFour.getCronTriggerName(),
				cronDetailFour.getCronTriggerGroup(),
				cronDetailFour.getCronTriggerTimer());

		scheduler4 = new StdSchedulerFactory().getScheduler();
		scheduler4.start();
		scheduler4.scheduleJob(job4, trigger4);
		LOG.info("===SE CONFIGURA CORRECTAMENTE EL JOB 4 :  "
				+ cronDetailFour.getCronTriggerTimer() + " ===");
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @throws ParseException
	 * @throws SchedulerException
	 * 
	 * @since 1.X
	 */
	private void lanzarQuintoJob() throws ParseException, SchedulerException {
		JobDetail job5 = new JobDetail();
		job5.setName("job5");
		job5.setJobClass(FiveJob.class);

		CronTrigger trigger5 = new CronTrigger(
				cronDetailFive.getCronTriggerName(),
				cronDetailFive.getCronTriggerGroup(),
				cronDetailFive.getCronTriggerTimer());

		scheduler5 = new StdSchedulerFactory().getScheduler();
		scheduler5.start();
		scheduler5.scheduleJob(job5, trigger5);
		LOG.info("===SE CONFIGURA CORRECTAMENTE EL JOB 5 :  "
				+ cronDetailFive.getCronTriggerTimer() + " ===");

	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 08/08/2018, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * Metodo encargado de lanzar el sexto Job
	 * 
	 * @throws ParseException
	 * @throws SchedulerException
	 * @since 1.X
	 */
	private void lanzarSextoJob() throws ParseException, SchedulerException {
		// Configura JobDetails
		JobDetail job6 = new JobDetail();
		job6.setName(cronDetailSix.getJobName());
		job6.setGroup(cronDetailSix.getJobGroup());
		job6.setJobClass(SixJob.class); // clase del job

		// CONFIGURAR TIEMPO DE EJECUCION (minutos)
		CronTrigger trigger6 = new CronTrigger(
				cronDetailSix.getCronTriggerName(),
				cronDetailSix.getCronTriggerGroup(),
				cronDetailSix.getCronTriggerTimer());

		// CONFIGURA SCHEDULER
		scheduler6 = new StdSchedulerFactory().getScheduler();
		scheduler6.start();
		scheduler6.scheduleJob(job6, trigger6);
		LOG.info("===SE CONFIGURA CORRECTAMENTE EL JOB 6 : "
				+ cronDetailSix.getCronTriggerTimer() + " ===");
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 24/01/2019, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * Metodo encargado de lanzar el septimo Job
	 * 
	 * @throws ParseException
	 * @throws SchedulerException
	 * @since 1.X
	 */
	private void lanzarSeptimoJob() throws ParseException, SchedulerException {
		// Configura JobDetails
		JobDetail job7 = new JobDetail();
		job7.setName(cronDetailSeven.getJobName());
		job7.setGroup(cronDetailSeven.getJobGroup());
		job7.setJobClass(SevenJob.class); // clase del job

		// CONFIGURAR TIEMPO DE EJECUCION (minutos)
		CronTrigger trigger7 = new CronTrigger(
				cronDetailSeven.getCronTriggerName(),
				cronDetailSeven.getCronTriggerGroup(),
				cronDetailSeven.getCronTriggerTimer());

		// CONFIGURA SCHEDULER
		scheduler7 = new StdSchedulerFactory().getScheduler();
		scheduler7.start();
		scheduler7.scheduleJob(job7, trigger7);
		LOG.info("===SE CONFIGURA CORRECTAMENTE EL JOB 7 : "
				+ cronDetailSeven.getCronTriggerTimer() + " ===");
	}	

	

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @throws SchedulerException
	 * @since 1.X
	 */
	public void shutdownJobs() throws SchedulerException {

		if (scheduler1 != null && !scheduler1.isShutdown()) {
			scheduler1.shutdown(false);
		}
		if (scheduler2 != null && !scheduler2.isShutdown()) {
			scheduler2.shutdown(false);
		}
		if (scheduler3 != null && !scheduler3.isShutdown()) {
			scheduler3.shutdown(false);
		}
		if (scheduler4 != null && !scheduler4.isShutdown()) {
			scheduler4.shutdown(false);
		}
		if (scheduler5 != null && !scheduler5.isShutdown()) {
			scheduler5.shutdown(false);
		}

		// job 6
		if (scheduler6 != null && !scheduler6.isShutdown()) {
			scheduler6.shutdown(false);
		}
		// job 7
		if (scheduler7 != null && !scheduler7.isShutdown()) {
			scheduler7.shutdown(false);
		}


	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 8/1/2016, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo encargado de cargar los detalles de cada cron
	 * 
	 *
	 *
	 * 
	 */
	private void loadCronDetails() {
		Properties prop = PropertiesConfiguration.getCronProperties();

		// PRIMER JOB
		cronDetailFirst = new CronDetails();
		cronDetailFirst.setJobName(prop
				.getProperty(PropertyEntryTypes.FIRST_JOB_NAME.getKey()));
		cronDetailFirst.setJobGroup(prop
				.getProperty(PropertyEntryTypes.FIRST_JOB_GROUP.getKey()));
		cronDetailFirst.setCronTriggerName(prop
				.getProperty(PropertyEntryTypes.FIRST_JOB_CRON_TRIGGER_NAME
						.getKey()));
		cronDetailFirst.setCronTriggerGroup(prop
				.getProperty(PropertyEntryTypes.FIRST_JOB_CRON_TRIGGER_GROUP
						.getKey()));
		cronDetailFirst.setCronTriggerTimer(prop
				.getProperty(PropertyEntryTypes.FIRST_JOB_CRON_TRIGGER_TIMER
						.getKey()));

		// SEGUNDO JOB
		cronDetailSecond = new CronDetails();
		cronDetailSecond.setJobName(prop
				.getProperty(PropertyEntryTypes.SECOND_JOB_NAME.getKey()));
		cronDetailSecond.setJobGroup(prop
				.getProperty(PropertyEntryTypes.SECOND_JOB_GROUP.getKey()));
		cronDetailSecond.setCronTriggerName(prop
				.getProperty(PropertyEntryTypes.SECOND_JOB_CRON_TRIGGER_NAME
						.getKey()));
		cronDetailSecond.setCronTriggerGroup(prop
				.getProperty(PropertyEntryTypes.SECOND_JOB_CRON_TRIGGER_GROUP
						.getKey()));
		cronDetailSecond.setCronTriggerTimer(prop
				.getProperty(PropertyEntryTypes.SECOND_JOB_CRON_TRIGGER_TIMER
						.getKey()));

		// TERCER JOB
		cronDetailThird = new CronDetails();
		cronDetailThird.setJobName(prop
				.getProperty(PropertyEntryTypes.THIRD_JOB_NAME.getKey()));
		cronDetailThird.setJobGroup(prop
				.getProperty(PropertyEntryTypes.THIRD_JOB_GROUP.getKey()));
		cronDetailThird.setCronTriggerName(prop
				.getProperty(PropertyEntryTypes.THIRD_JOB_CRON_TRIGGER_NAME
						.getKey()));
		cronDetailThird.setCronTriggerGroup(prop
				.getProperty(PropertyEntryTypes.THIRD_JOB_CRON_TRIGGER_GROUP
						.getKey()));
		cronDetailThird.setCronTriggerTimer(prop
				.getProperty(PropertyEntryTypes.THIRD_JOB_CRON_TRIGGER_TIMER
						.getKey()));

		// CUARTO JOB
		cronDetailFour = new CronDetails();
		cronDetailFour.setJobName(prop
				.getProperty(PropertyEntryTypes.FOUR_JOB_NAME.getKey()));
		cronDetailFour.setJobGroup(prop
				.getProperty(PropertyEntryTypes.FOUR_JOB_GROUP.getKey()));
		cronDetailFour.setCronTriggerName(prop
				.getProperty(PropertyEntryTypes.FOUR_JOB_CRON_TRIGGER_NAME
						.getKey()));
		cronDetailFour.setCronTriggerGroup(prop
				.getProperty(PropertyEntryTypes.FOUR_JOB_CRON_TRIGGER_GROUP
						.getKey()));
		cronDetailFour.setCronTriggerTimer(prop
				.getProperty(PropertyEntryTypes.FOUR_JOB_CRON_TRIGGER_TIMER
						.getKey()));

		// QUINTO JOB
		cronDetailFive = new CronDetails();
		cronDetailFive.setJobName(prop
				.getProperty(PropertyEntryTypes.FIVE_JOB_NAME.getKey()));
		cronDetailFive.setJobGroup(prop
				.getProperty(PropertyEntryTypes.FIVE_JOB_GROUP.getKey()));
		cronDetailFive.setCronTriggerName(prop
				.getProperty(PropertyEntryTypes.FIVE_JOB_GROUP.getKey()));
		cronDetailFive.setCronTriggerGroup(prop
				.getProperty(PropertyEntryTypes.FIVE_JOB_CRON_TRIGGER_GROUP
						.getKey()));
		cronDetailFive.setCronTriggerTimer(prop
				.getProperty(PropertyEntryTypes.FIVE_JOB_CRON_TRIGGER_TIMER
						.getKey()));

		// SEXTO JOB
		cronDetailSix = new CronDetails();
		cronDetailSix.setJobName(prop
				.getProperty(PropertyEntryTypes.SIX_JOB_NAME.getKey()));
		cronDetailSix.setJobGroup(prop
				.getProperty(PropertyEntryTypes.SIX_JOB_GROUP.getKey()));
		cronDetailSix.setCronTriggerName(prop
				.getProperty(PropertyEntryTypes.SIX_JOB_GROUP.getKey()));
		cronDetailSix.setCronTriggerGroup(prop
				.getProperty(PropertyEntryTypes.SIX_JOB_CRON_TRIGGER_GROUP
						.getKey()));
		cronDetailSix.setCronTriggerTimer(prop
				.getProperty(PropertyEntryTypes.SIX_JOB_CRON_TRIGGER_TIMER
						.getKey()));

		// SEPTIMO JOB
		cronDetailSeven = new CronDetails();
		cronDetailSeven.setJobName(prop
				.getProperty(PropertyEntryTypes.SEVEN_JOB_NAME.getKey()));
		cronDetailSeven.setJobGroup(prop
				.getProperty(PropertyEntryTypes.SEVEN_JOB_GROUP.getKey()));
		cronDetailSeven.setCronTriggerName(prop
				.getProperty(PropertyEntryTypes.SEVEN_JOB_GROUP.getKey()));
		cronDetailSeven.setCronTriggerGroup(prop
				.getProperty(PropertyEntryTypes.SEVEN_JOB_CRON_TRIGGER_GROUP
						.getKey()));
		cronDetailSeven.setCronTriggerTimer(prop
				.getProperty(PropertyEntryTypes.SEVEN_JOB_CRON_TRIGGER_TIMER
						.getKey()));
		

	}

	//Métodos para ejecutar Jobs en la web
	/*
	public void ExecPrimerJob() throws ParseException, SchedulerException,
			InterruptedException
	{
	  lanzarPrimerJob();
	}
	
	public void ExecSegundoJob() throws ParseException, SchedulerException,
	InterruptedException
	{
		lanzarSegundoJob();
		
	}
	
	public void ExecTercerJob() throws ParseException, SchedulerException,
	InterruptedException
	{
		lanzarTercerJob();
		
	}
	
	public void ExecCuartoJob() throws ParseException, SchedulerException,
	InterruptedException
	{
		lanzarCuartoJob();
		
	}
	
	public void ExecQuintoJob() throws ParseException, SchedulerException,
	InterruptedException
	{
			lanzarQuintoJob();
		
	}
	
	public void ExecSextoJob() throws ParseException, SchedulerException,
	InterruptedException
	{
			lanzarSextoJob();
		
	}
	
	public void ExecSeptimoJob() throws ParseException, SchedulerException,
	InterruptedException
	{
		lanzarSeptimoJob();
		
	}
	
	*/
	
	
}
