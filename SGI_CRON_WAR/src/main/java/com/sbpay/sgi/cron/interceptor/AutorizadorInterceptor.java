package com.sbpay.sgi.cron.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class AutorizadorInterceptor extends HandlerInterceptorAdapter {

	
	public boolean preHandle(HttpServletRequest req, HttpServletResponse res) throws Exception{
		
		 String url = req.getRequestURI();
		 if(url.endsWith("login.htm")||url.contains("pages"))
		 {
			 
			 return true;
		 }
		
		if(req.getSession().getAttribute("usuariologado")!=null)
		{
			
			return true;
		}
		else{
			
			res.sendRedirect("login.htm");
			return false;
		}
		
	}
}
