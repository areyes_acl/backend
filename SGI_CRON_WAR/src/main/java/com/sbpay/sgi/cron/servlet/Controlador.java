 package com.sbpay.sgi.cron.servlet;



import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dto.CronTaskParam;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.job.FirstJob;
import com.sbpay.sgi.cron.job.FiveJob;
import com.sbpay.sgi.cron.job.FourJob;
import com.sbpay.sgi.cron.job.SecondJob;
import com.sbpay.sgi.cron.job.SevenJob;
import com.sbpay.sgi.cron.job.SixJob;
import com.sbpay.sgi.cron.job.ThirdJob;
import com.sbpay.sgi.cron.srv.utils.CommonOutgoingSrv;
import com.sbpay.sgi.cron.util.EncriptacionClave;
import com.sbpay.sgi.cron.validation.ProcessValidator;
import com.sbpay.sgi.cron.validation.impl.ProcessValidatorImpl;
import com.google.gson.Gson;




@Controller
public class Controlador {
	
	private FirstJob job1 = new FirstJob();
	private SecondJob job2 = new SecondJob();
	private ThirdJob job3 = new ThirdJob();
	private FourJob job4 = new FourJob();
	private FiveJob job5 = new FiveJob();
	private SixJob job6 = new SixJob();
	private SevenJob job7 = new SevenJob();
	ProcessValidator validator = new ProcessValidatorImpl();
	
	
	
	///Spring Security see this :
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(
		@RequestParam(value = "error", required = false) String error,
		@RequestParam(value = "logout", required = false) String logout){

		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error", "Usuario o contraseña incorrecta!");
		}

		if (logout != null) {
			model.addObject("msg", "Usted cerró sesión correctamente!");
		}
		
		return model;

	}
	
	
	@RequestMapping("/logout")
	public String logout(HttpSession session)
	{
		session.invalidate();
		return "redirect:login.htm?logout=1";
		
	}
	
	
		@RequestMapping(value = "/loginform", method = RequestMethod.POST)
		public String efectuarLogin(String username, String password, HttpSession session) throws SQLException, AppException
		{	
	
		if(CommonOutgoingSrv.getUsuarioServ(EncriptacionClave.TiraPuntosGuiones(username), password))
		{
			
			session.setAttribute("usuariologado", username);
			return "redirect:Inicio.htm";
		
		}
		else{
			
			return "redirect:login.htm?error=1";
		}
		}
	
	
		@RequestMapping(value = "/mostrarParam", method = RequestMethod.GET)
		public void mostrarParam(@RequestParam("task") int task, HttpSession session, HttpServletRequest req, HttpServletResponse res)throws AppException, IOException, SQLException{
			 
			if(session.getAttribute("usuariologado")!=null)
			{
				List<CronTaskParam> list = CommonOutgoingSrv.datosParametros(task);		
				
				String json = new Gson().toJson(list);
				res.setContentType("application/json");
			    res.setCharacterEncoding("UTF-8");
				
				
				res.getWriter().write(json);
			}
			
			else{
				
				res.getWriter().write("error");
				
			}
		}
		
	@RequestMapping(value = "/Inicio")
	public String inicio(HttpSession session) throws AppException
	{
		if(session.getAttribute("usuariologado")!=null)
		{
			//ModelAndView model = new ModelAndView("Inicio");
		
			
		//Job 1
				session.setAttribute("DESCARGAR_OUTGOING", validator.isActiveProcess(ProcessCodeType.DESCARGAR_OUTGOING));
				session.setAttribute("DESCARGAR_COMISIONES_TRANSBANK", validator.isActiveProcess(ProcessCodeType.DESCARGAR_COMISIONES));
				session.setAttribute("DESCARGAR_COMPENSACIONES_PAGO", validator.isActiveProcess(ProcessCodeType.DESCARGAR_COMPENSACION_DE_PAGOS));
				session.setAttribute("SUBIR_OUTGOING_NAC_IC", validator.isActiveProcess(ProcessCodeType.SUBIDA_OUTGOING_A_IC));
				session.setAttribute("DESCARGAR_OUTGOING_VISA_NAC", validator.isActiveProcess(ProcessCodeType.DESCARGAR_OUTGOING_VISA_NAC));
				session.setAttribute("SUBIR_OUTGOING_VISA_NAC_IC", validator.isActiveProcess(ProcessCodeType.SUBIDA_OUTGOING_VISA_NAC_A_IC));
				session.setAttribute("DESCARGAR_OUTGOING_VISA", validator.isActiveProcess(ProcessCodeType.DESCARGAR_OUTGOING_VISA));
				session.setAttribute("SUBIR_OUTGOING_VISA_A_IC", validator.isActiveProcess(ProcessCodeType.SUBIDA_OUTGOING_VISA_A_IC));
				
				
				
				//joB 2
				session.setAttribute("DESCARGAR_RECHAZOS", validator.isActiveProcess(ProcessCodeType.DESCARGAR_RECHAZOS));
				session.setAttribute("DESCARGAR_LOG_AUTORIZACION", validator.isActiveProcess(ProcessCodeType.DESCARGAR_LOG_DE_TRANSACCIONES_AUTORIZADAS));
				session.setAttribute("PROCESAR_LOG_AUTORIZACION", validator.isActiveProcess(ProcessCodeType.PROCESAR_LOG_DE_TRANSACCIONES_AUTORIZADAS));
				session.setAttribute("PROCESAR_OUTGOING_TRANSBANK", validator.isActiveProcess(ProcessCodeType.PROCESAR_OUTGOING));
				session.setAttribute("PROCESAR_OUTGOING_VISA_NAC", validator.isActiveProcess(ProcessCodeType.PROCESAR_OUTGOING_VISA_NAC));
				session.setAttribute("PROCESAR_OUTGOING_VISA_INT", validator.isActiveProcess(ProcessCodeType.PROCESAR_OUTGOING_VISA_INT));
				session.setAttribute("GENERAR_ITZ_AVANCES", validator.isActiveProcess(ProcessCodeType.GENERAR_ARCHIVO_AVA));
				session.setAttribute("DESCARGAR_BICE", validator.isActiveProcess(ProcessCodeType.DESCARGAR_AVANCES_BICE));
				session.setAttribute("DESCARGAR_AVANCES", validator.isActiveProcess(ProcessCodeType.DESCARGAR_AVANCES));
				session.setAttribute("DESCARGAR_ONUS", validator.isActiveProcess(ProcessCodeType.DESCARGAR_ONUS));
				session.setAttribute("PROCESAR_BICE", validator.isActiveProcess(ProcessCodeType.PROCESAR_BICE));
				session.setAttribute("PROCESAR_AVANCES", validator.isActiveProcess(ProcessCodeType.PROCESAR_AVANCES));
				session.setAttribute("PROCESAR_ONUS", validator.isActiveProcess(ProcessCodeType.PROCESAR_ONUS));		
				session.setAttribute("PROCESAR_RECHAZOS_TRANSBANK", validator.isActiveProcess(ProcessCodeType.PROCESAR_RECHAZOS));
				session.setAttribute("PROCESAR_COMISIONES", validator.isActiveProcess(ProcessCodeType.PROCESAR_COMISIONES));
				session.setAttribute("PROCESAR_CPAGOS", validator.isActiveProcess(ProcessCodeType.PROCESAR_COMPENSACION_DE_PAGOS));
				
				//Job 3
				session.setAttribute("GENERAR_INCOMING", validator.isActiveProcess(ProcessCodeType.GENERAR_INCOMING));
				session.setAttribute("GENERAR_INCOMING_VISA_NAC", validator.isActiveProcess(ProcessCodeType.GENERAR_INCOMING_VISA_NAC));
				session.setAttribute("GENERAR_INCOMING_VISA_INTERNACIONAL", validator.isActiveProcess(ProcessCodeType.GENERAR_INCOMING_VISA_INTERNACIONAL));
				session.setAttribute("GENERAR_SOLICITUD60", validator.isActiveProcess(ProcessCodeType.GENERAR_CARGOS_Y_ABONOS));
				session.setAttribute("SUBIR_INCOMING", validator.isActiveProcess(ProcessCodeType.SUBIDA_INCOMING_A_TRANSBANK));
				session.setAttribute("SUBIDA_INCOMING_VISA_NAC", validator.isActiveProcess(ProcessCodeType.SUBIDA_INCOMING_VISA_NAC));
				session.setAttribute("SUBIDA_INCOMING_VISA_INTERNACIONAL", validator.isActiveProcess(ProcessCodeType.SUBIDA_INCOMING_VISA_INTERNACIONAL));
				session.setAttribute("SUBIR_SOLICITUD60", validator.isActiveProcess(ProcessCodeType.SUBIDA_CARGOS_Y_ABONOS));
				session.setAttribute("GENERAR_CORREO", validator.isActiveProcess(ProcessCodeType.GENERAR_CORREO_DE_AVISO_VENCIMIENTO));
				
				//Job 4
				session.setAttribute("GENERAR_CONTABLE", validator.isActiveProcess(ProcessCodeType.GENERAR_ARCHIVO_CONTABLE));
				session.setAttribute("SUBIR_ASIENTOS_CONTABLES", validator.isActiveProcess(ProcessCodeType.SUBIDA_ARCHIVO_CONTABLE));
				
				//Job5
				session.setAttribute("DESCARGAR_ARCHIVO_BOL", validator.isActiveProcess(ProcessCodeType.DESCARGAR_ARCHIVO_BOL));
				session.setAttribute("SUBIR_ARCHIVO_BOL", validator.isActiveProcess(ProcessCodeType.SUBIDA_ARCHIVO_BOL));
				
				//Job6
				session.setAttribute("PROCESAR_CONCILIACION", validator.isActiveProcess(ProcessCodeType.PROCESAR_CONCILIACION));
				session.setAttribute("GENERAR_CONTABLE_AVANCE", validator.isActiveProcess(ProcessCodeType.GENERAR_ARCHIVO_CONTABLE_AVA));
				session.setAttribute("SUBIR_ASIENTO_CONTABLE_AVANCE", validator.isActiveProcess(ProcessCodeType.SUBIDA_ARCHIVO_CONTABLE_AVA));
				
				//Job7
				session.setAttribute("PROCESO_REGENERAR_CONTABLE", validator.isActiveProcess(ProcessCodeType.REGENERAR_ARCHIVO_CONTABLE_AVA));
				session.setAttribute("RESUBIR_ASIENTOS_CONTABLES_AVANCE", validator.isActiveProcess(ProcessCodeType.RESUBIDA_ARCHIVO_CONTABLE_AVA));
				session.setAttribute("GENERAR_REPORTE_ARCHIVO_CONTABLE", validator.isActiveProcess(ProcessCodeType.GENERAR_REPORTE_ARCHIVO_CONTABLE));
		
				return "Inicio";
				//return model;
		}
		
			return "redirect:login.htm";
			//return login(null, null);
		
	}
		
	
	//href = "updateParametro?paramNum=1&nuevoEstado=0"
	
	@RequestMapping("/updateParametro")
	public void updateParametro(@RequestParam("paramName") String paramName, 
			@RequestParam("nuevoEstado") int est, HttpSession session, HttpServletResponse res) throws AppException, IOException
	{
		
		if(session.getAttribute("usuariologado")!=null)
		{
		CommonOutgoingSrv.updateTblCronConfig(paramName, est);
			res.setCharacterEncoding("UTF-8");
		 	res.getWriter().write("Se cambió correctamente el estado del parámetro");
		}
		else{
			
			res.getWriter().write("Error");
		}
	}	
	
	@RequestMapping("/updateValor.htm")
	public void updateValor(@RequestParam("sid") int sid, @RequestParam("valor") String valor, HttpSession session, HttpServletResponse res) throws AppException, IOException{
		
		if(session.getAttribute("usuariologado") != null){
			CommonOutgoingSrv.updateValor(sid, valor);
			res.setCharacterEncoding("UTF-8");
			res.getWriter().write("Se actualizó el parametro correctamente");
		}
		else{
			res.getWriter().write("No se pudo actualzar el parametro");
		}
		
	}
	
	
	

	@RequestMapping("/Jobs")
	public void Jobs(@RequestParam("nameJob") int nameJob, HttpSession session, HttpServletResponse res) throws AppException, IOException
	{
		
		if(session.getAttribute("usuariologado")!=null)
		{
		switch (nameJob) {
		case 1:
			job1.execute(null);
			break;
			
		case 2:
			job2.execute(null);
			break;	

		case 3:
			job3.execute(null);
			break;
		case 4:
			job4.execute(null);
			break;
		case 5:
			job5.execute(null);
			break;
		case 6:
			job6.execute(null);
			break;
		case 7:
			job7.execute(null);
			break;
		default:
			break;
			
			
		}
		
		res.setCharacterEncoding("UTF-8");
		res.getWriter().write("Se ejecutó correctamente el Job");
		}
		else{
			res.getWriter().write("error");
		}
			
	}	
	
	
	@RequestMapping("/Task")
	public void Task(@RequestParam("task") int task, HttpSession session, HttpServletResponse res) throws AppException, IOException
	{
		if(session.getAttribute("usuariologado")!=null)
		{
		switch (task) {
		case 1://Descargar Outgoing Transbank 
			job1.descargarOutgoingTransbank();
			break;
			
		case 2://Descargar Comisiones Transbank
			job1.descargarComisionesTransbank();
			break;	

		case 3://Descargar Compensación Pagos
			job1.descargarCompensacionPagos();
			break;
		case 4://Subir Outgoing Nac IC
			job1.subirOutgoingNacIC();
			break;
		case 5://Descargar Outgoing Visa Nac
			job1.descargarOutgoingVisaNac();
			break;
		case 6://Subir Outgoing Visa Nac Ic
			job1.subirOutgoingVisaNacIc();
			break;
			
		case 7: // Descargar Outgoing Visa Int
			job1.descargarOutgoingVisaInt();
			break;
		case 8: // Subir Outgoing Visa Int IC
			job1.subirOutgoingVisaIntIC();
			break;
			
					
		case 9://Descargar Rechazos
			job2.descargarRechazos();
			break;
		case 10://Descargar Log Autorización
			job2.descargarLogAutorizacion();
			break;
		case 11://Procesar Log Autorización
			job2.procesarLogAutorizacion();
			break;
		case 12://Procesar Outgoing Transbank
			job2.procesarOutgoingTransbank();
			break;
		case 13://Procesar Outgoing Visa Nac
			job2.procesarOutgoingVisaNac();
			break;
		case 14://Procesar Outgoing Visa Int
			job2.procesarOutgoingVisaInt();
			break;
		case 15://Generar Itz Avances
			job2.generarItzAvances();
			break;
		case 16://Descargar Bice
			job2.descargarBice();
			break;
		case 17://Descargar Avances
			job2.descargarAvances();
			break;
		case 18://Descargar OnUs
			job2.descargarOnUs();
			break;
		case 19://Procesar Bice
			job2.procesarBice();
			break;
		case 20://Procesar Avances
			job2.procesarAvances();
			break;
		case 21://Procesar OnUs
			job2.procesarOnUs();
			break;
		case 22://Procesar Rechazos Transbank
			job2.procesarRechazosTransbank();
			break;
		case 23://Procesar Comisiones
			job2.procesarComisiones();
			break;
		case 24://Procesar CPagos
			job2.procesarCPagos();
			break;
		
		case 25://Generar Incoming
			job3.generarIncoming();
			break;	
		case 26: // Generar Incoming Visa
			job3.generarIncomingVisa();
			break;
		case 27 : //Generar Incoming Visa Int
			job3.generarIncomingVisaInt();
			break;		
		case 28://Generar Solicitud 60
			job3.generarSolicitud60();
			break;			
		case 29://Subir Incoming
			job3.subirIncoming();
			break;
			
		case 30: //Subir Incoming Visa
			job3.subirIncomingVisa();
			break;
		case 31: //Subir Incoming Visa Int
			job3.subirIncomingVisaInt();
			break;
				
		case 32://Subir Solicitud 60
			job3.subirSolicitud60();
			break;
		
		
		case 33://Generar Correo Venc Transac
			job3.generarCorreoVencTransac();
			break;
			
		case 34://Generar Contable
			job4.generarContable();
			break;
		case 35://Subir Asientos Contables
			job4.subirAsientosContables();
			break;
		case 36://Descargar Archivo BOL
			job5.descargarArchivoBOL();
			break;
		case 37://Subir Archivo BOL
			job5.subirArchivoBOL();
			break;
		case 38://Procesar Conciliación
			job6.procesarConciliacion();
			break;
		case 39://Generar Contable Avances
			job6.generarContableAvances();
			break;
		case 40://Subir Asientos Contables Avance
			job6.subirAsientosContablesAvance();
			break;
		case 41://Proceso Regenerar Contable
			job7.ProcesoRegenerarContable();
			break;
		case 42://Resubir Asientos Contables Avance
			job7.ResubirAsientosContablesAvance();
			break;
		case 43://Resubir Asientos Contables Avance
			job7.GenerarReporteDetalleContable();
			break;
		default:
			break;
		}
		
		res.setCharacterEncoding("UTF-8");
		res.getWriter().write("Se ejecutó correctamente la tarea");
		}
		else{
			
			res.getWriter().write("error");
		}
	}	
	
	
}