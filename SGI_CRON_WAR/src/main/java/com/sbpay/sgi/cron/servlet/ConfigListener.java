package com.sbpay.sgi.cron.servlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;
import org.quartz.SchedulerException;

import com.sbpay.sgi.cron.ds.PropertiesConfiguration;

public class ConfigListener implements ServletContextListener {
	/** LOG CONSTANT **/
	private static final Logger LOG = Logger.getLogger(ConfigListener.class);
	/** Scheduler **/
	private CronScheduler scheduler = null;

	@Override
	public void contextInitialized(ServletContextEvent arg0){

		ServletContext context = arg0.getServletContext();
		PropertiesConfiguration.loadApplicationProperties(context);
		
		LOG.info("==============>         VERSION 2.3.0 =>27/08/2019       <==============");
		LOG.info("==============>SE INICIO CRON SGI sbpay CORRECTAMENTE!!!<==============");

		cargaJobTransbank();
	}

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		LOG.info("==============>SE DETIENE CRON SGI ABCDCIN CORRECTAMENTE!!!<==============");

		if (scheduler != null) {
			try {
				scheduler.shutdownJobs();
			} catch (SchedulerException e) {
				LOG.error(e.getMessage(), e);
			}
		}
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 7/1/2016, (ACL-sbpay) - versi�n inicial
	 * </ul>
	 * <p>
	 * Carga los primeros Jobs Ejecutar
	 * 
	 * @since 1.X
	 */
	private void cargaJobTransbank() {
		LOG.info("=======CARGANDO SCHEDULER PARA EJECUCION DE JOBS=========");
		try {
			scheduler = new CronScheduler();
			scheduler.runJobs();

		} catch (Exception e) {
			LOG.error("HA OCURRIDO UN ERROR AL INICIAR EL SCHEDULER", e);
		}
	}

}
