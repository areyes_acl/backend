package com.sbpay.sgi.cron.enums;


public enum LogBD {
    
    // TABLAS Y SECUENCIAS
    TABLE_LOG_VISA("TBL_LOG_VISA", "SEQ_TBL_LOG_VISA"),
    TABLE_LOG_VISA_INT("TBL_LOG_VISA_INT", "SEQ_TBL_LOG_VISA_INT"),
    TABLE_LOG_CPAGO("TBL_LOG_CPAGO", "SEQ_TBL_LOG_CPAGO"),
    TABLE_LOG_INCOMING("TBL_LOG_INC", "SEQ_TBL_LOG_INC"),
    TABLE_LOG_OUTGOING("TBL_LOG_OUT" , "SEQ_TBL_LOG_OUT "),
    TABLE_LOG_RECHAZO("TBL_LOG_RCH", "SEQ_TBL_LOG_RCH"),
    TABLE_LOG_CARGO_ABONO("TBL_LOG_CARG_ABO", "SEQ_TBL_LOG_CAR_ABO"), 
    TABLE_LOG_COMISION("TBL_LOG_COMISION", "SEQ_TBL_LOG_COMISION"),
    TABLE_LOG_TRANSAC_AUTOR("TBL_LOG_TRANSAC_AUTOR", "SEQ_TBL_LOG_TRANSAC_AUTOR"), 
    TABLE_LOG_ONUS("TBL_LOG_ONUS","SEQ_TBL_LOG_ONUS"), 
    TBL_LOG_TRANSAC_AUTOR("TBL_LOG_TRANSAC_AUTOR","SEQ_TBL_LOG_TRANSAC_AUTOR"), 
    TABLE_LOG_OUTGOING_ONUS("TBL_LOG_OUT_ONUS","SEQ_TBL_LOG_OUT_ONUS"),
    TBL_LOG_OUT_VISA("TBL_LOG_OUT_VISA","SEQ_TBL_LOG_OUT_VISA"),
    TBL_LOG_OUT_VISA_INT("TBL_LOG_OUT_VISA_INT","SEQ_TBL_LOG_OUT_VISA_INT"),
    TABLE_LOG_CONTABLE("TBL_LOG_CONTABLE","SEQ_TBL_LOG_CONTABLE"),
    TABLE_LOG_BOL("TBL_LOG_BOL", "SEQ_TBL_LOG_BOL"),
    //BICE
    TABLE_LOG_AVANCES_BICE("TBL_LOG_AVANCES_BICE", "SEQ_TBL_LOG_AVA_BICE"),
    //AVANVCES
    TABLE_LOG_AVANCES("TBL_LOG_AVANCES", "SEQ_TBL_LOG_AVANCES"),
    
    //CONCILIACILION AVNACES
    TABLE_CONC_AVANCE("TBL_CONC_AVANCE","SEQ_TBL_CONC_AVANCE")
    ;
    
    /**
     * Valor ENUM.
     */
    private String table;
    private String sequence;
    
    private LogBD( String tableName, String sequence ) {
       this.setTable( tableName );
       this.setSequence( sequence );
    }

    public String getTable() {
        return table;
    }

    public void setTable( String table ) {
        this.table = table;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence( String sequence ) {
        this.sequence = sequence;
    }
    
  
}
