package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

public class ParamUploadIncomingDTO implements Serializable {
    
    /**
     * Serial clase ParamUploadIncomingDTO.
     */
    private static final long serialVersionUID = 1L;
    private DataFTP dataFTP;
    private String pathAbcInc;
    private String pathAbcIncError;
    private String ftpPathInc;
    private String pathAbcIncBkp;
    private String formatFileInc;
    private String formatExtNameIncCtr;
    private String formatExtNameInc;
    
    public DataFTP getDataFTP() {
        return dataFTP;
    }
    
    public void setDataFTP( DataFTP dataFTP ) {
        this.dataFTP = dataFTP;
    }
    
    public String getPathAbcInc() {
        return pathAbcInc;
    }
    
    public void setPathAbcInc( String pathAbcInc ) {
        this.pathAbcInc = pathAbcInc;
    }
    
    public String getPathAbcIncError() {
        return pathAbcIncError;
    }
    
    public void setPathAbcIncError( String pathAbcIncError ) {
        this.pathAbcIncError = pathAbcIncError;
    }
    
    public String getFtpPathInc() {
        return ftpPathInc;
    }
    
    public void setFtpPathInc( String ftpPathInc ) {
        this.ftpPathInc = ftpPathInc;
    }
    
    public String getPathAbcIncBkp() {
        return pathAbcIncBkp;
    }
    
    public void setPathAbcIncBkp( String pathAbcIncBkp ) {
        this.pathAbcIncBkp = pathAbcIncBkp;
    }
    
    public String getFormatFileInc() {
        return formatFileInc;
    }
    
    public void setFormatFileInc( String formatFileInc ) {
        this.formatFileInc = formatFileInc;
    }
    
    public String getFormatExtNameIncCtr() {
        return formatExtNameIncCtr;
    }
    
    public void setFormatExtNameIncCtr( String formatExtNameIncCtr ) {
        this.formatExtNameIncCtr = formatExtNameIncCtr;
    }
    
    public String getFormatExtNameInc() {
        return formatExtNameInc;
    }
    
    public void setFormatExtNameInc( String formatExtNameInc ) {
        this.formatExtNameInc = formatExtNameInc;
    }
    
    @Override
    public String toString() {
    return "ParamUploadIncomingDTO [dataFTP=" + dataFTP + ", pathAbcInc=" + pathAbcInc
        + ", pathAbcIncError=" + pathAbcIncError + ", ftpPathInc=" + ftpPathInc
        + ", pathAbcIncBkp=" + pathAbcIncBkp + ", formatFileInc=" + formatFileInc
        + ", formatExtNameIncCtr=" + formatExtNameIncCtr + ", formatExtNameInc=" + formatExtNameInc
        + "]";
    }
    


}
