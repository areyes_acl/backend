package com.sbpay.sgi.cron.enums;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 xx/xx/2018, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public enum MsgGenerationItzAvance {
	WARNING_DATA_NOT_FOUND("ADVERTENCIA: No se ha encontrado ningun registro para poder generar el archivo."),
    ERROR_ITZ_ALREADY_EXIST("Error!!! Archivo de avances : ? , existe actualmente en el directorio, no se puede generar nuevamete."), 
	ERROR_PARAM_NOT_FOUND("ADVERTENCIA: No se han encontrado los parametros, para la generacion del archivo de avances"),
	ERROR_GEN_ITZ_FILE("Ha ocurrido un error al intentar generar el archivo de avances"), 
    SUCCESFUL_GENERATE_ITZ_TITTLE("Se ha generado interfaz de avances correctamente"),
    SUCCESFUL_GENERATE_ITZ_MSG(" El proceso de generacion de interfaz de avances ha finalizado exitosamente."), 
	ERROR_CREATE_ITZ_FILE("Error: Se ha producido un error al generar el archivo de avances")
    ;
    
    /**
     * Valor ENUM.
     */
    private final String text;
    
    /**
     * Constructor.
     * 
     * @param text
     */
    private MsgGenerationItzAvance( final String text ) {
        this.text = text;
    }
    
    /**
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
}
