package com.sbpay.sgi.cron.utils.file;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.dto.ComisionesPagoDTO;

public class ComisionesReader implements ComisionesFileUtil{

    private static final Logger LOGGER = Logger.getLogger( IncomingReader.class );

	@Override
	public List<ComisionesPagoDTO> readComisionesLine(File file) throws Exception {
		// TODO Auto-generated method stub
		List<ComisionesPagoDTO> listaComisionesPagoDTO = new ArrayList<ComisionesPagoDTO>();
		ComisionesPagoDTO comisionesPagoDTO;
		String fecha = null;
		String fila;
		Scanner scanner = null;
		System.out.println("lista:"+listaComisionesPagoDTO);
		try{
			scanner = new Scanner(file);
			while (scanner.hasNext())	{
				fila = scanner.nextLine();
				if(Integer.parseInt(fila.substring(0,1))==2){
					comisionesPagoDTO = new ComisionesPagoDTO();
					comisionesPagoDTO.setFecha(fecha);
					comisionesPagoDTO.setDetalle(fila.substring(1, 17));
					comisionesPagoDTO.setComision(Integer.parseInt(fila.substring(17, 25)));
					comisionesPagoDTO.setSignoVenta(fila.substring(34,35 ));
					comisionesPagoDTO.setMontoVenta(Integer.parseInt(fila.substring(26, 34)));
					comisionesPagoDTO.setMontoCNC(Integer.parseInt(fila.substring(53, 61 )));
					listaComisionesPagoDTO.add(comisionesPagoDTO);
				}else if(Integer.parseInt(fila.substring(0,1))==1){
					fecha = fila.substring(41, 43) + "/" + fila.substring(43, 45) + "/" +fila.substring(45, 47);
				}
			}
		}catch(Exception ex){
			LOGGER.debug("Excepcion " + ex);
			throw ex;
		}finally{
			if(scanner != null){
				scanner.close();
			}
			
			
		}
		return listaComisionesPagoDTO;
	}

	@Override
	public Boolean validarArchivo(File file) throws Exception {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(file);
		String fila;
		Integer total = 0;
		boolean flag= true;
		int contador=2;
		while (scanner.hasNext())	{
			fila = scanner.nextLine();
			try{
			if(Integer.parseInt(fila.substring(0,1))==2){
				total += Integer.parseInt(fila.substring(17, 25));
				contador++;
			}else if(Integer.parseInt(fila.substring(0,1))==9){
				if(total != Integer.parseInt(fila.substring(10, 21))){
					flag = false;
					LOGGER.info("Monto total no cuadra con footer del archivo");
				}
			}
			}catch(NumberFormatException e){
				LOGGER.info("Error en formato CEC linea "+contador +" " +e);
				if(scanner != null){
					scanner.close();
				}
				return false;
				}
		}
		
		if(scanner != null){
			scanner.close();
		}
		
		return flag;
	}
    
}
