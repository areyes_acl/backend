package com.sbpay.sgi.cron.utils.file;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import au.com.bytecode.opencsv.CSVWriter;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dto.DetalleContableBean;
import com.sbpay.sgi.cron.dto.ParamExportDetContDTO;
import com.sbpay.sgi.cron.dto.TmpExportContableDTO;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.MsgGenerarReporteDetalleContable;
import com.sbpay.sgi.cron.enums.MsgGenerationCntbl;
import com.sbpay.sgi.cron.enums.MsgRegenerationCntblAvances;
import com.sbpay.sgi.cron.utils.generals.DateUtils;

/**
 * 
 * @author dmedina
 *
 */
public class ContableExport implements ContableFileUtil {

	private static final Logger LOGGER = Logger.getLogger(ContableExport.class);
	private ParamExportDetContDTO paramExportReCnblDTO;
	public static final String COD_DESCUENTO = "100007";
    public static final String COD_PAGO = "100006";
    public static final String COD_DESCUENTO_VN = "200007";
    public static final String COD_PAGO_VN = "200006";
    public static final String COD_DESCUENTO_VI = "300007";
    public static final String COD_PAGO_VI = "300006";

	// private final String RUT_CLIENTE = "0";

	/**
	 * 
	 */
	@Override
	public void exportContableFile(String ruta, String contableFilename,
			String cntblFilenameCtr, List<TmpExportContableDTO> listaRegistros)
			throws AppException {
		String rutaFile = ruta.concat(contableFilename);
		File incomingFile = new File(rutaFile);
		incomingFile.setExecutable(true);
		incomingFile.setReadable(true);
		incomingFile.setWritable(true);

		if (incomingFile.exists()) {
			LOGGER.error(MsgGenerationCntbl.ERROR_CONTABLE_ALREADY_EXIST
					.toString());
			throw new AppException(
					MsgGenerationCntbl.ERROR_CONTABLE_ALREADY_EXIST.toString()
							.replace("?", incomingFile.getName()));
		}

		try {
			generaArchivoContable(incomingFile, listaRegistros);
			generaArchivoControl(ruta, cntblFilenameCtr, contableFilename,
					listaRegistros);
		} catch (IOException e) {
			LOGGER.error(MsgGenerationCntbl.ERROR_CREATE_CONTABLE_FILE
					.toString());
			throw new AppException(
					MsgGenerationCntbl.ERROR_CREATE_CONTABLE_FILE.toString(), e);
		}
	}

	/**
	 * 2019 -- regenerar contable pero si existe reemplazar
	 */
	@Override
	public void exportContableFileReg(String ruta, String contableFilename,
			String cntblFilenameCtr, List<TmpExportContableDTO> listaRegistros)
			throws AppException {
		String rutaFile = ruta.concat(contableFilename);
		File incomingFile = new File(rutaFile);
		incomingFile.setExecutable(true);
		incomingFile.setReadable(true);
		incomingFile.setWritable(true);

		if (incomingFile.exists()) {
			incomingFile.delete();
			LOGGER.error(MsgRegenerationCntblAvances.DELETE_SUCCESS_CONTABLE_FILE
					.toString() + " name: " + incomingFile.getName());

		}

		try {
			generaArchivoContable(incomingFile, listaRegistros);
			generaArchivoControl(ruta, cntblFilenameCtr, contableFilename,
					listaRegistros);
		} catch (IOException e) {
			LOGGER.error(MsgRegenerationCntblAvances.ERROR_CREATE_CONTABLE_FILE
					.toString());
			throw new AppException(
					MsgRegenerationCntblAvances.ERROR_CREATE_CONTABLE_FILE
							.toString(),
					e);
		}
	}
	
	@Override
	public void exportContableFileReporte(String ruta, String contableFilename, List<DetalleContableBean> listaRegistros, ParamExportDetContDTO paramExportReCnblDTO) throws AppException {
		this.paramExportReCnblDTO = paramExportReCnblDTO;
		String rutaFile = ruta.concat(contableFilename);
		File incomingFile = new File(rutaFile);
		incomingFile.setExecutable(true);
		incomingFile.setReadable(true);
		incomingFile.setWritable(true);

		if (incomingFile.exists()) {
			incomingFile.delete();
			LOGGER.error(MsgGenerarReporteDetalleContable.DELETE_SUCCESS_CONTABLE_FILE.toString() + " name: " + incomingFile.getName());
		}

		try {
			generaArchivoContableReporte(incomingFile, listaRegistros);
		} catch (IOException e) {
			LOGGER.error(MsgGenerarReporteDetalleContable.ERROR_CREATE_CONTABLE_FILE.toString());
			throw new AppException(MsgGenerarReporteDetalleContable.ERROR_CREATE_CONTABLE_FILE .toString(), e);
		}
	}
	
	/**
	 * 2019 -- Elimina de carpeta error si regenra
	 */
	@Override
	public void exportContableFileRegDelete(String ruta, String contableFilename,
			String cntblFilenameCtr)
			throws AppException {
		String rutaFile = ruta.concat(contableFilename);
		File incomingFile = new File(rutaFile);
		incomingFile.setExecutable(true);
		incomingFile.setReadable(true);
		incomingFile.setWritable(true);

		if (incomingFile.exists()) {
			incomingFile.delete();
			LOGGER.error(MsgRegenerationCntblAvances.DELETE_SUCCESS_CONTABLE_FILE
					.toString() + " name: " + incomingFile.getName());

		}
		
		String rutaFilectrl = ruta.concat(cntblFilenameCtr);
		File incomingFilectrl = new File(rutaFilectrl);
		
		if (incomingFilectrl.exists()) {
			incomingFilectrl.delete();
			LOGGER.error(MsgRegenerationCntblAvances.DELETE_SUCCESS_CONTABLE_FILE
					.toString() + " name: " + incomingFile.getName());

		}
	}

	/**
	 * 
	 * @param ruta
	 * @param controlFilename
	 * @param contableFilename
	 * @param listaRegistros
	 * @throws IOException
	 */
	private void generaArchivoControl(String ruta, String controlFilename,
			String contableFilename, List<TmpExportContableDTO> listaRegistros)
			throws IOException {
		LOGGER.info("=========> SE GENERA ARCHIVO DE CONTROL <==========");
		String pathControlFile = ruta.concat(controlFilename);
		File controlFile = new File(pathControlFile);
		BufferedWriter bw = new BufferedWriter(new FileWriter(controlFile));
		bw.write(getLineControl(controlFilename, contableFilename,
				listaRegistros));
		bw.close();

	}

	/**
	 * 
	 * @param controlFilename
	 * @param contableFilename
	 * @param listaRegistros
	 * @return
	 */
	private String getLineControl(String controlFilename,
			String contableFilename, List<TmpExportContableDTO> listaRegistros) {
		StringBuilder str = new StringBuilder();
		str.append(DateUtils.getDateTodayInYYYYMMDD())
				.append(ConstantesUtil.PUNTO_COMA.toString())
				.append(contableFilename)
				.append(ConstantesUtil.PUNTO_COMA.toString())
				.append(listaRegistros.size());
		return str.toString();
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @param incomingFile
	 * @param listaTrasacciones
	 * @throws IOException
	 * @since 1.X
	 */
	private void generaArchivoContable(File cntblFile,
			List<TmpExportContableDTO> listaRegistros) throws IOException {
		int nroLinea = 0;

		StringBuilder contenido = new StringBuilder();

		for (int i = 0; i < listaRegistros.size(); i++) {
			nroLinea++;
			contenido.append(dtoToString(nroLinea, listaRegistros.get(i)));
			// SI ES EL ULTIMO REGISTRO NO SE HACE SALTO DE LINEA
			if ((i + 1) == listaRegistros.size()) {
				contenido.replace(contenido.length() - 1, contenido.length(),
						ConstantesUtil.EMPTY.toString());
			}
		}

		// ESCRIBE EN ARCHIVO CONTABLE
		writeCntblFile(contenido.toString(), cntblFile);
	}
	
	private void generaArchivoContableReporte(File cntblFile, List<DetalleContableBean> listaRegistros) throws IOException {
		
		List<String[]> dataList = null;
		CSVWriter writer = null;
		
		String[] header = generateHeader();
		dataList = parseListToString(listaRegistros);
		try {
		writer = new CSVWriter(new FileWriter(cntblFile), ';');
	    writer.writeNext(header);
	    writer.writeAll(dataList);
		} catch (IOException e) {
		    LOGGER.error(e.getMessage(), e);
		} finally {
		    if (writer != null) {
			try {
			    writer.close();
			} catch (IOException e) {
				LOGGER.error(e.getMessage(), e);
			}
		    }
		}
	}
	
	private List<String[]> parseListToString(
		    List<DetalleContableBean> listaDetalle) {
		List<String[]> dataList = new ArrayList<String[]>();

		for (DetalleContableBean detalle : listaDetalle) {
		    String registro = "";
		    registro += detalle.getFechaContable() + ";";
		    registro += detalle.getTipoMovimiento() + ";";
		    registro += detalle.getCuentaDebito() + ";";
		    registro += detalle.getCuentaCredito() + ";";
		    registro += formateaMontoPorTipoMovimiento(detalle.getTipoMovimiento(),detalle.getMonto()) + ";";
		    registro += detalle.getSubledger() + ";";
		    registro += (detalle.getNumeroTarjeta()==null) ? "" : detalle.getNumeroTarjeta() + ";";
		    registro += (detalle.getMicrofilm()==null) ? "" : detalle.getMicrofilm() + ";";
		    registro += (detalle.getTipoVenta()==null) ? "" : detalle.getTipoVenta() +";";
		    registro += (detalle.getCantidadCuotas() == null) ? "" : detalle.getCantidadCuotas() + ";";
		    registro += detalle.getNombreArchivo() + ";";

		    dataList.add(registro.split(";"));

		}
		return dataList;
	    }
	
	 private String formateaMontoPorTipoMovimiento(String tipoMovimiento,
			    String monto) {
			if (this.paramExportReCnblDTO.getCodDESCUENTO_TBK().equalsIgnoreCase(tipoMovimiento)
			   || this.paramExportReCnblDTO.getCodPAGO_TBK().equalsIgnoreCase(tipoMovimiento)
			   || this.paramExportReCnblDTO.getCodPAGO_VN().equalsIgnoreCase(tipoMovimiento)
			   || this.paramExportReCnblDTO.getCodDESCUENTO_VN().equalsIgnoreCase(tipoMovimiento)
			   || this.paramExportReCnblDTO.getCodPAGO_VI().equalsIgnoreCase(tipoMovimiento)
			   || this.paramExportReCnblDTO.getCodDESCUENTO_VI().equalsIgnoreCase(tipoMovimiento)) {
			    return monto;
			}

			return String.valueOf(Integer.valueOf(monto) / 100);
		    }
	
	private String[] generateHeader() {
		return new String[] { "FECHA CONTABLE", "TIPO MOVIMIENTO",
			"CUENTA DEBITO", "CUENTA CREDITO", "MONTO", "SUBLEDGET",
			"NUMERO TARJETA", "MICROFILM", "TIPO VENTA",
			"CANTIDAD DE CUOTAS", "NOMBRE ARCHIVO CONTABLE" };
	    }
	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 12/02/2016, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo encargado de escribir la linea sacando la info desde el dto
	 * 
	 * @param nroLinea
	 * @param TmpExportContableDTO
	 * @since 1.X
	 */
	private String dtoToString(Integer nroLinea,
			TmpExportContableDTO tmpExportContableDTO) {

		StringBuilder str = new StringBuilder();
		String monto = "";
		if (tmpExportContableDTO.getMonto() != null
				&& tmpExportContableDTO.getMonto().length() > 2) {
			monto = tmpExportContableDTO.getMonto().substring(0,
					tmpExportContableDTO.getMonto().length() - 2);
		}
		str.append(tmpExportContableDTO.getTransaccion());
		str.append(ConstantesUtil.PUNTO_COMA.toString());
		str.append(nroLinea);
		str.append(ConstantesUtil.PUNTO_COMA.toString());
		str.append(tmpExportContableDTO.getMovOrg());
		str.append(ConstantesUtil.PUNTO_COMA.toString());
		str.append(tmpExportContableDTO.getProducto());
		str.append(ConstantesUtil.PUNTO_COMA.toString());
		str.append(tmpExportContableDTO.getSucMov());
		str.append(ConstantesUtil.PUNTO_COMA.toString());
		str.append(tmpExportContableDTO.getSucPro());
		str.append(ConstantesUtil.PUNTO_COMA.toString());
		str.append(tmpExportContableDTO.getSucPago());
		str.append(ConstantesUtil.PUNTO_COMA.toString());
		str.append(tmpExportContableDTO.getComercio());
		str.append(ConstantesUtil.PUNTO_COMA.toString());
		str.append(tmpExportContableDTO.getFecha());
		str.append(ConstantesUtil.PUNTO_COMA.toString());
		str.append(tmpExportContableDTO.getIdentificador());
		str.append(ConstantesUtil.PUNTO_COMA.toString());
		str.append(monto);
		str.append(ConstantesUtil.PUNTO_COMA.toString());
		str.append(tmpExportContableDTO.getCuenta());
		str.append(ConstantesUtil.PUNTO_COMA.toString());
		str.append(tmpExportContableDTO.getCompania());
		str.append(ConstantesUtil.PUNTO_COMA.toString());
		str.append(tmpExportContableDTO.getRutEntidad());
		str.append(ConstantesUtil.PUNTO_COMA.toString());
		str.append(tmpExportContableDTO.getMov());
		str.append(ConstantesUtil.PUNTO_COMA.toString());
		str.append(tmpExportContableDTO.getPortafolio());
		str.append(ConstantesUtil.PUNTO_COMA.toString());
		str.append(tmpExportContableDTO.getRutCliente());
		str.append(ConstantesUtil.SKIP_LINE.toString());

		return str.toString();
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 11/12/2015, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * Metodo que crea el archivo en disco y escribe todo el contenido de este.
	 * 
	 * @param footer
	 * @param contenido
	 * @param header
	 * @param incomingFile
	 * @param incomingFilename
	 * @throws IOException
	 * 
	 * @since 1.X
	 */
	private void writeCntblFile(String contenido, File incomingFile)
			throws IOException {
		BufferedWriter bw = null;
		bw = new BufferedWriter(new FileWriter(incomingFile));
		if (contenido != null && contenido.length() > 0) {
			bw.write(contenido);
		}
		bw.close();
	}

}
