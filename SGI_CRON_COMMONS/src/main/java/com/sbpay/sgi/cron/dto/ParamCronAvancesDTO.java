package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 DD/10/2018, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * Clase encargada de mapear la tabla TBL_PRTS
 * 
 * </p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class ParamCronAvancesDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -771323209087441009L;
	private DataFTP dataFTP;
	private String ftpPathAvances;
	private String formatFilenameAvances;
	private String pathAclAvances;
	private String formatFilenameAvancesCorre;
	
	public DataFTP getDataFTP() {
		return dataFTP;
	}
	public void setDataFTP(DataFTP dataFTP) {
		this.dataFTP = dataFTP;
	}
	public String getFtpPathAvances() {
		return ftpPathAvances;
	}
	public void setFtpPathAvances(String ftpPathAvances) {
		this.ftpPathAvances = ftpPathAvances;
	}
	public String getFormatFilenameAvances() {
		return formatFilenameAvances;
	}
	public void setFormatFilenameAvances(String formatFilenameAvances) {
		this.formatFilenameAvances = formatFilenameAvances;
	}
	public String getPathAclAvances() {
		return pathAclAvances;
	}
	public void setPathAclAvances(String pathAclAvances) {
		this.pathAclAvances = pathAclAvances;
	}
	public String getFormatFilenameAvancesCorre() {
		return formatFilenameAvancesCorre;
	}
	public void setFormatFilenameAvancesCorre(String formatFilenameAvancesCorre) {
		this.formatFilenameAvancesCorre = formatFilenameAvancesCorre;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public String toString() {
		return "ParamCronAvancesDTO [dataFTP=" + dataFTP + ", ftpPathAvances="
				+ ftpPathAvances + ", formatFilenameAvances="
				+ formatFilenameAvances + ", pathAclAvances=" + pathAclAvances
				+ ", formatFilenameAvancesCorre=" + formatFilenameAvancesCorre
				+ "]";
	}

	
	
	


}
