package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 16/11/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * Clase encargada de mapear la tabla, TBL_CRON_LOG
 * 
 * 
 * 
 * </p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class CronLogDTO implements Serializable {
    /**
	 * 
	 */
    private static final long serialVersionUID = 2349298215535090169L;
    private long sid;
    private String fecha;
    private String incomingName;
    private String incomingFlag;
    private String incomingTs;
    private String outgoingName;
    private String outgoingFlag;
    private String outgoingTs;
    private String rechazoName;
    private String rechazoFlag;
    private String rechazoTs;
    public long getSid() {
		return sid;
	}

	public void setSid(long sid) {
		this.sid = sid;
	}

	public String getFecha() {
        return fecha;
    }
    
    public void setFecha( String fecha ) {
        this.fecha = fecha;
    }
    
    public String getIncomingName() {
        return incomingName;
    }
    
    public void setIncomingName( String incomingName ) {
        this.incomingName = incomingName;
    }
    
    public String getIncomingFlag() {
        return incomingFlag;
    }
    
    public void setIncomingFlag( String incomingFlag ) {
        this.incomingFlag = incomingFlag;
    }
    
    public String getIncomingTs() {
        return incomingTs;
    }
    
    public void setIncomingTs( String incomingTs ) {
        this.incomingTs = incomingTs;
    }
    
    public String getOutgoingName() {
        return outgoingName;
    }
    
    public void setOutgoingName( String outgoingName ) {
        this.outgoingName = outgoingName;
    }
    
    public String getOutgoingFlag() {
        return outgoingFlag;
    }
    
    public void setOutgoingFlag( String outgoingFlag ) {
        this.outgoingFlag = outgoingFlag;
    }
    
    public String getOutgoingTs() {
        return outgoingTs;
    }
    
    public void setOutgoingTs( String outgoingTs ) {
        this.outgoingTs = outgoingTs;
    }
    
    public String getRechazoName() {
		return rechazoName;
	}

	public void setRechazoName(String rechazoName) {
		this.rechazoName = rechazoName;
	}

	public String getRechazoFlag() {
		return rechazoFlag;
	}

	public void setRechazoFlag(String rechazoFlag) {
		this.rechazoFlag = rechazoFlag;
	}

	public String getRechazoTs() {
		return rechazoTs;
	}

	public void setRechazoTs(String rechazoTs) {
		this.rechazoTs = rechazoTs;
	}

	@Override
	public String toString() {
		return "CronLogDTO [sid=" + sid + ", fecha=" + fecha
				+ ", incomingName=" + incomingName + ", incomingFlag="
				+ incomingFlag + ", incomingTs=" + incomingTs
				+ ", outgoingName=" + outgoingName + ", outgoingFlag="
				+ outgoingFlag + ", outgoingTs=" + outgoingTs
				+ ", rechazoName=" + rechazoName + ", rechazoFlag="
				+ rechazoFlag + ", rechazoTs=" + rechazoTs + "]";
	}


    
}
