package com.sbpay.sgi.cron.enums;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 16/12/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * Enum que contiene los distintos formatos utilizados en la
 * aplicacion
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public enum DateFormatType {
    
    FORMAT_MMDD("MMdd"),
    FORMAT_YYMMDD("yyMMdd"),
    FORMAT_YDDD("yDDD"),
    FORMAT_YYDDD("yyddd"),
    FORMAT_DDMMYY("ddMMyy"),
    FORMAT_DDMMYYYY("ddMMyyyy"),
    FORMAT_YYYYMMDD("yyyyMMdd"),
    FORMAT_DDMMYY_WITH_SLASH_SEPARATOR("dd/MM/yy"),
    FORMAT_DD_MMM_YYYY("dd-MMM-yyyy"),
    FORMAT_YYYY_MM_DD("yyyy-MM-dd"); 
    
    
    /**
     * Valor ENUM.
     */
    private final String text;

    /**
     * Constructor.
     * 
     * @param text
     */
    private DateFormatType(final String text) {
        this.text = text;
    }

    /**
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
 }
