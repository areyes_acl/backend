package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 25/04/2019, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * Clase que guardara una transaccion completa contemplando para esto
 * el todos los registros involucrados.
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class TransaccionOutVisaPago implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = -8665924641652098866L;
    private RegistroOutVisaPagoDTO registro00;
    
    public RegistroOutVisaPagoDTO getRegistro00() {
        return registro00;
    }
    
    public void setRegistro00( RegistroOutVisaPagoDTO registro00 ) {
        this.registro00 = registro00;
    }

	@Override
	public String toString() {
		return "TransaccionOutVisaPago [registro00=" + registro00 +"]";
	}

	
    
}
