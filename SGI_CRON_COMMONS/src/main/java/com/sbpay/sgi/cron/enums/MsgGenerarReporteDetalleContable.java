package com.sbpay.sgi.cron.enums;
/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 xx/xx/2019, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public enum MsgGenerarReporteDetalleContable {
	
	ERROR_GEN_CNTBL_FILE("Ha ocurrido un error al intentar generar el archivo de reporte detalle contable"), 
	SUCCESFUL_GENERATE_CNTBL_MSG(" El proceso para generar Archivo de reporte Contable ha finalizado exitosamente."), 
	DELETE_SUCCESS_CONTABLE_FILE("Se elimino corretamente archivo de reporte contable existente..."),
	ERROR_CREATE_CONTABLE_FILE("Error: Se ha producido un error al generar el archivo contable");
	
	  /**
     * Valor ENUM.
     */
    private final String text;
    
    /**
     * Constructor.
     * 
     * @param text
     */
    private MsgGenerarReporteDetalleContable( final String text ) {
        this.text = text;
    }
    
    /**
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }

}
