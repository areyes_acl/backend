package com.sbpay.sgi.cron.dto;

public class ComisionesPagoDTO {

	private String fecha;
	private Integer comision;
	private String detalle;
	private String signoVenta;
	private Integer montoVenta;
	private Integer montoCNC;

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public Integer getComision() {
		return comision;
	}

	public void setComision(Integer comision) {
		this.comision = comision;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public String getSignoVenta() {
		return signoVenta;
	}

	public void setSignoVenta(String signoVenta) {
		this.signoVenta = signoVenta;
	}

	public Integer getMontoVenta() {
		return montoVenta;
	}

	public void setMontoVenta(Integer montoVenta) {
		this.montoVenta = montoVenta;
	}

	public Integer getMontoCNC() {
		return montoCNC;
	}

	public void setMontoCNC(Integer montoCNC) {
		this.montoCNC = montoCNC;
	}

    @Override
    public String toString() {
        return "ComisionesPagoDTO [fecha=" + fecha + ", comision=" + comision
                + ", detalle=" + detalle + ", signoVenta=" + signoVenta
                + ", montoVenta=" + montoVenta + ", montoCNC=" + montoCNC + "]";
    }
	
	

}
