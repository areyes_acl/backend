package com.sbpay.sgi.cron.enums;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 12/02/2016, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public enum MsgUploadBol {
	
    NO_PENDING_FILES("No se han encontrado archivos pendientes en BD"),
    NO_PARAMS("No se han encontrado todos los parametros requeridos para el proceso. Revisar configuraciòn en BD."),
    PROCESS_INACTIVE("El proceso de subida de archivo BOL se encuentra desactivado o no se ha podido obtener la configuracion desde base de datos."),
    NO_CONTROL_FILE("No se ha encontrado archivo de control asociado al BOL a subir, por lo que no se realiza la subida del archivo")
    ;
    
    /**
     * Valor ENUM.
     */
    private final String text;
    
    /**
     * Constructor.
     * 
     * @param text
     */
    private MsgUploadBol( final String text ) {
        this.text = text;
    }
    
    /**
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
}
