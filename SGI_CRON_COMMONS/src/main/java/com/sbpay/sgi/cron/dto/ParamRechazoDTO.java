package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 27/11/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * Clase encargada de mapear los parametros de carga de rechazos.
 * 
 * </p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class ParamRechazoDTO implements Serializable {

	/**
	 * Serial version.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * formato comience con del archivo de rechazo.
	 */
	private String formatFileNameAbcRch;
	/**
	 * Ruta fisica donde se alojaran los rechazos.
	 */
	private String pathAbcRch;
	/**
	 * Ruta fisica donde se alojaran los archivos de rechazo con error.
	 */
	private String pathAbcRchError;
	/**
	 * Ruta fisica donde se alojaran los archivos de rechazo procesados.
	 */
	private String pathAbcRchBkp;
	/**
	 * Extension de archivos de rechazos.
	 */
	private String formatExtNameRch;
	
	public String getFormatFileNameAbcRch() {
		return formatFileNameAbcRch;
	}

	public void setFormatFileNameAbcRch(final String formatFileNameAbcRch) {
		this.formatFileNameAbcRch = formatFileNameAbcRch;
	}

	public String getPathAbcRch() {
		return pathAbcRch;
	}

	public void setPathAbcRch(final String pathAbcRch) {
		this.pathAbcRch = pathAbcRch;
	}

	public String getPathAbcRchError() {
		return pathAbcRchError;
	}

	public void setPathAbcRchError(final String pathAbcRchError) {
		this.pathAbcRchError = pathAbcRchError;
	}

	public String getPathAbcRchBkp() {
		return pathAbcRchBkp;
	}

	public void setPathAbcRchBkp(final String pathAbcRchBkp) {
		this.pathAbcRchBkp = pathAbcRchBkp;
	}

	public String getFormatExtNameRch() {
		return formatExtNameRch;
	}

	public void setFormatExtNameRch(String formatExtNameRch) {
		this.formatExtNameRch = formatExtNameRch;
	}

	@Override
	public String toString() {
		return "ParamRechazoDTO [formatFileNameAbcRch=" + formatFileNameAbcRch
				+ ", pathAbcRch=" + pathAbcRch + ", pathAbcRchError="
				+ pathAbcRchError + ", pathAbcRchBkp=" + pathAbcRchBkp
				+ ", formatExtNameRch=" + formatExtNameRch + "]";
	}

}
