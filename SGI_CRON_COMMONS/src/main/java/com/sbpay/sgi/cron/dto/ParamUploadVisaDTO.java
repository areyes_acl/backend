package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

public class ParamUploadVisaDTO implements Serializable {

  private static final long serialVersionUID = 1L;
  private String pathVisaInc;
  private String pathVisaBkp;
  private String pathVisaError;
  private String extVisaFile;
  private String filenameVisa;
  private String ftpPathVisaOut;
  private DataFTP dataFTP;
  private String formatExtNameIncVisaCrt;

  public void setFormatExtNameIncVisaCrt(final String formatExtNameIncVisaCrt) {
		this.formatExtNameIncVisaCrt = formatExtNameIncVisaCrt;
  }
  
  public String getFormatExtNameIncVisaCrt() {
      return formatExtNameIncVisaCrt;
  }
  
  public DataFTP getDataFTP() {
	return dataFTP;
}

public void setDataFTP(DataFTP dataFTP) {
	this.dataFTP = dataFTP;
}

public ParamUploadVisaDTO() {
    // TODO Auto-generated constructor stub
  }

  public String getPathVisaBkp() {
    return pathVisaBkp;
  }

  public void setPathVisaBkp(String pathVisaBkp) {
    this.pathVisaBkp = pathVisaBkp;
  }

  public String getPathVisaError() {
    return pathVisaError;
  }

  public void setPathVisaError(String pathVisaError) {
    this.pathVisaError = pathVisaError;
  }

  public String getPathVisaInc() {
    return pathVisaInc;
  }

  public void setPathVisaInc(String pathVisaInc) {
    this.pathVisaInc = pathVisaInc;
  }

  public String getExtVisaFile() {
    return extVisaFile;
  }

  public void setExtVisaFile(String extVisaFile) {
    this.extVisaFile = extVisaFile;
  }

 
  public String getFtpPathVisaOut() {
    return ftpPathVisaOut;
  }

  public void setFtpPathVisaOut(String ftpPathVisaOut) {
    this.ftpPathVisaOut = ftpPathVisaOut;
  }

  public String getFilenameVisa() {
    return filenameVisa;
  }

  public void setFilenameVisa(String filenameVisa) {
    this.filenameVisa = filenameVisa;
  }
  
  

  @Override
public String toString() {
	return "ParamUploadVisaDTO [pathVisaInc=" + pathVisaInc + ", pathVisaBkp="
			+ pathVisaBkp + ", pathVisaError=" + pathVisaError
			+ ", extVisaFile=" + extVisaFile + ", filenameVisa=" + filenameVisa
			+ ", ftpPathVisaOut=" + ftpPathVisaOut + ", dataFTP=" + dataFTP + ", filenameVisaCTL="+formatExtNameIncVisaCrt
			+ "]";
}



}
