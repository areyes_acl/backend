package com.sbpay.sgi.cron.utils.file;

import java.io.File;
import java.util.List;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dto.ICCodeHomologationDTO;
import com.sbpay.sgi.cron.dto.LogTransaccionAvancesDTO;
import com.sbpay.sgi.cron.dto.TransaccionAvancesDTO;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/10/2018, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public interface TransaccionAvancesFileUtil {

	/**
	 * 
	 * @param file
	 * @param starWith
	 * @param listaCodigosIc
	 * @return
	 * @throws AppException
	 */
	public LogTransaccionAvancesDTO readLogTrxAvancesFile(File file,
			String starWith, List<ICCodeHomologationDTO> listaCodigosIc)
			throws AppException;

	/**
	 * 
	 * @param ruta
	 * @param incomingFilename
	 * @param controlFilename
	 * @param listaTrasacciones
	 * @throws AppException
	 */
	public void exportAvancesAvancesFile(String ruta, String incomingFilename,
			String controlFilename, List<TransaccionAvancesDTO> listaTrasacciones)
			throws AppException;

}
