package com.sbpay.sgi.cron.utils.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dto.ICCodeHomologationDTO;
import com.sbpay.sgi.cron.dto.LogTransaccionBiceDTO;
import com.sbpay.sgi.cron.dto.TransaccionBiceDTO;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.MsgErrorFile;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/10/2018, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class TransaccionBiceReader implements TransaccionBiceFileUtil {

	/** VARIABLE PARA EL LOGER */
	private static final Logger LOGGER = Logger
			.getLogger(TransaccionBiceReader.class);

	private List<ICCodeHomologationDTO> listaCodigosIc = null;
	private String detalleTrxLeidasConError = "";

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/10/2018, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * GETTERS Y SETTERS
	 * 
	 * @return
	 * @since 1.X
	 */
	public String getDetalleTrxLeidasConError() {
		return detalleTrxLeidasConError;
	}

	public void setDetalleTrxLeidasConError(String detalleTrxLeidasConError) {
		this.detalleTrxLeidasConError = detalleTrxLeidasConError;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/10/2018, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @param file
	 * @param starWith
	 * @param listaCodigosIc
	 * @return
	 * @throws AppException
	 * @see com.sbpay.sgi.cron.utils.file.TransaccionBiceFileUtil#readLogTrxBiceFile(java.io.File,
	 *      java.lang.String, java.util.List)
	 * @since 1.X
	 */
	@Override
	public LogTransaccionBiceDTO readLogTrxBiceFile(File file, String starWith,
			List<ICCodeHomologationDTO> listaCodigosIc) throws AppException {

		LogTransaccionBiceDTO logTrxBiceDTO = new LogTransaccionBiceDTO();
		List<TransaccionBiceDTO> trxAutList = null;
		BufferedReader buffReader = null;
		this.listaCodigosIc = listaCodigosIc;
		try {

			if (file == null) {
				throw new AppException(
						MsgErrorFile.ERROR_READ_FILE_NULL.toString());
			}

			String ruta = file.getAbsolutePath();
			buffReader = new BufferedReader(new FileReader(ruta));

			trxAutList = readFile(buffReader);

		} catch (IOException ioe) {
			throw new AppException("Ha ocurrido un error al leer el archivo : "
					+ ioe.getMessage(), ioe);
		} finally {
			try {
				if (buffReader != null) {
					buffReader.close();
				}
			} catch (IOException ioe1) {
				throw new AppException(
						"Ha ocurrido un error al cerrar el archivo", ioe1);
			}
		}

		logTrxBiceDTO.setTrxList(trxAutList);
		logTrxBiceDTO.setDetalleLectura(detalleTrxLeidasConError);

		return logTrxBiceDTO;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/10/2018, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo encargado de ir linea a linea leyendo el archivo AVANCES BICE
	 * 
	 * @param buffReader
	 * @return
	 * @throws IOException
	 * @throws AppException
	 * @since 1.X
	 */
	private List<TransaccionBiceDTO> readFile(BufferedReader buffReader)
			throws IOException, AppException {
		String line = buffReader.readLine();
		List<TransaccionBiceDTO> listaTransacciones = new ArrayList<TransaccionBiceDTO>();
		TransaccionBiceDTO tr = null;

		// RECORRE EL FILE LEYENDO LINEA POR LINEA
		
		while (line != null) {
			try {
				tr = parseToTransaccion(line);
				if (tr != null) {
					listaTransacciones.add(tr);
				}
			} catch (AppException e) {
				LOGGER.error(e.getMessage(), e);
				detalleTrxLeidasConError = detalleTrxLeidasConError.concat(
						"\n -").concat(e.getMessage());
			}
				line = buffReader.readLine();
			

		}

		LOGGER.info("-->Nº TRX AVANCES BICE leìdas  = "
				+ listaTransacciones.size());
		
		LOGGER.info("-->DATOS BICE LEIODOS  = "
				+ listaTransacciones);
				
		return listaTransacciones;

	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/10/2018, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo encargado de parsear la linea del archivo leido a un objeto del
	 * tipo transaccionBiceDTO.
	 * 
	 * @param line
	 * @return
	 * @throws AppException
	 * @since 1.X
	 */
	private TransaccionBiceDTO parseToTransaccion(String line)
			throws AppException {
		TransaccionBiceDTO transaccion = null;

		if (line.startsWith("DT")) {
			transaccion = new TransaccionBiceDTO();
			transaccion.setTipoRegistro(line.substring(0, 2));
			transaccion.setNumOperProg(line.substring(2, 12));
			transaccion.setCodServicio(line.substring(12, 16));
			transaccion.setRutOrdenante(line.substring(16, 26));
			transaccion.setNumCtaOrdenante(line.substring(26, 44));
			transaccion.setIdOperCliente(line.substring(44, 64));
			transaccion.setCodEstado(line.substring(64, 67));
			transaccion.setGlosaEstado(line.substring(67, 117));
			transaccion.setFechaInstruccion(line.substring(117, 136));
			transaccion.setFechaContable(line.substring(136, 146));
			transaccion.setCodBancoBeneficiario(line.substring(146, 149));
			transaccion.setTipoCtaBeneficiario(line.substring(149, 152));
			transaccion.setCtaBeneficiario(line.substring(152, 172));
			transaccion.setRutBeneficiario(line.substring(172, 182));
			transaccion.setNomBeneficiario(line.substring(182, 222));
			transaccion.setMonto(parseaMontos(line.substring(222, 235))); //HAY QUE AGREGARLE DOS 00
		}

		return transaccion;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/10/2018, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @return
	 * @since 1.X
	 */
	private String parseaMontos(String monto) {

		if (monto != null
				&& !monto.equalsIgnoreCase(ConstantesUtil.EMPTY.toString())) {
			return String.valueOf(Integer.parseInt(monto) * 100);

		} else {
			return String.valueOf(0 * 100);
		}

	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/10/2018, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @param icCode
	 * @return
	 * @throws AppException
	 * @since 1.X
	 */
	private String traducirCodigoDeIcACodigoTransbank(String icCode)
			throws AppException {
		String codTrx = null;

		for (ICCodeHomologationDTO instanceIc : listaCodigosIc) {
			if (instanceIc.isActivo() && instanceIc.getCodigoIC() != null
					&& instanceIc.getCodigoIC().equalsIgnoreCase(icCode)) {
				codTrx = instanceIc.getCodigoTransbank();
				break;
			}
		}

		if (codTrx == null) {
			throw new AppException(
					"No se ha encontrado codigo de transaccion de IC : "
							+ icCode
							+ ", homologado a los codigos de transbank en la tabla TBL_PRTS_IC_CODE.");
		}

		return codTrx;
	}

	@Override
	public void exportAvancesBiceFile(String ruta, String incomingFilename,
			String controlFilename, List<TransaccionBiceDTO> listaTrasacciones) {
		// TODO Auto-generated method stub

	}

}
