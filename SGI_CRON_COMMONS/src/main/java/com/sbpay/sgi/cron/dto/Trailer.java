package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

import com.sbpay.sgi.cron.utils.generals.CommonsUtils;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 9/12/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class Trailer implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 4353065043411539939L;
    private String codi;
    private String zero;
    private String sobi;
    private String fech;
    private String sumd;
    private String numd;
    private String nuba;
    private String nure;
    private String batn;
    private String nutr;
    private String ntrf;
    
    public String getCodi() {
        return codi;
    }
    
    public void setCodi( String codi ) {
        this.codi = codi;
    }
    
    public String getZero() {
        return zero;
    }
    
    public void setZero( String zero ) {
        this.zero = zero;
    }
    
    public String getSobi() {
        return sobi;
    }
    
    public void setSobi( String sobi ) {
        this.sobi = sobi;
    }
    
    public String getFech() {
        return fech;
    }
    
    public void setFech( String fech ) {
        this.fech = fech;
    }
    
    public String getSumd() {
        return sumd;
    }
    
    public void setSumd( String sumd ) {
        this.sumd = sumd;
    }
    
    public String getNumd() {
        return numd;
    }
    
    public void setNumd( String numd ) {
        this.numd = numd;
    }
    
    public String getNuba() {
        return nuba;
    }
    
    public void setNuba( String nuba ) {
        this.nuba = nuba;
    }
    
    public String getNure() {
        return nure;
    }
    
    public void setNure( String nure ) {
        this.nure = nure;
    }
    
    public String getBatn() {
        return batn;
    }
    
    public void setBatn( String batn ) {
        this.batn = batn;
    }
    
    public String getNutr() {
        return nutr;
    }
    
    public void setNutr( String nutr ) {
        this.nutr = nutr;
    }
    
    public String getNtrf() {
        return ntrf;
    }
    
    public void setNtrf( String ntrf ) {
        this.ntrf = ntrf;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 9/12/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    public String getStringTrailer() {
        
        StringBuilder str = new StringBuilder();
        str.append( this.codi );
        str.append( CommonsUtils.getWhitesSpaceString( 2 ) );
        str.append( this.zero );
        str.append( this.sobi );
        str.append( this.fech );
        str.append( this.sumd );
        str.append( this.numd );
        str.append( this.nuba );
        str.append( this.nure );
        str.append( CommonsUtils.getZeroString( 6 ) );
        str.append( this.batn );
        str.append( this.nutr );
        str.append( CommonsUtils.getZeroString( 18 ) );
        str.append( this.ntrf );
        str.append( CommonsUtils.getZeroString( 45 ) );
        str.append( CommonsUtils.getWhitesSpaceString( 7 ) );
        
        return str.toString();
    }
    

    
    @Override
    public String toString() {
        return "Trailer [codi=" + codi + ", zero=" + zero + ", sobi=" + sobi
                + ", fech=" + fech + ", sumd=" + sumd + ", numd=" + numd
                + ", nuba=" + nuba + ", nure=" + nure + ", batn=" + batn
                + ", nutr=" + nutr + ", ntrf=" + ntrf + "]";
    }
    
}
