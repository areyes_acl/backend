package com.sbpay.sgi.cron.enums;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 21/12/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public enum MsgDownloadComisionProcess {
    
    PROCESS_FINISH_WITH_WARN("Proceso Descarga Comisiones Con observaciones"),
    ALL_FILES_ALREADY_PROCESSED("El proceso se ha ejecutado correctamente, sin embargo se encuentran todos los archivos ya procesados."),
    ALL_FILES_HAS_NOT_VALID_FORMAT("El proceso se ha ejecutado sin embargo no se ha descargado ningun archivo de comisiones."),
    NOT_HAVE_FILES("El proceso de descarga de Comsiones desde Transbank se ha ejecutado, sin embargo no existe ningun archivo que descargar desde el SFTP de transbank"),
    ERROR_MAIL_TITLE_PROCESS_DOWNLOAD("Se ha provocado un error en la ejecucion del proceso: Descarga Rechazos"),
    ERROR_SAVE_LOG("Ha ocurrido un error al intentar guardar el log de descarga de comisiones"), 
    ERROR_SEARCH_LOG("Ha ocurrido un error al intentar buscar el log de descarga de comisiones");
    
    /**
     * Valor ENUM.
     */
    private final String text;
    
    /**
     * Constructor.
     * 
     * @param text
     */
    private MsgDownloadComisionProcess( final String text ) {
        this.text = text;
    }
    
    /**
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
    
}
