package com.sbpay.sgi.cron.utils.file;

import java.util.List;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dto.DetalleContableBean;
import com.sbpay.sgi.cron.dto.ParamExportDetContDTO;
import com.sbpay.sgi.cron.dto.TmpExportContableDTO;

public interface ContableFileUtil {
	
	void exportContableFile(String ruta, String contableFilename, String contableControlFileName,List<TmpExportContableDTO> listaRegistros) throws AppException;
	
	void exportContableFileReg(String ruta, String contableFilename, String contableControlFileName,List<TmpExportContableDTO> listaRegistros) throws AppException;

	void exportContableFileRegDelete(String ruta, String contableFilename, String cntblFilenameCtr) throws AppException;
	
	void exportContableFileReporte(String ruta, String contableFilename, List<DetalleContableBean> listaRegistros, ParamExportDetContDTO paramExportReCnblDTO) throws AppException;
}
