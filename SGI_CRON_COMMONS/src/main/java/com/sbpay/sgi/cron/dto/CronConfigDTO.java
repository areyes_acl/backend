package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 18/12/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * Clase que contiene los campos de las tablas que se utilizaran para el log de
 * los procesos
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class CronConfigDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5041292737720221194L;

    private long sid;
    private int estado;
    private String codigo;
    private String descripcion;

    public long getSid() {
	return sid;
    }

    public void setSid(long sid) {
	this.sid = sid;
    }

    public int getEstado() {
	return estado;
    }

    public void setEstado(int estado) {
	this.estado = estado;
    }

    public String getCodigo() {
	return codigo;
    }

    public void setCodigo(String codigo) {
	this.codigo = codigo;
    }

    public String getDescripcion() {
	return descripcion;
    }

    public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
    }

    @Override
    public String toString() {
	return "CronConfigDTO [sid=" + sid + ", estado=" + estado + ", codigo="
		+ codigo + ", descripcion=" + descripcion + "]";
    }

}
