package com.sbpay.sgi.cron.dto;

import com.sbpay.sgi.cron.utils.generals.CommonsUtils;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 25/04/2019, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class Registro07OutVisaIntDTO extends RegistroOutVisaIntDTO {
    
	/**
     * 
     */
    private static final long serialVersionUID = -4924876189694862634L;
    private String all;
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 25/04/2019, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * Constructor
     * 
     * @since 1.X
     */
    public Registro07OutVisaIntDTO() {
        
    }
    
    

	public String getAll() {
		return all;
	}



	public void setAll(String all) {
		this.all = all;
	}



	@Override
	public String toString() {
		return "Registro07OutVisaIntDTO [all=" + all + "]";
	}
      
}
