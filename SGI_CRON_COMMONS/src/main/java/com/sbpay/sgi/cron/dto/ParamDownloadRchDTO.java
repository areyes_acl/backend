package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

public class ParamDownloadRchDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ParamDownloadRchDTO() {
	}


	//PARAMETROS PATH OUTGOING 
    private String formatFileNameOut;
    private String pathAclInc;
    private String formatExtNameOutCtr;
    private String formatExtNameOut;
	//PARAMETROS DEL FTP IC
	private String ftpIpIc;
	private String ftpPortIc;
	private String ftpUserIc;
	private String ftpPassIc;
	private String pathFtpIcEntrada;



	public String getFormatFileNameOut() {
		return formatFileNameOut;
	}
	public void setFormatFileNameOut(String formatFileNameOut) {
		this.formatFileNameOut = formatFileNameOut;
	}
	public String getPathAclInc() {
		return pathAclInc;
	}
	public void setPathAclInc(String pathAclInc) {
		this.pathAclInc = pathAclInc;
	}
	public String getFormatExtNameOutCtr() {
		return formatExtNameOutCtr;
	}
	public void setFormatExtNameOutCtr(String formatExtNameOutCtr) {
		this.formatExtNameOutCtr = formatExtNameOutCtr;
	}
	public String getFormatExtNameOut() {
		return formatExtNameOut;
	}
	public void setFormatExtNameOut(String formatExtNameOut) {
		this.formatExtNameOut = formatExtNameOut;
	}
	public String getFtpIpIc() {
		return ftpIpIc;
	}
	public void setFtpIpIc(String ftpIpIc) {
		this.ftpIpIc = ftpIpIc;
	}
	public String getFtpPortIc() {
		return ftpPortIc;
	}
	public void setFtpPortIc(String ftpPortIc) {
		this.ftpPortIc = ftpPortIc;
	}
	public String getFtpUserIc() {
		return ftpUserIc;
	}
	public void setFtpUserIc(String ftpUserIc) {
		this.ftpUserIc = ftpUserIc;
	}
	public String getFtpPassIc() {
		return ftpPassIc;
	}
	public void setFtpPassIc(String ftpPassIc) {
		this.ftpPassIc = ftpPassIc;
	}
	public String getPathFtpIcEntrada() {
		return pathFtpIcEntrada;
	}
	public void setPathFtpIcEntrada(String pathFtpIcEntrada) {
		this.pathFtpIcEntrada = pathFtpIcEntrada;
	}
	@Override
	public String toString() {
		return "ParamUploadCargoAbonoDTO [formatFileNameOut="
				+ formatFileNameOut + ", pathAclInc=" + pathAclInc
				+ ", formatExtNameOutCtr=" + formatExtNameOutCtr
				+ ", formatExtNameOut=" + formatExtNameOut + ", ftpIpIc="
				+ ftpIpIc + ", ftpPortIc=" + ftpPortIc + ", ftpUserIc="
				+ ftpUserIc + ", ftpPassIc=" + ftpPassIc
				+ ", pathFtpIcEntrada=" + pathFtpIcEntrada + "]";
	}

	
}
