package com.sbpay.sgi.cron.dto;

import java.util.List;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 16/11/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class IncomingDTO {
    
    /**
     * TODO: PENDIENTE DE AGREGAR PARAMETROS QUE LO IDENTIFIQUEN COMO
     * INCOMING
     */
    private String incomingName;
    private String fechaProceso;
    private List<Transaccion> listaTransacciones;
    private List<TransaccionOutVisaCobroCargo> listaCobroCargo;
    private Trailer batchTrailer = new Trailer();
    private Trailer fileTrailer = new Trailer();
    
    public String getIncomingName() {
        return incomingName;
    }
    
    public void setIncomingName( String incomingName ) {
        this.incomingName = incomingName;
    }
    
    public String getFechaProceso() {
        return fechaProceso;
    }
    
    public void setFechaProceso( String fechaProceso ) {
        this.fechaProceso = fechaProceso;
    }
    
    public List<Transaccion> getListaTransacciones() {
        return listaTransacciones;
    }
    
    public void setListaTransacciones( List<Transaccion> listaTransacciones ) {
        this.listaTransacciones = listaTransacciones;
    }
    
    public Trailer getBatchTrailer() {
        return batchTrailer;
    }
    
    public void setBatchTrailer( Trailer batchTrailer ) {
        this.batchTrailer = batchTrailer;
    }
    
    public Trailer getFileTrailer() {
        return fileTrailer;
    }
    
    public void setFileTrailer( Trailer fileTrailer ) {
        this.fileTrailer = fileTrailer;
    }
    
    public List<TransaccionOutVisaCobroCargo> getListaCobroCargo() {
		return listaCobroCargo;
	}

	public void setListaCobroCargo(
			List<TransaccionOutVisaCobroCargo> listaCobroCargo) {
		this.listaCobroCargo = listaCobroCargo;
	}

	@Override
	public String toString() {
		return "IncomingDTO [incomingName=" + incomingName + ", fechaProceso="
				+ fechaProceso + ", listaTransacciones=" + listaTransacciones
				+ ", listaCobroCargo=" + listaCobroCargo + ", batchTrailer="
				+ batchTrailer + ", fileTrailer=" + fileTrailer + "]";
	}

	
    
}
