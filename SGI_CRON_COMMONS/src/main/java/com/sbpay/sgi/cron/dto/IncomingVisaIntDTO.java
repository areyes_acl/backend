package com.sbpay.sgi.cron.dto;

import java.util.List;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 25/04/2019, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class IncomingVisaIntDTO {
    
	/**
     * TODO: PENDIENTE DE AGREGAR PARAMETROS QUE LO IDENTIFIQUEN COMO
     * INCOMING VISA INTERNACIONAL
     */
    private String incomingName;
    private String fechaProceso;
    private List<TransaccionOutVisaInt> listaTransacciones;
    private Trailer batchTrailer = new Trailer();
    private Trailer fileTrailer = new Trailer();
    
    public String getIncomingName() {
        return incomingName;
    }
    
    public void setIncomingName( String incomingName ) {
        this.incomingName = incomingName;
    }
    
    public String getFechaProceso() {
        return fechaProceso;
    }
    
    public void setFechaProceso( String fechaProceso ) {
        this.fechaProceso = fechaProceso;
    }
    
    public List<TransaccionOutVisaInt> getListaTransacciones() {
        return listaTransacciones;
    }
    
    public void setListaTransacciones( List<TransaccionOutVisaInt> listaTransacciones ) {
        this.listaTransacciones = listaTransacciones;
    }
    
    public Trailer getBatchTrailer() {
        return batchTrailer;
    }
    
    public void setBatchTrailer( Trailer batchTrailer ) {
        this.batchTrailer = batchTrailer;
    }
    
    public Trailer getFileTrailer() {
        return fileTrailer;
    }
    
    public void setFileTrailer( Trailer fileTrailer ) {
        this.fileTrailer = fileTrailer;
    }

	@Override
	public String toString() {
		return "IncomingVisaIntDTO [incomingName=" + incomingName
				+ ", fechaProceso=" + fechaProceso + ", listaTransacciones="
				+ listaTransacciones + ", batchTrailer=" + batchTrailer
				+ ", fileTrailer=" + fileTrailer + "]";
	}
     
    
}
