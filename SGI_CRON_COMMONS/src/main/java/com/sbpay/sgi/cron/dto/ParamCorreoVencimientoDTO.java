package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

public class ParamCorreoVencimientoDTO implements Serializable {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = -5192878648505647635L;
	private String numeroTC;
	private Integer monto;
	private String fecVencimiento;
	private String codMotivo;
	private String motivo;

	public String getNumeroTC() {
		return numeroTC;
	}

	public void setNumeroTC(String numeroTC) {
		this.numeroTC = numeroTC;
	}

	public Integer getMonto() {
		return monto;
	}

	public void setMonto(Integer monto) {
		this.monto = monto;
	}

	public String getFecVencimiento() {
		return fecVencimiento;
	}

	public void setFecVencimiento(String fecVencimiento) {
		this.fecVencimiento = fecVencimiento;
	}

	public String getCodMotivo() {
		return codMotivo;
	}

	public void setCodMotivo(String codMotivo) {
		this.codMotivo = codMotivo;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

}
