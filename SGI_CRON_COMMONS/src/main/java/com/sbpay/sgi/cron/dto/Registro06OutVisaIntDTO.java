package com.sbpay.sgi.cron.dto;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 20/11/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class Registro06OutVisaIntDTO extends RegistroOutVisaIntDTO {
    
    /**
     * 
     */
    private static final long serialVersionUID = -4924876189694862634L;
    private String all;
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 20/11/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * Constructor
     * 
     * @since 1.X
     */
    public Registro06OutVisaIntDTO() {
        
    }

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}
    
	   @Override
	    public String toString() {
	        return "Registro06OutVisaIntDTO [tipoTransaccion = " + super.getTipoTransaccion()
	                + ", transcode =" + super.getTransCode() + ", all="
	                + all + "]";
	    }
    
}
