package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

/**
 * 
 * @author dmedina
 *
 */
public class ICCodeHomologationDTO implements Serializable {
    
    private static final long serialVersionUID = 1L;
    private long sid;
    private String codigoIC;
    private String codigoTransbank;
    private String descripcion;
    private boolean activo;
    private String fechaActualizacion;
    
    public long getSid() {
        return sid;
    }
    
    public void setSid( long sid ) {
        this.sid = sid;
    }
    
    public String getCodigoIC() {
        return codigoIC;
    }
    
    public void setCodigoIC( String codigoIC ) {
        this.codigoIC = codigoIC;
    }
    
    public String getCodigoTransbank() {
        return codigoTransbank;
    }
    
    public void setCodigoTransbank( String codigoTransbank ) {
        this.codigoTransbank = codigoTransbank;
    }
    
    public String getDescripcion() {
        return descripcion;
    }
    
    public void setDescripcion( String descripcion ) {
        this.descripcion = descripcion;
    }
    
    public boolean isActivo() {
        return activo;
    }
    
    public void setActivo( boolean activo ) {
        this.activo = activo;
    }
    
    public String getFechaActualizacion() {
        return fechaActualizacion;
    }
    
    public void setFechaActualizacion( String fechaActualizacion ) {
        this.fechaActualizacion = fechaActualizacion;
    }
    
    @Override
    public String toString() {
        return "ICCodeHomologationDTO [sid=" + sid + ", codigoIC=" + codigoIC
                + ", codigoTransbank=" + codigoTransbank + ", descripcion="
                + descripcion + ", activo=" + activo + ", fechaActualizacion="
                + fechaActualizacion + "]";
    }
    
}
