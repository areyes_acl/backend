package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 16/12/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class ConfigFile implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1116159800514138149L;
    private String starWith;
    private String endsWith;
    private String starWithCtr;
    private String endsWithCtr;
    
    
    
	public ConfigFile(String starWith, String endsWith, String starWithCtr,
			String endsWithCtr) {
		super();
		this.starWith = starWith;
		this.endsWith = endsWith;
		this.starWithCtr = starWithCtr;
		this.endsWithCtr = endsWithCtr;
	}

    public ConfigFile() {
        super();
    }

	public String getStarWith() {
		return starWith;
	}

	public void setStarWith(String starWith) {
		this.starWith = starWith;
	}

	public String getEndsWith() {
		return endsWith;
	}

	public void setEndsWith(String endsWith) {
		this.endsWith = endsWith;
	}

	public String getStarWithCtr() {
		return starWithCtr;
	}

	public void setStarWithCtr(String starWithCtr) {
		this.starWithCtr = starWithCtr;
	}

	public String getEndsWithCtr() {
		return endsWithCtr;
	}

	public void setEndsWithCtr(String endsWithCtr) {
		this.endsWithCtr = endsWithCtr;
	}

	@Override
	public String toString() {
		return "ConfigFile [starWith=" + starWith + ", endsWith=" + endsWith
				+ ", starWithCtr=" + starWithCtr + ", endsWithCtr="
				+ endsWithCtr + "]";
	}
    

    
}
