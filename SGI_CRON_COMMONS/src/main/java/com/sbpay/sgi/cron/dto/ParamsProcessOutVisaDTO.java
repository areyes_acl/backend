package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class ParamsProcessOutVisaDTO implements Serializable {

	private static final long serialVersionUID = 5413221437575627278L;
	private String pathVisaInc;
	private String pathVisaIncBkp;
	private String pathVisaIncError;
	private String formatExtNameOutVisa;
	private String formatFilenameOutVisa;

	public String getPathVisaInc() {
		return pathVisaInc;
	}

	public void setPathVisaInc(String pathVisaInc) {
		this.pathVisaInc = pathVisaInc;
	}

	public String getPathVisaIncBkp() {
		return pathVisaIncBkp;
	}

	public void setPathVisaIncBkp(String pathVisaIncBkp) {
		this.pathVisaIncBkp = pathVisaIncBkp;
	}

	public String getPathVisaIncError() {
		return pathVisaIncError;
	}

	public void setPathVisaIncError(String pathVisaIncError) {
		this.pathVisaIncError = pathVisaIncError;
	}

	public String getFormatExtNameOutVisa() {
		return formatExtNameOutVisa;
	}

	public void setFormatExtNameOutVisa(String formatExtNameOutVisa) {
		this.formatExtNameOutVisa = formatExtNameOutVisa;
	}

	public String getFormatFilenameOutVisa() {
		return formatFilenameOutVisa;
	}

	public void setFormatFilenameOutVisa(String formatFilenameOutVisa) {
		this.formatFilenameOutVisa = formatFilenameOutVisa;
	}

	@Override
	public String toString() {
		return "ParamsProcessOutVisaDTO [pathVisaInc=" + pathVisaInc
				+ ", pathVisaIncBkp=" + pathVisaIncBkp + ", pathVisaIncError="
				+ pathVisaIncError + ", formatExtNameOutVisa="
				+ formatExtNameOutVisa + ", formatFilenameOutVisa="
				+ formatFilenameOutVisa + "]";
	}

}
