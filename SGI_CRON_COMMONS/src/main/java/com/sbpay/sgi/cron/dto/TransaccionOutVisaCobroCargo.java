package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

public class TransaccionOutVisaCobroCargo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private RegistroOutVisaCobroCargoDTO registro1020;

	public RegistroOutVisaCobroCargoDTO getRegistro1020() {
		return registro1020;
	}

	public void setRegistro1020(RegistroOutVisaCobroCargoDTO registro1020) {
		this.registro1020 = registro1020;
	}

	@Override
	public String toString() {
		return "TransaccionOutVisaCobroCargo [registro1020=" + registro1020
				+ "]";
	}
}
