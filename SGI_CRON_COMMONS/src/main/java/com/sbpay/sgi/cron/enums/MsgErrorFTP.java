package com.sbpay.sgi.cron.enums;

public enum MsgErrorFTP {
	ERROR_CONNECTION_FTP("Error al conectar al FTP INCOMING de Transbank"), 
	ERROR_FTP_PROCESS("Error al procesar el archivo del FTP INCOMING de transbank"), 
	ERROR_FILE_CLOSE("Error al cerrar el archivo descargado del FTP INCOMING de Transbank"), 
	ERROR_LIST_FTP("Error al listar los archivos del FTP INCOMING de Transbank"), 
	ERROR_CONNECTION_FTP_UPLOAD("Error al intentar conectar con el servidor FTP"), 
	ERROR_UPLOAD_READ_FILE("Error al leer el archivo que se desea subir al FTP"), 
	ERROR_UPLOAD("Error al subir el archivo incoming al FTP de Transbank"), 
	ERROR_DOWNLOAD_FILE("Error al intentar descargar el archivo desde el FTP"), 
	ERROR_DOWNLOAD_FILE_NOT_FOUND("Error no se ha encontrado archivo que se desea descargar, en la ruta especificada"),
	ERROR_CLOSE_FILE("Error al cerrar el archivo transferido"), 
	ERROR_UPLOAD_TXT("Estimados:\n El archivo nombrado al final no fue posible subirlo al FTP de transbank ");

	/**
	 * Valor ENUM.
	 */
	private final String text;

	/**
	 * Constructor.
	 * 
	 * @param text
	 */
	private MsgErrorFTP(final String text) {
		this.text = text;
	}

	/**
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return text;
	}
}
