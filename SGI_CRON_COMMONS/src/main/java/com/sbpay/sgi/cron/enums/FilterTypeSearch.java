package com.sbpay.sgi.cron.enums;

public enum FilterTypeSearch {
    
    FILTER_BY_NAME("name", "FILE_NAME"),
    FILTER_BY_DATE("date", "FECHA"),
    FILTER_BY_AAAAMM("date_aaaammm", "FECHA");
    
    /**
     * Valor ENUM.
     */
    private String typeSearch;
    private String columnName;
    
    private FilterTypeSearch( String typeSearch, String columnName ) {
        this.setTypeSearch( typeSearch );
        this.setColumnName( columnName );
    }
    
    public String getTypeSearch() {
        return typeSearch;
    }
    
    public void setTypeSearch( String typeSearch ) {
        this.typeSearch = typeSearch;
    }
    
    public String getColumnName() {
        return columnName;
    }
    
    public void setColumnName( String columnName ) {
        this.columnName = columnName;
    }
    
}
