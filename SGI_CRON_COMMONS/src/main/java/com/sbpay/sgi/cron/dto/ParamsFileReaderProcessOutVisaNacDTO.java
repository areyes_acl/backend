package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 17/04/2019, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * Clase encargada de mapear la tabla TBL_PRTS
 * 
 * </p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class ParamsFileReaderProcessOutVisaNacDTO implements Serializable {
    

	/**
     * 
     */
    private static final long serialVersionUID = 5413221437575627278L;
    private String pathAclIncBkp;
    private String patchAclIncError;
    private String pathAclInc;
    private String formatFilenameOut;
    private String formatExtNameOut;
    private String rutsbpay;
    
    public String getFormatExtNameOut() {
        return formatExtNameOut;
    }
    
    public void setFormatExtNameOut( String formatExtNameOut ) {
        this.formatExtNameOut = formatExtNameOut;
    }
    
    public String getPathAclIncBkp() {
        return pathAclIncBkp;
    }
    
    public void setPathAclIncBkp( String pathAclIncBkp ) {
        this.pathAclIncBkp = pathAclIncBkp;
    }
    
    public String getPatchAclIncError() {
        return patchAclIncError;
    }
    
    public void setPatchAclIncError( String patchAclIncError ) {
        this.patchAclIncError = patchAclIncError;
    }
    
    public String getPathAclInc() {
        return pathAclInc;
    }
    
    public void setPathAclInc( String pathAclInc ) {
        this.pathAclInc = pathAclInc;
    }
    
    public String getFormatFilenameOut() {
        return formatFilenameOut;
    }
    
    public void setFormatFilenameOut( String formatFilenameOut ) {
        this.formatFilenameOut = formatFilenameOut;
    }
    
    public String getRutsbpay() {
        return rutsbpay;
    }
    
    public void setRutsbpay( String rutsbpay ) {
        this.rutsbpay = rutsbpay;
    }
    
    @Override
	public String toString() {
		return "ParamsFileReaderProcessOutVisaNacDTO [pathAclIncBkp="
				+ pathAclIncBkp + ", patchAclIncError=" + patchAclIncError
				+ ", pathAclInc=" + pathAclInc + ", formatFilenameOut="
				+ formatFilenameOut + ", formatExtNameOut=" + formatExtNameOut
				+ ", rutsbpay=" + rutsbpay + "]";
	}
    
}
