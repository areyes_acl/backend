package com.sbpay.sgi.cron.utils.file;

import java.io.File;
import java.util.List;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dto.ICCodeHomologationDTO;
import com.sbpay.sgi.cron.dto.LogTransaccionAutorizada;
import com.sbpay.sgi.cron.dto.TransacionAutorizada;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public interface TransaccionAutorFileUtil  {

  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
   * </ul>
   * <p>
   * metodo que dada la ruta y el comienza con 
   * 
   * 
   * @param ruta
   * @param starWith
   * @return
   * @throws AppException
   * @since 1.X
   */
  public List<TransacionAutorizada> readLogTrxAutorFile(String fullPath, String starWith)
      throws AppException;
  
  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 2/12/2015, (ACL-sbpay) - versión inicial
   * </ul>
   * <p>
   * Metodo que lee un archivo log trx desde un file y lo
   * transforma a DTO. Se le pasa el starWith para realizar la
   * validacion de la fecha del archivo
   * 
   * @param file
   * @param starWith
   * @return
   * @throws AppException
   * @since 1.X
   */
  public LogTransaccionAutorizada  readLogTrxAutorFile( File file, String starWith,  List<ICCodeHomologationDTO> listaCodigosIc)
          throws AppException;


}
