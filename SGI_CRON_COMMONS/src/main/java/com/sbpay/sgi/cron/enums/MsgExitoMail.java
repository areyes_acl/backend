package com.sbpay.sgi.cron.enums;

public enum MsgExitoMail {
	
	// PROCESO CORREO DE VENCIMIENTO
	EXITO_PROCESS_CORREO_VENCIMIENTO("Informacion vencimiento de transacacciones"),
	
	//PROCESO DOWNLOAD OUTGOING
	EXITO_PROCESS_DOWNLOAD_TITLE("Finalizada Descarga Outgoing Transbank"),
	EXITO_PROCESS_DOWNLOAD_OUTGOING("Estimados(as): \n\n El proceso 'Descarga Outgoing desde Transbank', ha Finalizado Correctamente"), 
	
	// PROCESAR COMISIONES
	EXITO_PROCESS_COMISION_TITLE("Finaliza el Procesamiento de Comisiones"),
	EXITO_PROCESS_COMISION_BODY("Estimados(as): \n\n El proceso 'Procesar Comisiones',  ha terminado correctamente."),
	
	// PROCESAR COMPENSACION DE PAGO
	EXITO_PROCESS_CPAGO_TITLE("Finaliza el Procesamiento de Compensacion de Pagos"),
	EXITO_PROCESS_CPAGO_BODY("Estimados(as): \n\n El proceso 'Procesar Compensaciones de Pago ',  ha terminado correctamente."),
	
	// PROCESO OUTGOING TRANSBANK
	EXITO_PROCESS_OUTGOING_TITLE("Finalizado Procesamiento Outgoing Transbank"),
	EXITO_PROCESS_OUTGOING_BODY("Estimados(as): \n\n El proceso 'Procesar Outgoing de Transbank',  ha terminado correctamente."),
	
	// PROCESO OUTGOING VISA NACIONAL
	EXITO_PROCESS_OUTGOING_VISA_NAC_TITLE("Finalizado Procesamiento Outgoing Visa Nacional"),
	EXITO_PROCESS_OUTGOING_VISA_NAC_BODY("Estimados(as): \n\n El proceso 'Procesar Outgoing Visa Nacional',  ha terminado correctamente."),
	
	// PROCESO OUTGOING VISA INTERNACIONAL
	EXITO_PROCESS_OUTGOING_VISA_INT_TITLE("Finalizado Procesamiento Outgoing Visa Internacional"),
	EXITO_PROCESS_OUTGOING_VISA_INT_BODY("Estimados(as): \n\n El proceso 'Procesar Outgoing Visa Internacional',  ha terminado correctamente."),
	
	// PROCESO DESCARGA COMISIONES TBK
	EXITO_PROCESS_DOWNLOAD_COM_TITLE("Finalizada proceso de descarga de comisiones desde Transbank"),
	EXITO_PROCESS_DOWNLOAD_COM_BODY("Estimados(as): \n\n El proceso 'Descargar Archivo de Comisiones desde transbank', ha finalizado correctamente."), 
	
	//PROCESO DOWNLOAD CPAGO
    EXITO_PROCESS_DOWNLOAD_CPAGO_TITLE("Finalizada Descarga de archivo de compensacion de pago desde Transbank"),
    EXITO_PROCESS_DOWNLOAD_CPAGO_BODY("Estimados(as): \n\n El proceso 'Descarga de archivo Compensacion de Pago desde Transbank', ha finalizado correctamente."),
    
    //PROCESO DOWNLOAD CPAGO VISA-NAC
    EXITO_PROCESS_DOWNLOAD_CPAGO_TITLE_VISA_NAC("Finalizada Descarga de archivo de compensacion de pago visa nacional desde SFTP"),
    EXITO_PROCESS_DOWNLOAD_CPAGO_BODY_VISA_NAC("Estimados(as): \n\n El proceso 'Descarga de archivo Compensacion de Pago visa nacional desde SFTP', ha finalizado correctamente."),
   
    // PROCESO DOWNLOAD OUTGOING VISA 
    EXITO_DOWNLOAD_OUTGOING_VISA_TITLE("Finalizada descarga Outgoing Visa Internacional"),
    EXITO_DOWNLOAD_OUTGOING_VISA_CAUSE("Estimados(as): \n\n El proceso 'Descarga de Outgoing desde VISA INTERNACIONAL', ha finalizado correctamente."),
    
    // PROCESO DOWNLOAD OUTGOING VISA NACIONAL
    EXITO_DOWNLOAD_OUTGOING_VISA_NAC_TITLE("Finalizada descarga Outgoing Visa Nacional"),
    EXITO_DOWNLOAD_OUTGOING_VISA_NAC_CAUSE("Estimados(as): \n\n El proceso 'Descarga de Outgoing desde VISA NACIONAL', ha finalizado correctamente."),
	
	// PROCESO OUTGOING VISA EXITOSO
	EXITO_PROCESS_OUTGOING_VISA_TITLE("Finalizado Procesamiento Outgoing Visa"),
	EXITO_PROCESS_OUTGOING_VISA_CAUSE("Estimados(as): \n\n El proceso 'Procesar Outgoing de VISA', ha finalizado correctamente."),

	//PROCESO UPLOAD INCOMING
	EXITO_PROCESS_UPLOAD_TITLE("Finaliza la subida del Incoming  a Transbank"),
	EXITO_PROCESS_UPLOAD_INC("Estimados(as): \n\n El proceso 'Subida Incoming a Transbank', ha finalizado correctamente."), 

	//PROCESO UPLOAD ASIENTOS CONTABLES
	EXITO_PROCESS_UPLOAD_TITLE_CNTBL("Finalizada subida de Asientos Contables a Transbank"),
	EXITO_PROCESS_UPLOAD_CNTBL("Estimados(as): \n\n El proceso 'Subida de Asientos Contables a Transbank', ha finalizado correctamente."), 
	
	//PROCESO UPLOAD ASIENTOS CONTABLES AVABCES
	EXITO_PROCESS_UPLOAD_TITLE_CNTBL_AVA("Finalizada subida de Asientos Contables de avances con transferencia"),
	EXITO_PROCESS_UPLOAD_CNTBL_AVA("Estimados(as): \n\n El proceso 'Subida de Asientos Contables de avances con transferencia', ha finalizado correctamente."), 
	
	// PROCESO UPLOAD OUTGOING VISA IC
	EXITO_PROCESS_UPLOAD_OUT_VISA_IC_TITLE("Finalizada subida de outgoing de Visa Internacional a IC"),
    EXITO_PROCESS_UPLOAD_OUT_VISA_BODY("Estimados(as): \n\n El proceso 'Subida de Outgoing de Visa Internacional a IC', ha finalizado correctamente."),   
    
	// PROCESO UPLOAD OUTGOING VISA NACIONAL IC
	EXITO_PROCESS_UPLOAD_OUT_VISA_NAC_IC_TITLE("Finalizada subida de outgoing de Visa nacional a IC"),
    EXITO_PROCESS_UPLOAD_OUT_VISA_NAC_BODY("Estimados(as): \n\n El proceso 'Subida de Outgoing de Visa nacional a IC', ha finalizado correctamente."), 
	
	// PROCESO DE UPLOAD OUTGOING IC
	EXITO_PROCESS_UPLOAD_IC_TITLE("Finalizada subida de outgoing de transbank a IC"),
	EXITO_PROCESS_UPLOAD_IC_BODY("Estimados(as): \n\n El proceso 'Subida de outgoing de transbank a IC', ha finalizado correctamente."), 
	
	//PROCESO DE RECHAZO
	EXITO_PROCESS_RCH_TITLE("Finalizado Procesamiento de Rechazos"), 
	EXITO_PROCESS_RCH("Estimados(as): \n\n El proceso 'Procesamiento de archivo de Rechazos', ha finalizado correctamente."),	
	
	//PROCESO DE GENERAR CARGO ABONO
	EXITO_PROCESS_CAR_ABO_TITLE("Finalizado el proceso de Generacion de Cargos y Abonos"), 
	EXITO_PROCESS_CAR_ABO("Estimados(as): \n\n El proceso 'Generacion de archivo de Cargos y Abonos', ha finalizado correctamente."),
	
	//PROCESO DE UPLOAD CARGO Y ABONO
	EXITO_PROCESS_UPLOAD_TITLE_CAR_ABO("Finalizada la subida del Archivo de Cargos y Abonos a FTP SOL60"), 
	EXITO_PROCESS_UPLOAD_CAR_ABO("Estimados(as): \n\n El proceso 'Subida de archivo de Cargos y Abonos a FTP SOL60', ha finalizado correctamente."),
	
	// PROCESO DE DESCARGA ONUS
	EXITO_PROCESS_DOWNLOAD_ONUS_IC_TITLE("Finalizada la Descarga del archivo ONUS desde IC"), 
	EXITO_PROCESS_DOWNLOAD_ONUS_IC_BODY("Estimados(as): \n\n El proceso 'Descarga de archivo ONUS desde FTP de IC', ha finalizado correctamente."),
	
	// PROCESAR ARCHIVO ONUS
	EXITO_PROCESS_ONUS_TITLE("Finalizado el procesamiento del archivo ONUS"),
	EXITO_PROCESS_ONUS_BODY("Estimados(as): \n\n El proceso 'Procesamiento de archivo ONUS', ha finalizo correctamente."),
	
	// UPLOAD ONUS A TRANSBANK
	EXITO_PROCESS_UPLOAD_TITLE_ONUS("Finalizada subida de archivo Incoming ONUS a Transbank"),
	EXITO_PROCESS_UPLOAD_ONUS("Estimados(as): \n\n El proceso 'Subida de Incoming ONUS a  Transbank', ha finalizado correctamente."),
	
	// PROCESO DESCARGA LOG AUTORIZACION
	EXITO_PROCESS_DOWNLOAD_TITLE_LOG_AUTORIZACION("Finalizada la descarga del Log trx autorizadas de IC"), 
	EXITO_PROCESS_DOWNLOAD_LOG_AUTORIZACION("Estimados(as): \n\n El proceso 'Descarga archivo de Log de transacciones autorizadas desde IC', ha finalizado correctamente."),
	
	// PROCESAMIENTO DE LOG AUTORIZACION
	EXITO_PROCESS_TITLE_LOG_AUTORIZACION("Finaliza proceso de lectura del archivo de Trx autorizadas"), 
	EXITO_PROCESS_BODY_LOG_AUTORIZACION("Estimados(as): \n\n El proceso 'Procesamiento de archivo de Log de Transacciones autorizadas', ha finalizado correctamente."), 
	
	// PROCESAMIENTO GENERAR INCOMING ON US
	EXITO_PROCESS_INC_ONUS_TITLE("Finaliza el proceso de generaracion de archivo Incoming ONUS"), 
	EXITO_PROCESS_INC_ONUS("Estimados(as): \n\n El proceso 'Generacion de archivo Incoming ONUS', ha finalizado correctamente."),
	
	// GENERACION DE INCOMING A VISA
	EXITO_GENERATE_INC_VISA_TITLE("Finaliza el proceso de generacion de archivo Incoming VISA Nacional"), 
	EXITO_GENERATE_INC_VISA_BODY("Estimados(as): \n\n El proceso 'Generacion de archivo Incoming VISA Nacional', ha finalizado correctamente."),
	
	// GENERACION DE INCOMING A VISA Internacional
	EXITO_GENERATE_INC_VISA_INT_TITLE("Finaliza el proceso de generacion de archivo Incoming VISA Internacional"), 
	EXITO_GENERATE_INC_VISA_INT_BODY("Estimados(as): \n\n El proceso 'Generacion de archivo Incoming VISA Internacional', ha finalizado correctamente."),
	
	// UPLOAD INCOMING A VISA
	EXITO_UPLOAD_INC_VISA_TITLE("Finalizada la subida del archivo Incoming-VISA Nacional a SFTP de VISA Nacional"), 
	EXITO_UPLOADTE_INC_VISA_BODY("Estimados(as): \n\n El proceso 'Subida de archivo Incoming-VISA Nacional al servidor de VISA Nacional, ha finalizado correctamente."),
	
	// UPLOAD INCOMING A VISA INT
	EXITO_UPLOAD_INC_VISA_INT_TITLE("Finalizada la subida del archivo Incoming-VISA Internacional a SFTP de VISA Internacional"), 
	EXITO_UPLOADTE_INC_VISA_INT_BODY("Estimados(as): \n\n El proceso 'Subida de archivo Incoming-VISA Internacional al servidor de VISA Internacional, ha finalizado correctamente."),
	
	
	//PROCESO DOWNLOAD BICE
	EXITO_PROCESS_DOWNLOAD_TITLE_AVANCES("Finalizada Descarga Avances"),
	EXITO_PROCESS_DOWNLOAD_AVANCES("Estimados(as): \n\n El proceso 'Descarga de Avances', ha Finalizado Correctamente"), 
	
	//PROCESO DOWNLOAD AVANCES
	EXITO_PROCESS_DOWNLOAD_TITLE_BICE("Finalizada Descarga avances BICE"),
	EXITO_PROCESS_DOWNLOAD_BICE("Estimados(as): \n\n El proceso 'Descarga avances desde BICE', ha Finalizado Correctamente"), 
	
	// PROCESAR ARCHIVO BICE
	EXITO_PROCESS_BICE_TITLE("Finalizado el procesamiento del archivo avences Bice"),
	EXITO_PROCESS_BICE_BODY("Estimados(as): \n\n El proceso 'Procesamiento de archivo avances Bice', ha finalizo correctamente."),
	
	// PROCESAR ARCHIVO AVANCES
	EXITO_PROCESS_AVANCES_TITLE("Finalizado el procesamiento del archivo avences Bice"),
	EXITO_PROCESS_AVANCES_BODY("Estimados(as): \n\n El proceso 'Procesamiento de archivo avances Bice', ha finalizo correctamente."),

	
	;

	/**
	 * Valor ENUM.
	 */
	private final String text;

	/**
	 * Constructor.
	 * 
	 * @param text
	 */
	private MsgExitoMail(final String text) {
		this.text = text;
	}

	/**
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return text;
	}
}
