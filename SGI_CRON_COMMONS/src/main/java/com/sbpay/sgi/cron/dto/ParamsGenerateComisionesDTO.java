package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 27/11/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * Clase encargada de mapear los parametros de procesar comisiones.
 * 
 * </p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class ParamsGenerateComisionesDTO implements Serializable {

	/**
	 * Serial version.
	 */
	private static final long serialVersionUID = 4081693614385403716L;

	private String formatFileTbkComision;
	private String formatExtNameTbkComision;
	private String pathTbkComision;
	private String pathTbkComErr;
	private String pathTbkComBkp;

	public String getFormatFileTbkComision() {
		return formatFileTbkComision;
	}

	public void setFormatFileTbkComision(final String formatFileTbkComision) {
		this.formatFileTbkComision = formatFileTbkComision;
	}

	public String getFormatExtNameTbkComision() {
		return formatExtNameTbkComision;
	}

	public void setFormatExtNameTbkComision(
			final String formatExtNameTbkComision) {
		this.formatExtNameTbkComision = formatExtNameTbkComision;
	}

	public String getPathTbkComision() {
		return pathTbkComision;
	}

	public void setPathTbkComision(final String pathTbkComision) {
		this.pathTbkComision = pathTbkComision;
	}

	public String getPathTbkComErr() {
		return pathTbkComErr;
	}

	public void setPathTbkComErr(final String pathTbkComErr) {
		this.pathTbkComErr = pathTbkComErr;
	}

	public String getPathTbkComBkp() {
		return pathTbkComBkp;
	}

	public void setPathTbkComBkp(final String pathTbkComBkp) {
		this.pathTbkComBkp = pathTbkComBkp;
	}

	@Override
	public String toString() {
		return "ParamsGenerateComisionesDTO [formatFileTbkComision="
				+ formatFileTbkComision + ", formatExtNameTbkComision="
				+ formatExtNameTbkComision + ", pathTbkComision="
				+ pathTbkComision + ", pathTbkComErr=" + pathTbkComErr
				+ ", pathTbkComBkp=" + pathTbkComBkp + "]";
	}

}
