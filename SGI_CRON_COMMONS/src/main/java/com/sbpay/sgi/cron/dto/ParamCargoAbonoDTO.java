package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

public class ParamCargoAbonoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

  public ParamCargoAbonoDTO() {}

	private String formatFileNameCarAbo;
	private String formatExtNameCarAbo;
	private String pathCarAbo;

	public String getFormatFileNameCarAbo() {
		return formatFileNameCarAbo;
	}

	public void setFormatFileNameCarAbo(String formatFileNameCarAbo) {
		this.formatFileNameCarAbo = formatFileNameCarAbo;
	}

	public String getFormatExtNameCarAbo() {
		return formatExtNameCarAbo;
	}

	public void setFormatExtNameCarAbo(String formatExtNameCarAbo) {
		this.formatExtNameCarAbo = formatExtNameCarAbo;
	}
	
	public String getPathCarAbo() {
		return pathCarAbo;
	}

	public void setPathCarAbo(String pathCarAbo) {
		this.pathCarAbo = pathCarAbo;
	}

	@Override
	public String toString() {
    return "ParamCargoAbonoDTO [formatFileNameCarAbo=" + formatFileNameCarAbo
        + ", formatExtNameCarAbo=" + formatExtNameCarAbo + ", pathCarAbo=" + pathCarAbo + "]";
	}

	
}
