package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 17/04/2019, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * Clase que guardara una transaccion completa contemplando para esto
 * el todos los registros involucrados.
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class TransaccionOutVisaNac implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = -8665924641652098866L;
    private int numeroTransaccion;
    private String rutsbpay;
    private RegistroOutVisaNacDTO registro00;
    private RegistroOutVisaNacDTO registro01;
    private RegistroOutVisaNacDTO registro05;
    private RegistroOutVisaNacDTO registro07;
    private RegistroOutVisaNacDTO registro02;
    
    public String getRutsbpay() {
        return rutsbpay;
    }
    
    public void setRutsbpay( String rutsbpay ) {
        this.rutsbpay = rutsbpay;
    }
    
    public int getNumeroTransaccion() {
        return numeroTransaccion;
    }
    
    public void setNumeroTransaccion( int numeroTransaccion ) {
        this.numeroTransaccion = numeroTransaccion;
    }
    
    public RegistroOutVisaNacDTO getRegistro00() {
        return registro00;
    }
    
    public void setRegistro00( RegistroOutVisaNacDTO registro00 ) {
        this.registro00 = registro00;
    }
    
    public RegistroOutVisaNacDTO getRegistro01() {
        return registro01;
    }
    
    public void setRegistro01( RegistroOutVisaNacDTO registro01 ) {
        this.registro01 = registro01;
    }
    
    public RegistroOutVisaNacDTO getRegistro05() {
        return registro05;
    }
    
    public void setRegistro05( RegistroOutVisaNacDTO registro05 ) {
        this.registro05 = registro05;
    }
    
    public RegistroOutVisaNacDTO getRegistro07() {
        return registro07;
    }
    
    public void setRegistro07( RegistroOutVisaNacDTO registro07 ) {
        this.registro07 = registro07;
    }
    
	public RegistroOutVisaNacDTO getRegistro02() {
		return registro02;
	}

	public void setRegistro02(RegistroOutVisaNacDTO registro02) {
		this.registro02 = registro02;
	}

	@Override
	public String toString() {
		return "TransaccionOutVisaNac [numeroTransaccion=" + numeroTransaccion
				+ ", rutsbpay=" + rutsbpay + ", registro00=" + registro00
				+ ", registro01=" + registro01 + ", registro05=" + registro05
				+ ", registro07=" + registro07 + ", registro02=" + registro02
				+ "]";
	}   
    
}
