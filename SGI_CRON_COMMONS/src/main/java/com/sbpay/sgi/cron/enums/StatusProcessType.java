package com.sbpay.sgi.cron.enums;

public enum StatusProcessType {
    PROCESS_SUCESSFUL("1"),
    PROCESS_ERROR("-1"), 
    PROCESS_PENDING("0"),
    PROCESS_REUPLOAD("3");
    
    /**
     * Valor ENUM.
     */
    private String value;
    
    private StatusProcessType( String value ) {
        this.setValue( value );
    }
    
    public String getValue() {
        return value;
    }
    
    public void setValue( String value ) {
        this.value = value;
    }
    
    
}
