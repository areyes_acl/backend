package com.sbpay.sgi.cron.dto;

public class Registro00OutVisaCobroCargoDTO extends RegistroOutVisaCobroCargoDTO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String mit;
	private String codigoFuncion;
	private String destinationBin;
	private String sourceBin;
	private String reasonCode;
	private String countryCode;
	private String eventDate;
	private String accountNumber;
	private String accountNumberExtension;
	private String destinationAmount;
	private String destinationCurrencyCode;
	private String sourceAmount;
	private String sourceCurrencyCode;
	private String messageText;
	private String settlementFlag;
	private String transactionIdentifier;
	private String reserved;
	private String centralProcessingDate;
	private String reimbursementAttribute;
	private int operador;
	private String datosAdicionales;
	
	
	public String getMit() {
		return mit;
	}
	public void setMit(String mit) {
		this.mit = mit;
	}
	public String getCodigoFuncion() {
		return codigoFuncion;
	}
	public void setCodigoFuncion(String codigoFuncion) {
		this.codigoFuncion = codigoFuncion;
	}
	public String getDestinationBin() {
		return destinationBin;
	}
	public void setDestinationBin(String destinationBin) {
		this.destinationBin = destinationBin;
	}
	public String getSourceBin() {
		return sourceBin;
	}
	public void setSourceBin(String sourceBin) {
		this.sourceBin = sourceBin;
	}
	public String getReasonCode() {
		return reasonCode;
	}
	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getEventDate() {
		return eventDate;
	}
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getAccountNumberExtension() {
		return accountNumberExtension;
	}
	public void setAccountNumberExtension(String accountNumberExtension) {
		this.accountNumberExtension = accountNumberExtension;
	}
	public String getDestinationAmount() {
		return destinationAmount;
	}
	public void setDestinationAmount(String destinationAmount) {
		this.destinationAmount = destinationAmount;
	}
	public String getDestinationCurrencyCode() {
		return destinationCurrencyCode;
	}
	public void setDestinationCurrencyCode(String destinationCurrencyCode) {
		this.destinationCurrencyCode = destinationCurrencyCode;
	}
	public String getSourceAmount() {
		return sourceAmount;
	}
	public void setSourceAmount(String sourceAmount) {
		this.sourceAmount = sourceAmount;
	}
	public String getSourceCurrencyCode() {
		return sourceCurrencyCode;
	}
	public void setSourceCurrencyCode(String sourceCurrencyCode) {
		this.sourceCurrencyCode = sourceCurrencyCode;
	}
	public String getMessageText() {
		return messageText;
	}
	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}
	public String getSettlementFlag() {
		return settlementFlag;
	}
	public void setSettlementFlag(String settlementFlag) {
		this.settlementFlag = settlementFlag;
	}
	public String getTransactionIdentifier() {
		return transactionIdentifier;
	}
	public void setTransactionIdentifier(String transactionIdentifier) {
		this.transactionIdentifier = transactionIdentifier;
	}
	public String getReserved() {
		return reserved;
	}
	public void setReserved(String reserved) {
		this.reserved = reserved;
	}
	public String getCentralProcessingDate() {
		return centralProcessingDate;
	}
	public void setCentralProcessingDate(String centralProcessingDate) {
		this.centralProcessingDate = centralProcessingDate;
	}
	public String getReimbursementAttribute() {
		return reimbursementAttribute;
	}
	public void setReimbursementAttribute(String reimbursementAttribute) {
		this.reimbursementAttribute = reimbursementAttribute;
	}
	public int getOperador() {
		return operador;
	}
	public void setOperador(int operador) {
		this.operador = operador;
	}
	public String getDatosAdicionales() {
		return datosAdicionales;
	}
	public void setDatosAdicionales(String datosAdicionales) {
		this.datosAdicionales = datosAdicionales;
	}
	@Override
	public String toString() {
		return "Registro00OutVisaCobroCargoDTO [mit=" + mit
				+ ", codigoFuncion=" + codigoFuncion + ", destinationBin="
				+ destinationBin + ", sourceBin=" + sourceBin + ", reasonCode="
				+ reasonCode + ", countryCode=" + countryCode + ", eventDate="
				+ eventDate + ", accountNumber=" + accountNumber
				+ ", accountNumberExtension=" + accountNumberExtension
				+ ", destinationAmount=" + destinationAmount
				+ ", destinationCurrencyCode=" + destinationCurrencyCode
				+ ", sourceAmount=" + sourceAmount + ", sourceCurrencyCode="
				+ sourceCurrencyCode + ", messageText=" + messageText
				+ ", settlementFlag=" + settlementFlag
				+ ", transactionIdentifier=" + transactionIdentifier
				+ ", reserved=" + reserved + ", centralProcessingDate="
				+ centralProcessingDate + ", reimbursementAttribute="
				+ reimbursementAttribute + ", operador=" + operador
				+ ", datosAdicionales=" + datosAdicionales + "]";
	}
	
	

}
