package com.sbpay.sgi.cron.ds;

import java.sql.Connection;
import java.sql.SQLException;

import oracle.jdbc.pool.OracleDataSource;

public class TestConnection {

	public static Connection getConnection(){
		// Configuración del DataSource
        String url = "jdbc:oracle:thin:@127.0.0.1:1521:xe";
        String user = "SIG";
        String password = "SIG";
        OracleDataSource oracleDS = null;
        try {
            oracleDS = new OracleDataSource();
            oracleDS.setURL( url );
            oracleDS.setUser( user );
            oracleDS.setPassword( password );
           return oracleDS.getConnection();
        }
        catch ( SQLException e ) {
            System.out.println( e );
        }
		return null;
	}
}
