package com.sbpay.sgi.cron.utils.file;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dto.TmpExportItzAvanceDTO;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.MsgGenerationItzAvance;

/**
 * 
 * @author dmedina
 *
 */
public class AvancesExport implements AvanceFileUtil {

	private static final Logger LOGGER = Logger.getLogger(AvancesExport.class);
	  
	/**
	 * 
	 */
	@Override
	public void exportAvanceFile(String ruta, String avanceFilename, List<TmpExportItzAvanceDTO> listaRegistros)
			throws AppException {
		
	    String rutaFile = ruta.concat(avanceFilename);
	    File incomingFile = new File(rutaFile);
	    incomingFile.setExecutable(true);
	    incomingFile.setReadable(true);
	    incomingFile.setWritable(true);

	    if (incomingFile.exists()) {
	      LOGGER.error(MsgGenerationItzAvance.ERROR_ITZ_ALREADY_EXIST.toString());
	      throw new AppException(MsgGenerationItzAvance.ERROR_ITZ_ALREADY_EXIST.toString().replace("?", incomingFile.getName()));
	    }

	    try {
	    	generaArchivo(incomingFile, listaRegistros);
	    } catch (IOException e) {
	    	LOGGER.error(MsgGenerationItzAvance.ERROR_CREATE_ITZ_FILE.toString());
	    	throw new AppException(MsgGenerationItzAvance.ERROR_CREATE_ITZ_FILE.toString(),
	          e);
	    }
	}
    
	/**
	   * 
	   * 
	   * <p>
	   * Registro de versiones:
	   * <ul>
	   * <li>1.0 XX/YY/2018, (ACL SPA) - versión inicial
	   * </ul>
	   * <p>
	   * 
	   * @param incomingFile
	   * @param listaTrasacciones
	   * @throws IOException
	   * @since 1.X
	   */
	  private void generaArchivo(File avanceFile,
			  List<TmpExportItzAvanceDTO> listaRegistros) throws IOException {
		int nroLinea = 0;

	    StringBuilder contenido = new StringBuilder();


	    for (int i = 0; i < listaRegistros.size(); i++) {
	    	nroLinea ++;
	    	contenido.append(dtoToString(nroLinea, listaRegistros.get(i)));
	      // SI ES EL ULTIMO REGISTRO NO SE HACE SALTO DE LINEA
	      if ((i + 1) == listaRegistros.size()) {
	        contenido.replace(contenido.length() - 1, contenido.length(),
	            ConstantesUtil.EMPTY.toString());
	      }
	    }

	    // ESCRIBE EN ARCHIVO
	    writeFile(contenido.toString(), avanceFile);
	  }

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/XX/2018, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo encargado de escribir la linea sacando la info desde el dto
	 * 
	 * @param nroLinea
	 * @param TmpExportItzAvanceDTO
	 * @since 1.X
	 */
	private String dtoToString(Integer nroLinea, TmpExportItzAvanceDTO tmpExportDTO) {

		StringBuilder str = new StringBuilder();
		String monto = "";
		if (tmpExportDTO.getMonto() != null
				&& tmpExportDTO.getMonto().length() > 2) {
			monto = tmpExportDTO.getMonto().substring(0,
					tmpExportDTO.getMonto().length() - 2);
		}
		
		//Integer tamFecha =10;
		Integer tamCodAu =6;
		Integer tamNumTrx =10;
		Integer tamNumTarjeta =16;
		Integer tamMonto =13;
		Integer tamEstado =3;
		Integer tamCanal =10;
		Integer tamMontoSeguro =10;
		
		/*System.out.println(tmpExportDTO.getFechaTransferencia()+" : "+tmpExportDTO.getCodAutorizacion()+" : "+tmpExportDTO.getNumeroTrx()+" : "+tmpExportDTO.getNumeroTarjeta()+" : "+tmpExportDTO.getMonto()
				+" : "+tmpExportDTO.getEstado()+" : "+tmpExportDTO.getNumeroCanal());*/

		str.append(tmpExportDTO.getFechaTransferencia());
		str.append(validaTamano(tmpExportDTO.getCodAutorizacion(),tamCodAu));
		str.append(validaTamano(tmpExportDTO.getNumeroTrx(),tamNumTrx));
		str.append(validaTamano(tmpExportDTO.getNumeroTarjeta(),tamNumTarjeta));
		
		//para monto de avances 
		if(tmpExportDTO.getMonto().length()< tamMonto){
			str.append(paddingZeroToNumber(Integer.parseInt(tmpExportDTO.getMonto()),tamMonto));
		}else if(tmpExportDTO.getMonto().length() > tamMonto){
			str.append(tmpExportDTO.getMonto().substring(0, tamMonto));
		}else{
			str.append(tmpExportDTO.getMonto());
		}
		
		str.append(validaTamano(tmpExportDTO.getEstado(),tamEstado));
		str.append(validaTamano(tmpExportDTO.getNumeroCanal(),tamCanal));
		
		//para monto de seguro
		if(tmpExportDTO.getMontoSeguro().length()< tamMontoSeguro){
			str.append(paddingZeroToNumber(Integer.parseInt(tmpExportDTO.getMontoSeguro()),tamMontoSeguro));
		}else if(tmpExportDTO.getMontoSeguro().length() > tamMontoSeguro){
			str.append(tmpExportDTO.getMontoSeguro().substring(0, tamMontoSeguro));
		}else{
			str.append(tmpExportDTO.getMontoSeguro());
		}
		
		str.append(ConstantesUtil.SKIP_LINE.toString());
		
		return str.toString();
	}
	
	public static String validaTamano(String valor, Integer canValor){
		StringBuilder str = new StringBuilder();
		
		if(valor.length() < canValor){
			Integer resta = canValor - valor.length() ;
			str.append(valor).append(getWhitesSpaceString(resta));
			
		}else if(valor.length() > canValor){
			str.append(valor.substring(0, canValor));
		}else{
			str.append(valor);
		}
		return str.toString();
	}
	
	public static String getWhitesSpaceString(Integer numOfspaces) {
		StringBuilder str = new StringBuilder();
		for (int i = 0; i < numOfspaces; i++) {
			str.append(ConstantesUtil.WHITE.toString());
		}

		return str.toString();
	}
	
	public static String paddingZeroToNumber(Integer number, Integer totalLength) {

		return String.format("%0" + totalLength + "d", number);
	}

	 /**
	   * 
	   * 
	   * <p>
	   * Registro de versiones:
	   * <ul>
	   * <li>1.0 11/12/2015, (ACL-sbpay) - versión inicial
	   * </ul>
	   * <p>
	   * 
	   * Metodo que crea el archivo en disco y escribe todo el contenido de este.
	   * 
	   * @param footer
	   * @param contenido
	   * @param header
	   * @param incomingFile
	   * @param incomingFilename
	   * @throws IOException
	   * 
	   * @since 1.X
	   */
	  private void writeFile(String contenido,
	      File incomingFile) throws IOException {
	    BufferedWriter bw = null;
	    bw = new BufferedWriter(new FileWriter(incomingFile));
	    if (contenido != null && contenido.length() > 0) {
	      bw.write(contenido);
	    }
	    bw.close();
	  }

}
