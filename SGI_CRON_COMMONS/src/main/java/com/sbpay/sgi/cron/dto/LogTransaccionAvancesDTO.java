package com.sbpay.sgi.cron.dto;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 xx/10/2018, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class LogTransaccionAvancesDTO implements Serializable {
    
    /**
   * 
   */
    private static final long serialVersionUID = -8721266764009601962L;
    private String filename;
    private List<TransaccionAvancesDTO> trxList;
    private String detalleLectura;
    
    public String getFilename() {
        return filename;
    }
    
    public void setFilename( String filename ) {
        this.filename = filename;
    }
    
    public List<TransaccionAvancesDTO> getTrxList() {
        return trxList;
    }
    
    public void setTrxList( List<TransaccionAvancesDTO> trxList ) {
        this.trxList = trxList;
    }
    
    public String getDetalleLectura() {
        return detalleLectura;
    }
    
    public void setDetalleLectura( String detalleLectura ) {
        this.detalleLectura = detalleLectura;
    }

	@Override
	public String toString() {
		return "LogTransaccionAvancesDTO [filename=" + filename + ", trxList="
				+ trxList + ", detalleLectura=" + detalleLectura + "]";
	}


    

    
}
