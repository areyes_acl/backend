package com.sbpay.sgi.cron.dao;

import java.sql.SQLException;

/**
 * <p>
 * Interfaz Transacciones SQL Commons.
 * <p>
 * <br/>
 * @author Dante Medina (ACL) en nombre de sbpay
 * @version 1.6
 * @since 2015-11-09
 */
public interface Dao {
    /**
     * Commit TRX.
     * @throws SQLException Error SQL.
     */
    void endTx() throws SQLException;

    /**
     * rollback TRX.
     * @throws SQLException Error SQL.
     */
    void rollBack() throws SQLException;

    /**
     * close connection.
     * @throws SQLException Error SQL.
     */
    void close() throws SQLException;
}