package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 xx/10/2018, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class TransaccionBiceDTO implements Serializable {

	/**
   * 
   */
	private static final long serialVersionUID = 8887889677674146517L;
	private long sid;
	private String tipoRegistro;
	private String numOperProg;
	private String codServicio;
	private String rutOrdenante;
	private String numCtaOrdenante;
	private String idOperCliente;
	private String codEstado;
	private String glosaEstado;
	private String fechaInstruccion;
	private String fechaContable;
	private String codBancoBeneficiario;
	private String tipoCtaBeneficiario;
	private String ctaBeneficiario;
	private String rutBeneficiario;
	private String nomBeneficiario;
	private String monto;

	public long getSid() {
		return sid;
	}
	public void setSid(long sid) {
		this.sid = sid;
	}
	public String getTipoRegistro() {
		return tipoRegistro;
	}
	public void setTipoRegistro(String tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}
	public String getNumOperProg() {
		return numOperProg;
	}
	public void setNumOperProg(String numOperProg) {
		this.numOperProg = numOperProg;
	}
	public String getCodServicio() {
		return codServicio;
	}
	public void setCodServicio(String codServicio) {
		this.codServicio = codServicio;
	}
	public String getRutOrdenante() {
		return rutOrdenante;
	}
	public void setRutOrdenante(String rutOrdenante) {
		this.rutOrdenante = rutOrdenante;
	}
	public String getNumCtaOrdenante() {
		return numCtaOrdenante;
	}
	public void setNumCtaOrdenante(String numCtaOrdenante) {
		this.numCtaOrdenante = numCtaOrdenante;
	}
	public String getIdOperCliente() {
		return idOperCliente;
	}
	public void setIdOperCliente(String idOperCliente) {
		this.idOperCliente = idOperCliente;
	}
	public String getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}
	public String getGlosaEstado() {
		return glosaEstado;
	}
	public void setGlosaEstado(String glosaEstado) {
		this.glosaEstado = glosaEstado;
	}
	public String getFechaInstruccion() {
		return fechaInstruccion;
	}
	public void setFechaInstruccion(String fechaInstruccion) {
		this.fechaInstruccion = fechaInstruccion;
	}
	public String getFechaContable() {
		return fechaContable;
	}
	public void setFechaContable(String fechaContable) {
		this.fechaContable = fechaContable;
	}
	public String getCodBancoBeneficiario() {
		return codBancoBeneficiario;
	}
	public void setCodBancoBeneficiario(String codBancoBeneficiario) {
		this.codBancoBeneficiario = codBancoBeneficiario;
	}
	public String getTipoCtaBeneficiario() {
		return tipoCtaBeneficiario;
	}
	public void setTipoCtaBeneficiario(String tipoCtaBeneficiario) {
		this.tipoCtaBeneficiario = tipoCtaBeneficiario;
	}
	public String getCtaBeneficiario() {
		return ctaBeneficiario;
	}
	public void setCtaBeneficiario(String ctaBeneficiario) {
		this.ctaBeneficiario = ctaBeneficiario;
	}
	public String getRutBeneficiario() {
		return rutBeneficiario;
	}
	public void setRutBeneficiario(String rutBeneficiario) {
		this.rutBeneficiario = rutBeneficiario;
	}
	public String getNomBeneficiario() {
		return nomBeneficiario;
	}
	public void setNomBeneficiario(String nomBeneficiario) {
		this.nomBeneficiario = nomBeneficiario;
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public String toString() {
		return "TransaccionBiceDTO [sid=" + sid + ", tipoRegistro="
				+ tipoRegistro + ", numOperProg=" + numOperProg
				+ ", codServicio=" + codServicio + ", rutOrdenante="
				+ rutOrdenante + ", numCtaOrdenante=" + numCtaOrdenante
				+ ", idOperCliente=" + idOperCliente + ", codEstado="
				+ codEstado + ", glosaEstado=" + glosaEstado
				+ ", fechaInstruccion=" + fechaInstruccion + ", fechaContable="
				+ fechaContable + ", codBancoBeneficiario="
				+ codBancoBeneficiario + ", tipoCtaBeneficiario="
				+ tipoCtaBeneficiario + ", ctaBeneficiario=" + ctaBeneficiario
				+ ", rutBeneficiario=" + rutBeneficiario + ", nomBeneficiario="
				+ nomBeneficiario + ", monto=" + monto + "]";
	}

	
	



}
