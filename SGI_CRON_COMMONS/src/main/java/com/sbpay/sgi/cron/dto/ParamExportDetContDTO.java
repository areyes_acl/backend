package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

public class ParamExportDetContDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String pathSalida;
	
	private String codDESCUENTO_TBK;
	private String codPAGO_TBK;
	private String codDESCUENTO_VN;
	private String codPAGO_VN;
	private String codDESCUENTO_VI;
	private String codPAGO_VI;
	
	
	public String getCodDESCUENTO_TBK() {
		return codDESCUENTO_TBK;
	}
	public void setCodDESCUENTO_TBK(String codDESCUENTO_TBK) {
		this.codDESCUENTO_TBK = codDESCUENTO_TBK;
	}
	public String getCodPAGO_TBK() {
		return codPAGO_TBK;
	}
	public void setCodPAGO_TBK(String codPAGO_TBK) {
		this.codPAGO_TBK = codPAGO_TBK;
	}
	public String getCodDESCUENTO_VN() {
		return codDESCUENTO_VN;
	}
	public void setCodDESCUENTO_VN(String codDESCUENTO_VN) {
		this.codDESCUENTO_VN = codDESCUENTO_VN;
	}
	public String getCodPAGO_VN() {
		return codPAGO_VN;
	}
	public void setCodPAGO_VN(String codPAGO_VN) {
		this.codPAGO_VN = codPAGO_VN;
	}
	public String getCodDESCUENTO_VI() {
		return codDESCUENTO_VI;
	}
	public void setCodDESCUENTO_VI(String codDESCUENTO_VI) {
		this.codDESCUENTO_VI = codDESCUENTO_VI;
	}
	public String getCodPAGO_VI() {
		return codPAGO_VI;
	}
	public void setCodPAGO_VI(String codPAGO_VI) {
		this.codPAGO_VI = codPAGO_VI;
	}
	
	public String getPathSalida() {
		return pathSalida;
	}
	public void setPathSalida(String pathSalida) {
		this.pathSalida = pathSalida;
	}
	
	@Override
	public String toString() {
		return "ParamExportReCnblDTO [pathSalida=" + pathSalida + "]";
	}

}
