package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

public class ParamUploadCntbleDTO implements Serializable {
    
    private static final long serialVersionUID = -8953255366294568706L;
    private DataFTP dataFTP;
    private String sftpPathIn;
    private String sbpayServerPath;
    private String sbpayServerPathBkp;
    private String sbpayServerPathError;
    private ConfigFile formatContable = new ConfigFile();
    
    public DataFTP getDataFTP() {
        return dataFTP;
    }
    
    public void setDataFTP( DataFTP dataFTP ) {
        this.dataFTP = dataFTP;
    }
    
    public String getSftpPathIn() {
        return sftpPathIn;
    }
    
    public void setSftpPathIn( String sftpPathIn ) {
        this.sftpPathIn = sftpPathIn;
    }
    
    public String getsbpayServerPath() {
        return sbpayServerPath;
    }
    
    public void setsbpayServerPath( String sbpayServerPath ) {
        this.sbpayServerPath = sbpayServerPath;
    }
    
    public String getsbpayServerPathBkp() {
        return sbpayServerPathBkp;
    }
    
    public void setsbpayServerPathBkp( String sbpayServerPathBkp ) {
        this.sbpayServerPathBkp = sbpayServerPathBkp;
    }
    
    public String getsbpayServerPathError() {
        return sbpayServerPathError;
    }
    
    public void setsbpayServerPathError( String sbpayServerPathError ) {
        this.sbpayServerPathError = sbpayServerPathError;
    }
    
    public ConfigFile getFormatContable() {
        return formatContable;
    }
    
    public void setFormatContable( ConfigFile formatContable ) {
        this.formatContable = formatContable;
    }
    
    @Override
    public String toString() {
        return "ParamUploadCntbleDTO [dataFTP=" + dataFTP + ", sftpPathIn="
                + sftpPathIn + ", sbpayServerPath=" + sbpayServerPath
                + ", sbpayServerPathBkp=" + sbpayServerPathBkp
                + ", sbpayServerPathError=" + sbpayServerPathError
                + ", formatContable=" + formatContable + "]";
    }
    
}
