package com.sbpay.sgi.cron.commons;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 27/11/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * Clase de cargar configuracion del log4j.
 * 
 * </p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public final class AppConfig {
	/**
	 * Propiedades del aplicativo.
	 */
	private Properties props;

	/** The single instance. */
	private static AppConfig singleINSTANCE = null;

	/**
	 * Creates the instance.
	 */
	private static void createInstance() {
		synchronized (AppConfig.class) {
			if (singleINSTANCE == null) {
				singleINSTANCE = new AppConfig();
			}
		}
	}

	/**
	 * Patron singleton.
	 * 
	 * @return AppConfig retorna instancia del servicio.
	 */
	public static AppConfig getInstance() {
		if (singleINSTANCE == null) {
			createInstance();
		}
		return singleINSTANCE;
	}

	/**
	 * Metodo de restricion de sobreescritura de la clase.
	 */
	@Override
	protected Object clone() throws CloneNotSupportedException {
		super.clone();
		throw new CloneNotSupportedException();
	}

	/**
	 * Constructor Privado.
	 */
	private AppConfig() {
		super();
		props = new Properties();
	}

	/**
	 * Metodo para cargar las propiedades del sistema.
	 * 
	 * @param inStream
	 *            Stream con el archivo properties a cargar.
	 * @throws IOException
	 *             Excepcion de carga.
	 */
	public void loadProperties(final InputStream inStream) throws IOException {
		props.load(inStream);
	}

	/**
	 * Metodo para obtener un parametro del aplicativo.
	 * 
	 * @param key
	 *            Key del parametro a obtener.
	 * @return Valor del parametro.
	 */
	public String getParam(final String key) {
		return props.getProperty(key);
	}

	/**
	 * Metodo Retorna las Propiedades.
	 * 
	 * @return Properties
	 */
	public Properties getProperties() {
		return props;
	}
}
