package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/10/2018, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class ParamsProcessAvancesDTO implements Serializable{
	
	/**
     * 
     */
	private static final long serialVersionUID = 4081693614785403716L;
	private String formatFileNameAvances;
	private String pathAbcAvances;
	private String pathAbcAvancesError;
	private String pathAbcAvancesBkp;
	
	public String getFormatFileNameAvances() {
		return formatFileNameAvances;
	}
	public void setFormatFileNameAvances(String formatFileNameAvances) {
		this.formatFileNameAvances = formatFileNameAvances;
	}
	public String getPathAbcAvances() {
		return pathAbcAvances;
	}
	public void setPathAbcAvances(String pathAbcAvances) {
		this.pathAbcAvances = pathAbcAvances;
	}
	public String getPathAbcAvancesError() {
		return pathAbcAvancesError;
	}
	public void setPathAbcAvancesError(String pathAbcAvancesError) {
		this.pathAbcAvancesError = pathAbcAvancesError;
	}
	public String getPathAbcAvancesBkp() {
		return pathAbcAvancesBkp;
	}
	public void setPathAbcAvancesBkp(String pathAbcAvancesBkp) {
		this.pathAbcAvancesBkp = pathAbcAvancesBkp;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public String toString() {
		return "ParamsProcessAvancesDTO [formatFileNameAvances="
				+ formatFileNameAvances + ", pathAbcAvances=" + pathAbcAvances
				+ ", pathAbcAvancesError=" + pathAbcAvancesError
				+ ", pathAbcAvancesBkp=" + pathAbcAvancesBkp + "]";
	}

}
