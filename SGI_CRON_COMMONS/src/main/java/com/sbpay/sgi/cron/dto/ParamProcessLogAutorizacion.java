package com.sbpay.sgi.cron.dto;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class ParamProcessLogAutorizacion {

  private String starWith;
  private String endsWith;
  private String pathAbcLogTrx;
  private String pathBkp;
  private String pathError;
  
  
  
  public String getStarWith() {
    return starWith;
  }
  public void setStarWith(String starWith) {
    this.starWith = starWith;
  }
  public String getEndsWith() {
    return endsWith;
  }
  public void setEndsWith(String endsWith) {
    this.endsWith = endsWith;
  }
  public String getPathAbcLogTrx() {
    return pathAbcLogTrx;
  }
  public void setPathAbcLogTrx(String pathAbcLogTrx) {
    this.pathAbcLogTrx = pathAbcLogTrx;
  }
  public String getPathBkp() {
    return pathBkp;
  }
  public void setPathBkp(String pathBkp) {
    this.pathBkp = pathBkp;
  }
  public String getPathError() {
    return pathError;
  }
  public void setPathError(String pathError) {
    this.pathError = pathError;
  }
  @Override
  public String toString() {
    return "ParamProcessLogAutorizacion [starWith=" + starWith + ", endsWith=" + endsWith
        + ", pathAbcLogTrx=" + pathAbcLogTrx + ", pathBkp=" + pathBkp + ", pathError=" + pathError
        + "]";
  }
  
  

}
