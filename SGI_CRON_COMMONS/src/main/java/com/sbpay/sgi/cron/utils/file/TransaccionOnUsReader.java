package com.sbpay.sgi.cron.utils.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dto.ICCodeHomologationDTO;
import com.sbpay.sgi.cron.dto.LogTransaccionOnUsDTO;
import com.sbpay.sgi.cron.dto.TransaccionOnUsDTO;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.MsgErrorFile;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class TransaccionOnUsReader implements TransaccionOnUsFileUtil {
    
    /** VARIABLE PARA EL LOGER */
    private static final Logger LOGGER = Logger
            .getLogger( TransaccionOnUsReader.class );
    
    private List<ICCodeHomologationDTO> listaCodigosIc = null;
    private String detalleTrxLeidasConError = "";
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * GETTERS Y SETTERS
     * 
     * @return
     * @since 1.X
     */
    public String getDetalleTrxLeidasConError() {
        return detalleTrxLeidasConError;
    }
    
    public void setDetalleTrxLeidasConError( String detalleTrxLeidasConError ) {
        this.detalleTrxLeidasConError = detalleTrxLeidasConError;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param file
     * @param starWith
     * @param listaCodigosIc
     * @return
     * @throws AppException
     * @see com.sbpay.sgi.cron.utils.file.TransaccionOnUsFileUtil#readLogTrxOnUsrFile(java.io.File,
     *      java.lang.String, java.util.List)
     * @since 1.X
     */
    @Override
    public LogTransaccionOnUsDTO readLogTrxOnUsrFile( File file,
            String starWith, List<ICCodeHomologationDTO> listaCodigosIc )
            throws AppException {
        
        LogTransaccionOnUsDTO logTrxOnUsDTO = new LogTransaccionOnUsDTO();
        List<TransaccionOnUsDTO> trxAutList = null;
        BufferedReader buffReader = null;
        this.listaCodigosIc = listaCodigosIc;
        try {
            
            if ( file == null ) {
                throw new AppException(
                        MsgErrorFile.ERROR_READ_FILE_NULL.toString() );
            }
            
            String ruta = file.getAbsolutePath();
            buffReader = new BufferedReader( new FileReader( ruta ) );
            
            trxAutList = readFile( buffReader );
            
        }
        catch ( IOException ioe ) {
            throw new AppException(
                    "Ha ocurrido un error al leer el archivo : "
                            + ioe.getMessage(), ioe );
        }
        finally {
            try {
                if ( buffReader != null ) {
                    buffReader.close();
                }
            }
            catch ( IOException ioe1 ) {
                throw new AppException(
                        "Ha ocurrido un error al cerrar el archivo", ioe1 );
            }
        }
        
        logTrxOnUsDTO.setTrxList( trxAutList );
        logTrxOnUsDTO.setDetalleLectura( detalleTrxLeidasConError );
        
        
        return logTrxOnUsDTO;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Metodo encargado de ir linea a linea leyendo el archivo ONUS
     * 
     * @param buffReader
     * @return
     * @throws IOException
     * @throws AppException
     * @since 1.X
     */
    private List<TransaccionOnUsDTO> readFile( BufferedReader buffReader )
            throws IOException, AppException {
        String line = buffReader.readLine();
        List<TransaccionOnUsDTO> listaTransacciones = new ArrayList<TransaccionOnUsDTO>();
        TransaccionOnUsDTO tr = null;
        
        // RECORRE EL FILE LEYENDO LINEA POR LINEA
        while ( line != null ) {
            try {
                tr = parseToTransaccion( line );
                listaTransacciones.add( tr );
            }
            catch ( AppException e ) {
                LOGGER.error( e.getMessage(), e );
                detalleTrxLeidasConError = detalleTrxLeidasConError.concat(
                        "\n -" ).concat( e.getMessage() );
            }
            line = buffReader.readLine();
            
        }
        
        LOGGER.info( "-->Nº TRX ON US leìdas  = " + listaTransacciones.size() );
        return listaTransacciones;
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Metodo encargado de parsear la linea del archivo leido a un
     * objeto del tipo transaccionOnusDTO.
     * 
     * @param line
     * @return
     * @throws AppException
     * @since 1.X
     */
    private TransaccionOnUsDTO parseToTransaccion( String line )
            throws AppException {
        TransaccionOnUsDTO transaccion = null;
        String separador = ConstantesUtil.PUNTO_COMA.toString();
        String[] transaccionSeparada = line.split( separador );
        String sbpayCode = null;
        if ( transaccionSeparada != null && transaccionSeparada.length == 15 ) {
            
            // SE TRADUCE CODIGO DE TRANSACCION DESDE IC A CODIGO
            // TRANSBANK
            String tranbankCode = traducirCodigoDeIcACodigoTransbank( transaccionSeparada[0] );
            
            // SE TRADUCE CODIGO DE TRANSACCION DESDE CODIGO TBK A
            // CODIGO ABDCIN
            sbpayCode = CommonsUtils
                    .obtieneCodTrxOnussbpayPorCodTransbank( tranbankCode );
            
            if ( sbpayCode != null ) {
                transaccion = new TransaccionOnUsDTO();
                transaccion.setCodigoTransaccion( sbpayCode );
                transaccion.setNumeroTarjeta( transaccionSeparada[1] );
                transaccion.setFechaCompra( transaccionSeparada[2] );
                transaccion.setFechaAutorizacion( transaccionSeparada[3] );
                transaccion.setFechaPosteo( transaccionSeparada[4] );
                transaccion.setTipoVenta( transaccionSeparada[5] );
                transaccion.setNumCuotas( transaccionSeparada[6] );
                transaccion.setNumMicrofilm( transaccionSeparada[7] );
                transaccion.setNumComercio( transaccionSeparada[8] );
                transaccion.setMontoTransac( parseaMontos( transaccionSeparada[9] )); //  HAY QUE AGREGARLE DOS 00
                transaccion.setValorCuota( parseaMontos( transaccionSeparada[10] ));  //  HAY QUE AGREGALE LOS 00 AL VALOR DE LA CUOTA
                transaccion.setNombreComercio( transaccionSeparada[11] );
                transaccion.setCiudadComercio( transaccionSeparada[12] );
                transaccion.setRubroComercio( transaccionSeparada[13] );
                transaccion.setCodAutor( transaccionSeparada[14] );
            }
        }
        return transaccion;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    private String parseaMontos(String monto ){
        
        if (monto != null && !monto.equalsIgnoreCase( ConstantesUtil.EMPTY.toString() )){
          return  String.valueOf( Integer.parseInt(monto) * 100 );
        
        }else{
           return String.valueOf( 0  * 100 );
        }
        
       
    }
    
    
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param icCode
     * @return
     * @throws AppException
     * @since 1.X
     */
    private String traducirCodigoDeIcACodigoTransbank( String icCode )
            throws AppException {
        String codTrx = null;
        
        for ( ICCodeHomologationDTO instanceIc : listaCodigosIc ) {
            if ( instanceIc.isActivo() && instanceIc.getCodigoIC() != null
                    && instanceIc.getCodigoIC().equalsIgnoreCase( icCode ) ) {
                codTrx = instanceIc.getCodigoTransbank();
                break;
            }
        }
        
        if ( codTrx == null ) {
            throw new AppException(
                    "No se ha encontrado codigo de transaccion de IC : "
                            + icCode
                            + ", homologado a los codigos de transbank en la tabla TBL_PRTS_IC_CODE." );
        }
        
        return codTrx;
    }
    
    @Override
    public void exportIncomingOnUsFile( String ruta, String incomingFilename,
            String controlFilename, List<TransaccionOnUsDTO> listaTrasacciones ) {
        // TODO Auto-generated method stub
        
    }
    
}
