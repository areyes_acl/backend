package com.sbpay.sgi.cron.enums;

public enum MsgErrorCargoAbono {

    ERROR_INIT_DAO("Ha ocurrido un error al intentar crear la instancia del DAO de cargos y abonos"),
	ERROR_CALL_SP_CARGASALIDACARGOABONO("Ha ocurrido un error al llamar al SP: SP_CARGASALIDACARGOABONO"),
	ERROR_CALL_SP_CARGASALIDACARGOABONO_CLOSE_CONECTION("Ha ocurrido un error al cerrar la conexion"), 
	WARNING_TRANSACTIONS_NOT_FOUND("No existen cargo o abonos que procesar"),
	ERROR_INSERT_TBL_CRON_IC("Ha ocurrido un error al insertar en la tabla TBL_CRON_IC"), 
	ERROR_CALL_SP_CAR_ABO_TBK_CLOSE_CONECTION("Ha ocurrido un error al cerrar la conexion"), 
	ERROR_INSERT_TBL_CRON_IC_CLOSE_CONECTION("Ha ocurrido un error al cerrar la conexion");

	/**
	 * Valor ENUM.
	 */
	private final String text;

	/**
	 * Constructor.
	 * 
	 * @param text
	 */
	private MsgErrorCargoAbono(final String text) {
		this.text = text;
	}

	/**
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return text;
	}
}
