package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

public class ParamMailDTO implements Serializable {
    
    /**
     * Serial ID.
     */
    private static final long serialVersionUID = 1L;
    
    public ParamMailDTO() {
        super();
    }
    
    private String mailSmtp;
    private int mailPort;
    private String mailUser;
    private String mailPass;
    private String mailSecurity;
    private String mailAuth;
    private String mailDest;
    private String mailDestOk;
    private String environment;
    

	public String getMailSmtp() {
        return mailSmtp;
    }
    
    public void setMailSmtp( final String mailSmtp ) {
        this.mailSmtp = mailSmtp;
    }
    
    public int getMailPort() {
        return mailPort;
    }
    
    public void setMailPort( final int mailPort ) {
        this.mailPort = mailPort;
    }
    
    public String getMailUser() {
        return mailUser;
    }
    
    public void setMailUser( final String mailUser ) {
        this.mailUser = mailUser;
    }
    
    public String getMailPass() {
        return mailPass;
    }
    
    public void setMailPass( final String mailPass ) {
        this.mailPass = mailPass;
    }
    
    public String getMailSecurity() {
        return mailSecurity;
    }
    
    public void setMailSecurity( final String mailSecurity ) {
        this.mailSecurity = mailSecurity;
    }
    
    public String getMailAuth() {
        return mailAuth;
    }
    
    public void setMailAuth( final String mailAuth ) {
        this.mailAuth = mailAuth;
    }
    
    public String getMailDest() {
        return mailDest;
    }
    
    public void setMailDest( final String mailDest ) {
        this.mailDest = mailDest;
    }
    
    public String getMailDestOk() {
		return mailDestOk;
	}

	public void setMailDestOk(String mailDestOk) {
		this.mailDestOk = mailDestOk;
	}

	    
    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment( String environment ) {
        this.environment = environment;
    }

    @Override
    public String toString() {
        return "ParamMailDTO [mailSmtp=" + mailSmtp + ", mailPort=" + mailPort
                + ", mailUser=" + mailUser + ", mailPass=" + mailPass
                + ", mailSecurity=" + mailSecurity + ", mailAuth=" + mailAuth
				+ ", mailDest=" + mailDest + ", mailDestOk=" + mailDestOk + "]";
    }
    
}
