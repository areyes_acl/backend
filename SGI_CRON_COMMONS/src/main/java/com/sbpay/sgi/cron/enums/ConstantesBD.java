package com.sbpay.sgi.cron.enums;

public enum ConstantesBD {
	ESQUEMA("SIG"), 
	SEQ_CRON_LOG("SEQ_TBL_CRON_LOG"),
	TABLE_LOG_INCOMING("TBL_LOG_INC"),
	TABLE_LOG_OUTGOING("TBL_LOG_OUT"),
	TABLE_LOG_RECHAZO("TBL_LOG_RCH"),
	TABLE_LOG_CARGO_ABONO("TBL_LOG_CARG_ABO"),
    TABLE_LOG_AVANCES_BICE("TBL_LOG_AVANCES_BICE");
	
	
	/**
	 * Valor ENUM.
	 */
	private final String text;

	/**
	 * Constructor.
	 * 
	 * @param text
	 */
	private ConstantesBD(final String text) {
		this.text = text;
	}

	/**
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return text;
	}
}
