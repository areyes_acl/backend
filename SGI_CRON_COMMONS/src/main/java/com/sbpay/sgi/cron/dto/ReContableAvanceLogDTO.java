package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

public class ReContableAvanceLogDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer sidContable;
    private String fecha;
    private String filename;
    private String flag;
    
	public Integer getSidContable() {
		return sidContable;
	}
	public void setSidContable(Integer sidContable) {
		this.sidContable = sidContable;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	@Override
	public String toString() {
		return "ReContableAvanceLogDTO [sidContable=" + sidContable
				+ ", fecha=" + fecha + ", filename=" + filename + ", flag="
				+ flag + "]";
	}  

}
