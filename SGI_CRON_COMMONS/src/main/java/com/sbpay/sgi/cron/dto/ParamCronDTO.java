package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 16/11/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * Clase encargada de mapear la tabla TBL_PRTS
 * 
 * </p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class ParamCronDTO implements Serializable {
    
    /**
	 * 
	 */
    private static final long serialVersionUID = -771323209087441009L;
    private DataFTP dataFTP;
    private String ftpPathOut;
    private String formatFilenameOut;
    private String pathAclInc;
    private String rutsbpay;
    private String formatExtNameOutCtr;
    private String formatExtNameOut;
    
    public DataFTP getDataFTP() {
        return dataFTP;
    }
    
    public void setDataFTP( DataFTP dataFTP ) {
        this.dataFTP = dataFTP;
    }
    
    public String getFtpPathOut() {
        return ftpPathOut;
    }
    
    public void setFtpPathOut( String ftpPathOut ) {
        this.ftpPathOut = ftpPathOut;
    }
    
    public String getFormatFilenameOut() {
        return formatFilenameOut;
    }
    
    public void setFormatFilenameOut( String formatFilenameOut ) {
        this.formatFilenameOut = formatFilenameOut;
    }
    
    public String getPathAclInc() {
        return pathAclInc;
    }
    
    public void setPathAclInc( String pathAclInc ) {
        this.pathAclInc = pathAclInc;
    }
    
    public String getRutsbpay() {
        return rutsbpay;
    }
    
    public void setRutsbpay( String rutsbpay ) {
        this.rutsbpay = rutsbpay;
    }
    
    public String getFormatExtNameOut() {
        return formatExtNameOut;
    }
    
    public void setFormatExtNameOut( String formatExtNameOut ) {
        this.formatExtNameOut = formatExtNameOut;
    }
    
    public String getFormatExtNameOutCtr() {
        return formatExtNameOutCtr;
    }
    
    public void setFormatExtNameOutCtr( String formatExtNameOutCtr ) {
        this.formatExtNameOutCtr = formatExtNameOutCtr;
    }
    
    @Override
    public String toString() {
        return "ParamCronDTO [dataFTP=" + dataFTP + ", ftpPathOut="
                + ftpPathOut + ", formatFilenameOut=" + formatFilenameOut
                + ", pathAclInc=" + pathAclInc + ", rutsbpay=" + rutsbpay
                + ", formatExtNameOutCtr=" + formatExtNameOutCtr
                + ", formatExtNameOut=" + formatExtNameOut + "]";
    }
    
}
