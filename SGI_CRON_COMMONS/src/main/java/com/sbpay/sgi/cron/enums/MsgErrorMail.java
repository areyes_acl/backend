package com.sbpay.sgi.cron.enums;

public enum MsgErrorMail {
	
	// ENVIO DE EMAIL
	ERROR_MSG_SEND("Error al enviar el correo de alerta"), 
	ERROR_GENERAL_SEND("Error general al enviar el correo de alerta"), 
	
	// FALTA DE PARAMETROS (TBL_PRTS)
	ALERTA_PRTS("Alerta error de configuracion el CRON"), 
	ALERTA_PRTS_TXT("Estimados:\n No se encuentran Cargados todos los parametros del cron el base de datos, verificar tabla TBL_PRTS."), 
	
	// DOWNLOAD OUTGOING DESDE TRANSBANK
	ALERTA_FILE_PROCESS("Alerta Casilla de intercambio TBK sin archivos"), 
	ALERTA_FILE_DOWNLOAD_OUTG_TBK("Estimados:\n No se han encontrado archivos OUTGOING para descargar en la casilla de intercambio (STFP) de Transbank."),
	
	// DOWNLOAD ARCHIVO CPAGO TRANSBANK
	ALERTA_DOWNLOAD_CPAGO_NO_FILES_FOUND_TITLE("Alerta Casilla de intercambio TBK sin archivos"), 
	ALERTA_DOWNLOAD_CPAGO_NO_FILES_FOUND_BODY("Estimados:\n No se han encontrado archivos Compensacion de Pago validos, para procesar en la casilla de intercambio de Transbank."), 
	ALERTA_DOWNLOAD_CPAGO_PROBLEM_TITLE("Alerta de descarga de archivo compensación de pago"),
	ALERTA_DOWNLOAD_CPAGO_PROBLEM_BODY("Estimados:\n No se han podido descargar los siguientes archivos desde SFTP de Transbank : "),
	ALERTA_DOWNLOAD_ALL_FILES_ALREADY_PROCESSED_CPAGO_TITLE("Alerta : Casilla de intercambio TBK sin archivos de Comision de Pago pendientes"),
	ALERTA_DOWNLOAD_ALL_FILES_ALREADY_PROCESSED_CPAGO_BODY("Estimados:\n No se han encontrado archivos de Compensacion de Pagos pendientes de descarga en la casilla de intercambio de Transbank."),
		
	ALERTA_FILE_PROCESSED("Alerta Casilla de intercambio TBK sin archivos pendientes"),
	ALERTA_FILE_PROCESSED_TXT("Estimados:\n No se han encontrado archivos OUTGOING pendientes de procesamiento en la casilla de intercambio de Transbank."), 
	ALERTA_FILE_LOAD("Alerta de descarga de archivo outgoing"),
	ALERTA_FILE_LOAD_TXT("Estimados:\n No ha sido posible descargar de Transbank el/los siguiente archivo:\n"), 
	ALERTA_PRTS_UPLOAD("Alerta Faltan Parametros del Proceso Upload Incoming Transbank"), 
	ALERTA_PRTS_TXT_UPLOAD("Estimados:\n No se encuentran Cargados todos los parametros del proceso incoming transbank en la base de datos, verificar tabla TBL_PRTS."), 

	
	//ASIENTOS CONTABLES
	ALERTA_PRTS_UPLOAD_CNTBL("Alerta Faltan Parametros del Proceso Upload de Asientos Contables"), 
	ALERTA_PRTS_TXT_UPLOAD_CNTBL("Estimados:\n No se encuentran Cargados todos los parametros del proceso de asientos contables en la base de datos, verificar tabla TBL_PRTS."), 
	
	//ASIENTOS CONTABLES AVANCES
	ALERTA_PRTS_UPLOAD_CNTBL_AVA("Alerta Faltan Parametros del Proceso Upload de Asientos Contables para avances con transferencias"), 
	ALERTA_PRTS_TXT_UPLOAD_CNTBL_AVA("Estimados:\n No se encuentran Cargados todos los parametros del proceso de asientos contables para avances con transferenciasen la base de datos, verificar tabla TBL_PRTS."), 
	
	//PROCESO UPLOAD INCOMING
	ALERTA_FILE_UPLOAD("Alerta Casilla de intercambio sbpay sin archivos"), 
	ALERTA_FILE_UPLOAD_TXT("Estimados:\n No se han encontrado archivos INCOMING para subir a Transbank"), 
	ALERTA_FILE_UPLOAD_TXT_CNTBL("Estimados:\n No se han encontrado archivos de Asientos Contables para subir a Transbank"), 
	ALERTA_FILE_PROCESSED_UPLOAD("Alerta Casilla de intercambio sbpay sin archivos pendientes"), 
	ALERTA_FILE_PROCESSED_UPLOAD_TXT("Estimados:\n No se han encontrado archivos INCOMING pendientes para subir a Transbank."), 
	 
	ALERTA_FILE_UPLOAD_INCOMING("Alerta Upload archivo incoming"), 
	ALERTA_FILE_UPLOAD_INCOMING_TXT("Estimados:\n No ha sido posible subir a TRANSBANK el(los) siguiente(s) archivo(s):\n"), 
	//ASIENTOS CONTABLE
	ALERTA_FILE_UPLOAD_CNTBL("Alerta al subir archivos Asientos Contables"), 
	ALERTA_UPLOAD_FILE_CNTBL_OWORLD("Estimados:\n No ha sido posible subir a ONEWORD el(los) siguiente(s) archivo(s):\n"),
	
	//ASIENTOS CONTABLE AVANCES
	ALERTA_FILE_UPLOAD_CNTBL_AVA("Alerta al subir archivos Asientos Contables para avances con transferencia"), 
	ALERTA_UPLOAD_FILE_CNTBL_OWORLD_AVA("Estimados:\n No ha sido posible subir a ONEWORD el(los) siguiente(s) archivo(s):\n"),
	ALERTA_FILE_UPLOAD_TXT_CNTBL_AVA("Estimados:\n No se han encontrado archivos de Asientos Contables de avances con tranferencia para subir a Transbank"), 
	
	// PROCESO UPLOAD OUTGOING VISA IC
	ALERTA_FILE_UPLOAD_OUT_VISA_IC_TITLE("Alerta en proceso Upload archivo outgoing Visa a IC"), 
    ALERTA_FILE_UPLOAD_OUT_VISA_IC_BODY("Estimados:\n No ha sido posible subir al fto de IC el/los siguiente(s) archivo:\n"),
    ALERTA_FILE_UPLOAD_OUT_VISA_IC_WARN("Estimados:\n No se han encontrado archivos OUTGOING VISA pendientes para subir a ftp IC."),
        	
	//PROCESO RECHAZOS
	ALERTA_RCH_PRTS("Alerta Faltan Parametros del Proceso de Rechazo"), 
	ALERTA_RCH_PRTS_TXT("Estimados:\n No se encuentran Cargados todos los parametros del proceso de rechazos en la base de datos"),
	
	//PROCESO BICE
	ALERTA_RCH_PRTS_BICE("Alerta Faltan Parametros del Proceso"), 
	ALERTA_RCH_PRTS_TXT_BICE("Estimados:\n No se encuentran Cargados todos los parametros del proceso en la base de datos"),
	
	//PROCESO AVANCES
	ALERTA_RCH_PRTS_AVA("Alerta Faltan Parametros del Proceso de Avances"), 
	ALERTA_RCH_PRTS_TXT_AVA("Estimados:\n No se encuentran Cargados todos los parametros del proceso de avances en la base de datos"),
	
	// PROCESO GENERACION INCOMING
	ERROR_GEN_INPUT_FILE("Ha ocurrido un error al intentar generar el incoming transbank"), 
	//PROCESO CARGO Y ABONOS (generacion archivo)
	ALERTA_ABO_CAR_PRTS("Alerta Faltan Parametros del Proceso de Cargo y Abonos"), 
	ALERTA_ABO_CAR_PRTS_TXT("Estimados:\n No se encuentran Cargados todos los parametros del proceso de cargos y abonos en la base de datos"),
	ALERTA_ABO_CAR_PROCESS("Alerta Archivo Cargo y Abonos"), 
	ALERTA_ABO_CAR_PROCESS_TXT("Estimados:\n No es posible generar el archivo de Cargos y Abono"), 
	ALERTA_PRTS_UPLOAD_IC("Alerta Faltan Parametros del Proceso Upload Outgoing IC"), 
	
	//VISA NACIONAL
	ALERTA_PRTS_UPLOAD_IC_VISA_NAC("Alerta Faltan Parametros del Proceso Upload Outgoing visa nacional IC"), 
	ALERTA_PRTS_TXT_UPLOAD_IC_VISA_NAC("Estimados:\n No se encuentran Cargados todos los parametros del proceso upload outgoingvisa nacional IC en la base de datos, verificar tabla TBL_PRTS."), 
	ALERTA_FILE_UPLOAD_TXT_IC_VISA_NAC("Estimados:\n No se han encontrado archivos outgoing visa nacional, para poder subir a IC"),
	ALERTA_FILE_UPLOAD_IC_VISA_NAC("Alerta carpeta de intercambio outgoing sbpay-IC sin archivos"), 
	ALERTA_FILE_PROCESSED_UPLOAD_TXT_IC_VISA_NAC("Estimados:\n No se han encontrado archivos Outgoing visa nacional pendientes para subir a IC."),
	ALERTA_FILE_PROCESSED_UPLOAD_IC_VISA_NAC("Alerta Casilla de intercambio sbpay-IC sin archivos pendientes"), 
	MSG_NOT_ALL_FILE_UPLOADED_VISA_NAC("Estimados:\n Se ha ejecutado el proceso de subida del outgoing visa nacional a IC, sin embargo no se ha subido algunos archivos : "),
	ALERTA_NOT_ALL_FILE_UPLOADED_VISA_NAC("[Alerta] Proceso de subida de outgoing visa nacional a IC"),
	
	
	ALERTA_PRTS_TXT_UPLOAD_IC("Estimados:\n No se encuentran Cargados todos los parametros del proceso upload outgoing IC en la base de datos, verificar tabla TBL_PRTS."), 
	ALERTA_FILE_UPLOAD_IC("Alerta carpeta de intercambio outgoing sbpay-IC sin archivos"), 
	ALERTA_FILE_UPLOAD_TXT_IC("Estimados:\n No se han encontrado archivos outgoing de transbank, para poder subir a IC"), 
	ALERTA_FILE_PROCESSED_UPLOAD_IC("Alerta Casilla de intercambio sbpay-IC sin archivos pendientes"), 
	ALERTA_FILE_PROCESSED_UPLOAD_TXT_IC("Estimados:\n No se han encontrado archivos Outgoing pendientes para subir a IC."),
	ALERTA_NOT_ALL_FILE_UPLOADED("[Alerta] Proceso de subida de outgoing de transbank a IC"),
	MSG_NOT_ALL_FILE_UPLOADED("Estimados:\n Se ha ejecutado el proceso de subida del outgoing de transbank a IC, sin embargo no se ha subido algunos archivos : "),
	ALERTA_FILE_UPLOAD_CAR_ABO("Alerta directorio de intercambio sbpay de cargos y abonos sin archivos"),
	ALERTA_FILE_UPLOAD_TXT_CAR_ABO("Estimados:\n No se han encontrado archivos de cargos y abonos para subir a IC"),
	ALERTA_PRTS_UPLOAD_CAR_ABO("Alerta Faltan Parametros del Proceso Upload Cargo y Abono IC"),
	ALERTA_PRTS_TXT_UPLOAD_CAR_ABO("Estimados:\n No se encuentran Cargados todos los parametros del proceso upload de cargos y abonos a IC en la base de datos, verificar tabla TBL_PRTS."),
	ALERTA_FILE_PROCESSED_UPLOAD_CAR_ABO("Alerta directorio de cargos y abonos sin archivos pendientes"),
	ALERTA_FILE_PROCESSED_UPLOAD_TXT_CAR_ABO("Estimados:\n No se han encontrado archivos de cargos y abonos pendientes para subir a SFTP."),
	ALERTA_FILE_UPLOAD_CAR_ABO_TITLE("Ha ocurrido un problema al subir archivo SOLICITUD 60"),
	ALERTA_FILE_UPLOAD_CAR_ABO_BODY("Estimados:\n Ha ocurrido un problema al intentar subir un archivo a SFTP"),
	
	//PROCESO ONUS
	ALERTA_FILE_PROCESS_ONUS("Alerta Casilla de intercambio ONUS sin archivos"), 
	ALERTA_FILE_PROCESS_ONUS_TXT("Estimados:\n No se han encontrado archivos ONUS para procesar en la casilla de intercambio de ONUS."), 
	ALERTA_FILE_PROCESSED_ONUS_TXT("Estimados:\n No se han encontrado archivos ONUS pendientes de procesamiento en la casilla de intercambio de ONUS."), 
	ALERTA_FILE_PROCESSED_ONUS("Alerta Casilla de intercambio ONUS sin archivos pendientes"),
	ALERTA_FILE_ONUS_LOAD("Alerta de descarga de archivo ONUS"),
	ALERTA_FILE_LOAD_ONUS_TXT("Estimados:\n No ha sido posible descargar de ONUS el siguiente archivo:\n"),
	
	// PROCESO LOG AUTORIZACION
	ALERTA_FILE_PROCESSED_AUTO("Alerta Casilla de intercambio Log Autorizacion sin archivos pendientes"),
	ALERTA_FILE_PROCESSED_AUTO_TXT("Estimados:\n No se han encontrado archivos de Log Autorizacion pendientes de procesamiento en la casilla de intercambio de IC."),
	ALERTA_FILE_PROCESS_AUTO("Alerta Casilla de intercambio Log de Autorizacion sin archivos"), 
	ALERTA_FILE_PROCESS_AUTO_TXT("Estimados:\n No se han encontrado archivos de Log de Autorizacion para procesar en la casilla de intercambio de IC."),
	ALERTA_FILE_AUTO_LOAD("Alerta de descarga de archivo LOG AUTORIZACION"),
	ALERTA_FILE_LOAD_AUTO_TXT("Estimados:\n No ha sido posible descargar de IC el siguiente archivo:\n"),
	
	// PROCESO UPLOAD ONUS
	ALERTA_TITLE_UPLOAD_TO_TBK_ONUS("Alerta en proceso de subida de incoming ON US A Transbank"), 
	ALERTA_ALL_ALREADY_PROCESS_UPLOAD_TO_TBK_ONUS_TXT("Estimados:\n No se han encontrado archivos ONUS para Subir a Transbank, todos los archivos se encuentran ya procesados."), 
	ALERTA_NOT_FILES_FOUND_UPLOAD_TO_TBK_ONUS("Estimados:\n No se han encontrado archivos ONUS para Subir a Transbank."), 
	ALERTA_FILES_NOT_UPLOADED("Finaliza el proceso de subida de Incoming On Us a Transbank, sin embargo los siguientes archivos no podido ser cargados : ?"),
	
	
	// DOWNLOAD OUTGOING VISA INTERNACIONAL
	ALERTA_FILE_DOWNLOAD_VISA_TITLE("Alerta Casilla de intercambio VISA sin archivos pendientes"),
	ALERTA_FILE_DOWNLOAD_VISA_BODY("Estimados:\n No se han encontrado archivos OUTGOING Visa Internacional pendientes de descarga en la casilla de intercambio de VISA."),
	ALERTA_FILE_PROCESS_VISA("Alerta Casilla de intercambio VISA sin archivos"), 
	ALERTA_FILE_PROCESS_VISA_TXT("Estimados:\n No se han encontrado archivos OUTGOING Internacional para procesar en la casilla de intercambio de VISA."),
	ALERTA_FILE_LOAD_VISA("Alerta de descarga de archivo outgoing VISA"),
	ALERTA_FILE_LOAD_VISA_TXT("Estimados:\n No ha sido posible descargar de Visa el siguiente archivo:\n"),
	
	
	// DOWNLOAD OUTGOING VISA NACIONAL
	ALERTA_FILE_DOWNLOAD_VISA_NAC_TITLE("Alerta Casilla de intercambio VISA NACIONAL sin archivos pendientes"),
	ALERTA_FILE_DOWNLOAD_VISA_NAC_BODY("Estimados:\n No se han encontrado archivos OUTGOING Visa nacional pendientes de descarga en la casilla de intercambio de VISA NACIONAL."),
	ALERTA_FILE_PROCESS_VISA_NAC("Alerta Casilla de intercambio VISA NACIONAL sin archivos"), 
	ALERTA_FILE_PROCESS_VISA_NAC_TXT("Estimados:\n No se han encontrado archivos OUTGOING nacional para procesar en la casilla de intercambio de VISA NACIONAL."),
	ALERTA_FILE_LOAD_VISA_NAC("Alerta de descarga de archivo outgoing VISA NACIONAL"),
	ALERTA_FILE_LOAD_VISA_NAC_TXT("Estimados:\n No ha sido posible descargar de Visa nacional el siguiente archivo:\n"),
	
	//PROCESO UPLOAD VISA IC
	ALERTA_FILE_UPLOAD_VISA_IC("Alerta Casilla de intercambio sbpay sin archivos"), 
	ALERTA_FILE_UPLOAD_VISA_IC_TXT("Estimados:\n No se han encontrado archivos INCOMING VISA para subir a IC"), 
	ALERTA_FILE_PROCESSED_UPLOAD_VISA_IC("Alerta Casilla de intercambio sbpay sin archivos pendientes"), 
	ALERTA_FILE_PROCESSED_UPLOAD_VISA_IC_TXT("Estimados:\n No se han encontrado archivos INCOMING VISA pendientes para subir a IC."), 
	 
	
	//PROCESO UPLOAD VISA
	ALERTA_FILE_UPLOAD_VISA("Alerta Casilla de intercambio sin archivos"), 
	ALERTA_FILE_UPLOAD_VISA_INT("Alerta Casilla de intercambio sin archivos"), 
	ALERTA_FILE_UPLOAD_VISA_TXT("Estimados:\n No se han encontrado archivos Incoming VISA para subir a SFTP de Visa Nacional"), 
	ALERTA_FILE_UPLOAD_VISA_INT_TXT("Estimados:\n No se han encontrado archivos Incoming VISA Internacional para subir a SFTP de Visa Internacional"), 
	ALERTA_FILE_PROCESSED_UPLOAD_VISA("Alerta Casilla de intercambio sin archivos pendientes"), 
	ALERTA_FILE_PROCESSED_UPLOAD_VISA_INT("Alerta Casilla de intercambio sin archivos pendientes"), 
	ALERTA_FILE_PROCESSED_UPLOAD_VISA_TXT("Estimados:\n No se han encontrado archivos pendientes para subir a SFTP de Visa Nacinoal."), 
	ALERTA_FILE_PROCESSED_UPLOAD_VISA_INT_TXT("Estimados:\n No se han encontrado archivos pendientes para subir a SFTP de Visa Internacional."), 
	ALERTA_FTP_CONNECT_UPLOAD_VISA_TXT("Estimados:\n No es posible conectarse con el FTP de Visa para upload"), 
	ALERTA_FTP_CONNECT_UPLOAD_VISA("Alerta problemas de conectividad con Visa"), 
	ALERTA_FILE_UPLOAD_INCOMING_VISA("Alerta en proceso de subida de Incoming a Visa Nacional"), 
	ALERTA_FILE_UPLOAD_INCOMING_VISA_INT("Alerta en proceso de subida de Incoming a Visa Internacional"),
	ALERTA_FILE_UPLOAD_INCOMING_VISA_TXT("Estimados:\n Se ha ejectutado proceso de subida a SFTP VISA Nacional, sin embargo no ha sido posible subir el/los siguiente/s archivo/s:\n ?"),
	ALERTA_FILE_UPLOAD_INCOMING_VISA_INT_TXT("Estimados:\n Se ha ejectutado proceso de subida a SFTP VISA Internacional, sin embargo no ha sido posible subir el/los siguiente/s archivo/s:\n ?"),
	
	// PROCESO COMPENSACION DE PAGO
	 ERROR_PROCESS_DOWNLOAD_CPAGO_BODY("Proceso de descarga del archivo compensación de pago, no ha finalizado correctamente"),
	 
	 // UPLOAD OUTGOING IC 
	 ERROR_PROCESS_UPLOAD_OUT_VISA_BODY("Proceso de subida de outgoing de Visa a ftp de IC, se ha ejecutado con observaciones!"),
	 
	 // UPLOAD OUTGOING VISA NACIONALIC 
	 ERROR_PROCESS_UPLOAD_OUT_VISA_NAC_BODY("Proceso de subida de outgoing de Visa nacional a ftp de IC, se ha ejecutado con observaciones!"),
	 
	 // UPLOAD IC OUTGOING TRANSBANK
	 ERROR_PROCESS_UPLOAD_IC_BODY("Proceso de subida de outgoing de transbank a ftp de IC, se ha ejecutado con observaciones!"),
	 
	 // UPLOAD CONTABLE A ONEWORLD
	 ALERTA_FILE_PROCESSED_UPLOAD_TXT_CNTBL("Estimados:\n No se han encontrado archivos de Asientos Contables pendientes para subir a Transbank."), 
	 
	 // UPLOAD CONTABLE AVANCES A ONEWORLD
	 ALERTA_FILE_PROCESSED_UPLOAD_TXT_CNTBL_AVA("Estimados:\n No se han encontrado archivos de Asientos Contables para avances con transferencia pendientes para subir a Transbank."), 
	 
	 
	 //	// DOWNLOAD ARCHIVO DE AVANCES BICE
		ALERTA_FILE_PROCESSED_BICE("Alerta Casilla de intercambio BICE sin archivos pendientes"),
		ALERTA_FILE_PROCESSED_DAT_BICE("Estimados:\n No se han encontrado archivos de avances pendientes de procesamiento en la casilla de intercambio de BICE."),
		ALERTA_FILE_PROCESS_BICE("Alerta Casilla de intercambio BICE sin archivos"), 
		ALERTA_FILE_PROCESS_BICE_CTR("Alerta Casilla de intercambio BICE sin archivos de control"), 
		ALERTA_FILE_DOWNLOAD_BICE("Estimados:\n No se han encontrado archivos de avances para descargar en la casilla de intercambio (STFP) de BICE."),
		ALERTA_FILE_LOAD_BICE("Alerta de descarga de archivo de avances"),
		ALERTA_FILE_LOAD_DAT_BICE("Estimados:\n No ha sido posible descargar de BICE el/los siguiente archivo:\n"), 
		
		
		 //	// DOWNLOAD ARCHIVO DE AVANCES 
		ALERTA_FILE_PROCESSED_AVANCES("Alerta Casilla de intercambio AVANCES sin archivos pendientes"),
		ALERTA_FILE_PROCESSED_DAT_AVANCES("Estimados:\n No se han encontrado archivos de avances pendientes de procesamiento en la casilla de intercambio de AVANCES."),
		ALERTA_FILE_PROCESS_AVANCES("Alerta Casilla de intercambio AVANCES sin archivos"), 
		ALERTA_FILE_DOWNLOAD_AVANCES("Estimados:\n No se han encontrado archivos de avances para descargar en la casilla de intercambio (STFP) de AVANCES."),
		ALERTA_FILE_LOAD_AVANCES("Alerta de descarga de archivo de avances"),
		ALERTA_FILE_LOAD_DAT_AVANCES("Estimados:\n No ha sido posible descargar los Avances el/los siguiente archivo:\n"), 

	;

	/**
	 * Valor ENUM.
	 */
	private final String text;

	/**
	 * Constructor.
	 * 
	 * @param text
	 */
	private MsgErrorMail(final String text) {
		this.text = text;
	}

	/**
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return text;
	}
}
