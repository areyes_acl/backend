package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 18/12/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class ParamsProcessOnusDTO implements Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = 4081693614785403716L;
	private String formatFileNameOnUs;
	private String formatExtNameOnUs;
	private String pathAbcOnUs;
	private String pathAbcOnUsError;
	private String pathAbcOnUsBkp;

	public String getFormatFileNameOnUs() {
		return formatFileNameOnUs;
	}

	public void setFormatFileNameOnUs(String formatFileNameOnUs) {
		this.formatFileNameOnUs = formatFileNameOnUs;
	}

	public String getFormatExtNameOnUs() {
		return formatExtNameOnUs;
	}

	public void setFormatExtNameOnUs(String formatExtNameOnUs) {
		this.formatExtNameOnUs = formatExtNameOnUs;
	}

	public String getPathAbcOnUs() {
		return pathAbcOnUs;
	}

	public void setPathAbcOnUs(String pathAbcOnUs) {
		this.pathAbcOnUs = pathAbcOnUs;
	}

	public String getPathAbcOnUsError() {
		return pathAbcOnUsError;
	}

	public void setPathAbcOnUsError(String pathAbcOnUsError) {
		this.pathAbcOnUsError = pathAbcOnUsError;
	}

	public String getPathAbcOnUsBkp() {
		return pathAbcOnUsBkp;
	}

	public void setPathAbcOnUsBkp(String pathAbcOnUsBkp) {
		this.pathAbcOnUsBkp = pathAbcOnUsBkp;
	}
	
	@Override
	public String toString() {
		return "ParamsProcessOnusDTO [formatFileNameOnUs=" + formatFileNameOnUs
				+ ", formatExtNameOnUs=" + formatExtNameOnUs + ", pathAbcOnUs="
				+ pathAbcOnUs + ", pathAbcOnUsError=" + pathAbcOnUsError
				+ ", pathAbcOnUsBkp=" + pathAbcOnUsBkp + "]";
	}
}
