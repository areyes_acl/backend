package com.sbpay.sgi.cron.utils.file;

import java.io.File;
import java.util.List;

import com.sbpay.sgi.cron.dto.CompensacionPagoDTO;

public interface CompscnPagoFIleUtil {

	public List<CompensacionPagoDTO> readCompscnPagoFile(File file)throws Exception ;
}
