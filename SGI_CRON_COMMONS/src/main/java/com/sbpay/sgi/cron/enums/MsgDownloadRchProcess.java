package com.sbpay.sgi.cron.enums;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 21/12/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public enum MsgDownloadRchProcess {
    
    PROCESS_FINISH_OK_TITLE("Proceso Descarga Rechazos Ejecutado correctamente."),
    PROCESS_FINISH_WITH_WARN("Proceso Descarga Rechazos Ejecutado Con observaciones"),
    PROCESS_FINISH_OK_BODY("Estimados, el proceso de descarga de archivos de rechazos desde IC, ha concluido satistfactoriamente."),
    ALL_FILES_ALREADY_PROCESSED("El proceso se ha ejecutado correctamente, sin embargo se encuentran todos los archivos ya procesados."),
    ALL_FILES_HAS_NOT_VALID_FORMAT("El proceso se ha ejecutado correctamente sin embargo no se ha descargado ningun archivo debido a que no contienen el formato de válido para los rechazos."),
    ERROR_MAIL_TITLE_PROCESS_DOWNLOAD("Se ha provocado un error en la ejecucion del proceso: Descarga Rechazos"),
    ERROR_SAVE_LOG("Ha ocurrido un error al intentar guardar el log de descarga de rechazos");
    
    /**
     * Valor ENUM.
     */
    private final String text;
    
    /**
     * Constructor.
     * 
     * @param text
     */
    private MsgDownloadRchProcess( final String text ) {
        this.text = text;
    }
    
    /**
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
    
}
