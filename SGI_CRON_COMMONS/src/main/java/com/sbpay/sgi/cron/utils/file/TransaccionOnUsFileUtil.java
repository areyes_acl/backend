package com.sbpay.sgi.cron.utils.file;

import java.io.File;
import java.util.List;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dto.ICCodeHomologationDTO;
import com.sbpay.sgi.cron.dto.LogTransaccionOnUsDTO;
import com.sbpay.sgi.cron.dto.TransaccionOnUsDTO;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public interface TransaccionOnUsFileUtil {

	/**
	 * 
	 * @param file
	 * @param starWith
	 * @param listaCodigosIc
	 * @return
	 * @throws AppException
	 */
	public LogTransaccionOnUsDTO readLogTrxOnUsrFile(File file,
			String starWith, List<ICCodeHomologationDTO> listaCodigosIc)
			throws AppException;

	/**
	 * 
	 * @param ruta
	 * @param incomingFilename
	 * @param controlFilename
	 * @param listaTrasacciones
	 * @throws AppException
	 */
	public void exportIncomingOnUsFile(String ruta, String incomingFilename,
			String controlFilename, List<TransaccionOnUsDTO> listaTrasacciones)
			throws AppException;

}
