package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

public class SolicitudReporteContableDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer sid;
	private Integer idOperador;
    private Integer idProducto;
    private String tipoMov;
    private String fechaIni;
    private String fechaFin;
    private String fichero;
    
    
	public Integer getSid() {
		return sid;
	}

	public void setSid(Integer sid) {
		this.sid = sid;
	}

	public String getFechaIni() {
		return fechaIni;
	}

	public void setFechaIni(String fechaIni) {
		this.fechaIni = fechaIni;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getFichero() {
		return fichero;
	}

	public void setFichero(String fichero) {
		this.fichero = fichero;
	}
	
	public String getTipoMov() {
		return tipoMov;
	}

	public void setTipoMov(String tipoMov) {
		this.tipoMov = tipoMov;
	}
	

	public Integer getIdOperador() {
		return idOperador;
	}

	public void setIdOperador(Integer idOperador) {
		this.idOperador = idOperador;
	}

	public Integer getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(Integer idProducto) {
		this.idProducto = idProducto;
	}

	@Override
	public String toString() {
		return "SolicitudReporteContableDTO [sid=" + sid
				+ ", fechaIni=" + fechaIni + ", fechaFin=" + fechaFin + ", idOperador="
				+ idOperador + ", fichero=" + fichero + ", tipoMov="+tipoMov+ ", idProducto="+idProducto+ "]";
	}  

}
