package com.sbpay.sgi.cron.enums;

public enum PropertyEntryTypes {
	FIRST_JOB_NAME("jobDetail.firstJob.name"),
	FIRST_JOB_GROUP("jobDetail.firstJob.group"),
	FIRST_JOB_CRON_TRIGGER_NAME("jobDetail.firstJob.cronTrigger.name"),
	FIRST_JOB_CRON_TRIGGER_GROUP("jobDetail.firstJob.cronTrigger.group"),
	FIRST_JOB_CRON_TRIGGER_TIMER("jobDetail.firstJob.cronTrigger.timer"),
	
	SECOND_JOB_NAME("jobDetail.secondJob.name"),
	SECOND_JOB_GROUP("jobDetail.secondJob.group"),
	SECOND_JOB_CRON_TRIGGER_NAME("jobDetail.secondJob.cronTrigger.name"),
	SECOND_JOB_CRON_TRIGGER_GROUP("jobDetail.secondJob.cronTrigger.group"),
	SECOND_JOB_CRON_TRIGGER_TIMER("jobDetail.secondJob.cronTrigger.timer"),
	
	THIRD_JOB_NAME("jobDetail.thirdJob.name"),
	THIRD_JOB_GROUP("jobDetail.thirdJob.group"),
	THIRD_JOB_CRON_TRIGGER_NAME("jobDetail.thirdJob.cronTrigger.name"),
	THIRD_JOB_CRON_TRIGGER_GROUP("jobDetail.thirdJob.cronTrigger.group"),
	THIRD_JOB_CRON_TRIGGER_TIMER("jobDetail.thirdJob.cronTrigger.timer"),
	
	FOUR_JOB_NAME("jobDetail.fourJob.name"),
	FOUR_JOB_GROUP("jobDetail.fourJob.group"),
	FOUR_JOB_CRON_TRIGGER_NAME("jobDetail.fourJob.cronTrigger.name"),
	FOUR_JOB_CRON_TRIGGER_GROUP("jobDetail.fourJob.cronTrigger.group"),
	FOUR_JOB_CRON_TRIGGER_TIMER("jobDetail.fourJob.cronTrigger.timer"),
	
	FIVE_JOB_NAME("jobDetail.fiveJob.name"),
	FIVE_JOB_GROUP("jobDetail.fiveJob.group"),
	FIVE_JOB_CRON_TRIGGER_NAME("jobDetail.fiveJob.cronTrigger.name"),
	FIVE_JOB_CRON_TRIGGER_GROUP("jobDetail.fiveJob.cronTrigger.group"),
	FIVE_JOB_CRON_TRIGGER_TIMER("jobDetail.fiveJob.cronTrigger.timer"),
	
	//se configura  parametros de ejecucion desde properties
	SIX_JOB_NAME("jobDetail.sixJob.name"),
	SIX_JOB_GROUP("jobDetail.sixJob.group"),
	SIX_JOB_CRON_TRIGGER_NAME("jobDetail.sixJob.cronTrigger.name"),
	SIX_JOB_CRON_TRIGGER_GROUP("jobDetail.sixJob.cronTrigger.group"),
	SIX_JOB_CRON_TRIGGER_TIMER("jobDetail.sixJob.cronTrigger.timer"),
	
	//se configura  parametros de ejecucion desde properties
	SEVEN_JOB_NAME("jobDetail.sevenJob.name"),
	SEVEN_JOB_GROUP("jobDetail.sevenJob.group"),
	SEVEN_JOB_CRON_TRIGGER_NAME("jobDetail.sevenJob.cronTrigger.name"),
	SEVEN_JOB_CRON_TRIGGER_GROUP("jobDetail.sevenJob.cronTrigger.group"),
	SEVEN_JOB_CRON_TRIGGER_TIMER("jobDetail.sevenJob.cronTrigger.timer"),
	
	//se configura  parametros de ejecucion desde properties
	EIGHT_JOB_NAME("jobDetail.eightJob.name"),
	EIGHT_JOB_GROUP("jobDetail.eightJob.group"),
	EIGHT_JOB_CRON_TRIGGER_NAME("jobDetail.eightJob.cronTrigger.name"),
	EIGHT_JOB_CRON_TRIGGER_GROUP("jobDetail.eightJob.cronTrigger.group"),
	EIGHT_JOB_CRON_TRIGGER_TIMER("jobDetail.eightJob.cronTrigger.timer"),
	
	JNDI("sbpay.cron.jndi"),
	//se agrega para poder generar conexion a bd oracle erp
	JNDI_AVA("erp.cron.jndi")
	;
	
	
	/**
	 * Valor ENUM.
	 */
	private final String key;

	/**
	 * Constructor.
	 * 
	 * @param text
	 */
	private PropertyEntryTypes(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}
	
	


}
