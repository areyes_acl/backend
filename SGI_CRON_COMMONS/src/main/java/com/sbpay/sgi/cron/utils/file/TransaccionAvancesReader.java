package com.sbpay.sgi.cron.utils.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dto.ICCodeHomologationDTO;
import com.sbpay.sgi.cron.dto.LogTransaccionAvancesDTO;
import com.sbpay.sgi.cron.dto.TransaccionAvancesDTO;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.MsgErrorFile;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/10/2018, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class TransaccionAvancesReader implements TransaccionAvancesFileUtil {

	/** VARIABLE PARA EL LOGER */
	private static final Logger LOGGER = Logger
			.getLogger(TransaccionAvancesReader.class);

	private List<ICCodeHomologationDTO> listaCodigosIc = null;
	private String detalleTrxLeidasConError = "";

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/10/2018, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * GETTERS Y SETTERS
	 * 
	 * @return
	 * @since 1.X
	 */
	public String getDetalleTrxLeidasConError() {
		return detalleTrxLeidasConError;
	}

	public void setDetalleTrxLeidasConError(String detalleTrxLeidasConError) {
		this.detalleTrxLeidasConError = detalleTrxLeidasConError;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/10/2018, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @param file
	 * @param starWith
	 * @param listaCodigosIc
	 * @return
	 * @throws AppException
	 * @see com.sbpay.sgi.cron.utils.file.TransaccionAvancesFileUtil#readLogTrxAvancesFile(java.io.File,
	 *      java.lang.String, java.util.List)
	 * @since 1.X
	 */
	@Override
	public LogTransaccionAvancesDTO readLogTrxAvancesFile(File file, String starWith,
			List<ICCodeHomologationDTO> listaCodigosIc) throws AppException {

		LogTransaccionAvancesDTO logTrxAvancesDTO = new LogTransaccionAvancesDTO();
		List<TransaccionAvancesDTO> trxAutList = null;
		BufferedReader buffReader = null;
		this.listaCodigosIc = listaCodigosIc;
		try {

			if (file == null) {
				throw new AppException(
						MsgErrorFile.ERROR_READ_FILE_NULL.toString());
			}

			String ruta = file.getAbsolutePath();
			buffReader = new BufferedReader(new FileReader(ruta));

			trxAutList = readFile(buffReader);

		} catch (IOException ioe) {
			throw new AppException("Ha ocurrido un error al leer el archivo : "
					+ ioe.getMessage(), ioe);
		} finally {
			try {
				if (buffReader != null) {
					buffReader.close();
				}
			} catch (IOException ioe1) {
				throw new AppException(
						"Ha ocurrido un error al cerrar el archivo", ioe1);
			}
		}

		logTrxAvancesDTO.setTrxList(trxAutList);
		logTrxAvancesDTO.setDetalleLectura(detalleTrxLeidasConError);

		return logTrxAvancesDTO;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/10/2018, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo encargado de ir linea a linea leyendo el archivo AVANCES
	 * 
	 * @param buffReader
	 * @return
	 * @throws IOException
	 * @throws AppException
	 * @since 1.X
	 */
	private List<TransaccionAvancesDTO> readFile(BufferedReader buffReader)
			throws IOException, AppException {
		String line = buffReader.readLine();
		List<TransaccionAvancesDTO> listaTransacciones = new ArrayList<TransaccionAvancesDTO>();
		TransaccionAvancesDTO tr = null;

		// RECORRE EL FILE LEYENDO LINEA POR LINEA
		
		while (line != null) {
			try {
				tr = parseToTransaccion(line);
				if (tr != null) {
					listaTransacciones.add(tr);
				}
			} catch (AppException e) {
				LOGGER.error(e.getMessage(), e);
				detalleTrxLeidasConError = detalleTrxLeidasConError.concat(
						"\n -").concat(e.getMessage());
			}
				line = buffReader.readLine();
			

		}

		LOGGER.info("-->Nº TRX AVANCES  leìdas  = "
				+ listaTransacciones.size());
		return listaTransacciones;

	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/10/2018, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo encargado de parsear la linea del archivo leido a un objeto del
	 * tipo TransaccionAvancesDTO.
	 * 
	 * @param line
	 * @return
	 * @throws AppException
	 * @since 1.X
	 */
	private TransaccionAvancesDTO parseToTransaccion(String line)
			throws AppException {
		TransaccionAvancesDTO transaccion = null;
		
		 if ( line != null) {

			transaccion = new TransaccionAvancesDTO();
			transaccion.setFechaTransferencia(line.substring(0, 10));
			transaccion.setCodAutorizacion(line.substring(10, 16));
			transaccion.setNumTrx(line.substring(16, 26));
			transaccion.setNumTarjeta(line.substring(26, 42));
			transaccion.setMonto(parseaMontos(line.substring(42, 55))); //HAY QUE AGREGARLE DOS 00
			transaccion.setEstado(line.substring(55, 58));
			transaccion.setNumCanal(line.substring(58, 68));
			transaccion.setMontoSeguro(parseaMontos(line.substring(68, 78)));//HAY QUE AGREGARLE DOS 00
		 }

		return transaccion;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/10/2018, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @return
	 * @since 1.X
	 */
	private String parseaMontos(String monto) {

		if (monto != null
				&& !monto.equalsIgnoreCase(ConstantesUtil.EMPTY.toString())) {
			return String.valueOf(Integer.parseInt(monto) * 100);

		} else {
			return String.valueOf(0 * 100);
		}

	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/10/2018, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @param icCode
	 * @return
	 * @throws AppException
	 * @since 1.X
	 */
	private String traducirCodigoDeIcACodigoTransbank(String icCode)
			throws AppException {
		String codTrx = null;

		for (ICCodeHomologationDTO instanceIc : listaCodigosIc) {
			if (instanceIc.isActivo() && instanceIc.getCodigoIC() != null
					&& instanceIc.getCodigoIC().equalsIgnoreCase(icCode)) {
				codTrx = instanceIc.getCodigoTransbank();
				break;
			}
		}

		if (codTrx == null) {
			throw new AppException(
					"No se ha encontrado codigo de transaccion de IC : "
							+ icCode
							+ ", homologado a los codigos de transbank en la tabla TBL_PRTS_IC_CODE.");
		}

		return codTrx;
	}

	@Override
	public void exportAvancesAvancesFile(String ruta, String incomingFilename,
			String controlFilename, List<TransaccionAvancesDTO> listaTrasacciones) {
		// TODO Auto-generated method stub

	}

}



