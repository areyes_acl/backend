package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 18/12/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * Clase que contiene los campos de las tablas que se utilizaran para el log de
 * los procesos
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class BolLogDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 460341730561246832L;
    private long sid;
    private String fecha;
    private String filename;
    private String fileFlag;
    private String fileTsDownload;
    private String fileTSUpload;

    public String getFileTSUpload() {
	return fileTSUpload;
    }

    public void setFileTSUpload(String fileTSUpload) {
	this.fileTSUpload = fileTSUpload;
    }

    public long getSid() {
	return sid;
    }

    public void setSid(long sid) {
	this.sid = sid;
    }

    public String getFecha() {
	return fecha;
    }

    public void setFecha(String fecha) {
	this.fecha = fecha;
    }

    public String getFilename() {
	return filename;
    }

    public void setFilename(String filename) {
	this.filename = filename;
    }

    public String getFileFlag() {
	return fileFlag;
    }

    public void setFileFlag(String fileFlag) {
	this.fileFlag = fileFlag;
    }

    public String getFileTsDownload() {
	return fileTsDownload;
    }

    public void setFileTsDownload(String fileTsDownload) {
	this.fileTsDownload = fileTsDownload;
    }

    @Override
    public String toString() {
	return "BolLogDTO [sid=" + sid + ", fecha=" + fecha + ", filename="
		+ filename + ", fileFlag=" + fileFlag + ", fileTsDownload="
		+ fileTsDownload + ", fileTSUpload=" + fileTSUpload + "]";
    }

}
