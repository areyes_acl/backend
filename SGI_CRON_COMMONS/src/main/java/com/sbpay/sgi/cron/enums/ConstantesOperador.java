package com.sbpay.sgi.cron.enums;

public enum ConstantesOperador {
	TBK("TBK"),
	VISA_NAC("VN"),
	VISA_INT("VI");
	
	
	/**
	 * Valor ENUM.
	 */
	private final String text;

	/**
	 * Constructor.
	 * 
	 * @param text
	 */
	private ConstantesOperador(final String text) {
		this.text = text;
	}

	/**
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return text;
	}
}
