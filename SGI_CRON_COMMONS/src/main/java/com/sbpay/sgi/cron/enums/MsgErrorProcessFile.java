package com.sbpay.sgi.cron.enums;

public enum MsgErrorProcessFile {
	ERROR_INCOMING_NAME_NOT_FOUND("Error al consultar por el arhivo incoming, no se encuentra registro en la tabla TBL_LOG_INC."), 
	ERROR_INVALID_INCOMING_STATE("Archivo no es valido para ser procesado, su codigo incoming_flag en la tabla TBL_LOG_INC es distinto de 0"), 
	ERROR_INVALID_DATES("Error producido por que La Fecha del nombre del archivo no es igual a la que contiene dentro en su cabecera"), 
	ERROR_CLOSE_FILE("Error al cerrar el archivo transferido de carga Incoming en sbpay"),
	ERROR_IN_PROCESS_OUTGOING("Error al Procesar OUTGOING Transbank"), 
	WARNING_NO_FILE_PROCESSING_TITLE("[ADVERTENCIA]: Ningun archivo procesado"),
	WARNING_NO_FILES_FOUNDS(" no se ha procesado ningun archivo debido a que no existe ningun archivo en el directorio para ser procesado"),
	WARNING_NO_FILE_PROCESSING_CAUSE(" No se ha procesado ningun archivo"),
	ERROR_LOG_TRX_AUTOR_NOT_FOUND("Error al consultar por log del archivo trx autorizadas,  no se encuentra registro en la tabla TBL_LOG_TRANSAC_AUTOR."),
	ERROR_INVALID_LOGTRXAUTOR_STATE("Archivo no es valido para ser procesado, su codigo flag en la tabla TBL_LOG_TRANSAC_AUTOR es distinto de 0"),
	ERROR_IN_PROCESS_LOG_TRX_TRANSAC("Error al procesar log de transacciones"),
	ERROR_NOT_ALL_LOG_TRX_ARE_READER("Ha ocurrido un problema y no se han podido leer todas las transacciones"),
	ERROR_IN_PROCESS_OUTGOING_VISA("Error al Procesar Archivo Outgoing VISA"),
	ERROR_INCOMING_VISA_NAME_NOT_FOUND("Error al consultar por el arhivo incoming, no se encuentra registro en la tabla TBL_LOG_VISA."),
	ERROR_INVALID_INCOMING_VISA_NULL_STATE("Archivo no es valido para ser procesado, su codigo flag se encuentra con un valor nulo en la tabla TBL_LOG_VISA"),
	ERROR_INVALID_INCOMING_VISA_NO_OK_STATE("Archivo no es valido para ser procesado, su codigo incoming_flag en la tabla TBL_LOG_VISA es distinto de 0"),
	ERROR_LIST_CODE_IS_NOT_AVAILABLE("No se ha cargado (o no estan activos) los codigos de homologacion de IC, en la tabla TLB_PRST_IC_CODE."),
	ERROR_LIST_CODE_IS_NOT_AVAILABLE_BICE("No se ha cargado todos los parametros necesario para procesar archivo bice, en la tabla TBL_PRTS."),
	ERROR_LIST_CODE_IS_NOT_AVAILABLE_AVANCES("No se ha cargado todos los parametros necesarios para procesar archivo de avances, en la tabla TBL_PRTS."),
	
	//VISA NACIONAL
	ERROR_INCOMING_VISA_NAC_NAME_NOT_FOUND("Error al consultar por el arhivo incoming(outgoing) visa nacional, no se encuentra registro en la tabla TBL_LOG_VISA."),
	ERROR_INVALID_INCOMING_VISA_NAC_STATE("Archivo no es valido para ser procesado, su codigo incomingvisa(outgoing) nacional flag en la tabla TBL_LOG_VISA es distinto de 0"), 
	ERROR_IN_PROCESS_OUTGOING_VISA_NAC("Error al Procesar OUTGOING VISA NACIONAL"), 
	
	//VISA INTERNACIONAL
	ERROR_INCOMING_VISA_INT_NAME_NOT_FOUND("Error al consultar por el arhivo incoming(outgoing) visa internacional, no se encuentra registro en la tabla TBL_LOG_VISA_INT."),
	ERROR_INVALID_INCOMING_VISA_INT_STATE("Archivo no es valido para ser procesado, su codigo incomingvisa(outgoing) internacional flag en la tabla TBL_LOG_VISA_INT es distinto de 0"), 
	ERROR_IN_PROCESS_OUTGOING_VISA_INT("Error al Procesar OUTGOING VISA INTERNACIONAL")
	
	;

	/**
	 * Valor ENUM.
	 */
	private final String text;

	/**
	 * Constructor.
	 * 
	 * @param text
	 */
	private MsgErrorProcessFile(final String text) {
		this.text = text;
	}

	/**
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return text;
	}
}
