package com.sbpay.sgi.cron.enums;

public enum MsgErrorProcessFileCPago {
	ERROR_CPAGO_NAME_NOT_FOUND("Error al consultar por el arhivo de compensaciones, no se encuentra registro en la tabla TBL_LOG_CPAGO."), 
	ERROR_IN_PROCESS("Error al Procesar Compesaciones"),
    ERROR_CALL_SP_SIG_GUARDAR_CPAGO("Ha ocurrido un error al insertar el registro de compensacion de pago"), 
	ERROR_INVALID_CPAGO_STATE("Archivo no es valido para ser procesado, su codigo file_flag en la tabla TBL_LOG_CPAGO es distinto de 0"),;

	/**
	 * Valor ENUM.
	 */
	private final String text;

	/**
	 * Constructor.
	 * 
	 * @param text
	 */
	private MsgErrorProcessFileCPago(final String text) {
		this.text = text;
	}

	/**
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return text;
	}
}
