package com.sbpay.sgi.cron.enums;

public enum MsgErrorProcessFileComision {
	ERROR_COMISION_NAME_NOT_FOUND("Error al consultar por el arhivo de comisiones, no se encuentra registro en la tabla TBL_LOG_COMISION."), 
	ERROR_INVALID_COMISION_STATE("Archivo no es valido para ser procesado, su codigo file_flag en la tabla TBL_LOG_COMISION es distinto de 0"), 
	ERROR_INVALID_DATES("Error producido por que La Fecha del nombre del archivo no es igual a la que contiene dentro en su cabecera"), 
	ERROR_IN_PROCESS("Error al procesar archivo de  Comisiones"), 
	WARNING_NO_FILE_PROCESSING_TITLE("[ADVERTENCIA]: Ningun archivo procesado"),
	WARNING_NO_FILES_FOUNDS(" No se ha procesado ningun archivo debido a que no se existe ningun archivo en el directorio para ser procesado"),
	WARNING_NO_FILE_PROCESSING_CAUSE(" No se ha procesado ningun archivo de Comisiones.");

	/**
	 * Valor ENUM.
	 */
	private final String text;

	/**
	 * Constructor.
	 * 
	 * @param text
	 */
	private MsgErrorProcessFileComision(final String text) {
		this.text = text;
	}

	/**
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return text;
	}
}
