package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

public class ParamExportItzAvanceDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String pathSalida;
	private String nomArchivo;
	
	public String getPathSalida() {
		return pathSalida;
	}
	public void setPathSalida(String pathSalida) {
		this.pathSalida = pathSalida;
	}
	public String getNomArchivo() {
		return nomArchivo;
	}
	public void setNomArchivo(String nomArchivo) {
		this.nomArchivo = nomArchivo;
	}
	
	@Override
	public String toString() {
		return "ParamExportItzAvanceDTO [pathSalida=" + pathSalida
				+ ", nomArchivo=" + nomArchivo + "]";
	}
	
	
	
}
