package com.sbpay.sgi.cron.utils.file;

import java.io.File;
import java.util.List;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dto.IncomingDTO;
import com.sbpay.sgi.cron.dto.Transaccion;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 1/12/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * Interfaz que declara los metodos para leer los diversos arvchivos
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public interface IncomingFileUtils {

  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 2/12/2015, (ACL-sbpay) - versión inicial
   * </ul>
   * <p>
   * Metodo que lee un archivo incoming desde una ruta especificada y lo transforma a DTO. Se le
   * pasa el starWith para realizar la validacion de la fecha.
   * 
   * @param ruta
   * @param starWith
   * @return
   * @throws AppException
   * @since 1.X
   */
  public IncomingDTO readIncomingFile(String ruta, String starWith) throws AppException;

  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 2/12/2015, (ACL-sbpay) - versión inicial
   * </ul>
   * <p>
   * Metodo que lee un archivo incoming desde un file y lo transforma a DTO. Se le pasa el starWith
   * para realizar la validacion de la fecha del archivo
   * 
   * @param file
   * @param starWith
   * @return
   * @throws AppException
   * @since 1.X
   */
  public IncomingDTO readIncomingFile(File file, String starWith) throws AppException;


  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
   * </ul>
   * <p>
   * Metodo que dada la ruta el nombre y la lista de transacciones genera un archivo Incoming a TBK.
   * 
   * @param ruta : donde se almacenara el archivo incoming a generar
   * @param incomingFilename : Nombre del archivo incoming a generar
   * @param controlFilename : Nombre del archivo de control a generar
   * @param listaTrasacciones : Lista de transacciones que llenaran el archivo incoming a generar
   * @throws AppException
   * @since 1.X
   */
  public void exportIncomingFile(String ruta, String incomingFilename, String controlFilename,
      List<Transaccion> listaTrasacciones) throws AppException;

}
