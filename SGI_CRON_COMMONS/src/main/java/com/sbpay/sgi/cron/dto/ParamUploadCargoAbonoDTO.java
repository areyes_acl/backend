package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class ParamUploadCargoAbonoDTO implements Serializable {
    
    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;
    
    public ParamUploadCargoAbonoDTO() {
    }
    
    // PARAMETROS PATH OUTGOING
    private String pathCarAbo;
    private String formatFilenameCarAbo;
    private String formatExtNameCarAbo;
    private String pathCarAboBkp;
    private String pathCarAboError;
    private String pathSFTPEntrada;
    
    // PARAMETROS DEL FTPS60
    private DataFTP dataFTP;
    
    public String getPathCarAbo() {
        return pathCarAbo;
    }
    
    public void setPathCarAbo( final String pathCarAbo ) {
        this.pathCarAbo = pathCarAbo;
    }
    
    public String getFormatFilenameCarAbo() {
        return formatFilenameCarAbo;
    }
    
    public void setFormatFilenameCarAbo( final String formatFilenameCarAbo ) {
        this.formatFilenameCarAbo = formatFilenameCarAbo;
    }
    
    public String getFormatExtNameCarAbo() {
        return formatExtNameCarAbo;
    }
    
    public void setFormatExtNameCarAbo( final String formatExtNameCarAbo ) {
        this.formatExtNameCarAbo = formatExtNameCarAbo;
    }
    
    public String getPathCarAboBkp() {
        return pathCarAboBkp;
    }
    
    public void setPathCarAboBkp( final String pathCarAboBkp ) {
        this.pathCarAboBkp = pathCarAboBkp;
    }
    
    public String getPathCarAboError() {
        return pathCarAboError;
    }
    
    public void setPathCarAboError( final String pathCarAboError ) {
        this.pathCarAboError = pathCarAboError;
    }
    
    public DataFTP getDataFTP() {
        return dataFTP;
    }
    
    public void setDataFTP( DataFTP dataFTP ) {
        this.dataFTP = dataFTP;
    }
    
    public String getPathSFTPEntrada() {
        return pathSFTPEntrada;
    }
    
    public void setPathSFTPEntrada( String pathSFTPEntrada ) {
        this.pathSFTPEntrada = pathSFTPEntrada;
    }
    
    @Override
    public String toString() {
        return "ParamUploadCargoAbonoDTO [pathCarAbo=" + pathCarAbo
                + ", formatFilenameCarAbo=" + formatFilenameCarAbo
                + ", formatExtNameCarAbo=" + formatExtNameCarAbo
                + ", pathCarAboBkp=" + pathCarAboBkp + ", pathCarAboError="
                + pathCarAboError + ", pathSFTPEntrada=" + pathSFTPEntrada
                + ", dataFTP=" + dataFTP + "]";
    }
    
}
