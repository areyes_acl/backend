package com.sbpay.sgi.cron.enums;

public enum MsgErrorSQL {
	ERROR_QUERY_TBL_LOG("Error al consulta la tabla "),
	ERROR_SAVE_TBL_LOG("Ha ocurrido un error al intentar guardar la tabla "),
	ERROR_UPDATE_TBL_LOG("Ha ocurrido un error al intentar actualizar la tabla "),
	ERROR_MAIL_PRTS("Error en la Base de Datos al Consultar los parametros del envio de Mail"), 
	ERROR_MAIL_CLOSE("Error en la Base de Datos al cerrar la conexion en la consulta de parametros de envio de Mail"), 
	ERROR_LOAD_PROCESS("Error en la Base de Datos al Procesar la Carga del Archivo INCOMING de Transbank"), 		
	ERROR_LOAD_CLOSE("Error en la Base de Datos al cerrar la conexion en la Carga del Archivo INCOMING de Transbank"), 
	ERROR_QUERY_PROCESS("Error en la Base de Datos al consultar la fecha de Carga del Archivo INCOMING de Transbank"), 
	ERROR_QUERY_PRTS("Error en la Base de Datos al consultar por los parametros del cron"), 
	ERROR_QUERY_PRTS_CLOSE("Error en la Base de Datos al cerrar la conexion en la consulta por los parametros del cron"), 
	ERROR_ROLLBACK("Error en la Base de Datos al realizar RollBack"), 
	ERROR_MAKE_A_ROLLBAK("Se ha provocado un error, por lo que se aplicara Rollback"),
	ERROR_FIND_INCOMING_BY_NAME("Error en la Base de Datos al buscar un archivo Incoming en la tabla LOG_INC"),
	ERROR_SAVE_INCOMING("Ha ocurrido un error al intentar guardar el incoming, por ende no se ha guardado ninguna de sus transacciones"),
	ERROR_QUERY_PRTS_UPLOAD("Error en la Base de Datos al consultar por los parametros del proceso Upload Incoming Transbank"),
	
	
	
	//VISA NACIONAL
	ERROR_QUERY_PROCESS_CLOSE_UPLOAD_IC_VISA_NAC("Error en la Base de Datos al cerrar la conexion en la validacion de fecha y si esta procesado el archivo Outgoing visa nacional"),
	ERROR_QUERY_PROCESS_UPLOAD_IC_VISA_NAC("Error en la Base de Datos al validar la fecha y si esta procesado el archivo Outgoing vis anacional"),
	ERROR_LOAD_PROCESS_VISA_NAC("Error en la Base de Datos al Procesar la Carga del Archivo INCOMING VISA NACIONAL"), 	
	ERROR_LOAD_PROCESS_CP_VISA_NAC("Error en la Base de Datos al Procesar la Carga del Archivo comision de pago visa nacional"), 
	ERROR_FIND_INCOMING_VISA_NAC_BY_NAME("Error en la Base de Datos al buscar un archivo Incoming visa nacional en la tabla TBL_LOG_VISA"),
	ERROR_QUERY_UP_OUT_PRTS_VISA_NAC("Error en la Base de Datos al consultar por los parametros del cron de Upload Outgoing"), 
	ERROR_QUERY_UP_OUT_PRTS_CLOSE_NAC_IC("Error en la Base de Datos al cerrar la conexion en la consulta por los parametros del cron de upload outgoing IC"), 
	ERROR_SAVE_INCOMING_VISA_NAC("Ha ocurrido un error al intentar guardar el incoming visa nacional, por ende no se ha guardado ninguna de sus transacciones"),
	
	//VISA INTERNACIONAL
	ERROR_QUERY_PROCESS_CLOSE_UPLOAD_IC_VISA_INT("Error en la Base de Datos al cerrar la conexion en la validacion de fecha y si esta procesado el archivo Outgoing visa internacional"),
	ERROR_QUERY_PROCESS_UPLOAD_IC_VISA_INT("Error en la Base de Datos al validar la fecha y si esta procesado el archivo Outgoing visa internacional"),
	ERROR_LOAD_PROCESS_VISA_INT("Error en la Base de Datos al Procesar la Carga del Archivo INCOMING VISA INTERNACIONAL"), 	
	ERROR_FIND_INCOMING_VISA_INT_BY_NAME("Error en la Base de Datos al buscar un archivo Incoming visa internacional en la tabla TBL_LOG_VISA_INT"),
	ERROR_QUERY_UP_OUT_PRTS_VISA_INT("Error en la Base de Datos al consultar por los parametros del cron de Upload Outgoing"), 
	ERROR_SAVE_INCOMING_VISA_INT("Ha ocurrido un error al intentar guardar el incoming visa internacional, por ende no se ha guardado ninguna de sus transacciones"),
	
	
	ERROR_QUERY_PRTS_UPLOAD_CNTBL("Error en la Base de Datos al consultar por los parametros del proceso Upload Asientos Contables Transbank"),
	ERROR_QUERY_PRTS_UPLOAD_CLOSE("Error en la Base de Datos al cerrar la conexion en la consulta de parametros del proceso Upload Incoming Transbank"), 
	ERROR_QUERY_PRTS_UPLOAD_CLOSE_CNTBL("Error en la Base de Datos al cerrar la conexion en la consulta de parametros del proceso Upload Asientos Contables"), 
	ERROR_MAIL_PRTS_UPLOAD("Error en la Base de Datos al Consultar los parametros del envio de Mail en el proceso Upload Incoming Transbank"), 
	ERROR_MAIL_CLOSE_UPLOAD("Error en la Base de Datos al cerrar la conexion en la consulta de parametros de envio de Mail en el proceso Upload Incoming Transbank"), 
	ERROR_MAIL_PARAMETERS_NOT_FOUND("No se han podido obtener todos los parametros para configuracion de envio de email"),
	ERROR_QUERY_PROCESS_UPLOAD("Error en la Base de Datos al validar la fecha y si esta procesado el archivo Incoming"), 
	
	ERROR_QUERY_PROCESS_UPLOAD_CNTBL("Error en la Base de Datos al validar la fecha y si esta procesado el archivo de Asientos Contables"),
	ERROR_QUERY_PROCESS_UPLOAD_IC("Error en la Base de Datos al validar la fecha y si esta procesado el archivo Outgoing tbk"),
	ERROR_QUERY_PROCESS_CLOSE_UPLOAD("Error en la Base de Datos al cerrar la conexion en la validacion de fecha y si esta procesado el archivo Incoming"), 
	ERROR_QUERY_PROCESS_CLOSE_UPLOAD_CNTBL("Error en la Base de Datos al cerrar la conexion en la validacion de fecha y si esta procesado el archivo de Asientos Contables"),
	ERROR_QUERY_PROCESS_CLOSE_UPLOAD_IC("Error en la Base de Datos al cerrar la conexion en la validacion de fecha y si esta procesado el archivo Outgoing Tbk"),
	ERROR_UPDATE_UPLOAD("Error en la Base de Datos al actualizar la subida del archivo incoming a transbank"), 
	ERROR_UPDATE_UPLOAD_TXT("Estimados:\n En el archivo nombrado al final no fue posible actualizarlo en la base de datos como subido a transbank "), 
	ERROR_UPDATE_INCOMING_UPLOAD("Error en la Base de datos el actualizar la transaccion de subida de archivos Incoming al FTP de transbank"), 
	ERROR_UPDATE_INCOMING_CLOSE_UPLOAD("Error en la Base de datos al cerrar la conexion de la transaccion de subida de archivos Incoming al FTP de transbank"), 
	//PARAM RCH
	ERROR_QUERY_RCH_PRTS("Error en la Base de Datos al consultar por los parametros del cron de Rechazos"), 
	ERROR_QUERY_RCH_PRTS_CLOSE("Error en la Base de Datos al cerrar la conexion en la consulta por los parametros del cron de Rechazos"), 
	ERROR_SAVE_RCH("Ha ocurrido un error al intentar guardar los rechazos, por ende no se ha guardado ninguna de sus transacciones"), 
	ERROR_QUERY_PROCESS_RCH("Error en la Base de Datos al validar si el archivo de rechazos esta validado"), 
	ERROR_QUERY_PROCESS_CLOSE_RCH("Error en la Base de Datos al cerrar la conexion en la validacion del archivo de rechazo"), 
	//PARAM ABO_CARG
	ERROR_QUERY_ABO_CARG_PRTS("Error en la Base de Datos al consultar por los parametros del cron para la generacion de cargo y abonos"), 
	ERROR_QUERY_ABO_CARG_PRTS_CLOSE("Error en la Base de Datos al cerrar la conexion en la consulta por los parametros para la generacion de cargo y abonos"),
	//PARAM UPLOAD OUTGOING IC
	ERROR_QUERY_UP_OUT_PRTS("Error en la Base de Datos al consultar por los parametros del cron de Upload Outgoing"), 
	ERROR_QUERY_UP_OUT_PRTS_CLOSE("Error en la Base de Datos al cerrar la conexion en la consulta por los parametros del cron de upload outgoing IC"), 
	ERROR_UPDATE_UPLOAD_IC("Error en la Base de Datos al actualizar la subida del archivo outgoing a IC"), 
	ERROR_UPDATE_UPLOAD_TXT_IC("Estimados:\n En el archivo nombrado al final no fue posible actualizarlo en la base de datos como subido a IC "),
	
	ERROR_CLOSE_CONECTION("Error al cerrar la conexión"),
	//PARAM UPLOAD CARGOS Y ABONOS
	ERROR_QUERY_PROCESS_UPLOAD_CAR_ABO("Error en la Base de Datos al validar la fecha y si esta procesado el archivo Cargo y Abonos"),
	ERROR_QUERY_PROCESS_CLOSE_UPLOAD_CAR_ABO("Error en la Base de Datos al cerrar la conexion en la validacion de fecha y si esta procesado el archivo de cargo y abono"),
	ERROR_UPDATE_UPLOAD_CAR_ABO("Error en la Base de Datos al actualizar la subida del archivo cargo y abono a IC"),
	ERROR_UPDATE_UPLOAD_TXT_CAR_ABO("Estimados:\n En el archivo nombrado al final no fue posible actualizarlo en la base de datos como subido como cargo y abono a IC "),
	ERROR_QUERY_PROCESS_CAR_ABO("Error en la Base de Datos al actualizar el estado del proceso del archivo cargo y abono"),
	ERROR_MAIL_PRTS_UPLOAD_CAR_ABO("Error en la Base de Datos al Consultar los parametros del envio de Mail en el proceso Upload Cargos y Abonos para IC"),
	ERROR_MAIL_CLOSE_UPLOAD_CAR_ABO("Error en la Base de Datos al cerrar la conexion en la consulta de parametros de envio de Mail en el proceso Upload Cargos y Abonos para IC"), 
	ERROR_QUERY_PRTS_UPLOAD_CAR_ABO("Error en la Base de Datos al consultar por los parametros del proceso Upload Cargos y Abonos a IC"), 
	ERROR_QUERY_PRTS_EXPORT_CNTBL("Error en la Base de Datos al consultar por los parametros del proceso Export Contable"), 
	ERROR_QUERY_PRTS_UPLOAD_CLOSE_CAR_ABO("Error en la Base de Datos al cerrar la conexion en la consulta de parametros del proceso Upload Cargos y Abonos IC"),
	//PARAM COMISIONES
	ERROR_FIND_COMISIONES_BY_NAME("Error en la Base de Datos al buscar un archivo Comisiones en la tabla TBL_CRON_COMISIONES"), 
	ERROR_FIND_CPAGO_BY_NAME("Error en la Base de Datos al buscar un archivo Compensaciones en la tabla TBL_CPAGO"), 
	ERROR_UPDATE_COMISION("Error en la Base de Datos al actualizar un archivo Comisiones en la tabla TBL_CRON_COMISIONES"),
	//PARAM ONUS
	ERROR_FIND_ONUS_BY_NAME("Error en la Base de Datos al buscar un archivo ONUS en la tabla TBL_CRON_ONUS"),
	// LOG TRX AUTORIZADAS
	ERROR_FIND_LOGTRXAUTOR("Error en la Base de Datos al buscar un archivo log de trx autorizadas en la tabla TBL_LOG_TRANSAC_AUTOR"),
	ERROR_CLOSE_LOGTRXAUTOR("Error al cerrar la conexion a la Base de Datos al buscar un archivo log de trx autorizadas en la tabla TBL_LOG_TRANSAC_AUTOR"),
	ERROR_SAVE_LOGTRXAUTOR("Ha ocurrido un error al intentar guardar el archivo log de transacciones autorizadas"),
	// PARAM GENERAR INCOMING ON US
	ERROR_DAO_ONUS_CLOSE("Error en la Base de Datos al cerrar la conexion en el proceso de Generar/Exportar Incoming On Us"),
	ERROR_SAVE_LOGONUS("Ha ocurrido un error al intentar guardar el archivo log de transacciones On Us"),
	ERROR_UPDATE_ONUS("Error en la Base de Datos al actualizar un archivo ONUS en la tabla TBL_LOG_ONUS"),
	ERROR_FIND_INCOMING_VISA_BY_NAME("Error en la Base de Datos al buscar un archivo Incoming en la tabla LOG_INC_VISA"),
	// ERROR CERRAR CONEXION 
	ERROR_GEN_INC_VISA_CLOSE("Error al cerrar la conexion a la Base de Datos, en el proceso de generacion de incoming VISA "),
	
	//ERROR BICE
	ERROR_LOAD_PROCESS_BICE("Error en la Base de Datos al Procesar la Carga del Archivo de avances de BICE"), 
	ERROR_FIND_BICE_BY_NAME("Error en la Base de Datos al buscar un archivo BICE en la tabla TBL_LOG_AVANCES_BICE"),
	ERROR_UPDATE_BICE("Error en la Base de Datos al actualizar un archivo AVACES BICE en la tabla TBL_LOG_AVANCES_BICE"),
	ERROR_SAVE_LOGBICE("Ha ocurrido un error al intentar guardar el archivo log de transacciones Avances Bice"),
	
	
	//ERROR AVANCES
	ERROR_LOAD_PROCESS_AVANCES("Error en la Base de Datos al Procesar la Carga del Archivo de Avances"), 
	ERROR_FIND_AVANCES_BY_NAME("Error en la Base de Datos al buscar un archivo Avances en la tabla TBL_LOG_AVANCES"),
	ERROR_UPDATE_AVANCES("Error en la Base de Datos al actualizar un archivo Avances en la tabla TBL_LOG_AVANCES"),
	ERROR_SAVE_LOGAVANCES("Ha ocurrido un error al intentar guardar el archivo log de transacciones Avances"),
	
	//ERROR CONCILIAR
	ERROR_SAVE_CONCILIAR("Ha ocurrido un error al intentar guardar la conciliacion"),


	;
	/**
	 * Valor ENUM.
	 */
	private final String text;

	/**
	 * Constructor.
	 * 
	 * @param text
	 */
	private MsgErrorSQL(final String text) {
		this.text = text;
	}

	/**
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return text;
	}
}
