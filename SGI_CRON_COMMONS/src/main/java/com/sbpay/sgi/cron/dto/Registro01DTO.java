package com.sbpay.sgi.cron.dto;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 18/11/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class Registro01DTO extends RegistroDTO {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1293881323131051551L;
    private String chargRefN;
    private String documInd;
    private String mensaje;
    private String speCondInd;
    private String cardAcceptorId;
    private String terminalId;
    private String valorCuota;
    private String indicatorTransaction;
    private String comisionCic;
    private String cardHolder;
    private String prepairCardIn;
    private String authSourceCode;
    private String atmAccSelec;
    private String instalPayCount;
    private String itemDescrptor;
    private String flagDiferido;
    private String flagMesDeGracia;
    private String periodosDeGracia;
    private String periodosDeDiferido;
    private String flagPromoEmisora;
    private String origen;
    private String rubroTransbank;
    private String tasaEECC;
    private String cashBack;
    
    public String getChargRefN() {
        return chargRefN;
    }
    
    public String getMensaje() {
        return mensaje;
    }
    
    public void setMensaje( String mensaje ) {
        this.mensaje = mensaje;
    }
    
    public String getSpeCondInd() {
        return speCondInd;
    }
    
    public void setSpeCondInd( String speCondInd ) {
        this.speCondInd = speCondInd;
    }
    
    public void setChargRefN( String chargRefN ) {
        this.chargRefN = chargRefN;
    }
    
    public String getDocumInd() {
        return documInd;
    }
    
    public void setDocumInd( String documInd ) {
        this.documInd = documInd;
    }
    
    public String getCardAcceptorId() {
        return cardAcceptorId;
    }
    
    public void setCardAcceptorId( String cardAcceptorId ) {
        this.cardAcceptorId = cardAcceptorId;
    }
    
    public String getTerminalId() {
        return terminalId;
    }
    
    public void setTerminalId( String terminalId ) {
        this.terminalId = terminalId;
    }
    
    public String getValorCuota() {
        return valorCuota;
    }
    
    public void setValorCuota( String valorCuota ) {
        this.valorCuota = valorCuota;
    }
    
    public String getIndicatorTransaction() {
        return indicatorTransaction;
    }
    
    public void setIndicatorTransaction( String indicatorTransaction ) {
        this.indicatorTransaction = indicatorTransaction;
    }
    
    public String getComisionCic() {
        return comisionCic;
    }
    
    public void setComisionCic( String comisionCic ) {
        this.comisionCic = comisionCic;
    }
    
    public String getCardHolder() {
        return cardHolder;
    }
    
    public void setCardHolder( String cardHolder ) {
        this.cardHolder = cardHolder;
    }
    
    public String getPrepairCardIn() {
        return prepairCardIn;
    }
    
    public void setPrepairCardIn( String prepairCardIn ) {
        this.prepairCardIn = prepairCardIn;
    }
    
    public String getAuthSourceCode() {
        return authSourceCode;
    }
    
    public void setAuthSourceCode( String authSourceCode ) {
        this.authSourceCode = authSourceCode;
    }
    
    public String getAtmAccSelec() {
        return atmAccSelec;
    }
    
    public void setAtmAccSelec( String atmAccSelec ) {
        this.atmAccSelec = atmAccSelec;
    }
    
    public String getInstalPayCount() {
        return instalPayCount;
    }
    
    public void setInstalPayCount( String instalPayCount ) {
        this.instalPayCount = instalPayCount;
    }
    
    public String getItemDescrptor() {
        return itemDescrptor;
    }
    
    public void setItemDescrptor( String itemDescrptor ) {
        this.itemDescrptor = itemDescrptor;
    }
    
    public String getFlagDiferido() {
        return flagDiferido;
    }
    
    public void setFlagDiferido( String flagDiferido ) {
        this.flagDiferido = flagDiferido;
    }
    
    public String getFlagMesDeGracia() {
        return flagMesDeGracia;
    }
    
    public void setFlagMesDeGracia( String flagMesDeGracia ) {
        this.flagMesDeGracia = flagMesDeGracia;
    }
    
    public String getPeriodosDeGracia() {
        return periodosDeGracia;
    }
    
    public void setPeriodosDeGracia( String periodosDeGracia ) {
        this.periodosDeGracia = periodosDeGracia;
    }
    
    public String getPeriodosDeDiferido() {
        return periodosDeDiferido;
    }
    
    public void setPeriodosDeDiferido( String periodosDeDiferido ) {
        this.periodosDeDiferido = periodosDeDiferido;
    }
    
    public String getFlagPromoEmisora() {
        return flagPromoEmisora;
    }
    
    public void setFlagPromoEmisora( String flagPromoEmisora ) {
        this.flagPromoEmisora = flagPromoEmisora;
    }
    
    public String getOrigen() {
        return origen;
    }
    
    public void setOrigen( String origen ) {
        this.origen = origen;
    }
    
    public String getRubroTransbank() {
        return rubroTransbank;
    }
    
    public void setRubroTransbank( String rubroTransbank ) {
        this.rubroTransbank = rubroTransbank;
    }
    
    public String getTasaEECC() {
        return tasaEECC;
    }
    
    public void setTasaEECC( String tasaEECC ) {
        this.tasaEECC = tasaEECC;
    }
    
    public String getCashBack() {
        return cashBack;
    }
    
    public void setCashBack( String cashBack ) {
        this.cashBack = cashBack;
    }
    
    public String getP0165() {
        StringBuffer str = new StringBuffer();
        str.append( this.flagDiferido ).append( this.flagMesDeGracia )
                .append( this.periodosDeGracia )
                .append( this.periodosDeDiferido )
                .append( this.flagPromoEmisora );
        return str.toString();
    }
    
    public void setP0165( String str ) {
    	if(str != null){
    		this.flagDiferido = str.substring( 0, 1 );
            this.flagMesDeGracia = str.substring( 1, 2 );
            this.periodosDeGracia = str.substring( 2, 3 );
            this.periodosDeDiferido = str.substring( 3, 4 );
            this.flagPromoEmisora = str.substring( 4, 5 );
    	}
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 9/12/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @param indicadorLiquidacion
     * @since 1.X
     */
    public void setIndicadorLiquidacion( String indicadorLiquidacion ) {
    	if(indicadorLiquidacion != null){
            this.flagDiferido = indicadorLiquidacion.substring( 0, 1 );
            this.flagMesDeGracia = indicadorLiquidacion.substring( 1, 2 );
            this.periodosDeGracia = indicadorLiquidacion.substring( 2, 3 );
            this.periodosDeDiferido = indicadorLiquidacion.substring( 3, 4 );
            this.flagPromoEmisora = indicadorLiquidacion.substring( 4, 5 );	
    	}
    }
    
    public String toStringLengths() {
        return "Registro01DTO [chargRefN="
                + chargRefN.length()
                + ", documInd="
                + documInd.length()
                + ", mensaje="
                + ( ( mensaje != null ? mensaje.length() : null ) )
                + ", speCondInd="
                + ( ( speCondInd != null ? speCondInd.length() : null ) )
                + ", cardAcceptorId="
                + ( ( cardAcceptorId != null ? cardAcceptorId.length() : null ) )
                + ", terminalId="
                + terminalId.length()
                + ", valorCuota="
                + valorCuota.length()
                + ", indicatorTransaction="
                + indicatorTransaction.length()
                + ", comisionCic="
                + comisionCic.length()
                + ", cardHolder="
                + ( ( cardHolder != null ? cardHolder.length() : null ) )
                + ", prepairCardIn="
                + ( ( prepairCardIn != null ? prepairCardIn.length() : null ) )
                + ", authSourceCode="
                + ( ( authSourceCode != null ) ? authSourceCode.length() : null )
                + ", atmAccSelec="
                + ( ( atmAccSelec != null ) ? atmAccSelec.length() : null )
                + ", instalPayCount="
                + ( ( instalPayCount != null ) ? instalPayCount.length() : null )
                + ", itemDescrptor="
                + ( ( itemDescrptor != null ) ? itemDescrptor.length() : null )
                + ", flagDiferido=" + flagDiferido.length()
                + ", flagMesDeGracia=" + flagMesDeGracia.length()
                + ", periodosDeGracia=" + periodosDeGracia.length()
                + ", periodosDeDiferido=" + periodosDeDiferido.length()
                + ", flagPromoEmisora=" + flagPromoEmisora.length()
                + ", origen=" + origen.length() + ", rubroTransbank="
                + rubroTransbank.length() + ", tasaEECC=" + tasaEECC.length()
                + ", cashBack="
                + ( ( cashBack != null ) ? cashBack.length() : null )
                + ", getP0165()=" + getP0165() + "]";
    }
    
    @Override
    public String toString() {
        return "Registro01DTO [chargRefN=" + chargRefN + ", documInd="
                + documInd + ", mensaje=" + mensaje + ", speCondInd="
                + speCondInd + ", cardAcceptorId=" + cardAcceptorId
                + ", terminalId=" + terminalId + ", valorCuota=" + valorCuota
                + ", indicatorTransaction=" + indicatorTransaction
                + ", comisionCic=" + comisionCic + ", cardHolder=" + cardHolder
                + ", prepairCardIn=" + prepairCardIn + ", authSourceCode="
                + authSourceCode + ", atmAccSelec=" + atmAccSelec
                + ", instalPayCount=" + instalPayCount + ", itemDescrptor="
                + itemDescrptor + ", flagDiferido=" + flagDiferido
                + ", flagMesDeGracia=" + flagMesDeGracia
                + ", periodosDeGracia=" + periodosDeGracia
                + ", periodosDeDiferido=" + periodosDeDiferido
                + ", flagPromoEmisora=" + flagPromoEmisora + ", origen="
                + origen + ", rubroTransbank=" + rubroTransbank + ", tasaEECC="
                + tasaEECC + ", cashBack=" + cashBack + "]";
    }
    
}
