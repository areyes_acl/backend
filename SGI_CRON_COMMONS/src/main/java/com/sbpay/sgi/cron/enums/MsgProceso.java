package com.sbpay.sgi.cron.enums;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 12/02/2016, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public enum MsgProceso {
	
    MSG_PROCESO_DESACTIVADO_ASUNTO("El proceso :?: se encuentra desactivado."),
    MSG_PROCESO_DESACTIVADO_BODY("El proceso :?: se encuentra desactivado por base de datos, por lo tanto no se ejecutara."),
    MSG_CAMBIAR_CONFIG("Para activar/desactivar el proceso, se debe dirigir a la tabla TBL_CRON_CONFIG."),
    MSG_ERROR_FALTAN_PARAMETROS("Faltan parametros de la tabla TBL_PRTS necesarios para la ejecución del proceso."),
    MSG_GENERIC_ERROR_MSJ("Ha ocurrido un problema al ejecutar el proceso : "),
    ;
    
    /**
     * Valor ENUM.
     */
    private final String text;
    
    /**
     * Constructor.
     * 
     * @param text
     */
    private MsgProceso( final String text ) {
        this.text = text;
    }
    
    /**
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
}
