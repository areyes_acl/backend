package com.sbpay.sgi.cron.utils.file;

import java.io.BufferedWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dto.IncomingDTO;
import com.sbpay.sgi.cron.dto.Registro00DTO;
import com.sbpay.sgi.cron.dto.Registro01DTO;
import com.sbpay.sgi.cron.dto.Registro05DTO;
import com.sbpay.sgi.cron.dto.Registro07DTO;
import com.sbpay.sgi.cron.dto.Registro1020DTO;
import com.sbpay.sgi.cron.dto.RegistroDTO;
import com.sbpay.sgi.cron.dto.Trailer;
import com.sbpay.sgi.cron.dto.Transaccion;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.MsgGenerationIncoming;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;
import com.sbpay.sgi.cron.utils.generals.DateUtils;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 10/12/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class IncomingExportVisaNac implements IncomingFileUtils {
    
    /** VARIABLE PARA EL LOGER */
    private static final Logger LOGGER = Logger
            .getLogger( IncomingExportVisaNac.class );
    
    private static final String INCOMING_HEADER_NUMBER = "90";
    private static final String INCOMING_BATCH_NUMBER = "91";
    private static final String INCOMING_FILE_NUMBER = "92";
    private static final String INCOMING_REGISTER_00 = "00";
    private static final String INCOMING_REGISTER_01 = "01";
    private static final String INCOMING_REGISTER_05 = "05";
    private static final String INCOMING_REGISTER_07 = "07";
    private static final String INCOMING_PETICION_VALE = "52";
    private static final String INCOMING_CODIGO_CONTRACARGO = "15";
    private String visaNumber;
    private static final String INCOMING_MONETARY_TRANSACTION = "2";
    private static final String[] CODIGOS_TRANSAC_MONETARY = { "15_00" };
    
    // TOTAL DE ARCHIVOS SIN FOOTER NI HEADER
    private int totalRegistrosArchivo = 0;
    private int totalTransaccionesMonetarias = 0;
    
    public IncomingExportVisaNac(String binOperador) {
    	visaNumber = binOperador;
	}

	/**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 10/12/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @param ruta
     * @param starWith
     * @return
     * @throws AppException
     * @see com.sbpay.sgi.cron.utils.file.IncomingFileUtils#readIncomingFile(java.lang.String,
     *      java.lang.String)
     * @since 1.X
     */
    @Override
    public IncomingDTO readIncomingFile( String ruta, String starWith )
            throws AppException {
        // TODO Auto-generated method stub
        return null;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 10/12/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @param file
     * @param starWith
     * @return
     * @throws AppException
     * @see com.sbpay.sgi.cron.utils.file.IncomingFileUtils#readIncomingFile(java.io.File,
     *      java.lang.String)
     * @since 1.X
     */
    @Override
    public IncomingDTO readIncomingFile( File file, String starWith )
            throws AppException {
        // TODO Auto-generated method stub
        return null;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 10/12/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @param ruta
     * @return
     * @throws AppException
     * @throws IOException
     * @see com.sbpay.sgi.cron.utils.file.IncomingFileUtils#exportIncomingFile(java.lang.String)
     * @since 1.X
     */
    @Override
    public void exportIncomingFile( String ruta, String incomingFilename,
            String controlFilename, List<Transaccion> listaTrasacciones )
            throws AppException {
        
        String rutaFile = ruta.concat( incomingFilename );
        File incomingFile = new File( rutaFile );
        incomingFile.setExecutable( true );
        incomingFile.setReadable( true );
        incomingFile.setWritable( true );
        
        if ( incomingFile.exists() ) {
            throw new AppException(
                    MsgGenerationIncoming.ERROR_INCOMING_ALREADY_EXIST
                            .toString() );
        }
        
        try {
            generaArchivoIncoming( incomingFile, listaTrasacciones );
            generaArchivoControl( ruta, controlFilename );
        }
        catch ( IOException e ) {
            throw new AppException(
                    MsgGenerationIncoming.ERROR_CREATE_INCOMING_FILE.toString(),
                    e );
        }
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 11/12/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @param controlFilename
     * @param ruta
     * @throws IOException
     * 
     * @since 1.X
     */
    private void generaArchivoControl( String ruta, String controlFilename )
            throws IOException {
        LOGGER.info( "=========> SE GENERA ARCHIVO DE CONTROL <==========" );
        String pathControlFile = ruta.concat( controlFilename );
        File controlFile = new File( pathControlFile );
        BufferedWriter bw = new BufferedWriter( new FileWriter( controlFile ) );
        bw.close();
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 10/12/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * Metodo que reccorre las transacciones y llama al escribe en
     * archivo incoming para generar el archivo
     * 
     * @param incomingFile
     * @param incomingFilename
     * @param listaTrasacciones
     * @throws AppException
     * @throws IOException
     * @since 1.X
     */
    private void generaArchivoIncoming( File incomingFile,
            List<Transaccion> listaTransacciones ) throws AppException,
            IOException {
        LOGGER.info( "=========> SE GENERA ARCHIVO INCOMING <==========" );
        StringBuilder contenido = new StringBuilder();
        
        // SE OBTIENE EL HEADER
        String header = generateIncomingHeader();
        
        LOGGER.info( "TOTAL DE TRANSACCIONES EN EL ARCHIVO: "
                + listaTransacciones.size() );
        
        // GENERA EL CONTENIDO DE TRANSACCIONES
        for ( int i = 0; i < listaTransacciones.size(); i++ ) {
            contenido.append( getTransactionsToString(
                    listaTransacciones.get( i ), listaTransacciones.size() ) );
            // SI ES EL ULTIMO REGISTRO NO SE HACE SALTO DE LINEA
            if ( ( i + 1 ) == listaTransacciones.size() ) {
                contenido.replace( contenido.length() - 1, contenido.length(),
                        ConstantesUtil.EMPTY.toString() );
            }
        }
        
        // UNA VEZ GENERADO EL CONTENIDO DEL ARCHIVO SE CUENTA EL
        // TOTAL DE REGISTROS
        totalRegistrosArchivo = CommonsUtils.countLines( contenido.toString() );
        
        // CUENTA EL NUMERO DE TRANSACCIONES MONETARIAS DEL BATCH
        totalTransaccionesMonetarias = getNumeroDeTransaccionesMonetarias( listaTransacciones );
        
        // ESCRIBE EL FOOTER
        String footer = generateTotalRegister( listaTransacciones );
        
        // ESCRIBE EN ARCHIVO INCOMING
        writeIncomingFile( header, contenido.toString(), footer, incomingFile );
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 11/12/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    private String generateTotalRegister( List<Transaccion> listaTransacciones ) {
        StringBuilder str = new StringBuilder();
        str.append( generateBatchTrailer( listaTransacciones ) );
        str.append( ConstantesUtil.SKIP_LINE.toString() );
        str.append( generateFileTrailer( listaTransacciones ) );
        return str.toString();
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 11/12/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * Metodo encargado de escribir toda una transaccion es decir los
     * registro 00, 01, 05 ,07
     * 
     * @param contenido
     * @param transaccion
     * @since 1.X
     */
    private String getTransactionsToString( Transaccion transaccion,
            Integer cantidadTransacciones ) {
        StringBuilder registro = new StringBuilder();
        
     // Si es 10 o 20
        if ( transaccion.getRegistro1020() != null ) {
            registro.append( retrieveRegisterFor1020( ( Registro1020DTO ) transaccion.getRegistro1020() ) );
            
            if ( cantidadTransacciones > 1 ) {
                registro.append( ConstantesUtil.SKIP_LINE.toString() );
            }
            
        } else // STRING QUE CONTIENE LA PETICION DE VALE
        if ( INCOMING_PETICION_VALE
                .equalsIgnoreCase( ( ( Registro00DTO ) transaccion
                        .getRegistro00() ).getCodigoTransaccion() )
                && INCOMING_REGISTER_00.equalsIgnoreCase( transaccion
                        .getRegistro00().getTransCode() ) ) {
            registro.append( retrieveRegisterForPeticionVale( ( Registro00DTO ) transaccion
                    .getRegistro00() ) );
            // SI EXISTEN MAS DE UNA TRANSACCION REALIZA EL SALTO
            // LINEA
            //LOGGER.info( "REALIZA SALTO DE LINEA PARA PETICION DE VALE? : "
            //        + ( cantidadTransacciones > 1 ) );
            if ( cantidadTransacciones > 1 ) {
                registro.append( ConstantesUtil.SKIP_LINE.toString() );
            }
            
        }
        // STRING QUE CONTIENE LOS CONTRACARGOS
        else {
            if ( INCOMING_REGISTER_00.equalsIgnoreCase( transaccion
                    .getRegistro00().getTransCode() ) ) {
                registro.append( getRegister00( ( Registro00DTO ) transaccion
                        .getRegistro00() ) );
                registro.append( ConstantesUtil.SKIP_LINE.toString() );
            }
            if ( INCOMING_REGISTER_01.equalsIgnoreCase( transaccion
                    .getRegistro01().getTransCode() ) ) {
                
                registro.append( getRegister01( ( Registro01DTO ) transaccion
                        .getRegistro01() ) );
                registro.append( ConstantesUtil.SKIP_LINE.toString() );
            }
            if ( transaccion.getRegistro05() != null
                    && INCOMING_REGISTER_05.equalsIgnoreCase( transaccion
                            .getRegistro05().getTransCode() ) ) {
                registro.append( getRegister05( ( Registro05DTO ) transaccion
                        .getRegistro05() ) );
                registro.append( ConstantesUtil.SKIP_LINE.toString() );
            }
            if ( transaccion.getRegistro07() != null
                    && INCOMING_REGISTER_07.equalsIgnoreCase( transaccion
                            .getRegistro07().getTransCode() ) ) {
                registro.append( getRegister07( ( Registro07DTO ) transaccion
                        .getRegistro07() ) );
                registro.append( ConstantesUtil.SKIP_LINE.toString() );
            }
        }
        
        return registro.toString();
    }
    
    private Object retrieveRegisterFor1020( Registro1020DTO registro00 ) {
        StringBuilder str = new StringBuilder();
        String mit = registro00.getMit()+"  ";
        String mensaje = registro00.getMensaje();
        String codigoRazon = registro00.getCodrazon();
        String lineaOrigen = registro00.getDatosAdicionales1().substring(0, 168);
        mensaje = CommonsUtils.ajustCadena(mensaje, " ", 70);
        String seccion1 = mit+lineaOrigen.substring(2, 16) + codigoRazon + lineaOrigen.substring(20, 76) + mensaje + lineaOrigen.substring(146, 168);
        //LOGGER.info("linea de salida de 10-20: "+seccion1);
        str.append( seccion1 );
        return str.toString();
    }

    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 15/12/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * OBTIENE LA PETICION DE VALE SEGUN LA LOGICA QUE NOS INDICA
     * 
     * @param registro00
     * @return
     * @since 1.X
     */
    private Object retrieveRegisterForPeticionVale( Registro00DTO registro00 ) {
        StringBuilder str = new StringBuilder();
        str.append( registro00.getCodigoTransaccion() );
        str.append( CommonsUtils.getWhitesSpaceString( 2 ) );
        str.append( ( ( RegistroDTO ) registro00 ).getTransCode() );
        str.append( registro00.getNumeroTarjeta() );
        str.append( CommonsUtils.getZeroString( 3 ) );
        str.append( registro00.getNumeroReferencia() );
        str.append( registro00.getAcqMembId() );
        str.append( DateUtils.transformDatefromYYMMDDToFormatMMDD( registro00
                .getFechaComp().substring( 0, 6 ) ) );
        str.append( registro00.getMontoFuen() );
        str.append( registro00.getMonedaFuen() );
        // ES DE LARGO 19 Y SE LE SUMAN ESPACIOS PARA LLEGAR A 25
        str.append( registro00.getNombComer().concat(
                CommonsUtils.getWhitesSpaceString( 6 ) ) );
        // SE LE AGREGA UN ESPACIO AL CUIDAD POR QUE ES DE LARGO 13
        str.append( registro00.getCiudadComer().concat(
                CommonsUtils.getWhitesSpaceString( 1 ) ) );
        str.append( registro00.getPais() );
        str.append( registro00.getRubroComer() );
        str.append( CommonsUtils.getZeroString( 5 ) );
        str.append( CommonsUtils.getWhitesSpaceString( 3 ) );
        str.append( DateUtils.getTodayInYYMMDDFormat()
                + CommonsUtils.paddingZeroToNumber(
                        Integer.valueOf( registro00.getNumeroMensaje() ), 3 ) );
        str.append( registro00.getCodRazon() );
        str.append( registro00.getSettleFlag() );
        str.append( CommonsUtils.getZeroString( 12 ) );
        str.append( CommonsUtils.getWhitesSpaceString( 1 ) );
        str.append( CommonsUtils.getZeroString( 12 ) );
        String fechaJuliana = DateUtils.getTodayStringDateInJulianFormat();
        str.append( fechaJuliana.substring( 1, fechaJuliana.length() ) );
        str.append( CommonsUtils.getZeroString( 1 ) );
       
        
        return str.toString();
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 11/12/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * Metodo que crea el archivo en disco y escribe todo el contenido
     * de este.
     * 
     * @param footer
     * @param contenido
     * @param header
     * @param incomingFile
     * @param incomingFilename
     * @throws IOException
     * 
     * @since 1.X
     */
    private void writeIncomingFile( String header, String contenido,
            String footer, File incomingFile ) throws IOException {
        BufferedWriter bw = null;
        bw = new BufferedWriter( new FileWriter( incomingFile ) );
        bw.write( header );
        if ( contenido != null && contenido.length() > 0 ) {
            bw.write( ConstantesUtil.SKIP_LINE.toString() );
            bw.write( contenido );
        }
        bw.write( ConstantesUtil.SKIP_LINE.toString() );
        bw.write( footer );
        bw.close();
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 - 7/12/2015, (ACL) - versión inicial
     * </ul>
     * <p>
     * 
     * @param registro00
     * @return
     * @since 1.X
     */
    private String getRegister00( Registro00DTO registro00 ) {
        StringBuilder str = new StringBuilder();
        str.append( registro00.getCodigoTransaccion() );
        str.append( CommonsUtils.getWhitesSpaceString( 2 ) );
        str.append( ( ( RegistroDTO ) registro00 ).getTransCode() );
        str.append( registro00.getNumeroTarjeta() );
        str.append( CommonsUtils.getZeroString( 3 ) );
        str.append( CommonsUtils.getWhitesSpaceString( 3 ) );
        str.append( registro00.getNumeroReferencia() );
        str.append( registro00.getAcqMembId() );
        String fechaMMDD = DateUtils
                .transformDatefromYYMMDDToFormatMMDD( registro00.getFechaComp()
                        .substring( 0, 6 ) );
        str.append( fechaMMDD );
        str.append( CommonsUtils.getZeroString( 12 ) );
        str.append( CommonsUtils.getWhitesSpaceString( 3 ) );
        str.append( registro00.getMontoFuen() );
        str.append( registro00.getMonedaFuen() );
        str.append( registro00.getNombComer() );
        str.append( DateUtils.transformDatefromYYMMDDToFormatMMDD( registro00
                .getFechaDCompensacion() ) );
        str.append( registro00.getOfic() );
        str.append( registro00.getCiudadComer() );
        str.append( CommonsUtils.getZeroString( 1 ) );
        str.append( registro00.getPais() );
        str.append( registro00.getRubroComer() );
        str.append( registro00.getZipCodeComer() );
        str.append( CommonsUtils.getWhitesSpaceString( 3 ) );
        str.append( registro00.getCodMunic() );
        str.append( registro00.getUsagCode() );
        str.append( CommonsUtils.paddingZeroToNumber(
                Integer.valueOf( registro00.getCodRazon() ), 2 ) );
        str.append( registro00.getSettleFlag() );
        str.append( CommonsUtils.getWhitesSpaceString( 1 ) );
        str.append( registro00.getCodAutor() );
        str.append( registro00.getPosTerminalCapability() );
        str.append( INCOMING_MONETARY_TRANSACTION );
        str.append( registro00.getCardHolderIdMethod() );
        str.append( CommonsUtils.getWhitesSpaceString( 1 ) );
        str.append( registro00.getPosEntryMode() );
        str.append( registro00.getFechaProc() );
        str.append( CommonsUtils.getZeroString( 1 ) );
                
        return str.toString();
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 - 7/12/2015, (ACL) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    private String getRegister01( Registro01DTO registro01 ) {
        
        StringBuilder str = new StringBuilder();
        str.append( registro01.getTipoTransaccion() );
        str.append( CommonsUtils.getWhitesSpaceString( 2 ) );
        str.append( ( ( RegistroDTO ) registro01 ).getTransCode() );
        str.append( CommonsUtils.getWhitesSpaceString( 12 ) );
        str.append( registro01.getChargRefN() );
        str.append( registro01.getDocumInd() );
        str.append( CommonsUtils.getWhitesSpaceString( 50 ) );
        str.append( CommonsUtils.getWhitesSpaceString( 2 ) );
        str.append( CommonsUtils.getWhitesSpaceString( 5 ) );
        str.append( registro01.getCardAcceptorId() );
        str.append( registro01.getTerminalId() );
        str.append( registro01.getValorCuota() );
        str.append( registro01.getIndicatorTransaction() );
        str.append( CommonsUtils.getWhitesSpaceString( 1 ) );
        str.append( registro01.getComisionCic() );
        str.append( CommonsUtils.getWhitesSpaceString( 1 ) );
        str.append( CommonsUtils.getWhitesSpaceString( 1 ) );
        str.append( CommonsUtils.getWhitesSpaceString( 2 ) );
        str.append( CommonsUtils.getWhitesSpaceString( 1 ) );
        str.append( CommonsUtils.getWhitesSpaceString( 1 ) );
        str.append( CommonsUtils.getWhitesSpaceString( 1 ) );
        str.append( CommonsUtils.getWhitesSpaceString( 2 ) );
        str.append( CommonsUtils.getWhitesSpaceString( 10 ) );
        str.append( registro01.getFlagDiferido() );
        str.append( registro01.getFlagMesDeGracia() );
        str.append( registro01.getPeriodosDeGracia() );
        str.append( registro01.getPeriodosDeDiferido() );
        str.append( registro01.getFlagPromoEmisora() );
        str.append( registro01.getOrigen() );
        str.append( registro01.getRubroTransbank() );
        str.append( registro01.getTasaEECC() );
        str.append( CommonsUtils.getZeroString( 9 ) );
        str.append( CommonsUtils.getWhitesSpaceString( 2 ) );
        
        return str.toString();
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 - 7/12/2015, (ACL) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    private String getRegister05( Registro05DTO registro05 ) {
        StringBuilder str = new StringBuilder();
        
        // SE REEMPLAZA POR EL NUMERO QUE INDICA CONTRACARGOS QUE ES
        // EL 15
        if ( ( ( RegistroDTO ) registro05 ).getTipoTransaccion()
                .equalsIgnoreCase( INCOMING_REGISTER_05 ) ) {
            str.append( INCOMING_CODIGO_CONTRACARGO );
        }
        str.append( CommonsUtils.getWhitesSpaceString( 2 ) );
        str.append( registro05.getTransCode() );
        str.append( registro05.getTransId() );
        str.append( registro05.getAuthAmt() );
        str.append( registro05.getMoneda() );
        str.append( registro05.getAuthRespCd() );
        str.append( registro05.getValCode() );
        str.append( registro05.getExclTranIdRsn() );
        str.append( registro05.getCrsProcngCd() );
        str.append( registro05.getChrgbkCondCd() );
        str.append( registro05.getMultClearSeqNbr() );
        str.append( registro05.getMultClearSeqCnt() );
        str.append( registro05.getMktAuthDataInd() );
        str.append( registro05.getTotAuthAmt() );
        str.append( registro05.getInfoInd() );
        str.append( registro05.getMerPhone() );
        str.append( registro05.getAddtlDataInd() );
        str.append( registro05.getMerchVolInd() );
        str.append( registro05.getElecGoodInd() );
        str.append( registro05.getMerchVerValue() );
        str.append( registro05.getInterFeeAmt() );
        str.append( registro05.getInterFeeSign() );
        str.append( registro05.getSourcurrExchRate() );
        str.append( registro05.getBasecurrExchRate() );
        str.append( registro05.getOptIsaAmt() );
        str.append( registro05.getProdId() );
        str.append( registro05.getProgId() );
        str.append( registro05.getReserved() );
        str.append( registro05.getResultCode() );
        
        return str.toString();
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 - 7/12/2015, (ACL) - versión inicial
     * </ul>
     * <p>
     * 
     * @param registro07
     * @return
     * @since 1.X
     */
    private String getRegister07( Registro07DTO registro07 ) {
        StringBuilder str = new StringBuilder();
        
        // SI ES 05 SE CAMBIA POR EL CODIGO DE UN CONTRACARGO
        if ( ( ( RegistroDTO ) registro07 ).getTipoTransaccion()
                .equalsIgnoreCase( INCOMING_REGISTER_05 ) ) {
            str.append( INCOMING_CODIGO_CONTRACARGO );
        }
        str.append( CommonsUtils.getWhitesSpaceString( 2 ) );
        str.append( registro07.getTransCode() );
        str.append( registro07.getTransType() );
        str.append( registro07.getCardSeqNbr() );
        str.append( registro07.getTermTrnDate() );
        str.append( registro07.getTermCapProf() );
        str.append( registro07.getTermCounCode() );
        str.append( registro07.getTermSerNbr() );
        str.append( registro07.getUnpredNumber() );
        str.append( registro07.getAppTrnCount() );
        str.append( registro07.getAppInterProf() );
        str.append( registro07.getCrypt() );
        str.append( registro07.getIssAppDataB2() );
        str.append( registro07.getIssAppDataB3() );
        str.append( registro07.getTermVerRsult() );
        str.append( registro07.getIssAppDataB4() );
        str.append( registro07.getCryptAmt() );
        str.append( registro07.getIssAppDataB8() );
        str.append( registro07.getIssAppDataB9() );
        str.append( registro07.getIssAppDataB1() );
        str.append( registro07.getIssAppDataB17() );
        str.append( registro07.getIssAppDataB18() );
        str.append( registro07.getFormFactInd() );
        str.append( registro07.getIssRsult() );
                
        return str.toString();
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 - 9/12/2015, (ACL) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    private String generateIncomingHeader() {
        StringBuilder str = new StringBuilder();
        str.append( INCOMING_HEADER_NUMBER );
        str.append( CommonsUtils.getWhitesSpaceString( 2 ) );
        str.append( CommonsUtils.getWhitesSpaceString( 6 ) );
        str.append( DateUtils.getTodayStringDateInJulianFormat() );
        str.append( CommonsUtils.getWhitesSpaceString( 16 ) );
        str.append( CommonsUtils.getWhitesSpaceString( 4 ) );
        str.append( CommonsUtils.getWhitesSpaceString( 29 ) );
        str.append( CommonsUtils.getWhitesSpaceString( 14 ) );
        str.append( CommonsUtils.getZeroString( 3 ) );
        str.append( CommonsUtils.getWhitesSpaceString( 89 ) );
        return str.toString();
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 14/12/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @param listaTransacciones
     * @return
     * @since 1.X
     */
    private String getMontoDeTransaccionesMonetarias(
            List<Transaccion> listaTransacciones ) {
        Integer sumaMonto = 0;
        for ( Transaccion transaccion : listaTransacciones ) {
            String codigo = ( ( Registro00DTO ) transaccion.getRegistro00() )
                    .getCodigoTransaccion()
                    + "_"
                    + transaccion.getRegistro00().getTransCode();
            
            if ( Arrays.asList( CODIGOS_TRANSAC_MONETARY ).contains( codigo ) ) {
                String montoTransaccion = ( ( Registro00DTO ) transaccion
                        .getRegistro00() ).getMontoFuen();
                sumaMonto = sumaMonto
                        + Integer.valueOf( montoTransaccion.substring( 0,
                                montoTransaccion.length() - 2 ) );
            }
        }
        
        // SE DEBE MULTIPLICAR * 100 ANTES DE LA SUMA
        sumaMonto = sumaMonto * 100;
        
        return CommonsUtils.paddingZeroToNumber( sumaMonto, 15 );
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 11/12/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * Suma transacciones Monetarias TODO: Se consideran todas las
     * opciones, compras, creditos y representaciones
     * 
     * @param listaTransacciones
     * @return
     * @since 1.X
     */
    private Integer getNumeroDeTransaccionesMonetarias(
            List<Transaccion> listaTransacciones ) {
        int suma = 0;
        
        for ( Transaccion transaccion : listaTransacciones ) {
            String codigo = ( ( Registro00DTO ) transaccion.getRegistro00() )
                    .getCodigoTransaccion()
                    + "_"
                    + transaccion.getRegistro00().getTransCode();
            if ( Arrays.asList( CODIGOS_TRANSAC_MONETARY ).contains( codigo ) ) {
                suma++;
            }
        }
        
        return suma;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 - 9/12/2015, (ACL) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    private String generateBatchTrailer( List<Transaccion> listaTransacciones ) {
        
        Trailer batchTrailer = new Trailer();
        String montoTransaccionesMonetarias = getMontoDeTransaccionesMonetarias( listaTransacciones );
        String numeroTransaccionesBatch = CommonsUtils.paddingZeroToNumber(
                totalRegistrosArchivo, 12 );
        
        batchTrailer.setCodi( INCOMING_BATCH_NUMBER );
        batchTrailer.setZero( CommonsUtils.getZeroString( 2 ) );
        batchTrailer.setSobi( visaNumber );
        batchTrailer.setFech( DateUtils.getTodayStringDateInJulianFormat() );
        batchTrailer.setSumd( CommonsUtils.getZeroString( 15 ) );
        batchTrailer.setNumd( CommonsUtils.paddingZeroToNumber(
                totalTransaccionesMonetarias, 12 ) );
        batchTrailer.setNuba( CommonsUtils.getZeroString( 6 ) );
        batchTrailer.setNure( numeroTransaccionesBatch );
        batchTrailer.setBatn( CommonsUtils.paddingZeroToNumber( 1, 8 ) );
        batchTrailer.setNutr( CommonsUtils.paddingZeroToNumber(
                Integer.valueOf( totalTransaccionesMonetarias ) + 1, 9 ) );
        batchTrailer.setNtrf( montoTransaccionesMonetarias );
        
        return batchTrailer.getStringTrailer();
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 - 9/12/2015, (ACL) - versión inicial
     * </ul>
     * <p>
     * 
     * @param listaTransacciones
     * 
     * @return
     * @since 1.X
     */
    private String generateFileTrailer( List<Transaccion> listaTransacciones ) {
        
        String numeroTransaccionesFile = CommonsUtils.paddingZeroToNumber(
                totalTransaccionesMonetarias, 12 );
        // SE LE SUMA 1 POR FORMATO DE ARCHIVO
        String numeroTotalBatchFile = CommonsUtils.paddingZeroToNumber(
                ( totalRegistrosArchivo + 1 ), 12 );
                
        Trailer fileTrailer = new Trailer();
        fileTrailer.setCodi( INCOMING_FILE_NUMBER );
        fileTrailer.setZero( CommonsUtils.getZeroString( 2 ) );
        fileTrailer.setSobi( visaNumber );
        fileTrailer.setFech( DateUtils.getTodayStringDateInJulianFormat() );
        fileTrailer.setSumd( CommonsUtils.getZeroString( 15 ) );
        fileTrailer.setNumd( numeroTransaccionesFile );
        fileTrailer.setNuba( CommonsUtils.getZeroString( 5 ).concat( "1" ) );
        fileTrailer.setNure( numeroTotalBatchFile );
        fileTrailer.setBatn( CommonsUtils.getZeroString( 8 ) );
        fileTrailer.setNutr( CommonsUtils.paddingZeroToNumber(
                totalTransaccionesMonetarias + 2, 9 ) );
        fileTrailer
                .setNtrf( getMontoDeTransaccionesMonetarias( listaTransacciones ) );
        
        return fileTrailer.getStringTrailer();
    }
    
}
