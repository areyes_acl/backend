package com.sbpay.sgi.cron.dto;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 20/11/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class Registro1020DTO extends RegistroDTO {
    /**
     * 
     */
	 private static final long serialVersionUID = 2403995758607146919L;
	    private String mit;
	    private String codfuncion;
	    private String mensaje;
	    private String codrazon;
	    private String datosAdicionales1;
	    private String nroMensaje;
	    
	    
	    public String getNroMensaje() {
			return nroMensaje;
		}




		public void setNroMensaje(String nroMensaje) {
			this.nroMensaje = nroMensaje;
		}




		public String getMit() {
			return mit;
		}




		public void setMit(String mit) {
			this.mit = mit;
		}




		public String getCodfuncion() {
			return codfuncion;
		}




		public void setCodfuncion(String codfuncion) {
			this.codfuncion = codfuncion;
		}




		public String getMensaje() {
			return mensaje;
		}




		public void setMensaje(String mensaje) {
			this.mensaje = mensaje;
		}




		public String getCodrazon() {
			return codrazon;
		}




		public void setCodrazon(String codrazon) {
			this.codrazon = codrazon;
		}




		public String getDatosAdicionales1() {
			return datosAdicionales1;
		}




		public void setDatosAdicionales1(String datosAdicionales1) {
			this.datosAdicionales1 = datosAdicionales1;
		}




		/**
	     * 
	     * 
	     * <p>
	     * Registro de versiones:
	     * <ul>
	     * <li>1.0 9/12/2015, (ACL-sbpay) - versión inicial
	     * </ul>
	     * <p>
	     * 
	     * @return
	     * @see com.sbpay.sgi.cron.dto.RegistroDTO#toString()
	     * @since 1.X
	     */
	    @Override
	    public String toString() {
	        return "Registro05DTO [TipoTransaccion = " + super.getTipoTransaccion()
	                + ", transcode = " + super.getTransCode() ;
    
}
	    
}
