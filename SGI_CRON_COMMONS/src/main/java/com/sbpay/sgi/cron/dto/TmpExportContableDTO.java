package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

public class TmpExportContableDTO implements Serializable {

	private static final long serialVersionUID = 4256532444788242634L;
	private String transaccion;
	private String movOrg;
	private String producto;
	private String sucMov;
	private String sucPro;
	private String sucPago;
	private Long comercio;
	private String monto;
	private String fecha;
	private String identificador;
	private String compania;
	private String rutEntidad;
	private String mov;
	private String portafolio;
	private String cuenta;
	private String rutCliente;
	

	public String getRutCliente() {
		return rutCliente;
	}

	public void setRutCliente(String rutCliente) {
		this.rutCliente = rutCliente;
	}

	public String getTransaccion() {
		return transaccion;
	}

	public void setTransaccion(String transaccion) {
		this.transaccion = transaccion;
	}

	public String getMovOrg() {
		return movOrg;
	}

	public void setMovOrg(String movOrg) {
		this.movOrg = movOrg;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getSucMov() {
		return sucMov;
	}

	public void setSucMov(String sucMov) {
		this.sucMov = sucMov;
	}

	public String getSucPro() {
		return sucPro;
	}

	public void setSucPro(String sucPro) {
		this.sucPro = sucPro;
	}

	public String getSucPago() {
		return sucPago;
	}

	public void setSucPago(String sucPago) {
		this.sucPago = sucPago;
	}

	public Long getComercio() {
		return comercio;
	}

	public void setComercio(Long comercio) {
		this.comercio = comercio;
	}

	public String getMonto() {
		return monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public String getCompania() {
		return compania;
	}

	public void setCompania(String compania) {
		this.compania = compania;
	}

	public String getRutEntidad() {
		return rutEntidad;
	}

	public void setRutEntidad(String rutEntidad) {
		this.rutEntidad = rutEntidad;
	}

	public String getMov() {
		return mov;
	}

	public void setMov(String mov) {
		this.mov = mov;
	}

	public String getPortafolio() {
		return portafolio;
	}

	public void setPortafolio(String portafolio) {
		this.portafolio = portafolio;
	}

	public String getCuenta() {
		return cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

}
