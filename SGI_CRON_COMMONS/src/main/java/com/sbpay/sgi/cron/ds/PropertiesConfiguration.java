package com.sbpay.sgi.cron.ds;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;

import org.apache.log4j.PropertyConfigurator;

/**
 * 
 * @author dmedina
 *
 */
public class PropertiesConfiguration {

  /** Constante de directorio aplicación. */
  private static final String DIRAPPCONFIG = "dirAppConfig";
  /** Constante de serverPath. */
  private static final String USERDIR = "user.dir";
  /** Constante de properties config **/
  private static final String CONFIG_PROPERTY_ENTRY_NAME = "sigCronConfig";
  private static final Properties prop =  new Properties();

  /**
   * Metodo que inicializa los parametros del Log4J
   * 
   * @param config
   * @throws IOException
   */
  private static void cargaLog4jConfig(ServletConfig config) throws IOException {
    InputStream inStream = null;
    final String serverPath = System.getProperty(USERDIR).replaceAll("\\\\", "/");
    try {
      final String file = config.getServletContext().getInitParameter("log4jConfig");
      final String dirApp = config.getServletContext().getInitParameter(DIRAPPCONFIG);
      inStream = new BufferedInputStream(new FileInputStream(serverPath + dirApp + file));
      Properties p = new Properties();
      p.load(inStream);
      PropertyConfigurator.configure(p);
    } finally {
      if (inStream != null) {
        inStream.close();
      }
    }
  }

  /**
   * 
   * @param context
   */
  private static void cargaLog4jConfig(ServletContext context) {
    InputStream inStream = null;
    final String serverPath = System.getProperty(USERDIR).replaceAll("\\\\", "/");
    try {
      final String file = context.getInitParameter("log4jConfig");
      final String dirApp = context.getInitParameter(DIRAPPCONFIG);
      inStream = new BufferedInputStream(new FileInputStream(serverPath + dirApp + file));
      Properties p = new Properties();
      p.load(inStream);
      PropertyConfigurator.configure(p);
    } catch (Exception e) {
      System.out.println("NO SE PUEDE CARGAR LOG4J");

    } finally {
      if (inStream != null) {
        try {
          inStream.close();
        } catch (IOException e) {
          System.out.println("NO SE PUEDE CERRAR STREMA DE LOG4J");
          e.printStackTrace();
        }
      }
    }
  }

  /**
   * 
   * @return
   */
  private static void cargaAppConfig(ServletContext context) {
    InputStream input = null;
    String serverPath = System.getProperty(USERDIR).replaceAll("\\\\", "/");
    FileInputStream fileInput = null;

    try {
      String file = context.getInitParameter(CONFIG_PROPERTY_ENTRY_NAME);
      String dirApp = context.getInitParameter(DIRAPPCONFIG);

      System.out.println("FILE : " + file);
      System.out.println("dirApp : " + dirApp);
      System.out.println("ServerPath : " + serverPath);
      System.out.println("PATH : " + serverPath + dirApp + file);

      fileInput = new FileInputStream(serverPath + dirApp + file);
      System.out.println("fileInput : " + fileInput);

      input = new BufferedInputStream(fileInput);
      System.out.println("input : " + input);

      // load a properties file
      prop.load(input);

    } catch (IOException ex) {
      ex.printStackTrace();
    } finally {
      if (input != null) {
        try {
          input.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }

  }

  /**
   * 
   * @param context
   */
  public static void loadApplicationProperties(ServletContext context) {

    cargaLog4jConfig(context);
    cargaAppConfig(context);

  }


  public static Properties getCronProperties() {
    return prop;
  }

}
