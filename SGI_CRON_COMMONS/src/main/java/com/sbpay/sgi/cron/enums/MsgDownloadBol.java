package com.sbpay.sgi.cron.enums;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 12/02/2016, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public enum MsgDownloadBol {
	
    GENERIC_ERROR_MSJ("Ha ocurrido un problema al ejecutar la descarga del bol : "),
    NO_VALID_FILES("No existe archivo BOL valido a descargar."),
    CANT_CALCULATE_DIFERENCE_BETWEENN_DATES("No se ha podido realizar el calculo de la diferencia entre las fechas de bd y de archivo en sftp"),
    NO_EXIST_NEW_FILES("No existe un archivo nuevo para ser descargado. La fecha del ultimo archivo en SFTP, es la misma que la fecha del ultimo archivo registrado como descargado en BD."),
    INVALID_DATE_BETWEEN_FILE_AND_REGISTER("La fecha del ultimo archivo en SFTP, es menor al ultimo archivo registrado como descagado en BD."),
    ERROR_RETRIEVE_LOG_REGISTER("Ha ocurrido un error al intentar rescatar el log del archivo bol desde BD."),
    ERROR_CLOSE_CONECTION("Ha ocurrido un error al intentar cerrar la conexión a BD."),
    ERROR_GENERATE_CONTROL_FILE("Ha ocurrido un error al intentar generar el archivo de control para el BOL."),
    WARN_PROCESS_DEACTIVATED("El proceso de desacarga de archivo Bol no se encuentra activo"),
    INVALID_PARAMS("No se han cargado los parametros necesarios para la descarga del archivo BOL");
    ;
    
    /**
     * Valor ENUM.
     */
    private final String text;
    
    /**
     * Constructor.
     * 
     * @param text
     */
    private MsgDownloadBol( final String text ) {
        this.text = text;
    }
    
    /**
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
}
