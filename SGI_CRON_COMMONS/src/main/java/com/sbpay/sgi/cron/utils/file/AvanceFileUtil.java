package com.sbpay.sgi.cron.utils.file;

import java.util.List;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dto.TmpExportItzAvanceDTO;

public interface AvanceFileUtil {
	
	void exportAvanceFile(String ruta, String avanceFilename, List<TmpExportItzAvanceDTO> listaRegistros) throws AppException;
}
