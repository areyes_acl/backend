package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 18/12/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class ParamDownloadComisionesDTO implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = 4081693614785403716L;
    private String formatFileTbkComision;
    private String formatExtNameTbkComision;
    private String formatFileControlTbkComision;
    private String formatExtNameControlTbkComision;
    private String pathTbkComision;
    private String pathFtpTbkComision;
    private DataFTP dataFTP;
    
	public String getFormatFileTbkComision() {
		return formatFileTbkComision;
	}
	public void setFormatFileTbkComision(final String formatFileTbkComision) {
		this.formatFileTbkComision = formatFileTbkComision;
	}
	public String getFormatExtNameTbkComision() {
		return formatExtNameTbkComision;
	}
	public void setFormatExtNameTbkComision(final String formatExtNameTbkComision) {
		this.formatExtNameTbkComision = formatExtNameTbkComision;
	}
	public String getFormatFileControlTbkComision() {
		return formatFileControlTbkComision;
	}
	public void setFormatFileControlTbkComision(final String formatFileControlTbkComision) {
		this.formatFileControlTbkComision = formatFileControlTbkComision;
	}
	public String getFormatExtNameControlTbkComision() {
		return formatExtNameControlTbkComision;
	}
	public void setFormatExtNameControlTbkComision(
			String formatExtNameControlTbkComision) {
		this.formatExtNameControlTbkComision = formatExtNameControlTbkComision;
	}
	public String getPathTbkComision() {
		return pathTbkComision;
	}
	public void setPathTbkComision(final String pathTbkComision) {
		this.pathTbkComision = pathTbkComision;
	}
	public String getPathFtpTbkComision() {
		return pathFtpTbkComision;
	}
	public void setPathFtpTbkComision(final String pathFtpTbkComision) {
		this.pathFtpTbkComision = pathFtpTbkComision;
	}
	public DataFTP getDataFTP() {
		return dataFTP;
	}
	public void setDataFTP(final DataFTP dataFTP) {
		this.dataFTP = dataFTP;
	}
	@Override
	public String toString() {
		return "ParamDownloadComisionesDTO [formatFileTbkComision="
				+ formatFileTbkComision + ", formatExtNameTbkComision="
				+ formatExtNameTbkComision + ", formatFileControlTbkComision="
				+ formatFileControlTbkComision
				+ ", formatExtNameControlTbkComision="
				+ formatExtNameControlTbkComision + ", pathTbkComision="
				+ pathTbkComision + ", pathFtpTbkComision="
				+ pathFtpTbkComision + ", dataFTP=" + dataFTP + "]";
	}
    

    
}
