package com.sbpay.sgi.cron.utils.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dto.IncomingDTO;
import com.sbpay.sgi.cron.dto.Registro00DTO;
import com.sbpay.sgi.cron.dto.Registro00OutVisaCobroCargoDTO;
import com.sbpay.sgi.cron.dto.Registro01DTO;
import com.sbpay.sgi.cron.dto.Registro05DTO;
import com.sbpay.sgi.cron.dto.Registro07DTO;
import com.sbpay.sgi.cron.dto.Transaccion;
import com.sbpay.sgi.cron.dto.TransaccionOutVisaCobroCargo;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.MsgErrorFile;
import com.sbpay.sgi.cron.enums.MsgErrorProcessFile;
import com.sbpay.sgi.cron.utils.generals.DateUtils;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 23/11/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class IncomingReader implements IncomingFileUtils {
    
    /** VARIABLE PARA EL LOGER */
    private static final Logger LOGGER = Logger
            .getLogger( IncomingReader.class );
    
    private static final String CODIGO_CABECERA_INCOMING = "90";
    private static final String CODIGO_REGISTRO_00 = "00";
    private static final String CODIGO_REGISTRO_01 = "01";
    private static final String CODIGO_REGISTRO_05 = "05";
    private static final String CODIGO_REGISTRO_07 = "07";
    private static final String CODIGO_FINAL_ARCHIVO = "92";
    private static final String CODIGO_SUMA_TOTALES = "91";
    private static final String CODIGO_REGISTRO_10 = "10";
    private static final String CODIGO_REGISTRO_20 = "20";
    private static final int ESPACIO = 2; // Parametro para procesar el fichero con o sin espacios EJ: 0 = (con 2 espacios), (2 = sin espacios)
    
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 24/11/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * Metodo que lee un archivo incoming desde una ruta especificada
     * y lo transforma a DTO,
     * 
     * 
     * @param ruta
     * @return
     * @throws AppException
     * @since 1.X
     */
    @Override
    public IncomingDTO readIncomingFile( String ruta, String starWith )
            throws AppException {
        BufferedReader buffReader = null;
        IncomingDTO incoming = null;
        
        try {
            buffReader = new BufferedReader( new FileReader( ruta ) );
            String[] tokens = ruta.split( "[\\\\|x/]" );
            String filename = tokens[tokens.length - 1];
            
            incoming = readFile( buffReader, filename, starWith );

        }
        catch ( IOException ioe ) {
            throw new AppException( "Ha ocurrido un error al leer el archivo",
                    ioe );
        }
        finally {
            try {
                if ( buffReader != null ) {
                    buffReader.close();
                }
            }
            catch ( IOException ioe1 ) {
                throw new AppException(
                        "Ha ocurrido un error al cerrar el archivo", ioe1 );
            }
        }
        
        return incoming;
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 2/12/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @param listaTransacciones
     * @param buffReader
     * @param line
     * @param filename
     * @return
     * @throws AppException
     * @throws IOException
     * @since 1.X
     */
    private IncomingDTO readFile( BufferedReader buffReader, String filename,
            String starWith ) throws AppException, IOException {
        
        String line = buffReader.readLine();
        List<Transaccion> listaTransacciones = new ArrayList<Transaccion>();
        List<TransaccionOutVisaCobroCargo> listaCobroCargo= new ArrayList<TransaccionOutVisaCobroCargo>();
        IncomingDTO incoming;
        Transaccion transaccion = null;
        TransaccionOutVisaCobroCargo transaccionCobroCargo = null;
        int numeroTransaccion = 0;
        
        // RECORRE EL FILE LEYENDO LINEA POR LINEA
        while ( line != null ) {
            // EL CODIGO DE LA CABECERA
            String codigo = line.substring( 0, 2 );
            
            if ( CODIGO_CABECERA_INCOMING.equalsIgnoreCase( codigo ) ) {
                validarFechaArchivoIncoming( filename, line, starWith );
            }
            else if ( CODIGO_FINAL_ARCHIVO.equalsIgnoreCase( codigo ) ) {
                    LOGGER.debug( "*** SE TERMINO DE LEER EL ARCHIVO, CODIGO 92 ****" );
                    if(transaccion != null){
                    	listaTransacciones.add( transaccion );
                    }
                    if(transaccionCobroCargo != null){
                    	listaCobroCargo.add(transaccionCobroCargo);
                    }
                
                }else if (CODIGO_SUMA_TOTALES.equalsIgnoreCase( codigo )){
                    LOGGER.debug( "*** SUMATORIA INTERMEDIA DEL OUTGOING, CODIGO 91 ****" );
                }
                else {
                    String transCode = line.substring( 4, 6 );
                    if(CODIGO_REGISTRO_00.equalsIgnoreCase( transCode ) && (CODIGO_REGISTRO_10.equalsIgnoreCase(codigo) || CODIGO_REGISTRO_20.equalsIgnoreCase(codigo))){
                    	LOGGER.info("Hay registros 10 o 10 de tbk");
                    	if(transaccionCobroCargo != null){
                    		listaCobroCargo.add(transaccionCobroCargo);
                    	}
                    	transaccionCobroCargo = new TransaccionOutVisaCobroCargo();
                    	transaccionCobroCargo.setRegistro1020(leerRegistro00CobroCargoTBK(line));
                    	
                    }
                    else if ( CODIGO_REGISTRO_00.equalsIgnoreCase( transCode ) ) {
                        /**
                         * Si es 00 y no es la primera transacción se
                         * agrega la lista de transacciones
                         */
                        if ( transaccion != null ) {
                            listaTransacciones.add( transaccion );
                        }
                        transaccion = new Transaccion();
                        transaccion.setNumeroTransaccion( numeroTransaccion );
                        numeroTransaccion++;
                        transaccion.setRegistro00( leerRegistro00( line ) );
                    }
                     
                    else if ( CODIGO_REGISTRO_01.equalsIgnoreCase( transCode ) ) {
                            transaccion.setRegistro01( leerRegistro01( line ) );
                        }
                        else
                            if ( CODIGO_REGISTRO_05
                                    .equalsIgnoreCase( transCode ) ) {
                                
                                transaccion
                                        .setRegistro05( leerRegistro05( line ) );
                            }
                            else
                                if ( CODIGO_REGISTRO_07
                                        .equalsIgnoreCase( transCode ) ) {
                                    transaccion
                                            .setRegistro07( leerRegistro07( line ) );
                                }
                    
                }
            line = buffReader.readLine();
        }
        
        LOGGER.info( "********* CANTIDAD DE TRANSACCIONES : "
                + listaTransacciones.size() + " **********" );
        LOGGER.info("Cantidad de cobro cargo " + listaCobroCargo.size());
        
        incoming = new IncomingDTO();
        incoming.setIncomingName( filename );
        incoming.setListaTransacciones( listaTransacciones );
        incoming.setListaCobroCargo(listaCobroCargo);
        return incoming;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 2/12/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * Metodo que recibe un file y lo transforma a DTO
     * 
     * @param file
     * @return
     * @throws AppException
     * @see com.sbpay.sgi.cron.utils.file.IncomingFileUtils#readIncomingFile(java.io.File)
     * @since 1.X
     */
    @Override
    public IncomingDTO readIncomingFile( File file, String starWith )
            throws AppException {
        
        BufferedReader buffReader = null;
        IncomingDTO incoming = null;
        
        try {
            
            if ( file == null ) {
                throw new AppException(
                        MsgErrorFile.ERROR_READ_FILE_IS_NULL.toString() );
            }
            
            String ruta = file.getAbsolutePath();
            String filename = file.getName();
            
            buffReader = new BufferedReader( new FileReader( ruta ) );
            incoming = readFile( buffReader, filename, starWith );
            
        }
        catch ( IOException ioe ) {
            throw new AppException(
                    "Ha ocurrido un error al leer el archivo : "
                            + ioe.getMessage(), ioe );
        }
        finally {
            try {
                if ( buffReader != null ) {
                    buffReader.close();
                }
            }
            catch ( IOException ioe1 ) {
                throw new AppException(
                        "Ha ocurrido un error al cerrar el archivo", ioe1 );
            }
        }
        
        return incoming;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 2/12/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @param line
     * @return
     * @since 1.X
     */
    private Registro07DTO leerRegistro07( String line ) {
        Registro07DTO registro07 = new Registro07DTO();
        registro07.setTipoTransaccion( line.substring( 0, 2 ) );
        registro07.setTransCode( line.substring( 4, 6 ) );
        registro07.setTransType( line.substring( 6, 8 ) );
        registro07.setCardSeqNbr( line.substring( 8, 11 ) );
        registro07.setTermTrnDate( line.substring( 11, 17 ) );
        registro07.setTermCapProf( line.substring( 17, 23 ) );
        registro07.setTermCounCode( line.substring( 23, 26 ) );
        registro07.setTermSerNbr( line.substring( 26, 34 ) );
        registro07.setUnpredNumber( line.substring( 34, 42 ) );
        registro07.setAppTrnCount( line.substring( 42, 46 ) );
        registro07.setAppInterProf( line.substring( 46, 50 ) );
        registro07.setCrypt( line.substring( 50, 66 ) );
        registro07.setIssAppDataB2( line.substring( 66, 68 ) );
        registro07.setIssAppDataB3( line.substring( 68, 70 ) );
        registro07.setTermVerRsult( line.substring( 70, 80 ) );
        registro07.setIssAppDataB4( line.substring( 80, 88 ) );
        registro07.setCryptAmt( line.substring( 88, 100 ) );
        registro07.setIssAppDataB8( line.substring( 100, 102 ) );
        registro07.setIssAppDataB9( line.substring( 102, 118 ) );
        registro07.setIssAppDataB1( line.substring( 118, 120 ) );
        registro07.setIssAppDataB17( line.substring( 120, 122 ) );
        registro07.setIssAppDataB18( line.substring( 122, 152 ) );
        registro07.setFormFactInd( line.substring( 152, 160 ) );
        registro07.setIssRsult( line.substring( 160, 170 ) );
        
        return registro07;
        
    }
    
    private Registro00OutVisaCobroCargoDTO leerRegistro00CobroCargoTBK(String fila){
    	Registro00OutVisaCobroCargoDTO registro00 = new Registro00OutVisaCobroCargoDTO();
    	
    	//1-2	(02) Transaction Code
    	registro00.setMit(fila.substring(0, 2));
    	//3+4	(02) Transaction Code Qualifier + Transaction Component Sequence Number
    	registro00.setCodigoFuncion(fila.substring(2+ESPACIO, 4+ESPACIO));
    	//5-10	(06) Destination BIN
    	registro00.setDestinationBin(fila.substring(4+ESPACIO, 10+ESPACIO));
    	//11-16	(06) Source BIN
    	registro00.setSourceBin(fila.substring(10+ESPACIO, 16+ESPACIO));
    	//17-20 (04) Reason Code
    	registro00.setReasonCode(fila.substring(16+ESPACIO, 20+ESPACIO));
    	//21–23 (03) Country Code
    	registro00.setCountryCode(fila.substring(20+ESPACIO, 23+ESPACIO));
    	//24–27 (04) Event Date (MMDD)
    	registro00.setEventDate(fila.substring(23+ESPACIO, 27+ESPACIO));
    	//28–43 (16) Account Number
    	registro00.setAccountNumber(fila.substring(27+ESPACIO, 43+ESPACIO));
    	//44–46 (03) Account Number Extension
    	registro00.setAccountNumberExtension(fila.substring(43+ESPACIO, 46+ESPACIO));
    	//47–58 (12) Destination Amount
    	registro00.setDestinationAmount(fila.substring(46+ESPACIO, 58+ESPACIO));
    	//59–61 (03) Destination Currency Code
    	registro00.setDestinationCurrencyCode(fila.substring(58+ESPACIO, 61+ESPACIO));
    	//62–73 (12) Source Amount
    	registro00.setSourceAmount(fila.substring(61+ESPACIO, 73+ESPACIO));
    	//74–76 (03) Source Currency Code
    	registro00.setSourceCurrencyCode(fila.substring(73+ESPACIO, 76+ESPACIO));
    	//77–146 (70) Message Text
    	registro00.setMessageText(fila.substring(76+ESPACIO, 146+ESPACIO));
    	//147 	(01) Settlement Flag
    	registro00.setSettlementFlag(fila.substring(146+ESPACIO, 147+ESPACIO));
    	//148–162 (15) Transaction Identifier
    	registro00.setTransactionIdentifier(fila.substring(147+ESPACIO, 162+ESPACIO));
    	//163 	(01) Reserved
    	registro00.setReserved(fila.substring(162+ESPACIO, 163+ESPACIO));
    	//164–167 (04) Central Processing Date (YDDD)
    	registro00.setCentralProcessingDate(fila.substring(163+ESPACIO, 167+ESPACIO));
    	//168 (01) Reimbursement Attribute
    	registro00.setReimbursementAttribute(fila.substring(167+ESPACIO, 168+ESPACIO));
    	//operador
    	
    	//Datos adicionales
    	registro00.setDatosAdicionales(fila.substring(0, 168+ESPACIO));
    	
    	return registro00;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 - 20/11/2015, (ACL) - versión inicial
     * </ul>
     * <p>
     * 
     * @param line
     * @since 1.X
     */
    private Registro05DTO leerRegistro05( String line ) {
        Registro05DTO registro05 = new Registro05DTO();
        registro05.setTipoTransaccion( line.substring( 0, 2 ) );
        registro05.setTransCode( line.substring( 4, 6 ) );
        registro05.setTransId( line.substring( 6, 21 ) );
        registro05.setAuthAmt( line.substring( 21, 33 ) );
        registro05.setMoneda( line.substring( 33, 36 ) );
        registro05.setAuthRespCd( line.substring( 36, 38 ) );
        registro05.setValCode( line.substring( 38, 42 ) );
        registro05.setExclTranIdRsn( line.substring( 42, 43 ) );
        registro05.setCrsProcngCd( line.substring( 43, 44 ) );
        registro05.setChrgbkCondCd( line.substring( 44, 46 ) );
        registro05.setMultClearSeqNbr( line.substring( 46, 48 ) );
        registro05.setMultClearSeqCnt( line.substring( 48, 50 ) );
        registro05.setMktAuthDataInd( line.substring( 50, 51 ) );
        registro05.setTotAuthAmt( line.substring( 51, 63 ) );
        registro05.setInfoInd( line.substring( 63, 64 ) );
        registro05.setMerPhone( line.substring( 64, 78 ) );
        registro05.setAddtlDataInd( line.substring( 78, 79 ) );
        registro05.setMerchVolInd( line.substring( 79, 81 ) );
        registro05.setElecGoodInd( line.substring( 81, 83 ) );
        registro05.setMerchVerValue( line.substring( 83, 93 ) );
        registro05.setInterFeeAmt( line.substring( 93, 108 ) );
        registro05.setInterFeeSign( line.substring( 108, 109 ) );
        registro05.setSourcurrExchRate( line.substring( 109, 117 ) );
        registro05.setBasecurrExchRate( line.substring( 117, 125 ) );
        registro05.setOptIsaAmt( line.substring( 125, 137 ) );
        registro05.setProdId( line.substring( 137, 139 ) );
        registro05.setProgId( line.substring( 139, 145 ) );
        registro05.setReserved( line.substring( 145, 169 ) );
        registro05.setResultCode( line.substring( 169, 170 ) );
        
        return registro05;
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 - 20/11/2015, (ACL) - versión inicial
     * </ul>
     * <p>
     * 
     * @param line
     * @since 1.X
     */
    private Registro01DTO leerRegistro01( String line ) {
        Registro01DTO registro01 = new Registro01DTO();
        registro01.setTipoTransaccion( line.substring( 0, 2 ) );
        registro01.setTransCode( line.substring( 4, 6 ) );
        registro01.setChargRefN( line.substring( 18, 24 ) );
        registro01.setDocumInd( line.substring( 24, 25 ) );
        registro01.setMensaje( line.substring( 25, 75 ) );
        registro01.setSpeCondInd( line.substring( 75, 77 ) );
        registro01.setCardAcceptorId( line.substring( 82, 97 ) );
        registro01.setTerminalId( line.substring( 97, 105 ) );
        registro01.setValorCuota( line.substring( 105, 117 ) );
        registro01.setIndicatorTransaction( line.substring( 117, 118 ) );
        registro01.setComisionCic( line.substring( 119, 125 ) );
        registro01.setCardHolder( line.substring( 125, 126 ) );
        registro01.setPrepairCardIn( line.substring( 126, 127 ) );
        registro01.setAuthSourceCode( line.substring( 129, 130 ) );
        registro01.setAtmAccSelec( line.substring( 131, 132 ) );
        registro01.setInstalPayCount( line.substring( 132, 134 ) );
        registro01.setItemDescrptor( line.substring( 134, 144 ) );
        registro01.setFlagDiferido( line.substring( 144, 145 ) );
        registro01.setFlagMesDeGracia( line.substring( 145, 146 ) );
        registro01.setPeriodosDeGracia( line.substring( 146, 147 ) );
        registro01.setPeriodosDeDiferido( line.substring( 147, 148 ) );
        registro01.setFlagPromoEmisora( line.substring( 148, 149 ) );
        registro01.setOrigen( line.substring( 149, 151 ) );
        registro01.setRubroTransbank( line.substring( 151, 155 ) );
        registro01.setTasaEECC( line.substring( 155, 159 ) );
        registro01.setCashBack( line.substring( 159, 168 ) );
        
        return registro01;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 24/11/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @param fila
     * @return
     * @since 1.X
     */
    private Registro00DTO leerRegistro00( String fila ) {
        Registro00DTO registro00 = new Registro00DTO();
        registro00.setCodigoTransaccion( fila.substring( 0, 2 ) );
        registro00.setTransCode( fila.substring( 4, 6 ) );
        registro00.setNumeroTarjeta( fila.substring( 6, 22 ) );
        registro00.setNumberExt( fila.substring( 22, 25 ) );
        registro00.setFormCode( fila.substring( 28, 29 ) );
        registro00.setBinFuente( fila.substring( 29, 35 ) );
        registro00.setFechaCapt( fila.substring( 35, 39 ) );
        registro00.setTipoVenta( fila.substring( 39, 40 ) );
        registro00.setNumCuo( fila.substring( 40, 42 ) );
        registro00.setNumIdn( fila.substring( 42, 50 ) );
        registro00.setCheckDig( fila.substring( 50, 51 ) );
        registro00.setAcqMembId( fila.substring( 51, 59 ) );
        registro00.setFechaComp( fila.substring( 59, 63 ) );
        registro00.setMontoDest( fila.substring( 63, 75 ) );
        registro00.setMonedaDest( fila.substring( 75, 78 ) );
        registro00.setMontoFuen( fila.substring( 78, 90 ) );
        registro00.setMonedaFuen( fila.substring( 90, 93 ) );
        registro00.setNombComer( fila.substring( 93, 112 ) );
        registro00.setFechaDCompensacion( fila.substring( 112, 116 ) );
        registro00.setOfic( fila.substring( 116, 118 ) );
        registro00.setCiudadComer( fila.substring( 118, 130 ) );
        registro00.setTdab( fila.substring( 130, 131 ) );
        registro00.setPais( fila.substring( 131, 134 ) );
        registro00.setRubroComer( fila.substring( 134, 138 ) );
        registro00.setZipCodeComer( fila.substring( 138, 143 ) );
        registro00.setCodMunic( fila.substring( 146, 148 ) );
        registro00.setUsagCode( fila.substring( 148, 149 ) );
        registro00.setCodRazon( fila.substring( 149, 151 ) );
        registro00.setSettleFlag( fila.substring( 151, 152 ) );
        registro00.setCodAutor( fila.substring( 153, 159 ) );
        registro00.setPosTerminalCapability( fila.substring( 159, 160 ) );
        registro00.setIntFeeIndic( fila.substring( 160, 161 ) );
        registro00.setCardHolderIdMethod( fila.substring( 161, 162 ) );
        registro00.setPosEntryMode( fila.substring( 163, 165 ) );
        registro00.setFechaProc( fila.substring( 165, 169 ) );
        registro00.setReimbAttr( fila.substring( 169, 170 ) );
        return registro00;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 17/11/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * Valida si la fecha del nombre del archivo es la misma que
     * contiene el archivo en su cabecera
     * 
     * 
     * @throws AppException
     * 
     * @since 1.X
     */
    private void validarFechaArchivoIncoming( String filename, String line,
            String starWith ) throws AppException {
        
        String fechaNombreArchivo = DateUtils.getDateStringFromFilename(
                filename, starWith );
        fechaNombreArchivo = DateUtils.getDateFileTbk( fechaNombreArchivo )
                .replaceAll( ConstantesUtil.SLASH.toString(),
                        ConstantesUtil.EMPTY.toString() );
        
        String fechaContenidoArchivo = DateUtils
                .getDateStringFromFileHeaderContent( line, 10, 15 );
        
        LOGGER.info( "Fecha en el Nombre del Archivo :" + fechaNombreArchivo );
        LOGGER.info( "Fecha en el Contenido del Archivo :"
                + fechaContenidoArchivo );
        if ( DateUtils
                .dateAreEquals( fechaNombreArchivo, fechaContenidoArchivo ) ) {
            LOGGER.info( "(OK) Fechas validas : La fecha que tiene el nombre del archivo es igual a la que posee este en su cabecera" );
        }
        else {
            throw new AppException(
                    MsgErrorProcessFile.ERROR_INVALID_DATES.toString() );
        }
        
    }

    @Override
    public void exportIncomingFile( String ruta, String incomingFilename,
            String controlFilename, List<Transaccion> listaTrasacciones )
            throws AppException {
        
    }
    
    
    
}
