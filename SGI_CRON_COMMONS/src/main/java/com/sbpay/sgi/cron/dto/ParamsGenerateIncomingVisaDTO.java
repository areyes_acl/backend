package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 08/02/2016, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * Clase que guarda los parametros necesarios para generar el incoming
 * Visa desde la tabla TBL_PRTS
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class ParamsGenerateIncomingVisaDTO implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = 7782219696904883769L;
    private String pathOutVisa;
    private String formatFilenameIncVisa;
    private String formatExtNameIncVisa;
    private String formatExtNameIncVisaCrt;
	public String getPathOutVisa() {
		return pathOutVisa;
	}
	public void setPathOutVisa(final String pathOutVisa) {
		this.pathOutVisa = pathOutVisa;
	}
	public String getFormatFilenameIncVisa() {
		return formatFilenameIncVisa;
	}
	public void setFormatFilenameIncVisa(final String formatFilenameIncVisa) {
		this.formatFilenameIncVisa = formatFilenameIncVisa;
	}
	public String getFormatExtNameIncVisa() {
		return formatExtNameIncVisa;
	}
	public void setFormatExtNameIncVisa(final String formatExtNameIncVisa) {
		this.formatExtNameIncVisa = formatExtNameIncVisa;
	}
	public String getFormatExtNameIncVisaCrt() {
		return formatExtNameIncVisaCrt;
	}
	public void setFormatExtNameIncVisaCrt(final String formatExtNameIncVisaCrt) {
		this.formatExtNameIncVisaCrt = formatExtNameIncVisaCrt;
	}
	@Override
	public String toString() {
		return "ParamsGenerateIncomingVisaDTO [pathOutVisa="
				+ pathOutVisa + ", formatFilenameIncVisa="
				+ formatFilenameIncVisa + ", formatExtNameIncVisa="
				+ formatExtNameIncVisa + ", formatExtNameIncVisaCrt="
				+ formatExtNameIncVisaCrt + "]";
	}
    

    
}
