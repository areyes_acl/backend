package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

public class ParamExportCnblDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String pathSalida;
	private String nomArchivo;
	private String extension;
	private String formatFileNameOnewCtr;
	private String formatExtNameOnewCtr;
	
	public String getPathSalida() {
		return pathSalida;
	}

	public void setPathSalida(String pathSalida) {
		this.pathSalida = pathSalida;
	}

	public String getNomArchivo() {
		return nomArchivo;
	}

	public void setNomArchivo(String nomArchivo) {
		this.nomArchivo = nomArchivo;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getFormatFileNameOnewCtr() {
		return formatFileNameOnewCtr;
	}

	public void setFormatFileNameOnewCtr(String formatFileNameOnewCtr) {
		this.formatFileNameOnewCtr = formatFileNameOnewCtr;
	}

	public String getFormatExtNameOnewCtr() {
		return formatExtNameOnewCtr;
	}

	public void setFormatExtNameOnewCtr(String formatExtNameOnewCtr) {
		this.formatExtNameOnewCtr = formatExtNameOnewCtr;
	}

	@Override
	public String toString() {
		return "ParamExportCnblDTO [pathSalida=" + pathSalida + ", nomArchivo="
				+ nomArchivo + ", extension=" + extension
				+ ", formatFileNameOnewCtr=" + formatFileNameOnewCtr
				+ ", formatExtNameOnewCtr=" + formatExtNameOnewCtr + "]";
	}


	
	

}
