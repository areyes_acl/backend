package com.sbpay.sgi.cron.utils.generals;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.dto.ParametroUserDTO;
import com.sbpay.sgi.cron.enums.CodigoTransaccionOnusType;
import com.sbpay.sgi.cron.enums.ConstantesUtil;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 17/11/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * Clase que contendrà metodos auxiliares que serviran a la app
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class CommonsUtils {

	/** VARIABLE PARA EL LOGER */
	private static final Logger LOGGER = Logger.getLogger(CommonsUtils.class);

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 24/11/2015, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * Metodo Permite extraer el valor que lleve por nombre el "codiDato" dentro
	 * de la lista de parametrosGenerica
	 * 
	 * @param paramGenList
	 * @param codiDato
	 * @return
	 * @since 1.X
	 */
	public static String getCodiDato(final List<ParametroDTO> paramGenList,
			final String codiDato) {
		String aux = null;

		for (ParametroDTO dto : paramGenList) {
			if (dto.getCodDato().equals(codiDato)) {
				aux = dto.getValor();

				break;
			}
		}
		return aux;
	}

	/**
	 * Metodo Permite extraer el valor de cod dato requerido.
	 * 
	 * @param paramGenList
	 *            Lista de parametros.
	 * @param codiDato
	 *            cod dato buscado.
	 * @return Valor del cod dato.
	 */
	public static String getCodiDatoUser(
			final List<ParametroUserDTO> paramGenList, final String codiDato) {
		String aux = null;

		for (ParametroUserDTO dto : paramGenList) {
			if (dto.getCodDato().equals(codiDato)) {
				aux = dto.getValor();

				break;
			}
		}
		return aux;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 27/11/2015, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que mueve un archivo desde un path de origen a un path de destino
	 * 
	 * @param pathOri
	 * @param pathDest
	 * @return
	 * @since 1.X
	 */
	public static boolean moveFile(String pathOri, String pathDest) {

		try {
			LOGGER.info("mover archivo archivo " + pathOri + " -> " + pathDest);
			File oriFile = new File(pathOri);
			File destFile = new File(pathDest);
			String nameFile = oriFile.getName();
			Boolean flag = oriFile.renameTo(destFile);

			if (flag) {
				flag = Boolean.TRUE;
				LOGGER.info("archivo movido correctamente " + nameFile);
			} else {
				LOGGER.info("error al mover el archivo " + nameFile);
				borrarArchivo(pathOri, pathDest);
			}
			return flag;
		} catch (Exception e) {
			LOGGER.error("error al mover el archivo");
			return false;
		}
	}
	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 - 27/11/2015, (ACL) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * METODO QUE OBTIENE UNA LISTA DE ARCHIVOS DESDE UNA CARPETA PASADA COMO
	 * PARAMETRO
	 * 
	 * @return
	 * @throws AppException
	 * @since 1.X
	 */
	public static File[] getFilesInFolder(String folderPath)
			throws AppException {
		File[] listOfFiles = null;
		File folder = null;

		// OBTIENE EL FOLDER
		folder = new File(folderPath);

		listOfFiles = folder.listFiles();
		return listOfFiles;

	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 18/12/2015, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * METODO QUE OBTIENE UNA LISTA DE ARCHIVOS DESDE UNA CARPETA PASADA COMO
	 * PARAMETRO, QUE COMIENCE CON UN NOMBRE Y QUE TERMINE CON OTRO
	 * 
	 * @param folderPath
	 *            Ruta de carpeta donde se buscaran los archivos
	 * @param starWith
	 *            Prefijo que comince con, para filtrar la busqueda de archivos
	 * @param endsWith
	 *            Prefijo que termine con, para filtrar la busqueda de archivos
	 * @return List<File> Lista de archivos encontrados en la carpeta que
	 *         contienen los nombres indicados en los filtros
	 * @throws AppException
	 * @since 1.X
	 */
	public static List<File> getFilesInFolder(String folderPath,
			String starWith, String endsWith) throws AppException {

		File[] listOfFiles = getFilesInFolder(folderPath);
		List<File> listOfOutgoingsFiles = new ArrayList<File>();

		if (listOfFiles == null) {
			LOGGER.info("no hay ficheros en el directorio al parecer");
			return null;
		} else {
			for (File file : listOfFiles) {
				if (!file.isDirectory() && file.isFile()
						&& file.getName().startsWith(starWith)
						&& file.getName().endsWith(endsWith)) {
					LOGGER.info("Fichero encontrado "+file.getAbsolutePath());
					listOfOutgoingsFiles.add(file);
				}

			}

		}

		return listOfOutgoingsFiles;

	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/10/2018, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * METODO QUE OBTIENE UNA LISTA DE ARCHIVOS DESDE UNA CARPETA PASADA COMO
	 * PARAMETRO, QUE COMIENCE CON UN NOMBRE Y QUE TERMINE CON OTRO
	 * 
	 * @param folderPath
	 *            Ruta de carpeta donde se buscaran los archivos
	 * @param starWith
	 *            Nombre del archivo
	 * @return List<File> Lista de archivos encontrados en la carpeta que
	 *         contienen los nombres indicados en los filtros
	 * @throws AppException
	 * @since 1.X
	 */
	public static List<File> getFilesInFolderBice(String folderPath,
			String starWith) throws AppException {

		File[] listOfFiles = getFilesInFolder(folderPath);
		List<File> listOfOutgoingsFiles = new ArrayList<File>();

		if (listOfFiles == null) {
			return null;
		} else {
			for (File file : listOfFiles) {
				if (!file.isDirectory() && file.isFile()
						&& file.getName().substring(8).startsWith(starWith.substring(8))) {
					listOfOutgoingsFiles.add(file);
				}

			}

		}

		return listOfOutgoingsFiles;

	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/10/2018, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * METODO QUE OBTIENE UNA LISTA DE ARCHIVOS DESDE UNA CARPETA PASADA COMO
	 * PARAMETRO, QUE COMIENCE CON UN NOMBRE Y QUE TERMINE CON OTRO
	 * 
	 * @param folderPath
	 *            Ruta de carpeta donde se buscaran los archivos
	 * @param starWith
	 *            Nombre del archivo
	 * @return List<File> Lista de archivos encontrados en la carpeta que
	 *         contienen los nombres indicados en los filtros
	 * @throws AppException
	 * @since 1.X
	 */
	public static List<File> getFilesInFolderAvances(String folderPath,
			String starWith) throws AppException {

		File[] listOfFiles = getFilesInFolder(folderPath);
		List<File> listAvancesFiles = new ArrayList<File>();

		if (listOfFiles == null) {
			return null;
		} else {
			for (File file : listOfFiles) {
				if (!file.isDirectory() && file.isFile()
						&& file.getName().substring(8).startsWith(starWith.substring(8))) {
					listAvancesFiles.add(file);
				}

			}

		}

		return listAvancesFiles;

	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 7/12/2015, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * Metodoq que dado un numero de ceros retorna una cadena con la cantidad
	 * ingresada en ceros
	 * 
	 * @param numOfceros
	 *            Numero de ceros que se retornaran en la cadena
	 * @return
	 * @since 1.X
	 */
	public static String getZeroString(Integer numOfceros) {
		StringBuilder str = new StringBuilder();
		for (int i = 0; i < numOfceros; i++) {
			str.append(ConstantesUtil.ZERO.toString());
		}

		return str.toString();
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 7/12/2015, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * Metodoq que dado un numero de ceros retorna una cadena con la cantidad
	 * ingresada en ceros
	 * 
	 * @param numOfceros
	 *            Numero de ceros que se retornaran en la cadena
	 * @return
	 * @since 1.X
	 */
	public static String getWhitesSpaceString(Integer numOfspaces) {
		StringBuilder str = new StringBuilder();
		for (int i = 0; i < numOfspaces; i++) {
			str.append(ConstantesUtil.WHITE.toString());
		}

		return str.toString();
	}

	public static String concatStringLeft(String cadena, String concatenar,
			long largo) {
		String cadenaAux = cadena;
		for (int i = cadena.length(); i < largo; i++) {
			cadenaAux = concatenar.concat(cadenaAux);
		}
		return cadenaAux;
	}

	public static String concatStringRight(String cadena, String concatenar,
			long largo) {
		String cadenaAux = cadena;
		for (int i = cadena.length(); i < largo; i++) {
			cadenaAux = cadenaAux.concat(concatenar);
		}
		return cadenaAux;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 11/12/2015, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @param str
	 * @return
	 * @since 1.X
	 */
	public static int countLines(String str) {
		String[] lines = str.split("\r\n|\r|\n");
		return lines.length;
	}

	public static String paddingZeroToNumber(Integer number, Integer totalLength) {

		return String.format("%0" + totalLength + "d", number);
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 21/12/2015, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que valida el nombre de un archivo, segun el comience con o el
	 * termine con
	 * 
	 * @param filename
	 *            Nombre de archivo a validar
	 * @param starWith
	 *            Cadena comience con
	 * @param endsWith
	 *            Cadena de termine con
	 * @return
	 * @since 1.X
	 */
	public static Boolean validateFilename(String filename, String starWith,
			String endsWith) {
		if (filename.startsWith(starWith) && filename.endsWith(endsWith)) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	/**
	 * BICE
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 DD/10/2018, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que valida el nombre de un archivo, segun el comience con o el
	 * termine con
	 * 
	 * @param filename
	 *            Nombre de archivo a validar
	 * @param starWith
	 *            Cadena comience con
	 * @param endsWith
	 *            Cadena de termine con
	 * @return
	 * @since 1.X
	 */
	public static Boolean validateFilenameBice(String filename,
			String starWith, String endsWith) {
						
			if (filename.substring(8).startsWith(starWith.substring(8)) && filename.endsWith(endsWith)) {
				return Boolean.TRUE;
			}
		
		return Boolean.FALSE;
	}
	/**
	 * BICE CTR
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 DD/10/2018, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que valida el nombre de un archivo de control, segun el comience con o el
	 * termine con
	 * 
	 * @param filename
	 *            Nombre de archivo a validar
	 * @param starWith
	 *            Cadena comience con
	 * @param endsWith
	 *            Cadena de termine con
	 * @return
	 * @since 1.X
	 */
	public static Boolean validateFilenameBiceCtr(String fileCtrl,
			String starWithCtrl, String endsWithCtrl) {

		if (fileCtrl.substring(8).startsWith(starWithCtrl.substring(8))
				&& fileCtrl.endsWith(endsWithCtrl)) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	/**
	 * AVANCES
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 DD/10/2018, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que valida el nombre de un archivo, segun el comience con o el
	 * termine con
	 * 
	 * @param filename
	 *            Nombre de archivo a validar
	 * @param starWith
	 *            Cadena comience con
	 * @param endsWith
	 *            Cadena de termine con
	 * @return
	 * @since 1.X
	 */
	public static Boolean validateFilenameAvances(String filename,
			String starWith) {

		if (filename.substring(8).startsWith(starWith.substring(8))) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @param auxString
	 * @return
	 * @since 1.X
	 */
	public static String getStringNull(final String auxString) {

		if (auxString == null) {
			return "";
		} else {
			return auxString;
		}
	}

	/**
	 * Decodifica los codigos de los tipo de acciones para que el sistema los
	 * entienda de codTbk a sbpay
	 * 
	 * Si lee lo cambia por 05_00 ---------> 01_00 | 25_00 ---------> 02_00 |
	 * 07_00 ---------> 03_00 | 27_00 ---------> 04_00 |
	 * 
	 * @param string
	 * @return
	 */
	public static String obtieneCodTrxOnussbpayPorCodTransbank(String codigoTbk) {

		String code = "";

		// COMPRA 05_00 -- > 01_00
		if (CodigoTransaccionOnusType.COD_TRANSACCION_COMPRAS_ONUS
				.getCodigoTransbank().equalsIgnoreCase(codigoTbk)) {
			code = CodigoTransaccionOnusType.COD_TRANSACCION_COMPRAS_ONUS
					.getCodigosbpay();

			// DEVOLUCION DE COMPRA 25 --- > 02_00
		} else if (CodigoTransaccionOnusType.COD_TRANSACCION_DEVOLUCION_COMPRA_ONUS
				.getCodigoTransbank().equalsIgnoreCase(codigoTbk)) {
			code = CodigoTransaccionOnusType.COD_TRANSACCION_DEVOLUCION_COMPRA_ONUS
					.getCodigosbpay();
		}

		// COMPRA CREDITO 07 --> 03_00
		else if (CodigoTransaccionOnusType.COD_TRANSACCION_AVANCE_ONUS
				.getCodigoTransbank().equalsIgnoreCase(codigoTbk)) {
			code = CodigoTransaccionOnusType.COD_TRANSACCION_AVANCE_ONUS
					.getCodigosbpay();
		}

		// COMPRA CREDITO 27 --> 04_00
		else if (CodigoTransaccionOnusType.COD_TRANSACCION_DEVOLUCION_AVANCE_ONUS
				.getCodigoTransbank().equalsIgnoreCase(codigoTbk)) {
			code = CodigoTransaccionOnusType.COD_TRANSACCION_DEVOLUCION_AVANCE_ONUS
					.getCodigosbpay();
		}else{
			code=codigoTbk;
		}
		
		
		return code;
	}

	/**
	 * Decodifica los codigos de los tipo de acciones para que el sistema los
	 * entienda de abdin a cod Transbank
	 * 
	 * Si lee lo cambia por 01_00 ---------> 05 02_00 ---------> 25 03_00
	 * ---------> 07 04_00 ---------> 27
	 * 
	 * @param string
	 * @return
	 */
	public static String obtieneCodTrxOnusTbkPorCodsbpay(
			String codTrxOnussbpay) {

		String code = null;

		// COMPRA 01_00 -- > 05
		if (CodigoTransaccionOnusType.COD_TRANSACCION_COMPRAS_ONUS
				.getCodigosbpay().equalsIgnoreCase(codTrxOnussbpay)) {
			code = CodigoTransaccionOnusType.COD_TRANSACCION_COMPRAS_ONUS
					.getCodigoTransbank();

			// DEVOLUCION DE COMPRA 02_00 --- > 25
		} else if (CodigoTransaccionOnusType.COD_TRANSACCION_DEVOLUCION_COMPRA_ONUS
				.getCodigosbpay().equalsIgnoreCase(codTrxOnussbpay)) {
			code = CodigoTransaccionOnusType.COD_TRANSACCION_DEVOLUCION_COMPRA_ONUS
					.getCodigoTransbank();
		}

		// COMPRA CREDITO 03_00 --> 07
		else if (CodigoTransaccionOnusType.COD_TRANSACCION_AVANCE_ONUS
				.getCodigosbpay().equalsIgnoreCase(codTrxOnussbpay)) {
			code = CodigoTransaccionOnusType.COD_TRANSACCION_AVANCE_ONUS
					.getCodigoTransbank();
		}

		// COMPRA CREDITO 04_00 --> 27
		else if (CodigoTransaccionOnusType.COD_TRANSACCION_DEVOLUCION_AVANCE_ONUS
				.getCodigosbpay().equalsIgnoreCase(codTrxOnussbpay)) {
			code = CodigoTransaccionOnusType.COD_TRANSACCION_DEVOLUCION_AVANCE_ONUS
					.getCodigoTransbank();
		}

		return code;
	}

	public static void borrarArchivo(String pathOri, String pathDest) {
		try {

			File file = new File(pathDest);

			if (file.delete()) {
				LOGGER.info(file.getName() + " is deleted!");
				moveFile(pathOri, pathDest);
			} else {
				LOGGER.info("Delete operation is failed.");

			}

		} catch (Exception e) {

			LOGGER.warn(e);

		}

	}
	
	public static String ajustCadena(String cadena, String caracter, int cantidad){
		String aux = cadena;
		int logitud = aux.length();
		if (logitud < cantidad) {
			for(int i = logitud; i < cantidad; i++){
				aux+=caracter;
			}
		}
		return aux;
    }
	
	public static boolean copiarArchivo(String fromFile, String toFile) {
        File origin = new File(fromFile);
        File destination = new File(toFile);
        if (origin.exists()) {
            try {
                InputStream in = new FileInputStream(origin);
                OutputStream out = new FileOutputStream(destination);
                // We use a buffer for the copy (Usamos un buffer para la copia).
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                in.close();
                out.close();
                return true;
            } catch (IOException ioe) {
                ioe.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
    }
	
	public static boolean borrarArchivo (String args)  
	{   
		File f= new File(args);          
		if(f.delete()) {
			return true;
		}else{
			return false;
		}
	}

}
