package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

public class TmpRechazosDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long sid;
	private String merchantCodeOriginal;
	private String merchantCodeIc;
	private String merchantName;
	private String referenceNro;
	private String transactionCode;
	private String authNro;
	private String transactionDate;
	private String cardNro;
	private String debitCredit;
	private String currency;
	private String grossAmount;
	private String rejectReason;
	private String operador;
	
	public long getSid() {
		return sid;
	}
	public void setSid(long sid) {
		this.sid = sid;
	}
	public String getMerchantCodeOriginal() {
		return merchantCodeOriginal;
	}
	public void setMerchantCodeOriginal(String merchantCodeOriginal) {
		this.merchantCodeOriginal = merchantCodeOriginal;
	}
	public String getMerchantCodeIc() {
		return merchantCodeIc;
	}
	public void setMerchantCodeIc(String merchantCodeIc) {
		this.merchantCodeIc = merchantCodeIc;
	}
	public String getMerchantName() {
		return merchantName;
	}
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}
	public String getReferenceNro() {
		return referenceNro;
	}
	public void setReferenceNro(String referenceNro) {
		this.referenceNro = referenceNro;
	}
	public String getTransactionCode() {
		return transactionCode;
	}
	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}
	public String getAuthNro() {
		return authNro;
	}
	public void setAuthNro(String authNro) {
		this.authNro = authNro;
	}
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getCardNro() {
		return cardNro;
	}
	public void setCardNro(String cardNro) {
		this.cardNro = cardNro;
	}
	public String getDebitCredit() {
		return debitCredit;
	}
	public void setDebitCredit(String debitCredit) {
		this.debitCredit = debitCredit;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getGrossAmount() {
		return grossAmount;
	}
	public void setGrossAmount(String grossAmount) {
		this.grossAmount = grossAmount;
	}
	public String getRejectReason() {
		return rejectReason;
	}
	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}
	
	public String getOperador() {
		return operador;
	}
	public void setOperador(String operador) {
		this.operador = operador;
	}
	
	@Override
	public String toString() {
		return "TmpRechazosDTO [sid=" + sid + ", merchantCodeOriginal="
				+ merchantCodeOriginal + ", merchantCodeIc=" + merchantCodeIc
				+ ", merchantName=" + merchantName + ", referenceNro="
				+ referenceNro + ", transactionCode=" + transactionCode
				+ ", authNro=" + authNro + ", transactionDate="
				+ transactionDate + ", cardNro=" + cardNro + ", debitCredit="
				+ debitCredit + ", currency=" + currency + ", grossAmount="
				+ grossAmount + ", rejectReason=" + rejectReason + ", operador="+operador+ "]";
	}
	
}
