package com.sbpay.sgi.cron.utils.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dto.ICCodeHomologationDTO;
import com.sbpay.sgi.cron.dto.LogTransaccionAutorizada;
import com.sbpay.sgi.cron.dto.TransacionAutorizada;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.MsgErrorFile;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class TransaccionAutorReader implements TransaccionAutorFileUtil {

    /** VARIABLE PARA EL LOGER */
    private static final Logger LOGGER = Logger
            .getLogger( TransaccionAutorReader.class );
    
    // LISTA DE CODIGOS IC
    private List<ICCodeHomologationDTO> listaCodigosIc = null;
    private String detalleTrxLeidasConError = "";
    

@Override
  public List<TransacionAutorizada> readLogTrxAutorFile(String fullPath, String starWith)
      throws AppException {
    BufferedReader buffReader = null;
    List<TransacionAutorizada> trxAutList = null;

    try {
      buffReader = new BufferedReader(new FileReader(fullPath));
      String[] tokens = fullPath.split("[\\\\|x/]");
      String filename = tokens[tokens.length - 1];

      trxAutList = readFile(buffReader, filename, starWith);

    } catch (IOException ioe) {
      throw new AppException("Ha ocurrido un error al leer el archivo", ioe);
    } finally {
      try {
        if (buffReader != null) {
          buffReader.close();
        }
      } catch (IOException ioe1) {
        throw new AppException("Ha ocurrido un error al cerrar el archivo", ioe1);
      }
    }

    return trxAutList;
  }

  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 2/12/2015, (ACL-sbpay) - versión inicial
   * </ul>
   * <p>
   * 
   * @param listaTransacciones
   * @param buffReader
   * @param line
   * @param filename
   * @return
   * @throws AppException
   * @throws IOException
   * @since 1.X
   */
  private List<TransacionAutorizada> readFile(BufferedReader buffReader, String filename,
      String starWith) throws AppException, IOException {

    String line = buffReader.readLine();
    List<TransacionAutorizada> listaTransacciones = new ArrayList<TransacionAutorizada>();

    // RECORRE EL FILE LEYENDO LINEA POR LINEA
        while ( line != null ) {
            try {
                TransacionAutorizada tr = parseToTransaccion( line );
                listaTransacciones.add( tr );
            }
            catch ( AppException e ) {
                LOGGER.error( e.getMessage(), e );
                detalleTrxLeidasConError = detalleTrxLeidasConError.concat( "\n -" ).concat( e.getMessage()); 
            }
            line = buffReader.readLine();
        }
        
    LOGGER.info("-->Nº TRX AUTORIZADAS  = " + listaTransacciones.size());
    return listaTransacciones;
  }

  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
   * </ul>
   * <p>
   * Metodo que convierte una linea de string a una transaccion
   * 
   * @param line
 * @throws AppException 
   * @since 1.X
   */
  private TransacionAutorizada parseToTransaccion(String line) throws AppException {
    TransacionAutorizada transaccion = new TransacionAutorizada();
    String separador = ConstantesUtil.PUNTO_COMA.toString();
    String[] transaccionSeparada = line.split(separador);

    if (transaccionSeparada != null && transaccionSeparada.length == 17 ) {
    	
    // SE TRADUCE CODIGO DE TRANSACCION DESDE IC A CODIGO TRANSBANK (05_00)
     String tranbankCode = traducirCodigoDeIcATransbank(transaccionSeparada[0]);
    	
      transaccion.setCodigoTransaccion(tranbankCode);
      transaccion.setNumeroTarjeta(transaccionSeparada[1]);
      transaccion.setFechaProceso(transaccionSeparada[2]);
      transaccion.setTipoVenta(transaccionSeparada[3]);
      transaccion.setNumCuotas(transaccionSeparada[4]);
      transaccion.setNumMicrofilm(transaccionSeparada[5]);
      transaccion.setNumComercio(transaccionSeparada[6]);
      transaccion.setFechaCompra(transaccionSeparada[7]);
      transaccion.setMontoTransac(transaccionSeparada[8]);
      transaccion.setValorCuota(transaccionSeparada[9]);
      transaccion.setNombreComercio(transaccionSeparada[10]);
      transaccion.setCiudadComercio(transaccionSeparada[11]);
      transaccion.setRubroComercio(transaccionSeparada[12]);
      transaccion.setCodAutorizacion(transaccionSeparada[13]);
      transaccion.setCodLocal(transaccionSeparada[14]);
      transaccion.setCodPos(transaccionSeparada[15]);
      transaccion.setFechaHoraTrx(transaccionSeparada[16]);

    }
    return transaccion;
  }

  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
   * </ul>
   * <p>
   * 
   * @param file
   * @param starWith
   * @return
   * @throws AppException
   * @see com.sbpay.sgi.cron.utils.file.TransaccionAutorFileUtil#readLogTrxAutorFile(java.io.File,
   *      java.lang.String)
   * @since 1.X
   */
  @Override
  public LogTransaccionAutorizada readLogTrxAutorFile(File file, String starWith, List<ICCodeHomologationDTO> listaCodigosIc)
      throws AppException {
    BufferedReader buffReader = null;
    LogTransaccionAutorizada logTrxAutor = new LogTransaccionAutorizada();
    List<TransacionAutorizada> trxAutList = null;
    FileReader fr = null;

    this.listaCodigosIc = listaCodigosIc;
    try {

      if (file == null) {
        throw new AppException(MsgErrorFile.ERROR_READ_FILE_IS_NULL.toString());
      }

      String ruta = file.getAbsolutePath();
      String filename = file.getName();
      fr = new FileReader(ruta);
      buffReader = new BufferedReader(fr);
      trxAutList = readFile(buffReader, filename, starWith);

    } catch (IOException ioe) {
      throw new AppException("Ha ocurrido un error al leer el archivo : " + ioe.getMessage(), ioe);
    } finally {
      try {
        if (buffReader != null) {
          buffReader.close();
        }
        if (fr != null) {
          fr.close();
        }
      } catch (IOException ioe1) {
        throw new AppException("Ha ocurrido un error al cerrar el archivo", ioe1);
      }
    }
    logTrxAutor.setTrxList(trxAutList);
    logTrxAutor.setDetalleLectura( detalleTrxLeidasConError );
    return logTrxAutor;
  }
  
  
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * Metodo que traduce un codigo de Ic a codigo Transbank
     * 
     * @param icCode
     *            : Codigo ic
     * @return : Codigo de transbank
     * @throws AppException
     * @since 1.X
     */
    private String traducirCodigoDeIcATransbank( String icCode )
            throws AppException {
        String[] codTrx = null;
        
        for ( ICCodeHomologationDTO instanceIc : listaCodigosIc ) {
            if ( instanceIc.isActivo() && instanceIc.getCodigoIC() != null
                    && instanceIc.getCodigoIC().equalsIgnoreCase( icCode ) ) {
                codTrx = instanceIc.getCodigoTransbank().split(
                        ConstantesUtil.UNDERSCORE.toString() );
                break;
            }
        }
        
        if ( codTrx == null ) {
            throw new AppException(
                    "No se ha encontrado codigo de transaccion de IC : "+icCode+", homologado a los codigos de transbank en la tabla TBL_PRTS_IC_CODE." );
        }
        
        return codTrx[0];
    }



}
