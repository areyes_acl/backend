package com.sbpay.sgi.cron.utils.file;

import java.io.File;
import java.util.List;

import com.sbpay.sgi.cron.dto.ComisionesPagoDTO;

public interface ComisionesFileUtil {

	public List<ComisionesPagoDTO> readComisionesLine(File file)throws Exception;
	
	public Boolean validarArchivo(File file)throws Exception;
}
