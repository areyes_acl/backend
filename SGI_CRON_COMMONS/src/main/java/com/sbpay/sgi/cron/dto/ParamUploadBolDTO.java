package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class ParamUploadBolDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private DataFTP dataFTP;
    private String pathFtpUpload;
    private String pathFtpDownload;
    private String pathLocalServer;
    private String pathLocalServerBkp;
    private String pathLocalServerError;
    private String formatFilename;
    private String formatExtName;
    private String formatExtNameCtrl;

    public DataFTP getDataFTP() {
	return dataFTP;
    }

    public void setDataFTP(DataFTP dataFTP) {
	this.dataFTP = dataFTP;
    }

    public String getPathFtpDownload() {
	return pathFtpDownload;
    }

    public void setPathFtpDownload(String pathFtpDownload) {
	this.pathFtpDownload = pathFtpDownload;
    }

    public String getFormatFilename() {
	return formatFilename;
    }

    public void setFormatFilename(String formatFilename) {
	this.formatFilename = formatFilename;
    }

    public String getFormatExtName() {
	return formatExtName;
    }

    public void setFormatExtName(String formatExtName) {
	this.formatExtName = formatExtName;
    }

    public String getFormatExtNameCtrl() {
	return formatExtNameCtrl;
    }

    public void setFormatExtNameCtrl(String formatExtNameCtrl) {
	this.formatExtNameCtrl = formatExtNameCtrl;
    }

    public String getPathFtpUpload() {
	return pathFtpUpload;
    }

    public void setPathFtpUpload(String pathFtpUpload) {
	this.pathFtpUpload = pathFtpUpload;
    }

    public String getPathLocalServer() {
	return pathLocalServer;
    }

    public void setPathLocalServer(String pathLocalServer) {
	this.pathLocalServer = pathLocalServer;
    }

    public String getPathLocalServerBkp() {
	return pathLocalServerBkp;
    }

    public void setPathLocalServerBkp(String pathLocalServerBkp) {
	this.pathLocalServerBkp = pathLocalServerBkp;
    }

    public String getPathLocalServerError() {
	return pathLocalServerError;
    }

    public void setPathLocalServerError(String pathLocalServerError) {
	this.pathLocalServerError = pathLocalServerError;
    }

    @Override
    public String toString() {
	return "ParamUploadBolDTO [dataFTP=" + dataFTP + ", pathFtpUpload="
		+ pathFtpUpload + ", pathFtpDownload=" + pathFtpDownload
		+ ", pathLocalServer=" + pathLocalServer
		+ ", pathLocalServerBkp=" + pathLocalServerBkp
		+ ", pathLocalServerError=" + pathLocalServerError
		+ ", formatFilename=" + formatFilename + ", formatExtName="
		+ formatExtName + ", formatExtNameCtrl=" + formatExtNameCtrl
		+ "]";
    }

}