package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 16/12/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class DataFTP implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1116159800514138149L;
    private String ipHost;
    private Integer port;
    private String user;
    private String password;
    
    public DataFTP() {
        super();
    }
    
    public String getIpHost() {
        return ipHost;
    }
    
    public void setIpHost( String ipHost ) {
        this.ipHost = ipHost;
    }
    
    public Integer getPort() {
        return port;
    }
    
    public void setPort( Integer port ) {
        this.port = port;
    }
    
    public String getUser() {
        return user;
    }
    
    public void setUser( String user ) {
        this.user = user;
    }
    
    public String getPassword() {
        return password;
    }
    
    public void setPassword( String password ) {
        this.password = password;
    }
    
    @Override
    public String toString() {
        return "DataFTP [ipHost=" + ipHost + ", port=" + port + ", user="
                + user + ", password=" + password + "]";
    }
    
}
