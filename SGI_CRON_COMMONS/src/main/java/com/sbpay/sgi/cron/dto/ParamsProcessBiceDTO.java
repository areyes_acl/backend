package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/10/2018, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class ParamsProcessBiceDTO implements Serializable{
	
	/**
     * 
     */
	private static final long serialVersionUID = 4081693614785403716L;
	private String formatFileNameBice;
	private String pathAbcBice;
	private String pathAbcBiceError;
	private String pathAbcBiceBkp;
	
	public String getFormatFileNameBice() {
		return formatFileNameBice;
	}
	public void setFormatFileNameBice(String formatFileNameBice) {
		this.formatFileNameBice = formatFileNameBice;
	}
	public String getPathAbcBice() {
		return pathAbcBice;
	}
	public void setPathAbcBice(String pathAbcBice) {
		this.pathAbcBice = pathAbcBice;
	}
	public String getPathAbcBiceError() {
		return pathAbcBiceError;
	}
	public void setPathAbcBiceError(String pathAbcBiceError) {
		this.pathAbcBiceError = pathAbcBiceError;
	}
	public String getPathAbcBiceBkp() {
		return pathAbcBiceBkp;
	}
	public void setPathAbcBiceBkp(String pathAbcBiceBkp) {
		this.pathAbcBiceBkp = pathAbcBiceBkp;
	}
	
	@Override
	public String toString() {
		return "ParamsProcessBiceDTO [formatFileNameBice=" + formatFileNameBice
				+ ", pathAbcBice=" + pathAbcBice + ", pathAbcBiceError="
				+ pathAbcBiceError + ", pathAbcBiceBkp=" + pathAbcBiceBkp + "]";
	}

}
