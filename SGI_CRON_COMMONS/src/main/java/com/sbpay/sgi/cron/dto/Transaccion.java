package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 23/11/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * Clase que guardara una transaccion completa contemplando para esto
 * el todos los registros involucrados.
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class Transaccion implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = -8665924641652098866L;
    private int numeroTransaccion;
    private String rutsbpay;
    private RegistroDTO registro00;
    private RegistroDTO registro01;
    private RegistroDTO registro05;
    private RegistroDTO registro07;
    private RegistroDTO registro1020;
    
    public String getRutsbpay() {
        return rutsbpay;
    }
    
    public void setRutsbpay( String rutsbpay ) {
        this.rutsbpay = rutsbpay;
    }
    
    public int getNumeroTransaccion() {
        return numeroTransaccion;
    }
    
    public void setNumeroTransaccion( int numeroTransaccion ) {
        this.numeroTransaccion = numeroTransaccion;
    }
    
    public RegistroDTO getRegistro00() {
        return registro00;
    }
    
    public void setRegistro00( RegistroDTO registro00 ) {
        this.registro00 = registro00;
    }
    
    public RegistroDTO getRegistro01() {
        return registro01;
    }
    
    public void setRegistro01( RegistroDTO registro01 ) {
        this.registro01 = registro01;
    }
    
    public RegistroDTO getRegistro05() {
        return registro05;
    }
    
    public void setRegistro05( RegistroDTO registro05 ) {
        this.registro05 = registro05;
    }
    
    public RegistroDTO getRegistro07() {
        return registro07;
    }
    
    public void setRegistro07( RegistroDTO registro07 ) {
        this.registro07 = registro07;
    }
    
    public RegistroDTO getRegistro1020() {
		return registro1020;
	}

	public void setRegistro1020(RegistroDTO registro1020) {
		this.registro1020 = registro1020;
	}

	@Override
    public String toString() {
        return "Transaccion [numeroTransaccion=" + numeroTransaccion
                + ", rutsbpay=" + rutsbpay + ", registro00=" + registro00
                + ", registro01=" + registro01 + ", registro05=" + registro05
                + ", registro07=" + registro07 + ", registro1020=" + registro1020 +"]";
    }
    
}
