package com.sbpay.sgi.cron.dao;

import java.sql.SQLException;

/**
 * <p>
 * Clase Factory Dao Commons.
 * <p>
 * <br/>
 * @param <T>
 * @author Dante Medina (ACL) en nombre de sbpay
 * @version 1.6
 * @since 2015-11-09
 */
public abstract class BaseDaoFactory<T> {
    /**
     * Manejo de conexiones.
     */
    protected BaseDaoFactory() {
        super();
    }

    /**
     * Metodo para obtener un DAO.
     * @return una implementacion de T.
     * @throws SQLException Error de SQL.
     */
    public abstract T getNewEntidadDao() throws SQLException;
}