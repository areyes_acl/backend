package com.sbpay.sgi.cron.dto;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class LogTransaccionAutorizada implements Serializable {
    
    /**
   * 
   */
    private static final long serialVersionUID = -8721266764009601962L;
    private String filename;
    private String detalleLectura;
    private List<TransacionAutorizada> trxList;
    
    public String getFilename() {
        return filename;
    }
    
    public void setFilename( String filename ) {
        this.filename = filename;
    }
    
    public String getDetalleLectura() {
        return detalleLectura;
    }
    
    public void setDetalleLectura( String detalleLectura ) {
        this.detalleLectura = detalleLectura;
    }
    
    public List<TransacionAutorizada> getTrxList() {
        return trxList;
    }
    
    public void setTrxList( List<TransacionAutorizada> trxList ) {
        this.trxList = trxList;
    }
    
    @Override
    public String toString() {
        return "LogTransaccionAutorizada [filename=" + filename
                + ", detalleLectura=" + detalleLectura + ", trxList=" + trxList
                + "]";
    }
    
}
