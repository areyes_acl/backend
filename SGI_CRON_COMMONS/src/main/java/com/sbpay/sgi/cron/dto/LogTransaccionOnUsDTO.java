package com.sbpay.sgi.cron.dto;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 25/01/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class LogTransaccionOnUsDTO implements Serializable {
    
    /**
   * 
   */
    private static final long serialVersionUID = -8721266764009601962L;
    private String filename;
    private List<TransaccionOnUsDTO> trxList;
    private String detalleLectura;
    
    public String getFilename() {
        return filename;
    }
    
    public void setFilename( String filename ) {
        this.filename = filename;
    }
    
    public List<TransaccionOnUsDTO> getTrxList() {
        return trxList;
    }
    
    public void setTrxList( List<TransaccionOnUsDTO> trxList ) {
        this.trxList = trxList;
    }
    
    public String getDetalleLectura() {
        return detalleLectura;
    }
    
    public void setDetalleLectura( String detalleLectura ) {
        this.detalleLectura = detalleLectura;
    }
    
    @Override
    public String toString() {
        return "LogTransaccionOnUsDTO [filename=" + filename + ", trxList="
                + trxList + ", detalleLectura=" + detalleLectura + "]";
    }
    
}
