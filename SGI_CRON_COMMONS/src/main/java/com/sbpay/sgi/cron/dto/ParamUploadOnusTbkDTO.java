package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

public class ParamUploadOnusTbkDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String pathOnusTbkInc;
	private String ftpPathIn;
	private String pathBackup;
	private String pathError;
	private String extOnusTbkFile;
	private String nameOnusTbkFile;
	private DataFTP dataFTP;

	public DataFTP getDataFTP() {
		return dataFTP;
	}

	public String getFtpPathIn() {
		return ftpPathIn;
	}

	public void setFtpPathIn(String ftpPathIn) {
		this.ftpPathIn = ftpPathIn;
	}

	public String getPathBackup() {
		return pathBackup;
	}

	public void setPathBackup(String pathBackup) {
		this.pathBackup = pathBackup;
	}

	public String getPathError() {
		return pathError;
	}

	public void setPathError(String pathError) {
		this.pathError = pathError;
	}

	public void setDataFTP(DataFTP dataFTP) {
		this.dataFTP = dataFTP;
	}

	public ParamUploadOnusTbkDTO() {
		// TODO Auto-generated constructor stub
	}

	public String getPathOnusTbkInc() {
		return pathOnusTbkInc;
	}

	public void setPathOnusTbkInc(String pathOnusTbkInc) {
		this.pathOnusTbkInc = pathOnusTbkInc;
	}

	public String getExtOnusTbkFile() {
		return extOnusTbkFile;
	}

	public void setExtOnusTbkFile(String extOnusTbkFile) {
		this.extOnusTbkFile = extOnusTbkFile;
	}

	public String getNameOnusTbkFile() {
		return nameOnusTbkFile;
	}

	public void setNameOnusTbkFile(String nameOnusTbkFile) {
		this.nameOnusTbkFile = nameOnusTbkFile;
	}

	@Override
	public String toString() {
		return "ParamUploadOnusTbkDTO [pathOnusTbkInc=" + pathOnusTbkInc
				+ ", ftpPathIn=" + ftpPathIn + ", pathBackup=" + pathBackup
				+ ", pathError=" + pathError + ", extOnusTbkFile="
				+ extOnusTbkFile + ", nameOnusTbkFile=" + nameOnusTbkFile
				+ ", dataFTP=" + dataFTP + "]";
	}

}
