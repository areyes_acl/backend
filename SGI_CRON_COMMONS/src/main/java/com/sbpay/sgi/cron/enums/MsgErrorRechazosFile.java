package com.sbpay.sgi.cron.enums;

public enum MsgErrorRechazosFile {
	WARNING_NO_FILE_RECHAZOS_TITLE("ADVERTENCIA"),
	WARNING_NO_FILE_RECHAZO_CAUSE(" No se ha podido procesar ningun archivo de Rechazo, Las posibles causas: \n "
	                                + "1- Por algun error provocado en el cron. \n"
	                                + "2- Por que no fue posible actualizar la base de datos"), 
	ERROR_READ_RECHAZOS_TITLE("ADVERTENCIA");

	/**
	 * Valor ENUM.
	 */
	private final String text;

	/**
	 * Constructor.
	 * 
	 * @param text
	 */
	private MsgErrorRechazosFile(final String text) {
		this.text = text;
	}

	/**
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return text;
	}
}
