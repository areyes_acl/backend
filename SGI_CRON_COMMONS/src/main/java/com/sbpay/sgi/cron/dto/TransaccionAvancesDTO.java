package com.sbpay.sgi.cron.dto;

import java.io.Serializable;


public class TransaccionAvancesDTO implements Serializable{
	/**
	   * 
	   */
		private static final long serialVersionUID = 8887889677674146517L;
		private long sid;
		private String fechaTransferencia;
		private String codAutorizacion;
		private String numTrx;
		private String numTarjeta;
		private String monto;
		private String estado;
		private String numCanal;
		private String montoSeguro;
		
		public long getSid() {
			return sid;
		}
		public void setSid(long sid) {
			this.sid = sid;
		}
		public String getFechaTransferencia() {
			return fechaTransferencia;
		}
		public void setFechaTransferencia(String fechaTransferencia) {
			this.fechaTransferencia = fechaTransferencia;
		}
		public String getCodAutorizacion() {
			return codAutorizacion;
		}
		public void setCodAutorizacion(String codAutorizacion) {
			this.codAutorizacion = codAutorizacion;
		}
		public String getNumTrx() {
			return numTrx;
		}
		public void setNumTrx(String numTrx) {
			this.numTrx = numTrx;
		}
		public String getNumTarjeta() {
			return numTarjeta;
		}
		public void setNumTarjeta(String numTarjeta) {
			this.numTarjeta = numTarjeta;
		}
		public String getMonto() {
			return monto;
		}
		public void setMonto(String monto) {
			this.monto = monto;
		}
		public String getEstado() {
			return estado;
		}
		public void setEstado(String estado) {
			this.estado = estado;
		}
		public static long getSerialversionuid() {
			return serialVersionUID;
		}
		
		
		public String getNumCanal() {
			return numCanal;
		}
		public void setNumCanal(String numCanal) {
			this.numCanal = numCanal;
		}

		public String getMontoSeguro() {
			return montoSeguro;
		}
		public void setMontoSeguro(String montoSeguro) {
			this.montoSeguro = montoSeguro;
		}
		@Override
		public String toString() {
			return "TransaccionAvancesDTO [sid=" + sid
					+ ", fechaTransferencia=" + fechaTransferencia
					+ ", codAutorizacion=" + codAutorizacion + ", numTrx="
					+ numTrx + ", numTarjeta=" + numTarjeta + ", monto="
					+ monto + ", estado=" + estado + ", numCanal=" + numCanal
					+ ", montoSeguro=" + montoSeguro + "]";
		}	
		
}
