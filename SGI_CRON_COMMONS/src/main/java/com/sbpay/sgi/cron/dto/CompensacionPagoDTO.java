package com.sbpay.sgi.cron.dto;

public class CompensacionPagoDTO {

    private String fecPago;
    private Integer cantidad;
    private Double montoPago;
    private Double neta;
    private Double iva;
    private Double total;
    private Double neto;
    private Integer tipoTransaccion;

    public Integer getTipoTransaccion() {
        return tipoTransaccion;
    }

    public void setTipoTransaccion(Integer tipoTransaccion) {
        this.tipoTransaccion = tipoTransaccion;
    }

    public String getFecPago() {
        return fecPago;
    }

    public void setFecPago(String fecPago) {
        this.fecPago = fecPago;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Double getMontoPago() {
        return montoPago;
    }

    public void setMontoPago(Double montoPago) {
        this.montoPago = montoPago;
    }

    public Double getNeta() {
        return neta;
    }

    public void setNeta(Double neta) {
        this.neta = neta;
    }

    public Double getIva() {
        return iva;
    }

    public void setIva(Double iva) {
        this.iva = iva;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Double getNeto() {
        return neto;
    }

    public void setNeto(Double neto) {
        this.neto = neto;
    }

    @Override
    public String toString() {
        return "CompensacionPagoDTO [fecPago=" + fecPago + ", cantidad="
            + cantidad + ", montoPago=" + montoPago + ", neta=" + neta
            + ", iva=" + iva + ", total=" + total + ", neto=" + neto
            + ", tipoTransaccion=" + tipoTransaccion + "]";
    }
    
    

}
