package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

public class ParamUploadIncomingVisaDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private DataFTP dataFTP;
	private String ftpPathIcEntrada;
	private String pathVisaInc;
	private String filenameOutVisa;
	private String extFileOutVisa;
	private String extFileCtrOutVisa;
	private String filenameOutVisaToIc;
	private String extFileOutVisaToIc;
	private String extFileCtrOutVisaToIc;
	private String pathVisaIncError;

	public DataFTP getDataFTP() {
		return dataFTP;
	}

	public void setDataFTP(DataFTP dataFTP) {
		this.dataFTP = dataFTP;
	}

	public String getFtpPathIcEntrada() {
		return ftpPathIcEntrada;
	}

	public void setFtpPathIcEntrada(String ftpPathIcEntrada) {
		this.ftpPathIcEntrada = ftpPathIcEntrada;
	}

	public String getPathVisaInc() {
		return pathVisaInc;
	}

	public void setPathVisaInc(String pathVisaInc) {
		this.pathVisaInc = pathVisaInc;
	}

	public String getFilenameOutVisa() {
		return filenameOutVisa;
	}

	public void setFilenameOutVisa(String filenameOutVisa) {
		this.filenameOutVisa = filenameOutVisa;
	}

	public String getExtFileOutVisa() {
		return extFileOutVisa;
	}

	public void setExtFileOutVisa(String extFileOutVisa) {
		this.extFileOutVisa = extFileOutVisa;
	}

	public String getPathVisaIncError() {
		return pathVisaIncError;
	}

	public void setPathVisaIncError(String pathVisaIncError) {
		this.pathVisaIncError = pathVisaIncError;
	}

	public String getExtFileCtrOutVisa() {
		return extFileCtrOutVisa;
	}

	public void setExtFileCtrOutVisa(String extFileCtrOutVisa) {
		this.extFileCtrOutVisa = extFileCtrOutVisa;
	}

	public String getFilenameOutVisaToIc() {
		return filenameOutVisaToIc;
	}

	public void setFilenameOutVisaToIc(String filenameOutVisaToIc) {
		this.filenameOutVisaToIc = filenameOutVisaToIc;
	}

	public String getExtFileOutVisaToIc() {
		return extFileOutVisaToIc;
	}

	public void setExtFileOutVisaToIc(String extFileOutVisaToIc) {
		this.extFileOutVisaToIc = extFileOutVisaToIc;
	}

	public String getExtFileCtrOutVisaToIc() {
		return extFileCtrOutVisaToIc;
	}

	public void setExtFileCtrOutVisaToIc(String extFileCtrOutVisaToIc) {
		this.extFileCtrOutVisaToIc = extFileCtrOutVisaToIc;
	}

	@Override
	public String toString() {
		return "ParamUploadIncomingVisaDTO [dataFTP=" + dataFTP
				+ ", ftpPathIcEntrada=" + ftpPathIcEntrada + ", pathVisaInc="
				+ pathVisaInc + ", filenameOutVisa=" + filenameOutVisa
				+ ", extFileOutVisa=" + extFileOutVisa + ", pathVisaIncError="
				+ pathVisaIncError + "]";
	}
	
	

}
