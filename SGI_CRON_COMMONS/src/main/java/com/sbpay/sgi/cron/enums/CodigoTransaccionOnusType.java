package com.sbpay.sgi.cron.enums;

public enum CodigoTransaccionOnusType {
	
	
	COD_TRANSACCION_COMPRAS_ONUS("05_00","01_00"),
	COD_TRANSACCION_DEVOLUCION_COMPRA_ONUS("25_00","02_00"),
	COD_TRANSACCION_AVANCE_ONUS("07_00","03_01"),
	COD_TRANSACCION_DEVOLUCION_AVANCE_ONUS("27_00","04_00")
	;
	
	
	/**
	 * Valor ENUM.
	 */
	private final String codigoTransbank;
	private final String codigosbpay;

	/**
	 * Constructor.
	 * 
	 * @param text
	 */
	private CodigoTransaccionOnusType(String codigoTransbank,
			String codigosbpay) {
		this.codigoTransbank = codigoTransbank;
		this.codigosbpay = codigosbpay;
	}

	public String getCodigoTransbank() {
		return codigoTransbank;
	}

	public String getCodigosbpay() {
		return codigosbpay;
	}

	public String getsbpayCodeByTransbankCode(String tbkCode) {
		
		for (CodigoTransaccionOnusType tipoAccion : CodigoTransaccionOnusType.values()) {
			if(tipoAccion.getCodigoTransbank().equalsIgnoreCase(tbkCode)){
				return tipoAccion.getCodigosbpay();
			}
		}
		return null;
	}

	
	/**
	 * Decodifica los codigos de los tipo de acciones para que el sistema los
	 * entienda
	 * 
	 * Si lee lo cambia por 01_00 ---------> 05 
	 * 						02_00 ---------> 25 
	 * 						03_00 ---------> 06 
	 * 					    04_00 ---------> 26
	 * 
	 * @param string
	 * @return
	 */
	public String getTransbankCodeBysbpayCode(String sbpayCode) {
		for (CodigoTransaccionOnusType tipoAccion : CodigoTransaccionOnusType.values()) {
			if(tipoAccion.getCodigosbpay().equalsIgnoreCase(sbpayCode)){
				return tipoAccion.getCodigoTransbank();
			}
		}
		return null;
	}
	
	


}
