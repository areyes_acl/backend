package com.sbpay.sgi.cron.dto;

import com.sbpay.sgi.cron.utils.generals.CommonsUtils;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 20/11/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class Registro07DTO extends RegistroDTO {
    
    /**
     * 
     */
    private static final long serialVersionUID = -4924876189694862634L;
    private String transType;
    private String cardSeqNbr;
    private String termTrnDate;
    private String termCapProf;
    private String termCounCode;
    private String termSerNbr;
    private String unpredNumber;
    private String appTrnCount;
    private String appInterProf;
    private String crypt;
    private String issAppDataB2;
    private String issAppDataB3;
    private String termVerRsult;
    private String issAppDataB4;
    private String cryptAmt;
    private String issAppDataB8;
    private String issAppDataB9;
    private String issAppDataB1;
    private String issAppDataB17;
    private String issAppDataB18;
    private String formFactInd;
    private String issRsult;
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 20/11/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * Constructor
     * 
     * @since 1.X
     */
    public Registro07DTO() {
        
    }
    
    public String getTransType() {
        return transType;
    }
    
    public void setTransType( String transType ) {
        this.transType = transType;
    }
    
    public String getCardSeqNbr() {
        return cardSeqNbr;
    }
    
    public void setCardSeqNbr( String cardSeqNbr ) {
        this.cardSeqNbr = cardSeqNbr;
    }
    
    public String getTermCapProf() {
        return termCapProf;
    }
    
    public void setTermCapProf( String termCapProf ) {
        this.termCapProf = termCapProf;
    }
    
    public String getTermCounCode() {
        return termCounCode;
    }
    
    public void setTermCounCode( String termCounCode ) {
        this.termCounCode = termCounCode;
    }
    
    public String getTermSerNbr() {
        return termSerNbr;
    }
    
    public void setTermSerNbr( String termSerNbr ) {
        this.termSerNbr = termSerNbr;
    }
    
    public String getUnpredNumber() {
        return unpredNumber;
    }
    
    public void setUnpredNumber( String unpredNumber ) {
        this.unpredNumber = unpredNumber;
    }
    
    public String getAppTrnCount() {
        return appTrnCount;
    }
    
    public void setAppTrnCount( String appTrnCount ) {
        this.appTrnCount = appTrnCount;
    }
    
    public String getAppInterProf() {
        return appInterProf;
    }
    
    public void setAppInterProf( String appInterProf ) {
        this.appInterProf = appInterProf;
    }
    
    public String getCrypt() {
        return crypt;
    }
    
    public void setCrypt( String crypt ) {
        this.crypt = crypt;
    }
    
    public String getIssAppDataB2() {
        return issAppDataB2;
    }
    
    public void setIssAppDataB2( String issAppDataB2 ) {
        this.issAppDataB2 = issAppDataB2;
    }
    
    public String getIssAppDataB3() {
        return issAppDataB3;
    }
    
    public void setIssAppDataB3( String issAppDataB3 ) {
        this.issAppDataB3 = issAppDataB3;
    }
    
    public String getTermVerRsult() {
        return termVerRsult;
    }
    
    public void setTermVerRsult( String termVerRsult ) {
        this.termVerRsult = termVerRsult;
    }
    
    public String getIssAppDataB4() {
        return issAppDataB4;
    }
    
    public void setIssAppDataB4( String issAppDataB4 ) {
        this.issAppDataB4 = issAppDataB4;
    }
    
    public String getCryptAmt() {
        return cryptAmt;
    }
    
    public void setCryptAmt( String cryptAmt ) {
        this.cryptAmt = cryptAmt;
    }
    
    public String getIssAppDataB8() {
        return issAppDataB8;
    }
    
    public void setIssAppDataB8( String issAppDataB8 ) {
        this.issAppDataB8 = issAppDataB8;
    }
    
    public String getIssAppDataB9() {
        return issAppDataB9;
    }
    
    public void setIssAppDataB9( String issAppDataB9 ) {
        this.issAppDataB9 = issAppDataB9;
    }
    
    public String getIssAppDataB1() {
        return issAppDataB1;
    }
    
    public void setIssAppDataB1( String issAppDataB1 ) {
        this.issAppDataB1 = issAppDataB1;
    }
    
    public String getIssAppDataB17() {
        return issAppDataB17;
    }
    
    public void setIssAppDataB17( String issAppDataB17 ) {
        this.issAppDataB17 = issAppDataB17;
    }
    
    public String getIssAppDataB18() {
        return issAppDataB18;
    }
    
    public void setIssAppDataB18( String issAppDataB18 ) {
        this.issAppDataB18 = issAppDataB18;
    }
    
    public String getFormFactInd() {
        return formFactInd;
    }
    
    public void setFormFactInd( String formFactInd ) {
        this.formFactInd = formFactInd;
    }
    
    public String getIssRsult() {
        return issRsult;
    }
    
    public void setIssRsult( String issRsult ) {
        this.issRsult = issRsult;
    }
    
    public String getTermTrnDate() {
        return termTrnDate;
    }
    
    public void setTermTrnDate( String termTrnDate ) {
        this.termTrnDate = termTrnDate;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 23/11/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * METODO QUE RETORNA TODO EL REGISTO NUMERO 07 JUNTO
     * 
     * @return
     * @since 1.X
     */
    public String getAllRegister07() {
        StringBuffer str = new StringBuffer();
        str.append( super.getTipoTransaccion() );
        str.append( CommonsUtils.getWhitesSpaceString( 2 ) );
        str.append( super.getTransCode() );
        str.append( this.getTransType() );
        str.append( this.cardSeqNbr );
        str.append( this.termTrnDate );
        str.append( this.termCapProf );
        str.append( this.termCounCode );
        str.append( this.termSerNbr );
        str.append( this.unpredNumber );
        str.append( this.appTrnCount );
        str.append( this.appInterProf );
        str.append( this.crypt );
        str.append( this.issAppDataB2 );
        str.append( this.issAppDataB3 );
        str.append( this.termVerRsult );
        str.append( this.issAppDataB4 );
        str.append( this.cryptAmt );
        str.append( this.issAppDataB8 );
        str.append( this.issAppDataB9 );
        str.append( this.issAppDataB1 );
        str.append( this.issAppDataB17 );
        str.append( this.issAppDataB18 );
        str.append( this.formFactInd );
        str.append( this.issRsult );

        return str.toString();
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 9/12/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @param str
     * @since 1.X
     */
    public void setAllRegister07( String str ) {
        super.setTipoTransaccion( str.substring( 0, 2 ) );
        super.setTransCode( str.substring( 4, 6 ) );
        this.transType = str.substring( 6, 8 );
        this.cardSeqNbr = str.substring( 8, 11 );
        this.termTrnDate = str.substring( 11, 17 );
        this.termCapProf = str.substring( 17, 23 );
        this.termCounCode = str.substring( 23, 26 );
        this.termSerNbr = str.substring( 26, 34 );
        this.unpredNumber = str.substring( 34, 42 );
        this.appTrnCount = str.substring( 42, 46 );
        this.appInterProf = str.substring( 46, 50 );
        this.crypt = str.substring( 50, 66 );
        this.issAppDataB2 = str.substring( 66, 68 );
        this.issAppDataB3 = str.substring( 68, 70 );
        this.termVerRsult = str.substring( 70, 80 );
        this.issAppDataB4 = str.substring( 80, 88 );
        this.cryptAmt = str.substring( 88, 100 );
        this.issAppDataB8 = str.substring( 100, 102 );
        this.issAppDataB9 = str.substring( 102, 118 );
        this.issAppDataB1 = str.substring( 118, 120 );
        this.issAppDataB17 = str.substring( 120, 122 );
        this.issAppDataB18 = str.substring( 122, 152 );
        this.formFactInd = str.substring( 152, 160 );
        this.issRsult = str.substring( 160, 170 );
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 23/11/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @see com.sbpay.sgi.cron.dto.RegistroDTO#toString()
     * @since 1.X
     */
    
    public String toStringLengths() {
        return "Registro07DTO [tipoTransaccion="
                + super.getTipoTransaccion().length() + ", Transcode ="
                + super.getTransCode().length() + "transType="
                + transType.length() + ", cardSeqNbr=" + cardSeqNbr.length()
                + ", termTrnDate=" + termTrnDate.length() + ", termCapProf="
                + termCapProf.length() + ", termCouncCode="
                + termCounCode.length() + ", termSerNbr=" + termSerNbr.length()
                + ", unpredNumber=" + unpredNumber.length() + ", appTrnCount="
                + appTrnCount.length() + ", appInterProf="
                + appInterProf.length() + ", crypt=" + crypt.length()
                + ", issAppDataB2=" + issAppDataB2.length() + ", issAppDataB3="
                + issAppDataB3.length() + ", termVerRsult="
                + termVerRsult.length() + ", issAppDataB4="
                + issAppDataB4.length() + ", cryptAmt=" + cryptAmt.length()
                + ", issAppDataB8=" + issAppDataB8.length() + ", issAppDataB9="
                + issAppDataB9.length() + ", issAppDataB1="
                + issAppDataB1.length() + ", issAppDataB17="
                + issAppDataB17.length() + ", issAppDataB18="
                + issAppDataB18.length() + ", formFactInd="
                + formFactInd.length() + ", issRsult=" + issRsult.length()
                + "]";
    }
    
    @Override
    public String toString() {
        return "Registro07DTO [tipoTransaccion = " + super.getTipoTransaccion()
                + ", transcode =" + super.getTransCode() + ", transType="
                + transType + ", cardSeqNbr=" + cardSeqNbr + ", termTrnDate="
                + termTrnDate + ", termCapProf=" + termCapProf
                + ", termCounCode=" + termCounCode + ", termSerNbr="
                + termSerNbr + ", unpredNumber=" + unpredNumber
                + ", appTrnCount=" + appTrnCount + ", appInterProf="
                + appInterProf + ", crypt=" + crypt + ", issAppDataB2="
                + issAppDataB2 + ", issAppDataB3=" + issAppDataB3
                + ", termVerRsult=" + termVerRsult + ", issAppDataB4="
                + issAppDataB4 + ", cryptAmt=" + cryptAmt + ", issAppDataB8="
                + issAppDataB8 + ", issAppDataB9=" + issAppDataB9
                + ", issAppDataB1=" + issAppDataB1 + ", issAppDataB17="
                + issAppDataB17 + ", issAppDataB18=" + issAppDataB18
                + ", formFactInd=" + formFactInd + ", issRsult=" + issRsult
                + "]";
    }
    
}
