package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

public class ParamsReadCPagoDTO implements Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = 1L;
  private String fileNameCPago;
  private String pathFTPPathCPago;
  private String pathCPAGO;
  private DataFTP dataFTP;
  private String pathCPagoBKP;
  private String pathCPagoError;

  
  
  
  public DataFTP getDataFTP() {
    return dataFTP;
  }

  public void setDataFTP(DataFTP dataFTP) {
    this.dataFTP = dataFTP;
  }

  public String getPathCPAGO() {
    return pathCPAGO;
  }

  public void setPathCPAGO(String pathCPAGO) {
    this.pathCPAGO = pathCPAGO;
  }

  public String getFileNameCPago() {
    return fileNameCPago;
  }

  public void setFileNameCPago(String fileNameCPago) {
    this.fileNameCPago = fileNameCPago;
  }


  public String getPathCPagoBKP() {
    return pathCPagoBKP;
  }

  public void setPathCPagoBKP(String pathCPagoBKP) {
    this.pathCPagoBKP = pathCPagoBKP;
  }

  public String getPathCPagoError() {
    return pathCPagoError;
  }

  public void setPathCPagoError(String pathCPagoError) {
    this.pathCPagoError = pathCPagoError;
  }


  public String getPathFTPPathCPago() {
    return pathFTPPathCPago;
  }

  public void setPathFTPPathCPago(String pathFTPPathCPago) {
    this.pathFTPPathCPago = pathFTPPathCPago;
  }


  @Override
  public String toString() {
    return "ParamsReadCPagoDTO [fileNameCPago=" + fileNameCPago + ", pathFTPPathCPago="
        + pathFTPPathCPago + ", pathCPAGO=" + pathCPAGO + ", dataFTP=" + dataFTP
        + ", pathCPagoBKP=" + pathCPagoBKP + ", pathCPagoError=" + pathCPagoError + "]";
  }

}
