package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 25/04/2019, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class RegistroOutVisaPagoDTO implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -5714723268153729246L;
    private String tipoTransaccion;
    private String transCode;
    
    public String getTipoTransaccion() {
        return tipoTransaccion;
    }
    
    public void setTipoTransaccion( String tipoTransaccion ) {
        this.tipoTransaccion = tipoTransaccion;
    }
    
    public String getTransCode() {
        return transCode;
    }
    
    public void setTransCode( String transCode ) {
        this.transCode = transCode;
    }

	@Override
	public String toString() {
		return "RegistroOutVisaPagoDTO [tipoTransaccion=" + tipoTransaccion
				+ ", transCode=" + transCode + "]";
	}

}
