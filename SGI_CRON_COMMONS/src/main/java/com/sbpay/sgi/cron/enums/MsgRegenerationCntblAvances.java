package com.sbpay.sgi.cron.enums;
/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 xx/xx/2019, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public enum MsgRegenerationCntblAvances {
	
	WARNING_DATA_NOT_FOUND("ADVERTENCIA: No se ha encontrado ningun registro en la tabla TMP_CONTABLE para poder generar el archivo."),
	WARNING_DATA_NOT_FOUND_LOG("ADVERTENCIA: No se ha encontrado ningun registro en la tabla TBL_LOG_CONTABLE para poder generar el archivo."),
    ERROR_CONTABLE_ALREADY_EXIST("Error!!! Archivo contable : ? , existe actualmente en el directorio, no se puede generar nuevamete."), 
	ERROR_PARAM_NOT_FOUND("ADVERTENCIA: No se han encontrado los parametros, para la generacion del archivo contable"),
	ERROR_GEN_CNTBL_FILE("Ha ocurrido un error al intentar generar el archivo contable"), 
    SUCCESFUL_GENERATE_CNTBL_TITTLE("Se ha generado Contable correctamente"),
    SUCCESFUL_GENERATE_CNTBL_MSG(" El proceso para regenerar de Archivo Contable ha finalizado exitosamente."), 
	ERROR_CREATE_CONTABLE_FILE("Error: Se ha producido un error al regenerar el archivo contable"),
	DELETE_SUCCESS_CONTABLE_FILE("Se elimino corretamente archivo contable existente...")
    ;
	
	  /**
     * Valor ENUM.
     */
    private final String text;
    
    /**
     * Constructor.
     * 
     * @param text
     */
    private MsgRegenerationCntblAvances( final String text ) {
        this.text = text;
    }
    
    /**
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }

}
