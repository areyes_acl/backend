package com.sbpay.sgi.cron.dto;

public class Registro00OutVisaPagoDTO extends RegistroOutVisaPagoDTO {
	/**
     * 
     */
    private static final long serialVersionUID = 2664046852952957779L;
    
    private String mit;
    private String codigoFuncion;
    private String destinationBIN;
    private String sourceBIN;
    
    private String repSREIdentifier;
    private String recSREIdentifier;
    private String serviceIdentifier;
    private String currencyCode;
    private String businessMode;
    private String reserved1;
    
    private String report;
    private String settlementDate;
    private String reportDate;
    
    private String totalInterchangeCount;
    private String totalInterchangeValue;
    private String interchangeValueSign;
    
    private String totalReimbursementFees;
    private String reimbursementFeesSign;
    
    private String totalVisaCharges;
    private String visaChargesSign;
    private String netSettlementAmount;
    private String netSettlementAmountSign;
    private String summaryLevel;
    private String reserved2;
    
    
    
    
	public String getMit() {
		return mit;
	}




	public void setMit(String mit) {
		this.mit = mit;
	}




	public String getCodigoFuncion() {
		return codigoFuncion;
	}




	public void setCodigoFuncion(String codigoFuncion) {
		this.codigoFuncion = codigoFuncion;
	}




	public String getDestinationBIN() {
		return destinationBIN;
	}




	public void setDestinationBIN(String destinationBIN) {
		this.destinationBIN = destinationBIN;
	}




	public String getSourceBIN() {
		return sourceBIN;
	}




	public void setSourceBIN(String sourceBIN) {
		this.sourceBIN = sourceBIN;
	}




	public String getRepSREIdentifier() {
		return repSREIdentifier;
	}




	public void setRepSREIdentifier(String repSREIdentifier) {
		this.repSREIdentifier = repSREIdentifier;
	}




	public String getRecSREIdentifier() {
		return recSREIdentifier;
	}




	public void setRecSREIdentifier(String recSREIdentifier) {
		this.recSREIdentifier = recSREIdentifier;
	}




	public String getServiceIdentifier() {
		return serviceIdentifier;
	}




	public void setServiceIdentifier(String serviceIdentifier) {
		this.serviceIdentifier = serviceIdentifier;
	}




	public String getCurrencyCode() {
		return currencyCode;
	}




	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}




	public String getBusinessMode() {
		return businessMode;
	}




	public void setBusinessMode(String businessMode) {
		this.businessMode = businessMode;
	}




	public String getReserved1() {
		return reserved1;
	}




	public void setReserved1(String reserved1) {
		this.reserved1 = reserved1;
	}




	public String getReport() {
		return report;
	}




	public void setReport(String report) {
		this.report = report;
	}




	public String getSettlementDate() {
		return settlementDate;
	}




	public void setSettlementDate(String settlementDate) {
		this.settlementDate = settlementDate;
	}




	public String getReportDate() {
		return reportDate;
	}




	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}




	public String getTotalInterchangeCount() {
		return totalInterchangeCount;
	}




	public void setTotalInterchangeCount(String totalInterchangeCount) {
		this.totalInterchangeCount = totalInterchangeCount;
	}




	public String getTotalInterchangeValue() {
		return totalInterchangeValue;
	}




	public void setTotalInterchangeValue(String totalInterchangeValue) {
		this.totalInterchangeValue = totalInterchangeValue;
	}




	public String getInterchangeValueSign() {
		return interchangeValueSign;
	}




	public void setInterchangeValueSign(String interchangeValueSign) {
		this.interchangeValueSign = interchangeValueSign;
	}




	public String getTotalReimbursementFees() {
		return totalReimbursementFees;
	}




	public void setTotalReimbursementFees(String totalReimbursementFees) {
		this.totalReimbursementFees = totalReimbursementFees;
	}




	public String getReimbursementFeesSign() {
		return reimbursementFeesSign;
	}




	public void setReimbursementFeesSign(String reimbursementFeesSign) {
		this.reimbursementFeesSign = reimbursementFeesSign;
	}




	public String getTotalVisaCharges() {
		return totalVisaCharges;
	}




	public void setTotalVisaCharges(String totalVisaCharges) {
		this.totalVisaCharges = totalVisaCharges;
	}




	public String getVisaChargesSign() {
		return visaChargesSign;
	}




	public void setVisaChargesSign(String visaChargesSign) {
		this.visaChargesSign = visaChargesSign;
	}




	public String getNetSettlementAmount() {
		return netSettlementAmount;
	}




	public void setNetSettlementAmount(String netSettlementAmount) {
		this.netSettlementAmount = netSettlementAmount;
	}




	public String getNetSettlementAmountSign() {
		return netSettlementAmountSign;
	}




	public void setNetSettlementAmountSign(String netSettlementAmountSign) {
		this.netSettlementAmountSign = netSettlementAmountSign;
	}




	public String getSummaryLevel() {
		return summaryLevel;
	}




	public void setSummaryLevel(String summaryLevel) {
		this.summaryLevel = summaryLevel;
	}




	public String getReserved2() {
		return reserved2;
	}




	public void setReserved2(String reserved2) {
		this.reserved2 = reserved2;
	}




	@Override
	public String toString() {
		return "Registro00OutVisaPagoDTO ["
			    + "mit="+	mit
			    + "codigoFuncion="+	codigoFuncion
			    + "destinationBIN="+	destinationBIN
			    + "sourceBIN="+	sourceBIN
			    + "repSREIdentifier="+	repSREIdentifier
			    + "recSREIdentifier="+	recSREIdentifier
			    + "serviceIdentifier="+	serviceIdentifier
			    + "currencyCode="+	currencyCode
			    + "businessMode="+	businessMode
			    + "reserved1="+	reserved1
			    + "report="+	report
			    + "settlementDate="+	settlementDate
			    + "reportDate="+	reportDate
			    + "totalInterchangeCount="+	totalInterchangeCount
			    + "totalInterchangeValue="+	totalInterchangeValue
			    + "interchangeValueSign="+	interchangeValueSign
			    + "totalReimbursementFees="+	totalReimbursementFees
			    + "reimbursementFeesSign="+	reimbursementFeesSign
			    + "totalVisaCharges="+	totalVisaCharges
			    + "visaChargesSign="+	visaChargesSign
			    + "netSettlementAmount="+	netSettlementAmount
			    + "netSettlementAmountSign="+	netSettlementAmountSign
			    + "summaryLevel="+	summaryLevel
			    + "reserved2="+	reserved2 + "]";
	}
    
 
    
}
