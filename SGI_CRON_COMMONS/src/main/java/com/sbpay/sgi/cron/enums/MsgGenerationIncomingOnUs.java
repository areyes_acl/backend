package com.sbpay.sgi.cron.enums;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 11/12/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public enum MsgGenerationIncomingOnUs {
     
    ERROR_SQL_ON_US_OUT_TBK("Ha ocurrido un error SQL al realizar el proceso de Generar/Exportar el archivo On Us"), 
    WARNING_TRANSACTIONS_NOT_FOUND("ADVERTENCIA: No se ha encontrado ninguna transaccion On Us, para crear el archivo, se generará un archivo en blanco."), 
    ERROR_INCOMING_ONUS_ALREADY_EXIST("Error: Archivo incoming On Us ya existe en el directorio"), 
    ERROR_CREATE_INCOMING_ONUS_FILE("Error: Se ha producido un error al generar el archivo incoming On Us");
    
    /**
     * Valor ENUM.
     */
    private final String text;
    
    /**
     * Constructor.
     * 
     * @param text
     */
    private MsgGenerationIncomingOnUs( final String text ) {
        this.text = text;
    }
    
    /**
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
}
