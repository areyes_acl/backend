package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 25/04/2019, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * Clase que guardara una transaccion completa contemplando para esto
 * el todos los registros involucrados.
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class TransaccionOutVisa implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = -8665924641652098866L;
    private int numeroTransaccion;
    private String rutsbpay;
    private RegistroOutVisaIntDTO registro00;
    private RegistroOutVisaIntDTO registro01;
    private RegistroOutVisaIntDTO registro03;
    private RegistroOutVisaIntDTO registro04;
    private RegistroOutVisaIntDTO registro05;
    private RegistroOutVisaIntDTO registro06;
    private RegistroOutVisaIntDTO registro07;
    private RegistroOutVisaIntDTO registro0D;
    private RegistroOutVisaIntDTO registro2D;
    private RegistroOutVisaIntDTO registro02;
    
    public String getRutsbpay() {
        return rutsbpay;
    }
    
    public void setRutsbpay( String rutsbpay ) {
        this.rutsbpay = rutsbpay;
    }
    
    public int getNumeroTransaccion() {
        return numeroTransaccion;
    }
    
    public void setNumeroTransaccion( int numeroTransaccion ) {
        this.numeroTransaccion = numeroTransaccion;
    }
    
    public RegistroOutVisaIntDTO getRegistro00() {
        return registro00;
    }
    
    public void setRegistro00( RegistroOutVisaIntDTO registro00 ) {
        this.registro00 = registro00;
    }
    
    public RegistroOutVisaIntDTO getRegistro01() {
        return registro01;
    }
    
    public void setRegistro01( RegistroOutVisaIntDTO registro01 ) {
        this.registro01 = registro01;
    }
    
    public RegistroOutVisaIntDTO getRegistro05() {
        return registro05;
    }
    
    public void setRegistro05( RegistroOutVisaIntDTO registro05 ) {
        this.registro05 = registro05;
    }
    
    public RegistroOutVisaIntDTO getRegistro07() {
        return registro07;
    }
    
    public void setRegistro07( RegistroOutVisaIntDTO registro07 ) {
        this.registro07 = registro07;
    }

	public RegistroOutVisaIntDTO getRegistro03() {
		return registro03;
	}

	public void setRegistro03(RegistroOutVisaIntDTO registro03) {
		this.registro03 = registro03;
	}

	public RegistroOutVisaIntDTO getRegistro04() {
		return registro04;
	}

	public void setRegistro04(RegistroOutVisaIntDTO registro04) {
		this.registro04 = registro04;
	}

	public RegistroOutVisaIntDTO getRegistro06() {
		return registro06;
	}

	public void setRegistro06(RegistroOutVisaIntDTO registro06) {
		this.registro06 = registro06;
	}
	
	public RegistroOutVisaIntDTO getRegistro0D() {
		return registro0D;
	}

	public void setRegistro0D(RegistroOutVisaIntDTO registro0d) {
		registro0D = registro0d;
	}

	public RegistroOutVisaIntDTO getRegistro2D() {
		return registro2D;
	}

	public void setRegistro2D(RegistroOutVisaIntDTO registro2d) {
		registro2D = registro2d;
	}

	public RegistroOutVisaIntDTO getRegistro02() {
		return registro02;
	}

	public void setRegistro02(RegistroOutVisaIntDTO registro02) {
		this.registro02 = registro02;
	}

	@Override
	public String toString() {
		return "TransaccionOutVisaInt [numeroTransaccion=" + numeroTransaccion
				+ ", rutsbpay=" + rutsbpay + ", registro00=" + registro00
				+ ", registro01=" + registro01 + ", registro02=" + registro02 + ", registro03=" + registro03 + ", registro04=" + registro04 +", registro05=" + registro05 + ", registro07=" + registro07 + ", registro06=" + registro06 + ", registro0D=" + registro0D + ", registro2D=" + registro2D
				+ "]";
	}

	
    
}
