package com.sbpay.sgi.cron.ds;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.naming.NoInitialContextException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.enums.PropertyEntryTypes;

public final class ConnectionProviderGenerarAvance {
    /**
     * Logger.
     */
    private static final Logger LOG = Logger
            .getLogger( ConnectionProviderGenerarAvance.class.getName() );
    /**
     * Data Source.
     */
    private DataSource dataSource;
    
    /** The single instance. */
    private static ConnectionProviderGenerarAvance singleINSTANCE = null;
    
    /**
     * Creates the instance.
     */
    private static void createInstance() {
        synchronized ( ConnectionProviderGenerarAvance.class ) {
            if ( singleINSTANCE == null ) {
                singleINSTANCE = new ConnectionProviderGenerarAvance();
            }
        }
    }
    
    /**
     * Patron singleton.
     * 
     * @return  retorna instancia del servicio.
     */
    public static ConnectionProviderGenerarAvance getInstance() {
        if ( singleINSTANCE == null ) {
            createInstance();
        }
        return singleINSTANCE;
    }
    
    /**
     * Metodo de restricion de sobreescritura de la clase.
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        super.clone();
        throw new CloneNotSupportedException();
    }
    
    /**
     * Constructor privado.
     */
    private ConnectionProviderGenerarAvance() {
        super();
        Context ict;
        try {
            String jndi = getInstanceJNDI();
            LOG.info( "JNDI DE CONEXION LEIDO EXITOSAMENTE DESDE ARCHIVO DE PROPIEDADES: "
                    + jndi );
            ict = new InitialContext();
            this.dataSource = ( DataSource ) ict.lookup( jndi );
            
        }
        catch ( NoInitialContextException ex ) {
            LOG.error( ex.getMessage(), ex );
        }
        catch ( NamingException ex ) {
            LOG.error( ex.getMessage(), ex );
        }
    }
    
    /**
     * @return the dataSource.
     * @throws SQLException
     */
    public Connection getConnection() throws SQLException {
        // Connection con = TestConnection.getConnection();
        Connection con = dataSource.getConnection();
        con.setAutoCommit( false );
        return con;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2018, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Metodo encargado de obtener el jndi desde el properties de la
     * aplicación
     * 
     * 
     * @since 1.X
     */
    private String getInstanceJNDI() {
        Properties prop = PropertiesConfiguration.getCronProperties();
        return prop.getProperty( PropertyEntryTypes.JNDI_AVA.getKey() );
        
    }
}
