package com.sbpay.sgi.cron.utils.file;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dto.ICCodeHomologationDTO;
import com.sbpay.sgi.cron.dto.LogTransaccionOnUsDTO;
import com.sbpay.sgi.cron.dto.Trailer;
import com.sbpay.sgi.cron.dto.TransaccionOnUsDTO;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.DateFormatType;
import com.sbpay.sgi.cron.enums.MsgGenerationIncomingOnUs;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;
import com.sbpay.sgi.cron.utils.generals.DateUtils;


/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class TransaccionOnUsExport implements TransaccionOnUsFileUtil {

  private static final Logger LOGGER = Logger.getLogger(TransaccionOnUsExport.class);
  private static final String INCOMING_HEADER_NUMBER = "90";
  private static final String FILE_BATCH_NUMBER = "91";
  private static final String FILE_TRAILER_NUMBER = "92";
  private static final String TRANSBANK_NUMBER = "455771";
  private static final String FORM_CODE = "7";
  private static final String CODIGO_MONEDA_NACIONAL = "152";
  private static final String CODIGO_PAIS = "CL ";
  private static final String[] CODIGOS_TRANSAC_MONETARY = {"05_00"};
  // TOTAL DE ARCHIVOS SIN FOOTER NI HEADER
  private int totalRegistrosArchivo = 0;
  private int totalTransaccionesMonetarias = 0;

 

  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
   * </ul>
   * <p>
   * 
   * @param ruta
   * @param incomingFilename
   * @param controlFilename
   * @param listaTrasacciones
   * @throws AppException
   * @see com.sbpay.sgi.cron.utils.file.TransaccionOnUsFileUtil#exportIncomingOnUsFile(java.lang.String,
   *      java.lang.String, java.lang.String, java.util.List)
   * @since 1.X
   */
  @Override
  public void exportIncomingOnUsFile(String ruta, String incomingFilename, String controlFilename,
      List<TransaccionOnUsDTO> listaTrasacciones) throws AppException {
    String rutaFile = ruta.concat(incomingFilename);
    File incomingFile = new File(rutaFile);
    incomingFile.setExecutable(true);
    incomingFile.setReadable(true);
    incomingFile.setWritable(true);

    if (incomingFile.exists()) {
      throw new AppException(MsgGenerationIncomingOnUs.ERROR_INCOMING_ONUS_ALREADY_EXIST.toString());
    }

    try {
      generaArchivoIncomingOnUs(incomingFile, listaTrasacciones);
      generaArchivoControl(ruta,controlFilename);

      // 01-FEB-2016 : ONUS NO POSEE ARCHIVO DE CONTROL
      // generaArchivoControl( ruta, controlFilename );
    } catch (IOException e) {
      throw new AppException(MsgGenerationIncomingOnUs.ERROR_CREATE_INCOMING_ONUS_FILE.toString(),
          e);
    }

  }
  
  
  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 11/12/2015, (ACL-sbpay) - versión inicial
   * </ul>
   * <p>
   * 
   * @param controlFilename
   * @param ruta
   * @throws IOException
   * 
   * @since 1.X
   */
  private void generaArchivoControl( String ruta, String controlFilename )
          throws IOException {
      LOGGER.info( "=========> SE GENERA ARCHIVO DE CONTROL ONUS <==========" );
      String pathControlFile = ruta.concat( controlFilename );
      File controlFile = new File( pathControlFile );
      BufferedWriter bw = new BufferedWriter( new FileWriter( controlFile ) );
      bw.close();
      
  }

  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
   * </ul>
   * <p>
   * 
   * @param incomingFile
   * @param listaTrasacciones
   * @throws IOException
   * @since 1.X
   */
  private void generaArchivoIncomingOnUs(File incomingFile,
      List<TransaccionOnUsDTO> listaTransacciones) throws IOException {

    LOGGER.info("=========> SE GENERA ARCHIVO INCOMING ONUS <==========");
    StringBuilder contenido = new StringBuilder();

    // SE OBTIENE EL HEADER
    String header = generateIncomingHeader();

    LOGGER.info("TOTAL DE TRANSACCIONES EN EL ARCHIVO: " + listaTransacciones.size());

    // GENERA EL CONTENIDO DE TRANSACCIONES
    for (int i = 0; i < listaTransacciones.size(); i++) {
      contenido.append(getTransactionsToString(listaTransacciones.get(i)));
      // SI ES EL ULTIMO REGISTRO NO SE HACE SALTO DE LINEA
      if ((i + 1) == listaTransacciones.size()) {
        contenido.replace(contenido.length() - 1, contenido.length(),
            ConstantesUtil.EMPTY.toString());
      }
    }

    // UNA VEZ GENERADO EL CONTENIDO DEL ARCHIVO SE CUENTA EL
    // TOTAL DE REGISTROS
    totalRegistrosArchivo = CommonsUtils.countLines(contenido.toString());

    // CUENTA EL NUMERO DE TRANSACCIONES MONETARIAS DEL BATCH
    totalTransaccionesMonetarias = getNumeroDeTransaccionesMonetarias(listaTransacciones);

    // ESCRIBE EL FOOTER
    String footer = generateTotalRegister(listaTransacciones);

    // ESCRIBE EN ARCHIVO INCOMING
    writeIncomingOnusFile(header, contenido.toString(), footer, incomingFile);


  }

  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 11/12/2015, (ACL-sbpay) - versión inicial
   * </ul>
   * <p>
   * 
   * Metodo que crea el archivo en disco y escribe todo el contenido de este.
   * 
   * @param footer
   * @param contenido
   * @param header
   * @param incomingFile
   * @param incomingFilename
   * @throws IOException
   * 
   * @since 1.X
   */
  private void writeIncomingOnusFile(String header, String contenido, String footer,
      File incomingFile) throws IOException {
    BufferedWriter bw = null;
    bw = new BufferedWriter(new FileWriter(incomingFile));
    bw.write(header);
    if (contenido != null && contenido.length() > 0) {
      bw.write(ConstantesUtil.SKIP_LINE.toString());
      bw.write(contenido);
    }
    bw.write(ConstantesUtil.SKIP_LINE.toString());
    bw.write(footer);
    bw.close();
  }

  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 11/12/2015, (ACL-sbpay) - versión inicial
   * </ul>
   * <p>
   * 
   * @return
   * @since 1.X
   */
  private String generateTotalRegister(List<TransaccionOnUsDTO> listaTransacciones) {
    StringBuilder str = new StringBuilder();
    str.append(generateBatchTrailer(listaTransacciones));
    str.append(ConstantesUtil.SKIP_LINE.toString());
    str.append(generateFileTrailer(listaTransacciones));
    return str.toString();
  }

  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 - 9/12/2015, (ACL) - versión inicial
   * </ul>
   * <p>
   * 
   * @return
   * @since 1.X
   */
  private String generateBatchTrailer(List<TransaccionOnUsDTO> listaTransacciones) {

    Trailer batchTrailer = new Trailer();
    String montoTransaccionesMonetarias = getMontoDeTransaccionesMonetarias(listaTransacciones);
    String numeroTransaccionesBatch = CommonsUtils.paddingZeroToNumber(totalRegistrosArchivo, 12);

    batchTrailer.setCodi(FILE_BATCH_NUMBER);
    batchTrailer.setZero(CommonsUtils.getZeroString(2));
    batchTrailer.setSobi(TRANSBANK_NUMBER);
    batchTrailer.setFech(DateUtils.getTodayStringDateInJulianFormat());
    batchTrailer.setSumd(CommonsUtils.getZeroString(15));
    batchTrailer.setNumd(CommonsUtils.paddingZeroToNumber(totalTransaccionesMonetarias, 12));
    batchTrailer.setNuba(CommonsUtils.getZeroString(6));
    batchTrailer.setNure(numeroTransaccionesBatch);
    batchTrailer.setBatn(CommonsUtils.paddingZeroToNumber(1, 8));
    batchTrailer.setNutr(CommonsUtils.paddingZeroToNumber(
        Integer.valueOf(totalTransaccionesMonetarias) + 1, 9));
    batchTrailer.setNtrf(montoTransaccionesMonetarias);

    return batchTrailer.getStringTrailer();
  }


  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 - 9/12/2015, (ACL) - versión inicial
   * </ul>
   * <p>
   * 
   * @return
   * @since 1.X
   */
  private String generateIncomingHeader() {
    StringBuilder str = new StringBuilder();
    str.append(INCOMING_HEADER_NUMBER);
    str.append(CommonsUtils.getWhitesSpaceString(2));
    str.append(CommonsUtils.getWhitesSpaceString(6));
    str.append(DateUtils.getTodayStringDateInJulianFormat());
    str.append(CommonsUtils.getWhitesSpaceString(16));
    str.append(CommonsUtils.getWhitesSpaceString(4));
    str.append(CommonsUtils.getWhitesSpaceString(29));
    str.append(CommonsUtils.getWhitesSpaceString(14));
    str.append(CommonsUtils.getZeroString(3));
    str.append(CommonsUtils.getWhitesSpaceString(89));
    return str.toString();
  }


  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 - 9/12/2015, (ACL) - versión inicial
   * </ul>
   * <p>
   * 
   * @param listaTransacciones
   * 
   * @return
   * @since 1.X
   */
  private String generateFileTrailer(List<TransaccionOnUsDTO> listaTransacciones) {

    String numeroTransaccionesFile =
        CommonsUtils.paddingZeroToNumber(totalTransaccionesMonetarias, 12);
    // SE LE SUMA 1 POR FORMATO DE ARCHIVO
    String numeroTotalBatchFile = CommonsUtils.paddingZeroToNumber((totalRegistrosArchivo + 1), 12);

    Trailer fileTrailer = new Trailer();
    fileTrailer.setCodi(FILE_TRAILER_NUMBER);
    fileTrailer.setZero(CommonsUtils.getZeroString(2));
    fileTrailer.setSobi(TRANSBANK_NUMBER);
    fileTrailer.setFech(DateUtils.getTodayStringDateInJulianFormat());
    fileTrailer.setSumd(CommonsUtils.getZeroString(15));
    fileTrailer.setNumd(numeroTransaccionesFile);
    fileTrailer.setNuba(CommonsUtils.getZeroString(5).concat("1"));
    fileTrailer.setNure(numeroTotalBatchFile);
    fileTrailer.setBatn(CommonsUtils.getZeroString(8));
    fileTrailer.setNutr(CommonsUtils.paddingZeroToNumber(totalTransaccionesMonetarias + 2, 9));
    fileTrailer.setNtrf(getMontoDeTransaccionesMonetarias(listaTransacciones));

    return fileTrailer.getStringTrailer();
  }

  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
   * </ul>
   * <p>
   * Método que realiza la sumatoria de las transacciones,
   * 
   * @param listaTransacciones
   * @return
   * @since 1.X
   */
  private String getMontoDeTransaccionesMonetarias(List<TransaccionOnUsDTO> listaTransacciones) {
    Integer sumaMonto = 0;

    for (TransaccionOnUsDTO transaccion : listaTransacciones) {
      String codigo = transaccion.getCodigoTransaccion() + "_00";
      if (Arrays.asList(CODIGOS_TRANSAC_MONETARY).contains(codigo)) {
        String montoTransaccion = transaccion.getMontoTransac();
        sumaMonto =
            sumaMonto
                + Integer.valueOf(montoTransaccion.substring(0, montoTransaccion.length() - 2));
      }
    }

    // SE DEBE MULTIPLICAR * 100 ANTES DE LA SUMA
    sumaMonto = sumaMonto * 100;

    return CommonsUtils.paddingZeroToNumber(sumaMonto, 15);
  }

  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 11/12/2015, (ACL-sbpay) - versión inicial
   * </ul>
   * <p>
   * Metodo encargado de escribir luna transaccion de salida
   * 
   * @param contenido
   * @param transaccion
   * @since 1.X
   */
  private String getTransactionsToString(TransaccionOnUsDTO transaccion) {

    StringBuilder str = new StringBuilder();
   transaccion.setCodigoTransaccion(CommonsUtils.obtieneCodTrxOnusTbkPorCodsbpay(transaccion.getCodigoTransaccion()));

      
    str.append(transaccion.getCodigoTransaccion());
    str.append(CommonsUtils.getWhitesSpaceString(1));
    str.append(CommonsUtils.getWhitesSpaceString(1));
    str.append(CommonsUtils.getZeroString(2));
    str.append(transaccion.getNumeroTarjeta());
    str.append(CommonsUtils.getZeroString(3));
    str.append(FORM_CODE).append(TRANSBANK_NUMBER).append(CommonsUtils.getWhitesSpaceString(4))
        .append(transaccion.getTipoVenta()).append(transaccion.getNumCuotas())
        .append(transaccion.getNumMicrofilm()).append(CommonsUtils.getWhitesSpaceString(1));
    str.append(transaccion.getNumComercio());
    str.append(DateUtils.parseFormat(transaccion.getFechaCompra(),
        DateFormatType.FORMAT_YYYY_MM_DD, DateFormatType.FORMAT_MMDD));
    str.append(transaccion.getMontoTransac());
    str.append(CODIGO_MONEDA_NACIONAL);
    str.append(transaccion.getNombreComercio());
    str.append(transaccion.getCiudadComercio());
    str.append(CODIGO_PAIS);
    str.append(transaccion.getRubroComercio());
    str.append(CommonsUtils.getZeroString(5));
    str.append(CommonsUtils.getWhitesSpaceString(3));
    str.append(CommonsUtils.getWhitesSpaceString(9));
    str.append(CommonsUtils.getWhitesSpaceString(2));
    str.append(CommonsUtils.getWhitesSpaceString(1));
    str.append(CommonsUtils.getZeroString(12));
    str.append(CommonsUtils.getWhitesSpaceString(1));
    str.append(CommonsUtils.getZeroString(12));
    String fechaJuliana = DateUtils.getTodayStringDateInJulianFormat();
    str.append(fechaJuliana.substring(1, fechaJuliana.length()));
    str.append(CommonsUtils.getZeroString(1));
    str.append(ConstantesUtil.SKIP_LINE.toString());


    return str.toString();
  }

  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 11/12/2015, (ACL-sbpay) - versión inicial
   * </ul>
   * <p>
   * Suma transacciones Monetarias: sólo se consideran las 05
   * 
   * @param listaTransacciones
   * @return
   * @since 1.X
   */
  private Integer getNumeroDeTransaccionesMonetarias(List<TransaccionOnUsDTO> listaTransacciones) {
    int suma = 0;

    for (TransaccionOnUsDTO transaccion : listaTransacciones) {
      String codigo = transaccion.getCodigoTransaccion() + "_00";
      if (Arrays.asList(CODIGOS_TRANSAC_MONETARY).contains(codigo)) {
        suma++;
      }
    }

    return suma;
  }


@Override
public LogTransaccionOnUsDTO readLogTrxOnUsrFile(File file, String starWith,
		List<ICCodeHomologationDTO> listaCodigosIc) throws AppException {
	// TODO Auto-generated method stub
	return null;
}

}
