package com.sbpay.sgi.cron.dto;

public class ParamDownloadVisaNacDTO {

  private String fileNameVISA;
  private String extNameVISA;
  private String extNameVISACTR;
  private String pathVisaInc;
  private String ftpPathOutVisa;
  private String pathVisaBKP;
  private String pathVisaERROR;
  private DataFTP dataFTP;

  public String getFileNameVISA() {
    return fileNameVISA;
  }

  public void setFileNameVISA(String fileNameVISA) {
    this.fileNameVISA = fileNameVISA;
  }

  public String getExtNameVISA() {
    return extNameVISA;
  }

  public void setExtNameVISA(String extNameVISA) {
    this.extNameVISA = extNameVISA;
  }

  public String getExtNameVISACTR() {
    return extNameVISACTR;
  }

  public void setExtNameVISACTR(String extNameVISACTR) {
    this.extNameVISACTR = extNameVISACTR;
  }

  public String getPathVisaInc() {
    return pathVisaInc;
  }

  public void setPathVisaInc(String pathVisaInc) {
    this.pathVisaInc = pathVisaInc;
  }

  public String getFtpPathOutVisa() {
    return ftpPathOutVisa;
  }

  public void setFtpPathOutVisa(String ftpPathOutVisa) {
    this.ftpPathOutVisa = ftpPathOutVisa;
  }

  public String getPathVisaBKP() {
    return pathVisaBKP;
  }

  public void setPathVisaBKP(String pathVisaBKP) {
    this.pathVisaBKP = pathVisaBKP;
  }

  public String getPathVisaERROR() {
    return pathVisaERROR;
  }

  public void setPathVisaERROR(String pathVisaERROR) {
    this.pathVisaERROR = pathVisaERROR;
  }

  public DataFTP getDataFTP() {
    return dataFTP;
  }

  public void setDataFTP(DataFTP dataFTP) {
    this.dataFTP = dataFTP;
  }

  @Override
  public String toString() {
    return "ParamDownloadVisaNacDTO [fileNameVISA=" + fileNameVISA + ", extNameVISA=" + extNameVISA
        + ", extNameVISACTR=" + extNameVISACTR + ", pathVisaInc=" + pathVisaInc
        + ", ftpPathOutVisa=" + ftpPathOutVisa + ", pathVisaBKP=" + pathVisaBKP
        + ", pathVisaERROR=" + pathVisaERROR + ", dataFTP=" + dataFTP + "]";
  }



}
