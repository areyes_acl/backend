package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class TransacionAutorizada implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 8887889677674146517L;
  private String codigoTransaccion;
  private String numeroTarjeta;
  private String fechaProceso;
  private String tipoVenta;
  private String numCuotas;
  private String numMicrofilm;
  private String numComercio;
  private String fechaCompra;
  private String montoTransac;
  private String valorCuota;
  private String nombreComercio;
  private String ciudadComercio;
  private String rubroComercio;
  private String codAutorizacion;
  private String codLocal;
  private String codPos;
  private String fechaHoraTrx;

  public String getCodigoTransaccion() {
    return codigoTransaccion;
  }

  public void setCodigoTransaccion(String codigoTransaccion) {
    this.codigoTransaccion = codigoTransaccion;
  }

  public String getNumeroTarjeta() {
    return numeroTarjeta;
  }

  public void setNumeroTarjeta(String numeroTarjeta) {
    this.numeroTarjeta = numeroTarjeta;
  }

  public String getFechaProceso() {
    return fechaProceso;
  }

  public String getCiudadComercio() {
    return ciudadComercio;
  }

  public void setCiudadComercio(String ciudadComercio) {
    this.ciudadComercio = ciudadComercio;
  }

  public void setFechaProceso(String fechaProceso) {
    this.fechaProceso = fechaProceso;
  }

  public String getTipoVenta() {
    return tipoVenta;
  }

  public void setTipoVenta(String tipoVenta) {
    this.tipoVenta = tipoVenta;
  }

  public String getNumCuotas() {
    return numCuotas;
  }

  public void setNumCuotas(String numCuotas) {
    this.numCuotas = numCuotas;
  }

  public String getNumMicrofilm() {
    return numMicrofilm;
  }

  public void setNumMicrofilm(String numMicrofilm) {
    this.numMicrofilm = numMicrofilm;
  }

  public String getNumComercio() {
    return numComercio;
  }

  public void setNumComercio(String numComercio) {
    this.numComercio = numComercio;
  }

  public String getFechaCompra() {
    return fechaCompra;
  }

  public void setFechaCompra(String fechaCompra) {
    this.fechaCompra = fechaCompra;
  }

  public String getMontoTransac() {
    return montoTransac;
  }

  public void setMontoTransac(String montoTransac) {
    this.montoTransac = montoTransac;
  }

  public String getValorCuota() {
    return valorCuota;
  }

  public void setValorCuota(String valorCuota) {
    this.valorCuota = valorCuota;
  }

  public String getNombreComercio() {
    return nombreComercio;
  }

  public void setNombreComercio(String nombreComercio) {
    this.nombreComercio = nombreComercio;
  }

  public String getRubroComercio() {
    return rubroComercio;
  }

  public void setRubroComercio(String rubroComercio) {
    this.rubroComercio = rubroComercio;
  }

  public String getCodAutorizacion() {
    return codAutorizacion;
  }

  public void setCodAutorizacion(String codAutorizacion) {
    this.codAutorizacion = codAutorizacion;
  }

  public String getCodLocal() {
    return codLocal;
  }

  public void setCodLocal(String codLocal) {
    this.codLocal = codLocal;
  }

  public String getCodPos() {
    return codPos;
  }

  public void setCodPos(String codPos) {
    this.codPos = codPos;
  }

  public String getFechaHoraTrx() {
    return fechaHoraTrx;
  }

  public void setFechaHoraTrx(String fechaHoraTrx) {
    this.fechaHoraTrx = fechaHoraTrx;
  }

  @Override
  public String toString() {
    return "TransacionAutorizada [codigoTransaccion=" + codigoTransaccion + ", numeroTarjeta="
        + numeroTarjeta + ", fechaProceso=" + fechaProceso + ", tipoVenta=" + tipoVenta
        + ", numCuotas=" + numCuotas + ", numMicrofilm=" + numMicrofilm + ", numComercio="
        + numComercio + ", fechaCompra=" + fechaCompra + ", montoTransac=" + montoTransac
        + ", valorCuota=" + valorCuota + ", nombreComercio=" + nombreComercio + ", ciudadComercio="
        + ciudadComercio + ", rubroComercio=" + rubroComercio + ", codAutorizacion="
        + codAutorizacion + ", codLocal=" + codLocal + ", codPos=" + codPos + ", fechaHoraTrx="
        + fechaHoraTrx + "]";
  }





}
