package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

public class RegistroOutVisaCobroCargoDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String tipoTransaccion;
    private String transCode;
	public String getTipoTransaccion() {
		return tipoTransaccion;
	}
	public void setTipoTransaccion(String tipoTransaccion) {
		this.tipoTransaccion = tipoTransaccion;
	}
	public String getTransCode() {
		return transCode;
	}
	public void setTransCode(String transCode) {
		this.transCode = transCode;
	}
	@Override
	public String toString() {
		return "RegistroOutVisaCobroCargoDTO [tipoTransaccion="
				+ tipoTransaccion + ", transCode=" + transCode + "]";
	}

}
