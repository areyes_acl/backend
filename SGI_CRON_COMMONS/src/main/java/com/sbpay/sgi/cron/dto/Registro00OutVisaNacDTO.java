package com.sbpay.sgi.cron.dto;

import com.sbpay.sgi.cron.enums.ConstantesUtil;

public class Registro00OutVisaNacDTO extends RegistroOutVisaNacDTO {

	/**
     * 
     */
    private static final long serialVersionUID = 2664046852952957779L;
    // MIT
    private String codigoTransaccion;
    private String numeroTarjeta;
    private String numberExt;
    // NUMERO DE REFERENCIA
    private String formCode;
    private String binFuente;
    private String fechaCapt;
    private String tipoVenta;
    private String numCuo;
    private String numIdn;
    private String checkDig;
    
    private String acqMembId;
    private String fechaComp;
    private String montoDest;
    private String monedaDest;
    private String montoFuen;
    private String monedaFuen;
    private String nombComer;
    private String fechaDCompensacion;
    private String ofic;
    private String ciudadComer;
    private String tdab;
    private String pais;
    private String rubroComer;
    private String zipCodeComer;
    private String codMunic;
    private String usagCode;
    private String codRazon;
    private String settleFlag;
    private String indAutor;
	private String codAutor;
    private String posTerminalCapability;
    private String intFeeIndic;
    private String cardHolderIdMethod;
    private String posEntryMode;
    private String fechaProc;
    private String reimbAttr;
    private String numeroMensaje;
    
    public String getIndAutor() {
		return indAutor;
	}

	public void setIndAutor(String indAutor) {
		this.indAutor = indAutor;
	}
	
    public String getNumeroMensaje() {
        return numeroMensaje;
    }
    
    public void setNumeroMensaje( String numeroMensaje ) {
        this.numeroMensaje = numeroMensaje;
    }
    
    public String getCodigoTransaccion() {
        return codigoTransaccion;
    }
    
    public void setCodigoTransaccion( String codigoTransaccion ) {
        this.codigoTransaccion = codigoTransaccion;
    }
    
    public String getNumeroTarjeta() {
        return numeroTarjeta;
    }
    
    public void setNumeroTarjeta( String numeroTarjeta ) {
        this.numeroTarjeta = numeroTarjeta;
    }
    
    public String getNumberExt() {
        return numberExt;
    }
    
    public String getMonedaDest() {
        return monedaDest;
    }
    
    public void setMonedaDest( String monedaDest ) {
        this.monedaDest = monedaDest;
    }
    
    public void setNumberExt( String numberExt ) {
        this.numberExt = numberExt;
    }
    
    public String getFormCode() {
        return formCode;
    }
    
    public void setFormCode( String formCode ) {
        this.formCode = formCode;
    }
    
    public String getBinFuente() {
        return binFuente;
    }
    
    public void setBinFuente( String binFuente ) {
        this.binFuente = binFuente;
    }
    
    public String getFechaCapt() {
        return fechaCapt;
    }
    
    public void setFechaCapt( String fechaCapt ) {
        this.fechaCapt = fechaCapt;
    }
    
    public String getTipoVenta() {
        return tipoVenta;
    }
    
    public void setTipoVenta( String tipoVenta ) {
        this.tipoVenta = tipoVenta;
    }
    
    public String getNumCuo() {
        return numCuo;
    }
    
    public void setNumCuo( String numCuo ) {
        this.numCuo = numCuo;
    }
    
    public String getNumIdn() {
        return numIdn;
    }
    
    public void setNumIdn( String numIdn ) {
        this.numIdn = numIdn;
    }
    
    public String getCheckDig() {
        return checkDig;
    }
    
    public void setCheckDig( String checkDig ) {
        this.checkDig = checkDig;
    }
    
    public String getAcqMembId() {
        return acqMembId;
    }
    
    public void setAcqMembId( String acqMembId ) {
        this.acqMembId = acqMembId;
    }
    
    public String getFechaComp() {
        return fechaComp;
    }
    
    public void setFechaComp( String fechaComp ) {
        this.fechaComp = fechaComp;
    }
    
    public String getMontoDest() {
        return montoDest;
    }
    
    public void setMontoDest( String montoDest ) {
        this.montoDest = montoDest;
    }
    
    public String getMontoFuen() {
        return montoFuen;
    }
    
    public void setMontoFuen( String montoFuen ) {
        this.montoFuen = montoFuen;
    }
    
    public String getMonedaFuen() {
        return monedaFuen;
    }
    
    public void setMonedaFuen( String monedaFuen ) {
        this.monedaFuen = monedaFuen;
    }
    
    public String getNombComer() {
        return nombComer;
    }
    
    public void setNombComer( String nombComer ) {
        this.nombComer = nombComer;
    }
    
    public String getFechaDCompensacion() {
        return fechaDCompensacion;
    }
    
    public void setFechaDCompensacion( String fechaDCompensacion ) {
        this.fechaDCompensacion = fechaDCompensacion;
    }
    
    public String getOfic() {
        return ofic;
    }
    
    public void setOfic( String ofic ) {
        this.ofic = ofic;
    }
    
    public String getCiudadComer() {
        return ciudadComer;
    }
    
    public void setCiudadComer( String ciudadComer ) {
        this.ciudadComer = ciudadComer;
    }
    
    public String getTdab() {
        return tdab;
    }
    
    public void setTdab( String tdab ) {
        this.tdab = tdab;
    }
    
    public String getPais() {
        return pais;
    }
    
    public void setPais( String pais ) {
        this.pais = pais;
    }
    
    public String getRubroComer() {
        return rubroComer;
    }
    
    public void setRubroComer( String rubroComer ) {
        this.rubroComer = rubroComer;
    }
    
    public String getZipCodeComer() {
        return zipCodeComer;
    }
    
    public void setZipCodeComer( String zipCodeComer ) {
        this.zipCodeComer = zipCodeComer;
    }
    
    public String getCodMunic() {
        return codMunic;
    }
    
    public void setCodMunic( String codMunic ) {
        this.codMunic = codMunic;
    }
    
    public String getUsagCode() {
        return usagCode;
    }
    
    public void setUsagCode( String usagCode ) {
        this.usagCode = usagCode;
    }
    
    public String getCodRazon() {
        return codRazon;
    }
    
    public void setCodRazon( String codRazon ) {
        this.codRazon = codRazon;
    }
    
    public String getSettleFlag() {
        return settleFlag;
    }
    
    public void setSettleFlag( String settleFlag ) {
        this.settleFlag = settleFlag;
    }
    
    public String getCodAutor() {
        return codAutor;
    }
    
    public void setCodAutor( String codAutor ) {
        this.codAutor = codAutor;
    }
    
    public String getPosTerminalCapability() {
        return posTerminalCapability;
    }
    
    public void setPosTerminalCapability( String posTerminalCapability ) {
        this.posTerminalCapability = posTerminalCapability;
    }
    
    public String getIntFeeIndic() {
        return intFeeIndic;
    }
    
    public void setIntFeeIndic( String intFeeIndic ) {
        this.intFeeIndic = intFeeIndic;
    }
    
    public String getCardHolderIdMethod() {
        return cardHolderIdMethod;
    }
    
    public void setCardHolderIdMethod( String cardHolderIdMethod ) {
        this.cardHolderIdMethod = cardHolderIdMethod;
    }
    
    public String getPosEntryMode() {
        return posEntryMode;
    }
    
    public void setPosEntryMode( String posEntryMode ) {
        this.posEntryMode = posEntryMode;
    }
    
    public String getFechaProc() {
        return fechaProc;
    }
    
    public void setFechaProc( String fechaProc ) {
        this.fechaProc = fechaProc;
    }
    
    public String getReimbAttr() {
        return reimbAttr;
    }
    
    public void setReimbAttr( String reimbAttr ) {
        this.reimbAttr = reimbAttr;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 17/04/2019, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    public String getNumeroReferencia() {
        
        StringBuffer str = new StringBuffer();
        str.append( this.formCode ).append( this.binFuente )
                .append( this.fechaCapt ).append( this.tipoVenta )
                .append( this.numCuo ).append( this.numIdn )
                .append( this.checkDig );
        return str.toString();
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 17/04/2019, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @param numRef
     * @since 1.X
     */
    public void setNumeroReferencia( String numRef ) {
        this.formCode = numRef.substring( 0, 1 );
        this.binFuente = numRef.substring( 1, 7 );
        this.fechaCapt = numRef.substring( 7, 11 );
        this.tipoVenta = numRef.substring( 11, 12 );
        this.numCuo = numRef.substring( 12, 14 );
        this.numIdn = numRef.substring( 14, 22 );
        this.checkDig = numRef.substring( 22, 23 );
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 17/04/2019, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    public String getNombreUbicAcepTarjeta() {
        StringBuffer str = new StringBuffer();
        str.append( this.nombComer )
                .append( ConstantesUtil.BACK_SLASH.toString() )
                .append( this.ciudadComer )
                .append( ConstantesUtil.BACK_SLASH.toString() )
                .append( this.pais );
        
        return str.toString();
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 17/04/2019, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @param nombreUbicTarjet
     * @since 1.X
     */
    public void setNombreUbicAcepTarjeta( String nombreUbicTarjet ) {
        String str[] = nombreUbicTarjet.split( "\\\\" );
        this.nombComer = str[0];
        this.ciudadComer = str[1];
        this.pais = str[2];
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 17/04/2019, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    public String getCodIdentDestTransc() {
        StringBuffer str = new StringBuffer();
        str.append( this.settleFlag )
        .append( ConstantesUtil.SLASH.toString() )
        .append( this.indAutor )
        .append( ConstantesUtil.SLASH.toString() )
        .append( this.codAutor )
         .append( ConstantesUtil.SLASH.toString() )
        .append( this.posTerminalCapability )
        .append( ConstantesUtil.SLASH.toString() )
        .append( this.intFeeIndic )
         .append( ConstantesUtil.SLASH.toString() )
        .append( this.cardHolderIdMethod );
        
        return str.toString();
    }
    
    public String getBinTarjeta() {
        return numeroTarjeta.substring( 0, 6 );
        
    }
    
    public String getBinAdquiriente() {
        return getNumeroReferencia().substring( 0, 6 );     
    }
    
    
	@Override
	public String toString() {
		return "Registro00OutVisaNacDTO [codigoTransaccion="
				+ codigoTransaccion + ", numeroTarjeta=" + numeroTarjeta
				+ ", numberExt=" + numberExt + ", formCode=" + formCode
				+ ", binFuente=" + binFuente + ", fechaCapt=" + fechaCapt
				+ ", tipoVenta=" + tipoVenta + ", numCuo=" + numCuo
				+ ", numIdn=" + numIdn + ", checkDig=" + checkDig
				+ ", acqMembId=" + acqMembId + ", fechaComp=" + fechaComp
				+ ", montoDest=" + montoDest + ", monedaDest=" + monedaDest
				+ ", montoFuen=" + montoFuen + ", monedaFuen=" + monedaFuen
				+ ", nombComer=" + nombComer + ", fechaDCompensacion="
				+ fechaDCompensacion + ", ofic=" + ofic + ", ciudadComer="
				+ ciudadComer + ", tdab=" + tdab + ", pais=" + pais
				+ ", rubroComer=" + rubroComer + ", zipCodeComer="
				+ zipCodeComer + ", codMunic=" + codMunic + ", usagCode="
				+ usagCode + ", codRazon=" + codRazon + ", settleFlag="
				+ settleFlag + ", codAutor=" + codAutor
				+ ", posTerminalCapability=" + posTerminalCapability
				+ ", intFeeIndic=" + intFeeIndic + ", cardHolderIdMethod="
				+ cardHolderIdMethod + ", posEntryMode=" + posEntryMode
				+ ", fechaProc=" + fechaProc + ", reimbAttr=" + reimbAttr
				+ ", numeroMensaje=" + numeroMensaje + "]";
	}
    
 
    
}
