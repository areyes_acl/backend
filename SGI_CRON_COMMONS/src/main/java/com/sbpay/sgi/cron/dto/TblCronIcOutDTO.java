package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 16/11/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * Clase encargada de mapear la tabla, TBL_CRON_IC_OUT
 * 
 * 
 * 
 * </p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class TblCronIcOutDTO implements Serializable {
    /**
	 * 
	 */
    private static final long serialVersionUID = 2349298215535090169L;
    private long sid; 
	private String fecha;
	private String fileName; 
	private String fileFlag; 
	private String fileTs;
	public long getSid() {
		return sid;
	}
	public void setSid(long sid) {
		this.sid = sid;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileFlag() {
		return fileFlag;
	}
	public void setFileFlag(String fileFlag) {
		this.fileFlag = fileFlag;
	}
	public String getFileTs() {
		return fileTs;
	}
	public void setFileTs(String fileTs) {
		this.fileTs = fileTs;
	}
	@Override
	public String toString() {
		return "TblCronIcOutDTO [sid=" + sid + ", fecha=" + fecha
				+ ", fileName=" + fileName + ", fileFlag=" + fileFlag
				+ ", fileTs=" + fileTs + "]";
	}
        
}
