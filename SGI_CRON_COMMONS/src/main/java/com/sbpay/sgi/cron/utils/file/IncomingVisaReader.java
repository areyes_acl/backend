package com.sbpay.sgi.cron.utils.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dto.IncomingVisaDTO;
import com.sbpay.sgi.cron.dto.Registro00OutVisaCobroCargoDTO;
import com.sbpay.sgi.cron.dto.Registro00OutVisaIntDTO;
import com.sbpay.sgi.cron.dto.Registro00OutVisaPagoDTO;
import com.sbpay.sgi.cron.dto.Registro01OutVisaIntDTO;
import com.sbpay.sgi.cron.dto.Registro02OutVisaIntDTO;
import com.sbpay.sgi.cron.dto.Registro03OutVisaIntDTO;
import com.sbpay.sgi.cron.dto.Registro04OutVisaIntDTO;
import com.sbpay.sgi.cron.dto.Registro05OutVisaIntDTO;
import com.sbpay.sgi.cron.dto.Registro06OutVisaIntDTO;
import com.sbpay.sgi.cron.dto.Registro07OutVisaIntDTO;
import com.sbpay.sgi.cron.dto.Registro0DOutVisaIntDTO;
import com.sbpay.sgi.cron.dto.Registro2DOutVisaIntDTO;
import com.sbpay.sgi.cron.dto.TransaccionOutVisa;
import com.sbpay.sgi.cron.dto.TransaccionOutVisaCobroCargo;
import com.sbpay.sgi.cron.dto.TransaccionOutVisaPago;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.MsgErrorFile;
import com.sbpay.sgi.cron.enums.MsgErrorProcessFile;
import com.sbpay.sgi.cron.utils.generals.DateUtils;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 25/04/2019, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class IncomingVisaReader implements IncomingVisaFileUtils{
    
    /** VARIABLE PARA EL LOGER */
    private static final Logger LOGGER = Logger
            .getLogger( IncomingVisaReader.class );
    
    private static final String CODIGO_CABECERA_INCOMING = "90";
    private static final String CODIGO_REGISTRO_00 = "00";
    private static final String CODIGO_REGISTRO_01 = "01";
    private static final String CODIGO_REGISTRO_05 = "05";
    private static final String CODIGO_REGISTRO_07 = "07";
    
    private static final String CODIGO_REGISTRO_02 = "02";
    private static final String CODIGO_REGISTRO_03 = "03";
    private static final String CODIGO_REGISTRO_04 = "04";
    private static final String CODIGO_REGISTRO_06 = "06";
    private static final String CODIGO_REGISTRO_0D = "0D";
    private static final String CODIGO_REGISTRO_2D = "2D";
    
    private static final String CODIGO_FINAL_ARCHIVO = "92";
    private static final String CODIGO_SUMA_TOTALES = "91";
    private static final String CODIGO_REGISTRO_46 = "46";
    private static final String CODIGO_REGISTRO_47 = "47";
    
    private static final String CODIGO_REGISTRO_10 = "10";
    private static final String CODIGO_REGISTRO_20 = "20";
    
    private String settlementDateHeader;
    
    private static final int ESPACIO = 2; // Parametro para procesar el fichero con o sin espacios EJ: 0 = (con 2 espacios), (2 = sin espacios)

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 25/04/2019, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * Metodo que lee un archivo incoming visa internacional desde una ruta especificada
     * y lo transforma a DTO,
     * 
     * 
     * @param ruta
     * @return
     * @throws AppException
     * @since 1.X
     */
	@Override
	public IncomingVisaDTO readIncomingVisaFile(String ruta, String starWith)
			 throws AppException {
        BufferedReader buffReader = null;
        IncomingVisaDTO incoming = null;
        
        LOGGER.info("ruta: "+ruta);
        try {
            buffReader = new BufferedReader( new FileReader( ruta ) );
            String[] tokens = ruta.split( "[\\\\|x/]" );
            String filename = tokens[tokens.length - 1];
            
            incoming = readFile( buffReader, filename, starWith );
            
        }
        catch ( IOException ioe ) {
            throw new AppException( "Ha ocurrido un error al leer el archivo",
                    ioe );
        }
        finally {
            try {
                if ( buffReader != null ) {
                    buffReader.close();
                }
            }
            catch ( IOException ioe1 ) {
                throw new AppException(
                        "Ha ocurrido un error al cerrar el archivo", ioe1 );
            }
        }
        
        return incoming;
	}

	 /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 25/04/2019, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @param listaTransacciones
     * @param buffReader
     * @param line
     * @param filename
     * @return
     * @throws AppException
     * @throws IOException
     * @since 1.X
     */
    private IncomingVisaDTO readFile( BufferedReader buffReader, String filename,
            String starWith ) throws AppException, IOException {
        
        String line = buffReader.readLine();
        List<TransaccionOutVisa> listaTransacciones = new ArrayList<TransaccionOutVisa>();
        List<TransaccionOutVisaPago> listaPagos = new ArrayList<TransaccionOutVisaPago>();
        List<TransaccionOutVisaCobroCargo> listaCobroCargo= new ArrayList<TransaccionOutVisaCobroCargo>();
        
        IncomingVisaDTO incoming;
        TransaccionOutVisa transaccion = null;
        TransaccionOutVisaPago transaccionPago = null;
        TransaccionOutVisaCobroCargo transaccionCobroCargo = null;
        int numeroTransaccion = 0;
        
        // RECORRE EL FILE LEYENDO LINEA POR LINEA
        while ( line != null ) {
            // EL CODIGO DE LA CABECERA
            String codigo = line.substring( 0, 2 );
            
            if ( CODIGO_CABECERA_INCOMING.equalsIgnoreCase( codigo ) ) {
            	validarFechaArchivoIncomingVisa( filename, line, starWith );
            }
            else
                if ( CODIGO_FINAL_ARCHIVO.equalsIgnoreCase( codigo ) ) {
                    LOGGER.debug( "*** SE TERMINO DE LEER EL ARCHIVO, CODIGO 92 ****" );
                    if(transaccion != null){
                    listaTransacciones.add( transaccion );
                    }
                    if(transaccionPago != null){
                    	listaPagos.add( transaccionPago );
                    }
                    if(transaccionCobroCargo != null){
                    	listaCobroCargo.add(transaccionCobroCargo);
                    }
                                    
                }else if (CODIGO_SUMA_TOTALES.equalsIgnoreCase( codigo )){
                    LOGGER.debug( "*** SUMATORIA INTERMEDIA DEL OUTGOING VISA NACIONAL, CODIGO 91 ****" );
                }else if (CODIGO_REGISTRO_00.equalsIgnoreCase( codigo )){
                    LOGGER.debug( "*** REGISTRO EXTRA DEL ARCHIVO DE VISA, CODIGO 00 ****" );
                }else if (CODIGO_REGISTRO_47.equalsIgnoreCase( codigo )){
                    LOGGER.debug( "*** REGISTRO DE PAGO, CODIGO 47  ****" );
                }else {
                    String transCode = line.substring( 4-ESPACIO, 6-ESPACIO );
                    String nodata = line.substring( 43, 44 ); //Indicador que determina si la linea contiene datos o no
                    String report = line.substring( 58, 60 ); // reporte V5
                    if(CODIGO_REGISTRO_00.equalsIgnoreCase( transCode ) && CODIGO_REGISTRO_46.equalsIgnoreCase( codigo )){
                    	LOGGER.debug( "*** REGISTRO DE PAGO, CODIGO 46 ****" );
                    	LOGGER.debug( "TC46 No Data? = "+nodata+" REPORT = "+ report );
                    	/**
                         * Si es 00 y 46 y no es la primera transacción se
                         * agrega la lista de transacciones
                         */
                    	if(!"Y".equals(nodata) && "V5".equals(report)){
                    		if ( transaccionPago != null ) {
                            	listaPagos.add( transaccionPago );
                            }
                            
                            transaccionPago = new TransaccionOutVisaPago();
                            transaccionPago.setRegistro00( leerRegistro00Pago( line ) );
                    	}
                    	
                        
                        
                    }else if(CODIGO_REGISTRO_00.equalsIgnoreCase( transCode ) && (CODIGO_REGISTRO_10.equalsIgnoreCase(codigo) || CODIGO_REGISTRO_20.equalsIgnoreCase(codigo))){
                    	LOGGER.info("hay registros 10 o 20 de Visa");
                    	if(transaccionCobroCargo != null){
                    		listaCobroCargo.add(transaccionCobroCargo);
                    	}
                    	
                    	transaccionCobroCargo = new TransaccionOutVisaCobroCargo();
                    	transaccionCobroCargo.setRegistro1020(leerRegistro00CobroCargo(line));
                    	LOGGER.info("====" + transaccionCobroCargo);
                    	
                    }else if(CODIGO_REGISTRO_00.equalsIgnoreCase( transCode )) {
                        /**
                         * Si es 00 y no es la primera transacción se
                         * agrega la lista de transacciones
                         */
                        if ( transaccion != null ) {
                            listaTransacciones.add( transaccion );
                        }
                        transaccion = new TransaccionOutVisa();
                        transaccion.setNumeroTransaccion( numeroTransaccion );
                        numeroTransaccion++;
                        transaccion.setRegistro00( leerRegistro00( line ) );
                    }
                    else if ( CODIGO_REGISTRO_03
                            .equalsIgnoreCase( transCode )  && transaccion != null ) {
                        
                        transaccion.setRegistro03( leerRegistro03( line ) );
                    } else if ( CODIGO_REGISTRO_02.equalsIgnoreCase( transCode )  && transaccion != null ) {
                        transaccion.setRegistro02( leerRegistro02( line ) );
                    } else
                	if ( CODIGO_REGISTRO_04.equalsIgnoreCase( transCode )  && transaccion != null) {
                        transaccion.setRegistro04( leerRegistro04( line ) );
                    } else if ( CODIGO_REGISTRO_06
                            .equalsIgnoreCase( transCode )  && transaccion != null) {
                        
                        transaccion.setRegistro06( leerRegistro06( line ) );
                    } else if ( CODIGO_REGISTRO_0D.equalsIgnoreCase( transCode )  && transaccion != null) {
                        
                        transaccion.setRegistro0D( leerRegistro0D( line ) );
                    }else if ( CODIGO_REGISTRO_2D.equalsIgnoreCase( transCode )  && transaccion != null ) {
                        
                        transaccion.setRegistro2D( leerRegistro2D( line ) );
                    }else
                        if ( CODIGO_REGISTRO_01.equalsIgnoreCase( transCode ) && transaccion != null) {
                        	
                            transaccion.setRegistro01( leerRegistro01( line ) );
                            LOGGER.info(transaccion);
                        }
                        else
                            if ( CODIGO_REGISTRO_05
                                    .equalsIgnoreCase( transCode )  && transaccion != null ) {
                                
                                transaccion
                                        .setRegistro05( leerRegistro05( line ) );
                            }
                            else
                                if ( CODIGO_REGISTRO_07
                                        .equalsIgnoreCase( transCode )  && transaccion != null) {
                                    transaccion
                                            .setRegistro07( leerRegistro07( line ) );
                                }
                    
                }
            line = buffReader.readLine();
        }
        
        LOGGER.info( "********* CANTIDAD DE TRANSACCIONES : "
                + listaTransacciones.size() + " **********" );
        
        incoming = new IncomingVisaDTO();
        incoming.setIncomingName( filename );
        incoming.setListaTransacciones( listaTransacciones );
        incoming.setListaPago(listaPagos);
        incoming.setListaCobroCargo(listaCobroCargo);
        return incoming;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 25/04/2019, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * Metodo que recibe un file y lo transforma a DTO
     * 
     * @param file
     * @return
     * @throws AppException
     * @see com.sbpay.sgi.cron.utils.file.IncomingFileUtils#readIncomingFile(java.io.File)
     * @since 1.X
     */
    @Override
    public IncomingVisaDTO readIncomingVisaFile( File file, String starWith )
            throws AppException {
        
        BufferedReader buffReader = null;
        IncomingVisaDTO incoming = null;
        
        try {
            
            if ( file == null ) {
                throw new AppException(
                        MsgErrorFile.ERROR_READ_FILE_IS_NULL.toString() );
            }
            
            String ruta = file.getAbsolutePath();
            String filename = file.getName();
            
            buffReader = new BufferedReader( new FileReader( ruta ) );
            incoming = readFile( buffReader, filename, starWith );
            LOGGER.info(incoming);
            
        }
        catch ( IOException ioe ) {
            throw new AppException(
                    "Ha ocurrido un error al leer el archivo : "
                            + ioe.getMessage(), ioe );
        }
        finally {
            try {
                if ( buffReader != null ) {
                    buffReader.close();
                }
            }
            catch ( IOException ioe1 ) {
                throw new AppException(
                        "Ha ocurrido un error al cerrar el archivo", ioe1 );
            }
        }
        
        return incoming;
    }
    
    private Registro02OutVisaIntDTO leerRegistro02( String line ) {
    	Registro02OutVisaIntDTO registro02 = new Registro02OutVisaIntDTO();
    	// 1–2 Transaction Code (2)
    	registro02.setTipoTransaccion( line.substring( 0, 2 ) );
    	// 3+4 Transaction Code Qualifier + Transaction Component Sequence Number (2)
    	registro02.setTransCode( line.substring( 4-ESPACIO, 6-ESPACIO ) ); 
    	// 5 Sale Type Indicator (Tipo Venta) (1)
    	registro02.setTipoVenta( line.substring( 6-ESPACIO, 7-ESPACIO));
    	// 6-8 Number of Installment Payments (3)
    	registro02.setNumPagosPlazo( line.substring( 7-ESPACIO, 10-ESPACIO ));
    	// 9-11 Installment Payment Number (3)
    	registro02.setNumPagosPlazoActual( line.substring( 10-ESPACIO, 13-ESPACIO ));
    	// 12 Issuer Promo FlagFlag Promo-Emisora (1)
    	registro02.setFlagPromoEmisora( line.substring( 13-ESPACIO, 14-ESPACIO ));
    	// 13 Deferred FlagFlag Diferido (1)
    	registro02.setFlagDiferido( line.substring( 14-ESPACIO, 15-ESPACIO ));
    	// 14 Deferred periodPeriodos de Diferido (1)
    	registro02.setPeriodoDiferido( line.substring( 15-ESPACIO, 16-ESPACIO ));
    	// 15 Grace Period FlagFlag Mes de Gracia (1)
    	registro02.setFlagMesGracias( line.substring( 16-ESPACIO, 17-ESPACIO ));
    	// 16 Grace PeriodPeriodos de Gracia (1)
    	registro02.setPeriodoMesGracia( line.substring( 17-ESPACIO, 18-ESPACIO ));
    	// 17–19 Country Code (3)
    	registro02.setCodigoPais( line.substring( 18-ESPACIO, 21-ESPACIO ));
    	// 20-31 Installment Payment Total Amount (12)
    	registro02.setPagoTotalAPlazos( line.substring( 21-ESPACIO, 33-ESPACIO ));
    	// 32-43 Installment Payment Amount (12)
    	registro02.setValorPagoAPlazos( line.substring( 33-ESPACIO, 45-ESPACIO ));
    	// 44-48 Installments Interest Rate (5)
    	registro02.setCuotasInteres( line.substring( 45-ESPACIO, 50-ESPACIO ));
    	// 49-52 VAT Rate (4)
    	registro02.setTasaIVA( line.substring( 50-ESPACIO, 54-ESPACIO ));
    	// 53-64 Interchange Reimbursement Fee (IRF) (12)
    	registro02.setTarifaReemIntercambioNacional( line.substring( 54-ESPACIO, 66-ESPACIO ));
    	// 65-76 VAT National Interchange Reimbursement Fee (12)
    	registro02.setTasaReemIntercambioNacionalIVA( line.substring( 66-ESPACIO, 78-ESPACIO ));
    	// 77-82 Deferred Settlement Date (YYMMDD) (6)
    	registro02.setFechaLiquidacionDiferida( line.substring( 78-ESPACIO, 84-ESPACIO ));
    	// 83-88 Deferred Settlement Date of the Original (YYMMDD) (6)
    	registro02.setFechaLiquidacionDiferidaOriginal( line.substring( 84-ESPACIO, 90-ESPACIO ));
    	// 89-168 Reserved (80)
    	registro02.setReservado( line.substring( 90-ESPACIO, 170-ESPACIO ));
    	
    	return registro02;
    }
    
    private Registro03OutVisaIntDTO leerRegistro03( String line ) {
        Registro03OutVisaIntDTO registro03 = new Registro03OutVisaIntDTO();
        registro03.setAll(line);
        return registro03;
    }
    
    private Registro04OutVisaIntDTO leerRegistro04( String line ) {
        Registro04OutVisaIntDTO registro04 = new Registro04OutVisaIntDTO();
        registro04.setAll(line);
        return registro04;
    }
    
    private Registro06OutVisaIntDTO leerRegistro06( String line ) {
    	Registro06OutVisaIntDTO registro06 = new Registro06OutVisaIntDTO();
    	registro06.setAll(line);
        return registro06;
    }
    
    private Registro0DOutVisaIntDTO leerRegistro0D( String line ) {
    	Registro0DOutVisaIntDTO registro0D = new Registro0DOutVisaIntDTO();
    	registro0D.setAll(line);
        return registro0D;
    }
    
    private Registro2DOutVisaIntDTO leerRegistro2D( String line ) {
    	Registro2DOutVisaIntDTO registro2D = new Registro2DOutVisaIntDTO();
    	registro2D.setAll(line);
        return registro2D;
    }
    
    private Registro00OutVisaCobroCargoDTO leerRegistro00CobroCargo(String fila){
    	Registro00OutVisaCobroCargoDTO registro00 = new Registro00OutVisaCobroCargoDTO();
    	
    	//1-2	(02) Transaction Code
    	registro00.setMit(fila.substring(0, 2));
    	//3+4	(02) Transaction Code Qualifier + Transaction Component Sequence Number
    	registro00.setCodigoFuncion(fila.substring(2, 4));
    	//5-10	(06) Destination BIN
    	registro00.setDestinationBin(fila.substring(4, 10));
    	//11-16	(06) Source BIN
    	registro00.setSourceBin(fila.substring(10, 16));
    	//17-20 (04) Reason Code
    	registro00.setReasonCode(fila.substring(16, 20));
    	//21–23 (03) Country Code
    	registro00.setCountryCode(fila.substring(20, 23));
    	//24–27 (04) Event Date (MMDD)
    	registro00.setEventDate(fila.substring(23, 27));
    	//28–43 (16) Account Number
    	registro00.setAccountNumber(fila.substring(27, 43));
    	//44–46 (03) Account Number Extension
    	registro00.setAccountNumberExtension(fila.substring(43, 46));
    	//47–58 (12) Destination Amount
    	registro00.setDestinationAmount(fila.substring(46, 58));
    	//59–61 (03) Destination Currency Code
    	registro00.setDestinationCurrencyCode(fila.substring(58, 61));
    	//62–73 (12) Source Amount
    	registro00.setSourceAmount(fila.substring(61, 73));
    	//74–76 (03) Source Currency Code
    	registro00.setSourceCurrencyCode(fila.substring(73, 76));
    	//77–146 (70) Message Text
    	registro00.setMessageText(fila.substring(76, 146));
    	//147 	(01) Settlement Flag
    	registro00.setSettlementFlag(fila.substring(146, 147));
    	//148–162 (15) Transaction Identifier
    	registro00.setTransactionIdentifier(fila.substring(147, 162));
    	//163 	(01) Reserved
    	registro00.setReserved(fila.substring(162, 163));
    	//164–167 (04) Central Processing Date (YDDD)
    	registro00.setCentralProcessingDate(fila.substring(163, 167));
    	//168 (01) Reimbursement Attribute
    	registro00.setReimbursementAttribute(fila.substring(167, 168));
    	//operador
    	
    	//Datos adicionales
    	registro00.setDatosAdicionales(fila.substring(0, 168));
    	
    	return registro00;
    }
    
    private Registro00OutVisaPagoDTO leerRegistro00Pago( String fila ) {
    	Registro00OutVisaPagoDTO registro00 = new Registro00OutVisaPagoDTO();
    	// 1–2 (02) Transaction Code
        registro00.setMit( fila.substring( 0, 2 ) );
        // 3+4 (02) Transaction Code Qualifier + Transaction Component Sequence Number
        registro00.setCodigoFuncion(fila.substring( 2, 4 ));
        // 5-10 (06) Destination BIN
        registro00.setDestinationBIN(fila.substring( 4, 10 ));
        // 11-16 (06) Source BIN
        registro00.setSourceBIN(fila.substring( 10, 16 ));
        // 17-26 (10) Reporting for SRE Identifier
        registro00.setRepSREIdentifier(fila.substring( 16, 26 ));
        // 27-36 (10) Recap For SRE Identifier
        registro00.setRecSREIdentifier(fila.substring( 26, 36 ));
        // 37–39 (03) Settlement Service Identifier
        registro00.setServiceIdentifier(fila.substring( 36, 39 ));
        // 40–42 (03) Settlement Currency Code
        registro00.setCurrencyCode(fila.substring( 39, 42 ));
        // 43 (01) Business Mode
        registro00.setBusinessMode(fila.substring( 42, 43 ));
        // 44 (01) No Data Indicator / fila.substring( 43, 44 )
        // 45–58 (14) Reserved
        registro00.setReserved1(fila.substring( 44, 58 ));
        // 59 + 60 + (61–63)  (05) Report Group + Report Subgroup + Report Identifier Number
        registro00.setReport(fila.substring( 58, 63 ));
        // 64–65 (02) Report Identification Suffix
        // 66–72 (07) Settlement Date
        registro00.setSettlementDate(fila.substring( 65, 72 ));
        // 73–79 (07) Report Date
        registro00.setReportDate(fila.substring( 72, 79 ));
        // 80–94 (15) Total Interchange Count
        registro00.setTotalInterchangeCount(fila.substring( 79, 94 ));
        // 95–109 (15) Total Interchange Value
        registro00.setTotalInterchangeValue(fila.substring( 94, 109 ));
        // 110–111 (02) Interchange Value Sign
        registro00.setInterchangeValueSign(fila.substring( 109, 111 ));
        // 112–126 (15) Total Reimbursement Fees
        registro00.setTotalReimbursementFees(fila.substring( 111, 126 ));
        // 127–128 (02) Reimbursement Fees Sign
        registro00.setReimbursementFeesSign(fila.substring( 126, 128 ));
        // 129–143 (15) Total Visa Charges
        registro00.setTotalVisaCharges(fila.substring( 128, 143 ));
        // 144–145 (2) Visa Charges Sign
        registro00.setVisaChargesSign(fila.substring( 143, 145 ));
        // 146–160 (15) Net Settlement Amount
        registro00.setNetSettlementAmount(fila.substring( 145, 160 ));
        // 161–162 (02) Net Settlement Amount Sign
        registro00.setNetSettlementAmountSign(fila.substring( 160, 162 ));
        // 163–164 (02) Summary Level
        registro00.setSummaryLevel(fila.substring( 162, 164 ));
        // (165–167) + 168 (03) Reserved + Reimbursement Attribute
        registro00.setReserved2(fila.substring( 164, 168 ));
        
        return registro00;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 25/04/2019, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @param fila
     * @return
     * @since 1.X
     */
    private Registro00OutVisaIntDTO leerRegistro00( String fila ) {
    	Registro00OutVisaIntDTO registro00 = new Registro00OutVisaIntDTO();
    	// 01–02 (02) Transaction Code
        registro00.setCodigoTransaccion( fila.substring( 0, 2 ) );
        registro00.setTransCode( fila.substring( 4-ESPACIO, 6-ESPACIO ) );
        // 05–20 (16) Account Number
        registro00.setNumeroTarjeta( fila.substring( 6-ESPACIO, 22-ESPACIO ) );
        // 21–23 (03) Account Number Extension
        registro00.setNumberExt( fila.substring( 22-ESPACIO, 25-ESPACIO ) );
        // 27–49 (23) Acquirer Reference Number
        registro00.setFormCode( fila.substring( 28-ESPACIO, 29-ESPACIO ) );
        registro00.setBinFuente( fila.substring( 29-ESPACIO, 35-ESPACIO ) );
        registro00.setFechaCapt( fila.substring( 35-ESPACIO, 39-ESPACIO ) );
        registro00.setTipoVenta( fila.substring( 39-ESPACIO, 40-ESPACIO ) );
        registro00.setNumCuo( fila.substring( 40-ESPACIO, 42-ESPACIO ) );
        registro00.setNumIdn( fila.substring( 42-ESPACIO, 50-ESPACIO ) );
        registro00.setCheckDig( fila.substring( 50-ESPACIO, 51-ESPACIO ) );
        // 50–57 (08) Acquirer's Business ID
        registro00.setAcqMembId( fila.substring( 51-ESPACIO, 59-ESPACIO ) );
        // 58–61 (04) Purchase Date (MMDD)
        registro00.setFechaComp( fila.substring( 59-ESPACIO, 63-ESPACIO ) );
        // 62–73 (12) Destination Amount
        registro00.setMontoDest( fila.substring( 63-ESPACIO, 75-ESPACIO ) );
        // 74–76 (03) Destination Currency Code
        registro00.setMonedaDest( fila.substring( 75-ESPACIO, 78-ESPACIO ) );
        // 77–88 (12) Source Amount
        registro00.setMontoFuen( fila.substring( 78-ESPACIO, 90-ESPACIO ) );
        // 89–91 (03) Source Currency Code
        registro00.setMonedaFuen( fila.substring( 90-ESPACIO, 93-ESPACIO ) );
        // 92–116 (25) Merchant Name
        registro00.setNombComer( fila.substring( 93-ESPACIO, 118-ESPACIO ) );
        /*registro00.setFechaDCompensacion( fila.substring( 112, 116 ) );*/
        /*registro00.setOfic( fila.substring( 116, 118 ) );*/
        // 117–129 (13) Merchant City
        registro00.setCiudadComer( fila.substring( 118-ESPACIO, 131-ESPACIO ) );
        //registro00.setTdab( fila.substring( 130, 131 ) );
        // 130–132 (03) Merchant Country Code
        registro00.setPais( fila.substring( 131-ESPACIO, 134-ESPACIO ) );
        // 133–136 (04) Merchant Category Code
        registro00.setRubroComer( fila.substring( 134-ESPACIO, 138-ESPACIO ) );
        // 137–141 (05 )Merchant ZIP Code
        registro00.setZipCodeComer( fila.substring( 138-ESPACIO, 143-ESPACIO ) );
        
        registro00.setCodMunic( fila.substring( 146-ESPACIO, 148-ESPACIO ) );
        registro00.setUsagCode( fila.substring( 148-ESPACIO, 149-ESPACIO ) );
        
        registro00.setCodRazon( fila.substring( 149-ESPACIO, 151-ESPACIO ) );
        registro00.setSettleFlag( fila.substring( 151-ESPACIO, 152-ESPACIO ) );
        registro00.setIndAutor( fila.substring( 152-ESPACIO, 153-ESPACIO ) );
        registro00.setCodAutor( fila.substring( 153-ESPACIO, 159-ESPACIO ) );
        registro00.setPosTerminalCapability( fila.substring( 159-ESPACIO, 160-ESPACIO ) );
        registro00.setIntFeeIndic( fila.substring( 160-ESPACIO, 161-ESPACIO ) );
        registro00.setCardHolderIdMethod( fila.substring( 161-ESPACIO, 162-ESPACIO ) );
        registro00.setPosEntryMode( fila.substring( 163-ESPACIO, 165-ESPACIO ) );
        registro00.setFechaProc( fila.substring( 165-ESPACIO, 169-ESPACIO ) );
        registro00.setReimbAttr( fila.substring( 169-ESPACIO, 170-ESPACIO ) );
        registro00.setSettlementDateHeader(settlementDateHeader);
        
        LOGGER.info(fila.substring( 59-ESPACIO, 63-ESPACIO ) +"-"+fila.substring( 59, 63));
        
        return registro00;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 - 25/04/2019, (ACL) - versión inicial
     * </ul>
     * <p>
     * 
     * @param line
     * @since 1.X
     */
    private Registro01OutVisaIntDTO leerRegistro01( String line ) {
    	
    	Registro01OutVisaIntDTO registro01 = new Registro01OutVisaIntDTO();
    	// Transaction Code (2)
    	registro01.setTipoTransaccion( line.substring( 0, 2 ) );
    	
    	// Transaction Code Qualifier + Transaction Component Sequence Number (2)
        registro01.setTransCode( line.substring( 4-ESPACIO, 6-ESPACIO ) );
        
        // Business Format Code (1) (6-7)
        // Token Assurance Level (2) (7-9)
        // Reserved (9) (9-18)
        // Chargeback Reference Number (6)
        registro01.setChargRefN( line.substring( 18-ESPACIO, 24-ESPACIO ) );
        
        // Documentation Indicator (1)
        registro01.setDocumInd( line.substring( 24-ESPACIO, 25-ESPACIO ) );
        
        // Member Message Text (50)
        registro01.setMensaje( line.substring( 25-ESPACIO, 75-ESPACIO ) );
        
        // Special Condition Indicators (2)
        registro01.setSpeCondInd( line.substring( 75-ESPACIO, 77-ESPACIO ) );
        
        // Fee Program Indicator (3) (77-80)
        // Issuer Charge (1) (80-81)
        // Reserved (1) (81-82)
        // Card Acceptor ID (15)
        registro01.setCardAcceptorId( line.substring( 82-ESPACIO, 97-ESPACIO ) );
        
        // Terminal ID (8)
        registro01.setTerminalId( line.substring( 97-ESPACIO, 105-ESPACIO ) );
        
        // National Reimbursement Fee (12)
        registro01.setValorCuota( line.substring( 105-ESPACIO, 117-ESPACIO ) );
        
        // Mail/Phone/Electronic Commerce and Payment Indicator (1)
        registro01.setIndicatorTransaction( line.substring( 117-ESPACIO, 118-ESPACIO ) );
        
        // Special Chargeback Indicator (1) (118-119)
        // Interface Trace Number (6)
        registro01.setComisionCic( line.substring( 119-ESPACIO, 125-ESPACIO ) );
        
        // Acceptance Terminal Indicator (1)
        registro01.setCardHolder( line.substring( 125-ESPACIO, 126-ESPACIO ) );
        
        // Prepaid Card Indicator (1)
        registro01.setPrepairCardIn( line.substring( 126-ESPACIO, 127-ESPACIO ) );
        
        // Service Development Field (1) (127-128)
        // AVS Response Code (1) (128-129)
        // Authorization Source Code (1)
        registro01.setAuthSourceCode( line.substring( 129-ESPACIO, 130-ESPACIO ) );
        
        // Purchase Identifier Format (1) (130-131)
        // Account Selection (1)
        registro01.setAtmAccSelec( line.substring( 131-ESPACIO, 132-ESPACIO ) );
        
        // Installment Payment Count (2)
        registro01.setInstalPayCount( line.substring( 132-ESPACIO, 134-ESPACIO ) );
        
        // Purchase Identifier (25)
        registro01.setItemDescrptor( line.substring( 134-ESPACIO, 159-ESPACIO ) );
        
        // Cashback (9)
        registro01.setCashBack( line.substring( 159-ESPACIO, 168-ESPACIO ) );
        
        // Chip Condition Code (1)
        registro01.setChipCondCode( line.substring( 168-ESPACIO, 169-ESPACIO ) );
        
        // POS Environment (1)
        registro01.setPosEnv(line.substring( 169-ESPACIO, 170-ESPACIO ));
        
        return registro01;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 - 25/04/2019, (ACL) - versión inicial
     * </ul>
     * <p>
     * 
     * @param line
     * @since 1.X
     */
    private Registro05OutVisaIntDTO leerRegistro05( String line ) {
    	Registro05OutVisaIntDTO registro05 = new Registro05OutVisaIntDTO();
    	registro05.setAll(line);
        return registro05;
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 25/04/2019, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @param line
     * @return
     * @since 1.X
     */
    private Registro07OutVisaIntDTO leerRegistro07( String line ) {
    	Registro07OutVisaIntDTO registro07 = new Registro07OutVisaIntDTO();
    	registro07.setAll(line);
        return registro07;
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 25/04/2019, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * Valida si la fecha del nombre del archivo es la misma que
     * contiene el archivo en su cabecera
     * 
     * 
     * @throws AppException
     * 
     * @since 1.X
     */
    private void validarFechaArchivoIncomingVisa( String filename, String line, String starWith ) throws AppException {
        LOGGER.info(starWith);
        LOGGER.info(line);
        LOGGER.info(filename);
        String fechaNombreArchivo = DateUtils.getDateStringFromFilenameVisa(
                filename, starWith );
        fechaNombreArchivo = DateUtils.getDateFileVisaNac( fechaNombreArchivo )
                .replaceAll( ConstantesUtil.SLASH.toString(),
                        ConstantesUtil.EMPTY.toString() );
        
        String fechaContenidoArchivo = DateUtils.getDateStringFromFileHeaderContent( line, 10-ESPACIO, 15-ESPACIO );
        settlementDateHeader = DateUtils.getDateStringFromFileHeaderContent2( line, 19 , 24 );
        
        LOGGER.info( "Fecha en el Nombre del Archivo :" + fechaNombreArchivo );
        LOGGER.info( "Fecha en el Contenido del Archivo :" + fechaContenidoArchivo );
        LOGGER.info( "Fecha en el Settlement Date :" + settlementDateHeader );
        if ( DateUtils.dateAreEquals( fechaNombreArchivo, fechaContenidoArchivo ) ) {
            LOGGER.info( "(OK) Fechas validas : La fecha que tiene el nombre del archivo es igual a la que posee este en su cabecera" );
        }
        else {
            throw new AppException(
                    MsgErrorProcessFile.ERROR_INVALID_DATES.toString() );
        }
        
    }


}
