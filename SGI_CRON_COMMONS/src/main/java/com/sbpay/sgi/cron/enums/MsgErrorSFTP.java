package com.sbpay.sgi.cron.enums;

public enum MsgErrorSFTP {
    ERROR_CONNECTION_SFTP("Error al intentar realizar la conexion con el servidor SFTP."), 
	ERROR_FILE_CLOSE_SFTP("Error al cerrar el archivo descargado del SFTP INCOMING de Transbank."), 
	ERROR_UPLOAD_READ_FILE_SFTP("Error al leer el archivo que se desea subir al SFTP."), 
	ERROR_UPLOAD_SFTP("Error al subir el archivo incoming al FTP de Transbank."), 
	ERROR_DOWNLOAD_FILE_SFTP("Error al intentar descargar el archivo desde el SFTP."), 
	ERROR_DOWNLOAD_FILE_NOT_FOUND_SFTP("Error no se ha encontrado archivo que se desea descargar, en la ruta especificada."),
	ERROR_CLOSE_FILE_SFTP("Error al cerrar el archivo transferido al servivod SFTP."), 
	ERROR_UPLOAD_TXT_SFTP("Estimados:\n El archivo nombrado al final no fue posible subirlo al SFTP."),
	ERROR_LIST_SFTP("Error al obtener lista de nombres de archivos del directorio SFTP.");

	/**
	 * Valor ENUM.
	 */
	private final String text;

	/**
	 * Constructor.
	 * 
	 * @param text
	 */
	private MsgErrorSFTP(final String text) {
		this.text = text;
	}

	/**
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return text;
	}
}
