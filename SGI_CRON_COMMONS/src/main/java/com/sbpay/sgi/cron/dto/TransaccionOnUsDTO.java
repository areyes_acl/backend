package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 25/01/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class TransaccionOnUsDTO implements Serializable {

	/**
   * 
   */
	private static final long serialVersionUID = 8887889677674146517L;
	private long sid;
	private String codigoTransaccion;
	private int estadoProceso;
	private String numeroTarjeta;
	private String fechaCompra;
	private String fechaAutorizacion;
	private String fechaPosteo;
	private String tipoVenta;
	private String numCuotas;
	private String numMicrofilm;
	private String numComercio;
	private String montoTransac;
	private String valorCuota;
	private String nombreComercio;
	private String ciudadComercio;
	private String rubroComercio;
	private String codAutor;

	public long getSid() {
		return sid;
	}

	public void setSid(long sid) {
		this.sid = sid;
	}

	public String getCodigoTransaccion() {
		return codigoTransaccion;
	}

	public void setCodigoTransaccion(String codigoTransaccion) {
		this.codigoTransaccion = codigoTransaccion;
	}

	public int getEstadoProceso() {
		return estadoProceso;
	}

	public void setEstadoProceso(int estadoProceso) {
		this.estadoProceso = estadoProceso;
	}

	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}

	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}

	public String getFechaCompra() {
		return fechaCompra;
	}

	public void setFechaCompra(String fechaCompra) {
		this.fechaCompra = fechaCompra;
	}

	public String getFechaAutorizacion() {
		return fechaAutorizacion;
	}

	public void setFechaAutorizacion(String fechaAutorizacion) {
		this.fechaAutorizacion = fechaAutorizacion;
	}

	public String getFechaPosteo() {
		return fechaPosteo;
	}

	public void setFechaPosteo(String fechaPosteo) {
		this.fechaPosteo = fechaPosteo;
	}

	public String getTipoVenta() {
		return tipoVenta;
	}

	public void setTipoVenta(String tipoVenta) {
		this.tipoVenta = tipoVenta;
	}

	public String getNumCuotas() {
		return numCuotas;
	}

	public void setNumCuotas(String numCuotas) {
		this.numCuotas = numCuotas;
	}

	public String getNumMicrofilm() {
		return numMicrofilm;
	}

	public void setNumMicrofilm(String numMicrofilm) {
		this.numMicrofilm = numMicrofilm;
	}

	public String getNumComercio() {
		return numComercio;
	}

	public void setNumComercio(String numComercio) {
		this.numComercio = numComercio;
	}

	public String getMontoTransac() {
		return montoTransac;
	}

	public void setMontoTransac(String montoTransac) {
		this.montoTransac = montoTransac;
	}

	public String getValorCuota() {
		return valorCuota;
	}

	public void setValorCuota(String valorCuota) {
		this.valorCuota = valorCuota;
	}

	public String getNombreComercio() {
		return nombreComercio;
	}

	public void setNombreComercio(String nombreComercio) {
		this.nombreComercio = nombreComercio;
	}

	public String getCiudadComercio() {
		return ciudadComercio;
	}

	public void setCiudadComercio(String ciudadComercio) {
		this.ciudadComercio = ciudadComercio;
	}

	public String getRubroComercio() {
		return rubroComercio;
	}

	public void setRubroComercio(String rubroComercio) {
		this.rubroComercio = rubroComercio;
	}

	public String getCodAutor() {
		return codAutor;
	}

	public void setCodAutor(String codAutor) {
		this.codAutor = codAutor;
	}

	@Override
	public String toString() {
		return "TransaccionOnUsDTO [codigoTransaccion=" + codigoTransaccion
				+ ", estadoProceso=" + estadoProceso + ", numeroTarjeta="
				+ numeroTarjeta + ", fechaCompra=" + fechaCompra
				+ ", fechaAutorizacion=" + fechaAutorizacion + ", fechaPosteo="
				+ fechaPosteo + ", tipoVenta=" + tipoVenta + ", numCuotas="
				+ numCuotas + ", numMicrofilm=" + numMicrofilm
				+ ", numComercio=" + numComercio + ", montoTransac="
				+ montoTransac + ", valorCuota=" + valorCuota
				+ ", nombreComercio=" + nombreComercio + ", ciudadComercio="
				+ ciudadComercio + ", rubroComercio=" + rubroComercio
				+ ", codAutor=" + codAutor + "]";
	}



}
