package com.sbpay.sgi.cron.dto;

import com.sbpay.sgi.cron.utils.generals.CommonsUtils;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 20/11/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class Registro05DTO extends RegistroDTO {
    /**
     * 
     */
    private static final long serialVersionUID = 2403995758607146919L;
    private String transId;
    private String authAmt;
    private String moneda;
    private String authRespCd;
    private String valCode;
    private String exclTranIdRsn;
    private String crsProcngCd;
    private String chrgbkCondCd;
    private String multClearSeqNbr;
    private String multClearSeqCnt;
    private String mktAuthDataInd;
    private String totAuthAmt;
    private String infoInd;
    private String merPhone;
    private String addtlDataInd;
    private String merchVolInd;
    private String elecGoodInd;
    private String merchVerValue;
    private String interFeeAmt;
    private String interFeeSign;
    private String sourcurrExchRate;
    private String basecurrExchRate;
    private String optIsaAmt;
    private String prodId;
    private String progId;
    private String reserved;
    private String resultCode;
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 20/11/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * Constructor de la clase
     * 
     * @since 1.X
     */
    public Registro05DTO() {
        
    }
    
    public String getTransId() {
        return transId;
    }
    
    public void setTransId( String transId ) {
        this.transId = transId;
    }
    
    public String getAuthAmt() {
        return authAmt;
    }
    
    public void setAuthAmt( String authAmt ) {
        this.authAmt = authAmt;
    }
    
    public String getMoneda() {
        return moneda;
    }
    
    public void setMoneda( String moneda ) {
        this.moneda = moneda;
    }
    
    public String getAuthRespCd() {
        return authRespCd;
    }
    
    public void setAuthRespCd( String authRespCd ) {
        this.authRespCd = authRespCd;
    }
    
    public String getValCode() {
        return valCode;
    }
    
    public void setValCode( String valCode ) {
        this.valCode = valCode;
    }
    
    public String getExclTranIdRsn() {
        return exclTranIdRsn;
    }
    
    public void setExclTranIdRsn( String exclTranIdRsn ) {
        this.exclTranIdRsn = exclTranIdRsn;
    }
    
    public String getCrsProcngCd() {
        return crsProcngCd;
    }
    
    public void setCrsProcngCd( String crsProcngCd ) {
        this.crsProcngCd = crsProcngCd;
    }
    
    public String getChrgbkCondCd() {
        return chrgbkCondCd;
    }
    
    public void setChrgbkCondCd( String chrgbkCondCd ) {
        this.chrgbkCondCd = chrgbkCondCd;
    }
    
    public String getMultClearSeqNbr() {
        return multClearSeqNbr;
    }
    
    public void setMultClearSeqNbr( String multClearSeqNbr ) {
        this.multClearSeqNbr = multClearSeqNbr;
    }
    
    public String getMultClearSeqCnt() {
        return multClearSeqCnt;
    }
    
    public void setMultClearSeqCnt( String multClearSeqCnt ) {
        this.multClearSeqCnt = multClearSeqCnt;
    }
    
    public String getMktAuthDataInd() {
        return mktAuthDataInd;
    }
    
    public void setMktAuthDataInd( String mktAuthDataInd ) {
        this.mktAuthDataInd = mktAuthDataInd;
    }
    
    public String getTotAuthAmt() {
        return totAuthAmt;
    }
    
    public void setTotAuthAmt( String totAuthAmt ) {
        this.totAuthAmt = totAuthAmt;
    }
    
    public String getInfoInd() {
        return infoInd;
    }
    
    public void setInfoInd( String infoInd ) {
        this.infoInd = infoInd;
    }
    
    public String getMerPhone() {
        return merPhone;
    }
    
    public void setMerPhone( String merPhone ) {
        this.merPhone = merPhone;
    }
    
    public String getAddtlDataInd() {
        return addtlDataInd;
    }
    
    public void setAddtlDataInd( String addtlDataInd ) {
        this.addtlDataInd = addtlDataInd;
    }
    
    public String getMerchVolInd() {
        return merchVolInd;
    }
    
    public void setMerchVolInd( String merchVolInd ) {
        this.merchVolInd = merchVolInd;
    }
    
    public String getElecGoodInd() {
        return elecGoodInd;
    }
    
    public void setElecGoodInd( String elecGoodInd ) {
        this.elecGoodInd = elecGoodInd;
    }
    
    public String getMerchVerValue() {
        return merchVerValue;
    }
    
    public void setMerchVerValue( String merchVerValue ) {
        this.merchVerValue = merchVerValue;
    }
    
    public String getInterFeeAmt() {
        return interFeeAmt;
    }
    
    public void setInterFeeAmt( String interFeeAmt ) {
        this.interFeeAmt = interFeeAmt;
    }
    
    public String getInterFeeSign() {
        return interFeeSign;
    }
    
    public void setInterFeeSign( String interFeeSign ) {
        this.interFeeSign = interFeeSign;
    }
    
    public String getSourcurrExchRate() {
        return sourcurrExchRate;
    }
    
    public void setSourcurrExchRate( String sourcurrExchRate ) {
        this.sourcurrExchRate = sourcurrExchRate;
    }
    
    public String getBasecurrExchRate() {
        return basecurrExchRate;
    }
    
    public void setBasecurrExchRate( String basecurrExchRate ) {
        this.basecurrExchRate = basecurrExchRate;
    }
    
    public String getOptIsaAmt() {
        return optIsaAmt;
    }
    
    public void setOptIsaAmt( String optIsaAmt ) {
        this.optIsaAmt = optIsaAmt;
    }
    
    public String getProdId() {
        return prodId;
    }
    
    public void setProdId( String prodId ) {
        this.prodId = prodId;
    }
    
    public String getProgId() {
        return progId;
    }
    
    public void setProgId( String progId ) {
        this.progId = progId;
    }
    
    public String getReserved() {
        return reserved;
    }
    
    public void setReserved( String reserved ) {
        this.reserved = reserved;
    }
    
    public String getResultCode() {
        return resultCode;
    }
    
    public void setResultCode( String resultCode ) {
        this.resultCode = resultCode;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 23/11/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * Obtener todo el registro 05
     * 
     * @return
     * @since 1.X
     */
    public String getAll05Register() {
        StringBuffer str = new StringBuffer();
        
        str.append( super.getTipoTransaccion() );
        str.append( CommonsUtils.getWhitesSpaceString( 2 ) );
        str.append( super.getTransCode() );
        str.append( this.transId );
        str.append( this.authAmt );
        str.append( this.moneda );
        str.append( this.authRespCd );
        str.append( this.valCode );
        str.append( this.exclTranIdRsn );
        str.append( this.crsProcngCd );
        str.append( this.chrgbkCondCd );
        str.append( this.multClearSeqNbr );
        str.append( this.multClearSeqCnt );
        str.append( this.mktAuthDataInd );
        str.append( this.totAuthAmt );
        str.append( this.infoInd );
        str.append( this.merPhone );
        str.append( this.addtlDataInd );
        str.append( this.merchVolInd );
        str.append( this.elecGoodInd );
        str.append( this.merchVerValue );
        str.append( this.interFeeAmt );
        str.append( this.interFeeSign );
        str.append( this.sourcurrExchRate );
        str.append( this.basecurrExchRate );
        str.append( this.optIsaAmt );
        str.append( this.prodId );
        str.append( this.progId );
        str.append( this.reserved );
        str.append( this.resultCode );

        return str.toString();
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 9/12/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @param reg
     * @since 1.X
     */
    public void setAll05Register( String reg ) {
        super.setTipoTransaccion( reg.substring( 0, 2 ) );
        super.setTransCode( reg.substring( 4, 6 ) );
        this.transId = reg.substring( 6, 21 );
        this.authAmt = reg.substring( 21, 33 );
        this.moneda = reg.substring( 33, 36 );
        this.authRespCd = reg.substring( 36, 38 );
        this.valCode = reg.substring( 38, 42 );
        this.exclTranIdRsn = reg.substring( 42, 43 );
        this.crsProcngCd = reg.substring( 43, 44 );
        this.chrgbkCondCd = reg.substring( 44, 46 );
        this.multClearSeqNbr = reg.substring( 46, 48 );
        this.multClearSeqCnt = reg.substring( 48, 50 );
        this.mktAuthDataInd = reg.substring( 50, 51 );
        this.totAuthAmt = reg.substring( 51, 63 );
        this.infoInd = reg.substring( 63, 64 );
        this.merPhone = reg.substring( 64, 78 );
        this.addtlDataInd = reg.substring( 78, 79 );
        this.merchVolInd = reg.substring( 79, 81 );
        this.elecGoodInd = reg.substring( 81, 83 );
        this.merchVerValue = reg.substring( 83, 93 );
        this.interFeeAmt = reg.substring( 93, 108 );
        this.interFeeSign = reg.substring( 108, 109 );
        this.sourcurrExchRate = reg.substring( 109, 117 );
        this.basecurrExchRate = reg.substring( 117, 125 );
        this.optIsaAmt = reg.substring( 125, 137 );
        this.prodId = reg.substring( 137, 139 );
        this.progId = reg.substring( 139, 145 );
        this.reserved = reg.substring( 145, 169 );
        this.resultCode = reg.substring( 169, 170 );
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 9/12/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @see com.sbpay.sgi.cron.dto.RegistroDTO#toString()
     * @since 1.X
     */
    @Override
    public String toString() {
        return "Registro05DTO [TipoTransaccion = " + super.getTipoTransaccion()
                + ", transcode = " + super.getTransCode() + ", transId="
                + transId + ", authAmt=" + authAmt + ", moneda=" + moneda
                + ", authRespCd=" + authRespCd + ", valCode=" + valCode
                + ", exclTranIdRsn=" + exclTranIdRsn + ", crsProcngCd="
                + crsProcngCd + ", chrgbkCondCd=" + chrgbkCondCd
                + ", multClearSeqNbr=" + multClearSeqNbr + ", multClearSeqCnt="
                + multClearSeqCnt + ", mktAuthDataInd=" + mktAuthDataInd
                + ", totAuthAmt=" + totAuthAmt + ", infoInd=" + infoInd
                + ", merPhone=" + merPhone + ", addtlDataInd=" + addtlDataInd
                + ", merchVolInd=" + merchVolInd + ", elecGoodInd="
                + elecGoodInd + ", merchVerValue=" + merchVerValue
                + ", interFeeAmt=" + interFeeAmt + ", interFeeSign="
                + interFeeSign + ", sourcurrExchRate=" + sourcurrExchRate
                + ", basecurrExchRate=" + basecurrExchRate + ", optIsaAmt="
                + optIsaAmt + ", prodId=" + prodId + ", progId=" + progId
                + ", reserved=" + reserved + ", resultCode=" + resultCode + "]";
    }
    
}
