package com.sbpay.sgi.cron.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

import org.apache.log4j.Logger;


/**
 * <p>
 * Clase Exception Conexion a la Base de Datos.
 * <p>
 * <br/>
 * @param <T> the type of the value being BaseDao
 * @author Dante Medina (ACL) en nombre de sbpay
 * @version 1.6
 * @since 2015-11-09
 */
public abstract class BaseDao<T> implements Dao {

    /**
     * Clase para imprimir en el log.
     */
	private static final Logger LOG = Logger.getLogger(BaseDao.class
			.getName());
    /**
     * Conexion del DAO.
     */
    private Connection connection;

    /**
     * @return the connection
     */
    public Connection getConnection() {
        return connection;
    }

    /**
     * Contructor con paramtros.
     * @param connection Conexion activa.
     */
    public BaseDao(Connection connection) {
        this.connection = connection;
    }

    /**
     * Commit TRX.
     * @throws SQLException Error SQL.
     */
    @Override
    public void endTx() throws SQLException {
        this.connection.commit();
    }

    /**
     * rollback TRX.
     * @throws SQLException Error SQL.
     */
    @Override
    public void rollBack() throws SQLException {
        this.connection.rollback();
    }

    /**
     * close connection.
     * @throws SQLException Error SQL.
     */
    @Override
    public void close() throws SQLException {
        this.connection.close();
    }

    /**
     * Valida si un string puede ser convertivo a Long.
     * @param obj Objeto a validar.
     * @return True si es posible convertirlo en Long. False si no es posible
     *         covertirlo en Long.
     */
    protected boolean isLong(final String obj) {
        try {
            Long.valueOf(obj);
            return true;
        } catch (Exception ex) {
            LOG.debug("ERROR AL PARSEAR UN LONG -" + ex.getMessage(), ex);
            return false;
        }
    }

    /**
     * Valida si un string puede ser convertivo a Int.
     * @param obj Objeto a validar.
     * @return True si es posible convertirlo en Int. False si no es posible
     *         covertirlo en Int.
     */
    protected boolean isInt(final String obj) {
        try {
            Integer.valueOf(obj);
            return true;
        } catch (Exception ex) {
            LOG.debug("ERROR AL PARSEAR UN INT -" + ex.getMessage(), ex);
            return false;
        }
    }

    /**
     * Setea un String en una consulta.
     * @param value Valor.
     * @param index Indice del parametro.
     * @param stmt Consulta.
     * @throws SQLException Error SQL.
     */
    protected void setStringNull(final String value, final int index,
            final PreparedStatement stmt) throws SQLException {
        if (value == null) {
            stmt.setNull(index, Types.NULL);
        } else {
            stmt.setString(index, value);
        }
    }

    /**
     * Setea un Long en una consulta.
     * @param value Valor.
     * @param index Indice del parametro.
     * @param stmt Consulta.
     * @throws SQLException Error SQL.
     */
    protected void setLongNull(final String value, final int index,
            final PreparedStatement stmt) throws SQLException {
        if (isLong(value)) {
            stmt.setLong(index, Long.valueOf(value));
        } else {
            stmt.setNull(index, Types.NUMERIC);
        }
    }

    /**
     * Setea un Int en una consulta.
     * @param value Valor.
     * @param index Indice del parametro.
     * @param stmt Consulta.
     * @throws SQLException Error SQL.
     */
    protected void setIntNull(final String value, final int index,
            final PreparedStatement stmt) throws SQLException {
        if (isLong(value)) {
            stmt.setInt(index, Integer.valueOf(value));
        } else {
            stmt.setNull(index, Types.NUMERIC);
        }
    }

	
	
}