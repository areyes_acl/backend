package com.sbpay.sgi.cron.enums;

public enum MsgErrorProcessFileOnus {
	ERROR_ONUS_NAME_NOT_FOUND("Error al consultar por el arhivo de ONUS, no se encuentra registro en la tabla TBL_LOG_ONUS."), 
	ERROR_INVALID_ONUS_STATE("Archivo no es valido para ser procesado, su codigo file_flag en la tabla TBL_LOG_ONUS es distinto de 0"), 
	ERROR_CLOSE_FILE("Error al cerrar el archivo transferido de ONUS en sbpay"),
	ERROR_IN_PROCESS("Error al Procesar ONUS"), 
	WARNING_NO_FILE_PROCESSING_TITLE("[ADVERTENCIA]: Ningun archivo procesado"),
	WARNING_NO_FILES_FOUNDS(" No se ha procesado ningun archivo debido a que no se existe ningun archivo en el directorio para ser procesado"),
	WARNING_NO_FILE_PROCESSING_CAUSE(" No se ha procesado ningun archivo ONUS.");

	/**
	 * Valor ENUM.
	 */
	private final String text;

	/**
	 * Constructor.
	 * 
	 * @param text
	 */
	private MsgErrorProcessFileOnus(final String text) {
		this.text = text;
	}

	/**
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return text;
	}
}
