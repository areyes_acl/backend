package com.sbpay.sgi.cron.utils.file;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.dto.CompensacionPagoDTO;

public class CompscnPagoReader implements CompscnPagoFIleUtil{

    /**
     * Variable de log4j
     */
    private static final Logger LOGGER = Logger.getLogger( CompscnPagoReader.class );
    
    /**
     * Metodo que lee el archivo de compensaciones de Pago.
     * Este metodo lee el archivo por linea y busca las expersiones regulares que se le indican
     * para los tipos de Outgoing y  Incoming.
     * @throws Exception 
     */
    @Override
    public List<CompensacionPagoDTO> readCompscnPagoFile(File file) throws Exception {
        // TODO Auto-generated method stub
        List<CompensacionPagoDTO> listaCompensacionPagoDTO = new ArrayList<CompensacionPagoDTO>();
        Scanner sc = null;
        try{
            String outgoingFormat = "^TOTAL FECHA";
            String incomingFormat = "^CONTRACARGO VENTA";
            String fila;
            String[] columnas;
            sc = new Scanner(file);
            Pattern patronOut = Pattern.compile(outgoingFormat);
            Pattern patronInc = Pattern.compile(incomingFormat);
            CompensacionPagoDTO compensacionPagoDTO;
            Matcher matcherOut;
            Matcher matcherInc;
            while (sc.hasNext())    {
                compensacionPagoDTO = null;
                fila = sc.nextLine().replaceAll("\\.","").replaceAll("\\,",".");
                            matcherOut = patronOut.matcher(fila);
                            matcherInc = patronInc.matcher(fila);
                
                    while(matcherOut.find()) {
                        columnas = fila.split(" +");
                        compensacionPagoDTO = new CompensacionPagoDTO();
                        if(Integer.parseInt(columnas[2])!=0) compensacionPagoDTO.setFecPago(columnas[2].substring(0,2) + "/" + 
                                                             columnas[2].substring(2,4) + "/" + columnas[2].substring(4,6));
                        compensacionPagoDTO.setCantidad(Integer.parseInt(columnas[3]));
                        compensacionPagoDTO.setMontoPago(Double.parseDouble(columnas[4]));
                        compensacionPagoDTO.setNeta(Double.parseDouble(columnas[5]));
                        compensacionPagoDTO.setIva(Double.parseDouble(columnas[6]));
                        compensacionPagoDTO.setTotal(Double.parseDouble(columnas[7]));
                        compensacionPagoDTO.setNeto(Double.parseDouble(columnas[8]));
                        compensacionPagoDTO.setTipoTransaccion(1);
                    }
                    while(matcherInc.find()) {
                        columnas = fila.split(" +");
                        String fechaExtraida = columnas[2].substring(3,9);
                        compensacionPagoDTO = new CompensacionPagoDTO();
                        if(Integer.parseInt(columnas[3])!=0)
                        {
                                compensacionPagoDTO.setFecPago(fechaExtraida.substring(0,2) + "/" + fechaExtraida.substring(2,4) + "/" + fechaExtraida.substring(4,6));
                            compensacionPagoDTO.setCantidad(Integer.parseInt(columnas[3]));
                            compensacionPagoDTO.setMontoPago(Double.parseDouble(columnas[4]));
                            compensacionPagoDTO.setTipoTransaccion(-1);
                            compensacionPagoDTO.setNeta(Double.parseDouble("0"));
                            compensacionPagoDTO.setIva(Double.parseDouble("0"));
                            compensacionPagoDTO.setTotal(Double.parseDouble("0"));
                            compensacionPagoDTO.setNeto(Double.parseDouble("0"));
                        }
                        else compensacionPagoDTO = null;

                    }
                    
                    if(compensacionPagoDTO!=null){
                        listaCompensacionPagoDTO.add(compensacionPagoDTO);
                    }
            }
            return listaCompensacionPagoDTO;
        }catch(Exception ex){
            LOGGER.error("Ha ocurrido un error durante la lectura del archivo de compensacion de pago ", ex);
            throw new Exception(ex);
        }finally{
            if(sc != null){
                sc.close();
            }
        }
    }
}