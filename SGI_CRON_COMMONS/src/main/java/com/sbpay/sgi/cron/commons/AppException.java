package com.sbpay.sgi.cron.commons;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * <p>
 * Clase Exception Proyecto ABC-DIN.
 * <p>
 * <br/>
 * 
 * @author Dante Medina (ACL) en nombre de ABC-DIN
 * @version 1.6
 * @date 03-11-2015
 */
public class AppException extends Exception {
    
    /**
     * serial.
     */
    private static final long serialVersionUID = 1L;
    private String stackTraceDescripcion;
    
    /**
     * Constructor.
     * 
     * @param message
     *            .
     */
    public AppException( final String message ) {
        super( message );
    }
    
    public String getStackTraceDescripcion() {
        return stackTraceDescripcion;
    }
    
    public void setStackTraceDescripcion( String stackTraceDescripcion ) {
        this.stackTraceDescripcion = stackTraceDescripcion;
    }
    
    /**
     * Contructor.
     * 
     * @param message
     *            .
     * @param cause
     *            .
     */
    public AppException( final String message, final Throwable cause ) {
        super( message, cause );
        this.stackTraceDescripcion = getStackTrace( cause );
        
    }
    
    /**
     * Contructor.
     * 
     * @param message
     *            .
     * @param cause
     *            .
     */
    public AppException(final Throwable cause ) {
        super(cause );
        this.stackTraceDescripcion = getStackTrace( cause );
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 3/12/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * Metodo que dada una excepcion retorna su traza completa
     * 
     * @param t
     * @return
     * @since 1.X
     */
    public static String getStackTrace( Throwable t ) {
        StringWriter sw = new StringWriter();
        t.printStackTrace( new PrintWriter( sw ) );
        return sw.toString();
    }
}
