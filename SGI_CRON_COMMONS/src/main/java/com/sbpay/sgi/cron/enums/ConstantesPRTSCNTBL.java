package com.sbpay.sgi.cron.enums;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 27/11/2015, (ACL-sbpay) -  versión inicial 
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public enum ConstantesPRTSCNTBL {
    // NOMBRE DEL GRUPO
    CRON_TBK("CRON_TBK"), 
    // VARIABLES TABLA PRTS
    FTP_IP_CNTBL("FTP_IP_CNTBL"), 
    FTP_PORT_CNTBL("FTP_PORT_CNTBL"), 
    FTP_USER_CNTBL("FTP_USER_CNTBL"), 
    FTP_PASS_CNTBL("FTP_PASS_CNTBL"), 
    FORMAT_EXTNAME_CNTBL("FORMAT_EXTNAME_CNTBL"), 
    FORMAT_EXTNAME_CNTBL_CTR("FORMAT_EXTNAME_CNTBL_CTR"),
	FORMAT_FILENAME_OUT("FORMAT_FILENAME_OUT")
	;
	
	

	/**
	 * Valor ENUM.
	 */
	private final String text;

	/**
	 * Constructor.
	 * 
	 * @param text
	 */
	private ConstantesPRTSCNTBL(final String text) {
		this.text = text;
	}

	/**
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return text;
	}
}
