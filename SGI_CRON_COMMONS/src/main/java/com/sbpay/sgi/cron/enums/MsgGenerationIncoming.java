package com.sbpay.sgi.cron.enums;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 11/12/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public enum MsgGenerationIncoming {
    WARNING_TRANSACTIONS_NOT_FOUND("ADVERTENCIA: No se ha encontrado ninguna transaccion en la tabla TBL_OUTGOING_TRANSBANK, para crear el archivo, se generará un archivo en blanco."),
    WARNING_CALL_SP_NO_OK("Advertencia: la llamada al proceso SP_SIG_CARGAOUTGOING no a finalizado correctamente"),
    ERROR_INCOMING_ALREADY_EXIST("Error: Archivo incoming Transbank ya existe en el directorio"),
    ERROR_INCOMING_ALREADY_EXIST_VISA_NAC("Error: Archivo incoming Visa Nacional ya existe en el directorio"),
    ERROR_INCOMING_ALREADY_EXIST_VISA_INT("Error: Archivo incoming Visa Internacional ya existe en el directorio"),
    ERROR_CREATE_INCOMING_FILE("Error: Se ha producido un error al generar el archivo incoming"),
    ERROR_CALL_SP_OUT_TBK_ROLLBACK("Ha ocurrido un error al realizar el rollback"),
    ERROR_CALL_SP_OUT_TRBK("Ha ocurrido un error al llamar al SP: SP_SIG_CARGAOUTGOING_TBK"),
    ERROR_CALL_SP_OUT_VISA("Ha ocurrido un error al llamar al SP: SP_SIG_CARGAOUTGOING_VISA"),
    ERROR_CALL_SP_OUT_TBK_CLOSE_CONECTION("Ha ocurrido un error al cerrar la conexion"),
    SUCCESFUL_GENERATE_INCOMING_TITTLE("Se ha generado Incoming Transbank correctamente"),
    SUCCESFUL_GENERATE_INCOMING_MSG(" El proceso de generacion de Incoming Transbank ha finalizado exitosamente."), 
    ERROR_SQL_OUT_VISA("Ha ocurrido un error en la base de datos al intentar generar el Incoming para VISA Internacional")
    ;
    
    /**
     * Valor ENUM.
     */
    private final String text;
    
    /**
     * Constructor.
     * 
     * @param text
     */
    private MsgGenerationIncoming( final String text ) {
        this.text = text;
    }
    
    /**
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
}
