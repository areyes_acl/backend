package com.sbpay.sgi.cron.utils.file;

import java.io.File;
import java.util.List;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dto.IncomingVisaNacDTO;
import com.sbpay.sgi.cron.dto.TransaccionOutVisaNac;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 17/04/2019, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * Interfaz que declara los metodos para leer los diversos arvchivos
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public interface IncomingVisaNacFileUtils {

  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 17/04/2019, (ACL-sbpay) - versión inicial
   * </ul>
   * <p>
   * Metodo que lee un archivo incoming visa nacional desde una ruta especificada y lo transforma a DTO. Se le
   * pasa el starWith para realizar la validacion de la fecha.
   * 
   * @param ruta
   * @param starWith
   * @return
   * @throws AppException
   * @since 1.X
   */
  public IncomingVisaNacDTO readIncomingVisaNacFile(String ruta, String starWith) throws AppException;

  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 17/04/2019, (ACL-sbpay) - versión inicial
   * </ul>
   * <p>
   * Metodo que lee un archivo incoming visa nacional desde un file y lo transforma a DTO. Se le pasa el starWith
   * para realizar la validacion de la fecha del archivo
   * 
   * @param file
   * @param starWith
   * @return
   * @throws AppException
   * @since 1.X
   */
  public IncomingVisaNacDTO readIncomingVisaNacFile(File file, String starWith) throws AppException;


}
