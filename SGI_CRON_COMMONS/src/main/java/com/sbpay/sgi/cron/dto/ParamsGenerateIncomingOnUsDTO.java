package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 28/01/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * Clase que guarda los parametros necesarios para generar el incoming On Us
 * desde la tabla TBL_PRTS
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class ParamsGenerateIncomingOnUsDTO implements Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = 7782219696904883769L;
	private String formatFilenameIncOnUs;
	private String formatExtNameIncOnUs;
	private String formatExtNameIncCrtOnUs;
	private String pathAbcOnUsOut;

	public String getFormatFilenameIncOnUs() {
		return formatFilenameIncOnUs;
	}

	public void setFormatFilenameIncOnUs(final String formatFilenameIncOnUs) {
		this.formatFilenameIncOnUs = formatFilenameIncOnUs;
	}

	public String getFormatExtNameIncOnUs() {
		return formatExtNameIncOnUs;
	}

	public void setFormatExtNameIncOnUs(final String formatExtNameIncOnUs) {
		this.formatExtNameIncOnUs = formatExtNameIncOnUs;
	}

	public String getFormatExtNameIncCrtOnUs() {
		return formatExtNameIncCrtOnUs;
	}

	public void setFormatExtNameIncCrtOnUs(final String formatExtNameIncCrtOnUs) {
		this.formatExtNameIncCrtOnUs = formatExtNameIncCrtOnUs;
	}
	
	public String getPathAbcOnUsOut() {
		return pathAbcOnUsOut;
	}

	public void setPathAbcOnUsOut(String pathAbcOnUsOut) {
		this.pathAbcOnUsOut = pathAbcOnUsOut;
	}

	@Override
	public String toString() {
		return "ParamsGenerateIncomingOnUsDTO [formatFilenameIncOnUs="
				+ formatFilenameIncOnUs + ", formatExtNameIncOnUs="
				+ formatExtNameIncOnUs + ", formatExtNameIncCrtOnUs="
				+ formatExtNameIncCrtOnUs + ", pathAbcOnUsOut="
				+ pathAbcOnUsOut + "]";
	}

}
