package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

public class ParamUploadOutgoingICDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

  public ParamUploadOutgoingICDTO() {}


	//PARAMETROS PATH OUTGOING 
    private String formatFileNameOut;
    private String pathAclInc;
    private String formatExtNameOutCtr;
    private String formatExtNameOut;
	private String pathFtpIcEntrada;
  private DataFTP dataFTP;



	public String getFormatFileNameOut() {
		return formatFileNameOut;
	}

	public void setFormatFileNameOut(String formatFileNameOut) {
		this.formatFileNameOut = formatFileNameOut;
	}

  public DataFTP getDataFTP() {
    return dataFTP;
  }

  public void setDataFTP(DataFTP dataFTP) {
    this.dataFTP = dataFTP;
  }

	public String getPathAclInc() {
		return pathAclInc;
	}

	public void setPathAclInc(String pathAclInc) {
		this.pathAclInc = pathAclInc;
	}

	public String getFormatExtNameOutCtr() {
		return formatExtNameOutCtr;
	}

	public void setFormatExtNameOutCtr(String formatExtNameOutCtr) {
		this.formatExtNameOutCtr = formatExtNameOutCtr;
	}

	public String getFormatExtNameOut() {
		return formatExtNameOut;
	}

	public void setFormatExtNameOut(String formatExtNameOut) {
		this.formatExtNameOut = formatExtNameOut;
	}

	public String getPathFtpIcEntrada() {
		return pathFtpIcEntrada;
	}

	public void setPathFtpIcEntrada(String pathFtpIcEntrada) {
		this.pathFtpIcEntrada = pathFtpIcEntrada;
	}

	@Override
    public String toString() {
    return "ParamUploadOutgoingICDTO [formatFileNameOut=" + formatFileNameOut + ", pathAclInc="
        + pathAclInc + ", formatExtNameOutCtr=" + formatExtNameOutCtr + ", formatExtNameOut="
        + formatExtNameOut + ", pathFtpIcEntrada=" + pathFtpIcEntrada + ", dataFTP=" + dataFTP
        + "]";
    }

	
}
