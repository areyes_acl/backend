package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

public class TmpExportItzAvanceDTO implements Serializable{
	
	private static final long serialVersionUID = 4256532444788242634L;
	private String fechaTransferencia;
	private String codAutorizacion;
	private String numeroTrx;
	private String numeroTarjeta;
	private String monto;
	private String estado;
	private String numeroCanal;
	private String montoSeguro;
	
	
	public String getFechaTransferencia() {
		return fechaTransferencia;
	}
	public void setFechaTransferencia(String fechaTransferencia) {
		this.fechaTransferencia = fechaTransferencia;
	}
	public String getCodAutorizacion() {
		return codAutorizacion;
	}
	public void setCodAutorizacion(String codAutorizacion) {
		this.codAutorizacion = codAutorizacion;
	}
	public String getNumeroTrx() {
		return numeroTrx;
	}
	public void setNumeroTrx(String numeroTrx) {
		this.numeroTrx = numeroTrx;
	}
	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}
	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getNumeroCanal() {
		return numeroCanal;
	}
	public void setNumeroCanal(String numeroCanal) {
		this.numeroCanal = numeroCanal;
	}
	public String getMontoSeguro() {
		return montoSeguro;
	}
	public void setMontoSeguro(String montoSeguro) {
		this.montoSeguro = montoSeguro;
	}
	
	
	
}
