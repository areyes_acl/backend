package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 18/12/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class ParamDownloadLogAutorizacionDTO implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = 4081693614785403716L;
    private String formatFileAbcLog;
    private String formatExtNameAbcLog;
    private String pathAbcLogTrx;
    private String pathFtpIcSalida;
    private DataFTP dataFTP;
    
    public String getFormatFileAbcLog() {
        return formatFileAbcLog;
    }
    
    public void setFormatFileAbcLog( final String formatFileAbcLog ) {
        this.formatFileAbcLog = formatFileAbcLog;
    }
    
    public String getFormatExtNameAbcLog() {
        return formatExtNameAbcLog;
    }
    
    public void setFormatExtNameAbcLog( final String formatExtNameAbcLog ) {
        this.formatExtNameAbcLog = formatExtNameAbcLog;
    }
    
    public String getPathAbcLogTrx() {
        return pathAbcLogTrx;
    }
    
    public void setPathAbcLogTrx( final String pathAbcLogTrx ) {
        this.pathAbcLogTrx = pathAbcLogTrx;
    }
    
    public String getPathFtpIcSalida() {
        return pathFtpIcSalida;
    }
    
    public void setPathFtpIcSalida( String pathFtpIcSalida ) {
        this.pathFtpIcSalida = pathFtpIcSalida;
    }
    
    public DataFTP getDataFTP() {
        return dataFTP;
    }
    
    public void setDataFTP( final DataFTP dataFTP ) {
        this.dataFTP = dataFTP;
    }
    
    @Override
    public String toString() {
        return "ParamDownloadLogAutorizacionDTO [formatFileAbcLog="
                + formatFileAbcLog + ", formatExtNameAbcLog="
                + formatExtNameAbcLog + ", pathAbcLogTrx=" + pathAbcLogTrx
                + ", pathFtpIcSalida=" + pathFtpIcSalida + ", dataFTP="
                + dataFTP + "]";
    }
    
}
