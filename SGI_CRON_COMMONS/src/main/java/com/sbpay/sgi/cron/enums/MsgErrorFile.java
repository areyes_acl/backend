package com.sbpay.sgi.cron.enums;

public enum MsgErrorFile {
	ERROR_WRITE("Error al escribir y copiar el archivo de carga Incoming en sbpay"), 
	ERROR_WRITE_CLOSE("Error al cerrar el archivo de carga Incoming en sbpay"), 
	ERROR_COPY_FILE("Error al copiar el archivo incoming de Transbank en el Server"), 
	ERROR_CLOSE_FILE("Error al cerrar el archivo transferido de carga Incoming en sbpay"), 
	ERROR_READ_DIR("Error al leer el directorio "), 
	ERROR_FILE_FORMAT("Error en el formato del nombre del archivo Incoming"), 
	ERROR_FILE_FORMAT_CNTBL("Error en el formato del nombre del archivo de Asientos Contables"), 
	ERROR_FILE_FORMAT_TXT("Estimados:\n El archivo nombrado al final no cumple con el formato "), 
	ERROR_READ_UPLOAD("Error al leer el archivo incoming del proceso Upload Trannsbank"), 
	ERROR_READ_CLOSE_UPLOAD("Error al cerrar archivo incoming del proceso Upload Trannsbank"),
	ERROR_READ_FILE_IS_NULL("Ha ocurrido un error al intentar leer el Incoming, el file es nulo" ),
	ERROR_FILE_FORMAT_CAR_ABO("Error en el formato del nombre del archivo cargos y abonos"),
	ERROR_FILE_FORMAT_TXT_CAR_ABO("Estimados:\n El archivo nombrado al final no cumple con el formato "),
	ERROR_READ_FILE_NULL("Ha ocurrido un error al intentar leer archivo " )
	;

	/**
	 * Valor ENUM.
	 */
	private final String text;

	/**
	 * Constructor.
	 * 
	 * @param text
	 */
	private MsgErrorFile(final String text) {
		this.text = text;
	}

	/**
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return text;
	}
}
