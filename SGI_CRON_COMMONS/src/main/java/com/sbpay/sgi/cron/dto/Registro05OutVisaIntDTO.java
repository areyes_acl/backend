package com.sbpay.sgi.cron.dto;


/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 25/04/2019, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class Registro05OutVisaIntDTO extends RegistroOutVisaIntDTO {
 
	/**
     * 
     */
    private static final long serialVersionUID = 2403995758607146919L;
    private String all;
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 25/04/2019, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * Constructor de la clase
     * 
     * @since 1.X
     */
    public Registro05OutVisaIntDTO() {
        
    }

	public String getAll() {
		return all;
	}



	public void setAll(String all) {
		this.all = all;
	}



	@Override
	public String toString() {
		return "Registro05OutVisaIntDTO [all=" + all +"]";
	}
    
}
