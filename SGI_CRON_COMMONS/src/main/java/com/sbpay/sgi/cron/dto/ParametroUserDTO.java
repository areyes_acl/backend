package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 16/11/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * Clase encargada de mapear la tabla TBL_PRTS_USERS
 * 
 * </p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class ParametroUserDTO implements Serializable {
    
    /**
	 * 
	 */
    private static final long serialVersionUID = -739623701274876721L;
    private long sid;
    private String codGrupoDato;
    private String codDato;
    private String valor;
    private String descripcion;
    private String idUser;
    private String fechaCreacion;
    private String fechaModificacion;
    
    public long getSid() {
        return sid;
    }
    
    public void setSid( long sid ) {
        this.sid = sid;
    }
    
    public String getCodGrupoDato() {
        return codGrupoDato;
    }
    
    public void setCodGrupoDato( String codGrupoDato ) {
        this.codGrupoDato = codGrupoDato;
    }
    
    public String getValor() {
        return valor;
    }
    
    public void setValor( String valor ) {
        this.valor = valor;
    }
    
    public String getDescripcion() {
        return descripcion;
    }
    
    public void setDescripcion( String descripcion ) {
        this.descripcion = descripcion;
    }
    
    public String getFechaCreacion() {
        return fechaCreacion;
    }
    
    public void setFechaCreacion( String fechaCreacion ) {
        this.fechaCreacion = fechaCreacion;
    }
    
    public String getFechaModificacion() {
        return fechaModificacion;
    }
    
    public void setFechaModificacion( String fechaModificacion ) {
        this.fechaModificacion = fechaModificacion;
    }
    
    public String getCodDato() {
		return codDato;
	}

	public void setCodDato(String codDato) {
		this.codDato = codDato;
	}

	public String getIdUser() {
		return idUser;
	}

	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}

	@Override
    public String toString() {
        return "ParametroUser [sid=" + sid + ", codGrupoDato=" + codGrupoDato
                + ", valor=" + valor + ", descripcion=" + descripcion
                + ", fechaCreacion=" + fechaCreacion + ", fechaModificacion="
                + fechaModificacion + "]";
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ( int ) ( sid ^ ( sid >>> 32 ) );
        return result;
    }
    
    @Override
    public boolean equals( Object obj ) {
        if ( this == obj )
            return true;
        if ( obj == null )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        ParametroUserDTO other = ( ParametroUserDTO ) obj;
        if ( sid != other.sid )
            return false;
        return true;
    }
    
}
