package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 18/12/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class ParamDownloadBOLDTO implements Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = 4081693614785403716L;
	private String formatFileNameBol;
	private String formatExtNameBol;
	private String formatExtNameBolCtr;
	private String pathBol;
	private String pathServerFTP;
	private DataFTP dataFTP;

	public String getFormatFileNameBol() {
		return formatFileNameBol;
	}

	public void setFormatFileNameBol(String formatFileNameBol) {
		this.formatFileNameBol = formatFileNameBol;
	}

	public String getFormatExtNameBol() {
		return formatExtNameBol;
	}

	public void setFormatExtNameBol(String formatExtNameBol) {
		this.formatExtNameBol = formatExtNameBol;
	}

	public String getFormatExtNameBolCtr() {
		return formatExtNameBolCtr;
	}

	public void setFormatExtNameBolCtr(String formatExtNameBolCtr) {
		this.formatExtNameBolCtr = formatExtNameBolCtr;
	}

	public String getPathBol() {
		return pathBol;
	}

	public void setPathBol(String pathBol) {
		this.pathBol = pathBol;
	}

	public DataFTP getDataFTP() {
		return dataFTP;
	}

	public void setDataFTP(DataFTP dataFTP) {
		this.dataFTP = dataFTP;
	}

	public String getPathServerFTP() {
		return pathServerFTP;
	}

	public void setPathServerFTP(String pathServerFTP) {
		this.pathServerFTP = pathServerFTP;
	}

	@Override
	public String toString() {
		return "ParamDownloadBOLDTO [formatFileNameBol=" + formatFileNameBol
				+ ", formatExtNameBol=" + formatExtNameBol
				+ ", formatExtNameBolCtr=" + formatExtNameBolCtr + ", pathBol="
				+ pathBol + ", pathServerFTP=" + pathServerFTP + ", dataFTP="
				+ dataFTP + "]";
	}

}
