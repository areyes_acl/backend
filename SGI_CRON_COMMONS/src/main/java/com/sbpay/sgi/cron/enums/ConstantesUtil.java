package com.sbpay.sgi.cron.enums;

public enum ConstantesUtil {
	SLASH("/"),
	BACK_SLASH("\\"),
	FORMAT_DDMMYY("dd/MM/yy"),
	FORMAT_AAAAMMDD("yyyyMMdd"),
	EMPTY(""),
	POINT("."),
	ZERO("0"),
	ONE("1"),
	SKIP_LINE("\n"),
	UNDERSCORE("_"),
	WHITE(" "),
	PUNTO_COMA(";"), 
	ASTERISK("*"),
	DEFAULT_PORT("22"),
	PATTERN_IS_NUMBER("^-?\\d+$");
	;
	/**
	 * Valor ENUM.
	 */
	private final String text;

	/**
	 * Constructor.
	 * 
	 * @param text
	 */
	private ConstantesUtil(final String text) {
		this.text = text;
	}

	/**
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return text;
	}
}
