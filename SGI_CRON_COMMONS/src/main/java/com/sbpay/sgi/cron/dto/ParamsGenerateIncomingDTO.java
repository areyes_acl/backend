package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

import com.sbpay.sgi.cron.enums.ConstantesUtil;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 7/12/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * Clase que guarda los parametros necesarios para generar el incoming desde la tabla TBL_PRTS
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class ParamsGenerateIncomingDTO implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = 7782219696904883769L;
  private String pathABCIncoming;
    private String formatFilenameInc;
    private String formatExtNameInc;
    private String formatExtNameIncCrt;
    


  public String getPathABCIncoming() {
    return pathABCIncoming;
    }
    
  public void setPathABCIncoming(String pathABCIncoming) {
    this.pathABCIncoming = pathABCIncoming;
    }
    
    public String getFormatFilenameInc() {
        return formatFilenameInc;
    }
    
    public void setFormatFilenameInc( String formatFilenameInc ) {
        this.formatFilenameInc = formatFilenameInc;
    }
    
    public String getFormatExtNameInc() {
        return formatExtNameInc;
    }
    
    public void setFormatExtNameInc( String formatExtNameInc ) {
        this.formatExtNameInc = formatExtNameInc;
    }
    
    public String getFormatExtNameIncCrt() {
        return formatExtNameIncCrt;
    }
    
    public void setFormatExtNameIncCrt( String formatExtNameIncCrt ) {
        this.formatExtNameIncCrt = formatExtNameIncCrt;
    }
    

  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
   * </ul>
   * <p>
   * 
   * Metodo que retorna chequea que el objeto tenga todos los parametros
   * 
   * @return
   * @since 1.X
   */
  public Boolean hasAllParameters() {

    if (this.getFormatExtNameInc() != null && this.getFormatExtNameIncCrt() != null
        && this.getFormatFilenameInc() != null && this.getPathABCIncoming() != null
        && hasAllParametersWithData()) {
      return Boolean.TRUE;
    } else {
      return Boolean.FALSE;
    }

  }

  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
   * </ul>
   * <p>
   * 
   * Metodo que chequea si los parametros tienen datos
   * 
   * @return
   * @since 1.X
   */
  private Boolean hasAllParametersWithData() {

    if (!this.getFormatExtNameInc().equalsIgnoreCase(ConstantesUtil.EMPTY.toString())
        && !this.getFormatExtNameIncCrt().equalsIgnoreCase(ConstantesUtil.EMPTY.toString())
        && !this.getFormatFilenameInc().equalsIgnoreCase(ConstantesUtil.EMPTY.toString())
        && !this.getPathABCIncoming().equalsIgnoreCase(ConstantesUtil.EMPTY.toString())) {
      return Boolean.TRUE;
    } else {
      return  Boolean.FALSE;
    }
  }

    @Override
    public String toString() {
    return "ParamsGenerateIncomingDTO [pathABCIncoming=" + pathABCIncoming + ", formatFilenameInc="
        + formatFilenameInc + ", formatExtNameInc=" + formatExtNameInc + ", formatExtNameIncCrt="
        + formatExtNameIncCrt + "]";
    }
    


}
