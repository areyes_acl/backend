package com.sbpay.sgi.cron.dto;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 xx/10/2018, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class LogTransaccionBiceDTO implements Serializable {
    
    /**
   * 
   */
    private static final long serialVersionUID = -8721266764009601962L;
    private String filename;
    private List<TransaccionBiceDTO> trxList;
    private String detalleLectura;
    
    public String getFilename() {
        return filename;
    }
    
    public void setFilename( String filename ) {
        this.filename = filename;
    }
    
    public List<TransaccionBiceDTO> getTrxList() {
        return trxList;
    }
    
    public void setTrxList( List<TransaccionBiceDTO> trxList ) {
        this.trxList = trxList;
    }
    
    public String getDetalleLectura() {
        return detalleLectura;
    }
    
    public void setDetalleLectura( String detalleLectura ) {
        this.detalleLectura = detalleLectura;
    }

	@Override
	public String toString() {
		return "LogTransaccionBiceDTO [filename=" + filename + ", trxList="
				+ trxList + ", detalleLectura=" + detalleLectura + "]";
	}
    

    
}
