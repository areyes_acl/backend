package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 18/12/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class ParamDownloadOnusDTO implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = 4081693614785403716L;
    private String formatFileNameOnus;
    private String formatExtNameOnus;
    private String pathAbcOnUs;
    private String pathFtpIcSalida;
    private DataFTP dataFTP;
    

	public String getFormatFileNameOnus() {
		return formatFileNameOnus;
	}
	public void setFormatFileNameOnus(String formatFileNameOnus) {
		this.formatFileNameOnus = formatFileNameOnus;
	}


	public String getFormatExtNameOnus() {
		return formatExtNameOnus;
	}
	public void setFormatExtNameOnus(String formatExtNameOnus) {
		this.formatExtNameOnus = formatExtNameOnus;
	}

	public String getPathAbcOnUs() {
		return pathAbcOnUs;
	}
	public void setPathAbcOnUs(String pathAbcOnUs) {
		this.pathAbcOnUs = pathAbcOnUs;
	}

	public String getPathFtpIcSalida() {
		return pathFtpIcSalida;
	}
	public void setPathFtpIcSalida(String pathFtpIcSalida) {
		this.pathFtpIcSalida = pathFtpIcSalida;
	}
	public DataFTP getDataFTP() {
		return dataFTP;
	}
	public void setDataFTP(DataFTP dataFTP) {
		this.dataFTP = dataFTP;
	}
    @Override
    public String toString() {
        return "ParamDownloadOnusDTO [formatFileNameOnus=" + formatFileNameOnus
                + ", formatExtNameOnus=" + formatExtNameOnus + ", pathAbcOnUs="
                + pathAbcOnUs + ", pathFtpIcSalida=" + pathFtpIcSalida
                + ", dataFTP=" + dataFTP + "]";
    }
    
	

    
}
