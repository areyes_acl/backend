package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 DD/08/2018, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * Clase encargada de mapear la tabla TBL_PRTS
 * 
 * </p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class ParamCronBiceDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -771323209087441009L;
	private DataFTP dataFTP;
	private String ftpPathBice;
	private String formatFilenameBice;
	private String pathAclBice;
	private String formatFilenameBiceCorre;
	private String formatExtnameBiceCtr;
	
	public String getFormatFilenameBiceCorre() {
		return formatFilenameBiceCorre;
	}
	public void setFormatFilenameBiceCorre(String formatFilenameBiceCorre) {
		this.formatFilenameBiceCorre = formatFilenameBiceCorre;
	}
	public DataFTP getDataFTP() {
		return dataFTP;
	}
	public void setDataFTP(DataFTP dataFTP) {
		this.dataFTP = dataFTP;
	}
	public String getFtpPathBice() {
		return ftpPathBice;
	}
	public void setFtpPathBice(String ftpPathBice) {
		this.ftpPathBice = ftpPathBice;
	}
	public String getFormatFilenameBice() {
		return formatFilenameBice;
	}
	public void setFormatFilenameBice(String formatFilenameBice) {
		this.formatFilenameBice = formatFilenameBice;
	}
	public String getPathAclBice() {
		return pathAclBice;
	}
	public void setPathAclBice(String pathAclBice) {
		this.pathAclBice = pathAclBice;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	public String getFormatExtnameBiceCtr() {
		return formatExtnameBiceCtr;
	}
	public void setFormatExtnameBiceCtr(String formatExtnameBiceCtr) {
		this.formatExtnameBiceCtr = formatExtnameBiceCtr;
	}
	@Override
	public String toString() {
		return "ParamCronBiceDTO [dataFTP=" + dataFTP + ", ftpPathBice="
				+ ftpPathBice + ", formatFilenameBice=" + formatFilenameBice
				+ ", pathAclBice=" + pathAclBice + ", formatFilenameBiceCorre="
				+ formatFilenameBiceCorre + ", formatExtnameBiceCtr="
				+ formatExtnameBiceCtr + "]";
	}
	


}
