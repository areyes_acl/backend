package com.sbpay.sgi.cron.dto;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 18/12/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class ParamsDownloadRechazosDTO implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = 4081693614785403716L;
    private String formatFileAbcRch;
    private String formatExtNameRch;
    private String pathAbcRch;
    private String pathFtpIcSalida;
    private DataFTP dataFTP;
    
    public String getFormatFileAbcRch() {
        return formatFileAbcRch;
    }
    
    public void setFormatFileAbcRch( String formatFileAbcRch ) {
        this.formatFileAbcRch = formatFileAbcRch;
    }
    
    public String getFormatExtNameRch() {
        return formatExtNameRch;
    }
    
    public void setFormatExtNameRch( String formatExtNameRch ) {
        this.formatExtNameRch = formatExtNameRch;
    }
    
    public String getPathAbcRch() {
        return pathAbcRch;
    }
    
    public void setPathAbcRch( String pathAbcRch ) {
        this.pathAbcRch = pathAbcRch;
    }
    
    public String getPathFtpIcSalida() {
        return pathFtpIcSalida;
    }
    
    public void setPathFtpIcSalida( String pathFtpIcSalida ) {
        this.pathFtpIcSalida = pathFtpIcSalida;
    }
    
    public DataFTP getDataFTP() {
        return dataFTP;
    }
    
    public void setDataFTP( DataFTP dataFTP ) {
        this.dataFTP = dataFTP;
    }
    
    @Override
    public String toString() {
        return "ParamsDownloadRechazosDTO [formatFileAbcRch="
                + formatFileAbcRch + ", formatExtNameRch=" + formatExtNameRch
                + ", pathAbcRch=" + pathAbcRch + ", pathFtpIcSalida="
                + pathFtpIcSalida + ", dataFTP=" + dataFTP + "]";
    }
    
}
