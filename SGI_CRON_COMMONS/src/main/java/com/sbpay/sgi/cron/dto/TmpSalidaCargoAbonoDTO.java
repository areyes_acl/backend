package com.sbpay.sgi.cron.dto;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 15/12/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * Clase encargada de mapear la tabla TMP_SALIDA_CARGO_ABONO
 * 
 * </p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class TmpSalidaCargoAbonoDTO {
    
    public TmpSalidaCargoAbonoDTO() {
    }
    
    private long sid;
    private String tipoRegistro;
    private String codEmisor;
    private String tipoSolicitud;
    private String codTarjeta;
    private String descripcionTblCargosAbonos;
    private String nroCuotas;
    private String montoTransacCargoAbono;
    private String fechaEfectiva;
    private String sucursalRecaudadora;
    private String transactionCode;
    private String horaTransaccion;
    private String merchantNumber;
    private String paymentMode;
    private String paymentDetails;
    private String transactionCurrency;
    private String tasaInteres;
    private String filter1;
    private String filter2;
    private String pagoDiferido;
    private String tipoMoneda;
    
    public long getSid() {
        return sid;
    }
    
    public void setSid( long sid ) {
        this.sid = sid;
    }
    
    public String getTipoRegistro() {
        return tipoRegistro;
    }
    
    public void setTipoRegistro( String tipoRegistro ) {
        this.tipoRegistro = tipoRegistro;
    }
    
    public String getCodEmisor() {
        return codEmisor;
    }
    
    public void setCodEmisor( String codEmisor ) {
        this.codEmisor = codEmisor;
    }
    
    public String getTipoSolicitud() {
        return tipoSolicitud;
    }
    
    public void setTipoSolicitud( String tipoSolicitud ) {
        this.tipoSolicitud = tipoSolicitud;
    }
    
    public String getCodTarjeta() {
        return codTarjeta;
    }
    
    public void setCodTarjeta( String codTarjeta ) {
        this.codTarjeta = codTarjeta;
    }
    
    public String getDescripcionTblCargosAbonos() {
        return descripcionTblCargosAbonos;
    }
    
    public void setDescripcionTblCargosAbonos( String descripcionTblCargosAbonos ) {
        this.descripcionTblCargosAbonos = descripcionTblCargosAbonos;
    }
    
    public String getNroCuotas() {
        return nroCuotas;
    }
    
    public void setNroCuotas( String nroCuotas ) {
        this.nroCuotas = nroCuotas;
    }
    
    public String getMontoTransacCargoAbono() {
        return montoTransacCargoAbono;
    }
    
    public void setMontoTransacCargoAbono( String montoTransacCargoAbono ) {
        this.montoTransacCargoAbono = montoTransacCargoAbono;
    }
    
    public String getFechaEfectiva() {
        return fechaEfectiva;
    }
    
    public void setFechaEfectiva( String fechaEfectiva ) {
        this.fechaEfectiva = fechaEfectiva;
    }
    
    public String getSucursalRecaudadora() {
        return sucursalRecaudadora;
    }
    
    public void setSucursalRecaudadora( String sucursalRecaudadora ) {
        this.sucursalRecaudadora = sucursalRecaudadora;
    }
    
    public String getTransactionCode() {
        return transactionCode;
    }
    
    public void setTransactionCode( String transactionCode ) {
        this.transactionCode = transactionCode;
    }
    
    public String getHoraTransaccion() {
        return horaTransaccion;
    }
    
    public void setHoraTransaccion( String horaTransaccion ) {
        this.horaTransaccion = horaTransaccion;
    }
    
    public String getMerchantNumber() {
        return merchantNumber;
    }
    
    public void setMerchantNumber( String merchantNumber ) {
        this.merchantNumber = merchantNumber;
    }
    
    public String getPaymentMode() {
        return paymentMode;
    }
    
    public void setPaymentMode( String paymentMode ) {
        this.paymentMode = paymentMode;
    }
    
    public String getPaymentDetails() {
        return paymentDetails;
    }
    
    public void setPaymentDetails( String paymentDetails ) {
        this.paymentDetails = paymentDetails;
    }
    
    public String getTransactionCurrency() {
        return transactionCurrency;
    }
    
    public void setTransactionCurrency( String transactionCurrency ) {
        this.transactionCurrency = transactionCurrency;
    }
    
    public String getTasaInteres() {
        return tasaInteres;
    }
    
    public void setTasaInteres( String tasaInteres ) {
        this.tasaInteres = tasaInteres;
    }
    
    public String getFilter1() {
        return filter1;
    }
    
    public void setFilter1( String filter1 ) {
        this.filter1 = filter1;
    }
    
    public String getFilter2() {
        return filter2;
    }
    
    public void setFilter2( String filter2 ) {
        this.filter2 = filter2;
    }
    
    public String getPagoDiferido() {
        return pagoDiferido;
    }
    
    public void setPagoDiferido( String pagoDiferido ) {
        this.pagoDiferido = pagoDiferido;
    }
    
    public String getTipoMoneda() {
        return tipoMoneda;
    }
    
    public void setTipoMoneda( String tipoMoneda ) {
        this.tipoMoneda = tipoMoneda;
    }
    
    @Override
    public String toString() {
        return "TmpSalidaCargoAbonoDTO [sid=" + sid + ", tipoRegistro="
                + tipoRegistro + ", codEmisor=" + codEmisor
                + ", tipoSolicitud=" + tipoSolicitud + ", codTarjeta="
                + codTarjeta + ", descripcionTblCargosAbonos="
                + descripcionTblCargosAbonos + ", nroCuotas=" + nroCuotas
                + ", montoTransacCargoAbono=" + montoTransacCargoAbono
                + ", fechaEfectiva=" + fechaEfectiva + ", sucursalRecaudadora="
                + sucursalRecaudadora + ", transactionCode=" + transactionCode
                + ", horaTransaccion=" + horaTransaccion + ", merchantNumber="
                + merchantNumber + ", paymentMode=" + paymentMode
                + ", paymentDetails=" + paymentDetails
                + ", transactionCurrency=" + transactionCurrency
                + ", tasaInteres=" + tasaInteres + ", filter1=" + filter1
                + ", filter2=" + filter2 + ", pagoDiferido=" + pagoDiferido
                + ", tipoMoneda=" + tipoMoneda + "]";
    }
    
}
