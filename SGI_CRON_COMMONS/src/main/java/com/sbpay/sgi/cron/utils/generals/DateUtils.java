package com.sbpay.sgi.cron.utils.generals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.DateFormatType;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 17/11/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * Clase creada para el procesamiento de fechas
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class DateUtils {
    /** VARIABLE PARA EL LOGER */
    private static final Logger LOGGER = Logger.getLogger( DateUtils.class );
    // FECHA QUE PUEDE SER FUTURA
    public static final String FECHA_FUTURA = "FECHA_FUTURA";
    // FECHA QUE NO PUEDE SER FUTURA
    public static final String FECHA_NORMAL = "FECHA_NORMAL";
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 17/11/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * Metodo que transforma un String dado en fecha Julian a un
     * String de la fecha con el formato definido en al entrada
     * 
     * @param julianDate
     * @return
     * @throws ParseException
     * @since 1.X
     */
    public static String transformJulianDateInGregorianDateString(
            String julianDate ) {
        
        Date date = null;
        try {
            date = new SimpleDateFormat( "yyddd" ).parse( julianDate );
            String normalDate = new SimpleDateFormat( "ddMMyy" ).format( date );
            
            return normalDate;
            
        }
        catch ( ParseException e ) {
            LOGGER.error( "Error al transaformar la fecha en formato juliana",
                    e );
        }
        return "";
        
    }
    
    public static String transformJulianDateInGregorianDateString2(
            String julianDate ) {
        
        Date date = null;
        try {
            date = new SimpleDateFormat( "yyddd" ).parse( julianDate );
            String normalDate = new SimpleDateFormat( "yyMMdd" ).format( date );
            
            return normalDate;
            
        }
        catch ( ParseException e ) {
            LOGGER.error( "Error al transaformar la fecha en formato juliana",
                    e );
        }
        return "";
        
    }
    
    public static String transformJulianDateInGregorianDateStringPago(
            String julianDate ) {
        Date date = null;
        try {
            date = new SimpleDateFormat( "yyyyddd" ).parse( julianDate );
            String normalDate = new SimpleDateFormat( "ddMMyy" ).format( date );
            
            return normalDate;
            
        }
        catch ( ParseException e ) {
            LOGGER.error( "Error al transaformar la fecha en formato juliana",
                    e );
        }
        return "";
        
    }
    
    public static String transformJulianDateInGregorianDateStringCobroCargo(
            String julianDate ) {
        Date date = null;
        try {
            date = new SimpleDateFormat( "yddd" ).parse( julianDate );
            String normalDate = new SimpleDateFormat( "yyMMdd" ).format( date );
            
            return normalDate;
            
        }
        catch ( ParseException e ) {
            LOGGER.error( "Error al transaformar la fecha en formato juliana",
                    e );
        }
        return "";
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.1 02/12/2015, (ACL-sbpay) - Modificado
     * </ul>
     * <p>
     * 
     * Obtiene la fecha desde el nombre de archivo que viene en
     * formato 'mmdd'
     * 
     * @param filename
     *            : Nombre del arhivo
     * @param starWith
     *            : String con el que debiese comenzar el nombre del
     *            archivo
     * @return String Fecha con formato mmdd
     * @since 1.1
     */
    public static String getDateStringFromFilename( String filename,
            String starWith ) {
        
        return filename.replace( starWith, ConstantesUtil.EMPTY.toString() )
                .substring( 0, 4 );
        
    }
    
    
    public static String getDateStringFromFilenameVisa( String filename,
            String starWith ) {
        
        return filename.replace( starWith, ConstantesUtil.EMPTY.toString() )
                .substring( 0, 8 );
        
    }
    
    /**
     * BICE
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.1 DD/09/2018, (ACL-sbpay) - Modificado
     * </ul>
     * <p>
     * Obtiene la fecha desde el nombre de archivo que viene en
     * formato 'YYYYMMDD'
     * 
     * @param filename
     *            : Nombre del arhivo
     * @param starWith
     *            : String con el que debiese comenzar el nombre del
     *            archivo
     * @return String Fecha con formato mmdd
     * @since 1.1
     */
    public static String getDateStringFromFilenameBice( String filename,
            String starWith ) {
        return filename;
        //return filename.replace( starWith, ConstantesUtil.EMPTY.toString() )
          //      .substring( 0, 0 );
        
    }
    
    /**
     * AVANCES
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.1 DD/10/2018, (ACL-sbpay) - Modificado
     * </ul>
     * <p>
     * Obtiene la fecha desde el nombre de archivo que viene en
     * formato 'YYYYMMDD'
     * 
     * @param filename
     *            : Nombre del arhivo
     * @param starWith
     *            : String con el que debiese comenzar el nombre del
     *            archivo
     * @return String Fecha con formato mmdd
     * @since 1.1
     */
    public static String getDateStringFromFilenameAvances( String filename,
            String starWith ) {
        return filename;
        //return filename.replace( starWith, ConstantesUtil.EMPTY.toString() )
          //      .substring( 0, 0 );
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 17/11/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @param fila
     * @throws AppException
     * @since 1.X
     */
    
    public static String getDateStringFromFileHeaderContent( String fila, int pos1, int pos2)
            throws AppException {
        try {
            String fechaHeader = DateUtils
                    .transformJulianDateInGregorianDateString( fila.substring(
                    		pos1, pos2 ) );
            return fechaHeader;
        }
        catch ( Exception e ) {
            throw new AppException(
                    "Formato de fecha leída no es válida, El archivo no puede ser procesado",
                    e );
        }
    }
    
    public static String getDateStringFromFileHeaderContent2( String fila, int pos1, int pos2)
            throws AppException {
        try {
            String fechaHeader = DateUtils
                    .transformJulianDateInGregorianDateString2( fila.substring(
                    		pos1, pos2 ) );
            return fechaHeader;
        }
        catch ( Exception e ) {
            throw new AppException(
                    "Formato de fecha leída no es válida, El archivo no puede ser procesado",
                    e );
        }
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param fecha
     * @return
     * @since 1.X
     */
    public static String getStringDatefromYYMMMDDToFormatYYMMDD( String fecha ) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                DateFormatType.FORMAT_DD_MMM_YYYY.toString(), Locale.US );
        try {
            Date varDate = dateFormat.parse( fecha );
            dateFormat = new SimpleDateFormat(
                    DateFormatType.FORMAT_YYMMDD.toString() );
            return dateFormat.format( varDate );
        }
        catch ( Exception e ) {
            LOGGER.warn( e.getMessage(), e );
            return "";
        }
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 17/11/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @param dateOne
     * @param dateTwo
     * @return
     * @since 1.X
     */
    public static Boolean dateAreEquals( String dateOne, String dateTwo ) {
        if ( dateOne.equalsIgnoreCase( dateTwo ) ) {
            return Boolean.TRUE;
        }
        
        return Boolean.FALSE;
        
    }
    
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 23/11/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * Metodo valida que el formato de fecha sea el ingresado.
     * 
     * @param julianDate
     * @return true: formato fecha ok; false: formato fecha
     *         incorrecto.
     * @since 1.X
     */
    public static boolean validateFormatDate( final String format,
            final String date ) {
        SimpleDateFormat formatoFecha = null;
        try {
            formatoFecha = new SimpleDateFormat( format, Locale.getDefault() );
            formatoFecha.setLenient( false );
            formatoFecha.parse( date );
            return true;
        }
        catch ( ParseException e ) {
            return false;
        }
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 26/11/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * Metodo que compara una fecha ingresada con la fecha de hoy. Si
     * la fecha de entrada es mayor que la fecha de hoy la fecha que
     * se retorna va con el año disminuido en 1 y se rellenan con
     * 00:00:00
     * 
     * La comparaciòn se hace en base al mismo año pero a diferentes
     * dias y meses
     * 
     * FIX: se corrige que se ingrese el tipo de fecha, ya que la
     * fecha de compensacion de un incoming puede ser futura pero si
     * utiliza este metodo no se permitia que se respetara la fecha
     * futura, restando un año para esta tipo de fecha. Se agrega tipo
     * de fecha si es normal, se respetara que no puede ser futura y
     * si asi lo fuese se le restara un año. Si es fecha futura se le
     * respetarà el año actual.
     * 
     * Ejemplo: si hoy fuese 20/09/2016, y se recibe una fecha del
     * tipo 0930, el metodo la convierte segun el tipo en: tipo normal
     * la convierte en (yyMMdd)=> 150930, en cambio la del tipo futura
     * la convierte en (yyMMdd) => 160930
     * 
     * 
     * @return
     * @since 1.2
     */
    public static String getStringDateWithValidationProcess(
            String fechaCompra, String tipoFecha ) {
        SimpleDateFormat formatter = new SimpleDateFormat( "yyMMdd" );
        String mes = fechaCompra.substring( 0, 2 );
        String dia = fechaCompra.substring( 2, 4 );
        
        // FECHA COMPRA
        Calendar fechaCompraCal = Calendar.getInstance();
        fechaCompraCal.set( Calendar.DAY_OF_MONTH, Integer.valueOf( dia ) );
        
        // MES - 1 PARA QUE TOME BIEN EL VALOR
        fechaCompraCal.set( Calendar.MONTH, Integer.valueOf( mes ) - 1 );
        fechaCompraCal.set( Calendar.HOUR_OF_DAY, 00 );
        fechaCompraCal.set( Calendar.MINUTE, 00 );
        fechaCompraCal.set( Calendar.SECOND, 00 );
        
        // FECHA DE HOY - PARA COMPARAR
        Calendar fechaActual = Calendar.getInstance();
        
        if ( !FECHA_FUTURA.equalsIgnoreCase( tipoFecha )
                && fechaCompraCal.after( fechaActual ) ) {
            // SE RESTA UN AÑO
            fechaCompraCal.add( Calendar.YEAR, -1 );
        }
        
        String fechaReturn = formatter.format( fechaCompraCal.getTime() );
        
        return fechaReturn;
        
    }
    
    public static String getStringDateWithValidationProcessVisa(
            String fechaCompra ) {
        SimpleDateFormat formatter = new SimpleDateFormat( "yyMMdd" );
        String anio = fechaCompra.substring( 0, 2 );
        String mes = fechaCompra.substring( 2, 4 );
        String dia = fechaCompra.substring( 4, 6 );
        
        // FECHA COMPRA
        Calendar fechaCompraCal = Calendar.getInstance();
        fechaCompraCal.set( Calendar.DAY_OF_MONTH, Integer.valueOf( dia ) );
        
        // MES - 1 PARA QUE TOME BIEN EL VALOR
        fechaCompraCal.set( Calendar.MONTH, Integer.valueOf( mes ) - 1 );
        fechaCompraCal.set( Calendar.HOUR_OF_DAY, 00 );
        fechaCompraCal.set( Calendar.MINUTE, 00 );
        fechaCompraCal.set( Calendar.SECOND, 00 );
        fechaCompraCal.set( Calendar.YEAR, Integer.valueOf( anio ) );
        
        String fechaReturn = formatter.format( fechaCompraCal.getTime() );
        
        return fechaReturn;
        
    }
    

    
    /**
     * Metodo entrega la fecha formateada desde mmdd -> dd/mm/yy.
     * 
     * @param dateAux
     *            fecha en formato mmdd.
     * @return return fecha dd/mm/yy.
     */
    public static String getDateFileTbk( final String dateAux ) {
        
        String year = null;
        String fechaAux = null;
        if ( dateAux != null && dateAux.length() >= 4 ) {
            String mes = dateAux.substring( 0, 2 );
            String dia = dateAux.substring( 2, 4 );
            
            // FECHA COMPRA
            Calendar fechaCompraCal = Calendar.getInstance();
            fechaCompraCal.set( Calendar.DAY_OF_MONTH, Integer.valueOf( dia ) );
            // MES - 1 PARA QUE TOME BIEN EL VALOR
            fechaCompraCal.set( Calendar.MONTH, Integer.valueOf( mes ) - 1 );
            fechaCompraCal.set( Calendar.HOUR_OF_DAY, 00 );
            fechaCompraCal.set( Calendar.MINUTE, 00 );
            fechaCompraCal.set( Calendar.SECOND, 00 );
            
            // FECHA DE HOY - PARA COMPARAR
            Calendar fechaActual = Calendar.getInstance();
            
            if ( fechaCompraCal.after( fechaActual ) ) {
                // SE RESTA UN AÑO
                fechaCompraCal.add( Calendar.YEAR, -1 );
                year = Integer.toString( Calendar.getInstance().get(
                        Calendar.YEAR ) - 1 );
            }
            else {
                year = Integer.toString( Calendar.getInstance().get(
                        Calendar.YEAR ) );
            }
            year = year.substring( 2, 4 );
            fechaAux = dia.concat( ConstantesUtil.SLASH.toString() )
                    .concat( mes ).concat( ConstantesUtil.SLASH.toString() )
                    .concat( year );
            
            if ( !DateUtils.validateFormatDate(
                    ConstantesUtil.FORMAT_DDMMYY.toString(), fechaAux ) ) {
                fechaAux = null;
            }
        }
        return fechaAux;
    }
    
    /**
     * Metodo entrega la fecha formateada desde mmdd -> dd/mm/yy. Igual que tbk pero nombre general visa nacional
     * 
     * @param dateAux
     *            fecha en formato mmdd.
     * @return return fecha dd/mm/yy.
     */
    public static String getDateFileVisaNac( final String dateAux ) {
       
    	String year = null;
        String fechaAux = null;
        if ( dateAux != null) {
            String mes = dateAux.substring( 4, 6 );
            String dia = dateAux.substring( 6, 8 );
            year = dateAux.substring( 2, 4 );
            fechaAux = dia.concat( ConstantesUtil.SLASH.toString() )
                    .concat( mes ).concat( ConstantesUtil.SLASH.toString() )
                    .concat( year );
            
            if ( !DateUtils.validateFormatDate(
                    ConstantesUtil.FORMAT_DDMMYY.toString(), fechaAux ) ) {
                fechaAux = null;
            }
        }
        return fechaAux;
    }
    
    /**
     * Metodo entrega la fecha formateada desde mmdd -> dd/mm/yy. Igual que tbk pero nombre general visa internacional
     * 
     * @param dateAux
     *            fecha en formato mmdd.
     * @return return fecha dd/mm/yy.
     */
    public static String getDateFileVisaInt( final String dateAux ) {
        
    	
        String year = null;
        String fechaAux = null;
        if ( dateAux != null) {
            String mes = dateAux.substring( 4, 6 );
            String dia = dateAux.substring( 6, 8 );
            year = dateAux.substring( 2, 4 );
            fechaAux = dia.concat( ConstantesUtil.SLASH.toString() )
                    .concat( mes ).concat( ConstantesUtil.SLASH.toString() )
                    .concat( year );
            
            if ( !DateUtils.validateFormatDate(
                    ConstantesUtil.FORMAT_DDMMYY.toString(), fechaAux ) ) {
                fechaAux = null;
            }
        }
        return fechaAux;
    }
    
    
    /**
     * Metodo entrega la fecha formateada desde mmdd -> dd/mm/yy.
     * 
     * @param dateAux
     *            fecha en formato mmdd.
     * @return return fecha dd/mm/yy.
     */
    public static String getDateFileAsientosCntbl( final String dateAux ) {
        
        String year = null;
        String fechaAux = null;
        if ( dateAux != null && dateAux.length() >= 4 ) {
            String mes = dateAux.substring( 4, 6 );
            String dia = dateAux.substring( 6, 8 );
            
            // FECHA COMPRA
            Calendar fechaCompraCal = Calendar.getInstance();
            fechaCompraCal.set( Calendar.DAY_OF_MONTH, Integer.valueOf( dia ) );
            // MES - 1 PARA QUE TOME BIEN EL VALOR
            fechaCompraCal.set( Calendar.MONTH, Integer.valueOf( mes ) - 1 );
            fechaCompraCal.set( Calendar.HOUR_OF_DAY, 00 );
            fechaCompraCal.set( Calendar.MINUTE, 00 );
            fechaCompraCal.set( Calendar.SECOND, 00 );
            
            // FECHA DE HOY - PARA COMPARAR
            Calendar fechaActual = Calendar.getInstance();
            
            if ( fechaCompraCal.after( fechaActual ) ) {
                // SE RESTA UN AÑO
                fechaCompraCal.add( Calendar.YEAR, -1 );
                year = Integer.toString( Calendar.getInstance().get(
                        Calendar.YEAR ) - 1 );
            }
            else {
                year = Integer.toString( Calendar.getInstance().get(
                        Calendar.YEAR ) );
            }
            year = year.substring( 2, 4 );
            fechaAux = dia.concat( ConstantesUtil.SLASH.toString() )
                    .concat( mes ).concat( ConstantesUtil.SLASH.toString() )
                    .concat( year );
            
            if ( !DateUtils.validateFormatDate(
                    ConstantesUtil.FORMAT_DDMMYY.toString(), fechaAux ) ) {
                fechaAux = null;
            }
        }
        return fechaAux;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 7/12/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * Metodo que obtiene la fecha del dia de hoy en fomato MMdd
     * 
     * @return fecha en formato MMdd
     * @since 1.X
     */
    public static String getDateTodayInSpecialFormat() {
        SimpleDateFormat formatter = new SimpleDateFormat( "MMdd" );
        Calendar cal = Calendar.getInstance();
        return formatter.format( cal.getTime() );
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 12/02/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * Metodo que obtiene la fecha del dia de hoy en fomato YYYYMMDD
     * 
     * @return fecha en formato MMdd
     * @since 1.X
     */
    public static String getDateTodayInYYYYMMDD() {
        SimpleDateFormat formatter = new SimpleDateFormat( "yyyyMMdd" );
        Calendar cal = Calendar.getInstance();
        return formatter.format( cal.getTime() );
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 12/09/2018, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * Metodo que obtiene la fecha del dia(-1 dia) de hoy en fomato YYYYMMDD
     * 
     * @return fecha en formato MMdd
     * @since 1.X
     */
    public static  String getDateTodayInYYYYMMDDAnterior() {
        SimpleDateFormat formatter = new SimpleDateFormat( "yyyyMMdd" );
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return formatter.format( cal.getTime());
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 9/12/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * Metodo que retorna el dia de hoy en formato fecha juliana
     * 
     * @return Hoy en formato yyDDD
     * @since 1.X
     */
    public static String getTodayStringDateInJulianFormat() {
        String normalDate = new SimpleDateFormat( "yyDDD" ).format( Calendar
                .getInstance().getTime() );
        
        return normalDate;
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 11/12/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * Metodo que transforma de un formato yyMMdd a un formato MMdd
     * 
     * @param date
     *            formato yyMMDD
     * @return formato MMdd
     * @since 1.X
     */
    public static String transformDatefromYYMMDDToFormatMMDD( String date ) {
        SimpleDateFormat sdfIn = new SimpleDateFormat( "yyMMdd" );
        SimpleDateFormat sdfOut = new SimpleDateFormat( "MMdd" );
        try {
            Date fecha = sdfIn.parse( date );
            return sdfOut.format( fecha );
        }
        catch ( ParseException e ) {
            LOGGER.warn( e.getMessage(), e );
        }
        
        return null;
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 15/12/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * Metodo encargado de obtener la fecha de hoy en formato
     * 
     * 
     * @return yyMMdd
     * @since 1.X
     */
    public static String getTodayInYYMMDDFormat() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat( "yyMMdd" );
        return sdf.format( cal.getTime() );
        
    }
    
    public static String getTodayInYDDDFormat() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat( "yDDD" );
        return sdf.format( cal.getTime() );
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 16/12/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * Metodo que dada una fecha la convierte al formato de salida
     * 
     * @param date
     *            : Fecha a modificar en formato String
     * @param formatIn
     *            : Formato de fecha de entrada
     * @param formatOut
     *            : Formato de fecha de salida
     * @return
     * @since 1.X
     */
    public static String parseFormat( String date, DateFormatType formatIn,
            DateFormatType formatOut ) {
        SimpleDateFormat sdfIn = new SimpleDateFormat( formatIn.toString() );
        SimpleDateFormat sdfOut = new SimpleDateFormat( formatOut.toString() );
        String sDate = null;
        try {
            Date fecha = sdfIn.parse( date );
            sDate = sdfOut.format( fecha );
        }
        catch ( ParseException e ) {
            LOGGER.warn( e.getMessage(), e );
        }
        
        return sDate;
        
    }
    
    /**
     * Metodo entrega la fecha formateada desde mmdd -> dd/mm/yy.
     * 
     * @param dateAux
     *            fecha en formato mmdd.
     * @return return fecha dd/mm/yy.
     */
    public static String getDateFileTbkCP( final String dateAux ) {
        
        String year = null;
        String fechaAux = null;
        if ( dateAux != null && dateAux.length() >= 4 ) {
            String dia = dateAux.substring( 0, 2 );
            String mes = dateAux.substring( 2, 4 );
            
            // FECHA COMPRA
            Calendar fechaCompraCal = Calendar.getInstance();
            fechaCompraCal.set( Calendar.DAY_OF_MONTH, Integer.valueOf( dia ) );
            // MES - 1 PARA QUE TOME BIEN EL VALOR
            fechaCompraCal.set( Calendar.MONTH, Integer.valueOf( mes ) - 1 );
            fechaCompraCal.set( Calendar.HOUR_OF_DAY, 00 );
            fechaCompraCal.set( Calendar.MINUTE, 00 );
            fechaCompraCal.set( Calendar.SECOND, 00 );
            
            // FECHA DE HOY - PARA COMPARAR
            Calendar fechaActual = Calendar.getInstance();
            
            if ( fechaCompraCal.after( fechaActual ) ) {
                // SE RESTA UN AÑO
                fechaCompraCal.add( Calendar.YEAR, -1 );
                year = Integer.toString( Calendar.getInstance().get(
                        Calendar.YEAR ) - 1 );
            }
            else {
                year = Integer.toString( Calendar.getInstance().get(
                        Calendar.YEAR ) );
            }
            year = year.substring( 2, 4 );
            fechaAux = dia.concat( ConstantesUtil.SLASH.toString() )
                    .concat( mes ).concat( ConstantesUtil.SLASH.toString() )
                    .concat( year );
            
            if ( !DateUtils.validateFormatDate(
                    ConstantesUtil.FORMAT_DDMMYY.toString(), fechaAux ) ) {
                fechaAux = null;
            }
        }
        return fechaAux;
    }
    

    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 DD/09/2018, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Obtiene fecha de hoy
     * @return
     * @since 1.X
     */    
  public static String getDateFileBice( final String dateAux ) {
        
        String fechaAux = null;
        if ( dateAux != null) {
        	String anio = dateAux.substring( 0, 4 );
            String mes = dateAux.substring( 4, 6 );
            String dia = dateAux.substring( 6, 8 );
            
            // FECHA COMPRA
            Calendar fechaCompraCal = Calendar.getInstance();
            fechaCompraCal.set( Calendar.DAY_OF_MONTH, Integer.valueOf( dia ) );
            // MES - 1 PARA QUE TOME BIEN EL VALOR
            fechaCompraCal.set( Calendar.MONTH, Integer.valueOf( mes ) - 1 );
            fechaCompraCal.set( Calendar.HOUR_OF_DAY, 00 );
            fechaCompraCal.set( Calendar.MINUTE, 00 );
            fechaCompraCal.set( Calendar.SECOND, 00 );
                        
            fechaAux = dia.concat( ConstantesUtil.SLASH.toString() )
                    .concat( mes ).concat( ConstantesUtil.SLASH.toString() )
                    .concat( anio );
            
            if ( !DateUtils.validateFormatDate(
                    ConstantesUtil.FORMAT_DDMMYY.toString(), fechaAux ) ) {
                fechaAux = null;
            }            
        }
        return fechaAux;
    }
  
  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 DD/10/2018, (ACL SPA) - versión inicial
   * </ul>
   * <p>
   * Obtiene fecha de hoy
   * @return
   * @since 1.X
   */    
public static String getDateFileAvances( final String dateAux ) {
      
      String fechaAux = null;
      if ( dateAux != null) {
      	String anio = dateAux.substring( 0, 4 );
          String mes = dateAux.substring( 4, 6 );
          String dia = dateAux.substring( 6, 8 );
          
          // FECHA COMPRA
          Calendar fechaCompraCal = Calendar.getInstance();
          fechaCompraCal.set( Calendar.DAY_OF_MONTH, Integer.valueOf( dia ) );
          // MES - 1 PARA QUE TOME BIEN EL VALOR
          fechaCompraCal.set( Calendar.MONTH, Integer.valueOf( mes ) - 1 );
          fechaCompraCal.set( Calendar.HOUR_OF_DAY, 00 );
          fechaCompraCal.set( Calendar.MINUTE, 00 );
          fechaCompraCal.set( Calendar.SECOND, 00 );
                      
          fechaAux = dia.concat( ConstantesUtil.SLASH.toString() )
                  .concat( mes ).concat( ConstantesUtil.SLASH.toString() )
                  .concat( anio );
          
          if ( !DateUtils.validateFormatDate(
                  ConstantesUtil.FORMAT_DDMMYY.toString(), fechaAux ) ) {
              fechaAux = null;
          }            
      }
      return fechaAux;
  }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    public static String getToDayDDMMYY() {
        
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat( "dd/MM/yy" );
        StringBuilder str = new StringBuilder().append( sdf.format( cal
                .getTime() ) );
        return str.toString();
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * metodo que compara 2 fechas para averiguar cual es mayor o si
     * son iguales
     * 
     * @param fecha
     *            1 :fecha en formato dd/MM/yyyy fecha 2 :fehca en
     *            formato dd/MM/yyyy
     * @return -1: fecha1 < fecha2 0: fecha1 = fecha2 1: fecha1 >
     *         fecha2
     * @since 1.X
     */
    public static Integer compareDates( String fecha1, String fecha2 ) {
        
        try {
            SimpleDateFormat sdf = new SimpleDateFormat( "dd/MM/yy" );
            Date date1 = sdf.parse( fecha1 );
            Date date2 = sdf.parse( fecha2 );
            
            return date1.compareTo( date2 );
            
        }
        catch ( ParseException e ) {
            e.printStackTrace();
        }
        
        return null;
        
    }
}
