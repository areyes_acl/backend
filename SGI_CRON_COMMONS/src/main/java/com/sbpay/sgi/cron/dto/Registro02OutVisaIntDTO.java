package com.sbpay.sgi.cron.dto;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 30/04/2019, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class Registro02OutVisaIntDTO extends RegistroOutVisaIntDTO {
	/**
     * 
     */
	private static final long serialVersionUID = 2664046852952957779L;
	private String tipoVenta;//5
	private String numPagosPlazo;//6-8
	private String numPagosPlazoActual;//9-11
	private String flagPromoEmisora;//12
	private String flagDiferido;//13
	private String periodoDiferido;//14
	private String flagMesGracias;//15
	private String periodoMesGracia;//16
	private String codigoPais;//17-19
	private String pagoTotalAPlazos;//20-31
	private String valorPagoAPlazos;//32-43
	private String cuotasInteres;//44-55
	private String tasaIVA;//56-59
	private String tarifaReemIntercambioNacional;//60-71
	private String tasaReemIntercambioNacionalIVA;//72-83
	private String fechaLiquidacionDiferida;//84-89
	private String fechaLiquidacionDiferidaOriginal;//90-95
	private String reservado;//96-168 null
	public String getTipoVenta() {
		return tipoVenta;
	}
	public void setTipoVenta(String tipoVenta) {
		this.tipoVenta = tipoVenta;
	}
	public String getNumPagosPlazo() {
		return numPagosPlazo;
	}
	public void setNumPagosPlazo(String numPagosPlazo) {
		this.numPagosPlazo = numPagosPlazo;
	}
	public String getNumPagosPlazoActual() {
		return numPagosPlazoActual;
	}
	public void setNumPagosPlazoActual(String numPagosPlazoActual) {
		this.numPagosPlazoActual = numPagosPlazoActual;
	}
	public String getFlagPromoEmisora() {
		return flagPromoEmisora;
	}
	public void setFlagPromoEmisora(String flagPromoEmisora) {
		this.flagPromoEmisora = flagPromoEmisora;
	}
	public String getFlagDiferido() {
		return flagDiferido;
	}
	public void setFlagDiferido(String flagDiferido) {
		this.flagDiferido = flagDiferido;
	}
	public String getPeriodoDiferido() {
		return periodoDiferido;
	}
	public void setPeriodoDiferido(String periodoDiferido) {
		this.periodoDiferido = periodoDiferido;
	}
	public String getFlagMesGracias() {
		return flagMesGracias;
	}
	public void setFlagMesGracias(String flagMesGracias) {
		this.flagMesGracias = flagMesGracias;
	}
	public String getPeriodoMesGracia() {
		return periodoMesGracia;
	}
	public void setPeriodoMesGracia(String periodoMesGracia) {
		this.periodoMesGracia = periodoMesGracia;
	}
	public String getCodigoPais() {
		return codigoPais;
	}
	public void setCodigoPais(String codigoPais) {
		this.codigoPais = codigoPais;
	}
	public String getPagoTotalAPlazos() {
		return pagoTotalAPlazos;
	}
	public void setPagoTotalAPlazos(String pagoTotalAPlazos) {
		this.pagoTotalAPlazos = pagoTotalAPlazos;
	}
	public String getValorPagoAPlazos() {
		return valorPagoAPlazos;
	}
	public void setValorPagoAPlazos(String valorPagoAPlazos) {
		this.valorPagoAPlazos = valorPagoAPlazos;
	}
	public String getCuotasInteres() {
		return cuotasInteres;
	}
	public void setCuotasInteres(String cuotasInteres) {
		this.cuotasInteres = cuotasInteres;
	}
	public String getTasaIVA() {
		return tasaIVA;
	}
	public void setTasaIVA(String tasaIVA) {
		this.tasaIVA = tasaIVA;
	}
	public String getTarifaReemIntercambioNacional() {
		return tarifaReemIntercambioNacional;
	}
	public void setTarifaReemIntercambioNacional(
			String tarifaReemIntercambioNacional) {
		this.tarifaReemIntercambioNacional = tarifaReemIntercambioNacional;
	}
	public String getTasaReemIntercambioNacionalIVA() {
		return tasaReemIntercambioNacionalIVA;
	}
	public void setTasaReemIntercambioNacionalIVA(
			String tasaReemIntercambioNacionalIVA) {
		this.tasaReemIntercambioNacionalIVA = tasaReemIntercambioNacionalIVA;
	}
	public String getFechaLiquidacionDiferida() {
		return fechaLiquidacionDiferida;
	}
	public void setFechaLiquidacionDiferida(String fechaLiquidacionDiferida) {
		this.fechaLiquidacionDiferida = fechaLiquidacionDiferida;
	}
	public String getFechaLiquidacionDiferidaOriginal() {
		return fechaLiquidacionDiferidaOriginal;
	}
	public void setFechaLiquidacionDiferidaOriginal(
			String fechaLiquidacionDiferidaOriginal) {
		this.fechaLiquidacionDiferidaOriginal = fechaLiquidacionDiferidaOriginal;
	}
	public String getReservado() {
		return reservado;
	}
	public void setReservado(String reservado) {
		this.reservado = reservado;
	}
	@Override
	public String toString() {
		return "Registro02OutVisaIntDTO [tipoVenta=" + tipoVenta
				+ ", numPagosPlazo=" + numPagosPlazo + ", numPagosPlazoActual="
				+ numPagosPlazoActual + ", flagPromoEmisora="
				+ flagPromoEmisora + ", flagDiferido=" + flagDiferido
				+ ", periodoDiferido=" + periodoDiferido + ", flagMesGracias="
				+ flagMesGracias + ", periodoMesGracia=" + periodoMesGracia
				+ ", codigoPais=" + codigoPais + ", pagoTotalAPlazos="
				+ pagoTotalAPlazos + ", valorPagoAPlazos=" + valorPagoAPlazos
				+ ", cuotasInteres=" + cuotasInteres + ", tasaIVA=" + tasaIVA
				+ ", tarifaReemIntercambioNacional="
				+ tarifaReemIntercambioNacional
				+ ", tasaReemIntercambioNacionalIVA="
				+ tasaReemIntercambioNacionalIVA
				+ ", fechaLiquidacionDiferida=" + fechaLiquidacionDiferida
				+ ", fechaLiquidacionDiferidaOriginal="
				+ fechaLiquidacionDiferidaOriginal + ", reservado=" + reservado
				+ "]";
	}
	


}
