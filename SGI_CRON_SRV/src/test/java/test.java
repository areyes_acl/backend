import java.sql.Connection; 
import java.sql.DriverManager; 
import java.sql.Driver;
import java.sql.ResultSet; 
import java.sql.Statement; 

import com.sbpay.sgi.cron.enums.ConstantesUtil;

public class test {

	private java.sql.Connection con = null;
	private final String url = "jdbc:sqlserver://";
	private final String serverName = "192.168.0.237";
	private final String portNumber = "1433";
	private final String databaseName = "master";
	private final String userName = "sa";
	private final String password = "admin";
	// Indica al controlador que debe utilizar un cursor de servidor, // lo que
	// permite más de una instrucción activa // en una conexión.
	private final String selectMethod = "cursor";

	// Constructor public Connect(){}
	private String getConnectionUrl() {
		return url + serverName + ":" + portNumber + ";databaseName="
				+ databaseName + ";selectMethod=" + selectMethod + ";";
	}

	private java.sql.Connection getConnection() {
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			con = java.sql.DriverManager.getConnection(getConnectionUrl(),
					userName, password);
			if (con != null) {
				System.out.println("Conexión correcta.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error de seguimiento en getConnection() : "
					+ e.getMessage());
		}
		return con;
	}

	/*
	 * Mostrar las propiedades del controlador y los detalles de la base de
	 * datos
	 */
	public void displayDbProperties() {
		java.sql.DatabaseMetaData dm = null;
		java.sql.ResultSet rs = null;
		try {
			con = this.getConnection();
			if (con != null) {
				dm = con.getMetaData();
				System.out.println("Información del controlador");
				System.out.println("\tNombre del controlador: "
						+ dm.getDriverName());
				System.out.println("\tVersión del controlador: "
						+ dm.getDriverVersion());
				System.out.println("\nInformación de la base de datos ");
				System.out.println("\tNombre de la base de datos: "
						+ dm.getDatabaseProductName());
				System.out.println("\tVersión de la base de datos: "
						+ dm.getDatabaseProductVersion());
				System.out.println("Catálogos disponibles ");
				rs = dm.getCatalogs();
				while (rs.next()) {
					System.out.println("\tcatálogo: " + rs.getString(1));
				}
				rs.close();
				rs = null;
				closeConnection();
			} else {
				System.out.println("Error: No hay ninguna conexión activa");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		dm = null;
	}
	
	public void obtenerDatos() {
		java.sql.DatabaseMetaData dm = null;
		java.sql.ResultSet rs = null;
		java.sql.Statement stm  = null;
		
		String hola = "111";
		Integer cantHola = 10;
		
		System.out.println("lalala:"+paddingZeroToNumber(1000000,10));
		
		System.out.println(validaTamano(hola,cantHola));
		
		try {
			con = this.getConnection();
			if (con != null) {
				String sql = "SELECT  CONVERT(VARCHAR(10), FECHA_TRANSFERENCIA, 105) AS FECHA_TRANSFERENCIA,COD_AUTORIZACION,NUMERO_TRX,NUMERO_TARJETA,MONTO,ESTADO,NUM_CANAL FROM dbo.CANAL WHERE FECHA_REGISTRO =  CONVERT(date,getdate()-1)";
				stm = con.createStatement();
				rs = stm.executeQuery(sql);
				
				while (rs.next()) {
					System.out.println("\fecha: " + rs.getString(1));
					System.out.println("\tautoriza: " + rs.getString(2));
					System.out.println("\tnumero: " + rs.getString(3));
					System.out.println("\ttarjeta: " + rs.getString(4));
					System.out.println("\tmonto: " + rs.getString(5));
					System.out.println("\testado: " + rs.getString(6));
					System.out.println("\tcanal: " + rs.getString(7));
				}
				rs.close();
				rs = null;
				closeConnection();
			} else {
				System.out.println("Error: No hay ninguna conexión activa");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		dm = null;
	}

	private void closeConnection() {
		try {
			if (con != null) {
				con.close();
			}
			con = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws Exception {
		test myDbTest = new test();
		myDbTest.obtenerDatos();
	}
	
	public static String getWhitesSpaceString(Integer numOfspaces) {
		StringBuilder str = new StringBuilder();
		for (int i = 0; i < numOfspaces; i++) {
			str.append(ConstantesUtil.WHITE.toString());
		}

		return str.toString();
	}
	
	public static String validaTamano(String valor, Integer canValor){
		StringBuilder str = new StringBuilder();
		
		if(valor.length() < canValor){
			Integer resta = canValor - valor.length() ;
			str.append(valor).append(getWhitesSpaceString(resta));
			
		}else if(valor.length() > canValor){
			str.append(valor.substring(0, canValor));
		}
		return str.toString();
	}
	
	public static String paddingZeroToNumber(Integer number, Integer totalLength) {

		return String.format("%0" + totalLength + "d", number);
	}
}