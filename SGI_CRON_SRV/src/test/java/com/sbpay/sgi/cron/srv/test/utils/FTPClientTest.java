package com.sbpay.sgi.cron.srv.test.utils;

import junit.framework.TestCase;

import com.sbpay.sgi.cron.dao.CommonsDao;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 16/12/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class FTPClientTest extends TestCase {
    
    private CommonsDao commonsDAO;
    
//    /**
//     * 
//     * 
//     * <p>
//     * Registro de versiones:
//     * <ul>
//     * <li>1.0 XX/YY/2015, (ACL) - versión inicial
//     * </ul>
//     * <p>
//     * 
//     * @throws Exception
//     * @see junit.framework.TestCase#setUp()
//     * @since 1.X
//     */
//    protected void setUp() throws Exception {
//        super.setUp();
//        
//    }
//    
//    protected void tearDown() throws Exception {
//        super.tearDown();
//    }
//    
//    /**
//     * 
//     * 
//     * <p>
//     * Registro de versiones:
//     * <ul>
//     * <li>1.0 16/12/2015, (ACL-sbpay) - versión inicial
//     * </ul>
//     * <p>
//     * 
//     * @throws AppException
//     * 
//     * @since 1.X
//     */
//    public void _testFTPClientUpload() {
//        DataFTP dataFTP;
//        try {
//            dataFTP = getDataFTP();
//            if ( ClientFTPSrv.getInstance().uploadFileFTP( dataFTP,
//                    "C:/", "/ABC_DIN_TRANSBANK/FILENAME.dat", "CASA.TXT" ) ) {
//                System.out.println( "Se ha subido correctamente archivo" );
//            }
//            else {
//                System.out.println( "No se ha subido correctamente archivo" );
//            }
//        }
//        catch ( AppException e ) {
//            System.out.println( e.getStackTraceDescripcion() );
//        }
//    }
//    
//    /**
//     * <
//     * 
//     * <p>
//     * Registro de versiones:
//     * <ul>
//     * <li>1.0 17/12/2015, (ACL-sbpay) - versión inicial
//     * </ul>
//     * <p>
//     * 
//     * @since 1.X
//     */
//    public void _testFTPClientDownload() {
//        DataFTP dataFTP;
//        try {
//            dataFTP = getDataFTP();
//            String origenFTP = "/ABC_DIN_TRANSBANK/archivos.dat";
//            String destinoFTP = "C:/ArchivoDescargado.dat";
//            if ( ClientFTPSrv.getInstance().downloadFileFTP( dataFTP,
//                    origenFTP, destinoFTP ,"") ) {
//                System.out.println( "Se ha descargado correctamente archivo" );
//            }
//            else {
//                System.out
//                        .println( "No se ha descargado correctamente archivo" );
//            }
//        }
//        catch ( AppException e ) {
//            System.out.println( e );
//        }
//    }
//    
//    /**
//     * 
//     * 
//     * <p>
//     * Registro de versiones:
//     * <ul>
//     * <li>1.0 21/12/2015, (ACL-sbpay) - versión inicial
//     * </ul>
//     * <p>
//     * 
//     * @since 1.X
//     */
//    public void testFTPListOfFiles() {
//        DataFTP dataFTP;
//        try {
//            dataFTP = getDataFTP();
//            String folder = "/ic_salida/";
//            String starWith = "RCHP49";
//            String endsWith = "dat";
//            String folderDestination = "C:/PATH_CRON/RCH/";
//            try {
//                List<String> list = ClientFTPSrv.getInstance()
//                        .listFileSFTP( dataFTP, folder );
//                
//                // FILTRA POR NOMBRES
//                List<String> filenamesToDownloadList = retrieveValidFilenameListToDownload(
//                        list, starWith, endsWith );
//                
//                // VALIDAR FORMATOS NOMBRES ARCHIVOS
//                List<String> listToDownload = retrieveFilenameListToDownload(
//                        filenamesToDownloadList, starWith );
//                
//                if ( listToDownload != null && listToDownload.size() > 0 ) {
//                    // VALIDAR QUE NO SE HAYAN DESCARGADO ANTES
//                    listToDownload = currentlyNotDownloadedFiles(
//                            listToDownload, starWith );
//                    
//                    // SE DESCARGAN LOS ARCHIVOS
//                    for ( String filename : listToDownload ) {
//                        // DESCARGAR
//                        System.out.println( "DESCARGA ARCHIVO " );
//                        ClientFTPSrv.getInstance().downloadFileFTP( dataFTP,
//                                folder.concat( filename ),
//                                folderDestination.concat( filename ) ,"");
//                        System.out.println( "GRABA REGISTRO" );
//                        // INSERTA EN LA TBL_LOG_RCH_INC
//                        save( filename, starWith );
//                    }
//                    
//                }
//                else {
//                    System.out
//                            .println( "No existe ningun archivo de rechazo para descargar" );
//                }
//                
//            }
//            catch ( SQLException e ) {
//                System.out.println( e );
//                e.printStackTrace();
//            }
//        }
//        catch ( AppException e ) {
//            System.out.println( e );
//        }
//    }
//    
//    /**
//     * 
//     * 
//     * <p>
//     * Registro de versiones:
//     * <ul>
//     * <li>1.0 21/12/2015, (ACL-sbpay) - versión inicial
//     * </ul>
//     * <p>
//     * 
//     * @param filename
//     * @param starWith
//     * @throws SQLException
//     * @since 1.X
//     */
//    private void save( String filename, String starWith ) throws SQLException {
//        // INICIALIZA EL DAO
//        CommonsDao commonsDAO = new CommonsDaoJdbc(
//                TestConnection.getConnection() );
//        TblCronLogDTO cronLog = new TblCronLogDTO();
//        cronLog.setFecha( DateUtils.getDateFileTbk( DateUtils
//                .getDateStringFromFilename( filename, starWith ) ) );
//        cronLog.setFilename( filename );
//        System.out.println( "status :  "
//                + StatusProcessType.PROCESS_PENDING.getValue() );
//        cronLog.setFileFlag( StatusProcessType.PROCESS_PENDING.getValue() );
//        
//        commonsDAO.saveTblCronLog( cronLog, LogBD.TABLE_LOG_RECHAZO );
//        commonsDAO.endTx();
//    }
//    
//    /**
//     * 
//     * 
//     * <p>
//     * Registro de versiones:
//     * <ul>
//     * <li>1.0 21/12/2015, (ACL-sbpay) - versión inicial
//     * </ul>
//     * <p>
//     * 
//     * @param listToDownload
//     * @return
//     * @throws SQLException
//     * @since 1.X
//     */
//    private List<String> currentlyNotDownloadedFiles(
//            List<String> listToDownload, String starWith ) throws SQLException {
//        CommonsDao commonsDAO = new CommonsDaoJdbc(
//                TestConnection.getConnection() );
//        List<String> finalList = new ArrayList<String>();
//        TblCronLogDTO dto = new TblCronLogDTO();
//        
//        for ( String filename : listToDownload ) {
//            String fecha = DateUtils.getDateFileTbk( DateUtils
//                    .getDateStringFromFilename( filename, starWith ) );
//            dto.setFecha( fecha );
//            System.out.println( "fecha :  " + dto.getFecha() );
//            TblCronLogDTO log = commonsDAO.getTblCronLogByType( dto,
//                    LogBD.TABLE_LOG_RECHAZO.getTable(),
//                    FilterTypeSearch.FILTER_BY_DATE );
//            if ( log == null ) {
//                System.out.println( "se agrega: " + filename );
//                finalList.add( filename );
//            }
//        }
//        
//        return finalList;
//    }
//    
//    /**
//     * 
//     * 
//     * <p>
//     * Registro de versiones:
//     * <ul>
//     * <li>1.0 21/12/2015, (ACL-sbpay) - versión inicial
//     * </ul>
//     * <p>
//     * filtra por nombres
//     * 
//     * @param listAllFiles
//     * @param starWith
//     * @param endWith
//     * @return
//     * @since 1.X
//     */
//    private List<String> retrieveValidFilenameListToDownload(
//            List<String> listAllFiles, String starWith, String endWith ) {
//        
//        List<String> filenamesToDownloadList = new ArrayList<String>();
//        for ( String filename : listAllFiles ) {
//            System.out.println( "filename :" + filename );
//            if ( CommonsUtils.validateFilename( filename, starWith, endWith ) ) {
//                filenamesToDownloadList.add( filename );
//            }
//        }
//        
//        return filenamesToDownloadList;
//        
//    }
//    
//    /**
//     * 
//     * 
//     * <p>
//     * Registro de versiones:
//     * <ul>
//     * <li>1.0 21/12/2015, (ACL-sbpay) - versión inicial
//     * </ul>
//     * <p>
//     * 
//     * @param listToDownload
//     * @return
//     * @throws SQLException
//     * @since 1.X
//     */
//    private List<String> retrieveFilenameListToDownload( List<String> listToDownload,
//            String starWith ) throws SQLException {
//        
//        commonsDAO = new CommonsDaoJdbc( TestConnection.getConnection() );
//        List<String> finalList = new ArrayList<String>();
//        TblCronLogDTO dto = new TblCronLogDTO();
//        
//        for ( String filename : listToDownload ) {
//            String fecha = DateUtils.getDateFileTbk( DateUtils
//                    .getDateStringFromFilename( filename, starWith ) );
//            dto.setFecha( fecha );
//            System.out.println( "fecha :  " + dto.getFecha() );
//            TblCronLogDTO log = commonsDAO.getTblCronLogByType( dto,
//                    LogBD.TABLE_LOG_RECHAZO.getTable(),
//                    FilterTypeSearch.FILTER_BY_DATE );
//            if ( log == null ) {
//                System.out.println( "se agrega: " + filename );
//                finalList.add( filename );
//            }
//        }
//        
//        return finalList;
//    }
//    
////    /**
////     * 
////     * 
////     * <p>
////     * Registro de versiones:
////     * <ul>
////     * <li>1.0 16/12/2015, (ACL-sbpay) - versión inicial
////     * </ul>
////     * <p>
////     * 
////     * @return
////     * @throws AppException
////     * @since 1.X
////     */
////    private DataFTP getDataFTP() throws AppException {
////        List<ParametroDTO> paramDTOList = null;
////        DataFTP dataFTP = null;
////        try {
////            commonsDAO = new CommonsDaoJdbc( TestConnection.getConnection() );
////            paramDTOList = commonsDAO
////                    .getParametroCodGrupoDato( ConstantesPRTS.CRON_TBK
////                            .toString() );
////            if ( paramDTOList != null ) {
////                dataFTP = new DataFTP();
////                dataFTP.setIpHost( CommonsUtils.getCodiDato( paramDTOList,
////                        ConstantesPRTS.FTP_IP.toString() ) );
////                dataFTP.setPort( Integer.parseInt( CommonsUtils.getCodiDato(
////                        paramDTOList, ConstantesPRTS.FTP_PORT.toString() ) ) );
////                dataFTP.setUser( CommonsUtils.getCodiDato( paramDTOList,
////                        ConstantesPRTS.FTP_USER.toString() ) );
////                dataFTP.setPassword( CommonsUtils.getCodiDato( paramDTOList,
////                        ConstantesPRTS.FTP_PASS.toString() ) );
////            }
////            else {
////                dataFTP = null;
////            }
////            
////            System.out.println( "DATOS FTP :" + dataFTP );
////            return dataFTP;
////        }
////        catch ( SQLException e ) {
////            throw new AppException(
////                    MsgErrorSQL.ERROR_QUERY_PRTS_UPLOAD.toString(), e );
////        }
////        finally {
////            try {
////                if ( commonsDAO != null ) {
////                    commonsDAO.close();
////                }
////            }
////            catch ( SQLException e ) {
////                throw new AppException(
////                        MsgErrorSQL.ERROR_QUERY_PRTS_UPLOAD_CLOSE.toString(), e );
////            }
////        }
////    }
    
}
