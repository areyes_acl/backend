package com.sbpay.sgi.cron.srv.test;

import junit.framework.TestCase;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.enums.MsgDownloadRchProcess;
import com.sbpay.sgi.cron.enums.MsgErrorMail;
import com.sbpay.sgi.cron.enums.MsgExitoMail;
import com.sbpay.sgi.cron.srv.download.DownLoadCPagoSrv;
import com.sbpay.sgi.cron.srv.download.DownLoadComisionesSrv;
import com.sbpay.sgi.cron.srv.download.DownLoadLogAutorizacionSrv;
import com.sbpay.sgi.cron.srv.download.DownLoadOnusSrv;
import com.sbpay.sgi.cron.srv.download.DownLoadOutgoingTbkSrv;
import com.sbpay.sgi.cron.srv.download.DownLoadOutgoingVisaSrv;
import com.sbpay.sgi.cron.srv.download.DownloadBOLSrv;
import com.sbpay.sgi.cron.srv.download.DownloadRechazosSrv;
import com.sbpay.sgi.cron.srv.export.GenerarCargoAbonoSrv;
import com.sbpay.sgi.cron.srv.export.GenerarContableSrv;
import com.sbpay.sgi.cron.srv.export.GenerarIncomingOnUsSrv;
import com.sbpay.sgi.cron.srv.export.GenerarIncomingSrv;
import com.sbpay.sgi.cron.srv.export.GenerarIncomingVisaSrv;
import com.sbpay.sgi.cron.srv.process.ProcesarComisionesSrv;
import com.sbpay.sgi.cron.srv.process.ProcesarCompscnsPagoSrv;
import com.sbpay.sgi.cron.srv.process.ProcesarLogTrxAutorizadasSrv;
import com.sbpay.sgi.cron.srv.process.ProcesarOnUsSrv;
import com.sbpay.sgi.cron.srv.process.ProcesarOutgoingSrv;
import com.sbpay.sgi.cron.srv.process.ProcesarOutgoingVisaSrv;
import com.sbpay.sgi.cron.srv.process.ProcesarRechazosTbkSrv;
import com.sbpay.sgi.cron.srv.upload.UploadAsientosCntblSrv;
import com.sbpay.sgi.cron.srv.upload.UploadBolSrv;
import com.sbpay.sgi.cron.srv.upload.UploadCargoAbonoSrv;
import com.sbpay.sgi.cron.srv.upload.UploadICOutgoingTbkSrv;
import com.sbpay.sgi.cron.srv.upload.UploadICOutgoingVisaSrv;
import com.sbpay.sgi.cron.srv.upload.UploadIncomingSrv;
import com.sbpay.sgi.cron.srv.upload.UploadOnusTbkSrv;
import com.sbpay.sgi.cron.srv.upload.UploadVisaSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 24/02/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * Clase de prueba para la descarga de archivos incoming y control
 * desde transbank
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class JobsTest extends TestCase {
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Test
     * 
     * 
     * @since 1.X
     */
    public void _testDescargarOutgoindDesdeTransbank() {
        
        try {
            DownLoadOutgoingTbkSrv.getInstance().descargarOutgoing();
        }
        catch ( AppException e ) {
            e.printStackTrace();
        }
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Test
     * 
     * 
     * @since 1.X
     */
    public void _testDescargaComisiones() {
        
        try {
            DownLoadComisionesSrv.getInstance().descargarComisiones();
        }
        catch ( AppException e ) {
            e.printStackTrace();
        }
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Test
     * 
     * 
     * @since 1.X
     */
    public void _testDescargaCPago() {
        
        try {
            DownLoadCPagoSrv.getInstance().descargarArchivoCPago();
        }
        catch ( AppException e ) {
            MailSendSrv.getInstance().sendMail( e.getMessage(),
                    e.getStackTraceDescripcion() );
            e.printStackTrace();
        }
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Test
     * 
     * 
     * @since 1.X
     */
    public void _testDescargaOutgoingVisa() {
        
        try {
            if ( DownLoadOutgoingVisaSrv.getInstance()
                    .descargarArchivoVisaInt() ) {
              System.out.println("TODO OK ");
            }
        }
        catch ( AppException e ) {
          System.out.println("ERROR");
            e.printStackTrace();
        }
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Test
     * 
     * 
     * @since 1.X
     */
    public void _testSubirOutgoingNacionalAIC() {
        
        try {
            if ( UploadICOutgoingTbkSrv.getInstance().uploadOutgoingIC() ) {
                MailSendSrv.getInstance().sendMailOk(
                        MsgExitoMail.EXITO_PROCESS_UPLOAD_IC_TITLE.toString(),
                        MsgExitoMail.EXITO_PROCESS_UPLOAD_IC_BODY.toString() );
            }
        }
        catch ( AppException e ) {
            MailSendSrv.getInstance().sendMail( e.getMessage(),
                    e.getStackTraceDescripcion() );
            e.printStackTrace();
        }
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Test
     * 
     * 
     * @since 1.X
     */
    public void _testSubirOutgoingVisaASFTPIC() {
        
        try {
            if ( UploadICOutgoingVisaSrv.getInstance().upload() ) {
                System.out
                        .println( "Envio Correo de Upload  Satisfactorio: Proceso Upload Outgoing Visa A FTP IC" );
                MailSendSrv
                        .getInstance()
                        .sendMailOk(
                                MsgExitoMail.EXITO_PROCESS_UPLOAD_OUT_VISA_IC_TITLE
                                        .toString(),
                                MsgExitoMail.EXITO_PROCESS_UPLOAD_OUT_VISA_BODY
                                        .toString() );
                
            }
            else {
                System.out
                        .println( MsgErrorMail.ERROR_PROCESS_UPLOAD_OUT_VISA_BODY
                                .toString() );
                
            }
        }
        catch ( AppException e ) {
            MailSendSrv.getInstance().sendMail( e.getMessage(),
                    e.getStackTraceDescripcion() );
            e.printStackTrace();
        }
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Test
     * 
     * 
     * @since 1.X
     */
    public void _testDescargarRechazosDeIC() throws Exception {
        
        try {
            if ( DownloadRechazosSrv.getInstance().descargarRechazos() ) {
                System.out
                        .println( "======>El proceso (StepDownloadRechazos) de descarga de rechazos ha finalizado correctamente<=====" );
            }
            else {
                System.out
                        .println( "======> Ha concluido el proceso de descarga de rechazos con observaciones (ver logs)!" );
            }
        }
        catch ( AppException e ) {
            e.printStackTrace();
            MailSendSrv
                    .getInstance()
                    .sendMail(
                            MsgDownloadRchProcess.ERROR_MAIL_TITLE_PROCESS_DOWNLOAD
                                    .toString(), e.getStackTraceDescripcion() );
        }
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 12/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws Exception
     * @since 1.X
     */
    public void _testDescargarOnusIC() throws Exception {
        
        try {
            System.out.println( "Inicio Proceso:  Download Onus" );
            if ( DownLoadOnusSrv.getInstance().descargarOnus() ) {
                System.out
                        .println( "Envio Correo de Descarga Satisfactoria: Proceso DownLoad Onus" );
                MailSendSrv
                        .getInstance()
                        .sendMailOk(
                                MsgExitoMail.EXITO_PROCESS_DOWNLOAD_ONUS_IC_TITLE
                                        .toString(),
                                MsgExitoMail.EXITO_PROCESS_DOWNLOAD_ONUS_IC_BODY
                                        .toString() );
            }
            else {
                System.out
                        .println( "Finaliza Proceso :DownLoad Onus, no se realiza" );
                
            }
            
        }
        catch ( AppException e ) {
            MailSendSrv.getInstance().sendMail( e.getMessage(),
                    e.getStackTraceDescripcion() );
            e.printStackTrace();
        }
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 12/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws Exception
     * @since 1.X
     */
    public void _testDescargarLOGTRXIC() throws Exception {
        
        try {
            System.out.println( "Inicio Proceso:  Download Log Autorizacion" );
            if ( DownLoadLogAutorizacionSrv.getInstance()
                    .descargarLogAutorizacion() ) {
                System.out
                        .println( "Envio Correo de Descarga Satisfactoria: Proceso DownLoad Log Autorizacion" );
                MailSendSrv
                        .getInstance()
                        .sendMailOk(
                                MsgExitoMail.EXITO_PROCESS_DOWNLOAD_TITLE_LOG_AUTORIZACION
                                        .toString(),
                                MsgExitoMail.EXITO_PROCESS_DOWNLOAD_LOG_AUTORIZACION
                                        .toString() );
            }
            else {
                System.out
                        .println( "Finaliza Proceso :Descargar Log de Autorizacion de trx, no se realiza" );
                
            }
            
        }
        catch ( AppException e ) {
            e.printStackTrace();
            MailSendSrv.getInstance().sendMail( e.getMessage(),
                    e.getStackTraceDescripcion() );
        }
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 12/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws Exception
     * @since 1.X
     */
    public void _testProcessLogAutTrx() throws Exception {
        try {
            System.out
                    .println( "===>Inicio procesamiento LOG TRX : ProcesarLogTrxAutorizadasSrv<===" );
            ProcesarLogTrxAutorizadasSrv.getInstance()
                    .procesarLogTrxAutorizadas();
            
        }
        catch ( AppException e ) {
            e.printStackTrace();
            MailSendSrv.getInstance().sendMail( e.getMessage(),
                    e.getStackTraceDescripcion() );
            throw new Exception( e.getMessage(), e );
        }
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 12/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws Exception
     * @since 1.X
     */
    public void _testProcesarOutgoing() throws Exception {
        try {
            System.out
            
                    .println( "===>Inicio procesamiento Archivo Incoming Transbank : StepProcessIncoming<===" );
            ProcesarOutgoingSrv.getInstance().procesarArchivo();
            System.out
                    .println( "===>Fin procesamiento Archivo Incoming Transbank : StepProcessIncoming<===" );
        }
        catch ( AppException e ) {
            e.printStackTrace();
            MailSendSrv.getInstance().sendMail( e.getMessage(),
                    e.getStackTraceDescripcion() );
            throw new Exception( e.getMessage(), e );
        }
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 12/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws Exception
     * @since 1.X
     */
    public void _testProcesarOutgoingVISA() throws Exception {
        try {
            System.out
                    .println( "===>Inicio procesamiento Archivo Incoming Transbank : PROCESAR OUTGOING VISA<===" );
            ProcesarOutgoingVisaSrv.getInstance().procesar();
            
        }
        catch ( AppException e ) {
            e.printStackTrace();
            MailSendSrv.getInstance().sendMail( e.getMessage(),
                    e.getStackTraceDescripcion() );
            throw new Exception( e.getMessage(), e );
        }
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 12/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws Exception
     * @since 1.X
     */
    public void _testProcesarRechazos() throws Exception {
        try {
            System.out
                    .println( "===>Inicio procesamiento Archivo Incoming Transbank : PROCESAR OUTGOING VISA<===" );
            ProcesarRechazosTbkSrv.getInstance().procesarRechazos();
            
        }
        catch ( AppException e ) {
            e.printStackTrace();
            MailSendSrv.getInstance().sendMail( e.getMessage(),
                    e.getStackTraceDescripcion() );
            throw new Exception( e.getMessage(), e );
        }
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 12/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws Exception
     * @since 1.X
     */
    public void _testProcesarOnUs() throws Exception {
        try {
            System.out
                    .println( "===>Inicio procesamiento Archivo Incoming Transbank : PROCESAR OUTGOING VISA<===" );
            ProcesarOnUsSrv.getInstance().processOnus();
            
        }
        catch ( AppException e ) {
            e.printStackTrace();
            MailSendSrv.getInstance().sendMail( e.getMessage(),
                    e.getStackTraceDescripcion() );
            throw new Exception( e.getMessage(), e );
        }
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 12/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws Exception
     * @since 1.X
     */
    public void _testProcesarComisiones() throws Exception {
        try {
            System.out
                    .println( "===>Inicio Procesamiento comisiones<===" );
            ProcesarComisionesSrv.getInstance().procesarComisiones();
            
        }
        catch ( AppException e ) {
            e.printStackTrace();
            MailSendSrv.getInstance().sendMail( e.getMessage(),
                    e.getStackTraceDescripcion() );
            throw new Exception( e.getMessage(), e );
        }
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 12/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws Exception
     * @since 1.X
     */
    public void _testProcesarCompensacionPago() throws Exception {
        try {
            System.out
                    .println( "===>Inicio procesamiento Archivo Incoming Transbank : testProcesarCompensacionPago<===" );
            ProcesarCompscnsPagoSrv.getInstance().procesarCPagos();
            
        }
        catch ( AppException e ) {
            e.printStackTrace();
            MailSendSrv.getInstance().sendMail( e.getMessage(),
                    e.getStackTraceDescripcion() );
            throw new Exception( e.getMessage(), e );
        }
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 12/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws Exception
     * @since 1.X
     */
    public void _testGenerateIncoming() throws Exception {
        try {
            GenerarIncomingSrv.getInstance().generaArhivoIncoming();
            
        }
        catch ( AppException e ) {
            e.printStackTrace();
            MailSendSrv.getInstance().sendMail( e.getMessage(),
                    e.getStackTraceDescripcion() );
            throw new Exception( e.getMessage(), e );
        }
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 12/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws Exception
     * @since 1.X
     */
    public void _testGenerarIncomingOnUs() throws Exception {
        try {
            System.out
                    .println( "Inicio Proceso de Generar/Exportar Incoming On Us : StepGenerarIncomingOnUs " );
            if ( GenerarIncomingOnUsSrv.getInstance().generarIncomingOnUsTbk() ) {
                System.out
                        .println( "Envio Correo de Generar/Exportar Incoming On Us  Satisfactorio: Proceso Generar Incoming On Us" );
                MailSendSrv.getInstance().sendMailOk(
                        MsgExitoMail.EXITO_PROCESS_INC_ONUS_TITLE.toString(),
                        MsgExitoMail.EXITO_PROCESS_INC_ONUS.toString() );
            }
            else {
                System.out
                        .println( "Generar/Exportar Incoming On Us no realizados, Finaliza! (total o parcialmente consultar log)" );
            }
        }
        catch ( AppException e ) {
            System.out.println( e.getMessage() + e );
            MailSendSrv.getInstance().sendMail( e.getMessage(),
                    e.getStackTraceDescripcion() );
            throw new Exception( e.getMessage(), e );
        }
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 12/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws Exception
     * @since 1.X
     */
    public void _testGenerarIncomingVISA() throws Exception {
        try {
            // LLAMA A LA PRIMERA TAREA
            GenerarIncomingVisaSrv.getInstance().generaArhivoIncomingVisa();
            
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
        
    }
    
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
     * </ul>
     * <p>
     * 
     * @throws Exception
     * @since 1.X
     */
    public void _testGenerarSolicitud60() throws Exception {
        try {
            // LLAMA A LA PRIMERA TAREA
            GenerarCargoAbonoSrv.getInstance().generarCargoAbonoTbk();
            
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
        
    }
    
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 12/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws Exception
     * @since 1.X
     */
    public void _testSubirIngomingATransbank() throws Exception {
        try {
            // LLAMA A LA PRIMERA TAREA
            UploadIncomingSrv.getInstance().uploadIncoming();
            
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 12/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws Exception
     * @since 1.X
     */
    public void _testSubirOnusTBK() throws Exception {
        try {
            // LLAMA A LA PRIMERA TAREA
            UploadOnusTbkSrv.getInstance().uploadOnusTbk();
            
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 12/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws Exception
     * @since 1.X
     */
    public void _testSubirIncomingVisa() throws Exception {
        
        try {
            // LLAMA A LA PRIMERA TAREA
            UploadVisaSrv.getInstance().uploadVisa();
            
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
    }
    
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 12/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws Exception
     * @since 1.X
     */
    public void _testSubirCargoYAbono() throws Exception {
        
        try {
            // LLAMA A LA PRIMERA TAREA
            UploadCargoAbonoSrv.getInstance().uploadCargoAbono();
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
    }
    
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
     * </ul>
     * <p>
     * 
     * @throws Exception
     * @since 1.X
     */
    public void _testGenerarAsientosContables() throws Exception{
        try {
            // LLAMA A LA PRIMERA TAREA
            GenerarContableSrv.getInstance().generarArchivoContable();
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 12/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws Exception
     * @since 1.X
     */
    public void _testSubirAsientosContables() throws Exception {
        
        try {
            // LLAMA A LA PRIMERA TAREA
            UploadAsientosCntblSrv.getInstance().uploadAsientosContables();
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
    }
    
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 12/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws Exception
     * @since 1.X
     */
    public void _testSubirArchivoBOL() throws Exception {
        
        try {
            // LLAMA A LA PRIMERA TAREA
            UploadBolSrv.getInstance().uploadBol();
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
    }
    
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 12/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws Exception
     * @since 1.X
     */
    public void _testDownloadBol() throws Exception {
        
        try {
           DownloadBOLSrv.getInstance().descargarBOL();
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
    }
    
}
