package com.sbpay.sgi.cron.srv.test.download;

import junit.framework.TestCase;

import com.sbpay.sgi.cron.srv.download.DownLoadCPagoSrv;

/**
 * 
 * @author afreire
 * 
 *         Test para probar el servicio realizado para la descarga del archivo
 *         NCL desde FTP Configurado en los parametros
 * 
 *
 * lista a procesar
 * [NCL5ABCD.211117, NCL5ABCD.141117, NCL5ABCD.201117, NCL5ABCD.151117, NCL5ABCD.161117, NCL5ABCD.221117, NCL5ABCD.131117, NCL5ABCD.171117]
 *
 */
public class DescargarNCLTest extends TestCase{

	/**
	 * 
	 * @throws Exception
	 */
	public void testProcessLogAutTrx() throws Exception {
		
		DownLoadCPagoSrv.getInstance().descargarArchivoCPago();
	}

	

}
