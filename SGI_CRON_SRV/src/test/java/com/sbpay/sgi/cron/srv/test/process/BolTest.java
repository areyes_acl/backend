package com.sbpay.sgi.cron.srv.test.process;

import junit.framework.TestCase;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.srv.upload.UploadBolSrv;
import com.sbpay.sgi.cron.step.StepDownloadBOL;

public class BolTest extends TestCase {

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 12/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws Exception
     * @since 1.X
     */
    public void _testDownloadBOL() throws Exception {

	System.out.println("Inicio proceso : StepDownloadBOL ");

	StepDownloadBOL step = new StepDownloadBOL();
	step.execute();
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws Exception
     * @since 1.X
     */
    public void testUpload() throws Exception {

	System.out.println("Inicio proceso : StepDownloadBOL ");
	UploadBolSrv.getInstance().uploadBol();
    }
}
