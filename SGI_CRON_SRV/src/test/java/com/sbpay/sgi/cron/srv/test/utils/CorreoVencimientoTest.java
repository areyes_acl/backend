package com.sbpay.sgi.cron.srv.test.utils;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import junit.framework.TestCase;
import oracle.jdbc.pool.OracleDataSource;

import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoJdbc;
import com.sbpay.sgi.cron.dao.GenerarCorreoVencDAO;
import com.sbpay.sgi.cron.dao.GenerarCorreoVencDAOJdbc;
import com.sbpay.sgi.cron.dto.CompensacionPagoDTO;
import com.sbpay.sgi.cron.srv.download.DownLoadOutgoingVisaSrv;
import com.sbpay.sgi.cron.srv.process.ProcesarCompscnsPagoSrv;
import com.sbpay.sgi.cron.srv.upload.UploadOnusTbkSrv;

public class CorreoVencimientoTest extends TestCase {

	private GenerarCorreoVencDAO correoVencDAO;
	@SuppressWarnings("unused")
	private CommonsDao commonsDAO;

	/** The Constant LOGGER. */
	OracleDataSource oracleDS = null;

	protected void setUp() throws Exception {
		super.setUp();

		// Configuración del DataSource
		String url = "jdbc:oracle:thin:@127.0.0.1:1521:xe";
		String user = "sig";
		String password = "sig";
		try {
			oracleDS = new OracleDataSource();
			oracleDS.setURL(url);
			oracleDS.setUser(user);
			oracleDS.setPassword(password);
			commonsDAO = new CommonsDaoJdbc(oracleDS.getConnection());
			correoVencDAO = new GenerarCorreoVencDAOJdbc(
					oracleDS.getConnection());
			System.out.println();
		} catch (SQLException e) {
			System.out.println(e);
		}
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void _testConeccionConBD() {
		Connection conn = null;
		System.out.println("Conectando con la BD ...");
		try {
			System.out.println("Conectando...");
			// obtenemos la conexion desde el Objeto OracleDataSource.
			conn = oracleDS.getConnection();
			System.out.println("Conectado...");
			// Cerrar la conexión
			conn.close();
		} catch (Exception e) {
			System.out.println("No se pudo conectar");
			e.printStackTrace();
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
	}

	public void testLeerArchivo() {
		try {
			UploadOnusTbkSrv.getInstance().uploadOnusTbk();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
