package com.sbpay.sgi.cron.srv.test.process;

import junit.framework.TestCase;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.enums.MsgExitoMail;
import com.sbpay.sgi.cron.srv.download.DownLoadOnusSrv;
import com.sbpay.sgi.cron.srv.process.ProcesarOnUsSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;

/**
 * 
 * @author afreire Test para probar el flujo del archivo log de trx autorizadas
 *
 */
public class OnusTest extends TestCase {

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 12/1/2016, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @throws Exception
	 * @since 1.X
	 */
	public void _testDescargarOnusIC() throws Exception {

		try {
			System.out.println("Inicio Proceso:  Download Onus");
			if (DownLoadOnusSrv.getInstance().descargarOnus()) {
				System.out
						.println("Envio Correo de Descarga Satisfactoria: Proceso DownLoad Onus");
				MailSendSrv
						.getInstance()
						.sendMailOk(
								MsgExitoMail.EXITO_PROCESS_DOWNLOAD_ONUS_IC_TITLE
										.toString(),
								MsgExitoMail.EXITO_PROCESS_DOWNLOAD_ONUS_IC_BODY
										.toString());
			} else {
				System.out
						.println("Finaliza Proceso :DownLoad Onus, no se realiza");

			}

		} catch (AppException e) {
			MailSendSrv.getInstance().sendMail(e.getMessage(),
					e.getStackTraceDescripcion());
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 12/1/2016, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @throws Exception
	 * @since 1.X
	 */
	public void testProcesarOnUs() throws Exception {
		try {
			System.out
					.println("===>Inicio procesamiento Archivo Incoming Transbank : PROCESAR OUTGOING VISA<===");
			ProcesarOnUsSrv.getInstance().processOnus();

		} catch (AppException e) {
			e.printStackTrace();
			MailSendSrv.getInstance().sendMail(e.getMessage(),
					e.getStackTraceDescripcion());
			throw new Exception(e.getMessage(), e);
		}

	}

}
