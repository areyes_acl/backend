package com.sbpay.sgi.cron.srv.test.process;

import junit.framework.TestCase;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.enums.MsgExitoMail;
import com.sbpay.sgi.cron.srv.download.DownLoadLogAutorizacionSrv;
import com.sbpay.sgi.cron.srv.process.ProcesarLogTrxAutorizadasSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;

/**
 * 
 * @author afreire Test para probar el flujo del archivo log de trx autorizadas
 *
 */
public class LogTrxAutorizadasTest extends TestCase {

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 12/1/2016, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * Test que realiza la prueba de la descarga del archivo de transacciones
	 * autorizadas desde el sftp
	 * 
	 * @throws Exception
	 * @since 1.X
	 */
	public void _testDescargarLogTransaccionesAutorizadas() throws Exception {

		try {
			System.out.println("Inicio Proceso:  Download Log Autorizacion");
			if (DownLoadLogAutorizacionSrv.getInstance()
					.descargarLogAutorizacion()) {
				System.out
						.println("Envio Correo de Descarga Satisfactoria: Proceso DownLoad Log Autorizacion");
				MailSendSrv
						.getInstance()
						.sendMailOk(
								MsgExitoMail.EXITO_PROCESS_DOWNLOAD_TITLE_LOG_AUTORIZACION
										.toString(),
								MsgExitoMail.EXITO_PROCESS_DOWNLOAD_LOG_AUTORIZACION
										.toString());
			} else {
				System.out
						.println("Finaliza Proceso :Descargar Log de Autorizacion de trx, no se realiza");

			}

		} catch (AppException e) {
			e.printStackTrace();
			MailSendSrv.getInstance().sendMail(e.getMessage(),
					e.getStackTraceDescripcion());
		}

	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 12/1/2016, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que prueba el procesamiento del archivo del log de transacciones
	 * autorizadas
	 * 
	 * @throws Exception
	 * @since 1.X
	 */
	public void testProcessLogAutTrx() throws Exception {
		try {
			System.out
					.println("===>Inicio procesamiento LOG TRX : ProcesarLogTrxAutorizadasSrv<===");
			ProcesarLogTrxAutorizadasSrv.getInstance()
					.procesarLogTrxAutorizadas();

		} catch (AppException e) {
			e.printStackTrace();
			MailSendSrv.getInstance().sendMail(e.getMessage(),
					e.getStackTraceDescripcion());
			throw new Exception(e.getMessage(), e);
		}

	}

}
