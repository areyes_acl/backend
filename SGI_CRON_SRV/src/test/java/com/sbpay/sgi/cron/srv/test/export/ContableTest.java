package com.sbpay.sgi.cron.srv.test.export;

import junit.framework.TestCase;

import com.sbpay.sgi.cron.srv.export.GenerarContableSrv;


/**
 * 
 * @author afreire Test para probar el flujo del archivo log de trx autorizadas
 *
 */
public class ContableTest extends TestCase {
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @throws Exception
	 * @since 1.X
	 */
	public void testGenerarAsientosContables() throws Exception {
		try {
			// LLAMA A LA PRIMERA TAREA
			GenerarContableSrv.getInstance().generarArchivoContable();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
