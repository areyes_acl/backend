import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;


public class prueba_outgoing_to_ic {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		File dir = null;
		File[] ficheros = null;
		List<String> fileDirectory = null;
		String startWith ="VISA_INT_PRD_";
		String extFile =".C.IN";
		String startWithToIc ="VISAI";
		String extFiletoIc ="0.TXT";
		dir = new File("C:\\PATH_CRON\\INCOMING");
		String pathSearch = "C:\\PATH_CRON\\INCOMING\\";
		//VISA_INT_PRD_YYYYMMDD.C.IN VISAIMMDDC.TXT
		//VISA_DOM_PRD_YYYYMMDD.C.IN VISADMMDDC.TXT
		ficheros = dir.listFiles();
		if (ficheros != null && ficheros.length > 0) {
			fileDirectory = new ArrayList<String>();
			for (File file : ficheros) {
				if (file != null && file.isFile()
						&& file.getName().endsWith(extFile)
						&& file.getName().startsWith(startWith)) {
					fileDirectory.add(file.getName());			
					String medio = file.getName().replace(startWith, "").substring(4,8);
					String toFile = startWithToIc+medio+extFiletoIc;
					//System.out.println(file.getAbsolutePath()+" "+pathSearch+toFile);
					//System.out.println(medio);
					boolean a = copiarArchivo(file.getAbsolutePath(), pathSearch+toFile);
					System.out.println(a);
					borrarFile(pathSearch+toFile);
					
				}
			}
			if (fileDirectory.size() == 0) {
				fileDirectory = null;
			}else{
				System.out.println(fileDirectory.toString());
			}
		}
	}
	
	public static boolean copiarArchivo(String fromFile, String toFile) {
        File origin = new File(fromFile);
        File destination = new File(toFile);
        if (origin.exists()) {
            try {
                InputStream in = new FileInputStream(origin);
                OutputStream out = new FileOutputStream(destination);
                // We use a buffer for the copy (Usamos un buffer para la copia).
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                in.close();
                out.close();
                return true;
            } catch (IOException ioe) {
                ioe.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
    }
	
	public static void borrarFile (String args)  
	{   
		File f= new File(args);           //file to be delete  
		f.delete();  
	}  

}
