package com.sbpay.sgi.cron.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import oracle.jdbc.OracleCallableStatement;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.dao.BaseDao;
import com.sbpay.sgi.cron.dto.Registro00OutVisaCobroCargoDTO;
import com.sbpay.sgi.cron.dto.Registro00OutVisaIntDTO;
import com.sbpay.sgi.cron.dto.Registro00OutVisaPagoDTO;
import com.sbpay.sgi.cron.dto.Registro01OutVisaIntDTO;
import com.sbpay.sgi.cron.dto.Registro02OutVisaIntDTO;
import com.sbpay.sgi.cron.dto.Registro03OutVisaIntDTO;
import com.sbpay.sgi.cron.dto.Registro04OutVisaIntDTO;
import com.sbpay.sgi.cron.dto.Registro05OutVisaIntDTO;
import com.sbpay.sgi.cron.dto.Registro06OutVisaIntDTO;
import com.sbpay.sgi.cron.dto.Registro0DOutVisaIntDTO;
import com.sbpay.sgi.cron.dto.Registro2DOutVisaIntDTO;
import com.sbpay.sgi.cron.dto.Registro07OutVisaIntDTO;
import com.sbpay.sgi.cron.dto.TransaccionOutVisa;
import com.sbpay.sgi.cron.dto.TransaccionOutVisaCobroCargo;
import com.sbpay.sgi.cron.dto.TransaccionOutVisaPago;
import com.sbpay.sgi.cron.enums.ConstantesBD;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.utils.generals.DateUtils;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 25/04/2019, (ACL) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SA.</B>
 */
public class ProcesarIncomigVisaJdbc extends
		BaseDao<ProcesarIncomingVisaDao> implements
		ProcesarIncomingVisaDao {

	/** VARIABLE PARA EL LOGER */
	private static final Logger LOGGER = Logger
			.getLogger(ProcesarIncomigVisaJdbc.class);

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 25/04/2019, (ACL & sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @param connection
	 * @since 1.X
	 */
	public ProcesarIncomigVisaJdbc(Connection connection) {
		super(connection);

	}
	
	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 25/04/2019, (ACL & sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @param connection
	 * @since 1.X
	 */
	@Override
	public boolean saveTransaction(TransaccionOutVisa transaccion, Integer sidOperador) throws Exception {
		PreparedStatement stmt = null;

		try {
			StringBuilder str = new StringBuilder()
					.append("INSERT INTO ")
					.append(ConstantesBD.ESQUEMA.toString())
					.append(".TMP_INCOMING  ")
					.append(" (SID, "
							+ "MIT, "
							+ "NRO_TARJETA, "
							+ "codigo_procesamiento,")
					.append(" monto_transac, "
							+ "monto_conciliacion, "
							+ "monto_facturacion, "
							+ "tasa_convenio_conciliacion,")
					.append(" tasa_convenio_facturacion, "
							+ "fecha_hr_trasac, "
							+ "fecha_vencimiento_tarjeta, ")
					.append(" cod_datos_punto_servicio , "
							+ "nro_secuencia_tarjeta, "
							+ "codigo_funcion, "
							+ "cod_motivo_mensaje, ")
					.append(" mcc , "
							+ "montos_originales, "
							+ "datos_referencia_adquirente, "
							+ "cod_identificacion_adquirente, ")
					.append(" cod_ident_inst_envia , "
							+ "nro_referencia_recuperacion, "
							+ "codigo_autorizacion, "
							+ "codigo_servicio, ")
					.append(" ident_term_acep_tarjeta , "
							+ "cod_ident_acep_tarjeta, "
							+ "nombre_ubic_acep_tarjeta, "
							+ "datos_adicionales_1, ")
					.append(" cod_moneda_transac , "
							+ "cod_moneda_conciliacion, "
							+ "cod_moneda_facturacion, "
							+ "montos_adicionales, ")
					.append(" icc , "
							+ "datos_adicionales_2, "
							+ "ident_ciclo_duracion_transac, "
							+ "nro_mensaje, ")
					.append(" registro_de_datos , "
							+ "fecha_accion, "
							+ "cod_ident_inst_dest_transac, "
							+ "cod_ident_inst_orig_transac, ")
					.append(" datos_ref_emisor  , "
							+ "cod_ident_inst_recibe, "
							+ "monto_recargo_x_conver_moneda, "
							+ "datos_adicionales_3, ")
					.append(" datos_adicionales_4 , "
							+ "datos_adicionales_5, "
							+ "p0023, "
							+ "p0052, ")
					.append(" p0148 , "
							+ "p0158, "
							+ "p0165, "
							+ "p1000, ")
					.append(" p1001 , "
							+ "p1003, "
							+ "p1004, "
							+ "p1005, ")
					.append(" p1006 , "
							+ "p1007, "
							+ "p1008, "
							+ "p1009, ")
					.append(" p1010 , "
							+ "p1011, "
							+ "p1012, "
							+ "p0005, ")
					.append(" p0025 , "
							+ "p0026, "
							+ "p0044, "
							+ "p0057, ")
					.append(" OPERADOR, "
							+ "datos_adicionales_6, "
							+ "datos_adicionales_7, "
							+ "datos_adicionales_8) ")
					.append(" values (SEQ_TMP_INCOMING.nextval, ?, ?, ? , ")
					.append(" ?, ?, ?, ? ,").append(" ?, ?, ?, ? ,")
					.append(" ?, ?, ?, ? ,").append(" ?, ?, ?, ? ,")
					.append(" ?, ?, ?, ? ,").append(" ?, ?, ?, ? ,")
					.append(" ?, ?, ?, ? ,").append(" ?, ?, ?, ? ,")
					.append(" ?, ?, ?, ? ,").append(" ?, ?, ?, ? ,")
					.append(" ?, ?, ?, ? ,").append(" ?, ?, ?, ? ,")
					.append(" ?, ?, ?, ? ,").append(" ?, ?, ?, ? ,")
					.append(" ?, ?, ?, ? ,").append(" ?, ?, ?, ? ,")
					.append(" ?, ?, ?)");

			this.getConnection().setAutoCommit(false);
			stmt = this.getConnection().prepareStatement(str.toString());

			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getCodigoTransaccion(),
					1, stmt);
			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getNumeroTarjeta(),
					2, stmt);
			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getNumeroReferencia(),
					3, stmt);
			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getMontoFuen(),
					4, stmt);
			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00()).getMontoDest(), // se
																							// modifica
					5, stmt);
			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00()).getMontoDest(), // se
																							// modifica
					6, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 7, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 8, stmt);
			setStringNull((DateUtils.getStringDateWithValidationProcess(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getFechaComp(), DateUtils.FECHA_NORMAL)), 9, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 10, stmt);
			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getPosEntryMode(),
					11, stmt);

			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getNumberExt(),
					12, stmt); // se agrega (21-23) gsetNumberExt

			setStringNull((obtieneCodigoTransaccion(((Registro00OutVisaIntDTO) transaccion .getRegistro00()).getCodRazon(),((Registro00OutVisaIntDTO) transaccion.getRegistro00()).getTransCode())), 13,stmt);
			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getCodRazon(),
					14, stmt);
			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getRubroComer(),
					15, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 16, stmt);
			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getNumeroReferencia(),
					17, stmt);
			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getAcqMembId(),
					18, stmt);
			
			
			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getBinAdquiriente(),
					19, stmt); // REGISTRO01 - 30 //bin adquiriente se extraen 6 primeros digitos a getNumeroReferencia
	
			
			setStringNull(
					((Registro01OutVisaIntDTO) transaccion.getRegistro01())
							.getChargRefN(),
					20, stmt); // REGISTRO01 - 05
			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getCodAutor(),
					21, stmt);
			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getPosTerminalCapability(),
					22, stmt);
			setStringNull(
					((Registro01OutVisaIntDTO) transaccion.getRegistro01())
							.getTerminalId(),
					23, stmt);// REGISTRO01
			// REGISTRO01
			setStringNull(((Registro01OutVisaIntDTO) transaccion.getRegistro01()).getCardAcceptorId(),24, stmt);
			
			setStringNull(((Registro00OutVisaIntDTO) transaccion.getRegistro00()).getNombreUbicAcepTarjeta(),25, stmt);
			
			//DATOS_ADICIONALES_1 - ALL EL REGISTRO 03
			if( transaccion.getRegistro03() != null){
				setStringNull(((Registro03OutVisaIntDTO) transaccion.getRegistro03()).getAll(), 26, stmt);
			}else {
				setStringNull(ConstantesUtil.EMPTY.toString(), 26, stmt);
			}
			
			
			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getMonedaFuen(),
					27, stmt);
			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getMonedaDest(), // se modfica getMonedaDest
					28, stmt);
			
			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getMonedaDest(), // se modfica getMonedaDest
					29, stmt);
			
			setStringNull(ConstantesUtil.EMPTY.toString(), 30, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 31, stmt);
			// REGISTRO 05 COMPLETO
			setStringNull(
					(transaccion.getRegistro05() != null) ? ((Registro05OutVisaIntDTO) transaccion.getRegistro05()).getAll()
							: "", 32, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 33, stmt);
			// CORRELATIVO DE LECTURA EL NRO DE TRANSACCION QUE ESTOY
			// LEYENDO
			setStringNull(String.valueOf(transaccion.getNumeroTransaccion()),
					34, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 35, stmt);
			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getFechaProc(),
					36, stmt);
			
			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getCodIdentDestTransc(), // se modifca para recibir 5 oarametros pata formar separados por slash
					37, stmt);
			
			setStringNull(ConstantesUtil.EMPTY.toString(), 38, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 39, stmt);
			// BIN 4 PRIMEROS DIGITOS de tarjeta
			setStringNull(((Registro00OutVisaIntDTO) transaccion.getRegistro00()).getBinTarjeta(),40, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 41, stmt);
			String allRegister07 = (transaccion.getRegistro07() != null) ? ((Registro07OutVisaIntDTO) transaccion
					.getRegistro07()).getAll() : "";
			// 100 primeros bytes del registros completo 07
			setStringNull( (allRegister07.length() > 100) ? allRegister07.substring(0, 100) : allRegister07, 42, stmt);
			// Restantes 70 bytes del registro 07
			setStringNull((allRegister07.length() > 100) ? allRegister07.substring(100, allRegister07.length()) : "", 43, stmt);
			
			//DATOS_ADICIONALES_5 - ALL REGISTRO 04
			if(transaccion.getRegistro04() != null){
				setStringNull(((Registro04OutVisaIntDTO) transaccion.getRegistro04()).getAll(), 44, stmt);
			}else{
				setStringNull(ConstantesUtil.EMPTY.toString(), 44, stmt);
			}
			
			
			//P0023
			setStringNull(
					((Registro01OutVisaIntDTO) transaccion.getRegistro01())
							.getIndicatorTransaction(),
					45, stmt); 
			
			//P0052
			setStringNull(
					((Registro01OutVisaIntDTO) transaccion.getRegistro01())
							.getDocumInd(),
					46, stmt);
			
			//Procesamiento del registro 02
			if((Registro02OutVisaIntDTO) transaccion.getRegistro02() != null){
				// esta ok para registro 2
				setStringNull(((Registro02OutVisaIntDTO) transaccion.getRegistro02()).getCodigoPais(),47, stmt);
				
			
				// esta ok para registro 2
				setStringNull(
						((Registro02OutVisaIntDTO) transaccion.getRegistro02())
								.getTasaReemIntercambioNacionalIVA(),
						48, stmt);
				
				//esta ok para registro 2
				setStringNull(ConstantesUtil.EMPTY.toString(), 49, stmt);
	
				//esta ok para registro 2
				// p:1000 = fecha_de_compensacion Se setea segun condicion fecha
				// compra > fecha actual (P1000)
				setStringNull((DateUtils.getStringDateWithValidationProcessVisa(((Registro02OutVisaIntDTO) transaccion.getRegistro02()).getFechaLiquidacionDiferida())),50, stmt);
				
				//esta ok para registro 2
				setStringNull(
						((Registro02OutVisaIntDTO) transaccion.getRegistro02())
								.getTipoVenta(),
						51, stmt);
				
				//esta ok para registro 2
				setStringNull(
						((Registro02OutVisaIntDTO) transaccion.getRegistro02())
								.getTarifaReemIntercambioNacional(),
						52, stmt);
				
				//esta ok para registro 2
				setStringNull(
						((Registro02OutVisaIntDTO) transaccion.getRegistro02())
								.getNumPagosPlazo(),
						53, stmt);
				
				//esta ok para registro 2
				setStringNull(
						((Registro02OutVisaIntDTO) transaccion.getRegistro02())
								.getValorPagoAPlazos(),
						54, stmt);
				//esta ok para registro 2
				setStringNull(
						((Registro02OutVisaIntDTO) transaccion.getRegistro02())
								.getCuotasInteres(),
						55, stmt);
				//esta ok para registro 2
				setStringNull(
						((Registro02OutVisaIntDTO) transaccion.getRegistro02())
								.getTasaIVA(),
						56, stmt);
				//esta ok para registro 2
				setStringNull(
						((Registro02OutVisaIntDTO) transaccion.getRegistro02())
								.getFechaLiquidacionDiferidaOriginal(),
						57, stmt);
				//esta ok para registro 2
				setStringNull(((Registro02OutVisaIntDTO) transaccion.getRegistro02()).getPagoTotalAPlazos(),58, stmt);
				//esta ok para registro 2
				setStringNull(ConstantesUtil.EMPTY.toString(),59, stmt);
				//esta ok para registro 2
				// RUT sbpay OBTENER DESDE BD
				setStringNull(transaccion.getRutsbpay(), 60, stmt);
				//esta ok para registro 2
				setStringNull(((Registro02OutVisaIntDTO) transaccion.getRegistro02())
						.getNumPagosPlazoActual(), 61, stmt);
				//esta ok para registro 2
				setStringNull(((Registro02OutVisaIntDTO) transaccion.getRegistro02())
						.getFlagPromoEmisora(), 62, stmt);
				
				//esta ok para registro 2
				setStringNull(((Registro02OutVisaIntDTO) transaccion.getRegistro02())
						.getFlagDiferido(), 63, stmt);
				
				//esta ok para registro 2
				setStringNull(((Registro02OutVisaIntDTO) transaccion.getRegistro02())
						.getPeriodoDiferido(), 64, stmt);
				
				//esta ok para registro 2
				setStringNull(((Registro02OutVisaIntDTO) transaccion.getRegistro02())
						.getFlagMesGracias(), 65, stmt);
				
				//esta ok para registro 2
				setStringNull(((Registro02OutVisaIntDTO) transaccion.getRegistro02())
						.getPeriodoMesGracia(), 66, stmt);
			}else{
				//P0148
				setStringNull(
						((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getPais(),
							47, stmt);

				       //P0158
				       setStringNull(((Registro01OutVisaIntDTO) transaccion.getRegistro01()).getValorCuota(),48, stmt);
					   
				       //P0165
					   setStringNull( ConstantesUtil.EMPTY.toString(), 49, stmt );
					   
					   //P1000 
					   setStringNull(((Registro00OutVisaIntDTO) transaccion.getRegistro00()).getSettlementDateHeader(), 50, stmt );
					   
					   //P1001
					   setStringNull("1", 51, stmt );
					   
					   setStringNull( ConstantesUtil.EMPTY.toString(), 52, stmt );
					   setStringNull( ConstantesUtil.EMPTY.toString(), 53, stmt );
					   setStringNull( ConstantesUtil.EMPTY.toString(), 54, stmt );
					   setStringNull( ConstantesUtil.EMPTY.toString(), 55, stmt );
					   setStringNull( ConstantesUtil.EMPTY.toString(), 56, stmt );
					   setStringNull( ConstantesUtil.EMPTY.toString(), 57, stmt );
					   setStringNull( ConstantesUtil.EMPTY.toString(), 58, stmt );
					   setStringNull( ConstantesUtil.EMPTY.toString(), 59, stmt );
				
					   setStringNull( ConstantesUtil.EMPTY.toString(), 60, stmt );
			            setStringNull( ConstantesUtil.EMPTY.toString(), 61, stmt );
			            setStringNull(ConstantesUtil.EMPTY.toString(), 62, stmt);
						setStringNull(ConstantesUtil.EMPTY.toString(), 63, stmt);
						setStringNull(ConstantesUtil.EMPTY.toString(), 64, stmt);
						setStringNull(ConstantesUtil.EMPTY.toString(), 65, stmt);
						setStringNull(ConstantesUtil.EMPTY.toString(), 66, stmt);
			            
			}
			
			stmt.setInt(67, sidOperador);
			if(transaccion.getRegistro06() != null){
				setStringNull(((Registro06OutVisaIntDTO) transaccion.getRegistro06()).getAll(), 68, stmt);
			}else{
				setStringNull(ConstantesUtil.EMPTY.toString(), 68, stmt);
			}
			
if(transaccion.getRegistro0D() != null){
	setStringNull(((Registro0DOutVisaIntDTO) transaccion.getRegistro0D()).getAll(), 69, stmt);		
			}else{
				setStringNull(ConstantesUtil.EMPTY.toString(), 69, stmt);
			}
			
if(transaccion.getRegistro2D() != null){
	setStringNull(((Registro2DOutVisaIntDTO) transaccion.getRegistro2D()).getAll(), 70, stmt);	
			}else{
				setStringNull(ConstantesUtil.EMPTY.toString(), 70, stmt);
			}
			
			
			return (stmt.executeUpdate() > 0);
		} finally {
			if (stmt != null) {
				stmt.close();
			}
		}
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 - 25/04/2019, (ACL) - versión inicial
	 * </ul>
	 * <p>
	 * VALIDA EL SI EL CODIGO DE RAZON ES MAYOR QUE 0
	 * 
	 * @param codRazon
	 * @param transcode
	 * @return
	 * @since 1.X
	 */
	private String obtieneCodigoTransaccion(String codRazon, String transcode) {
		Integer iCodRazon = Integer.parseInt(codRazon);
		if (iCodRazon > 0) {
			return "205";
		}

		return transcode;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 25/04/2019, (Everis Chile) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * LLAMA AL PACKAGE PARA COPIAR EL CONTENIDO DESDE LA TMP A LA TBL
	 * 
	 * @return
	 * @throws SQLException
	 * @see com.sbpay.sgi.cron.dao.ProcesarIncomingDao#callSPCargaIncoming()
	 * @since 1.X
	 */
	@Override
	public boolean callSPCargaIncoming() throws SQLException {
		Connection dbConnection = null;
		OracleCallableStatement callableStatement = null;

		try {
			String callSPCargaIncomingSql = "{call SP_SIG_CARGAINCOMING(?,?)}";

			dbConnection = getConnection();
			callableStatement = (OracleCallableStatement) dbConnection
					.prepareCall(callSPCargaIncomingSql);

			callableStatement.registerOutParameter(1, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(2, java.sql.Types.NUMERIC);

			callableStatement.executeUpdate();

			String warning = callableStatement.getString(1);
			Integer codigoError = callableStatement.getInt(2);
			LOGGER.info("******SE EJECUTA STORE PROCEDURE SP_SIG_CARGAINCOMING*****");
			LOGGER.info("WARNING : " + warning);
			LOGGER.info("codigoError : " + codigoError);

		} finally {
			if (callableStatement != null) {
				callableStatement.close();
			}
		}
		return Boolean.TRUE;
	}
	
	@Override
	public boolean callSPCargaIncomingPago() throws SQLException {
		Connection dbConnection = null;
		OracleCallableStatement callableStatement = null;

		try {
			String callSPCargaIncomingSql = "{call SP_SIG_CARGA_CPAGO_VISA(?,?)}";

			dbConnection = getConnection();
			callableStatement = (OracleCallableStatement) dbConnection
					.prepareCall(callSPCargaIncomingSql);

			callableStatement.registerOutParameter(1, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(2, java.sql.Types.NUMERIC);

			callableStatement.executeUpdate();

			String warning = callableStatement.getString(1);
			Integer codigoError = callableStatement.getInt(2);
			LOGGER.info("******SE EJECUTA STORE PROCEDURE SP_SIG_CARGA_CPAGO_VISA*****");
			LOGGER.info("WARNING : " + warning);
			LOGGER.info("codigoError : " + codigoError);

		} finally {
			if (callableStatement != null) {
				callableStatement.close();
			}
		}
		return Boolean.TRUE;
	}
	//callSPCobroCargoVisa
	@Override
	public boolean callSPCobroCargoVisa() throws SQLException {
		Connection dbConnection = null;
		OracleCallableStatement callableStatement = null;

		try {
			String callSPCargaIncomingSql = "{call SP_SIG_COBRO_CARGO_VISA(?,?)}";

			dbConnection = getConnection();
			callableStatement = (OracleCallableStatement) dbConnection
					.prepareCall(callSPCargaIncomingSql);

			callableStatement.registerOutParameter(1, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(2, java.sql.Types.NUMERIC);

			callableStatement.executeUpdate();

			String warning = callableStatement.getString(1);
			Integer codigoError = callableStatement.getInt(2);
			LOGGER.info("******SE EJECUTA STORE PROCEDURE SP_SIG_COBRO_CARGO_VISA*****");
			LOGGER.info("WARNING : " + warning);
			LOGGER.info("codigoError : " + codigoError);

		} finally {
			if (callableStatement != null) {
				callableStatement.close();
			}
		}
		return Boolean.TRUE;
	}

	@Override
	public boolean saveTransactionPago(TransaccionOutVisaPago transaccion, Integer sidOperador) throws Exception {
		PreparedStatement stmt = null;

		try {
			StringBuilder str = new StringBuilder()
					.append("INSERT INTO ")
					.append(ConstantesBD.ESQUEMA.toString())
					.append(".TMP_CPAGO_VISA  ")
					.append(" (SID, "
							+ "MIT, "
							+ "CODIGO_FUNCION, "
							+ "DESTINATION_BIN,")
					.append(" SOURCE_BIN, "
							+ "REP_SRE_IDENTIFIER, "
							+ "REC_SRE_IDENTIFIER, "
							+ "SERVICE_IDENTIFIER,")
					.append(" CURRENCY_CODE, "
							+ "BUSINESS_MODE, "
							+ "RESERVED1, ")
					.append(" REPORT , "
							+ "SETTLEMENT_DATE, "
							+ "REPORT_DATE, "
							+ "TOTAL_INTERCHANGE_COUNT, ")
					.append(" TOTAL_INTERCHANGE_VALUE , "
							+ "INTERCHANGE_VALUE_SIGN, "
							+ "TOTAL_REIMBURSEMENT_FEES, "
							+ "REIMBURSEMENT_FEES_SIGN, ")
					.append(" TOTAL_VISA_CHARGES , "
							+ "VISA_CHARGES_SIGN, "
							+ "NET_SETTLEMENT_AMOUNT, "
							+ "NET_SETTLEMENT_AMOUNT_SIGN, ")
					.append(" SUMMARY_LEVEL , "
							+ "RESERVED2, "
							+ "OPERADOR)")
					.append(" values (SEQ_TMP_CPAGO_VISA.nextval, ?, ?, ? , ")
					.append(" ?, ?, ?, ? ,").append(" ?, ?, ?, ? ,")
					.append(" ?, ?, ?, ? ,").append(" ?, ?, ?, ? ,")
					.append(" ?, ?, ?, ? ,").append(" ?, ?)");

			this.getConnection().setAutoCommit(false);
			stmt = this.getConnection().prepareStatement(str.toString());
			
			//LOGGER.info("QUERY DE PAGO : " + str);

			setStringNull(((Registro00OutVisaPagoDTO) transaccion.getRegistro00()).getMit(), 1, stmt);
			setStringNull(((Registro00OutVisaPagoDTO) transaccion.getRegistro00()).getCodigoFuncion(), 2, stmt);
			setStringNull(((Registro00OutVisaPagoDTO) transaccion.getRegistro00()).getDestinationBIN(), 3, stmt);
			setStringNull(((Registro00OutVisaPagoDTO) transaccion.getRegistro00()).getSourceBIN(), 4, stmt);
			setStringNull(((Registro00OutVisaPagoDTO) transaccion.getRegistro00()).getRepSREIdentifier(), 5, stmt);
			setStringNull(((Registro00OutVisaPagoDTO) transaccion.getRegistro00()).getRecSREIdentifier(), 6, stmt);
			setStringNull(((Registro00OutVisaPagoDTO) transaccion.getRegistro00()).getServiceIdentifier(), 7, stmt);
			setStringNull(((Registro00OutVisaPagoDTO) transaccion.getRegistro00()).getCurrencyCode(), 8, stmt);
			setStringNull(((Registro00OutVisaPagoDTO) transaccion.getRegistro00()).getBusinessMode(), 9, stmt);
			setStringNull(((Registro00OutVisaPagoDTO) transaccion.getRegistro00()).getReserved1(), 10, stmt);
			setStringNull(((Registro00OutVisaPagoDTO) transaccion.getRegistro00()).getReport(), 11, stmt);
			// procesar fecha en juliano y dejarla normal
			setStringNull( DateUtils.transformJulianDateInGregorianDateStringPago(((Registro00OutVisaPagoDTO) transaccion.getRegistro00()).getSettlementDate()), 12, stmt);
			setStringNull( DateUtils.transformJulianDateInGregorianDateStringPago(((Registro00OutVisaPagoDTO) transaccion.getRegistro00()).getReportDate()), 13, stmt);
			
			setStringNull(((Registro00OutVisaPagoDTO) transaccion.getRegistro00()).getTotalInterchangeCount(), 14, stmt);
			setStringNull(((Registro00OutVisaPagoDTO) transaccion.getRegistro00()).getTotalInterchangeValue(), 15, stmt);
			setStringNull(((Registro00OutVisaPagoDTO) transaccion.getRegistro00()).getInterchangeValueSign(), 16, stmt);
			setStringNull(((Registro00OutVisaPagoDTO) transaccion.getRegistro00()).getTotalReimbursementFees(), 17, stmt);
			setStringNull(((Registro00OutVisaPagoDTO) transaccion.getRegistro00()).getReimbursementFeesSign(), 18, stmt);
			setStringNull(((Registro00OutVisaPagoDTO) transaccion.getRegistro00()).getTotalVisaCharges(), 19, stmt);
			setStringNull(((Registro00OutVisaPagoDTO) transaccion.getRegistro00()).getVisaChargesSign(), 20, stmt);
			setStringNull(((Registro00OutVisaPagoDTO) transaccion.getRegistro00()).getNetSettlementAmount(), 21, stmt);
			setStringNull(((Registro00OutVisaPagoDTO) transaccion.getRegistro00()).getNetSettlementAmountSign(), 22, stmt);
			setStringNull(((Registro00OutVisaPagoDTO) transaccion.getRegistro00()).getSummaryLevel(), 23, stmt);
			setStringNull(((Registro00OutVisaPagoDTO) transaccion.getRegistro00()).getReserved2(), 24, stmt);
			stmt.setInt(25, sidOperador);
			
			
			return (stmt.executeUpdate() > 0);
		} finally {
			if (stmt != null) {
				stmt.close();
			}
		}
	}
	
	//
	@Override
	public boolean saveTransactionCargoAbono(TransaccionOutVisaCobroCargo transaccion, Integer sidOperador) throws Exception {
		PreparedStatement stmt = null;

		try {
			StringBuilder str = new StringBuilder()
					.append("INSERT INTO ")
					.append(ConstantesBD.ESQUEMA.toString())
					.append(".TMP_COBRO_CARGO_VISA  ")
					.append(" (SID, "
							+ "MIT, "
							+ "CODIGO_FUNCION, "
							+ "DESTINATION_BIN,")
					.append(" SOURCE_BIN, "
							+ "REASON_CODE, "
							+ "COUNTRY_CODE, "
							+ "EVENT_DATE,")
					.append(" ACCOUNT_NUMBER, "
							+ "ACCOUNT_NUMBER_EXTENSION, "
							+ "DESTINATION_AMOUNT, ")
					.append(" DESTINATION_CURRENCY_CODE , "
							+ "SOURCE_AMOUNT, "
							+ "SOURCE_CURRENCY_CODE, "
							+ "MESSAGE_TEXT, ")
					.append(" SETTLEMENT_FLAG , "
							+ "TRANSACTION_IDENTIFIER, "
							+ "RESERVED, "
							+ "CENTRAL_PROCESSING_DATE, ")
					.append(" REIMBURSEMENT_ATTRIBUTE , "
							+ "OPERADOR, "
							+ "DATOS_ADICIONALES)")
					.append(" values (SEQ_TMP_COBRO_CARGO_VISA.nextval, ?, ?, ? , ")
					.append(" ?, ?, ?, ? ,").append(" ?, ?, ?, ? ,")
					.append(" ?, ?, ?, ? ,").append(" ?, ?, ?, ? ,")
					.append(" ?, ?)");

			this.getConnection().setAutoCommit(false);
			stmt = this.getConnection().prepareStatement(str.toString());
			
			LOGGER.info("QUERY DE Cobro Cargo : " + str);

			setStringNull(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getMit(), 1, stmt);
			setStringNull(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getCodigoFuncion(), 2, stmt);
			setStringNull(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getDestinationBin(), 3, stmt);
			setStringNull(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getSourceBin(), 4, stmt);
			setStringNull(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getReasonCode(), 5, stmt);
			setStringNull(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getCountryCode(), 6, stmt);
			
			setStringNull( DateUtils.getStringDateWithValidationProcess(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getEventDate(), DateUtils.FECHA_FUTURA), 7, stmt);
			
			setStringNull(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getAccountNumber(), 8, stmt);
			setStringNull(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getAccountNumberExtension(), 9, stmt);
			setStringNull(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getDestinationAmount(), 10, stmt);
			setStringNull(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getDestinationCurrencyCode(), 11, stmt);
			setStringNull(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getSourceAmount(), 12, stmt);
			setStringNull(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getSourceCurrencyCode(), 13, stmt);
			setStringNull(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getMessageText(), 14, stmt);
			setStringNull(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getSettlementFlag(), 15, stmt);
			setStringNull(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getTransactionIdentifier(), 16, stmt);
			setStringNull(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getReserved(), 17, stmt);
			
			setStringNull( DateUtils.transformJulianDateInGregorianDateStringCobroCargo(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getCentralProcessingDate()), 18, stmt);
			
			setStringNull(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getReimbursementAttribute(), 19, stmt);
			stmt.setInt(20, sidOperador);
			setStringNull(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getDatosAdicionales(), 21, stmt);
					
			
			return (stmt.executeUpdate() > 0);
		} finally {
			if (stmt != null) {
				stmt.close();
			}
		}
	}
	
	
	
	

}
