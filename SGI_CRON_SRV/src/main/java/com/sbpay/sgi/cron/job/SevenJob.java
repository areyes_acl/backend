package com.sbpay.sgi.cron.job;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;

import com.sbpay.sgi.cron.step.StepGenerarReporteDetalleContable;
import com.sbpay.sgi.cron.step.StepRegenerarCtblAvance;
import com.sbpay.sgi.cron.step.StepReuploadAsientosCntblAvance;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/11/2019, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * Job 7, se crea para poder regenerar una interfaz contable de avance
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class SevenJob implements Job {
	/** LOGGER CONSTANT **/
	private static final Logger LOG = Logger.getLogger(SevenJob.class);

	@Override
	public void execute(JobExecutionContext context) {
		LOG.info("========== EJECUCION DEL SEPTIMO JOB =============");
		GenerarReporteDetalleContable();
		ProcesoRegenerarContable();
		ResubirAsientosContablesAvance();
		
		LOG.info("========== FIN EJECUCION DEL SEPTIMO JOB =========");

	}
	
	
	public void GenerarReporteDetalleContable() {
		try {
			StepGenerarReporteDetalleContable stepGrc= new StepGenerarReporteDetalleContable();
			stepGrc.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/XX/2019, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @since 1.X
	 */
	public void ProcesoRegenerarContable() {
		try {
			StepRegenerarCtblAvance stepRegenerarCtblAvance = new StepRegenerarCtblAvance();
			stepRegenerarCtblAvance.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
	

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/XX/2019 , (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @since 1.X
	 */
	public void ResubirAsientosContablesAvance() {
		try {
			StepReuploadAsientosCntblAvance stepReuploadAsientosCntblAvance = new StepReuploadAsientosCntblAvance();
			stepReuploadAsientosCntblAvance.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
	

}
