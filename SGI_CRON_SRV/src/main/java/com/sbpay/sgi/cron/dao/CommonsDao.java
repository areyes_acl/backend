package com.sbpay.sgi.cron.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.sbpay.sgi.cron.dao.Dao;
import com.sbpay.sgi.cron.dto.BolLogDTO;
import com.sbpay.sgi.cron.dto.CronConfigDTO;
import com.sbpay.sgi.cron.dto.CronTaskParam;
import com.sbpay.sgi.cron.dto.ICCodeHomologationDTO;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.dto.ParametroUserDTO;
import com.sbpay.sgi.cron.dto.TblCronLogDTO;
import com.sbpay.sgi.cron.enums.FilterTypeSearch;
import com.sbpay.sgi.cron.enums.LogBD;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.enums.StatusProcessType;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public interface CommonsDao extends Dao {

    /**
     * 
     * @param codGrupoDato
     * @return
     * @throws SQLException
     */
    List<ParametroDTO> getParametroCodGrupoDato(final String codGrupoDato)
	    throws SQLException;

    /**
     * 
     * @param codDato
     * @return
     * @throws SQLException
     */
    public ParametroDTO getParametroCodDato(String codDato) throws SQLException;

    /**
     * 
     * @param codGrupoDato
     * @return
     * @throws SQLException
     */
    public List<ParametroUserDTO> getParametroCodGrupoDatoUser(
	    final String codGrupoDato) throws SQLException;

    /**
     * 
     * @param codDato
     * @return
     * @throws SQLException
     */
    public ParametroUserDTO getParametroCodDatoUser(final String codDato)
	    throws SQLException;

    /**
     * 
     * @param secuencia
     * @return
     * @throws SQLException
     */
    long getSecuencia(final String secuencia) throws SQLException;

    /**
     * 
     * @param dto
     * @param tableName
     * @param filter
     * @return
     * @throws SQLException
     */
    public TblCronLogDTO getTblCronLogByType(TblCronLogDTO dto,
	    String tableName, FilterTypeSearch filter) throws SQLException;
      
    
    /**
     * 
     * @param dto
     * @param tableName
     * @param filter
     * @return
     * @throws SQLException
     */
    public BolLogDTO getBolLogRegisterByName(String filename) throws SQLException;

    /**
     * 
     * @param dto
     * @param tableName
     * @param filter
     * @return
     * @throws SQLException
     */
    public TblCronLogDTO getRegisterWithMaxDate(LogBD log) throws SQLException;

    /**
     * 
     * @param dto
     * @param tableName
     * @param filter
     * @return
     * @throws SQLException
     */
    boolean updateTblCronLogByType(final TblCronLogDTO dto,
	    final String tableName, final FilterTypeSearch filter)
	    throws SQLException;
    
    
    

    /**
     * 
     * @param cod: nombre del parámetro al que se le cambiará el estado
     * @param est : nuevo estado
     * @return retorna si se cambió o no el estado
     * @throws SQLException
     */
    
    

    /**
     * 
     * 
     * 
     * @param dto
     *            : Datos que se enviaran a insertar
     * @param tableName
     *            : Tabla en la cual se realizara el update
     * @param filter
     *            : Filtro por el cula se realizara el update
     * @return
     * @throws SQLException
     */
    boolean updateTblCronLogIncByType(final TblCronLogDTO dto,
	    final String tableName, final FilterTypeSearch filter)
	    throws SQLException;

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Metodo disponible para actualizar el campo statu.
     * 
     * 
     * @param filename
     * @return
     * @throws SQLException
     * @since 1.X
     */
    boolean updateLogBol(String filename, StatusProcessType status)
	    throws SQLException;

    /**
     * 
     * 
     * 
     * @param dto
     *            : Datos que se enviaran a insertar
     * @param logTable
     *            : Tabla en la cual se realizara el insert
     * @return
     * @throws SQLException
     */
    public boolean saveTblCronLog(TblCronLogDTO dto, LogBD logTable)
	    throws SQLException;
    
    
    /**
     * 
     * 
     * 
     * @param dto
     *            : Datos que se enviaran a insertar
     * @param logTable
     *            : Tabla en la que realiza el insert es a la tb_log_contable solo avances
     * @return
     * @throws SQLException
     */
    public boolean saveTblCronLogContableAvance(TblCronLogDTO dto)
	    throws SQLException;
    
     /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param logDto
     * @param logTable
     * @return
     * @throws SQLException
     * @since 1.X
     */
    public boolean saveTblCronLogBOL(TblCronLogDTO logDto, LogBD logTable)
	    throws SQLException;

    /**
     * 
     * @param dto
     *            : Datos que se enviaran a insertar
     * @param logTable
     *            : Tabla en la cual se realizara el insert
     * @return
     * @throws SQLException
     */
    public boolean saveTblCronLogCM(TblCronLogDTO dto, LogBD logTable)
	    throws SQLException;

    /**
     * 
     * Metodo que consulta por la lista de codigos de IC que seran homologados,
     * a codigos transbank
     * 
     * 
     * @return Lista de codigos homologados de IC
     * @throws SQLException
     */
    public List<ICCodeHomologationDTO> obtenerCodigosIc() throws SQLException;

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Metodo que busca si un proceso esta activo o no dado el codigo del
     * proceso
     *
     *
     * @return
     * @throws SQLException
     * @since 1.X
     */
    public CronConfigDTO isProcessActive(ProcessCodeType configProcess)
	    throws SQLException;
    
    
    /**
     * 
     * @param tableName
     * @param operatorName
     * @return sidOperador
     * @throws SQLException
     */
   public Integer getOperador(final String operatorName)
	    throws SQLException;
   
   public Integer getIDOperador(final String operatorId)
		    throws SQLException;
   
   public String getBinOperador(final String operatorName)
		    throws SQLException;
   
   public List<Integer> getAllOperadorProducto()
		    throws SQLException;

   public boolean updateTblCronConfig(String paramName, int est)
			throws SQLException;

boolean getUsuario(String username, String password) throws SQLException;

List<CronTaskParam> datosParametros(int tarea) throws SQLException;

boolean updateValorParams(int sid, String valor) throws SQLException;
}
