package com.sbpay.sgi.cron.srv.download;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dto.ConfigFile;
import com.sbpay.sgi.cron.dto.DataFTP;
import com.sbpay.sgi.cron.dto.ParamDownloadVisaDTO;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.dto.TblCronLogDTO;
import com.sbpay.sgi.cron.enums.ConstantesPRTS;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.LogBD;
import com.sbpay.sgi.cron.enums.MsgErrorMail;
import com.sbpay.sgi.cron.enums.MsgErrorSQL;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.enums.StatusProcessType;
import com.sbpay.sgi.cron.srv.utils.ClientFTPSrv;
import com.sbpay.sgi.cron.srv.utils.CommonSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;
import com.sbpay.sgi.cron.utils.generals.DateUtils;
import com.sbpay.sgi.cron.validation.ProcessValidator;
import com.sbpay.sgi.cron.validation.impl.ProcessValidatorImpl;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * Clase del tipo servicio que realiza la descarga del outgoing
 * internacional de VISA.
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class DownLoadOutgoingVisaSrv extends CommonSrv{
	
    private static final Logger LOG = Logger.getLogger( DownLoadOutgoingVisaSrv.class.getName() );

    private static DownLoadOutgoingVisaSrv singleINSTANCE = null;
    
    private final ProcessCodeType PROCESO_ACTUAL = ProcessCodeType.DESCARGAR_OUTGOING_VISA;
    
    private static void createInstance() {
        synchronized ( DownLoadOutgoingVisaSrv.class ) {
            if ( singleINSTANCE == null ) singleINSTANCE = new DownLoadOutgoingVisaSrv();
        }
    }
    
    public static DownLoadOutgoingVisaSrv getInstance() {
        if ( singleINSTANCE == null ) createInstance();
        return singleINSTANCE;
    }
    
    @Override
    protected Object clone() throws CloneNotSupportedException {
        super.clone();
        throw new CloneNotSupportedException();
    }
    
    private DownLoadOutgoingVisaSrv() {
        super();
    }
    
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
     * </ul>
     * <p>
     * 
     * @return
     * @throws AppException
     * @since 1.X
     */
    public boolean descargarArchivoVisaInt() throws AppException {
        
        LOG.info( "=====INICIO DESCARGA ARCHIVO VISA INTERNACIONAL======" );
        
        // VALIDA ESTADO DEL PROCESO
        ProcessValidator validadorProceso = new ProcessValidatorImpl();
        if ( !validadorProceso.isActiveProcess( PROCESO_ACTUAL ) ) {
            String asunto = MsgProceso.MSG_PROCESO_DESACTIVADO_ASUNTO
                    .toString().replace( ":?:",
                            PROCESO_ACTUAL.getCodigoProceso() );
            String mensaje = MsgProceso.MSG_PROCESO_DESACTIVADO_BODY.toString()
                    .replace( ":?:", PROCESO_ACTUAL.getCodigoProceso() );
            MailSendSrv.getInstance().sendMail( asunto, mensaje );
        }
        
        boolean flag = Boolean.FALSE;
        ParamDownloadVisaDTO parametros = null;
        List<String> listFTP = null;
        List<String> listToDownload = null;
        int countDownloadProcess = 0;
        String errorFile = null;

        parametros = getParametros();
            
		if (validadorProceso.hasTheValidParametersForTheProcess( PROCESO_ACTUAL, parametros )) {
		  LOG.info("PARAMETROS DE PROCESO DE DESCARGA OUTGOING VISA CARGADOS CORRECTAMENTE");
			// OBTENER NOMBRES ARCHIVOS A DESCARGAR
			listFTP = ClientFTPSrv.getInstance().listFileSFTP(
							parametros.getDataFTP(), parametros.getFtpPathOutVisa());
			// VALIDAR FORMATOS NOMBRES ARCHIVOS
			ConfigFile configFile = new ConfigFile();
			configFile.setEndsWith(parametros.getExtNameVISA());
			configFile.setEndsWithCtr(parametros.getExtNameVISACTR());
			configFile.setStarWith(parametros.getFileNameVISA());
			configFile.setStarWithCtr(parametros.getFileNameVISA());
			
			// FILTRA ARCHIVOS A DESCARGAR DE ACUERDO A LOS QUE TENGAN ARCHIVOS DE CONTROL
			listToDownload = retrieveFilenameCtrList(listFTP, configFile);
			
			// SI EXISTE ALGUN ARCHIVO VALIDO CONTINUA PROCESO
			if (listToDownload != null && listToDownload.size() > 0) {

				// VALIDAR QUE NO SE HAYAN DESCARGADO ANTES
				listToDownload = currentlyNotDownloadedFilesVisaInt(listToDownload, parametros.getFileNameVISA(), LogBD.TABLE_LOG_VISA_INT);
				// VALIDACION QUE EXISTA ALGUNO
				if (listToDownload != null && listToDownload.size() > 0) {
					errorFile = ConstantesUtil.EMPTY.toString();
					for (String filename : listToDownload) {
						try {
							// DESCARGAR
							String pathFtp = parametros.getFtpPathOutVisa();
							String pathServer = parametros.getPathVisaInc();
							 
							LOG.info( "Descargando archivo :"+filename+", desde ftp : "+ parametros.getDataFTP().getIpHost() );
							ClientFTPSrv.getInstance().downloadFileSFTP(
									parametros.getDataFTP(), pathFtp,
									pathServer, filename);

							// INSERTA EN LA TBL_LOG_VISA_INT
							save(filename, parametros.getFileNameVISA());

							// CUENTA LOS REGISTROS INSERTADOS
							countDownloadProcess++;
						} catch (AppException e) {
							LOG.error("Ha ocurrido un error al intentar descargar el archivo : " + filename, e);
							errorFile = errorFile.concat(filename).concat(
									ConstantesUtil.SKIP_LINE.toString());
						}
					}
				} else {
					LOG.warn("No existen nuevos archivos outgoing Visa para descargar");
					MailSendSrv.getInstance().sendMail(
							MsgErrorMail.ALERTA_FILE_DOWNLOAD_VISA_TITLE.toString(),
							MsgErrorMail.ALERTA_FILE_DOWNLOAD_VISA_BODY.toString());
				}
			} else {
				LOG.warn("No existen archivos outgoing Visa que procesar");
				MailSendSrv.getInstance().sendMail(
						MsgErrorMail.ALERTA_FILE_PROCESS_VISA.toString(),
						MsgErrorMail.ALERTA_FILE_PROCESS_VISA_TXT.toString());
			}
		} else {
		    LOG.warn(MsgProceso.MSG_ERROR_FALTAN_PARAMETROS.toString());
			MailSendSrv.getInstance().sendMail(
					MsgErrorMail.ALERTA_PRTS.toString(),
					MsgErrorMail.ALERTA_PRTS_TXT.toString());
		}

		// SE SUBIERON CORRECTAMETNE TODOS LOS REGISTROS
		if (listToDownload != null
				&& countDownloadProcess == listToDownload.size()) {
			flag = Boolean.TRUE;
		} else if (errorFile != null) {
			MailSendSrv.getInstance().sendMail(
					MsgErrorMail.ALERTA_FILE_LOAD_VISA.toString(),
					MsgErrorMail.ALERTA_FILE_LOAD_VISA_TXT.toString().concat(
							errorFile));
		}

		return flag;
        
    }
   
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * Metodo crea un registro en la tabla log TBL_LOG_VISA
     * registrando la descarga nombre del archivo.
     * 
     * @param fileName
     *            nombre del archivo.
     * @param starWith
     *            prefijo del archivo.
     * @throws AppException
     *             Exception App.
     * @since 1.X
     */
	private void save(final String fileName, final String starWith) throws AppException {
        // INICIALIZA EL DAO
        CommonsDao commonsDAO = null;
        try {
            commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
            TblCronLogDTO cronLog = new TblCronLogDTO();
            cronLog.setFecha( DateUtils.getDateFileVisaInt( DateUtils.getDateStringFromFilenameVisa( fileName, starWith ) ) );
            cronLog.setFilename( fileName );
            cronLog.setFileFlag( StatusProcessType.PROCESS_PENDING.getValue() );
            
            if ( commonsDAO.saveTblCronLog( cronLog, LogBD.TABLE_LOG_VISA_INT ) ) {
                commonsDAO.endTx();
            }
            else {
                commonsDAO.rollBack();
            }
        }
        catch ( SQLException e ) {
        	rollBackTransaccion(commonsDAO);
            throw new AppException(
            		MsgErrorSQL.ERROR_LOAD_PROCESS.toString(), e );
		} finally {
			try {
				if (commonsDAO != null) {
					commonsDAO.close();
				}
			} catch (SQLException e) {
				rollBackTransaccion(commonsDAO);
				throw new AppException(
						MsgErrorSQL.ERROR_LOAD_PROCESS.toString(), e);
			}
		}
		
	}
	
	
	

	

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Metodo obtiene los parametros.
     * 
     * @return DTO de parametros.
     * @throws AppException
     *             Exception App.
     * 
     */
	private ParamDownloadVisaDTO getParametros() throws AppException {

        CommonsDao daoCommon = null;
        List<ParametroDTO> paramDTOList = null;
        DataFTP dataFTP = null;
        ParamDownloadVisaDTO paramDownloadVisaDTO = null;
        try {
            LOG.info( "CARGA PARAMETROS CRON VISA INTERNACIONAL" );
            daoCommon = CommonsDaoFactory.getInstance().getNewEntidadDao();
            paramDTOList = daoCommon.getParametroCodGrupoDato( ConstantesPRTS.CRON_VISA_INT.toString() );
            
            if ( paramDTOList != null  ) {
            	paramDownloadVisaDTO = new ParamDownloadVisaDTO();
                
            	// PARAMETROS SFTP DE TBK
                dataFTP = new DataFTP();
                dataFTP.setIpHost( CommonsUtils.getCodiDato( paramDTOList, ConstantesPRTS.SFTP_IP_DOWNLOAD_VISA_INT.toString() ) );
                
                String port =  CommonsUtils.getCodiDato( paramDTOList, ConstantesPRTS.SFTP_PORT_DOWNLOAD_VISA_INT.toString());
                dataFTP.setPort( Integer.parseInt((port != null && port.matches(ConstantesUtil.PATTERN_IS_NUMBER.toString()) ? port : ConstantesUtil.DEFAULT_PORT.toString())));
                		
                dataFTP.setUser( CommonsUtils.getCodiDato( paramDTOList, ConstantesPRTS.SFTP_USER_DOWNLOAD_VISA_INT.toString() ) );
                dataFTP.setPassword( CommonsUtils.getCodiDato( paramDTOList, ConstantesPRTS.SFTP_PASS_DOWNLOAD_VISA_INT.toString() ) );

                // RUTA DE FOLDER FTP DESDE DONDE SE DESCARGA EL ARCHIVO OUTGOING VISA 
                paramDownloadVisaDTO.setFtpPathOutVisa( CommonsUtils.getCodiDato( paramDTOList, ConstantesPRTS.PATH_OUT_DWNLD_VISA_INT.toString() ) );
                
                // RUTA SERVIDOR sbpay DONDE SE DESCARGARA EL ARCHIVO DE VISA
                paramDownloadVisaDTO.setPathVisaInc( CommonsUtils.getCodiDato( paramDTOList, ConstantesPRTS.PATH_ACL_INC_VISA_INT.toString() ) );
                
                // FORMATO DE NOMBRE DE ARCHIVO OUTGOING VISA
                paramDownloadVisaDTO.setFileNameVISA( CommonsUtils.getCodiDato(paramDTOList, ConstantesPRTS.FORMAT_FILENAME_OUT_VISA.toString() ) );
                
                // FORMATO DE EXTENSION DE ARCHIVO OUTGOING VISA 
                paramDownloadVisaDTO.setExtNameVISA( CommonsUtils.getCodiDato( paramDTOList, ConstantesPRTS.FORMAT_EXTNAME_OUT_VISA.toString() ) );
                
                // FORMATO DE EXTENSION DE ARCHIVO DE CONTROL DE OUTGOING VISA
                paramDownloadVisaDTO.setExtNameVISACTR( CommonsUtils.getCodiDato( paramDTOList, ConstantesPRTS.FORMAT_EXTNAME_OUT_VISA_CTR.toString() ) );
                paramDownloadVisaDTO.setDataFTP(dataFTP);
            }
            LOG.debug("PARAMETROS : " + paramDownloadVisaDTO);
            return paramDownloadVisaDTO;
        }
        catch ( SQLException e ) {
            throw new AppException( MsgErrorSQL.ERROR_QUERY_PRTS.toString(), e );
        }
        finally {
            try {
                if ( daoCommon != null ) {
                    daoCommon.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException( MsgErrorSQL.ERROR_QUERY_PRTS_CLOSE.toString(), e );
            }
        }
	}
	
	
	
	
}
