package com.sbpay.sgi.cron.dao;

import java.sql.SQLException;

import com.sbpay.sgi.cron.dao.BaseDaoFactory;
import com.sbpay.sgi.cron.ds.ConnectionProvider;


public class GenerarReporteContableDaoFactory  extends BaseDaoFactory<GenerarReporteContableDao>{
	/** The single instance. */
	private static GenerarReporteContableDaoFactory singleINSTANCE = null;

	/**
	 * Creates the instance.
	 * 
	 * @throws SQLException
	 */
	private static void createInstance() throws SQLException {
		synchronized (GenerarReporteContableDaoFactory.class) {
			if (singleINSTANCE == null) {
				singleINSTANCE = new GenerarReporteContableDaoFactory();
			}
		}
	}

	/**
	 * Patron singleton.
	 * 
	 * @return RegenerarContableAvanceDaoFactory retorna instancia del servicio.
	 * @throws SQLException
	 */
	public static GenerarReporteContableDaoFactory getInstance() throws SQLException {
		if (singleINSTANCE == null) {
			createInstance();
		}
		return singleINSTANCE;
	}

	/**
	 * Metodo de restricion de sobreescritura de la clase.
	 */
	@Override
	protected Object clone() throws CloneNotSupportedException {
		super.clone();
		throw new CloneNotSupportedException();
	}

	/**
	 * Constructor default.
	 * 
	 * @throws SQLException Error de SQL.
	 */
	private GenerarReporteContableDaoFactory() throws SQLException {
		super();
	}

	@Override
	public GenerarReporteContableDao getNewEntidadDao() throws SQLException {
		return new GenerarReporteContableDaoJdbc(ConnectionProvider.getInstance().getConnection());
	}	
}
