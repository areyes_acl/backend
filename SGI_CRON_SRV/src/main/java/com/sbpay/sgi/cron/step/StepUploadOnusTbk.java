package com.sbpay.sgi.cron.step;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.enums.MsgExitoMail;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.srv.upload.UploadOnusTbkSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;

public class StepUploadOnusTbk {

	private static final Logger LOG = Logger.getLogger(StepUploadOnusTbk.class);
	
	public void execute() throws Exception {
		
		try {
			LOG.info( "Inicio Proceso: Upload On Us Transbank" );
			if ( UploadOnusTbkSrv.getInstance().uploadOnusTbk()){
				LOG.info("Envio de correo de carga satisfactoria: Proceso Upload On Us Transbank");
				MailSendSrv.getInstance().sendMailOk(MsgExitoMail.EXITO_PROCESS_UPLOAD_TITLE_ONUS.toString(), MsgExitoMail.EXITO_PROCESS_UPLOAD_ONUS.toString());
			} else {
				LOG.info("Finaliza Proceso: Upload On Un Transbank, no se realiza correctamente.");
			}
		} catch(AppException e) {
		    MailSendSrv.getInstance().sendMail(
                    MsgProceso.MSG_GENERIC_ERROR_MSJ.toString(),
                    ( e.getStackTraceDescripcion() != null ) ? e
                            .getStackTraceDescripcion() : e.getMessage() );
		}
	}
}
