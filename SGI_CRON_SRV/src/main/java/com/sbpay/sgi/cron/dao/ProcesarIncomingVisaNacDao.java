package com.sbpay.sgi.cron.dao;

import java.sql.SQLException;

import com.sbpay.sgi.cron.dao.Dao;
import com.sbpay.sgi.cron.dto.TransaccionOutVisaNac;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 17/04/2019, (ACL) -  versión inicial 
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SA.</B>
 */
public interface ProcesarIncomingVisaNacDao extends Dao {
    
    public boolean saveTransaction(TransaccionOutVisaNac transaccion, Integer sidOperador)throws SQLException;
    
    public boolean callSPCargaIncoming() throws SQLException;
    
}