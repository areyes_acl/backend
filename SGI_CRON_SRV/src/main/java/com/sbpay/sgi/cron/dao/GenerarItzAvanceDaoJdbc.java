
package com.sbpay.sgi.cron.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.BaseDao;
import com.sbpay.sgi.cron.dto.TmpExportItzAvanceDTO;



public class GenerarItzAvanceDaoJdbc extends BaseDao<GenerarItzAvanceDao>
	implements GenerarItzAvanceDao {

	
	 /** VARIABLE PARA EL LOGER */
    private static final Logger LOGGER = Logger.getLogger( GenerarContableAvanceDaoJdbc.class );

    
	public GenerarItzAvanceDaoJdbc(Connection connection) {
		super(connection);
	}
	
	@Override
	public List<TmpExportItzAvanceDTO> obtenerInformacion() throws SQLException, AppException {
		List<TmpExportItzAvanceDTO> lstAvanceDTO = new ArrayList<TmpExportItzAvanceDTO>();
		TmpExportItzAvanceDTO paramExportAvanceleDTO;

		PreparedStatement stmt = null;
	    ResultSet rs = null;
		StringBuilder sql = new StringBuilder();

		LOGGER.info("******INICIO DE CONULTA PARA CARGAR INFORMACION DE AVANCES *****");

		sql.append("SELECT "
				+ " TO_CHAR(FECHA_ARQUEO, 'DD-MM-YYYY') AS fechaGeneracion, "
				+ " (SELECT  DOC2.TC2KAUTORIZ  FROM DOC_FINANCIEROS DOC2   "
				+ " WHERE DOC2.TIPO_DOCUMENTO = 'AVI'  AND DOC2.TIPO_VENTA = 'TRA'  " 
				+ " AND DOC2.FECHA_GENERACION= DOC.FECHA_GENERACION   "
				+ " AND DOC2.NUMERO_OPERACION_BANCO = DOC.NUMERO_OPERACION_BANCO  "
				+ " AND DOC2.ESTADO_DOCUM = 'VIG') AS codAutoriza, "
				+ " NVL(DOC.NUMERO_OPERACION_BANCO,0) AS numOperBanco, "
				+ " DOC.TC2KTARJETA AS tarjeta, "
				+ " DOC.VALOR_TOTAL_DOC AS valorAvance, "
				+ " 70 AS estado,  NVL(DOC.CODIGO_CANAL,0) AS codCanal, "
				+ " NVL(DOC.SEGURO_MONTO,0) AS montoSeguro  FROM DOC_FINANCIEROS  DOC   "
				+ " WHERE "
				+ " DOC.TIPO_DOC_PAGO = 'AVT' "
				+ " AND DOC.TIPO_DOCUMENTO = 'AVE' "
				+ " AND DOC.TIPO_VENTA = 'TRA' "
				+ " AND DOC.ESTADO_DOCUM = 'VIG' "
				+ " AND DOC.NUMERO_OPERACION_BANCO > 0 "
				+ " AND DOC.FECHA_GENERACION= TRUNC(sysdate-1)");
		
		  try {
	            stmt = this.getConnection().prepareStatement(sql.toString());
	            
	            // SE EJECUTA CONSULTA
	            rs = stmt.executeQuery();
	            
	            // LEE LAS COLUMNAS DE CADA REGISTRO
	            while ( rs.next() ) {
	            	
	            	paramExportAvanceleDTO = new TmpExportItzAvanceDTO();
					paramExportAvanceleDTO.setFechaTransferencia(rs
							.getString("fechaGeneracion"));
					paramExportAvanceleDTO.setCodAutorizacion(rs
							.getString("codAutoriza"));
					paramExportAvanceleDTO.setNumeroTrx(rs
							.getString("numOperBanco"));
					paramExportAvanceleDTO
							.setNumeroTarjeta(rs.getString("tarjeta"));
					paramExportAvanceleDTO.setMonto(rs.getString("valorAvance"));
					paramExportAvanceleDTO.setEstado(rs.getString("estado"));
					paramExportAvanceleDTO.setNumeroCanal(rs.getString("codCanal"));
					paramExportAvanceleDTO.setMontoSeguro(rs.getString("montoSeguro"));
					lstAvanceDTO.add(paramExportAvanceleDTO);
	               
	            }
	            
	            LOGGER.info("******FIN CONSULTA PARA CARGAR INFORMACION DE AVANCES *****");
	            
	            return lstAvanceDTO;
	        }
	        catch ( SQLException e ) {
	            throw new AppException( e.getMessage(), e );
	        }
	        finally {
	            
	            try {
	                if ( stmt != null ) {
	                    stmt.close();
	                }
	                if ( rs != null ) {
	                    rs.close();
	                }
	            }
	            catch ( SQLException e ) {
	                throw new AppException( e.getMessage(), e );
	                
	            }
	            
	        }
	}
	


}
