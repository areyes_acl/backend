package com.sbpay.sgi.cron.step;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.srv.process.ProcesarOutgoingVisaSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 09/02/2016, (ACL) - versión inicial
 * </ul>
 * <p>
 * Step del Job correspondiente a la tarea de procesar el Outgoing de Visa 
 * Internacional.
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA.</B>
 */
public class StepProcessOutgoingVisa {
    
    /**
     * VARIABLES
     */
    private static final Logger LOG = Logger
            .getLogger( StepProcessOutgoingVisa.class );
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2015, (Everis Chile) - versión inicial
     * </ul>
     * <p>
     * 
     * @param contribution
     * @param chunkContext
     * @return
     * @throws Exception
     * @since 1.X
     */
    public void execute() throws Exception {
        try {
            LOG.info( "========> Inicio procesamiento Archivo Incoming Visa Internacional : StepProcessOutgoingVisa <=======" );
            ProcesarOutgoingVisaSrv.getInstance().procesar();
            LOG.info( "========> Finaliza correctamente el procesamiento del Archivo Incoming Visa Internacional <=======" );
        }
        catch ( AppException e ) {
            LOG.error( e.getMessage(), e );
            MailSendSrv.getInstance().sendMail(
                    MsgProceso.MSG_GENERIC_ERROR_MSJ.toString(),
                    ( e.getStackTraceDescripcion() != null ) ? e
                            .getStackTraceDescripcion() : e.getMessage() );
        }
        
    }
    
}
