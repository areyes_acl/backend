package com.sbpay.sgi.cron.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.dao.BaseDao;
import com.sbpay.sgi.cron.dto.ParamCorreoVencimientoDTO;

public class GenerarCorreoVencDAOJdbc extends BaseDao<GenerarCorreoVencDAO> implements GenerarCorreoVencDAO{
	
    /** VARIABLE PARA EL LOGER */
    private static final Logger LOGGER = Logger.getLogger( GenerarCorreoVencDAOJdbc.class );
    
    private static final String CALL_SP_SIG_OBTENER_PROX_VENC = "{call SP_SIG_OBTENER_PROX_VENC(?,?,?,?)}";
    
	public GenerarCorreoVencDAOJdbc(Connection connection) {
		super(connection);
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<ParamCorreoVencimientoDTO> obtenerTransVenc(Integer dias) throws SQLException {
        Connection dbConnection = null;
        OracleCallableStatement callableStatement = null;
        List<ParamCorreoVencimientoDTO> listaCorreoVencimientoDTOs = new ArrayList<ParamCorreoVencimientoDTO>();
        ParamCorreoVencimientoDTO paramCorreoVencimientoDTO;
        try {
            
            dbConnection = getConnection();
            callableStatement = ( OracleCallableStatement ) dbConnection
                    .prepareCall( CALL_SP_SIG_OBTENER_PROX_VENC );

            callableStatement.setInt("dias",  dias);
			callableStatement.registerOutParameter("warning", OracleTypes.VARCHAR);
			callableStatement.registerOutParameter("cod_error", OracleTypes.NUMBER);
			callableStatement.registerOutParameter("prfCursor", OracleTypes.CURSOR);
            
            callableStatement.execute();
            
            ResultSet resultados = (ResultSet) ((callableStatement).getObject("prfCursor"));
			int codigoError = callableStatement.getInt("cod_error");
			
			if(codigoError==0)
				while (resultados.next()) {
					paramCorreoVencimientoDTO = new ParamCorreoVencimientoDTO();
					paramCorreoVencimientoDTO.setNumeroTC(resultados.getString("NUMERO_TARJETA"));
					paramCorreoVencimientoDTO.setMonto(resultados.getInt("MONTO"));
					paramCorreoVencimientoDTO.setFecVencimiento(resultados.getString("FECHA_VENCIMIENTO"));
					paramCorreoVencimientoDTO.setCodMotivo(resultados.getString("COD_MOTIVO"));
					paramCorreoVencimientoDTO.setMotivo(resultados.getString("MOTIVO"));
					listaCorreoVencimientoDTOs.add(paramCorreoVencimientoDTO);
				}
			else{
				LOGGER.error("Method: buscarReporteVisa - Problemas al leer reporte visa desde la base de datos.");
			}
        }
        finally {
            if ( callableStatement != null ) {
                callableStatement.close();
            }
        }
		return listaCorreoVencimientoDTOs;
	}

}
