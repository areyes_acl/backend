package com.sbpay.sgi.cron.srv.process;

import java.io.File;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dao.CompscnPagoDao;
import com.sbpay.sgi.cron.dao.CompscnPagoFactory;
import com.sbpay.sgi.cron.dto.CompensacionPagoDTO;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.dto.ParamsReadCPagoDTO;
import com.sbpay.sgi.cron.dto.TblCronLogDTO;
import com.sbpay.sgi.cron.enums.ConstantesPRTS;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.FilterTypeSearch;
import com.sbpay.sgi.cron.enums.LogBD;
import com.sbpay.sgi.cron.enums.MsgErrorMail;
import com.sbpay.sgi.cron.enums.MsgErrorProcessFileCPago;
import com.sbpay.sgi.cron.enums.MsgErrorProcessFileComision;
import com.sbpay.sgi.cron.enums.MsgErrorSQL;
import com.sbpay.sgi.cron.enums.MsgExitoMail;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.enums.StatusProcessType;
import com.sbpay.sgi.cron.srv.utils.CommonSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;
import com.sbpay.sgi.cron.utils.file.CompscnPagoFIleUtil;
import com.sbpay.sgi.cron.utils.file.CompscnPagoReader;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;
import com.sbpay.sgi.cron.validation.ProcessValidator;
import com.sbpay.sgi.cron.validation.impl.ProcessValidatorImpl;

public class ProcesarCompscnsPagoSrv extends CommonSrv{

	private static final Logger LOGGER = Logger.getLogger(ProcesarCompscnsPagoSrv.class);

	private static ProcesarCompscnsPagoSrv singleINSTANCE = null;
	
	private final ProcessCodeType PROCESO_ACTUAL = ProcessCodeType.PROCESAR_COMPENSACION_DE_PAGOS;

	private static void createInstance() {
		synchronized (ProcesarCompscnsPagoSrv.class) {
			if (singleINSTANCE == null)
				singleINSTANCE = new ProcesarCompscnsPagoSrv();
		}
	}

	public static ProcesarCompscnsPagoSrv getInstance() {
		if (singleINSTANCE == null)
			createInstance();
		return singleINSTANCE;
	}

	/**
	 * Metodo de restricion de sobreescritura de la clase.
	 */
	@Override
	protected Object clone() throws CloneNotSupportedException {
		super.clone();
		throw new CloneNotSupportedException();
	}

	private ProcesarCompscnsPagoSrv() {
		super();
	}
	
	/**
	 * Metodo orquesta el procesamiento de compensacion de pagos desde un archivo
	 * descargado de transbank.
	 * @throws Exception 
	 */
	public void procesarCPagos() throws Exception {
	    
	    ProcessValidator validadorProceso = new ProcessValidatorImpl();

	    if (validadorProceso.isActiveProcess(PROCESO_ACTUAL)) {
	        LOGGER.info("Proceso está activo");
	    
		// BUSCAR LOS PARAMETROS NECESARIOS PARA EL FILE
		CompscnPagoFIleUtil compscnPagoFIleUtil = new CompscnPagoReader();
		ParamsReadCPagoDTO parametros = getParametros();
		List<File> listOfFiles = null;
		int countFileProcessed = 0;
		
		if(validadorProceso.hasTheValidParametersForTheProcess( PROCESO_ACTUAL, parametros )){

		/*
		 * RECORRE LOS ARCHIVOS DEL FOLDER BUSCANDO ARCHIVOS COMISIONES SE
		 * VALIDA SI TIENE REGISTRO EN TBL_LOG_COMISION Y SI ESTA O NO PROCESADO
		 * (FLAG 1 : PROCESADO, FLAG -1: ERROR, FLAG : 0 )
		 */
		listOfFiles = CommonsUtils.getFilesInFolder( parametros.getPathCPAGO(), parametros.getFileNameCPago(), "");

		LOGGER.debug("Lista de archivos :  " + ((listOfFiles != null) ? "" + listOfFiles.size() : "No se encontraron archivos"));

		// SI NO EXISTE NINGUN ARCHIVO EN LA CARPETA TERMINA EL PROCESO
		if (listOfFiles == null) {
			LOGGER.warn(MsgErrorProcessFileComision.WARNING_NO_FILES_FOUNDS.toString());
			return;
		}

		for (File file : listOfFiles) {
				try {
					if (file.isFile() && file.getName().startsWith(parametros.getFileNameCPago())
							&& isValidCPAGO(file.getName())) {
						LOGGER.info("Archivo Compensacion de pago válido : Se comienza a procesar...");
					
						// 1 - PROCESA ARCHIVO COMISIONES Y LUEGO GUARDA LOS REGISTROS Y ACTUALIZA EL LOG
						actualizarLog(compscnPagoFIleUtil.readCompscnPagoFile(file), file.getName());
					
						// 2 - SE MUEVE INCOMING A LA CARPETA BACKUP
						CommonsUtils.moveFile(file.getAbsolutePath(), parametros.getPathCPagoBKP() + file.getName());
						LOGGER.info("Archivo CPAGO, procesado correctamente, por lo que se mueve a carpeta BKP : " + parametros.getPathCPagoBKP());
						countFileProcessed++;
					}
				} catch (AppException e) {
				
					// ERROR AL PROCESAR INCOMING SE ENVIA EMAIL
					LOGGER.error(MsgErrorProcessFileCPago.ERROR_IN_PROCESS.toString(), e);
					MailSendSrv.getInstance().sendMail(MsgErrorProcessFileComision.ERROR_IN_PROCESS.toString(), e.getStackTraceDescripcion());
				
					// MUEVE ARCHIVO A LA CARPETA DE ERROR
					CommonsUtils.moveFile(file.getAbsolutePath(), parametros.getPathCPagoError() + file.getName());
					LOGGER.info("Archivo CPAGO, no se ha procesado correctamente, por lo que se mueve a carpeta ERROR : " + parametros.getPathCPagoError());
				}
			
		}
		
		
		// SI NO SE PROCESA NINGUN FILE ESCRIBE ESCRIBE LOG
        String msj = "";
        if ( countFileProcessed == 0 ) {
        	LOGGER.warn( MsgErrorProcessFileComision.WARNING_NO_FILE_PROCESSING_TITLE
                    + "  - "
                    + MsgErrorProcessFileComision.WARNING_NO_FILE_PROCESSING_CAUSE );
            msj = "Sin embargo, "+ MsgErrorProcessFileComision.WARNING_NO_FILE_PROCESSING_CAUSE.toString().toLowerCase();
        }
        
        // SI SE PROCESARON TODOS LOS ARCHIVOS EXISTENTES SE ENVIA EMAIL CORRECTO
        if ( listOfFiles.size() == countFileProcessed && !msj.isEmpty() ) {
        	LOGGER.info( "Envio Correo de Proceso  Satisfactorio: Procesamiento de compensacion de pago " );
            MailSendSrv.getInstance().sendMailOk(
                    MsgExitoMail.EXITO_PROCESS_CPAGO_TITLE.toString(),
                    MsgExitoMail.EXITO_PROCESS_CPAGO_BODY.toString().concat(ConstantesUtil.SKIP_LINE.toString()).concat(msj) );
        }else{
        	 MailSendSrv.getInstance().sendMailOk(
                     MsgExitoMail.EXITO_PROCESS_CPAGO_TITLE.toString(),
                     MsgExitoMail.EXITO_PROCESS_CPAGO_BODY.toString());
        }
		
		}else{
			LOGGER.error("Parametros de Base de Datos Incompletos Proceso : Procesar CPago");
			MailSendSrv.getInstance().sendMail(
					MsgErrorMail.ALERTA_RCH_PRTS.toString(),
					MsgErrorMail.ALERTA_RCH_PRTS_TXT.toString());
			
		}
	    }else{
	        String asunto = MsgProceso.MSG_PROCESO_DESACTIVADO_ASUNTO
                    .toString().replace( ":?:",
                            PROCESO_ACTUAL.getCodigoProceso() );
            String mensaje = MsgProceso.MSG_PROCESO_DESACTIVADO_BODY.toString()
                    .replace( ":?:", PROCESO_ACTUAL.getCodigoProceso() );
            MailSendSrv.getInstance().sendMail( asunto, mensaje );
	    }
		
	}


	/**
	 * 
	 * @param object
	 * @param fileName
	 * @throws Exception 
	 */
	private void actualizarLog(List<CompensacionPagoDTO> listaCompensacionPagoDTO, String fileName) throws Exception {
		CommonsDao commonsDAO = null;
		CompscnPagoDao compscnPagoDao = null;
		TblCronLogDTO tblCronLogDTO = null;
		try {

			commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
			compscnPagoDao = CompscnPagoFactory.getInstance().getNewEntidadDao();
			
			tblCronLogDTO = new TblCronLogDTO();
			tblCronLogDTO.setFilename(fileName);
			tblCronLogDTO.setFileFlag(StatusProcessType.PROCESS_SUCESSFUL.getValue());
			
			// SE REALIZA EL UPDATE
			if (commonsDAO.updateTblCronLogByType(tblCronLogDTO, LogBD.TABLE_LOG_CPAGO.getTable(), FilterTypeSearch.FILTER_BY_NAME)) {
				// SE LLAMA AL SP CARGA INCOMING
				for(CompensacionPagoDTO compensacionPagoDTO : listaCompensacionPagoDTO){
					compscnPagoDao.guardarCompscnPagoDao(compensacionPagoDTO);
				}
				compscnPagoDao.endTx();
				commonsDAO.endTx();
			} else {
				// ROLLBACK CARGA UPDATE FLAG
				rollBackTransaccion(compscnPagoDao);
				rollBackTransaccion(commonsDAO);
			}
		} catch (SQLException e) {
			rollBackTransaccion(commonsDAO);
			throw new AppException(MsgErrorSQL.ERROR_UPDATE_TBL_LOG.toString()
					+ LogBD.TABLE_LOG_CPAGO.getTable(), e);
		} finally {
			try {
				if (commonsDAO != null) {
					commonsDAO.close();
				}
				if (compscnPagoDao != null) {
					compscnPagoDao.close();
				}
			} catch (SQLException e) {
				rollBackTransaccion(commonsDAO);
				throw new AppException(
						MsgErrorSQL.ERROR_UPDATE_TBL_LOG.toString()
								+ LogBD.TABLE_LOG_CPAGO.getTable(), e);
			}
		}
	}
	
	private ParamsReadCPagoDTO getParametros() throws AppException {

        CommonsDao daoCommon = null;
        List<ParametroDTO> paramDTOList = null;
        ParamsReadCPagoDTO paramCPagoDTO = null;
        try {
            LOGGER.info( "CARGA PARAMETROS CRON" );
            daoCommon = CommonsDaoFactory.getInstance().getNewEntidadDao();
            paramDTOList = daoCommon.getParametroCodGrupoDato( ConstantesPRTS.CRON_TBK.toString() );
            if ( paramDTOList != null  ) {
            	paramCPagoDTO = new ParamsReadCPagoDTO();
                paramCPagoDTO.setPathCPAGO(CommonsUtils.getCodiDato( paramDTOList, ConstantesPRTS.PATH_CPAGO.toString()));
                paramCPagoDTO.setPathCPagoBKP(CommonsUtils.getCodiDato( paramDTOList, ConstantesPRTS.PATH_CPAGO_BKP.toString()));
                paramCPagoDTO.setFileNameCPago(CommonsUtils.getCodiDato( paramDTOList, ConstantesPRTS.FORMAT_FILENAME_CPAGO.toString()));
                paramCPagoDTO.setPathCPagoError(CommonsUtils.getCodiDato( paramDTOList, ConstantesPRTS.PATH_CPAGO_ERROR.toString()));
                LOGGER.info(paramCPagoDTO);
            }
            return paramCPagoDTO;
        }
        catch ( SQLException e ) {
            throw new AppException( MsgErrorSQL.ERROR_QUERY_PRTS.toString(), e );
        }
        finally {
            try {
                if ( daoCommon != null ) {
                    daoCommon.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException(
                        MsgErrorSQL.ERROR_QUERY_PRTS_CLOSE.toString(), e );
            }
        }
	}
	
	/**
	 * Metodo valida si el archivo mensual de comisiones fue descargado.
	 * 
	 * @param filename
	 *            nombre del archivo de comisiones.
	 * @return true: si el archivo no ha sido descargado false: archivo
	 *         descargado.
	 * @throws AppException
	 */
	private boolean isValidCPAGO(String filename) throws AppException {
		CommonsDao commonsDAO = null;
		try {
			commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();

			// BUSCA REGISTRO LOG POR NOMBRE
			LOGGER.info("Nombre Compensacion de pago: " + filename);
			TblCronLogDTO filter = new TblCronLogDTO();
			filter.setFilename(filename);

			TblCronLogDTO cronlog = commonsDAO.getTblCronLogByType(filter, LogBD.TABLE_LOG_CPAGO.getTable(), FilterTypeSearch.FILTER_BY_NAME);
			LOGGER.info("=====> Valor de TABLE_LOG_CPAGO: " + cronlog);

			if (cronlog == null) {
				LOGGER.error(MsgErrorProcessFileCPago.ERROR_CPAGO_NAME_NOT_FOUND.toString());
				return Boolean.FALSE;
			}
			// SI ES DISTINTO DE 0 NO ES VÁLIDO
			else if (!StatusProcessType.PROCESS_PENDING.getValue().equalsIgnoreCase(cronlog.getFileFlag())) {
				LOGGER.info(MsgErrorProcessFileCPago.ERROR_INVALID_CPAGO_STATE.toString());
				return Boolean.FALSE;
			}

		} catch (SQLException e) {
			throw new AppException(
					MsgErrorSQL.ERROR_FIND_CPAGO_BY_NAME.toString(), e);
		} finally {

			try {
				if (commonsDAO != null) {
					commonsDAO.close();
				}
			} catch (SQLException e) {
				throw new AppException(
						MsgErrorSQL.ERROR_FIND_CPAGO_BY_NAME.toString(), e);
			}
		}

		return Boolean.TRUE;
	}
	
	

}
