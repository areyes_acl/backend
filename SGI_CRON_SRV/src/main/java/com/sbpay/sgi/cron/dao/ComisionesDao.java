package com.sbpay.sgi.cron.dao;

import com.sbpay.sgi.cron.dao.Dao;
import com.sbpay.sgi.cron.dto.ComisionesPagoDTO;


/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 24/11/2015, (ACL) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SA.</B>
 */
public interface ComisionesDao extends Dao {
    
	public Boolean guardarComision(ComisionesPagoDTO comisionesPagoDTO)throws Exception;
    
}
