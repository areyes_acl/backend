package com.sbpay.sgi.cron.srv.upload;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dto.ConfigFile;
import com.sbpay.sgi.cron.dto.DataFTP;
import com.sbpay.sgi.cron.dto.ParamUploadIncomingDTO;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.dto.TblCronLogDTO;
import com.sbpay.sgi.cron.enums.ConstantesPRTS;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.FilterTypeSearch;
import com.sbpay.sgi.cron.enums.LogBD;
import com.sbpay.sgi.cron.enums.MsgErrorFile;
import com.sbpay.sgi.cron.enums.MsgErrorMail;
import com.sbpay.sgi.cron.enums.MsgErrorSQL;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.enums.StatusProcessType;
import com.sbpay.sgi.cron.srv.utils.ClientFTPSrv;
import com.sbpay.sgi.cron.srv.utils.CommonSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;
import com.sbpay.sgi.cron.utils.generals.DateUtils;
import com.sbpay.sgi.cron.validation.ProcessValidator;
import com.sbpay.sgi.cron.validation.impl.ProcessValidatorImpl;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 24/11/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * Clase que realiza la logica de la subida del archivo Upload
 * Incoming
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public final class UploadIncomingSrv extends CommonSrv {
    /** The Constant LOGGER. */
    private static final Logger LOG = Logger
            .getLogger( UploadIncomingSrv.class );
    /** The single instance. */
    private static UploadIncomingSrv singleINSTANCE = null;
    
    private String msgfileNotUp = null;
    
    private final ProcessCodeType PROCESO_ACTUAL = ProcessCodeType.SUBIDA_INCOMING_A_TRANSBANK;
    
    
    /**
     * Creates the instance.
     */
    private static void createInstance() {
        synchronized ( UploadIncomingSrv.class ) {
            if ( singleINSTANCE == null ) {
                singleINSTANCE = new UploadIncomingSrv();
                
            }
        }
    }
    
    /**
     * Patron singleton.
     * 
     * @return UploadIncomingSrv retorna instancia del servicio.
     */
    public static UploadIncomingSrv getInstance() {
        if ( singleINSTANCE == null ) {
            createInstance();
        }
        return singleINSTANCE;
    }
    
    /**
     * Metodo de restricion de sobreescritura de la clase.
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        super.clone();
        throw new CloneNotSupportedException();
    }
    
    /**
     * Constructor Privado.
     */
    private UploadIncomingSrv() {
        super();
    }
    
    /**
     * Metodo orquesta el uso de la subida del archivo Incoming.
     * 
     * @return return boolean true: upload ok / false: upload error.
     * @throws AppException
     *             Exception App.
     */
    public boolean uploadIncoming() throws AppException {
        
        ProcessValidator validadorProceso = new ProcessValidatorImpl();
        if (validadorProceso.isActiveProcess(PROCESO_ACTUAL)) {
            LOG.info("Proceso está activo");
        
        ParamUploadIncomingDTO params = null;
        boolean flag = false;
        List<String> filesInDirectory = null;
        List<String> fileUpload = null;
        
        // BUSCA LOS PARAMETROS BD UTILIZADOS EN EL CRON
        params = getParametrosUploadCron();
        
        if (validadorProceso.hasTheValidParametersForTheProcess(PROCESO_ACTUAL, params)) {
            ConfigFile config = new ConfigFile();
            config.setStarWith( params.getFormatFileInc() );
            config.setEndsWith( params.getFormatExtNameInc() );
            config.setStarWithCtr( params.getFormatFileInc() );
            config.setEndsWithCtr( params.getFormatExtNameIncCtr() );
            
            String pathSearch = params.getPathAbcInc();
            
            // BUSCA LISTA DE ARCHIVOS QUE POSEAN NOMENCLATURA DE
            // NOMBRES VALIDOS PARA PROCESAR Y QUE TENGAN ARCHIVOS DE
            // CONTROL
            filesInDirectory = getFileStartsWith( pathSearch,
                    config.getStarWith(), config.getEndsWithCtr(),
                    config.getEndsWith() );
            
            if ( filesInDirectory != null && filesInDirectory.size() > 0 ) {
                
                // BUSCA LOS ARCHIVOS QUE TENGAN UN REGISTRO EN LA
                // TABLA TBL_LOG_OUT
                fileUpload = getFileUpload( filesInDirectory, params );
                
                // SE BUSCA ARCHIVOS DE CONTROL DE ESOS INCOMING
                List<String> controlFileList = retrieveFilenameCtrList(
                        pathSearch, config );
                
                if ( fileUpload != null && fileUpload.size() > 0 ) {
                    
                    // BUSCA LA LISTA DE ARCHIVOS EXISTENTES EN EL
                    // SFTP DE TRANSBANK
                    List<String> listaFtpTbk = ClientFTPSrv.getInstance()
                            .listFileSFTP( params.getDataFTP(),
                                    params.getFtpPathInc() );
                    
                    // SUBE ARCHIVOS INCOMING Y DE CTRL
                    int countInc = subirArchivosIncoming( params, fileUpload,
                            listaFtpTbk, controlFileList );
                    
                    if ( countInc == fileUpload.size() ) {
                        flag = true;
                    }
                    else {
                        LOG.info( "Algunos archivos no pudieron ser subidos a TBK" );
                        MailSendSrv
                                .getInstance()
                                .sendMail(
                                        MsgErrorMail.ALERTA_FILE_UPLOAD_INCOMING
                                                .toString(),
                                        MsgErrorMail.ALERTA_FILE_UPLOAD_INCOMING_TXT
                                                .toString()
                                                .concat(
                                                        ConstantesUtil.SKIP_LINE
                                                                .toString() )
                                                .concat( msgfileNotUp == null ? "" : msgfileNotUp ) );
                    }
                    
                }
                else {
                    LOG.info( "Todo archivos incoming para upload estan ya procesados" );
                    MailSendSrv
                            .getInstance()
                            .sendMail(
                                    MsgErrorMail.ALERTA_FILE_PROCESSED_UPLOAD
                                            .toString(),
                                    MsgErrorMail.ALERTA_FILE_PROCESSED_UPLOAD_TXT
                                            .toString() );
                }
            }
            else {
                LOG.info( "No existen nuevos archivos incoming con formato valido para subir" );
                MailSendSrv.getInstance().sendMail(
                        MsgErrorMail.ALERTA_FILE_UPLOAD.toString(),
                        MsgErrorMail.ALERTA_FILE_UPLOAD_TXT.toString() );
            }
        }
        else {
            LOG.warn(MsgProceso.MSG_ERROR_FALTAN_PARAMETROS.toString());
            MailSendSrv.getInstance().sendMail(
                    MsgErrorMail.ALERTA_PRTS_UPLOAD.toString(),
                    
                    MsgErrorMail.ALERTA_PRTS_TXT_UPLOAD.toString() );
        }
        return flag;
        
        }else{
            String asunto = MsgProceso.MSG_PROCESO_DESACTIVADO_ASUNTO
                    .toString().replace( ":?:",
                            PROCESO_ACTUAL.getCodigoProceso() );
            String mensaje = MsgProceso.MSG_PROCESO_DESACTIVADO_BODY.toString()
                    .replace( ":?:", PROCESO_ACTUAL.getCodigoProceso() );
            MailSendSrv.getInstance().sendMail( asunto, mensaje );
        }
        return false;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param params
     * @param fileUpload
     * @param listaFtpTbk
     *            lista con los nombres de los archivos existentes en
     *            FTP TBK
     * @param controlFileList
     * @return
     * @since 1.X
     */
    private int subirArchivosIncoming( ParamUploadIncomingDTO params,
            List<String> fileToUploadList, List<String> listaFtpTbk,
            List<String> controlFileList ) {
        int count = 0;
        String filenameCtr = null;
        // RECORRE LA LISTA DE ARCHIVOS
        for ( String filename : fileToUploadList ) {
        	filenameCtr = "";
            try {
                // SI NO SE ENCUENTRA EL CTR NI EL INCOMING EN FTP SE
                // SUBE
                if ( listaFtpTbk != null
                        && !listaFtpTbk.contains( filename )
                        && !listaFtpTbk.contains( filename.replace(
                                params.getFormatExtNameIncCtr(),
                                params.getFormatExtNameInc() ) ) ) {
                    
                    // Busco archivo de control
                    filenameCtr = buscarArchivoContrl( filename,
                            controlFileList, params.getFormatExtNameInc(),
                            params.getFormatExtNameIncCtr() );
                   
                    // SUBO INCOMING A TBK
                    if ( ClientFTPSrv.getInstance().uploadFileSFTP(
                            params.getDataFTP(), params.getPathAbcInc(),
                            params.getFtpPathInc(), filename ) ) {
                        LOG.info( "ARCHIVO " + filename
                                + " SUBIDO CORRECTAMENTE A FTP = "
                                + params.getDataFTP().getIpHost() );
                        
                      
                        if ( filenameCtr != null ) {
                            if ( ClientFTPSrv.getInstance().uploadFileSFTP(
                                    params.getDataFTP(),
                                    params.getPathAbcInc(),
                                    params.getFtpPathInc(), filenameCtr ) ) {
                                LOG.info( "ARCHIVO " + filename
                                        + " SUBIDO CORRECTAMENTE A FTP = "
                                        + params.getDataFTP().getIpHost() );
                                
                                // MUEVE ARCHIVO INC A CARPETA BKP
                                CommonsUtils.moveFile( params.getPathAbcInc()
                                        + filename, params.getPathAbcIncBkp()
                                        + filename );
                                
                                // MUEVE ARCHIVO CTR A CARPETA BKP
                                CommonsUtils
                                        .moveFile( params.getPathAbcInc()
                                                + filenameCtr,
                                                params.getPathAbcIncBkp()
                                                        + filenameCtr );
                                
                                // UPDATE TABLA LOG
                                updateLog( filename,
                                        StatusProcessType.PROCESS_SUCESSFUL );
                                
                                count++;
                            }
                            else {
                                LOG.warn( "NO SE HA PODIDO SUBIR INCOMING" );
                            }
                            
                        }
                        else {
                            LOG.warn( "ARCHIVO : " + filename
                                    + " YA SE ENCUENTRA EN FTP TBK" );
                        }
                    }
                    else {
                        LOG.warn( "NO SE HA PODIDO SUBIR ARCHIVO INCOMING A BKP" );
                    }
                }
            }
            catch ( Exception e ) {
                LOG.error( "NO SE PUDO SUBIR ARCHIVO " + filename
                        + " A FTP, SE MUEVE A FORLDER ERROR : "
                        + params.getPathAbcIncError() );
                updateLog( filename, StatusProcessType.PROCESS_ERROR );
               
                // MUEVE A ERROR INCOMING
                CommonsUtils.moveFile( params.getPathAbcInc() + filename,
                        params.getPathAbcIncError()+ filename );
                
                // MUEVE A ERROR CTR
                if( filenameCtr != null && !filenameCtr.isEmpty()){
                	CommonsUtils.moveFile( params.getPathAbcInc()+ filenameCtr,params.getPathAbcIncError() + filenameCtr );	
                }
                msgfileNotUp.concat( filename ).concat(
                        ConstantesUtil.SKIP_LINE.toString() );
            }
        }
        return count;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 18/2/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @param filename
     * @param controlFileList
     * @return
     * @since 1.X
     */
    private String buscarArchivoContrl( String filename,
            List<String> controlFileList, String extName, String ctrExt ) {
        
        for ( String fileCTR : controlFileList ) {
            String fileAuxCTR = fileCTR.replace( ctrExt, "" );
            String fileAuxiNC = filename.replace( extName, "" );
            
            if ( fileAuxCTR.equals( fileAuxiNC ) ) {
                return fileCTR;
            }
        }
        
        return null;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param filename
     * @since 1.X
     */
    private void updateLog( String filename, StatusProcessType status ) {
        CommonsDao commons = null;
        try {
            commons = CommonsDaoFactory.getInstance().getNewEntidadDao();
            TblCronLogDTO dto = new TblCronLogDTO();
            dto.setFilename( filename );
            dto.setFileFlag( status.getValue() );
            
            if ( commons.updateTblCronLogByType( dto,
                    LogBD.TABLE_LOG_OUTGOING.getTable(),
                    FilterTypeSearch.FILTER_BY_NAME ) ) {
                LOG.info( "SE UPDATEA CORRECTAMENTE TABLA LOG _PARA FILE  :"
                                + filename );
                commons.endTx();
            }
            else {
                LOG.warn( "NO SE UPDATEA TABLA LOG _PARA FILE  :"
                        + filename );
                commons.rollBack();
            }
        }
        catch ( SQLException e ) {
            LOG.warn( "NO SE HA PODIDO REALIZAR UPDATE " + e );
        }
        finally {
            
            if ( commons != null ) {
                try {
                    commons.close();
                }
                catch ( SQLException e ) {
                   LOG.error( e.getMessage(),e );
                }
            }
        }
        
    }
    
    /**
     * Metodo busca los archivos que van a ser subidos al servidor FTP
     * tbk. En la tabla TBL_LOG_OUT, por fecha de archivo
     * 
     * @param fileDirectory
     *            Archivos candidatos.
     * @return Lista de archivos que deben ser prosados.
     * @throws AppException
     *             App Exception.
     */
    private List<String> getFileUpload( final List<String> fileDirectory,
            ParamUploadIncomingDTO params ) throws AppException {
        
        String dateFile = null;
        List<String> fileCarga = null;
        CommonsDao daoCommons = null;
        String fileAux = null;
        // CronLogDTO cronLogDTO = null;
        TblCronLogDTO dto = null;
        TblCronLogDTO dtoAux = new TblCronLogDTO();
        try {
            fileCarga = new ArrayList<String>();
            daoCommons = CommonsDaoFactory.getInstance().getNewEntidadDao();
            
            /* Lista de archivos que se pueden procesar. */
            for ( String fileName : fileDirectory ) {
                // formato PREFIJOmmdds.dat
                fileAux = fileName.replace( params.getFormatFileInc(), "" );
                
                // Rescata fecha archivo incoming
                dateFile = DateUtils.getDateFileTbk( fileAux );
                if ( dateFile != null ) {
                    dtoAux.setFecha( dateFile );
                    dto = daoCommons.getTblCronLogByType( dtoAux,
                            LogBD.TABLE_LOG_OUTGOING.getTable(),
                            FilterTypeSearch.FILTER_BY_DATE );
                    if ( dto != null
                            && StatusProcessType.PROCESS_PENDING.getValue()
                                    .equalsIgnoreCase( dto.getFileFlag() ) ) {
                        fileCarga.add( fileName );
                    }
                }
                else {
                    LOG.info( "Archivo no cumple con el formato para obtener la fecha al procesar los incoming "
                            + fileName );
                    /* Se mueve archivo a path de error incoming */
                    CommonsUtils.moveFile( params.getPathAbcInc() + fileName,
                            params.getPathAbcIncError() + fileName );
                    /*
                     * Envia correo con el nombre del archivo mal
                     * formateado
                     */
                    MailSendSrv.getInstance().sendMail(
                            MsgErrorFile.ERROR_FILE_FORMAT.toString(),
                            MsgErrorFile.ERROR_FILE_FORMAT_TXT.toString()
                                    .concat( fileName ) );
                    
                }
            }
            return fileCarga;
        }
        catch ( SQLException e ) {
            throw new AppException(
                    MsgErrorSQL.ERROR_QUERY_PROCESS_UPLOAD.toString(), e );
        }
        finally {
            try {
                if ( daoCommons != null ) {
                    daoCommons.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException(
                        MsgErrorSQL.ERROR_QUERY_PROCESS_CLOSE_UPLOAD.toString(),
                        e );
            }
        }
    }
    
    /**
     * Metodo busca los parametros del proceso Upload Incoming.
     * 
     * @return Dto con los parametros utilizados.
     * @throws AppException
     *             Exception App.
     */
    private ParamUploadIncomingDTO getParametrosUploadCron()
            throws AppException {
        CommonsDao daoCommon = null;
        List<ParametroDTO> paramDTOList = null;
        ParamUploadIncomingDTO pUpLoadDto = null;
        try {
            daoCommon = CommonsDaoFactory.getInstance().getNewEntidadDao();
            paramDTOList = daoCommon
                    .getParametroCodGrupoDato( ConstantesPRTS.CRON_TBK
                            .toString() );
            
            if ( paramDTOList != null ) {
                // PARAMETROS DE FTP DE TRANSBANK
                DataFTP dataFTP = new DataFTP();
                dataFTP.setIpHost( CommonsUtils.getCodiDato( paramDTOList,
                        ConstantesPRTS.SFTP_IP_UPLOAD_INCOMING.toString() ) );
                String port = CommonsUtils.getCodiDato(paramDTOList, ConstantesPRTS.SFTP_PORT_UPLOAD_INCOMING.toString()) ;
                dataFTP.setPort( Integer.parseInt((port != null && port.matches(ConstantesUtil.PATTERN_IS_NUMBER.toString()) ? port : ConstantesUtil.DEFAULT_PORT.toString())));
                
                dataFTP.setUser( CommonsUtils.getCodiDato( paramDTOList,
                        ConstantesPRTS.SFTP_USER_UPLOAD_INCOMING.toString() ) );
                dataFTP.setPassword( CommonsUtils.getCodiDato( paramDTOList,
                        ConstantesPRTS.SFTP_PASS_UPLOAD_INCOMING.toString() ) );
                
                // PARAMETROS DE PROCESO UPLOAD INCOMING A TBK
                pUpLoadDto = new ParamUploadIncomingDTO();
                
                // RUTA DE FOLDER EN SERVIDOR DE sbpay DONDE SE
                // BUSCARAN LOS INCOMING A SUBIR
                pUpLoadDto.setPathAbcInc( CommonsUtils.getCodiDato(
                        paramDTOList, ConstantesPRTS.PATH_ABC_INC.toString() ) );
                
                // RUTA DE FTP DE TRANSBANK DONDE SE SUBIRAN LOS
                // INCOMING
                pUpLoadDto.setFtpPathInc( CommonsUtils.getCodiDato(
                        paramDTOList, ConstantesPRTS.PATH_IN_UPLD_INCOMING.toString() ) );
                
                // RUTA DE SERVIDOR LOCAL DONDE SE MOVERA EL ARCHIVO
                // INCOMING EN CASO DE ERROR
                pUpLoadDto.setPathAbcIncError( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.PATH_ABC_INC_ERROR.toString() ) );
                
                // RUTA DE SERVIDOR LOCAL DONDE SE MOVERA EL ARCHIVO
                // INCOMING CUANDO FINALIZE OK
                pUpLoadDto.setPathAbcIncBkp( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.PATH_ABC_INC_BKP.toString() ) );
                
                // FORMATO DE NOMBRE DE INCOMING
                pUpLoadDto.setFormatFileInc( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.FORMAT_FILENAME_INC.toString() ) );
                
                // FORMATO DE EXTENSION DE INCOMING
                pUpLoadDto.setFormatExtNameInc( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.FORMAT_EXTNAME_INC.toString() ) );
                
                // FORMATO DE EXTENSION DE ARCHIVO DE CONTROL
                pUpLoadDto.setFormatExtNameIncCtr( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.FORMAT_EXTNAME_INC_CTR.toString() ) );
                
                // SETEA FTP
                pUpLoadDto.setDataFTP( dataFTP );
                
            }
            else {
                pUpLoadDto = null;
            }
        }
        catch ( SQLException e ) {
            throw new AppException(
                    MsgErrorSQL.ERROR_QUERY_PRTS_UPLOAD.toString(), e );
        }
        finally {
            try {
                if ( daoCommon != null ) {
                    daoCommon.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException(
                        MsgErrorSQL.ERROR_QUERY_PRTS_UPLOAD_CLOSE.toString(), e );
            }
        }
        return pUpLoadDto;
    }
    
}
