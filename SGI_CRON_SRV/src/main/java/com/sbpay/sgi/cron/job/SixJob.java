package com.sbpay.sgi.cron.job;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;

import com.sbpay.sgi.cron.step.StepGenerateContableAvance;
import com.sbpay.sgi.cron.step.StepProcessConciliar;
import com.sbpay.sgi.cron.step.StepUploadAsientosCntblAvance;



/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/11/2018, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * Job 6, ejecuta la tarea programada para realizar conciliacion de bice, canal, IC
 * registra en BD y contable avances
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */


public class SixJob implements Job {
	/** LOGGER CONSTANT **/
	private static final Logger LOG = Logger.getLogger(SixJob.class);

	@Override
	public void execute(JobExecutionContext context) {

		LOG.info("========== EJECUCION DEL SEXTO JOB =============");
			procesarConciliacion();
			
			generarContableAvances();
			subirAsientosContablesAvance();
		LOG.info("========== FIN EJECUCION DEL SEXTO JOB =========");

	}
	
	

	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/11/2018, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @since 1.X
	 */
	public void procesarConciliacion() {
		try {
			// LLAMA A PROCESAR
			StepProcessConciliar process = new StepProcessConciliar();
			process.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

	}


	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/12/2018, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @since 1.X
	 */
	public void generarContableAvances() {
		try {
			// LLAMA A PROCESAR
			StepGenerateContableAvance process = new StepGenerateContableAvance();
			process.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

	}
	

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/12/2018, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @throws Exception
	 * @since 1.X
	 */
	public void subirAsientosContablesAvance() {
		try {
			// LLAMA A LA TERCERA TAREA
			StepUploadAsientosCntblAvance uploadContable = new StepUploadAsientosCntblAvance();
			uploadContable.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

	}
	
}
