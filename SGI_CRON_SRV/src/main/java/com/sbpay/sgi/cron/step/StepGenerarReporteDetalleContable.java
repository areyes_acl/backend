package com.sbpay.sgi.cron.step;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.enums.MsgGenerarReporteDetalleContable;
import com.sbpay.sgi.cron.srv.export.GenerarReporteDetalleContableSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/XX/2019, (ACL) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA.</B>
 */
public class StepGenerarReporteDetalleContable {
	
	  /**
     * VARIABLES
     */
    private static final Logger LOG = Logger
            .getLogger( StepGenerarReporteDetalleContable.class );
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/XX/2019, (sbpay Chile) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @throws Exception
     * @since 1.X
     */
    public void execute() throws Exception {
    	try{
    		if(GenerarReporteDetalleContableSrv.getInstance().generarReporteDetalleContable()){
    			LOG.info( MsgGenerarReporteDetalleContable.SUCCESFUL_GENERATE_CNTBL_MSG.toString());
    		}
    		
    	}catch(AppException e){
    		LOG.error(e.getMessage(), e);
    		LOG.info( MsgGenerarReporteDetalleContable.ERROR_GEN_CNTBL_FILE.toString());
    		MailSendSrv.getInstance().sendMail(MsgGenerarReporteDetalleContable.ERROR_GEN_CNTBL_FILE.toString(),
    		          (e.getStackTraceDescripcion()!= null)?e.getStackTraceDescripcion(): e.getMessage());
    	}
    }

}
