package com.sbpay.sgi.cron.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.text.html.parser.Parser;

import oracle.jdbc.OracleCallableStatement;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.BaseDao;
import com.sbpay.sgi.cron.dto.BolLogDTO;
import com.sbpay.sgi.cron.dto.CronConfigDTO;
import com.sbpay.sgi.cron.dto.CronTaskParam;
import com.sbpay.sgi.cron.dto.ICCodeHomologationDTO;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.dto.ParametroUserDTO;
import com.sbpay.sgi.cron.dto.TblCronLogDTO;
import com.sbpay.sgi.cron.enums.ConstantesBD;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.FilterTypeSearch;
import com.sbpay.sgi.cron.enums.LogBD;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.enums.StatusProcessType;
import com.sbpay.sgi.cron.srv.utils.EncriptacionClave;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 18/12/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class CommonsDaoJdbc extends BaseDao<CommonsDao> implements CommonsDao {
	/** VARIABLE PARA EL LOGER */
	private static final Logger LOGGER = Logger.getLogger(CommonsDaoJdbc.class);

	/**
	 * Constructor con conexion.
	 * 
	 * @param connection
	 *            Conexion JDBC.
	 */
	public CommonsDaoJdbc(Connection connection) {
		super(connection);
	}

	/**
	 * 
	 * @param codGrupoDato
	 * @return
	 * @throws SQLException
	 */
	@Override
	public List<ParametroDTO> getParametroCodGrupoDato(final String codGrupoDato)
			throws SQLException {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<ParametroDTO> codGrupoDatoList = null;
		ParametroDTO codGrupoDatoDTO = null;
		try {
			StringBuilder str = new StringBuilder(
					"select COD_GRUPO_DATO,  COD_DATO, VALOR, DESCRIPCION, ")
					.append(" ID_USER, FECHA_CREACION, FECHA_MODIFICACION ")
					.append(" from ").append(ConstantesBD.ESQUEMA.toString())
					.append(".TBL_PRTS where COD_GRUPO_DATO = ? ");
			stmt = this.getConnection().prepareStatement(str.toString());
			stmt.setString(1, codGrupoDato);
			rs = stmt.executeQuery();
			codGrupoDatoList = new ArrayList<ParametroDTO>();
			while (rs.next()) {
				codGrupoDatoDTO = new ParametroDTO();
				codGrupoDatoDTO.setCodGrupoDato(rs.getString("COD_GRUPO_DATO"));
				codGrupoDatoDTO.setCodDato(rs.getString("COD_DATO"));
				codGrupoDatoDTO.setValor(rs.getString("VALOR"));
				codGrupoDatoDTO.setDescripcion(rs.getString("DESCRIPCION"));
				codGrupoDatoDTO.setIdUser(rs.getString("ID_USER"));
				codGrupoDatoDTO
						.setFechaCreacion(rs.getString("FECHA_CREACION"));
				codGrupoDatoDTO.setFechaModificacion(rs
						.getString("FECHA_MODIFICACION"));
				codGrupoDatoList.add(codGrupoDatoDTO);
			}
			return codGrupoDatoList;
		} finally {
			if (stmt != null) {
				stmt.close();
			}
			if (rs != null) {
				rs.close();
			}
		}
	}

	
	//Método para devolver los parámetros de una tarea determinada
	@Override
	public ParametroDTO getParametroCodDato(String codDato) throws SQLException {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		ParametroDTO codGrupoDatoDTO = null;
		try {
			StringBuilder str = new StringBuilder(
					"select COD_GRUPO_DATO,  COD_DATO, VALOR, DESCRIPCION, ")
					.append(" ID_USER, FECHA_CREACION, FECHA_MODIFICACION from ")
					.append(ConstantesBD.ESQUEMA.toString())
					.append(".TBL_PRTS ").append(" where COD_DATO = ? ");
			stmt = this.getConnection().prepareStatement(str.toString());
			stmt.setString(1, codDato);
			rs = stmt.executeQuery();

			if (rs.next()) {
				codGrupoDatoDTO = new ParametroDTO();
				codGrupoDatoDTO.setCodGrupoDato(rs.getString("COD_GRUPO_DATO"));
				codGrupoDatoDTO.setCodDato(rs.getString("COD_DATO"));
				codGrupoDatoDTO.setValor(rs.getString("VALOR"));
				codGrupoDatoDTO.setDescripcion(rs.getString("DESCRIPCION"));
				codGrupoDatoDTO.setIdUser(rs.getString("ID_USER"));
				codGrupoDatoDTO
						.setFechaCreacion(rs.getString("FECHA_CREACION"));
				codGrupoDatoDTO.setFechaModificacion(rs
						.getString("FECHA_MODIFICACION"));
			}
			return codGrupoDatoDTO;
		} finally {
			if (stmt != null) {
				stmt.close();
			}
			if (rs != null) {
				rs.close();
			}
		}
	}

	@Override
	public List<ParametroUserDTO> getParametroCodGrupoDatoUser(
			final String codGrupoDato) throws SQLException {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<ParametroUserDTO> codGrupoDatoList = null;
		ParametroUserDTO codGrupoDatoDTO = null;
		try {
			StringBuilder str = new StringBuilder(
					"select COD_GRUPO_DATO,  COD_DATO, VALOR, DESCRIPCION, ")
					.append(" ID_USER, FECHA_CREACION, FECHA_MODIFICACION from ")
					.append(ConstantesBD.ESQUEMA.toString())
					.append(".TBL_PRTS_USERS ")
					.append(" where COD_GRUPO_DATO = ? ");
			stmt = this.getConnection().prepareStatement(str.toString());
			stmt.setString(1, codGrupoDato);
			rs = stmt.executeQuery();
			codGrupoDatoList = new ArrayList<ParametroUserDTO>();
			while (rs.next()) {
				codGrupoDatoDTO = new ParametroUserDTO();
				codGrupoDatoDTO.setCodGrupoDato(rs.getString("COD_GRUPO_DATO"));
				codGrupoDatoDTO.setCodDato(rs.getString("COD_DATO"));
				codGrupoDatoDTO.setValor(rs.getString("VALOR"));
				codGrupoDatoDTO.setDescripcion(rs.getString("DESCRIPCION"));
				codGrupoDatoDTO.setIdUser(rs.getString("ID_USER"));
				codGrupoDatoDTO
						.setFechaCreacion(rs.getString("FECHA_CREACION"));
				codGrupoDatoDTO.setFechaModificacion(rs
						.getString("FECHA_MODIFICACION"));
				codGrupoDatoList.add(codGrupoDatoDTO);
			}
			return codGrupoDatoList;
		} finally {
			if (stmt != null) {
				stmt.close();
			}
			if (rs != null) {
				rs.close();
			}
		}
	}

	@Override
	public ParametroUserDTO getParametroCodDatoUser(final String codDato)
			throws SQLException {
		PreparedStatement stmt = null;
		ResultSet rs = null;

		ParametroUserDTO codGrupoDatoDTO = null;
		try {
			StringBuilder str = new StringBuilder(
					"select COD_GRUPO_DATO,  COD_DATO, VALOR, DESCRIPCION, ")
					.append(" ID_USER, FECHA_CREACION, FECHA_MODIFICACION from ")
					.append(ConstantesBD.ESQUEMA.toString())
					.append(".TBL_PRTS_USERS ").append(" where COD_DATO = ? ");
			stmt = this.getConnection().prepareStatement(str.toString());
			stmt.setString(1, codDato);
			rs = stmt.executeQuery();

			if (rs.next()) {
				codGrupoDatoDTO = new ParametroUserDTO();
				codGrupoDatoDTO.setCodGrupoDato(rs.getString("COD_GRUPO_DATO"));
				codGrupoDatoDTO.setCodDato(rs.getString("COD_DATO"));
				codGrupoDatoDTO.setValor(rs.getString("VALOR"));
				codGrupoDatoDTO.setDescripcion(rs.getString("DESCRIPCION"));
				codGrupoDatoDTO.setIdUser(rs.getString("ID_USER"));
				codGrupoDatoDTO
						.setFechaCreacion(rs.getString("FECHA_CREACION"));
				codGrupoDatoDTO.setFechaModificacion(rs
						.getString("FECHA_MODIFICACION"));
			}
			return codGrupoDatoDTO;
		} finally {
			if (stmt != null) {
				stmt.close();
			}
			if (rs != null) {
				rs.close();
			}
		}
	}

	@Override
	public long getSecuencia(final String secuencia) throws SQLException {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		long lSecuencia = 0;
		try {
			StringBuilder str = new StringBuilder("SELECT ")
					.append(ConstantesBD.ESQUEMA.toString()).append(".")
					.append(secuencia).append(".nextval  FROM DUAL");
			stmt = this.getConnection().prepareStatement(str.toString());
			rs = stmt.executeQuery();
			while (rs.next()) {
				lSecuencia = rs.getLong(1);
			}
			return lSecuencia;
		} finally {
			if (stmt != null) {
				stmt.close();
			}
			if (rs != null) {
				rs.close();
			}
		}
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 18/12/2015, (ACL-sbpay) - versi�n inicial
	 * </ul>
	 * <p>
	 * 
	 * @param dto
	 * @param tableName
	 * @return
	 * @throws AppException
	 * @see com.sbpay.sgi.cron.dao.CommonsDao#getTblCronLogByFilename(com.sbpay.sgi.cron.dto.TblCronLogDTO,
	 *      java.lang.String)
	 * @since 1.X
	 */
	@Override
	public TblCronLogDTO getTblCronLogByType(TblCronLogDTO dto,
			String tableName, FilterTypeSearch filter) throws SQLException {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		TblCronLogDTO cronLog = null;
		String baseQuery = "select FECHA, FILE_NAME, FILE_FLAG, FILE_TS ";

		try {
			StringBuilder str = new StringBuilder(baseQuery).append("from ")
					.append(tableName).append(" where ");

			if (FilterTypeSearch.FILTER_BY_NAME.equals(filter)) {
				str.append(filter.getColumnName()).append("= ? ");
				str.append(" ORDER BY FECHA DESC");

			}

			if (FilterTypeSearch.FILTER_BY_DATE.equals(filter)) {
				str.append("TRUNC(").append(filter.getColumnName())
						.append(") = to_date(?,'DD/MM/YY')");
			}
			if (FilterTypeSearch.FILTER_BY_AAAAMM.equals(filter)) {
				str.append(filter.getColumnName()).append(
						" >= to_date(?,'DD/MM/YY') ");
			}

			// PREPARA LA CONSULTA CON LA QUERY SEÑALADA
			stmt = this.getConnection().prepareStatement(str.toString());

			if (FilterTypeSearch.FILTER_BY_NAME.equals(filter)) {
				stmt.setString(1, dto.getFilename());
			}

			if (FilterTypeSearch.FILTER_BY_DATE.equals(filter)
					|| FilterTypeSearch.FILTER_BY_AAAAMM.equals(filter)) {
				stmt.setString(1, dto.getFecha());
			}

			rs = stmt.executeQuery();

			if (rs.next()) {
				cronLog = new TblCronLogDTO();
				cronLog.setFecha(rs.getString("FECHA"));
				cronLog.setFileFlag(rs.getString("FILE_FLAG"));
				cronLog.setFilename(rs.getString("FILE_NAME"));
			}
			
			return cronLog;
		} finally {
			if (stmt != null) {
				stmt.close();
			}
			if (rs != null) {
				rs.close();
			}

		}
	}

	/**
	 * Metodo generico que realiza update de una tabla log de acuerdo al filtro
	 * ingresado SELECT * FROM TBL_LOG_INC WHERE FILE_NAME = 'OUP4908051.DAT'
	 * AND SID = (SELECT max(SID) FROM TBL_LOG_INC WHERE FILE_NAME =
	 * 'OUP4908051.DAT');
	 */
	@Override
	public boolean updateTblCronLogByType(TblCronLogDTO dto, String tableName,
			FilterTypeSearch filter) throws SQLException {
		PreparedStatement stmt = null;
		try {
			final StringBuilder str = new StringBuilder().append("UPDATE ")
					.append(ConstantesBD.ESQUEMA.toString())
					.append(ConstantesUtil.POINT.toString()).append(tableName)
					.append(" SET FILE_FLAG = ? ")
					.append(", FILE_TS = SYSDATE ").append(" WHERE ");

			if (FilterTypeSearch.FILTER_BY_NAME.equals(filter)) {
				str.append(filter.getColumnName()).append("= ? ");
				str.append(" AND SID = (SELECT MAX(SID) FROM ")
						.append(tableName).append(" WHERE FILE_NAME = ? )");
			}

			if (FilterTypeSearch.FILTER_BY_DATE.equals(filter)) {
				str.append("TRUNC(").append(filter.getColumnName())
						.append(") = to_date(?,'DD/MM/YY')");
			}
			stmt = this.getConnection().prepareStatement(str.toString());

			// CAMPO A UPDATEAR
			setStringNull(dto.getFileFlag(), 1, stmt);

			// CAMPOS A SETEAR EN LA CONSULTA
			if (FilterTypeSearch.FILTER_BY_NAME.equals(filter)) {
				setStringNull(dto.getFilename(), 2, stmt);
				setStringNull(dto.getFilename(), 3, stmt);
			}

			if (FilterTypeSearch.FILTER_BY_DATE.equals(filter)) {
				setStringNull(dto.getFecha(), 2, stmt);
			}

			return (stmt.executeUpdate() > 0);
		} finally {
			if (stmt != null) {
				stmt.close();
			}
		}
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 18/12/2015, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @param dto
	 * @param tableName
	 * @param filter
	 * @return
	 * @throws SQLException
	 * @see com.sbpay.sgi.cron.dao.CommonsDao#updateTblCronLogIncByType(com.sbpay.sgi.cron.dto.TblCronLogDTO,
	 *      java.lang.String, com.sbpay.sgi.cron.enums.FilterTypeSearch)
	 * @since 1.X
	 */
	@Override
	public boolean updateTblCronLogIncByType(final TblCronLogDTO dto,
			final String tableName, final FilterTypeSearch filter)
			throws SQLException {
		PreparedStatement stmt = null;
		try {
			final StringBuilder str = new StringBuilder().append("UPDATE ")
					.append(ConstantesBD.ESQUEMA.toString())
					.append(ConstantesUtil.POINT.toString()).append(tableName)
					.append(" SET  FILE_FLAG_INT = ? ")
					.append(", FILE_TS_INT = SYSDATE ").append(" WHERE ");

			if (FilterTypeSearch.FILTER_BY_NAME.equals(filter)) {
				str.append(filter.getColumnName()).append("= ? ");
			}

			if (FilterTypeSearch.FILTER_BY_DATE.equals(filter)) {
				str.append("TRUNC(").append(filter.getColumnName())
						.append(") = to_date(?,'DD/MM/YY')");
			}

			stmt = this.getConnection().prepareStatement(str.toString());

			setStringNull(dto.getFileFlag(), 1, stmt);
			if (FilterTypeSearch.FILTER_BY_NAME.equals(filter)) {
				setStringNull(dto.getFilename(), 2, stmt);
			}

			if (FilterTypeSearch.FILTER_BY_DATE.equals(filter)) {
				setStringNull(dto.getFecha(), 2, stmt);
			}
			return (stmt.executeUpdate() > 0);
		} finally {
			if (stmt != null) {
				stmt.close();
			}
		}
	}
	
	
	

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 18/12/2015, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @param dto
	 * @return
	 * @throws SQLException
	 * @see com.sbpay.sgi.cron.dao.CommonsDao#saveTblCronLogInc(com.sbpay.sgi.cron.dto.TblCronLogDTO)
	 * @since 1.X
	 */
	@Override
	public boolean saveTblCronLog(TblCronLogDTO dto, LogBD logTable)
			throws SQLException {
		PreparedStatement stmt = null;

		System.out.println("logTable: " + logTable + " dto:" + dto);

		try {
			final StringBuilder str = new StringBuilder();

			str.append("INSERT INTO ").append(ConstantesBD.ESQUEMA.toString())
					.append(".").append(logTable.getTable())
					.append(" (SID,FECHA,")
					.append(" FILE_NAME,FILE_FLAG,FILE_TS)")
					.append(" values (").append(logTable.getSequence())
					.append(".nextval,to_date(?,'DD/MM/RR'),?,?,SYSDATE)");

			stmt = this.getConnection().prepareStatement(str.toString());

			setStringNull(dto.getFecha(), 1, stmt);
			setStringNull(dto.getFilename(), 2, stmt);
			setStringNull(dto.getFileFlag(), 3, stmt);

			return (stmt.executeUpdate() > 0);
		} finally {
			if (stmt != null) {
				stmt.close();
			}
		}

	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 18/12/2015, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * DEBERIA UTILIZARSE EL GENERAL PERO SE DEJA POR QUE LA FECHA ES DISTINTA
	 * 
	 * @param dto
	 * @return
	 * @throws SQLException
	 * @see com.sbpay.sgi.cron.dao.CommonsDao#saveTblCronLogInc(com.sbpay.sgi.cron.dto.TblCronLogDTO)
	 * @since 1.X
	 */
	@Override
	public boolean saveTblCronLogCM(TblCronLogDTO dto, LogBD logTable)
			throws SQLException {
		PreparedStatement stmt = null;
		try {
			final StringBuilder str = new StringBuilder()
					.append("INSERT INTO ")
					.append(ConstantesBD.ESQUEMA.toString()).append(".")
					.append(logTable.getTable()).append(" (SID,FECHA,")
					.append(" FILE_NAME,FILE_FLAG,FILE_TS)")
					.append(" values (").append(logTable.getSequence())
					.append(".nextval,?,?,?,SYSDATE)");

			stmt = this.getConnection().prepareStatement(str.toString());

			setStringNull(dto.getFecha(), 1, stmt);
			setStringNull(dto.getFilename(), 2, stmt);
			setStringNull(dto.getFileFlag(), 3, stmt);
			return (stmt.executeUpdate() > 0);
		} finally {
			if (stmt != null) {
				stmt.close();
			}
		}

	}
	
	//Update TBL_CRON_CONFIG
	
	

	@Override
	public List<ICCodeHomologationDTO> obtenerCodigosIc() throws SQLException {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<ICCodeHomologationDTO> listaCodigos = null;

		try {
			StringBuilder str = new StringBuilder(
					"SELECT SID, COD_IC, COD_TRANSBANK, ")
					.append(" DESCRIPCION, ACTIVO, FECHA_ACTUALIZACION")
					.append(" FROM ").append(ConstantesBD.ESQUEMA.toString())
					.append(".TBL_PRTS_IC_CODE");

			stmt = this.getConnection().prepareStatement(str.toString());

			rs = stmt.executeQuery();
			listaCodigos = new ArrayList<ICCodeHomologationDTO>();

			while (rs.next()) {
				ICCodeHomologationDTO codigo = new ICCodeHomologationDTO();

				codigo.setSid(rs.getInt("SID"));
				codigo.setCodigoIC(rs.getString("COD_IC"));
				codigo.setCodigoTransbank(rs.getString("COD_TRANSBANK"));
				codigo.setDescripcion(rs.getString("DESCRIPCION"));
				codigo.setActivo(rs.getBoolean("ACTIVO"));
				codigo.setFechaActualizacion("FECHA_ACTUALIZACION");
				listaCodigos.add(codigo);
			}
			return listaCodigos;
		} finally {
			if (stmt != null) {
				stmt.close();
			}
			if (rs != null) {
				rs.close();
			}
		}
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @return
	 * @throws SQLException
	 * @see com.sbpay.sgi.cron.dao.CommonsDao#getRegisterWithMaxDate()
	 * @since 1.X
	 */
	@Override
	public TblCronLogDTO getRegisterWithMaxDate(LogBD log) throws SQLException {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		TblCronLogDTO registro = null;

		try {
			StringBuilder str = new StringBuilder(
					"SELECT SID, to_char(FECHA,'DD/MM/YYYY') AS FECHA, FILENAME, ")
					.append(" FILE_FLAG, FILE_TS_DOWNLOAD, FILE_TS_UPLOAD")
					.append(" FROM ")
					.append(ConstantesBD.ESQUEMA.toString())
					.append(ConstantesUtil.POINT.toString())
					.append(log.getTable())
					.append(" WHERE FECHA = (SELECT MAX(FECHA) FROM tbl_log_bol)");

			stmt = this.getConnection().prepareStatement(str.toString());

			rs = stmt.executeQuery();

			while (rs.next()) {
				registro = new TblCronLogDTO();
				registro.setSid(rs.getInt("SID"));
				registro.setFecha(rs.getString("FECHA"));
				registro.setFilename(rs.getString("FILENAME"));
				registro.setFileFlag(rs.getString("FILE_FLAG"));
				registro.setFileTs(rs.getString("FILE_TS_DOWNLOAD"));
				registro.setFileTSUpload("FILE_TS_UPLOAD");
			}
			return registro;
		} finally {
			if (stmt != null) {
				stmt.close();
			}
			if (rs != null) {
				rs.close();
			}
		}
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @param logDto
	 * @param logTable
	 * @return
	 * @throws SQLException
	 * @see com.sbpay.sgi.cron.dao.CommonsDao#saveTblCronLogBOL(com.sbpay.sgi.cron.dto.TblCronLogDTO,
	 *      com.sbpay.sgi.cron.enums.LogBD)
	 * @since 1.X
	 */
	@Override
	public boolean saveTblCronLogBOL(TblCronLogDTO logDto, LogBD logTable)
			throws SQLException {
		PreparedStatement stmt = null;
		try {
			final StringBuilder str = new StringBuilder();
			str.append("INSERT INTO ");
			str.append(ConstantesBD.ESQUEMA.toString()).append(".");
			str.append(logTable.getTable());
			str.append(" (SID, FECHA, FILENAME, FILE_FLAG, FILE_TS_DOWNLOAD)");
			str.append(" values (");
			str.append(logTable.getSequence());
			str.append(".nextval,to_date(?,'DD/MM/RR'),?,?,SYSDATE)");
			System.out.println(str.toString());
			stmt = this.getConnection().prepareStatement(str.toString());

			setStringNull(logDto.getFecha(), 1, stmt);
			setStringNull(logDto.getFilename(), 2, stmt);
			setIntNull(logDto.getFileFlag(), 3, stmt);
			return (stmt.executeUpdate() > 0);

		} finally {
			if (stmt != null) {
				stmt.close();
			}
		}
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * Metodo que busca si un proceso esta activo o no dado el codigo del
	 * proceso
	 * 
	 * @param configProcess
	 * @return
	 * @throws SQLException
	 * @see com.sbpay.sgi.cron.dao.CommonsDao#isProcessActive(com.sbpay.sgi.cron.enums.ConfigProcessType)
	 * @since 1.X
	 */
	@Override
	public CronConfigDTO isProcessActive(ProcessCodeType configProcess)
			throws SQLException {
		String tablaConfig = "TBL_CRON_CONFIG";
		PreparedStatement stmt = null;
		ResultSet rs = null;
		CronConfigDTO cronConfig = null;

		try {
			final StringBuilder str = new StringBuilder();
			str.append("SELECT  SID, ESTADO, CODIGO, DESCRIPCION  FROM ");
			str.append(ConstantesBD.ESQUEMA.toString()).append(".")
					.append(tablaConfig);
			str.append(" WHERE CODIGO =  ?");

			stmt = this.getConnection().prepareStatement(str.toString());
			setStringNull(configProcess.getCodigoProceso(), 1, stmt);

			rs = stmt.executeQuery();

			while (rs.next()) {
				cronConfig = new CronConfigDTO();
				cronConfig.setSid(rs.getLong("SID"));
				cronConfig.setEstado(rs.getInt("ESTADO"));
				cronConfig.setCodigo(rs.getString("CODIGO"));
				cronConfig.setDescripcion(rs.getString("DESCRIPCION"));
			}

		} finally {
			if (stmt != null) {
				stmt.close();
			}
			if (rs != null) {
				rs.close();
			}
		}

		return cronConfig;
	}

@Override
	public boolean updateTblCronConfig(String paramName, int est)
			throws SQLException {
		String tablaConfig = "TBL_CRON_CONFIG";
		PreparedStatement stmt = null;
		//CronConfigDTO cronConfig = isProcessActive(configProcess);
		//cronConfig.setEstado(est);

		try {
		final StringBuilder str = new StringBuilder()
					.append("UPDATE ")
					.append(ConstantesBD.ESQUEMA.toString()).append(".")
					.append(tablaConfig).append(" set estado = ").append(est)
					.append(" where ")
					.append("codigo = '").append(paramName).append("'");

			stmt = this.getConnection().prepareStatement(str.toString());
			
			System.out.println(str.toString());
			
			
			
			return (stmt.executeUpdate()>0);

		} finally {
			if (stmt != null) {
				stmt.close();
			}
			
		}

		
	}

@Override
public boolean updateValorParams(int sid, String valor)
		throws SQLException {
	String tablaConfig = "TBL_PRTS";
	PreparedStatement stmt = null;
	//CronConfigDTO cronConfig = isProcessActive(configProcess);
	//cronConfig.setEstado(est);

	try {
	final StringBuilder str = new StringBuilder()
				.append("UPDATE ")
				.append(ConstantesBD.ESQUEMA.toString()).append(".")
				.append(tablaConfig).append(" set valor = '").append(valor).append("'")
				.append(" where ")
				.append("sid = ").append(sid);

		stmt = this.getConnection().prepareStatement(str.toString());
		
		System.out.println(str.toString());
		
		
		
		return (stmt.executeUpdate()>0);

	} finally {
		if (stmt != null) {
			stmt.close();
		}
		
	}

	
}



//Método que me devuelve un usuario que cohincida con el user y el pass obtenidos por parámetros
@Override
public boolean getUsuario(String username, String password) throws SQLException
{
	User user = new User();
	EncriptacionClave claveencripta = new EncriptacionClave();
	String encrippass = claveencripta.getStringMessageDigest(password, claveencripta.SHA256);
	PreparedStatement stmt = null;
	ResultSet rs = null;
	boolean var = false;
	
	try{
		StringBuilder query = new StringBuilder(" select U.RUT, U.DV, C.CONTRASENA, TR.NOMBRE_ROL ")
				.append(" from ").append(ConstantesBD.ESQUEMA.toString())
				.append(".TBL_USUARIO_IG U ")
				.append(" INNER JOIN ").append(ConstantesBD.ESQUEMA.toString()) 
				.append(".TBL_CONTRASENA C ON U.SID = C.SID_USR ")
				.append(" INNER JOIN ").append(ConstantesBD.ESQUEMA.toString())
				.append(".TBL_ROL_USUARIO TRU ON U.SID = TRU.SID_USR ")
				.append(" INNER JOIN ").append(ConstantesBD.ESQUEMA.toString())
				.append(".TBL_ROL TR ON TRU.SID_ROL = TR.SID ")
				.append(" where U.ESTADO = ").append("1")
				.append(" and C.SID_CONTRASENA_ESTADO = ").append("2")
				.append(" and CONCAT(U.RUT, U.DV)= ").append(username)
				.append(" and C.CONTRASENA = '").append(encrippass)
				.append("'")
				.append(" and TR.NOMBRE_ROL = ").append("'").append("ADMIN_CRON_WEB").append("'");
		
		
		stmt = this.getConnection().prepareStatement(query.toString());
		rs = stmt.executeQuery();
		if(rs.next())
		{
			var = true;
			
		}
		
	}
	finally {
		if (stmt != null) {
			stmt.close();
		}
		if (rs != null) {
			rs.close();
		}
	}
	

	return var;
	
}


	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @param filename
	 * @param status
	 * @return
	 * @throws SQLException
	 * @see com.sbpay.sgi.cron.dao.CommonsDao#updateLogBol(java.lang.String,
	 *      com.sbpay.sgi.cron.enums.StatusProcessType)
	 * @since 1.X
	 */
	@Override
	public boolean updateLogBol(String filename, StatusProcessType status)
			throws SQLException {
		PreparedStatement stmt = null;
		try {
			final StringBuilder str = new StringBuilder().append("UPDATE ")
					.append(ConstantesBD.ESQUEMA.toString())
					.append(ConstantesUtil.POINT.toString())
					.append(LogBD.TABLE_LOG_BOL.getTable())
					.append(" SET FILE_FLAG = ? ,")
					.append(" FILE_TS_UPLOAD = SYSDATE ").append(" WHERE ")
					.append(" FILENAME = ? ");

			stmt = this.getConnection().prepareStatement(str.toString());

			// CAMPO A UPDATEAR
			setStringNull(status.getValue(), 1, stmt);
			setStringNull(filename, 2, stmt);

			return (stmt.executeUpdate() > 0);
		} finally {
			if (stmt != null) {
				stmt.close();
			}
		}
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @param filename
	 * @return
	 * @throws SQLException
	 * @see com.sbpay.sgi.cron.dao.CommonsDao#getLogBolRegisterByName(java.lang.String)
	 * @since 1.X
	 */
	@Override
	public BolLogDTO getBolLogRegisterByName(String filename)
			throws SQLException {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		BolLogDTO log = null;

		try {
			final StringBuilder str = new StringBuilder();
			str.append("SELECT SID, FECHA, FILENAME, FILE_FLAG, FILE_TS_DOWNLOAD, FILE_TS_UPLOAD FROM ");
			str.append(ConstantesBD.ESQUEMA.toString()).append(".")
					.append(LogBD.TABLE_LOG_BOL.getTable());
			str.append(" WHERE FILENAME =  ?");

			stmt = this.getConnection().prepareStatement(str.toString());
			setStringNull(filename, 1, stmt);

			rs = stmt.executeQuery();

			while (rs.next()) {
				log = new BolLogDTO();
				log.setSid(rs.getLong("SID"));
				log.setFecha(rs.getString("FECHA"));
				log.setFilename(rs.getString("FILENAME"));
				log.setFileFlag(rs.getString("FILE_FLAG"));
				log.setFileTsDownload(rs.getString("FILE_TS_DOWNLOAD"));
				log.setFileTSUpload(rs.getString("FILE_TS_UPLOAD"));
			}

		} finally {
			if (stmt != null) {
				stmt.close();
			}

			if (rs != null) {
				rs.close();
			}
		}

		return log;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 18/12/2019, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * solo para avnces con transferencia
	 * 
	 * @param dto
	 * @return
	 * @throws SQLException
	 * @since 1.X
	 */
	@Override
	public boolean saveTblCronLogContableAvance(TblCronLogDTO dto)
			throws SQLException {
		CallableStatement callableStatement = null;

		String SP_SIG_INSERT_LOG_CONTABLE = "{call SP_SIG_INSERT_LOG_CONTABLE(?,?,to_date(?,'DD/MM/RR'),?,?)}";

		LOGGER.info("******saveTblCronLogContableAvance ->SE EJECUTA STORE PROCEDURE SP_SIG_INSERT_LOG_CONTABLE*****");

		try {

			callableStatement = (OracleCallableStatement) this.getConnection()
					.prepareCall(SP_SIG_INSERT_LOG_CONTABLE);

			callableStatement.registerOutParameter(1, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(2, java.sql.Types.NUMERIC);
			callableStatement.setString(3, dto.getFecha());
			callableStatement.setString(4, dto.getFilename());
			callableStatement.setString(5, dto.getFileFlag());

			callableStatement.execute();

			Integer codigoError = callableStatement.getInt(1);
			String warning = callableStatement.getString(2);

			LOGGER.info("codigoError : " + codigoError);
			LOGGER.info("warning : " + warning);

			if (codigoError != 0) {
				LOGGER.warn("Cod error " + codigoError);
				LOGGER.info("warning : " + warning);
				return Boolean.FALSE;
			}
			return Boolean.TRUE;
		} finally {

			if (callableStatement != null) {
				callableStatement.close();
			}

		}

	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 08/04/2019, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * obtiene sid de tbl_operador
	 * @param tableName, operatorName
	 * @return sidOperador
	 * @throws SQLException
	 * @since 1.X
	 */
	@Override
	public Integer getOperador(final String operatorName) throws SQLException {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Integer sidOperador = 0;
		
		try {
			StringBuilder str = new StringBuilder().append("select SID from ")
					.append(ConstantesBD.ESQUEMA.toString())
					.append(ConstantesUtil.POINT.toString())
					.append("TBL_OPERADOR where OPERADOR = '"+operatorName+"'");
								
			stmt = this.getConnection().prepareStatement(str.toString());
			rs = stmt.executeQuery();
			
			if (rs.next()) {
				sidOperador = rs.getInt("SID");
			}
			return sidOperador;
		} finally {
			if (stmt != null) {
				stmt.close();
			}

			if (rs != null) {
				rs.close();
			}

		}
	}
	
	@Override
	public Integer getIDOperador(final String operatorId) throws SQLException {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Integer sidOperador = 0;
		
		try {
			StringBuilder str = new StringBuilder().append("select SID from ")
					.append(ConstantesBD.ESQUEMA.toString())
					.append(ConstantesUtil.POINT.toString())
					.append("TBL_OPERADOR where SID = '"+operatorId+"'");
								
			stmt = this.getConnection().prepareStatement(str.toString());
			rs = stmt.executeQuery();
			
			if (rs.next()) {
				sidOperador = rs.getInt("SID");
			}
			return sidOperador;
		} finally {
			if (stmt != null) {
				stmt.close();
			}

			if (rs != null) {
				rs.close();
			}

		}
	}
	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 08/04/2019, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * obtiene sid de tbl_operador
	 * @param tableName, operatorName
	 * @return sidOperador
	 * @throws SQLException
	 * @since 1.X
	 */
	@Override
	public String getBinOperador(final String operatorName) throws SQLException {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String sidOperador = "";
		
		try {
			StringBuilder str = new StringBuilder().append("select OBIN from ")
					.append(ConstantesBD.ESQUEMA.toString())
					.append(ConstantesUtil.POINT.toString())
					.append("TBL_OPERADOR where OPERADOR = '"+operatorName+"'");
								
			stmt = this.getConnection().prepareStatement(str.toString());
			rs = stmt.executeQuery();
			
			if (rs.next()) {
				sidOperador = rs.getString("OBIN");
			}
			return sidOperador;
		} finally {
			if (stmt != null) {
				stmt.close();
			}

			if (rs != null) {
				rs.close();
			}

		}
	}

	@Override
	public List<Integer> getAllOperadorProducto() throws SQLException {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<Integer> idsOperadores = new ArrayList<Integer>(); 
		
		try {
			StringBuilder str = new StringBuilder().append("select op.SID from ")
					.append(ConstantesBD.ESQUEMA.toString())
					.append(ConstantesUtil.POINT.toString())
					.append("tbl_operador_producto op inner join ").append(ConstantesBD.ESQUEMA.toString())
					.append(ConstantesUtil.POINT.toString()).append("tbl_operador o on op.ID_OPERADOR = o.sid WHERE ACTIVO = 1");
								
			stmt = this.getConnection().prepareStatement(str.toString());
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				idsOperadores.add(rs.getInt("SID"));
			}
			
			return idsOperadores;
		} finally {
			if (stmt != null) {
				stmt.close();
			}

			if (rs != null) {
				rs.close();
			}

		}
	}
	
	
	@Override
	public List<CronTaskParam> datosParametros(int tarea) throws SQLException
	{
		PreparedStatement stmt = null;
		ResultSet result = null;
		
		CronTaskParam param = null;
		List<CronTaskParam> lista = new ArrayList<CronTaskParam>();
		
		StringBuilder query = new StringBuilder().append("select tp.SID, tp.COD_DATO, tp.VALOR, tp.DESCRIPCION, tp.COD_GRUPO_DATO from ")
				.append(ConstantesBD.ESQUEMA.toString())
				.append(ConstantesUtil.POINT.toString())
				.append("TBL_PRTS tp where ");
		
		
		switch (tarea) {
		case 1:
			query.append("(")
				.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_TBK").append("'")
				.append(")")
				.append(" and (tp.COD_DATO = ").append("'").append("SFTP_IP_DOWNLOAD_OUTGOING").append("'")
				.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PORT_DOWNLOAD_OUTGOING").append("'")
				.append(" or  tp.COD_DATO = ").append("'").append("SFTP_USER_DOWNLOAD_OUTGOING").append("'")
				.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PASS_DOWNLOAD_OUTGOING").append("'")
				.append(" or  tp.COD_DATO = ").append("'").append("PATH_OUT_DWNLD_OUTGOING").append("'")
				.append(" or  tp.COD_DATO = ").append("'").append("PATH_ACL_INC").append("'")
				.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILENAME_OUT").append("'")
				.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_OUT").append("'")
				.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_OUT_CTR").append("'")
				.append(")");
			break;
			
		case 2:
			
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_TBK").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("SFTP_IP_DOWNLOAD_COMISSION").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PORT_DOWNLOAD_COMISSION").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_USER_DOWNLOAD_COMISSION").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PASS_DOWNLOAD_COMISSION").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_OUT_DWNLD_COMISSION").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_TBK_COM").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILE_TBK_COM").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_TBK_COM").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILE_TBK_COM_CTR").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_TBK_COM_CTR").append("'")
			.append(")");
			
			break;
		
		case 3:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_TBK").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("SFTP_IP_DOWNLOAD_CPAGO").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PORT_DOWNLOAD_CPAGO").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_USER_DOWNLOAD_CPAGO").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PASS_DOWNLOAD_CPAGO").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_OUT_DWNLD_CPAGO").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_CPAGO").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILENAME_CPAGO").append("'")
			.append(")");
			break;
			
		case 4:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_TBK").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("SFTP_IP_UPLOAD_OUTGOING_IC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PORT_UPLOAD_OUTGOING_IC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_USER_UPLOAD_OUTGOING_IC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PASS_UPLOAD_OUTGOING_IC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_IN_UPLD_OUTGOING_IC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_ACL_INC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILENAME_OUT").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_OUT").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_OUT_CTR").append("'")
			.append(")");
			break;
		
		case 5:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_VISA_NAC").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("SFTP_IP_DOWNLOAD_OUTGOING_VISA_NAC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PORT_DOWNLOAD_OUTGOING_VISA_NAC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_USER_DOWNLOAD_OUTGOING_VISA_NAC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PASS_DOWNLOAD_OUTGOING_VISA_NAC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_OUT_DWNLD_OUTGOING_VISA_NAC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_ACL_INC_VISA_NAC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILENAME_OUT_VISA_NAC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_OUT_VISA_NAC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_OUT_CTR_VISA_NAC").append("'")
			.append(")");
			break;
			
		case 6:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_VISA_NAC").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("SFTP_IP_DOWNLOAD_OUTGOING_VISA_NAC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PORT_DOWNLOAD_OUTGOING_VISA_NAC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_USER_DOWNLOAD_OUTGOING_VISA_NAC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PASS_DOWNLOAD_OUTGOING_VISA_NAC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_OUT_DWNLD_OUTGOING_VISA_NAC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_ACL_INC_VISA_NAC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILENAME_OUT_VISA_NAC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_OUT_VISA_NAC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_OUT_CTR_VISA_NAC").append("'")
			.append(")");
			break;
			
		case 7:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_VISA_INT").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("SFTP_IP_DOWNLOAD_VISA_INT").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PORT_DOWNLOAD_VISA_INT").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_USER_DOWNLOAD_VISA_INT").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PASS_DOWNLOAD_VISA_INT").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_OUT_DWNLD_VISA_INT").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_ACL_INC_VISA_INT").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILENAME_OUT_VISA").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_OUT_VISA").append("'")
			.append( "or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_OUT_VISA_CTR").append("'")
			.append(")");
			break;
			
		case 8:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_VISA_INT").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("SFTP_IP_UPLOAD_VISA_INT_IC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PORT_UPLOAD_VISA_INT_IC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_USER_UPLOAD_VISA_INT_IC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PASS_UPLOAD_VISA_INT_IC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_IN_UPLD_VISA_INT_IC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_ACL_INC_VISA_INT").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILENAME_OUT_VISA").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_OUT_VISA").append("'")
			.append(")");
			break;
			
		case 9:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_TBK").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("SFTP_IP_DOWNLOAD_REJECTED").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PORT_DOWNLOAD_REJECTED").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_USER_DOWNLOAD_REJECTED").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PASS_DOWNLOAD_REJECTED").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILE_ABC_RCH").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_RCH").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_ABC_RCH").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_OUT_DWNLD_REJECTED").append("'")
			.append(")");
			break;
			
		case 10:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_TBK").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("SFTP_IP_DOWNLOAD_LOG_TRX_AUTH").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PORT_DOWNLOAD_LOG_TRX_AUTH").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_USER_DOWNLOAD_LOG_TRX_AUTH").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PASS_DOWNLOAD_LOG_TRX_AUTH").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILE_ABC_LOG").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_ABC_LOG").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_ABC_LOG_TRX").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_OUT_DWNLD_LOG_TRX").append("'")
			.append(")");
			break;
			
			
		case 11:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_TBK").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("FORMAT_FILE_ABC_LOG").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_ABC_LOG").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_ABC_LOG_TRX").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_ABC_LOG_TRX_BKP").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_ABC_LOG_TRX_ERROR").append("'")
			.append(")");
			
			break;
			
		case 12:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_TBK").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("PATH_ACL_INC_BKP").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_ACL_INC_ERROR").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_ACL_INC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILENAME_OUT").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("RUT_COM_VI").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_OUT").append("'")
			.append(")");
			break;
			
		case 13:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_VISA_NAC").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("PATH_ACL_INC_VISA_NAC_BKP").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_ACL_INC_VISA_NAC_ERROR").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_ACL_INC_VISA_NAC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILENAME_OUT_VISA_NAC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("RUT_COM_VI_VISA_NAC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_OUT_VISA_NAC").append("'")
			.append(")");
			break;
			
		case 14:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_VISA_INT").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("PATH_ACL_INC_VISA_INT_BKP").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_ACL_INC_VISA_INT_ERROR").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_ACL_INC_VISA_INT").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILENAME_OUT_VISA").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("RUT_COM_VI_VISA_INT").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_OUT_VISA").append("'")
			.append(")");
			break;
			
		case 15:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_TBK").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("PATH_TRX_FILE_AVA").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILE_FILE_AVA").append("'")
			.append(")");
			break;
			
		case 16:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_TBK").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("SFTP_IP_DOWNLOAD_AVA_BICE").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PORT_DOWNLOAD_AVA_BICE").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_USER_DOWNLOAD_AVA_BICE").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_OUT_DWNLD_AVA_BICE").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_ACL_AVA_BICE").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILENAME_AVA_BICE").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILENAME_CORRELATIVO_AVA_BICE").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_AVA_BICE_CTR").append("'").append(")");
			break;
			
		case 17:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_TBK").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("SFTP_IP_DOWNLOAD_AVANCES").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PORT_DOWNLOAD_AVANCES").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_USER_DOWNLOAD_AVANCES").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PASS_DOWNLOAD_AVANCES").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_OUT_DWNLD_AVANCES").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_ACL_AVANCES").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILENAME_AVANCES").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILENAME_CORRELATIVO_AVANCES").append("'")
			.append(")");
			break;
			
		case 18:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_TBK").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("SFTP_IP_DOWNLOAD_ONUS").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PORT_DOWNLOAD_ONUS").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_USER_DOWNLOAD_ONUS").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PASS_DOWNLOAD_ONUS").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILENAME_ONUS").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_ONUS").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_ABC_ONUS").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_OUT_DWNLD_ONUS").append("'")
			.append(")");
			break;
			
		case 19:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_TBK").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("FORMAT_FILENAME_CORRELATIVO_AVA_BICE").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILENAME_AVA_BICE").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_ACL_AVA_BICE").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_ABC_AVA_BICE_BKP").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_ABC_AVA_BICE_ERROR").append("'")
			.append(")");
			break;
			
		case 20:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_TBK").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("FORMAT_FILENAME_CORRELATIVO_AVANCES").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILENAME_AVANCES").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_ACL_AVANCES").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_ABC_AVANCES_BKP").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_ABC_AVANCES_ERROR").append("'")
			.append(")");
			break;
			
		case 21:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_TBK").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("FORMAT_FILENAME_ONUS").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_ONUS").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_ABC_ONUS").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_ABC_ONUS_BKP").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_ABC_ONUS_ERROR").append("'")
			.append(")");
			break;
			
		case 22:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_TBK").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("FORMAT_FILE_ABC_RCH").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_ABC_RCH").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_ABC_RCH_BKP").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_ABC_RCH_ERROR").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_RCH").append("'")
			.append(")");
			break;
			
		case 23:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_TBK").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("FORMAT_FILE_TBK_COM").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_TBK_COM").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_TBK_COM").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_TBK_COM_BKP").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_TBK_COM_ERR").append("'")
			.append(")");
			break;
			
		case 24:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_TBK").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("PATH_CPAGO").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_CPAGO_BKP").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILENAME_CPAGO").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_CPAGO_ERROR").append("'")
			.append(")");
			break;
			
		case 25:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_TBK").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("PATH_ABC_INC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILENAME_INC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_INC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_INC_CTR").append("'")
			.append(")");
			break;
			
		case 26:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_VISA_NAC").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("PATH_VISA_UP_INC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILENAME_INC_VISA").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_INC_VISA").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_INC_VISA_CTR").append("'")
			.append(")");
			break;
			
		case 27:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_VISA_INT").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("PATH_VISA_INT_UP_INC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILENAME_INC_VISA_INT").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_INC_VISA_INT").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_INC_VISA_INT_CTR").append("'")
			.append(")");
			break;
			
		case 28:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_TBK").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("PATH_CAR_ABO").append("'")
			.append(" or  tp.COD_DATO =  ").append("'").append("FORMAT_FILENAME_CAR_ABO").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_CAR_ABO").append("'")
			.append(")");
			break;
			
		case 29:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_TBK").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("SFTP_IP_UPLOAD_INCOMING").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PORT_UPLOAD_INCOMING").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_USER_UPLOAD_INCOMING").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PASS_UPLOAD_INCOMING").append("'")
			.append(" or tp.COD_DATO = ").append("'").append("PATH_ABC_INC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_IN_UPLD_INCOMING").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_ABC_INC_ERROR").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_ABC_INC_BKP").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILENAME_INC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_INC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_INC_CTR").append("'")
			.append(")");
			break;
			
		case 30:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_VISA_NAC").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("SFTP_IP_UPLOAD_INCOMING_VISA").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PORT_UPLOAD_INCOMING_VISA").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_USER_UPLOAD_INCOMING_VISA").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PASS_UPLOAD_INCOMING_VISA").append("'")
			.append(" or tp.COD_DATO = ").append("'").append("PATH_VISA_UP_INC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_IN_UPLD_INCOMING_VISA").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_OUT_VISA_BKP").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_OUT_VISA_ERROR").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILENAME_INC_VISA").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_INC_VISA").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_INC_VISA_CTR").append("'")
			.append(")");
			break;
			
		case 31:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_VISA_INT").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("SFTP_IP_UPLOAD_INCOMING_VISA_INT").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PORT_UPLOAD_INCOMING_VISA_INT").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_USER_UPLOAD_INCOMING_VISA_INT").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PASS_UPLOAD_INCOMING_VISA_INT").append("'")
			.append(" or tp.COD_DATO = ").append("'").append("PATH_VISA_INT_UP_INC").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_IN_UPLD_INCOMING_VISA_INT").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_OUT_VISA_INT_BKP").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_OUT_VISA_INT_ERROR").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILENAME_INC_VISA_INT").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_INC_VISA_INT").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_INC_VISA_INT_CTR").append("'")
			.append(")");
			break;
			
		case 32:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_TBK").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("SFTP_IP_UPLOAD_SOL60").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PORT_UPLOAD_SOL60").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_USER_UPLOAD_SOL60").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PASS_UPLOAD_SOL60").append("'")
			.append(" or tp.COD_DATO = ").append("'").append("PATH_IN_UPLD_SOL60").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_CAR_ABO").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILENAME_CAR_ABO").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_CAR_ABO").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_CAR_ABO_BKP").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_CAR_ABO_ERROR").append("'")
			.append(")");
			break;
			
		case 33:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_TBK").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("DIAS_TRANSAC_VENCIMIENTO").append("'")
			.append(")");
			break;
			
		case 34:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_TBK").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("PATH_TRX_CONTABLE").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILE_ACCOUNT_FILE").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXT_ACCONT_FILE").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILE_ACCOUNT_FILE_CTR").append("'")
			.append(" or tp.COD_DATO = ").append("'").append("FORMAT_EXT_ACCOUNT_FILE_CTR").append("'")
			.append(")");
			break;
			
		case 35:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_TBK").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("SFTP_IP_UPLD_ACCOUNT_FILE").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PORT_UPLD_ACCOUNT_FILE").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_IN_UPLD_ACCOUNT_FILE").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_TRX_CONTABLE").append("'")
			.append(" or tp.COD_DATO = ").append("'").append("PATH_TRX_CONTABLE_ERROR").append("'")
			.append(" or tp.COD_DATO = ").append("'").append("PATH_TRX_CONTABLE_BKP").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILE_ACCOUNT_FILE").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXT_ACCONT_FILE").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILE_ACCOUNT_FILE_CTR").append("'")
			.append(" or tp.COD_DATO = ").append("'").append("FORMAT_EXT_ACCOUNT_FILE_CTR").append("'")
			.append(")");
			break;
			
		case 36:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_TBK").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("SFTP_IP_DOWNLOAD_BOL_FILE").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PORT_DOWNLOAD_BOL_FILE").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_USER_DOWNLOAD_BOL_FILE").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PASS_DOWNLOAD_BOL_FILE").append("'")
			.append(" or tp.COD_DATO = ").append("'").append("FORMAT_FILENAME_BOL").append("'")
			.append(" or tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_BOL").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_BOL_CTR").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_BOL").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_OUT_DWNLD_BOL_FILE").append("'")
			.append(")");
			break;
			
		case 37:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_TBK").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("SFTP_IP_UPLOAD_BOL_FILE").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PORT_UPLOAD_BOL_FILE").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_USER_UPLOAD_BOL_FILE").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PASS_UPLOAD_BOL_FILE").append("'")
			.append(" or tp.COD_DATO = ").append("'").append("PATH_IN_UPLD_BOL_FILE").append("'")
			.append(" or tp.COD_DATO = ").append("'").append("PATH_BOL").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_BOL_BKP").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_BOL_ERROR").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILENAME_BOL").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_BOL").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXTNAME_BOL_CTR").append("'")
			.append(")");
			break;
			
		case 38:
			query.append("tp.COD_DATO = ").append("'").append("SIN_PARAMETRO").append("'");
			break;
			
		case 39:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_TBK").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("PATH_TRX_CONTABLE_AVA").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILE_ACCOUNT_FILE_AVA").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXT_ACCONT_FILE_AVA").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILE_ACCOUNT_FILE_CTR_AVA").append("'")
			.append(" or tp.COD_DATO = ").append("'").append("FORMAT_EXT_ACCOUNT_FILE_CTR_AVA").append("'")
			.append(")");
			break;
			
		case 40:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_TBK").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("SFTP_IP_UPLD_ACCOUNT_FILE_AVA").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PORT_UPLD_ACCOUNT_FILE_AVA").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_USER_UPLD_ACCOUNT_FILE_AVA").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PASS_UPLD_ACCOUNT_FILE_AVA").append("'")
			.append(" or tp.COD_DATO = ").append("'").append("PATH_IN_UPLD_ACCOUNT_FILE_AVA").append("'")
			.append(" or tp.COD_DATO = ").append("'").append("PATH_TRX_CONTABLE_AVA").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_TRX_CONTABLE_ERROR_AVA").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_TRX_CONTABLE_BKP_AVA").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILE_ACCOUNT_FILE_AVA").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXT_ACCONT_FILE_AVA").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILE_ACCOUNT_FILE_CTR_AVA").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXT_ACCOUNT_FILE_CTR_AVA").append("'")
			.append(")");
			break;
			
		case 41:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_TBK").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("PATH_TRX_REG_CONTABLE_AVA").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILE_REG_ACCOUNT_FILE_AVA").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXT_REG_ACCONT_FILE_AVA").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILE_REG_ACCOUNT_FILE_CTR_AVA").append("'")
			.append(" or tp.COD_DATO = ").append("'").append("FORMAT_EXT_REG_ACCOUNT_FILE_CTR_AVA").append("'")
			.append(" or tp.COD_DATO = ").append("'").append("PATH_TRX_REG_CONTABLE_ERROR_AVA").append("'")
			.append(")");
			break;
			
		case 42:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_TBK").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("SFTP_IP_REUPLD_ACCOUNT_FILE_AVA").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PORT_REUPLD_ACCOUNT_FILE_AVA").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_USER_REUPLD_ACCOUNT_FILE_AVA").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("SFTP_PASS_REUPLD_ACCOUNT_FILE_AVA").append("'")
			.append(" or tp.COD_DATO = ").append("'").append("PATH_IN_REUPLD_ACCOUNT_FILE_AVA").append("'")
			.append(" or tp.COD_DATO = ").append("'").append("PATH_TRX_REG_CONTABLE_AVA").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_TRX_REG_CONTABLE_ERROR_AVA").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("PATH_TRX_REG_CONTABLE_BKP_AVA").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_FILE_REG_ACCOUNT_FILE_AVA").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("FORMAT_EXT_REG_ACCONT_FILE_AVA").append("'")
			.append(")");
			break;
		case 43:
			query.append("(")
			.append("tp.COD_GRUPO_DATO = ").append("'").append("CRON_TBK").append("'")
			.append(")")
			.append(" and (tp.COD_DATO = ").append("'").append("PATH_TRX_GEN_REPORTE_CONTABLE").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("COD_DESCUENTO_TBK").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("COD_PAGO_TBK").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("COD_DESCUENTO_VN").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("COD_PAGO_VN").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("COD_DESCUENTO_VI").append("'")
			.append(" or  tp.COD_DATO = ").append("'").append("COD_PAGO_VI").append("'")
			.append(")");
			break;
		default:
			break;
		}
				
		
		try {
								
			stmt = this.getConnection().prepareStatement(query.toString());
			result = stmt.executeQuery();
			
			while(result.next())
			{
				param = new CronTaskParam();
				param.setCodDato(result.getString("COD_DATO"));
				param.setValor(result.getString("VALOR"));
				param.setDescripcion(result.getString("DESCRIPCION"));
				param.setCodGrupo(result.getString("COD_GRUPO_DATO"));
				param.setSid(result.getInt("SID"));
				lista.add(param);
				
			}
			
		
			return lista;
			}
			
		 finally {
			if (stmt != null) {
				stmt.close();
			}

			if (result != null) {
				result.close();
			}

		}
		
	}
	
	

	
}
