package com.sbpay.sgi.cron.srv.utils;

import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dto.DataFTP;
import com.sbpay.sgi.cron.ftp.FTPClientUtils;
import com.sbpay.sgi.cron.ftp.FTPFileClient;
import com.sbpay.sgi.cron.ftp.SFTPClientUtils;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 16/12/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * Clase que realiza la conexion a un ftp y el traspaso de archivos de
 * forma generica
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class ClientFTPSrv extends CommonSrv {
    
    /** Constante para el logger */
    private static final Logger LOGGER = Logger.getLogger( ClientFTPSrv.class );
    
    /** The single instance. */
    private static ClientFTPSrv singleINSTANCE = null;
    
    /**
     * Creates the instance.
     */
    private static void createInstance() {
        synchronized ( ClientFTPSrv.class ) {
            if ( singleINSTANCE == null ) {
                singleINSTANCE = new ClientFTPSrv();
            }
        }
    }
    
    /**
     * Patron singleton.
     * 
     * @return MailSendSrv retorna instancia del servicio.
     */
    public static ClientFTPSrv getInstance() {
        if ( singleINSTANCE == null ) {
            createInstance();
        }
        return singleINSTANCE;
    }
    
    /**
     * Metodo de restricion de sobreescritura de la clase.
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        super.clone();
        throw new CloneNotSupportedException();
    }
    
    /**
     * Constructor Privado.
     */
    private ClientFTPSrv() {
        super();
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 18/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * Metodo que lista los archivos en un servidor ftp seguro
     * 
     * 
     * @return
     * @throws AppException
     * @since 1.X
     */
    public List<String> listFileSFTP( DataFTP dataFTPS, String folder )
            throws AppException {
        
        FTPFileClient sftp = new SFTPClientUtils();
        
        return sftp.listFiles( dataFTPS, folder );
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 17/12/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * Metodo que carga un archivo en un ftp
     * 
     * 
     * @param dataFTP
     *            Datos de conexion al ftp
     * @param pathOrigen
     *            ruta del archivo de origen en la maquina (Local)
     * @param pathDestino
     *            ruta del archivo de destino en FTP (Remoto)
     * @param filenameDest
     *            Nombre de archivo con el cual quedara en la maquina
     *            final
     * @return
     * @throws AppException
     * @since 1.X
     */
    public Boolean uploadFileSFTP( DataFTP dataFTP, String pathOrigen,
            String pathDestino, String filename ) throws AppException {
        // OBTIENE LA CONEXION AL FTP
        FTPFileClient sftp = new SFTPClientUtils();
        return sftp.upload( dataFTP, pathOrigen, pathDestino, filename );
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 17/12/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @param dataFTP
     *            Datos para realizar la conexion al ftp
     * @param filename2
     * @param pathOrigen
     *            Ruta del archivo en carpeta origen, es decir en el
     *            ftp (Nota: Ruta completa directorio + filename
     * @param pathDestino
     *            Ruta donde se dejara el archivo al descargarlo desde
     *            ftp (Nota: Ruta completa directorio + filename
     * @return True : carga correcta, False : carga incorrecta
     * @throws AppException
     * @since 1.X
     */
    public Boolean downloadFileSFTP( DataFTP dataFTP, String pathFtpOrigen,
            String pathDest, String fileName ) throws AppException {
        
        FTPFileClient sftp = new SFTPClientUtils();
        
        return sftp.download( dataFTP, pathFtpOrigen, pathDest, fileName );
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 18/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @param dataFTPS
     * @param folder
     * @return
     * @throws AppException
     * @since 1.X
     */
    public List<String> listFileFTP( DataFTP dataFTPS, String folder )
            throws AppException {
            
        FTPFileClient ftp = new FTPClientUtils();
        
        return ftp.listFiles( dataFTPS, folder );
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 17/12/2015, (ACL-sbpay) - versiÃ³n inicial
     * </ul>
     * <p>
     * Metodo que carga un archivo en un ftp
     * 
     * 
     * @param dataFTP
     *            Datos de conexion al ftp
     * @param pathOrigen
     *            ruta del archivo de origen en la maquina (Local)
     * @param pathDestino
     *            ruta del archivo de destino en FTP (Remoto)
     * @param filenameDest
     *            Nombre de archivo con el cual quedara en la maquina
     *            final
     * @return
     * @throws AppException
     * @since 1.X
     */
    public Boolean uploadFileFTP( DataFTP dataFTP, String pathOrigen,
            String pathDestino, String filenameDest ) throws AppException {
        LOGGER.info( "COMIENZA SUBIDA ARCHIVO FTP : " + dataFTP.getIpHost() );
		
        FTPFileClient ftp = new FTPClientUtils();
        
        return ftp.upload( dataFTP, pathOrigen, pathDestino, filenameDest );
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 17/12/2015, (ACL-sbpay) - versiÃ³n inicial
     * </ul>
     * <p>
     * 
     * @param dataFTP
     *            Datos para realizar la conexion al ftp
     * @param pathOrigen
     *            Ruta del archivo en carpeta origen, es decir en el
     *            ftp (Nota: Ruta completa directorio + filename
     * @param pathDestino
     *            Ruta donde se dejara el archivo al descargarlo desde
     *            ftp (Nota: Ruta completa directorio + filename
     * @return True : carga correcta, False : carga incorrecta
     * @throws AppException
     * @since 1.X
     */
    public Boolean downloadFileFTP( DataFTP dataFTP, String pathOrigen,
            String pathDestino, String filename ) throws AppException {
        LOGGER.info( "Comienza descarga de archivo desde ftp : "
                + dataFTP.getIpHost() );
                
        LOGGER.info( "PATH ORIGEN : " + pathOrigen );
        LOGGER.info( "PATH DESTINO : " + pathDestino );
        LOGGER.info( "FILENAME : " + filename );
        FTPFileClient ftp = new FTPClientUtils();
        
            
        return ftp.download( dataFTP, pathOrigen, pathDestino, filename );
    
		}

}
