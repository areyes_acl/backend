package com.sbpay.sgi.cron.srv.process;

import java.io.File;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dao.TransaccionAutorizadaDao;
import com.sbpay.sgi.cron.dao.TransaccionAutorizadaDaoFactory;
import com.sbpay.sgi.cron.dto.ICCodeHomologationDTO;
import com.sbpay.sgi.cron.dto.LogTransaccionAutorizada;
import com.sbpay.sgi.cron.dto.ParamProcessLogAutorizacion;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.dto.TblCronLogDTO;
import com.sbpay.sgi.cron.dto.TransacionAutorizada;
import com.sbpay.sgi.cron.enums.ConstantesPRTS;
import com.sbpay.sgi.cron.enums.FilterTypeSearch;
import com.sbpay.sgi.cron.enums.LogBD;
import com.sbpay.sgi.cron.enums.MsgErrorMail;
import com.sbpay.sgi.cron.enums.MsgErrorProcessFile;
import com.sbpay.sgi.cron.enums.MsgErrorSQL;
import com.sbpay.sgi.cron.enums.MsgExitoMail;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.srv.utils.CommonSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;
import com.sbpay.sgi.cron.utils.file.TransaccionAutorFileUtil;
import com.sbpay.sgi.cron.utils.file.TransaccionAutorReader;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;
import com.sbpay.sgi.cron.validation.ProcessValidator;
import com.sbpay.sgi.cron.validation.impl.ProcessValidatorImpl;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class ProcesarLogTrxAutorizadasSrv extends CommonSrv {
    
    /** VARIABLE PARA EL LOGER */
    private static final Logger LOGGER = Logger
            .getLogger( ProcesarLogTrxAutorizadasSrv.class );
    
    /** CONSTATE DE COMPARACION PARA ESTADO DE INCOMING */
    private static final String FILE_IS_NOT_PROCESS = "0";
    private static final String FLAG_PROCESS_OK = "1";
    private static final String FLAG_PROCESS_ERROR = "-1";
    
    /** The single instance. */
    private static ProcesarLogTrxAutorizadasSrv singleINSTANCE = null;
    
    /** PROCESO **/
    private final ProcessCodeType PROCESO_ACTUAL = ProcessCodeType.PROCESAR_LOG_DE_TRANSACCIONES_AUTORIZADAS;
    
    /**
     * Creates the instance.
     */
    private static void createInstance() {
        synchronized ( ProcesarLogTrxAutorizadasSrv.class ) {
            if ( singleINSTANCE == null ) {
                singleINSTANCE = new ProcesarLogTrxAutorizadasSrv();
            }
        }
    }
    
    /**
     * Patron singleton.
     * 
     * @return ActivarPrepagoSrv retorna instancia del servicio.
     */
    public static ProcesarLogTrxAutorizadasSrv getInstance() {
        if ( singleINSTANCE == null ) {
            createInstance();
        }
        return singleINSTANCE;
    }
    
    /**
     * Metodo de restricion de sobreescritura de la clase.
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        super.clone();
        throw new CloneNotSupportedException();
    }
    
    /**
     * Constructor Privado.
     */
    private ProcesarLogTrxAutorizadasSrv() {
        super();
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2015, (Everis Chile) - versión inicial
     * </ul>
     * <p>
     * Metodo principal que contrala toda la lógica del procesamiento
     * de un incoming
     * 
     * @throws AppException
     * 
     * 
     * @since 1.X
     */
    public void procesarLogTrxAutorizadas() throws AppException {
        
        ProcessValidator validadorProceso = new ProcessValidatorImpl();
        LogTransaccionAutorizada logTrx = new LogTransaccionAutorizada();
        
        if ( validadorProceso.isActiveProcess( PROCESO_ACTUAL ) ) {
            
            /*
             * OBTIENE LOS PARAMETROS UTILIZADOS PARA PROCESAR EL
             * ARCHIVO pathAclInc Y formatFileNameInc
             */
            ParamProcessLogAutorizacion params = getParametros();
            Integer countFileProcessed = 0;
            List<File> listOfFiles = null;
            String pathFolder = params.getPathAbcLogTrx();
            String starWith = params.getStarWith();
            String endsWith = params.getEndsWith();
            List<ICCodeHomologationDTO> listaCodigosIc = loadListIcCodes();
            
            if ( listaCodigosIc != null
                    && listaCodigosIc.size() > 0
                    && validadorProceso.hasTheValidParametersForTheProcess(
                            PROCESO_ACTUAL, params ) ) {
                
                listOfFiles = CommonsUtils.getFilesInFolder( pathFolder,
                        starWith, endsWith );
                
                LOGGER.info( "Lista de archivos :  " + listOfFiles );
                
                // SI NO EXISTE NINGUN ARCHIVO EN LA CARPETA TERMINA
                // EL PROCESO
                if ( listOfFiles == null ) {
                    LOGGER.warn( MsgErrorProcessFile.WARNING_NO_FILES_FOUNDS
                            .toString() );
                    return;
                }
                
                // RECORRE TODOS LOS FILES ENCONTRANDOS EN EL
                // DIRECTORIO
                for ( File file : listOfFiles ) {
                    
                    if ( esValidoLogAutorizacion( file, starWith ) ) {
                        try {
                            LOGGER.info( "Archivo: "
                                    + file.getName()
                                    + ", válido, por lo que se comienza a procesar..." );
                            
                            // 1- PROCESA ARCHIVO LOG TRX
                            TransaccionAutorFileUtil reader = new TransaccionAutorReader();
                            logTrx = reader.readLogTrxAutorFile( file,
                                    starWith, listaCodigosIc );
                            
                            logTrx.setFilename( file.getName() );
                            
                            // INFORMA REGISTROS NO SE LEYERON
                            enviaCorreoTrxNoLeidas( logTrx );
                            
                            // 2 - SE GUARDAN LAS TRANSACCIONES
                            saveData( logTrx );
                            
                            // 3- SE REALIZA EL UPDATE OK
                            updateFlag( FLAG_PROCESS_OK, logTrx.getFilename() );
                            
                            // 4 - SE MUEVE LOG AUTORIZACION A LA
                            // CARPETA BACKUP
                            CommonsUtils.moveFile( file.getAbsolutePath(),
                                    params.getPathBkp() + file.getName() );
                            countFileProcessed++;
                            
                        }
                        catch ( AppException e ) {
                            
                            // ERROR AL PROCESAR LOG AUTOR SE ENVIA
                            // EMAIL
                            LOGGER.error( MsgErrorProcessFile.ERROR_IN_PROCESS_LOG_TRX_TRANSAC
                                    .toString() + " - " + e );
                            
                            // SE REALIZA EL UPDATE ERROR
                            updateFlag( FLAG_PROCESS_ERROR,
                                    logTrx.getFilename() );
                            
                            MailSendSrv
                                    .getInstance()
                                    .sendMail(
                                            MsgErrorProcessFile.ERROR_IN_PROCESS_LOG_TRX_TRANSAC
                                                    .toString(),
                                            e.getStackTraceDescripcion() );
                            
                            // MUEVE ARCHIVO A LA CARPETA DE ERROR
                            CommonsUtils.moveFile( file.getAbsolutePath(),
                                    params.getPathError() + file.getName() );
                            
                        }
                    }
                    else {
                        LOGGER.error( "No hay archivos de log de autorizacion válidos" );
                    }
                }
                
                // SI NO SE PROCESA NINGUN FILE ESCRIBE ESCRIBE LOG
                if ( countFileProcessed == 0 ) {
                    LOGGER.info( MsgErrorProcessFile.WARNING_NO_FILE_PROCESSING_TITLE
                            + "  - "
                            + MsgErrorProcessFile.WARNING_NO_FILE_PROCESSING_CAUSE );
                }
                
                // SI SE PROCESARON TODOS LOS ARCHIVOS EXISTENTES SE
                // ENVIA MAIL CORRECTO
                if ( listOfFiles.size() == countFileProcessed ) {
                    LOGGER.info( "Envio Correo de Proceso  Satisfactorio: Procesamiento LOG TRX AUTORIZADAS" );
                    MailSendSrv
                            .getInstance()
                            .sendMailOk(
                                    MsgExitoMail.EXITO_PROCESS_TITLE_LOG_AUTORIZACION
                                            .toString(),
                                    MsgExitoMail.EXITO_PROCESS_BODY_LOG_AUTORIZACION
                                            .toString() );
                }
                
            }
            else {
                LOGGER.warn( MsgProceso.MSG_ERROR_FALTAN_PARAMETROS
                        .toString()
                        .concat( " ó " )
                        .concat(
                                MsgErrorProcessFile.ERROR_LIST_CODE_IS_NOT_AVAILABLE
                                        .toString() ) );
                MailSendSrv
                        .getInstance()
                        .sendMailOk(
                                MsgErrorMail.ALERTA_RCH_PRTS.toString(),
                                MsgErrorMail.ALERTA_RCH_PRTS_TXT
                                        .toString()
                                        .concat( " ó ," )
                                        .concat(
                                                MsgErrorProcessFile.ERROR_LIST_CODE_IS_NOT_AVAILABLE
                                                        .toString() ) );
            }
        }
        else {
            String asunto = MsgProceso.MSG_PROCESO_DESACTIVADO_ASUNTO
                    .toString().replace( ":?:",
                            PROCESO_ACTUAL.getCodigoProceso() );
            String mensaje = MsgProceso.MSG_PROCESO_DESACTIVADO_BODY.toString()
                    .replace( ":?:", PROCESO_ACTUAL.getCodigoProceso() );
            MailSendSrv.getInstance().sendMail( asunto, mensaje );
        }
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Metodo que envia correo avisando si alguna trx no fue leida
     * 
     * @param reader
     * @since 1.X
     */
    private void enviaCorreoTrxNoLeidas(
            LogTransaccionAutorizada logTrxAutorizadas ) {
        
        if ( logTrxAutorizadas.getDetalleLectura() != null
                && logTrxAutorizadas.getDetalleLectura().length() > 0 ) {
            String motivo = MsgErrorProcessFile.ERROR_NOT_ALL_LOG_TRX_ARE_READER
                    .toString().concat( ", del archivo : " )
                    .concat( logTrxAutorizadas.getFilename() );
            MailSendSrv.getInstance().sendMailOk( motivo,
                    logTrxAutorizadas.getDetalleLectura() );
            
        }
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 - 24/11/2015, (ACL) - versión inicial
     * </ul>
     * <p>
     * 
     * Metodo que valida si el incoming está o no ingresado en bd en
     * la tabla Cron_log si es asi se verifica su codigo si es 1- ya
     * se encuentra procesado si es -1 fallo en procesamientos
     * anteriores, y si es 0 se debe procesar.
     * 
     * @throws AppException
     * 
     * 
     * @since 1.X
     */
    private boolean esValidoLogAutorizacion( File file, String starWith )
            throws AppException {
        CommonsDao commonsDAO = null;
        try {
            
            // SI NO ES UN ARCHIVO RETORNA FALSE
            if ( !file.isFile() ) {
                return false;
            }
            
            commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
            
            // BUSCA REGISTRO EN LA TABLA TBL_LOG_TRANSAC_AUTOR POR
            // NOMBRE DE ARCHIVO
            LOGGER.info( "Nombre Log Trx Autor: " + file.getName() );
            TblCronLogDTO filter = new TblCronLogDTO();
            filter.setFilename( file.getName() );
            TblCronLogDTO logRegister = commonsDAO.getTblCronLogByType( filter,
                    LogBD.TABLE_LOG_TRANSAC_AUTOR.getTable(),
                    FilterTypeSearch.FILTER_BY_NAME );
            
            // SI NO ENCUENTRA EL REGISTRO EN LA TABLA
            // LOG_TRX_TRANSAC_AUTOR , RETORNA FALSE
            if ( logRegister == null ) {
                LOGGER.info( MsgErrorProcessFile.ERROR_LOG_TRX_AUTOR_NOT_FOUND
                        .toString() );
                return Boolean.FALSE;
            }
            
            // SI EL VALOR DEL REGISTRO DE LOG ES DISTINTO DE 0 EL
            // ARCHIVO YA FUE PROCESADO, RETORNA FALSE
            else
                if ( !FILE_IS_NOT_PROCESS.equalsIgnoreCase( logRegister
                        .getFileFlag() ) ) {
                    LOGGER.info( MsgErrorProcessFile.ERROR_INVALID_LOGTRXAUTOR_STATE
                            .toString() );
                    return Boolean.FALSE;
                }
            
        }
        catch ( SQLException e ) {
            throw new AppException(
                    MsgErrorSQL.ERROR_FIND_LOGTRXAUTOR.toString(), e );
        }
        finally {
            
            try {
                if ( commonsDAO != null ) {
                    commonsDAO.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException(
                        MsgErrorSQL.ERROR_CLOSE_LOGTRXAUTOR.toString(), e );
            }
        }
        
        return Boolean.TRUE;
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 - 16/11/2015, (ACL) - versión inicialfS
     * </ul>
     * <p>
     * Metodo encargado de recorrer las transacciones encontradas en
     * el incoming y enviarlas una a una al DAO para que sean
     * almacenadas
     * 
     * @throws AppException
     *
     * 
     * @since 1.X
     */
    private void saveData( LogTransaccionAutorizada logTrx )
            throws AppException {
        Integer flag = 0;
        TransaccionAutorizadaDao procesarDAO = null;
        
        try {
            procesarDAO = TransaccionAutorizadaDaoFactory.getInstance()
                    .getNewEntidadDao();
            
            // RECORRE TODAS LAS TRANSACCIONES DEL LOG DE
            // TRANSACCIONES
            for ( TransacionAutorizada instance : logTrx.getTrxList() ) {
                
                try {
                    // GUARDA EN TMP_TRANSAC_AUTOR, SI HAY ERROR SUMA
                    // -1
                    if ( !procesarDAO.saveTransaction( instance ) ) {
                        flag = flag - 1;
                    }
                }
                catch ( Exception e ) {
                    LOGGER.error( e.getMessage(), e );
                }
            }
            // COMMMIT DE TRANSACCIONES SAVE ACUMULADAS
            if ( flag == 0 ) {
                LOGGER.info( "SE REALIZA COMMIT SAVE TRANSACCIONES" );
                procesarDAO.endTx();
                
                // SE LLAMA A SP PARA CARGAR LA TBL_TRANSAC_AUTOR
                procesarDAO.callSPCargaLogTrxAutor();
                procesarDAO.endTx();
                
            }
            else {
                // SE REALIZA ROLLBACK
                rollBackTransaccion( procesarDAO );
                throw new AppException(
                        MsgErrorSQL.ERROR_MAKE_A_ROLLBAK.toString() );
                
            }
        }
        catch ( SQLException e ) {
            LOGGER.error( MsgErrorSQL.ERROR_SAVE_LOGTRXAUTOR.toString(), e );
            throw new AppException(
                    MsgErrorSQL.ERROR_SAVE_LOGTRXAUTOR.toString(), e );
        }
        finally {
            try {
                if ( procesarDAO != null ) {
                    procesarDAO.close();
                }
            }
            catch ( SQLException e ) {
                LOGGER.error( e.getMessage(), e );
            }
            
        }
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    private Boolean updateFlag( String flag, String filename ) {
        Boolean process = Boolean.FALSE;
        
        LOGGER.info( "Se actualiza flag de archivo :" + filename + ", a flag :"
                + flag );
        
        TblCronLogDTO tblCronLogDTO = new TblCronLogDTO();
        tblCronLogDTO.setFilename( filename );
        tblCronLogDTO.setFileFlag( flag );
        CommonsDao commonsDAO = null;
        
        try {
            commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
            
            if ( commonsDAO.updateTblCronLogByType( tblCronLogDTO,
                    LogBD.TABLE_LOG_TRANSAC_AUTOR.getTable(),
                    FilterTypeSearch.FILTER_BY_NAME ) ) {
                commonsDAO.endTx();
                process = Boolean.TRUE;
            }
            else {
                try {
                    rollBackTransaccion( commonsDAO );
                }
                catch ( AppException e ) {
                    LOGGER.info(
                            "ERROR AL REALIZAR EL ROLLBACK AL UPDATE PENDIENTE SOBRE LA TABLA LOG_TRANSAC_AUTOR",
                            e );
                }
            }
        }
        catch ( SQLException e1 ) {
            LOGGER.info( "ERROR AL UPDATEAR TABLA LOG_TRANSAC_AUTOR", e1 );
        }
        finally {
            
            try {
                if ( commonsDAO != null ) {
                    commonsDAO.close();
                }
            }
            catch ( SQLException e ) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
        }
        
        return process;
        
    }
    
    /**
     * Consulta la lista de codigos de IC desde la tabla
     * TBL_PRTS_IC_CODE
     * 
     * 
     * 
     * 
     * 
     * @throws AppException
     * @return Lista de codigos de ic
     * 
     */
    private List<ICCodeHomologationDTO> loadListIcCodes() throws AppException {
        CommonsDao commonsDAO = null;
        
        try {
            // INICIA EL DAO
            commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
            return commonsDAO.obtenerCodigosIc();
            
        }
        catch ( SQLException e ) {
            throw new AppException( MsgErrorSQL.ERROR_QUERY_PRTS.toString(), e );
        }
        finally {
            try {
                if ( commonsDAO != null ) {
                    commonsDAO.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException(
                        MsgErrorSQL.ERROR_QUERY_PRTS_CLOSE.toString(), e );
            }
        }
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 - 24/11/2015, (ACL) - versión inicial
     * </ul>
     * <p>
     * 
     * Metodo setea los parametros de sistema del cron transbank.
     * 
     * @return
     * @throws AppException
     * @since 1.X
     */
    private ParamProcessLogAutorizacion getParametros() throws AppException {
        ParamProcessLogAutorizacion parametros = null;
        List<ParametroDTO> paramDTOList = null;
        CommonsDao commonsDAO = null;
        try {
            // INICIA EL DAO
            commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
            // SE OBTIENEN TODOS LOS CODIGOS
            paramDTOList = commonsDAO
                    .getParametroCodGrupoDato( ConstantesPRTS.CRON_TBK
                            .toString() );
            
            LOGGER.info( "LISTA DE PARAMETROS : " + paramDTOList.size() );
            
            // SI EXISTEN PARAMETROS EN BD SE EXTRAEN
            if ( paramDTOList != null ) {
                parametros = new ParamProcessLogAutorizacion();
                
                // Nombre "starWith" del archivo de trx
                parametros.setStarWith( CommonsUtils.getCodiDato( paramDTOList,
                        ConstantesPRTS.FORMAT_FILE_ABC_LOG.toString() ) );
                
                // extension "endsWith" de los archivos de log de trx
                parametros.setEndsWith( CommonsUtils.getCodiDato( paramDTOList,
                        ConstantesPRTS.FORMAT_EXTNAME_ABC_LOG.toString() ) );
                
                // Carpeta donde se encuentran los archivos log de
                // transacciones en sbpay
                parametros.setPathAbcLogTrx( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.PATH_ABC_LOG_TRX.toString() ) );
                
                // Carpeta donde se movera bkp
                parametros.setPathBkp( CommonsUtils.getCodiDato( paramDTOList,
                        ConstantesPRTS.PATH_ABC_LOG_TRX_BKP.toString() ) );
                
                // Carpeta donde se movera a error
                parametros.setPathError( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.PATH_ABC_LOG_TRX_ERROR.toString() ) );
                
                LOGGER.info( "Parametros  BD : " + parametros );
            }
            
        }
        catch ( SQLException e ) {
            throw new AppException( MsgErrorSQL.ERROR_QUERY_PRTS.toString(), e );
        }
        finally {
            try {
                if ( commonsDAO != null ) {
                    commonsDAO.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException(
                        MsgErrorSQL.ERROR_QUERY_PRTS_CLOSE.toString(), e );
            }
        }
        return parametros;
    }
    
}
