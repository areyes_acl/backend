package com.sbpay.sgi.cron.ftp;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dto.DataFTP;
import com.sbpay.sgi.cron.enums.MsgErrorFTP;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 18/1/2016, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class FTPClientUtils implements FTPFileClient {
    
    private static final Logger LOGGER = Logger
            .getLogger( FTPClientUtils.class );
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 21/12/2015, (ACL-sbpay) - versiÃ³n inicial
     * </ul>
     * <p>
     * Metodo que regresa una lista de
     * 
     * @return
     * @throws IOException
     * @throws AppException
     * @since 1.X
     */
    public List<String> listFiles( DataFTP dataFtp, String folderPath )
            throws AppException {
        List<String> listaArchivos = null;
        FTPClient ftpClient = null;
        try {
            ftpClient = connect( dataFtp );
            FTPFile[] fileList;
            fileList = ftpClient.listFiles( folderPath );
            listaArchivos = new ArrayList<String>();
            for ( FTPFile ftpFile : fileList ) {
                if ( ftpFile.isFile() ) {
                    listaArchivos.add( ftpFile.getName() );
                }
            }
            ftpClient.logout();
            
        }
        catch ( IOException e ) {
            throw new AppException(
                    "Error al obtener lista de nombres de archivos del directorio FTP",
                    e );
        }
        finally {
            try {
                disconnect( ftpClient );
            }
            catch ( Exception e2 ) {
              LOGGER.error("Error al listar los componentes desde el ftp : ", e2);
            }
            
        }
        
        return listaArchivos;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 16/12/2015, (ACL-sbpay) - versiÃ³n inicial
     * </ul>
     * <p>
     * 
     * @param dataFTP
     *            Contiene los datos necesarios para subir archivo a
     *            ftp
     * @param ftp
     * @throws AppException
     * @since 1.X
     */
    public Boolean upload( DataFTP dataFTP, String pathOrigen,
            String pathDestino, String filename ) throws AppException {
        InputStream in = null;
        Boolean result = Boolean.FALSE;
        FTPClient ftpClient = null;
       
        try {
            ftpClient = connect( dataFTP );
            in = new FileInputStream( pathOrigen + filename );
            ftpClient.changeWorkingDirectory( pathDestino );
     
            result = ftpClient.storeFile( filename, in );
            

   
            
           
            
        }
        catch ( IOException e ) {
            throw new AppException(
                    MsgErrorFTP.ERROR_UPLOAD_READ_FILE.toString(), e );
        }
        finally {
            try {
                if ( in != null ) {
                    in.close();
                }
                
         
            }
            catch ( IOException e ) {
                throw new AppException(
                        MsgErrorFTP.ERROR_CLOSE_FILE.toString(), e );
            }
        }
        
        return result;
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 18/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @param ftpClient
     * @param pathOrigen
     * @param pathDestino
     * @return
     * @throws AppException
     * @since 1.X
     */
    public Boolean download( DataFTP dataFTP, String pathOrigen,
            String pathDestino, String filename ) throws AppException {
        
        FTPClient ftpClient = null;
        InputStream in = null;
        String fullPathfileDownload = pathDestino.concat( filename );
        File archivo = new File( fullPathfileDownload );
        BufferedWriter bw = null;
        FileWriter fw = null;
        boolean flag = false;
        int c;
        
        try {
            String pathFileOrigen = pathOrigen.concat( filename );
            ftpClient = connect( dataFTP );
            in = ftpClient.retrieveFileStream( pathFileOrigen );
            // SI ENCUENTRA ARCHIVO EN FTP
            if ( in != null ) {
                fw = new FileWriter( archivo );
                bw = new BufferedWriter( fw );
                while ( ( c = in.read() ) != -1 ) {
                    bw.write( c );
                }
                bw.flush();
                flag = Boolean.TRUE;
            }
            else {
                // SI NO ENCUENTRA ARCHIVO EN FTP
                throw new AppException(
                        MsgErrorFTP.ERROR_DOWNLOAD_FILE_NOT_FOUND.toString() );
            }
            ftpClient.logout();
        }
        catch ( IOException e ) {
            throw new AppException( MsgErrorFTP.ERROR_DOWNLOAD_FILE.toString(),
                    e );
        }
        finally {
            try {
                if ( bw != null ) {
                    bw.close();
                }
                
                if ( fw != null ) {
                    fw.close();
                }
                
                disconnect( ftpClient );
            }
            catch ( IOException e ) {
                flag = Boolean.FALSE;
                throw new AppException(
                        MsgErrorFTP.ERROR_CLOSE_FILE.toString(), e );
            }
        }
        return flag;
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 18/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @param dataFTP
     * @return Cliente conectado al FTP.
     * @throws AppException
     *             Exception de la App.
     * @since 1.X
     */
    private FTPClient connect( DataFTP dataFTP ) throws AppException {
        FTPClient ftpClient = null;
        
        try {
            ftpClient = new FTPClient();
            ftpClient.connect( dataFTP.getIpHost(), dataFTP.getPort() );
            int reply = ftpClient.getReplyCode();
            
            if ( !FTPReply.isPositiveCompletion( reply ) ) {
                ftpClient.disconnect();
                throw new AppException( "Error: Server response " + reply );
                
            }
            else
                if ( ftpClient.login( dataFTP.getUser(), dataFTP.getPassword() ) ) {
                    ftpClient.enterLocalPassiveMode();
                    ftpClient.setFileType( FTP.BINARY_FILE_TYPE );
                }
                else {
                    throw new AppException(
                            "Ha fallado el login en el servidor - "
                                    + dataFTP.getIpHost() + " , puerto "
                                    + dataFTP.getPort() + "como usuario :  "
                                    + dataFTP.getUser() );
                }
            return ftpClient;
        }
        catch ( SocketException e ) {
            throw new AppException(
                    MsgErrorFTP.ERROR_CONNECTION_FTP_UPLOAD.toString(), e );
        }
        catch ( IOException e ) {
            throw new AppException(
                    MsgErrorFTP.ERROR_CONNECTION_FTP_UPLOAD.toString(), e );
        }
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 21/12/2015, (ACL-sbpay) - versiÃ³n inicial
     * </ul>
     * <p>
     * 
     * @param ftpClient
     * @throws IOException
     * @since 1.X
     */
    private void disconnect( FTPClient ftpClient ) throws IOException {
        
        if ( ftpClient != null ) {
            if ( ftpClient.isConnected() ) {
                boolean logoutSuccess = ftpClient.logout();
                ftpClient.disconnect();
                if ( !logoutSuccess ) {
                    LOGGER.warn( "Logout ha fallado mientras se desconectaba, codigo de error : "
                            + ftpClient.getReplyCode() );
                }
            }
        }
    }
    
}
