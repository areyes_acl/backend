package com.sbpay.sgi.cron.step;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.srv.process.ProcesarConciliacionSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;

public class StepProcessConciliar {
    /**
     * VARIABLES
     */
    private static final Logger LOG = Logger
            .getLogger( StepProcessConciliar.class );
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/11/2018, (sbpay Chile) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @throws Exception
     * @since 1.X
     */
    public void execute() throws Exception {
        try {
            LOG.info( "Inicio procesamiento de conciliacion : StepProcessConciliar " );
            ProcesarConciliacionSrv.getInstance().processConciliacion();
            
        }
        catch ( AppException e ) {
            MailSendSrv.getInstance().sendMail(
                    MsgProceso.MSG_GENERIC_ERROR_MSJ.toString(),
                    ( e.getStackTraceDescripcion() != null ) ? e
                            .getStackTraceDescripcion() : e.getMessage() );
        }
        
    }

}
