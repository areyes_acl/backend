package com.sbpay.sgi.cron.step;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.enums.MsgExitoMail;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.srv.upload.UploadVisaSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;

public class StepUploadIncomingVisa {
    
    /** The Constant LOGGER. */
    private static final Logger LOG = Logger
            .getLogger( StepUploadIncomingVisa.class );
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 7/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws Exception
     * @since 1.X
     */
    public void execute() throws Exception {
        try {
            LOG.info( "============= Inicio Upload Incoming Visa Nacional : StepUploadIncomingVisa ==============" );
            if ( UploadVisaSrv.getInstance().uploadVisa() ) {
                LOG.info( "Envio Correo de Upload  Satisfactoria: Proceso Upload Incoming Visa Nacional" );
                MailSendSrv.getInstance().sendMailOk(
                        MsgExitoMail.EXITO_UPLOAD_INC_VISA_TITLE.toString(),
                        MsgExitoMail.EXITO_UPLOADTE_INC_VISA_BODY.toString() );
                
            }
            else {
                LOG.info( "Upload Incoming Visa Nacional no Ejecutado Finaliza!" );
                
            }
        }
        catch ( AppException e ) {
            LOG.error( e.getMessage(), e );
            MailSendSrv.getInstance().sendMail(
                    MsgProceso.MSG_GENERIC_ERROR_MSJ.toString(),
                    ( e.getStackTraceDescripcion() != null ) ? e
                            .getStackTraceDescripcion() : e.getMessage() );
        }
    }
    
}
