package com.sbpay.sgi.cron.srv.process;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dao.RechazarDao;
import com.sbpay.sgi.cron.dao.RechazarDaoFactory;
import com.sbpay.sgi.cron.dto.ParamRechazoDTO;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.dto.TblCronLogDTO;
import com.sbpay.sgi.cron.dto.TmpRechazosDTO;
import com.sbpay.sgi.cron.enums.ConstantesPRTS;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.FilterTypeSearch;
import com.sbpay.sgi.cron.enums.LogBD;
import com.sbpay.sgi.cron.enums.MsgErrorMail;
import com.sbpay.sgi.cron.enums.MsgErrorRechazosFile;
import com.sbpay.sgi.cron.enums.MsgErrorSQL;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.srv.utils.CommonOutgoingSrv;
import com.sbpay.sgi.cron.srv.utils.CommonSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;
import com.sbpay.sgi.cron.utils.generals.DateUtils;
import com.sbpay.sgi.cron.validation.ProcessValidator;
import com.sbpay.sgi.cron.validation.impl.ProcessValidatorImpl;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 11/01/2016, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * Este proceso se encarga de leer el archivo de rechazo IC y cargar 
 * las transacciones informadas en el Sistema, la lectura se realiza 
 * al buscar en una ruta local del servidor todos los archivos que 
 * posean el prefijo y terminen con una extension definida por la tabla 
 * de parametros TBL_PRTS a continuación se valida en la tabla TBL_LOG_RCH 
 * que el archivo no este procesado.Clase que realiza la logica de 
 * procesamiento de los rechazos.
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public final class ProcesarRechazosTbkSrv extends CommonSrv {
    /** The Constant LOGGER. */
    private static final Logger LOG = Logger
            .getLogger(ProcesarRechazosTbkSrv.class.getName());

	private static final String RECHAZO_FLAG_PROCESS_ERROR = "-1";

	private static final String RECHAZO_FLAG_PROCESS_OK = "1";
	
	private final ProcessCodeType PROCESO_ACTUAL = ProcessCodeType.PROCESAR_RECHAZOS;
    
    /** The single instance. */
    private static ProcesarRechazosTbkSrv singleINSTANCE = null;
	/** variable parametros de los rechazos */
	private ParamRechazoDTO paramDto = null;

    /**
     * Creates the instance.
     */
    private static void createInstance() {
        synchronized ( ProcesarRechazosTbkSrv.class ) {
            if ( singleINSTANCE == null ) {
                singleINSTANCE = new ProcesarRechazosTbkSrv();    
               
            }
        }
    }
    
    /**
     * Patron singleton.
     * 
     * @return ProcesarRechazosTbkSrv retorna instancia del servicio.
     */
    public static ProcesarRechazosTbkSrv getInstance() {
        if ( singleINSTANCE == null ) {
            createInstance();
        }
        return singleINSTANCE;
    }
    
    /**
     * Metodo de restricion de sobreescritura de la clase.
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        super.clone();
        throw new CloneNotSupportedException();
    }
    
    /**
     * Constructor Privado.
     */
    private ProcesarRechazosTbkSrv() {
        super();
    }

    /**
     * Metodo se encarga de buscar los archivos de rechazos pendientes de 
     * procesar y cargarlos en la BD para su posterior acualizacion de 
     * procesados.
     * @return boolean true: procesados; false: no procesado total o parcial.
     * @throws AppException Exception App.
     */
	public boolean procesarRechazos() throws AppException {
	    paramDto = new ParamRechazoDTO();
		boolean flag = false;
		List<String> fileProcess = null;
		List<String> fileRch = null;
		TblCronLogDTO logDTO = null;
		int countFileProcessed = 0;
		List<TmpRechazosDTO> list = null;
		LOG.info("INICIO PROCESO RECHAZOS");
		
	    ProcessValidator validadorProceso = new ProcessValidatorImpl();
		if (validadorProceso.isActiveProcess(PROCESO_ACTUAL)) {
	        LOG.info("Proceso está activo");
	      
	        //SETEA PARAMETROS DEL CRON
		getParamatrosCron();
		if (validadorProceso.hasTheValidParametersForTheProcess( PROCESO_ACTUAL, paramDto )) {
			//SETEA VARIABLE DEL CRON
			String pathSearch = paramDto.getPathAbcRch();
			String startWith = paramDto.getFormatFileNameAbcRch();
			String extFile = paramDto.getFormatExtNameRch();
			// BUSCA LOS ARCHIVOS QUE COMIENCEN CON PREFIXFECHA EN UNA RUTA LOCAL DEL SERVER
			fileProcess = getFileStartsWith(pathSearch, startWith, extFile);
			LOG.info("Lista de archivos que contiene formato de nombre valido para procesar:  " + fileProcess);
			
			// VALIDA QUE EXISTAN ARCHIVOS POTENCIALES A PROCESAR
			if (fileProcess != null && fileProcess.size() > 0) {
				//BUSCA QUE ARCHIVOS DE RCH ESTEN PENDIENTES DE CARGAR
				fileRch = getFileRch(fileProcess);
				LOG.info("Lista de archivos que estan pendientes de procesar:  " + fileProcess);
				
				//VALIDA QUE EXISTAN ARCHIVOS DE RCH PARA CARGAR
				if (fileRch != null && fileRch.size() > 0) {
					//LISTA LOS ARCHIVOS A PROCESAR RCH
					for (String file : fileRch) {
						LOG.info("Comienza a buscar fichero");
						int index = (file.length()-1) - extFile.length();
						String operador = file.substring(index-1, index);
						LOG.info("Operador en el nombre de fichero = " + operador);
						
						Integer operadorReal = CommonOutgoingSrv.getIDOperador(operador);
						LOG.info("Valor real = " + operadorReal);
						if(operadorReal == 0){
							operador = "";
						}
						
						LOG.info("Valor final de operador = " + operador);
						
						// 1- PROCESA ARCHIVO RECHAZOS
						try {
							list = readRechazoFile(paramDto.getPathAbcRch() + file, operador);
						} catch (AppException e) {
						  LOG.error(MsgErrorRechazosFile.ERROR_READ_RECHAZOS_TITLE.toString(), e);
							//MANDA CORREO
							MailSendSrv.getInstance().sendMail(
									MsgErrorRechazosFile.ERROR_READ_RECHAZOS_TITLE.toString(),
									e.getCause().toString());
							// MUEVE ARCHIVO A LA CARPETA DE ERROR
		                    CommonsUtils.moveFile( paramDto.getPathAbcRch() + file,
		                    		paramDto.getPathAbcRchError() + file );
		                    list = null;
						}
						//VALIDA QUE EXISTAN TRANSACCIONES
						if (list != null) {
							//SETEA EL DTO DE LA TABLA DE LOG DE RCH
							logDTO = setCronLog(file);
							if (logDTO != null) {
								// 2 - SE GUARDAN LAS TRANSACCIONES DE RECHAZO EN BD : TBL_LOG_RCH
								saveData(list, logDTO);

								// 3 - SE MUEVE INCOMING A LA CARPETA BACKUP
								CommonsUtils.moveFile(paramDto.getPathAbcRch()+ file, paramDto.getPathAbcRchBkp()+ file);
								countFileProcessed++;
							}
						}
					}
		            // SI NO SE PROCESA NINGUN FILE ENVIA EMAIL INFORMANDO
		            if ( countFileProcessed == 0 ) {
		                LOG.warn( MsgErrorRechazosFile.WARNING_NO_FILE_RECHAZOS_TITLE );
		                MailSendSrv
		                        .getInstance()
		                        .sendMail(
		                        		MsgErrorRechazosFile.WARNING_NO_FILE_RECHAZOS_TITLE
		                                        .toString(),
		                                MsgErrorRechazosFile.WARNING_NO_FILE_RECHAZO_CAUSE
		                                        .toString() );
					} else {
						flag = (countFileProcessed == fileRch.size());
						LOG.info("Archivos de rechazo: " + fileRch.size()
								+ " Porcesados correctamente: "
								+ countFileProcessed);
					}
					
				} else {
					 LOG.info( "Todo archivos de rechazos estan procesados" );
				}
			} else {
				LOG.info("No existen archivos en la carpeta de rechazos");
			}
		} else {
			LOG.info("Parametros de Base de Datos Incompletos proceso : Procesar Rechazos");
			MailSendSrv.getInstance().sendMail(
					MsgErrorMail.ALERTA_RCH_PRTS.toString(),
					MsgErrorMail.ALERTA_RCH_PRTS_TXT.toString());
		}
		return flag;
		
		}else{
		    String asunto = MsgProceso.MSG_PROCESO_DESACTIVADO_ASUNTO
                    .toString().replace( ":?:",
                            PROCESO_ACTUAL.getCodigoProceso() );
            String mensaje = MsgProceso.MSG_PROCESO_DESACTIVADO_BODY.toString()
                    .replace( ":?:", PROCESO_ACTUAL.getCodigoProceso() );
            MailSendSrv.getInstance().sendMail( asunto, mensaje );

		}
        return flag;
	}

	/**
	 * Metodo lee el archivo de rechazos y los carga en un objeto temporal.
	 * @param ruta del archivo a leer.
	 * @return Lista de TMP_RECHAZOS.
	 * @throws AppException Exception App.
	 */
	private List<TmpRechazosDTO> readRechazoFile(final String ruta, String operador) throws AppException {
        BufferedReader buffReader = null; 
        String [] array = null;
        List<TmpRechazosDTO> list = new ArrayList<TmpRechazosDTO>();
        TmpRechazosDTO dto = null;
        String fechaTransac = null;
        FileReader fr = null;
		try {
		    fr = new FileReader( ruta );
			buffReader = new BufferedReader( fr );
			String line = buffReader.readLine();
			line = buffReader.readLine();
			while (line != null) {
				array = line.split(ConstantesUtil.PUNTO_COMA.toString());
				if (array.length == 12) {
					dto = new TmpRechazosDTO();
					dto.setMerchantCodeOriginal(array[0]);
					dto.setMerchantCodeIc(array[1]);
					dto.setMerchantName(array[2]);
					dto.setReferenceNro(array[3]);
					dto.setTransactionCode(array[4]);
					dto.setAuthNro(array[5]);
					fechaTransac = DateUtils.getStringDatefromYYMMMDDToFormatYYMMDD(array[6]);
					dto.setTransactionDate(fechaTransac);
					dto.setCardNro(array[7]);
					dto.setDebitCredit(array[8]);
					dto.setCurrency(array[9]);
					dto.setGrossAmount(array[10]);
					dto.setRejectReason(array[11]);	
					dto.setOperador(operador);
					list.add(dto);
				} else {
					LOG.info("NO HAY DATOS ");
				}
				line = buffReader.readLine();
			}
			
			return list;
		} catch ( Exception ioe ) {
			throw new AppException( "Ha ocurrido un error al leer el archivo",
                    ioe );
        }
        finally {
        	LOG.info("FIN");
            try {
                if ( buffReader != null ) {
                    buffReader.close();
                }
                if ( fr != null){
                  fr.close();
                }
            }
            catch ( IOException ioe1 ) {
                throw new AppException(
                        "Ha ocurrido un error al cerrar el archivo", ioe1 );
            }
        }

	}

	/**
	 * Metodo setea el DTO del LOG de tabla de rechazos.
	 * @param file nombre del archivo a procesar.
	 * @return TblCronLogDTO dto con los datos a guardar en la BD. 
	 */
	private TblCronLogDTO setCronLog(String file) {
		TblCronLogDTO dto = null;
		LOG.info("METODO: setCronLog");
		String fileDate = file.replace(paramDto.getFormatFileNameAbcRch(), "");
		String dateFile = DateUtils.getDateFileTbk(fileDate);
		if (dateFile!=null) {
			dto = new TblCronLogDTO();
			dto.setFecha(dateFile);
			dto.setFileFlag(RECHAZO_FLAG_PROCESS_OK);
			dto.setFilename(file);
		}
		return dto;
	}

	/**
	 * Metodo procesa el archivo de rechazo a traves de una lista de 
	 * transacciones y guarda actualiza la tabla de log de rechazos al 
	 * finalizar TBL_LOG_RCH
	 * @param rchDto dto con lista de rechazos.
	 * @param logDTO dto a actualizar el log de rechazos.
	 * @throws AppException Exception App.
	 */
	private void saveData(List<TmpRechazosDTO> list, TblCronLogDTO logDTO) throws AppException{
		RechazarDao rchDao = null;
		CommonsDao daoCommons = null;
		Integer flag = 0;
		try {
			daoCommons = CommonsDaoFactory.getInstance().getNewEntidadDao();
			rchDao = RechazarDaoFactory.getInstance().getNewEntidadDao();
			// RECORRE TODAS LAS TRANSACCIONES DEL RECHAZO
			for (TmpRechazosDTO instance : list) {
					// GUARDA EN TMP_INCOMING, SI HAY ERROR SUMA -1
					if (instance != null && !rchDao.saveRchTransaction(instance)) {
						flag = flag - 1;
					} 

			}
			// COMMMIT DE TRANSACCIONES SAVE ACUMULADAS
			if (flag == 0) {
				rchDao.endTx();
				// UNA VEZ GUARDADAS LAS TRASSACCIONES SE ACTUALIZA
				// LA TABLA LOG DE LOS RECHAZOS
				LOG.info(" UPDATE FLAG TBL_LOG_RCH -  PATH : "
						+ paramDto.getPathAbcRch() + logDTO.getFilename());
				
				if (daoCommons.updateTblCronLogByType(logDTO,
						LogBD.TABLE_LOG_RECHAZO.getTable(),
						FilterTypeSearch.FILTER_BY_DATE)) {
					LOG.info("COMMITEA UPDATE RCH");
					daoCommons.endTx();
					// SE LLAMA AL SP CARGA RECHAZOS
					rchDao.callSPCargaRechazos();
				} else {
					// SE REALIZA ROLLBACK
					rollBackTransaccion(daoCommons);
				}
			} else {
				// SE REALIZA ROLLBACK
				rollBackTransaccion(rchDao);
				// SE UPDATEA EL FLAG A PROCESADO CON ERROR
				logDTO.setFileFlag(RECHAZO_FLAG_PROCESS_ERROR);
				daoCommons.updateTblCronLogByType(logDTO,
						LogBD.TABLE_LOG_RECHAZO.getTable(),
						FilterTypeSearch.FILTER_BY_DATE);
				rchDao.endTx();
			}
		} catch (SQLException e) {
			// SE REALIZA ROLLBACK
			rollBackTransaccion(rchDao);
			rollBackTransaccion(daoCommons);
			throw new AppException(MsgErrorSQL.ERROR_SAVE_RCH.toString(),
					e);
		}
        finally {
            try {
                if ( daoCommons != null ) {
                	daoCommons.close();
                }
                if ( rchDao != null ) {
                	rchDao.close();
                }
                
            }
            catch ( SQLException e ) {
    			// SE REALIZA ROLLBACK
    			rollBackTransaccion(rchDao);
    			rollBackTransaccion(daoCommons);
                throw new AppException(
                        MsgErrorSQL.ERROR_SAVE_RCH.toString(),
                        e );
            }
        }
        
	}

	/**
	 * Metodo busca los rechazos pendientes de procesar.
	 * @param fileProcess lista de archivos candidatos de procesar.
	 * @return Lista de archivos para procesar.
	 * @throws AppException Exception App.
	 */
	private List<String> getFileRch(final List<String> fileProcess)
			throws AppException {
		List<String> fileRch = null;
		String dateFile = null;
        String fileAux = null;
        TblCronLogDTO dto = null;
        TblCronLogDTO dtoAux = new TblCronLogDTO();
        CommonsDao daoCommons = null;
        try {
        	fileRch = new ArrayList<String>();
        	daoCommons = CommonsDaoFactory.getInstance().getNewEntidadDao();
        	for (String fileName : fileProcess) {
				fileAux = fileName.replace(paramDto.getFormatFileNameAbcRch(),
						"");
                dateFile = DateUtils.getDateFileTbk(fileAux);
				if (dateFile != null) {
					dtoAux.setFecha(dateFile);
					dto = daoCommons.getTblCronLogByType(dtoAux,
							LogBD.TABLE_LOG_RECHAZO.getTable(),
							FilterTypeSearch.FILTER_BY_DATE);
					if (dto != null) {
						fileRch.add(fileName);
					}
				} else {
					LOG.info("Archivo no cumple con el formato para obtener la fecha al procesar los rechazos "
							+ fileName);
                    /* Se mueve archivo a path de error incoming */
                    CommonsUtils.moveFile( paramDto.getPathAbcRch() + fileName,
                    		paramDto.getPathAbcRchError() );
				}
			}
    		return fileRch;
        }
        catch ( SQLException e ) {
            throw new AppException(
                    MsgErrorSQL.ERROR_QUERY_PROCESS_RCH.toString(), e );
        }
        finally {
            try {
                if ( daoCommons != null ) {
                	daoCommons.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException(
                        MsgErrorSQL.ERROR_QUERY_PROCESS_CLOSE_RCH.toString(),
                        e );
            }
        }
        

	}
	
	
	
	



	/**
	 * Parametros que cargan las variables que se utilizan en el proceso de 
	 * rechazos.
	 * @throws AppException Exption app.
	 */
	private void getParamatrosCron() throws AppException {
		CommonsDao daoCommon = null;
		List<ParametroDTO> paramDTOList = null;
		try {
			LOG.info("CARGA PARAMETROS RECHAZOS");
			daoCommon = CommonsDaoFactory.getInstance().getNewEntidadDao();
			paramDTOList = daoCommon
					.getParametroCodGrupoDato(ConstantesPRTS.CRON_TBK.toString());
			if (paramDTOList != null && paramDto != null) {
		        // FORMATO DE NOMBRE RECHAZOS IC
				paramDto.setFormatFileNameAbcRch(
						CommonsUtils.getCodiDato(paramDTOList,
						ConstantesPRTS.FORMAT_FILE_ABC_RCH.toString()));
		        // RUTA SERVIDOR DONDE SE LEERA EL ARCHIVO DE RECHAZOS PARA PROCESAR
				paramDto.setPathAbcRch(CommonsUtils.getCodiDato(paramDTOList,
						ConstantesPRTS.PATH_ABC_RCH.toString()));
				// RUTA SERVIDOR DONDE SE DEJARA EL ARCHIVO DE RECHAZOS PROCESADOS BKP    				
				paramDto.setPathAbcRchBkp(CommonsUtils.getCodiDato(paramDTOList,
						ConstantesPRTS.PATH_ABC_RCH_BKP.toString()));
		        // RUTA SERVIDOR DONDE SE DEJARA EL ARCHIVO DE RECHAZOS PROCESADO CON ERROR				
				paramDto.setPathAbcRchError(CommonsUtils.getCodiDato(paramDTOList,
						ConstantesPRTS.PATH_ABC_RCH_ERROR.toString()));
		        // FORMATO DE LA EXTENSION DEL ARCHIVO VISA DE TRANSBANK				
				paramDto.setFormatExtNameRch(CommonsUtils.getCodiDato(paramDTOList,
						ConstantesPRTS.FORMAT_EXTNAME_RCH.toString()));
			} else {
				LOG.info("Parametos no encontrados o paramDto nulo");
				paramDto = null;
			}				
		} catch (SQLException e) {
			throw new AppException(MsgErrorSQL.ERROR_QUERY_RCH_PRTS.toString(), e);
		} finally {
			try {
				if (daoCommon != null) {
					daoCommon.close();
				}
			} catch (SQLException e) {
				throw new AppException(MsgErrorSQL.ERROR_QUERY_RCH_PRTS_CLOSE.toString(), e);
			}
		}
	}   
}
