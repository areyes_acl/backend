package com.sbpay.sgi.cron.dao;

import java.sql.SQLException;
import java.util.List;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.Dao;
import com.sbpay.sgi.cron.dto.Transaccion;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 9/12/2015, (ACL) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SA.</B>
 */
public interface GenerarIncomingDAO extends Dao {
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 - 9/12/2015, (ACL) - versión inicial
     * </ul>
     * <p>
     * 
     * @return List<Transaccion> Lista de transacciones encontradas en
     *         la tabla TMP_OUTGOING
     * @throws SQLException
     * @throws AppException
     * @since 1.X
     */
    public List<Transaccion> loadTransactions() throws AppException;
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 - 11/12/2015, (ACL) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @throws AppException
     * @throws SQLException
     * @since 1.X
     */
    public boolean callSPCargaOutgoingTransbank() throws AppException;
    
    public boolean callSPCargaOutgoingVisa(final int operador) throws AppException;
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 17/12/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @throws AppException
     * @since 1.X
     */
    public void clearTemporalOutgoingTransbank() throws AppException;
    
}
