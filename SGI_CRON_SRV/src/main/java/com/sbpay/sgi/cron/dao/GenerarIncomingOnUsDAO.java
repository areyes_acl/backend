package com.sbpay.sgi.cron.dao;

import java.sql.SQLException;
import java.util.List;

import com.sbpay.sgi.cron.dao.Dao;
import com.sbpay.sgi.cron.dto.TransaccionOnUsDTO;
import com.sbpay.sgi.cron.enums.ConstantesBD;


/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 28/01/2015, (ACL) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SA.</B>
 */
public interface GenerarIncomingOnUsDAO extends Dao {
	static final String CALL_SP_SIG_CARGAOUT_ONUS_TBK_VISA = "{call SP_SIG_CARGAOUT_TBK_ONUS_VISA(?,?)}";
	static final String CALL_SP_SIG_CLEAROUT_ONUS_TBK_VISA = "{call SP_SIG_CLEAN_OUT_TBK_ONUS_VISA(?,?)}";
	static final String SELECT_ALL_FROM_TMP_OUTGOING_ONUS_TRANSBANK = "SELECT SID, CODIGO_TRANSACCION, ESTADO_PROCESO, NUMERO_TARJETA,FECHA_COMPRA, FECHA_AUTORIZACION, FECHA_POSTEO, TIPO_VENTA, NUM_CUOTAS, NUM_MICROFILM, NUM_COMERCIO, MONTO_TRANSACCION, VALOR_CUOTA,NOMBRE_COMERCIO, CIUDAD_COMERCIO, RUBRO_COMERCIO, COD_AUTOR FROM "+ ConstantesBD.ESQUEMA.toString() + ".TMP_TRANSAC_OUT_ONUS ";

	boolean callSPCargaOutgoingOnUsTransbank() throws SQLException;

	List<TransaccionOnUsDTO> loadTransactions() throws SQLException;

	void clearTemporalOutgoingOnUsTransbank() throws SQLException;

}
