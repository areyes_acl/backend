package com.sbpay.sgi.cron.dao;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.dao.BaseDao;

import oracle.jdbc.OracleCallableStatement;

public class TrxConciliarAvancesDaoJdbc extends BaseDao<TrxConciliarAvancesDao> implements TrxConciliarAvancesDao{
	
    /** VARIABLE PARA EL LOGER */
    private static final Logger LOGGER = Logger.getLogger( TrxConciliarAvancesDaoJdbc.class );
    
    /**
     * Constructor con conexion.
     * 
     * @param connection
     *            Conexion JDBC.
     */
    public TrxConciliarAvancesDaoJdbc( Connection connection ) {
        super( connection );
    }

	
	@Override
	public boolean callSPCargaConciliacionAvances() throws SQLException {
	    Connection dbConnection = null;
	    OracleCallableStatement callableStatement = null;

	    try {
	      String callSPCargaDatosSql = "{call SP_SIG_CONC_AVANCE(?,?)}";

	      dbConnection = getConnection();
	      callableStatement =
	          (OracleCallableStatement) dbConnection.prepareCall(callSPCargaDatosSql);

	      callableStatement.registerOutParameter(1, java.sql.Types.VARCHAR);
	      callableStatement.registerOutParameter(2, java.sql.Types.NUMERIC);

	      callableStatement.executeUpdate();

	      String warning = callableStatement.getString(1);
	      Integer codigoError = callableStatement.getInt(2);
	      LOGGER.info("****** SE EJECUTA STORE PROCEDURE SP_SIG_CONC_AVANCE *****");
	      LOGGER.info("WARNING : " + warning);
	      LOGGER.info("codigoError : " + codigoError);
	      LOGGER.info("*********** STORE PROCEDURE SP_SIG_CONC_AVANCE *********** ");

	    } finally {
	      if (callableStatement != null) {
	        callableStatement.close();
	      }
	    }
	    return Boolean.TRUE;
		
	}

}
