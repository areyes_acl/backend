package com.sbpay.sgi.cron.dao;

import java.sql.SQLException;

import com.sbpay.sgi.cron.dao.Dao;
import com.sbpay.sgi.cron.dto.Transaccion;
import com.sbpay.sgi.cron.dto.TransaccionOutVisaCobroCargo;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 19/11/2015, (ACL) -  versión inicial 
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SA.</B>
 */
public interface ProcesarIncomingDao extends Dao {
    
    public boolean saveTransaction(Transaccion transaccion, Integer sidOperador)throws SQLException;
    
    public boolean callSPCargaIncoming() throws SQLException;

	public boolean saveTransactionCargoAbono(TransaccionOutVisaCobroCargo transaccion,
			Integer sidOperador) throws SQLException;
	
	public boolean callSPCobroCargo() throws SQLException;
    
}
