package com.sbpay.sgi.cron.srv.export;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dao.GenerarIncomingDAO;
import com.sbpay.sgi.cron.dao.GenerarIncomingDaoFactory;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.dto.ParamsGenerateIncomingDTO;
import com.sbpay.sgi.cron.dto.TblCronLogDTO;
import com.sbpay.sgi.cron.dto.Transaccion;
import com.sbpay.sgi.cron.enums.ConstantesOperador;
import com.sbpay.sgi.cron.enums.ConstantesPRTS;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.DateFormatType;
import com.sbpay.sgi.cron.enums.LogBD;
import com.sbpay.sgi.cron.enums.MsgErrorMail;
import com.sbpay.sgi.cron.enums.MsgErrorSQL;
import com.sbpay.sgi.cron.enums.MsgGenerationIncoming;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.enums.StatusProcessType;
import com.sbpay.sgi.cron.srv.utils.CommonSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;
import com.sbpay.sgi.cron.utils.file.IncomingExport;
import com.sbpay.sgi.cron.utils.file.IncomingFileUtils;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;
import com.sbpay.sgi.cron.utils.generals.DateUtils;
import com.sbpay.sgi.cron.validation.ProcessValidator;
import com.sbpay.sgi.cron.validation.impl.ProcessValidatorImpl;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 7/12/2015, (ACL) - versión inicial
 * </ul>
 * <p>
 * 
 * Clase encargada de generar el outgoing
 * <p>
 * <B>Todos los derechos reservados por ACL SA.</B>
 */
public class GenerarIncomingSrv extends CommonSrv {

  /** VARIABLE PARA EL LOGER */
  private static final Logger LOGGER = Logger.getLogger(GenerarIncomingSrv.class);
  /** NUMERO DE SECUENCIA **/
  private static final String SECUENCE_NUMBER = "1";
  /** The single instance. */
  private static GenerarIncomingSrv singleINSTANCE = null;
  /** NOMBRE PROCESO */
  private final ProcessCodeType PROCESO_ACTUAL = ProcessCodeType.GENERAR_INCOMING;

  /**
   * Creates the instance.
   */
  private static void createInstance() {
    synchronized (GenerarIncomingSrv.class) {
      if (singleINSTANCE == null) {
        singleINSTANCE = new GenerarIncomingSrv();
      }
    }
  }

  /**
   * Patron singleton.
   * 
   * @return ActivarPrepagoSrv retorna instancia del servicio.
   */
  public static GenerarIncomingSrv getInstance() {
    if (singleINSTANCE == null) {
      createInstance();
    }
    return singleINSTANCE;
  }

  /**
   * Metodo de restricion de sobreescritura de la clase.
   */
  @Override
  protected Object clone() throws CloneNotSupportedException {
    super.clone();
    throw new CloneNotSupportedException();
  }

  /**
   * Constructor Privado.
   * 
   * @throws SQLException
   */
  private GenerarIncomingSrv() {
    super();
  }

  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 - 10/12/2015, (ACL) - versión inicial
   * </ul>
   * <p>
   * Metodo encargado de realizar todo el proceso de generacion de incoming a transbank, en las
   * carpetas correspondientes, el cual tanto en caso de error como en caso exitoso envia un email
 * @throws AppException 
   * 
   * 
   * @since 1.X
   */
  public void generaArhivoIncoming() throws AppException {
    GenerarIncomingDAO generarIncomingDAO = null;

    try {
        
      ProcessValidator validadorProceso = new ProcessValidatorImpl();

      if (validadorProceso.isActiveProcess(PROCESO_ACTUAL)) {

      generarIncomingDAO = GenerarIncomingDaoFactory.getInstance().getNewEntidadDao();

      // CARGA PARAMETROS BD
      ParamsGenerateIncomingDTO parametros = getParametros();

      if (validadorProceso.hasTheValidParametersForTheProcess( PROCESO_ACTUAL,parametros)) {

    	  CommonsDao commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
		  String binOperador = commonsDAO.getBinOperador(ConstantesOperador.TBK.toString());
		  
        // LLAMA AL SP_CARGA_OUTGOING_TRANSBANK
        if (generarIncomingDAO.callSPCargaOutgoingTransbank()) {
          LOGGER.info("SE HA LLAMADO AL PROCEDIMIENTO CORRECTAMENTE");
        }


        String incomingFilename = generateIncomingFilename(parametros);
        String controlFilename = generateControlFilename(parametros);
        String ruta = parametros.getPathABCIncoming();

        // OBTIENE LA LISTA DE TRANSACCIONES DESDE BD
        List<Transaccion> listaTrasacciones = generarIncomingDAO.loadTransactions();

        // ESCRIBE EN LOG ERROR NO SE ENCONTRARON RESULTADOS
        if (listaTrasacciones != null && listaTrasacciones.size() == 0) {
          LOGGER.info(MsgGenerationIncoming.WARNING_TRANSACTIONS_NOT_FOUND.toString());
        }

        // GENERA ARCHIVO INCOMING Y ARCHIVO DE CONTROL CON LAS
        // TRANSACCIONES ENCONTRADAS
        IncomingFileUtils incomingOut = new IncomingExport(binOperador);
        incomingOut.exportIncomingFile(ruta, incomingFilename, controlFilename, listaTrasacciones);

        // BORRA TABLA TMP_OUTGOING_TRANSBANK UNA VEZ QUE EL
        // PROCESO SE EJECUTA CORRECTAMENTE
        generarIncomingDAO.clearTemporalOutgoingTransbank();
        generarIncomingDAO.endTx();
        
        // SE DEJA REGISTRO EN LA TABLA TBL_LOG_OUT
        saveLog( incomingFilename );


        // SE ENVIA EMAIL CONFIRMANDO PROCESO EXITOSO
        MailSendSrv.getInstance().sendMailOk(
            MsgGenerationIncoming.SUCCESFUL_GENERATE_INCOMING_TITTLE.toString(),
            MsgGenerationIncoming.SUCCESFUL_GENERATE_INCOMING_MSG.toString());

      } else {
        LOGGER.error("Parametros de Base de Datos Incompletos");
        MailSendSrv.getInstance().sendMail(MsgErrorMail.ALERTA_PRTS.toString(),
            MsgErrorMail.ALERTA_PRTS_TXT.toString());
      }
      
      }else{
          String asunto = MsgProceso.MSG_PROCESO_DESACTIVADO_ASUNTO
                  .toString().replace( ":?:",
                          PROCESO_ACTUAL.getCodigoProceso() );
          String mensaje = MsgProceso.MSG_PROCESO_DESACTIVADO_BODY.toString()
                  .replace( ":?:", PROCESO_ACTUAL.getCodigoProceso() );
          MailSendSrv.getInstance().sendMail( asunto, mensaje );          
      }
     
    } catch (SQLException e) {
         LOGGER.error( e.getMessage(),e);
         throw new AppException( e);

    } finally {

      try {
        if (generarIncomingDAO != null) {
          generarIncomingDAO.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
        LOGGER.error(e.getMessage(), e);
      }
    }


  }

  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 18/12/2015, (ACL-sbpay) - versión inicial
   * </ul>
   * <p>
   * 
   * @param incomingFilename
   * @throws AppException
   * 
   * @since 1.X
   */
  private void saveLog(String incomingFilename) throws AppException {
    CommonsDao commonsDAO = null;
    TblCronLogDTO log = new TblCronLogDTO();
    log.setFilename(incomingFilename);
    log.setFileFlag(StatusProcessType.PROCESS_PENDING.getValue());
    log.setFecha(DateUtils.parseFormat(DateUtils.getTodayInYYMMDDFormat(),
        DateFormatType.FORMAT_YYMMDD, DateFormatType.FORMAT_DDMMYY_WITH_SLASH_SEPARATOR));
    try {
      commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
      commonsDAO.saveTblCronLog(log, LogBD.TABLE_LOG_OUTGOING);
      commonsDAO.endTx();
    } catch (SQLException e) {
      try {
        if (commonsDAO != null) {
          commonsDAO.rollBack();
        } else {
          LOGGER.info("No se ha ejecutado rollback");
        }
      } catch (SQLException e1) {
        throw new AppException("Ha ocurrido un error al realizar rollback", e1);
      }
      throw new AppException(e.getMessage(), e);
    } finally {
      try {
        if (commonsDAO != null) {
          commonsDAO.close();
        }
      } catch (SQLException e) {
        throw new AppException("Ha ocurrido un error al realizar rollback", e);
      }

    }

  }


  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 18/2/2016, (ACL-sbpay) - versiÃ³n inicial
   * </ul>
   * <p>
   * 
   * Metodo que genera el nombre del archivo incoming que posteriormente se enviara a Transbank.
   * 
   * Formato incoming : Starwith + date (mmdd) + 1 + Endwith.
   * 
   * Ejemplo : INP4902181.DAT
   * 
   * @param parametros Incoming de filename
   * @return
   * @throws AppException
   * @since 1.X
   */
  private String generateIncomingFilename(ParamsGenerateIncomingDTO parametros) throws AppException {

    String date = DateUtils.getDateTodayInSpecialFormat();
    StringBuilder str = new StringBuilder();
    str.append(parametros.getFormatFilenameInc());
    str.append(date);
    str.append(SECUENCE_NUMBER);
    str.append(ConstantesUtil.POINT);
    str.append(parametros.getFormatExtNameInc());

    return str.toString();

  }

  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 18/2/2016, (ACL-sbpay) - versiÃ³n inicial
   * </ul>
   * <p>
   * 
   * Metodo que genera el nombre del archivo de control del incoming que posteriormente se enviara a
   * Transbank.
   * 
   * Formato ctr : StarWithIncoming + date (mmdd) + 1 + EndsWithCTR.
   * 
   * Ejemplo : INP4902181.DAT_CTR
   * 
   * @param parametros
   * @return
   * @since 1.X
   */
  private String generateControlFilename(ParamsGenerateIncomingDTO parametros) {
    String date = DateUtils.getDateTodayInSpecialFormat();
    StringBuilder str = new StringBuilder();
    str.append(parametros.getFormatFilenameInc());
    str.append(date);
    str.append(SECUENCE_NUMBER);
    str.append(ConstantesUtil.POINT);
    str.append(parametros.getFormatExtNameIncCrt());

    return str.toString();
  }
  
  
 
  


  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
   * </ul>
   * <p>
   * Metodo que obtiene los parametros de la aplicacion necesarios para el proceso estos los saca
   * desde la tabla TBL_PRTS
   * 
   * @return
   * @throws AppException
   * @since 1.X
   */
  private ParamsGenerateIncomingDTO getParametros() throws AppException {
    ParamsGenerateIncomingDTO parametros = null;
    List<ParametroDTO> paramDTOList = null;
    CommonsDao commonsDAO = null;

    try {
      // OBTIENE PARAMETROS DESDE LA BD
      commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
      paramDTOList = commonsDAO.getParametroCodGrupoDato(ConstantesPRTS.CRON_TBK.toString());
      LOGGER.info("LISTA DE PARAMETROS : " + paramDTOList.size());

      // SI EXISTEN PARAMETROS EN BD SE EXTRAEN
      if (paramDTOList != null) {

        parametros = new ParamsGenerateIncomingDTO();

        // RUTA DONDE SE DEJARAN EL ARCHIVO INCOMING TBK Y EL ARCHIVO DE CONTROL GENERADOS
        parametros.setPathABCIncoming(CommonsUtils.getCodiDato(paramDTOList,
            ConstantesPRTS.PATH_ABC_INC.toString()));

        // FORMATO DEL NOMBRE DEL ARCHIVO INCOMING DE TRANSBANK GENERADO
        parametros.setFormatFilenameInc(CommonsUtils.getCodiDato(paramDTOList,
            ConstantesPRTS.FORMAT_FILENAME_INC.toString()));

        // FORMATO DE LA EXTENSION DEL ARCHIVO INCOMING TRANSBANK GENERADO
        parametros.setFormatExtNameInc(CommonsUtils.getCodiDato(paramDTOList,
            ConstantesPRTS.FORMAT_EXTNAME_INC.toString()));

        // FORMATO DE LA EXTENSION DEL ARCHIVO DE CONTROL
        parametros.setFormatExtNameIncCrt(CommonsUtils.getCodiDato(paramDTOList,
            ConstantesPRTS.FORMAT_EXTNAME_INC_CTR.toString()));
      }

      // SI NO EXISTEN TODOS LOS PARAMETROS RETORNA NULL
      if ((parametros != null) && parametros.hasAllParameters()) {
        return parametros;
      } else {
        parametros = null;
        return parametros;
      }
    } catch (SQLException e) {
      throw new AppException(MsgErrorSQL.ERROR_QUERY_PRTS.toString(), e);
    } finally {
      try {
        if (commonsDAO != null) {
          commonsDAO.close();
        }
      } catch (SQLException e) {
        throw new AppException(MsgErrorSQL.ERROR_QUERY_PRTS_CLOSE.toString(), e);
      }
    }
  }


}
