package com.sbpay.sgi.cron.srv.process;

import java.io.File;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dao.TrxOnUsDao;
import com.sbpay.sgi.cron.dao.TrxOnUsDaoFactory;
import com.sbpay.sgi.cron.dto.ICCodeHomologationDTO;
import com.sbpay.sgi.cron.dto.LogTransaccionOnUsDTO;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.dto.ParamsProcessOnusDTO;
import com.sbpay.sgi.cron.dto.TblCronLogDTO;
import com.sbpay.sgi.cron.dto.TransaccionOnUsDTO;
import com.sbpay.sgi.cron.enums.ConstantesPRTS;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.FilterTypeSearch;
import com.sbpay.sgi.cron.enums.LogBD;
import com.sbpay.sgi.cron.enums.MsgErrorMail;
import com.sbpay.sgi.cron.enums.MsgErrorProcessFile;
import com.sbpay.sgi.cron.enums.MsgErrorProcessFileOnus;
import com.sbpay.sgi.cron.enums.MsgErrorSQL;
import com.sbpay.sgi.cron.enums.MsgExitoMail;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.enums.StatusProcessType;
import com.sbpay.sgi.cron.srv.utils.CommonSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;
import com.sbpay.sgi.cron.utils.file.TransaccionOnUsFileUtil;
import com.sbpay.sgi.cron.utils.file.TransaccionOnUsReader;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;
import com.sbpay.sgi.cron.validation.ProcessValidator;
import com.sbpay.sgi.cron.validation.impl.ProcessValidatorImpl;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 18/01/2016, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * Este proceso se encarga de leer el archivo On Us y cargar las
 * transacciones informadas en el Sistema, la lectura se realiza al
 * buscar en una ruta local del servidor todos los archivos que posean
 * el prefijo y terminen con una extension definida por la tabla de
 * parametros TBL_PRTS a continuación se valida en la tabla
 * TBL_LOG_ONUS que el archivo no este procesado. Este proceso carga
 * una tabla temporal TMP_TRANSAC_ONUS actualiza la TBL_LOG_ONUS y
 * ejecuta un SP SP_SIG_CARGA_LOG_TRX_ONUS para mover a BKP el archivo
 * procesado.
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public final class ProcesarOnUsSrv extends CommonSrv {
    /** The Constant LOGGER. */
    private static final Logger LOG = Logger.getLogger( ProcesarOnUsSrv.class
            .getName() );
    
    /** The single instance. */
    private static ProcesarOnUsSrv singleINSTANCE = null;
    
    private final ProcessCodeType PROCESO_ACTUAL = ProcessCodeType.PROCESAR_ONUS;
    
    /**
     * Creates the instance.
     */
    private static void createInstance() {
        synchronized ( ProcesarOnUsSrv.class ) {
            if ( singleINSTANCE == null ) {
                singleINSTANCE = new ProcesarOnUsSrv();
            }
        }
    }
    
    /**
     * Patron singleton.
     * 
     * @return ProcesarOnusSrv retorna instancia del servicio.
     */
    public static ProcesarOnUsSrv getInstance() {
        if ( singleINSTANCE == null ) {
            createInstance();
        }
        return singleINSTANCE;
    }
    
    /**
     * Metodo de restricion de sobreescritura de la clase.
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        super.clone();
        throw new CloneNotSupportedException();
    }
    
    /**
     * Constructor Privado.
     */
    private ProcesarOnUsSrv() {
        super();
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Metodo principal del servicio procesar onus, encargado de tomar
     * un archivo ONUS y realizar la lectura, transformandolos a
     * registros validos para el sistema sbpay siempre y cuando el
     * archivo Onus venga con el formato acordado e n la interfaz
     * entregada por IC
     * 
     * @throws AppException
     * @since 1.X
     */
    public void processOnus() throws AppException {
        String detalleArchivosProcesados = "";
        ProcessValidator validadorProceso = new ProcessValidatorImpl();
        
        if ( validadorProceso.isActiveProcess( PROCESO_ACTUAL ) ) {
            
            // BUSCAR LOS PARAMETROS NECESARIOS PARA EL FILE
            ParamsProcessOnusDTO parametros = getParametros();
            List<File> listOfFiles = null;
            List<ICCodeHomologationDTO> listaCodigosIc = loadListIcCodes();
            
            if ( listaCodigosIc != null
                    && listaCodigosIc.size() > 0
                    && validadorProceso.hasTheValidParametersForTheProcess(
                            PROCESO_ACTUAL, parametros ) ) {
                
                LOG.info( "==> SE HAN ENCONTRADO LOS PARAMETROS NECESARIOS PARA EL PROCESO, SE DA INICIO AL PROCESAMIENTO DE ONUS...=== " );
                String pathFolder = parametros.getPathAbcOnUs();
                String starWith = parametros.getFormatFileNameOnUs();
                String endsWith = parametros.getFormatExtNameOnUs();
                
                int countFileProcessed = 0;
                
                listOfFiles = CommonsUtils.getFilesInFolder( pathFolder,
                        starWith, endsWith );
                
                LOG.debug( "Lista de archivos :  "
                        + ( ( listOfFiles != null ) ? "" + listOfFiles.size()
                                : "No se encontraron archivos" ) );
                // No hay archivos
                if ( listOfFiles == null ) {
                    LOG.warn( MsgErrorProcessFileOnus.WARNING_NO_FILES_FOUNDS
                            .toString() );
                    return;
                }
                
                for ( File file : listOfFiles ) {
                    if ( file.isFile()
                            && file.getName().startsWith(
                                    parametros.getFormatFileNameOnUs() )
                            && isValidOnus( file.getName() ) ) {
                        try {
                            LOG.info( "Archivo onus : " + file.getName()
                                    + "  válido , Se comienza a procesar..." );
                            
                            // 1- PROCESA ARCHIVO ON US
                            TransaccionOnUsFileUtil reader = new TransaccionOnUsReader();
                            LogTransaccionOnUsDTO logTrx = reader
                                    .readLogTrxOnUsrFile( file, starWith,
                                            listaCodigosIc );
                            logTrx.setFilename( file.getName() );
                            
                            
                            // INFORMA REGISTROS NO SE LEYERON
                            enviaCorreoTrxNoLeidas( logTrx );
                            
                            // 2 - SE GUARDAN LAS TRX ON US EN BD
                            saveData( logTrx );
                            
                              // 3 -- UPDATEA LOG A PROCESADO OK
                            updateFlag(StatusProcessType.PROCESS_SUCESSFUL.getValue(),
                                    logTrx.getFilename() ) ;
                            
                            // 4 - SE MUEVE INCOMING A LA CARPETA
                            // BACKUP
                            CommonsUtils.moveFile(
                                    file.getAbsolutePath(),
                                    parametros.getPathAbcOnUsBkp()
                                            + file.getName() );
                            countFileProcessed++;
                        }
                        catch ( AppException e ) {
                            // ERROR AL PROCESAR ON US SE ENVIA EMAIL
                            LOG.error( MsgErrorProcessFileOnus.ERROR_IN_PROCESS
                                    .toString().concat( ":" ).concat( file.getName() ), e );
                               
                            // UPDATEA REGISTRO A PROCESADO CON ERROR
                            updateFlag( StatusProcessType.PROCESS_ERROR.getValue(),
                                    file.getName());
                            
                            detalleArchivosProcesados = detalleArchivosProcesados.concat( "\n" ).concat( file.getName() );
                            
                            // MUEVE ARCHIVO A LA CARPETA DE ERROR
                            CommonsUtils.moveFile(
                                    file.getAbsolutePath(),
                                    parametros.getPathAbcOnUsError()
                                            + file.getName() );
                        }
                    }
                }
                
                // SI NO SE PROCESA NINGUN FILE ESCRIBE ESCRIBE LOG
                if ( countFileProcessed == 0 ) {
                    LOG.warn( MsgErrorProcessFileOnus.WARNING_NO_FILE_PROCESSING_TITLE
                            + "  - "
                            + MsgErrorProcessFileOnus.WARNING_NO_FILE_PROCESSING_CAUSE );                    
                    MailSendSrv
                            .getInstance()
                            .sendMailOk(
                                    MsgErrorProcessFileOnus.WARNING_NO_FILE_PROCESSING_TITLE
                                            .toString(),
                                    MsgErrorProcessFileOnus.WARNING_NO_FILE_PROCESSING_CAUSE
                                            .toString() );
                    
                 // NO TODOS LOS ARCHIVOS ONUS PROCESADOS    
                }else if (countFileProcessed > 0 &&  countFileProcessed != listOfFiles.size()) {
                    LOG.info( "Envio Correo de Proceso con archivos no leidos " );
                    LOG.info( detalleArchivosProcesados );
                    
                    detalleArchivosProcesados = "Sin embargo, los siguientes archivos no han sido procesados: " + detalleArchivosProcesados;
                    MailSendSrv
                            .getInstance()
                            .sendMailOk(
                                    MsgExitoMail.EXITO_PROCESS_ONUS_TITLE
                                            .toString(),
                                    MsgExitoMail.EXITO_PROCESS_ONUS_BODY
                                            .toString()
                                            .concat(
                                                    ConstantesUtil.SKIP_LINE
                                                            .toString() )
                                            .concat( detalleArchivosProcesados ) );
                // TODOS LOS ARCHIVOS PROCESADOS
                }else {
                    LOG.info( "Envio Correo de Proceso con todos archivos ONUS leidos " );
                    MailSendSrv.getInstance().sendMailOk(
                            MsgExitoMail.EXITO_PROCESS_ONUS_TITLE.toString(),
                            MsgExitoMail.EXITO_PROCESS_ONUS_BODY.toString() );
                }
                
            }
            else {
                LOG.error( "Parametros de Base de Datos Incompletos para proceso : Procesar OnUs, ó "
                        .concat( MsgErrorProcessFile.ERROR_LIST_CODE_IS_NOT_AVAILABLE
                                .toString() ) );
                MailSendSrv
                        .getInstance()
                        .sendMailOk(
                                MsgErrorMail.ALERTA_RCH_PRTS.toString(),
                                MsgErrorMail.ALERTA_RCH_PRTS_TXT
                                        .toString()
                                        .concat( " ó ," )
                                        .concat(
                                                MsgErrorProcessFile.ERROR_LIST_CODE_IS_NOT_AVAILABLE
                                                        .toString() ) );
            }
        }
        else {
            String asunto = MsgProceso.MSG_PROCESO_DESACTIVADO_ASUNTO
                    .toString().replace( ":?:",
                            PROCESO_ACTUAL.getCodigoProceso() );
            String mensaje = MsgProceso.MSG_PROCESO_DESACTIVADO_BODY.toString()
                    .replace( ":?:", PROCESO_ACTUAL.getCodigoProceso() );
            MailSendSrv.getInstance().sendMail( asunto, mensaje );
        }
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Metodo encargado de enviar un correo informando las
     * transacciones Onus que no pudieron se leìdas.
     * 
     * @param logTrx
     * @since 1.X
     */
    private void enviaCorreoTrxNoLeidas( LogTransaccionOnUsDTO logTrx ) {
        
        if ( logTrx.getDetalleLectura() != null
                && logTrx.getDetalleLectura().length() > 0 ) {
            String motivo = MsgErrorProcessFile.ERROR_NOT_ALL_LOG_TRX_ARE_READER
                    .toString().concat( ", del archivo : " )
                    .concat( logTrx.getFilename() );
            LOG.info( "Hay transacciones que no fueron leidas, se envia correo con el detalle" );
            MailSendSrv.getInstance().sendMailOk( motivo,
                    logTrx.getDetalleLectura() );
            
        }else{
            LOG.info( "Todas las trx fueron leìdas no se envia correo con detalle" );
        }
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param logTrx
     * @throws AppException
     * @since 1.X
     */
    private void saveData( final LogTransaccionOnUsDTO logTrx )
            throws AppException {
        Integer flag = 0;
        TrxOnUsDao procesarDAO = null;
        try {
            LOG.info( "GUARDAR DATOS" );
            procesarDAO = TrxOnUsDaoFactory.getInstance().getNewEntidadDao();
            
            // RECORRE TODAS LAS TRANSACCIONES DEL ON US
            for ( TransaccionOnUsDTO instance : logTrx.getTrxList() ) {
                
                // GUARDA EN TMP_INCOMING, SI HAY ERROR SUMA -1
                if ( !procesarDAO.saveTransaction( instance ) ) {
                    flag = flag - 1;
                }
            }
            // COMMMIT DE TRANSACCIONES SAVE ACUMULADAS
            if ( flag == 0 ) {
                procesarDAO.endTx();
                LOG.info( "COMMIT SAVE TRANSACCIONES" );
                
                LOG.info( "SE LLAMA A SP - CARGA LOG TRX ONUS" );
                // SE LLAMA AL SP CARGA TRX ON US
                procesarDAO.callSPCargaLogTrxOnUs();
                procesarDAO.endTx();
                
            }
            else {
                // SE REALIZA ROLLBACK
                rollBackTransaccion( procesarDAO );
                throw new AppException( MsgErrorSQL.ERROR_MAKE_A_ROLLBAK.toString());
            }
        }
        catch ( SQLException e ) {
            throw new AppException( MsgErrorSQL.ERROR_SAVE_LOGONUS.toString(),
                    e );
        }
        finally {
            try {
                if ( procesarDAO != null ) {
                    procesarDAO.close();
                }
            }
            catch ( SQLException e ) {
                LOG.error( e.getMessage(), e );
            }
            
        }
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Metodo que chequea si existe un registro ingresado en base de
     * datos en la tabla TBL_LOG_ONUS, con el nombre del archivo que
     * se esta intentando leer en el servidor
     * 
     * @param filename
     * @return
     * @throws AppException
     * @since 1.X
     */
    private boolean isValidOnus( String filename ) throws AppException {
        CommonsDao commonsDAO = null;
        try {
            commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
            
            // BUSCA REGISTRO LOG POR NOMBRE
            LOG.info( "Nombre Archivo ONUS: " + filename );
            TblCronLogDTO filter = new TblCronLogDTO();
            filter.setFilename( filename );
            
            TblCronLogDTO cronlog = commonsDAO.getTblCronLogByType( filter,
                    LogBD.TABLE_LOG_ONUS.getTable(),
                    FilterTypeSearch.FILTER_BY_NAME );
            LOG.info( "=====> Valor de TABLE_LOG_ONUS: " + cronlog );
            
            if ( cronlog == null ) {
                LOG.error( MsgErrorProcessFileOnus.ERROR_ONUS_NAME_NOT_FOUND
                        .toString() );
                return Boolean.FALSE;
            }
            // SI ES DISTINTO DE 0 NO ES VÁLIDO
            else
                if ( !StatusProcessType.PROCESS_PENDING.getValue()
                        .equalsIgnoreCase( cronlog.getFileFlag() ) ) {
                    LOG.info( MsgErrorProcessFileOnus.ERROR_INVALID_ONUS_STATE
                            .toString() );
                    return Boolean.FALSE;
                }
            
        }
        catch ( SQLException e ) {
            throw new AppException(
                    MsgErrorSQL.ERROR_FIND_ONUS_BY_NAME.toString(), e );
        }
        finally {
            
            try {
                if ( commonsDAO != null ) {
                    commonsDAO.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException(
                        MsgErrorSQL.ERROR_FIND_ONUS_BY_NAME.toString(), e );
            }
        }
        
        return Boolean.TRUE;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param flag
     * @param filename
     * @return
     * @throws AppException
     * @since 1.X
     */
    private Boolean updateFlag( String flag, String filename )
            throws AppException {
        Boolean process = Boolean.FALSE;
        
        LOG.info( "Se updatea flag del archivo "+filename+" a : "+flag+ ", en la tabla TBL_LOG_ONUS");
        
        // Crea registro a guardar
        TblCronLogDTO tblCronLogDTO = new TblCronLogDTO();
        tblCronLogDTO.setFilename( filename );
        tblCronLogDTO.setFileFlag( flag );
        CommonsDao commonsDAO = null;
        
        try {
            commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
            
            if ( commonsDAO.updateTblCronLogByType( tblCronLogDTO,
                    LogBD.TABLE_LOG_ONUS.getTable(),
                    FilterTypeSearch.FILTER_BY_NAME ) ) {
                commonsDAO.endTx();
                process = Boolean.TRUE;
            }
            else {
                try {
                    rollBackTransaccion( commonsDAO );
                }
                catch ( AppException e ) {
                    throw new AppException(
                            MsgErrorSQL.ERROR_UPDATE_ONUS.toString(), e );
                }
            }
        }
        catch ( SQLException e ) {
            throw new AppException( MsgErrorSQL.ERROR_UPDATE_ONUS.toString(), e );
        }
        finally {
            
            try {
                if ( commonsDAO != null ) {
                    commonsDAO.close();
                }
            }
            catch ( SQLException e ) {
                LOG.error( e.getMessage(), e );
            }
            
        }
        return process;
    }
    
    /**
     * Consulta la lista de codigos de IC desde la tabla
     * TBL_PRTS_IC_CODE
     * 
     * 
     * 
     * 
     * 
     * @throws AppException
     * @return Lista de codigos de ic
     * 
     */
    private List<ICCodeHomologationDTO> loadListIcCodes() throws AppException {
        CommonsDao commonsDAO = null;
        
        try {
            // INICIA EL DAO
            commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
            return commonsDAO.obtenerCodigosIc();
            
        }
        catch ( SQLException e ) {
            throw new AppException( MsgErrorSQL.ERROR_QUERY_PRTS.toString(), e );
        }
        finally {
            try {
                if ( commonsDAO != null ) {
                    commonsDAO.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException(
                        MsgErrorSQL.ERROR_QUERY_PRTS_CLOSE.toString(), e );
            }
        }
        
    }
    
    /**
     * 
     * 
     * 
     * 
     * Se obtienen los parametros desde la base de datos para el
     * proceso
     * 
     * 
     * 
     * 
     * @return
     * @throws AppException
     */
    private ParamsProcessOnusDTO getParametros() throws AppException {
        ParamsProcessOnusDTO parametros = null;
        List<ParametroDTO> paramDTOList = null;
        CommonsDao commonsDAO = null;
        
        try {
            // INICIA EL DAO
            commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
            // SE OBTIENEN TODOS LOS CODIGOS
            paramDTOList = commonsDAO
                    .getParametroCodGrupoDato( ConstantesPRTS.CRON_TBK
                            .toString() );
            LOG.info( "LISTA DE PARAMETROS : " + paramDTOList.size() );
            
            // SI EXISTEN PARAMETROS EN BD SE EXTRAEN
            if ( paramDTOList != null ) {
                
                // PARAMETROS DE COMISIONES
                parametros = new ParamsProcessOnusDTO();
                
                // FORMATO DEL NOMBRE DEL ARCHIVO ON US
                parametros.setFormatFileNameOnUs( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.FORMAT_FILENAME_ONUS.toString() ) );
                
                // FORMATO DE LA EXTENSION DEL ARCHIVO ON US
                parametros.setFormatExtNameOnUs( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.FORMAT_EXTNAME_ONUS.toString() ) );
                
                // RUTA SERVIDOR DONDE SE LEERA EL ARCHIVO ON US PARA
                // PROCESAR
                parametros
                        .setPathAbcOnUs( CommonsUtils.getCodiDato(
                                paramDTOList,
                                ConstantesPRTS.PATH_ABC_ONUS.toString() ) );
                
                // RUTA SERVIDOR DONDE SE DEJARA EL ARCHIVO ON US
                // PROCESADOS BKP
                parametros.setPathAbcOnUsBkp( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.PATH_ABC_ONUS_BKP.toString() ) );
                
                // RUTA SERVIDOR DONDE SE DEJARA EL ARCHIVO ON US
                // PROCESADO CON
                // ERROR
                parametros.setPathAbcOnUsError( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.PATH_ABC_ONUS_ERROR.toString() ) );
            }
            LOG.info( "Parametros  BD : " + parametros );
            return parametros;
        }
        catch ( SQLException e ) {
            throw new AppException( MsgErrorSQL.ERROR_QUERY_PRTS.toString(), e );
        }
        finally {
            try {
                if ( commonsDAO != null ) {
                    commonsDAO.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException(
                        MsgErrorSQL.ERROR_QUERY_PRTS_CLOSE.toString(), e );
            }
        }
    }
    
}
