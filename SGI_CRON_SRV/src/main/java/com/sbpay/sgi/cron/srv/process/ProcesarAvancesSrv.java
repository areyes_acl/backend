package com.sbpay.sgi.cron.srv.process;

import java.io.File;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dao.TrxAvancesDaoFactory;
import com.sbpay.sgi.cron.dao.TrxAvancesDao;
import com.sbpay.sgi.cron.dto.ICCodeHomologationDTO;
import com.sbpay.sgi.cron.dto.LogTransaccionAvancesDTO;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.dto.ParamsProcessAvancesDTO;
import com.sbpay.sgi.cron.dto.TblCronLogDTO;
import com.sbpay.sgi.cron.dto.TransaccionAvancesDTO;
import com.sbpay.sgi.cron.enums.ConstantesPRTS;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.FilterTypeSearch;
import com.sbpay.sgi.cron.enums.LogBD;
import com.sbpay.sgi.cron.enums.MsgErrorMail;
import com.sbpay.sgi.cron.enums.MsgErrorProcessFile;
import com.sbpay.sgi.cron.enums.MsgErrorProcessFileAvances;
import com.sbpay.sgi.cron.enums.MsgErrorSQL;
import com.sbpay.sgi.cron.enums.MsgExitoMail;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.enums.StatusProcessType;
import com.sbpay.sgi.cron.srv.download.DownLoadAvancesSrv;
import com.sbpay.sgi.cron.srv.utils.CommonSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;
import com.sbpay.sgi.cron.utils.file.TransaccionAvancesFileUtil;
import com.sbpay.sgi.cron.utils.file.TransaccionAvancesReader;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;
import com.sbpay.sgi.cron.utils.generals.DateUtils;
import com.sbpay.sgi.cron.validation.ProcessValidator;
import com.sbpay.sgi.cron.validation.impl.ProcessValidatorImpl;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 xx/10/2018, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * Este proceso se encarga de leer el archivo  y cargar las transacciones
 * informadas en el Sistema, la lectura se realiza al buscar en una ruta local
 * del servidor todos los archivos que posean el prefijo y definida por la tabla
 * de parametros TBL_PRTS a continuación se valida en la tabla
 * TBL_LOG_AVANCES que el archivo no este procesado. Este proceso carga una
 * tabla temporal TMP_TRANSAC_AVANCES actualiza la TBL_LOG_AVANCES y ejecuta
 * un SP SP_SIG_CARGA_LOG_TRX_AVANCES(PEDIENTE) para mover a BKP el archivo
 * procesado.
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */

public final class ProcesarAvancesSrv extends CommonSrv {
	/** The Constant LOGGER. */
	private static final Logger LOG = Logger.getLogger(ProcesarAvancesSrv.class
			.getName());
	/** The single instance. */
	private static ProcesarAvancesSrv singleINSTANCE = null;

	private final ProcessCodeType PROCESO_ACTUAL = ProcessCodeType.PROCESAR_AVANCES;

	/**
	 * Creates the instance.
	 */
	private static void createInstance() {
		synchronized (ProcesarAvancesSrv.class) {
			if (singleINSTANCE == null) {
				singleINSTANCE = new ProcesarAvancesSrv();
			}
		}
	}

	/**
	 * Patron singleton.
	 * 
	 * @return ProcesarAvancesSrv retorna instancia del servicio.
	 */
	public static ProcesarAvancesSrv getInstance() {
		if (singleINSTANCE == null) {
			createInstance();
		}
		return singleINSTANCE;
	}

	/**
	 * Metodo de restricion de sobreescritura de la clase.
	 */
	@Override
	protected Object clone() throws CloneNotSupportedException {
		super.clone();
		throw new CloneNotSupportedException();
	}

	/**
	 * Constructor Privado.
	 */
	private ProcesarAvancesSrv() {
		super();
	}

	public void processAvances() throws AppException {
		String detalleArchivosProcesados = "";

		ProcessValidator validadorProceso = new ProcessValidatorImpl();

		if (validadorProceso.isActiveProcess(PROCESO_ACTUAL)) {
			// BUSCAR LOS PARAMETROS NECESARIOS PARA EL FILE
			ParamsProcessAvancesDTO parametros = getParametros();
			List<File> listOfFiles = null;
			List<ICCodeHomologationDTO> listaCodigosIc = loadListIcCodes();

			if (listaCodigosIc != null
					&& listaCodigosIc.size() > 0
					&& validadorProceso.hasTheValidParametersForTheProcess(
							PROCESO_ACTUAL, parametros)) {

				LOG.info("==> SE HAN ENCONTRADO LOS PARAMETROS NECESARIOS PARA EL PROCESO, SE DA INICIO AL PROCESAMIENTO DE AVANCES...=== ");

				String pathFolder = parametros.getPathAbcAvances();
				String starWith = parametros.getFormatFileNameAvances();

				int countFileProcessed = 0;

				listOfFiles = CommonsUtils.getFilesInFolderAvances(pathFolder,
						starWith);
				
				LOG.info("Nombre de archivo a procesar "+ parametros.getFormatFileNameAvances());


				LOG.debug("Lista de archivos :  "
						+ ((listOfFiles != null) ? "" + listOfFiles.size()
								: "No se encontraron archivos"));

				// No hay archivos
				if (listOfFiles == null) {
					LOG.warn(MsgErrorProcessFileAvances.WARNING_NO_FILES_FOUNDS
							.toString());
					return;
				}

				for (File file : listOfFiles) {
					if (file.isFile()
							&& file.getName().substring(8).startsWith(
									parametros.getFormatFileNameAvances().substring(8))
							&& isValidAvances(file.getName())) {

						try {
							
							LOG.info("Archivo  : " + file.getName()
									+ "  válido , Se comienza a procesar...");

							// 1- PROCESA ARCHIVO AVANCES
							TransaccionAvancesFileUtil reader = new TransaccionAvancesReader();
							LogTransaccionAvancesDTO logTrx = reader
									.readLogTrxAvancesFile(file, starWith,
											listaCodigosIc);
							logTrx.setFilename(file.getName());

							// LOG.info("logTrx3: "+logTrx);
							// INFORMA REGISTROS NO SE LEYERON
							// enviaCorreoTrxNoLeidas( logTrx );

							// 2 - SE GUARDAN LAS TRX AVANCES AVANCES EN BD
							saveData(logTrx);
							// 3 -- UPDATEA LOG A PROCESADO OK
							updateFlag(
									StatusProcessType.PROCESS_SUCESSFUL
											.getValue(),
									logTrx.getFilename());

							// 4 - SE MUEVE Avances AVANCES A LA CARPETA
							// BACKUP
							CommonsUtils.moveFile(
									file.getAbsolutePath(),
									parametros.getPathAbcAvancesBkp()
											+ file.getName());
							countFileProcessed++;

						} catch (AppException e) {
							// ERROR AL PROCESAR AVANCES AVANCES SE ENVIA EMAIL
							LOG.error(
									MsgErrorProcessFileAvances.ERROR_IN_PROCESS
											.toString().concat(":")
											.concat(file.getName()), e);

							// UPDATEA REGISTRO A PROCESADO CON ERROR
							updateFlag(
									StatusProcessType.PROCESS_ERROR.getValue(),
									file.getName());

							detalleArchivosProcesados = detalleArchivosProcesados
									.concat("\n").concat(file.getName());

							// MUEVE ARCHIVO A LA CARPETA DE ERROR
							CommonsUtils.moveFile(
									file.getAbsolutePath(),
									parametros.getPathAbcAvancesError()
											+ file.getName());

						}

					}
				}
				// SI NO SE PROCESA NINGUN FILE ESCRIBE ESCRIBE LOG
				if (countFileProcessed == 0) {
					LOG.warn(MsgErrorProcessFileAvances.WARNING_NO_FILE_PROCESSING_TITLE
							+ "  - "
							+ MsgErrorProcessFileAvances.WARNING_NO_FILE_PROCESSING_CAUSE);
					MailSendSrv
							.getInstance()
							.sendMailOk(
									MsgErrorProcessFileAvances.WARNING_NO_FILE_PROCESSING_TITLE
											.toString(),
									MsgErrorProcessFileAvances.WARNING_NO_FILE_PROCESSING_CAUSE
											.toString());

					// NO TODOS LOS ARCHIVOS AVANCES AVANCES PROCESADOS
				} else if (countFileProcessed > 0
						&& countFileProcessed != listOfFiles.size()) {
					LOG.info("Envio Correo de Proceso con archivos no leidos ");
					LOG.info(detalleArchivosProcesados);

					detalleArchivosProcesados = "Sin embargo, los siguientes archivos no han sido procesados: "
							+ detalleArchivosProcesados;
					MailSendSrv
							.getInstance()
							.sendMailOk(
									MsgExitoMail.EXITO_PROCESS_AVANCES_TITLE
											.toString(),
									MsgExitoMail.EXITO_PROCESS_AVANCES_BODY
											.toString()
											.concat(ConstantesUtil.SKIP_LINE
													.toString())
											.concat(detalleArchivosProcesados));
					// TODOS LOS ARCHIVOS PROCESADOS
				} else {
					LOG.info("Envio Correo de Proceso con todos archivos avances Avances leidos ");
					MailSendSrv.getInstance().sendMailOk(
							MsgExitoMail.EXITO_PROCESS_AVANCES_TITLE.toString(),
							MsgExitoMail.EXITO_PROCESS_AVANCES_BODY.toString());
				}

			} else {
				LOG.error("Parametros de Base de Datos Incompletos para proceso : Procesar avances Avances, ó "
						.concat(MsgErrorProcessFile.ERROR_LIST_CODE_IS_NOT_AVAILABLE_AVANCES
								.toString()));
				MailSendSrv
						.getInstance()
						.sendMailOk(
								MsgErrorMail.ALERTA_RCH_PRTS_AVA.toString(),
								MsgErrorMail.ALERTA_RCH_PRTS_TXT_AVA
										.toString()
										.concat(" ó ,")
										.concat(MsgErrorProcessFile.ERROR_LIST_CODE_IS_NOT_AVAILABLE_AVANCES
												.toString()));
			}
		} else {
			String asunto = MsgProceso.MSG_PROCESO_DESACTIVADO_ASUNTO
					.toString().replace(":?:",
							PROCESO_ACTUAL.getCodigoProceso());
			String mensaje = MsgProceso.MSG_PROCESO_DESACTIVADO_BODY.toString()
					.replace(":?:", PROCESO_ACTUAL.getCodigoProceso());
			MailSendSrv.getInstance().sendMail(asunto, mensaje);
		}

	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/10/2018, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @param logTrx
	 * @throws AppException
	 * @since 1.X
	 */
	private void saveData(final LogTransaccionAvancesDTO logTrx)
			throws AppException {
		Integer flag = 0;
		TrxAvancesDao procesarDAO = null;
		try {
			LOG.info("GUARDAR DATOS");
			procesarDAO = TrxAvancesDaoFactory.getInstance().getNewEntidadDao();

			// RECORRE TODAS LAS TRANSACCIONES DEL AVANCES 
			for (TransaccionAvancesDTO instance : logTrx.getTrxList()) {

				// GUARDA EN TMP_INCOMING, SI HAY ERROR SUMA -1
				if (!procesarDAO.saveTransaction(instance)) {
					flag = flag - 1;
				}
			}
			// COMMMIT DE TRANSACCIONES SAVE ACUMULADAS
			if (flag == 0) {
				procesarDAO.endTx();
				LOG.info("COMMIT SAVE TRANSACCIONES");

				LOG.info("SE LLAMA A SP - CARGA LOG TRX AVANCES");
				// SE LLAMA AL SP CARGA TRX AVANCES AVANCES
				procesarDAO.callSPCargaLogTrxAvances();
				procesarDAO.endTx();

			} else {
				// SE REALIZA ROLLBACK
				rollBackTransaccion(procesarDAO);
				throw new AppException(
						MsgErrorSQL.ERROR_MAKE_A_ROLLBAK.toString());
			}
		} catch (SQLException e) {
			LOG.info(e.getMessage());
			throw new AppException(MsgErrorSQL.ERROR_SAVE_LOGAVANCES.toString(), e);
		} finally {
			try {
				if (procesarDAO != null) {
					procesarDAO.close();
				}
			} catch (SQLException e) {
				LOG.error(e.getMessage(), e);
			}

		}

	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/10/2018, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @param flag
	 * @param filename
	 * @return
	 * @throws AppException
	 * @since 1.X
	 */
	private Boolean updateFlag(String flag, String filename)
			throws AppException {
		Boolean process = Boolean.FALSE;

		LOG.info("Se updatea flag del archivo " + filename + " a : " + flag
				+ ", en la tabla TBL_LOG_AVANCES");

		// Crea registro a guardar
		TblCronLogDTO tblCronLogDTO = new TblCronLogDTO();
		tblCronLogDTO.setFilename(filename);
		tblCronLogDTO.setFileFlag(flag);
		CommonsDao commonsDAO = null;

		try {
			commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();

			if (commonsDAO.updateTblCronLogByType(tblCronLogDTO,
					LogBD.TABLE_LOG_AVANCES.getTable(),
					FilterTypeSearch.FILTER_BY_NAME)) {
				commonsDAO.endTx();
				process = Boolean.TRUE;
			} else {
				try {
					rollBackTransaccion(commonsDAO);
				} catch (AppException e) {
					throw new AppException(
							MsgErrorSQL.ERROR_UPDATE_AVANCES.toString(), e);
				}
			}
		} catch (SQLException e) {
			throw new AppException(MsgErrorSQL.ERROR_UPDATE_AVANCES.toString(), e);
		} finally {

			try {
				if (commonsDAO != null) {
					commonsDAO.close();
				}
			} catch (SQLException e) {
				LOG.error(e.getMessage(), e);
			}

		}
		return process;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/10/2018, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que chequea si existe un registro ingresado en base de datos en la
	 * tabla TBL_LOG_AVANCES, con el nombre del archivo que se esta intentando leer
	 * en el servidor
	 * 
	 * @param filename
	 * @return
	 * @throws AppException
	 * @since 1.X
	 */
	private boolean isValidAvances(String filename) throws AppException {
		CommonsDao commonsDAO = null;
		try {
			commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();

			// BUSCA REGISTRO LOG POR NOMBRE
			LOG.info("Nombre Archivo : " + filename);
			TblCronLogDTO filter = new TblCronLogDTO();
			filter.setFilename(filename);

			TblCronLogDTO cronlog = commonsDAO.getTblCronLogByType(filter,
					LogBD.TABLE_LOG_AVANCES.getTable(),
					FilterTypeSearch.FILTER_BY_NAME);
			LOG.info("=====> Valor de TABLE_LOG_AVANCES: " + cronlog);

			if (cronlog == null) {
				//
				DownLoadAvancesSrv logAvance = new DownLoadAvancesSrv();
				logAvance.save(filename, "");
			}
			// SI ES DISTINTO DE 0 NO ES VÁLIDO
			else if (!StatusProcessType.PROCESS_PENDING.getValue()
					.equalsIgnoreCase(cronlog.getFileFlag())) {
				LOG.info(MsgErrorProcessFileAvances.ERROR_INVALID_AVANCES_STATE
						.toString());
				return Boolean.FALSE;
			}

		} catch (SQLException e) {
			throw new AppException(
					MsgErrorSQL.ERROR_FIND_AVANCES_BY_NAME.toString(), e);
		} finally {

			try {
				if (commonsDAO != null) {
					commonsDAO.close();
				}
			} catch (SQLException e) {
				throw new AppException(
						MsgErrorSQL.ERROR_FIND_AVANCES_BY_NAME.toString(), e);
			}
		}

		return Boolean.TRUE;
	}

	/**
	 * Consulta la lista de codigos de IC desde la tabla TBL_PRTS_IC_CODE
	 * 
	 * 
	 * 
	 * 
	 * 
	 * @throws AppException
	 * @return Lista de codigos de ic
	 * 
	 */
	private List<ICCodeHomologationDTO> loadListIcCodes() throws AppException {
		CommonsDao commonsDAO = null;

		try {
			// INICIA EL DAO
			commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
			return commonsDAO.obtenerCodigosIc();

		} catch (SQLException e) {
			throw new AppException(MsgErrorSQL.ERROR_QUERY_PRTS.toString(), e);
		} finally {
			try {
				if (commonsDAO != null) {
					commonsDAO.close();
				}
			} catch (SQLException e) {
				throw new AppException(
						MsgErrorSQL.ERROR_QUERY_PRTS_CLOSE.toString(), e);
			}
		}

	}

	/**
	 * 
	 * 
	 * 
	 * 
	 * Se obtienen los parametros desde la base de datos para el proceso
	 * 
	 * 
	 * 
	 * 
	 * @return
	 * @throws AppException
	 */
	private ParamsProcessAvancesDTO getParametros() throws AppException {
		ParamsProcessAvancesDTO parametros = null;
		List<ParametroDTO> paramDTOList = null;
		CommonsDao commonsDAO = null;
		String formatoFile = "";

		try {
			// INICIA EL DAO
			commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
			// SE OBTIENEN TODOS LOS CODIGOS
			paramDTOList = commonsDAO
					.getParametroCodGrupoDato(ConstantesPRTS.CRON_TBK
							.toString());
			LOG.info("LISTA DE PARAMETROS : " + paramDTOList.size());
			// SI EXISTEN PARAMETROS EN BD SE EXTRAEN
			if (paramDTOList != null) {

				// PARAMETROS DE COMISIONES
				parametros = new ParamsProcessAvancesDTO();

				formatoFile = DateUtils
						.getDateTodayInYYYYMMDDAnterior()
						.concat(ConstantesUtil.UNDERSCORE.toString())
						.concat(CommonsUtils
								.getCodiDato(
										paramDTOList,
										ConstantesPRTS.FORMAT_FILENAME_CORRELATIVO_AVANCES
												.toString()))
						.concat(ConstantesUtil.UNDERSCORE.toString())
						.concat(CommonsUtils.getCodiDato(paramDTOList,
								ConstantesPRTS.FORMAT_FILENAME_AVANCES
										.toString()));

				// FORMATO DEL NOMBRE DEL ARCHIVO AVANCES
				parametros.setFormatFileNameAvances(formatoFile);

				// RUTA SERVIDOR DONDE SE LEERA EL ARCHIVO AVANCES PARA
				// PROCESAR
				parametros.setPathAbcAvances(CommonsUtils.getCodiDato(
						paramDTOList,
						ConstantesPRTS.PATH_ACL_AVANCES.toString()));

				// RUTA SERVIDOR DONDE SE DEJARA EL ARCHIVO AVANCES
				// PROCESADOS BKP
				parametros.setPathAbcAvancesBkp(CommonsUtils.getCodiDato(
						paramDTOList,
						ConstantesPRTS.PATH_ABC_AVANCES_BKP.toString()));

				// RUTA SERVIDOR DONDE SE DEJARA EL ARCHIVO 
				// PROCESADO CON
				// ERROR
				parametros.setPathAbcAvancesError(CommonsUtils.getCodiDato(
						paramDTOList,
						ConstantesPRTS.PATH_ABC_AVANCES_ERROR.toString()));
			}

			LOG.info("Parametros  BD : " + parametros);
			return parametros;
		} catch (SQLException e) {
			throw new AppException(MsgErrorSQL.ERROR_QUERY_PRTS.toString(), e);
		} finally {
			try {
				if (commonsDAO != null) {
					commonsDAO.close();
				}
			} catch (SQLException e) {
				throw new AppException(
						MsgErrorSQL.ERROR_QUERY_PRTS_CLOSE.toString(), e);
			}
		}
	}

}
