package com.sbpay.sgi.cron.srv.download;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dto.ConfigFile;
import com.sbpay.sgi.cron.dto.DataFTP;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.dto.ParamsDownloadRechazosDTO;
import com.sbpay.sgi.cron.dto.TblCronLogDTO;
import com.sbpay.sgi.cron.enums.ConstantesPRTS;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.LogBD;
import com.sbpay.sgi.cron.enums.MsgDownloadRchProcess;
import com.sbpay.sgi.cron.enums.MsgErrorMail;
import com.sbpay.sgi.cron.enums.MsgErrorSQL;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.enums.StatusProcessType;
import com.sbpay.sgi.cron.srv.utils.ClientFTPSrv;
import com.sbpay.sgi.cron.srv.utils.CommonSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;
import com.sbpay.sgi.cron.utils.generals.DateUtils;
import com.sbpay.sgi.cron.validation.ProcessValidator;
import com.sbpay.sgi.cron.validation.impl.ProcessValidatorImpl;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 18/12/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * Este proceso se encarga de descargar el archivo de Rechazos desde IC, partiendo 
 * con los que comiencen un prefijo, posteriormente valida en la BD TBL_LOG_RCH que 
 * no este descargado y lo descarga si no esta pendiente. 
 * (no existe archivo de control en este proceso)	
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class DownloadRechazosSrv extends CommonSrv {
    
    /** VARIABLE PARA EL LOGER */
    private static final Logger LOGGER = Logger
            .getLogger( DownloadRechazosSrv.class );

    /** PROCESO **/
    private final ProcessCodeType PROCESO_ACTUAL = ProcessCodeType.DESCARGAR_RECHAZOS;
    
    /** The single instance. */
    private static DownloadRechazosSrv singleINSTANCE = null;
    
    /**
     * Creates the instance.
     */
    private static void createInstance() {
        synchronized ( DownloadRechazosSrv.class ) {
            if ( singleINSTANCE == null ) {
                singleINSTANCE = new DownloadRechazosSrv();
            }
        }
    }
    
    /**
     * Patron singleton.
     * 
     * @return ActivarPrepagoSrv retorna instancia del servicio.
     */
    public static DownloadRechazosSrv getInstance() {
        if ( singleINSTANCE == null ) {
            createInstance();
        }
        return singleINSTANCE;
    }
    
    /**
     * Metodo de restricion de sobreescritura de la clase.
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        super.clone();
        throw new CloneNotSupportedException();
    }
    
    /**
     * Constructor Privado.
     * 
     * @throws SQLException
     */
    private DownloadRechazosSrv() {
        super();
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 18/12/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @throws AppException
     * @since 1.X
     */
	public Boolean descargarRechazos() throws AppException {
		Boolean flag = Boolean.FALSE;
		List<String> listFTP = null;
		List<String> listToDownload = null;
		int countDownloadProcess = 0;
		
		ProcessValidator validadorProceso = new ProcessValidatorImpl();

	    if (validadorProceso.isActiveProcess(PROCESO_ACTUAL)) {
	        
	        // BUSCAR LOS PARAMETROS NECESARIOS PARA EL FILE
	        ParamsDownloadRechazosDTO parametros = getParametros();

	        if (validadorProceso.hasTheValidParametersForTheProcess(PROCESO_ACTUAL, parametros)) {
			 LOGGER.info("===> SE HAN ENCONTRADO LOS PARAMETROS PARA EJECUTAR EL CRON, COMIENZA EL PROCESO...");

			// OBTENER NOMBRES LISTA DE ARCHIVOS EN FTP
			listFTP = ClientFTPSrv.getInstance().listFileSFTP(parametros.getDataFTP(), parametros.getPathFtpIcSalida());

			// VALIDAR FORMATOS NOMBRES ARCHIVOS
			ConfigFile configFile = new ConfigFile();
			configFile.setStarWith(parametros.getFormatFileAbcRch());
			configFile.setEndsWith(parametros.getFormatExtNameRch());
			listToDownload = retrieveFilenameListToDownload(listFTP, configFile);
			
			// SI EXISTE ALGUN ARCHIVO VALIDO CONTINUA PROCESO
			if (listToDownload != null && listToDownload.size() > 0) {

				// FILTRAR ARCHIVOS QUE NO HAN SIDO DESCARGADOS ANTERIORMENTE
				listToDownload = currentlyNotDownloadedFiles(listToDownload,parametros.getFormatFileAbcRch(),LogBD.TABLE_LOG_RECHAZO);
				
				// VALIDACION QUE EXISTA ALGUNO
				if (listToDownload != null && listToDownload.size() > 0) {
					for (String filename : listToDownload) {
						try {
							// DESCARGAR
							String pathFtpOri = parametros.getPathFtpIcSalida();
							String pathDest = parametros.getPathAbcRch();
							
							LOGGER.info( "Descargando archivo :"+filename+", desde ftp : "+ parametros.getDataFTP().getIpHost() );
							ClientFTPSrv.getInstance().downloadFileSFTP(
									parametros.getDataFTP(), pathFtpOri,
									pathDest, filename);

							// INSERTA EN LA TBL_LOG_RCH_INC
							save(filename, parametros.getFormatFileAbcRch());

							// CUENTA LOS REGISTROS INSERTADOS
							countDownloadProcess++;
						} catch (AppException e) {
							LOGGER.error(
									"Ha ocurrido un error al intentar descargar el archivo : "
											+ filename, e);
						}

					}
				} else {
					MailSendSrv
							.getInstance()
							.sendMailOk(
									MsgDownloadRchProcess.PROCESS_FINISH_WITH_WARN
											.toString(),
									MsgDownloadRchProcess.ALL_FILES_ALREADY_PROCESSED
											.toString());
					LOGGER.info(MsgDownloadRchProcess.ALL_FILES_ALREADY_PROCESSED
							.toString());
				}
			} else {
				MailSendSrv
						.getInstance()
						.sendMailOk(
								MsgDownloadRchProcess.PROCESS_FINISH_WITH_WARN
										.toString(),
								MsgDownloadRchProcess.ALL_FILES_HAS_NOT_VALID_FORMAT
										.toString());
				LOGGER.info(MsgDownloadRchProcess.ALL_FILES_HAS_NOT_VALID_FORMAT
						.toString());
			}

			// SE SUBIERON CORRECTAMENTE TODOS LOS REGISTROS
			if (listToDownload != null
					&& countDownloadProcess == listToDownload.size() && listToDownload.size() > 0 ) {
				flag = Boolean.TRUE;
				MailSendSrv
						.getInstance()
						.sendMailOk(
								MsgDownloadRchProcess.PROCESS_FINISH_OK_TITLE
										.toString(),
								MsgDownloadRchProcess.PROCESS_FINISH_OK_BODY
										.toString());
			}
			// no se encontraron los parametros para el proceso del cron
		}else{
		    LOGGER.warn(MsgProceso.MSG_ERROR_FALTAN_PARAMETROS.toString());
				MailSendSrv.getInstance().sendMail(
						MsgErrorMail.ALERTA_PRTS.toString(),
						MsgErrorMail.ALERTA_PRTS_TXT.toString());
		}

		return flag;
		
	    }else{
	        String asunto = MsgProceso.MSG_PROCESO_DESACTIVADO_ASUNTO
                    .toString().replace( ":?:",
                            PROCESO_ACTUAL.getCodigoProceso() );
            String mensaje = MsgProceso.MSG_PROCESO_DESACTIVADO_BODY.toString()
                    .replace( ":?:", PROCESO_ACTUAL.getCodigoProceso() );
            MailSendSrv.getInstance().sendMail( asunto, mensaje );
	    }
        return flag;
	}
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 21/12/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     *  Metodo encargado de insertar un registro del rechazo descargado desde IC, 
     *  en la tabla TBL_LOG_RCH
     * 
     * 
     * @param filename
     * @param starWith
     * @throws AppException
     * @throws SQLException
     * @since 1.X
     */
    private void save( String filename, String starWith ) throws AppException {
        // INICIALIZA EL DAO
        CommonsDao commonsDAO = null;
        try {
            commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
            TblCronLogDTO cronLog = new TblCronLogDTO();
            cronLog.setFecha( DateUtils.getDateFileTbk( DateUtils
                    .getDateStringFromFilename( filename, starWith ) ) );
            cronLog.setFilename( filename );
            cronLog.setFileFlag( StatusProcessType.PROCESS_PENDING.getValue() );
            
            if ( commonsDAO.saveTblCronLog( cronLog, LogBD.TABLE_LOG_RECHAZO ) ) {
                commonsDAO.endTx();
            }
            else {
                commonsDAO.rollBack();
            }
        }
        catch ( SQLException e ) {
            
            throw new AppException(
                    MsgDownloadRchProcess.ERROR_SAVE_LOG.toString(), e );
        }
        finally {
            try {
                if ( commonsDAO != null ) {
                    commonsDAO.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException(
                		MsgDownloadRchProcess.ERROR_SAVE_LOG.toString(), e );
            }
        }
        
    }
    
    
    
    
        
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 18/12/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @throws AppException
     * @since 1.X
     */
    private ParamsDownloadRechazosDTO getParametros() throws AppException {
        ParamsDownloadRechazosDTO parametros = null;
        List<ParametroDTO> paramDTOList = null;
        CommonsDao commonsDAO = null;
        DataFTP dataFTP = null;
        
        try {
            // INICIA EL DAO
            commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
            // SE OBTIENEN TODOS LOS CODIGOS
            paramDTOList = commonsDAO
                    .getParametroCodGrupoDato( ConstantesPRTS.CRON_TBK
                            .toString() );
            
            // SI EXISTEN PARAMETROS EN BD SE EXTRAEN
            if ( paramDTOList != null ) {
                // PARAMETROS DE FTP
                dataFTP = new DataFTP();
                dataFTP.setIpHost( CommonsUtils.getCodiDato( paramDTOList,
                        ConstantesPRTS.SFTP_IP_DOWNLOAD_REJECTED.toString() ) );
                String port = CommonsUtils.getCodiDato(paramDTOList, ConstantesPRTS.SFTP_PORT_DOWNLOAD_REJECTED.toString()) ;
                dataFTP.setPort( Integer.parseInt((port != null && port.matches(ConstantesUtil.PATTERN_IS_NUMBER.toString()) ? port : ConstantesUtil.DEFAULT_PORT.toString())));
                dataFTP.setUser( CommonsUtils.getCodiDato( paramDTOList,
                        ConstantesPRTS.SFTP_USER_DOWNLOAD_REJECTED.toString() ) );
                dataFTP.setPassword( CommonsUtils.getCodiDato( paramDTOList,
                        ConstantesPRTS.SFTP_PASS_DOWNLOAD_REJECTED.toString() ) );
                
                // PARAMETROS DE RECHAZO
                parametros = new ParamsDownloadRechazosDTO();
				// FORMATO DEL NOMBRE DEL ARCHIVO DE RECHAZO DE ENTRADA DESDE IC
                parametros.setFormatFileAbcRch( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.FORMAT_FILE_ABC_RCH.toString() ) );
                // FORMATO EXTENSION ARCHIVO DE RECHAZO
                parametros.setFormatExtNameRch( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.FORMAT_EXTNAME_RCH.toString() ) );
				// RUTA SERVIDOR DONDE SE DEJARA EL ARCHIVO DE RECHAZOS DESCARGADO (DE ENTRADA)
                parametros.setPathAbcRch( CommonsUtils.getCodiDato(
                        paramDTOList, ConstantesPRTS.PATH_ABC_RCH.toString() ) );
             // RUTA FTP IC DONDE BUSCA ARCHIVOS A DESCARGAR (SALIDA)
                parametros.setPathFtpIcSalida( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.PATH_OUT_DWNLD_REJECTED.toString() ) );
                parametros.setDataFTP( dataFTP );
                LOGGER.info(parametros);
                
            }
            return parametros;
        }
        catch ( SQLException e ) {
            throw new AppException( MsgErrorSQL.ERROR_QUERY_PRTS.toString(), e );
        }
        finally {
            try {
                if ( commonsDAO != null ) {
                    commonsDAO.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException(
                        MsgErrorSQL.ERROR_QUERY_PRTS_CLOSE.toString(), e );
            }
        }
    }
    
}
