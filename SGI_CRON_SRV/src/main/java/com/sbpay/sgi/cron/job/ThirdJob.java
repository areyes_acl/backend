package com.sbpay.sgi.cron.job;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;

import com.sbpay.sgi.cron.step.StepCorreoVencimiento;
import com.sbpay.sgi.cron.step.StepGenerarCargoAbono;
import com.sbpay.sgi.cron.step.StepGenerarIncomingOnUs;
import com.sbpay.sgi.cron.step.StepGenerateIncoming;
import com.sbpay.sgi.cron.step.StepGenerateIncomingVisa;
import com.sbpay.sgi.cron.step.StepGenerateIncomingVisaInt;
import com.sbpay.sgi.cron.step.StepUploadCargoAbono;
import com.sbpay.sgi.cron.step.StepUploadIncoming;
import com.sbpay.sgi.cron.step.StepUploadIncomingVisa;
import com.sbpay.sgi.cron.step.StepUploadIncomingVisaInt;
import com.sbpay.sgi.cron.step.StepUploadOnusTbk;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 7/1/2016, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class ThirdJob implements Job {

	/** LOGGER CONSTANT **/
	private static final Logger LOG = Logger.getLogger(SecondJob.class);

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 7/1/2016, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @param context
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
	 * @since 1.X
	 */
	@Override
	public void execute(JobExecutionContext context) {

		LOG.info("========== EJECUCION DEL TERCER JOB =============");
		generarIncoming();
		generarIncomingVisa();
		//generarIncomingOnUs();
		generarIncomingVisaInt();
		generarSolicitud60();
		subirIncoming();
		//subirOnusTbk();
		subirIncomingVisa();
		subirIncomingVisaInt();
		subirSolicitud60();
		generarCorreoVencTransac();
		LOG.info("========== FIN EJECUCION DEL TERCER JOB =============");

	}
	
	
	
	public void subirIncomingVisaInt() {
		try {
			// LLAMA A LA TERCERA TAREA
			StepUploadIncomingVisaInt upIncoming = new StepUploadIncomingVisaInt();
			upIncoming.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

	}
	
	
	public void subirIncomingVisa() {
		try {
			// LLAMA A LA TERCERA TAREA
			StepUploadIncomingVisa upIncoming = new StepUploadIncomingVisa();
			upIncoming.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

	}
	


	//Visa Nacional
	public void generarIncomingVisa() {
		try {
			// LLAMA A LA PRIMERA TAREA
			StepGenerateIncomingVisa genIncoming = new StepGenerateIncomingVisa();
			genIncoming.execute();

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

	}
	
	
	//Visa Internacional
	public void generarIncomingVisaInt() {
		try {
			// LLAMA A LA PRIMERA TAREA
			StepGenerateIncomingVisaInt genIncoming = new StepGenerateIncomingVisaInt();
			genIncoming.execute();

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

	}
	
	
	public void generarIncomingOnUs() {
		try {
			// LLAMA A LA TAREA GENERAR INCOMING ON US
			StepGenerarIncomingOnUs incomingOnUs = new StepGenerarIncomingOnUs();
			incomingOnUs.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

	}
	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 7/1/2016, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @throws Exception
	 * @since 1.X
	 */
	public void subirSolicitud60() {

		try {
			// LLAMA A LA TERCERA TAREA
			StepUploadCargoAbono upCargAbon = new StepUploadCargoAbono();
			upCargAbon.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}

	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 7/1/2016, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @throws Exception
	 * @since 1.X
	 */
	public void subirIncoming() {

		try {
			// LLAMA A LA TERCERA TAREA
			StepUploadIncoming upIncoming = new StepUploadIncoming();
			upIncoming.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 7/1/2016, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * Proceso que llama a la tarea de descargar el outgoing que envia transbank
	 * 
	 * @throws Exception
	 * 
	 * @since 1.X
	 */
	public void subirOnusTbk() {

		try {
			StepUploadOnusTbk stepUploadOnusTbk = new StepUploadOnusTbk();
			stepUploadOnusTbk.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

	}
	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 7/1/2016, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @since 1.X
	 */
	public void generarSolicitud60() {

		try {
			// LLAMA A LA SEGUNDA TAREA
			StepGenerarCargoAbono genCargAbon = new StepGenerarCargoAbono();
			genCargAbon.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
	

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 7/1/2016, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @since 1.X
	 */
	public void generarIncoming() {

		try {
			// LLAMA A LA PRIMERA TAREA
			StepGenerateIncoming genIncoming = new StepGenerateIncoming();
			genIncoming.execute();

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
	
	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 22/1/2016, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @since 1.X
	 */
	public void generarCorreoVencTransac() {

		try {
			// LLAMA A LA PRIMERA TAREA
			StepCorreoVencimiento correoVencimiento = new StepCorreoVencimiento();
			correoVencimiento.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
	
	
}
