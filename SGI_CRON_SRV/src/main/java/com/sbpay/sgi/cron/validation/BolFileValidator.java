package com.sbpay.sgi.cron.validation;

import com.sbpay.sgi.cron.commons.AppException;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * Interfaz que realiza la validacion del archivo BOL
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public interface BolFileValidator{

    public void validarArchivoBolADescargar(String nombreArchivo,
	    String starWith) throws AppException;

}
