package com.sbpay.sgi.cron.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import oracle.jdbc.OracleCallableStatement;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.dao.BaseDao;
import com.sbpay.sgi.cron.dto.TransacionAutorizada;
import com.sbpay.sgi.cron.enums.ConstantesBD;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 19/11/2015, (ACL) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SA.</B>
 */
public class TransaccionAutorizadaDaoJdbc extends BaseDao<TransaccionAutorizadaDao> implements
    TransaccionAutorizadaDao {

  /** VARIABLE PARA EL LOGER */
  private static final Logger LOGGER = Logger.getLogger(TransaccionAutorizadaDaoJdbc.class);

  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 19/11/2015, (ACL & sbpay) - versión inicial
   * </ul>
   * <p>
   * 
   * @param connection
   * @since 1.X
   */
  public TransaccionAutorizadaDaoJdbc(Connection connection) {
    super(connection);

  }

  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 19/11/2015, (ACL & sbpay) - versión inicial
   * </ul>
   * <p>
   * 
   * @param connection
   * @since 1.X
   */
  @Override
  public boolean saveTransaction(TransacionAutorizada transaccion) throws SQLException {
    PreparedStatement stmt = null;
    try {
      StringBuilder str =
          new StringBuilder()
              .append("INSERT INTO ")
              .append(ConstantesBD.ESQUEMA.toString())
              .append(".TMP_TRANSAC_AUTOR ")
              .append("(SID,CODIGO_TRANSACCION,NUMERO_TARJETA,FECHA_PROCESO,TIPO_VENTA,NUM_CUOTAS,")
              .append("NUM_MICROFILM,NUM_COMERCIO,FECHA_COMPRA,MONTO_TRANSAC,VALOR_CUOTA,")
              .append(
                  "NOMBRE_COMERCIO,CIUDAD_COMERCIO,RUBRO_COMERCIO,COD_AUTORIZACION,COD_LOCAL,COD_POS,FECHA_HORA_TRX)")
              .append(
                  " values (SEQ_TMP_TRANSAC_AUTOR.nextval , ?, ?,   To_date( ?, 'RR/MM/DD' ), ?, ?, ?, ?,  To_date( ?, 'RR/MM/DD' ), ?, ?, ?, ?, ?, ?, ?, ?, to_date( ?, 'RR/MM/DD HH24:MI:SS' ))");

      stmt = this.getConnection().prepareStatement(str.toString());

      setStringNull(transaccion.getCodigoTransaccion(), 1, stmt);
      setStringNull(transaccion.getNumeroTarjeta(), 2, stmt);
      setStringNull(transaccion.getFechaProceso(), 3, stmt);
      setStringNull(transaccion.getTipoVenta(), 4, stmt);
      setStringNull(transaccion.getNumCuotas(), 5, stmt);
      setStringNull(transaccion.getNumMicrofilm(), 6, stmt);
      setStringNull(transaccion.getNumComercio(), 7, stmt);
      setStringNull(transaccion.getFechaCompra(), 8, stmt);
      setStringNull(transaccion.getMontoTransac(), 9, stmt);
      setStringNull(transaccion.getValorCuota(), 10, stmt);
      setStringNull(transaccion.getNombreComercio(), 11, stmt);
      setStringNull(transaccion.getCiudadComercio(), 12, stmt);
      setStringNull(transaccion.getRubroComercio(), 13, stmt);
      setStringNull(transaccion.getCodAutorizacion(), 14, stmt);
      setStringNull(transaccion.getCodLocal(), 15, stmt);
      setStringNull(transaccion.getCodPos(), 16, stmt);
      setStringNull(transaccion.getFechaHoraTrx(), 17, stmt);      

      return (stmt.executeUpdate() > 0);
    } finally {
      if (stmt != null) {
        stmt.close();
      }
    }
  }


  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
   * </ul>
   * <p>
   * LLAMA AL PACKAGE PARA COPIAR EL CONTENIDO DESDE LA TMP A LA TBL
   * 
   * @return
   * @throws SQLException
   * @see com.sbpay.sgi.cron.dao.TransaccionAutorizadaDao#callSPCargaLogTrxAutor()
   * @since 1.X
   */
  @Override
  public boolean callSPCargaLogTrxAutor() throws SQLException {
    Connection dbConnection = null;
    OracleCallableStatement callableStatement = null;

    try {
      String callSPCargaIncomingSql = "{call SP_SIG_CARGA_LOG_TRX_AUTOR(?,?)}";

      dbConnection = getConnection();
      callableStatement =
          (OracleCallableStatement) dbConnection.prepareCall(callSPCargaIncomingSql);

      callableStatement.registerOutParameter(1, java.sql.Types.VARCHAR);
      callableStatement.registerOutParameter(2, java.sql.Types.NUMERIC);

      callableStatement.executeUpdate();

      String warning = callableStatement.getString(1);
      Integer codigoError = callableStatement.getInt(2);
      LOGGER.info("****** SE EJECUTA STORE PROCEDURE SP_SIG_CARGA_LOG_TRX_AUTOR *****");
      LOGGER.info("WARNING : " + warning);
      LOGGER.info("codigoError : " + codigoError);
      LOGGER.info("*********** STORE PROCEDURE SP_SIG_CARGA_LOG_TRX_AUTOR *********** ");

    } finally {
      if (callableStatement != null) {
        callableStatement.close();
      }
    }
    return Boolean.TRUE;
  }

}
