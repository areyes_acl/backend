package com.sbpay.sgi.cron.step;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.enums.MsgExitoMail;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.srv.process.ProcesarRechazosTbkSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 7/1/2016, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class StepRechazosTbk {
    
    /** The Constant LOGGER. */
    private static final Logger LOG = Logger.getLogger( StepRechazosTbk.class );
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 7/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * Metodo que ejecuta la tarea principal
     * 
     * @throws Exception
     * @since 1.X
     */
    public void execute() throws Exception {
        
        try {
            LOG.info( "Inicio Proceso de Rechazos : StepRechazosTbk " );
            if ( ProcesarRechazosTbkSrv.getInstance().procesarRechazos() ) {
                LOG.info( "Envio Correo de Rechazos  Satisfactorio: Proceso Rechazos Tbk" );
                MailSendSrv.getInstance().sendMailOk(
                        MsgExitoMail.EXITO_PROCESS_RCH_TITLE.toString(),
                        MsgExitoMail.EXITO_PROCESS_RCH.toString() );
            }
            else {
                LOG.info( "Rechazos no realizados Finaliza! (total o parcialmente consultar log)" );
                
            }
        }
        catch ( AppException e ) {
            MailSendSrv.getInstance().sendMail(
                    MsgProceso.MSG_GENERIC_ERROR_MSJ.toString(),
                    ( e.getStackTraceDescripcion() != null ) ? e
                            .getStackTraceDescripcion() : e.getMessage() );
        }
        
    }
    
}
