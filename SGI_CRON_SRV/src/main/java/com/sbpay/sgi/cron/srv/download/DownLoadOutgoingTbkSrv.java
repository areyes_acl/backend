package com.sbpay.sgi.cron.srv.download;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dto.ConfigFile;
import com.sbpay.sgi.cron.dto.DataFTP;
import com.sbpay.sgi.cron.dto.ParamCronDTO;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.dto.TblCronLogDTO;
import com.sbpay.sgi.cron.enums.ConstantesPRTS;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.LogBD;
import com.sbpay.sgi.cron.enums.MsgErrorMail;
import com.sbpay.sgi.cron.enums.MsgErrorSQL;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.enums.StatusProcessType;
import com.sbpay.sgi.cron.srv.utils.ClientFTPSrv;
import com.sbpay.sgi.cron.srv.utils.CommonSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;
import com.sbpay.sgi.cron.utils.generals.DateUtils;
import com.sbpay.sgi.cron.validation.ProcessValidator;
import com.sbpay.sgi.cron.validation.impl.ProcessValidatorImpl;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 03/12/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * Clase encargada de descargar el archivo OUTGOING del Transbank y
 * dejarlo en la carpeta de INCOMING de ABC-DIN.
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public final class DownLoadOutgoingTbkSrv extends CommonSrv {
    /** The Constant LOGGER. */
    private static final Logger LOG = Logger
            .getLogger( DownLoadOutgoingTbkSrv.class.getName() );
    /** The single instance. */
    private static DownLoadOutgoingTbkSrv singleINSTANCE = null;
    
    private final ProcessCodeType PROCESO_ACTUAL = ProcessCodeType.DESCARGAR_OUTGOING;
    
    /**
     * Creates the instance.
     */
    private static void createInstance() {
        synchronized ( DownLoadOutgoingTbkSrv.class ) {
            if ( singleINSTANCE == null ) {
                singleINSTANCE = new DownLoadOutgoingTbkSrv();
            }
        }
    }
    
    /**
     * Patron singleton.
     * 
     * @return DownLoadOutgoingTbkSrv retorna instancia del servicio.
     */
    public static DownLoadOutgoingTbkSrv getInstance() {
        if ( singleINSTANCE == null ) {
            createInstance();
        }
        return singleINSTANCE;
    }
    
    /**
     * Metodo de restricion de sobreescritura de la clase.
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        super.clone();
        throw new CloneNotSupportedException();
    }
    
    /**
     * Constructor Privado.
     */
    private DownLoadOutgoingTbkSrv() {
        super();
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Metodo principal controla la logica de carga del archivo
     * incoming de transbank a sbpay
     * 
     * @return flag de carga realziada true: OK false: ERROR
     * @throws AppException
     *
     * @throws AppException
     * @since 1.X
     */
    public boolean descargarOutgoing() throws AppException {
        
        boolean flag = Boolean.FALSE;
        ParamCronDTO parametros = null;
        List<String> listFTP = null;
        List<String> listToDownload = null;
        int countDownloadProcess = 0;
        String errorFile = null;
        
        ProcessValidator validadorProceso = new ProcessValidatorImpl();
        
        // VALIDA SI PROCESO SE ENCUENTRA ACTIVADO/DESACTIVADO
        if ( validadorProceso.isActiveProcess( PROCESO_ACTUAL ) ) {
            parametros = getParametros();
            
            // VALIDA PARAMETROS DE PROCESO
            if ( validadorProceso.hasTheValidParametersForTheProcess(
                    PROCESO_ACTUAL, parametros ) ) {
                LOG.info( "Se encontraron todos los parametros necesarios para iniciar el proceso" );
                
                // OBTENER NOMBRES ARCHIVOS A DESCARGAR
                listFTP = ClientFTPSrv.getInstance().listFileSFTP(
                        parametros.getDataFTP(), parametros.getFtpPathOut() );
                
                // VALIDAR FORMATOS NOMBRES ARCHIVOS
                ConfigFile configFile = new ConfigFile();
                configFile.setEndsWith( parametros.getFormatExtNameOut() );
                configFile.setEndsWithCtr( parametros.getFormatExtNameOutCtr() );
                configFile.setStarWith( parametros.getFormatFilenameOut() );
                configFile.setStarWithCtr( parametros.getFormatFilenameOut() );
                
                // FILTRA POR ARCHIVO DE CONTROL Y POR NOMBRE DE
                // ARCHIVOS
                listToDownload = retrieveFilenameCtrListToDownload( listFTP,
                        configFile );
                
                LOG.info( "Lista de archivos outgoings que tienen archivos de control : \n"
                        + listToDownload );
                
                // SI EXISTE ALGUN ARCHIVO VALIDO CONTINUA PROCESO
                if ( listToDownload != null && listToDownload.size() > 0 ) {
                    
                    // VALIDAR QUE NO SE HAYAN DESCARGADO ANTES
                    listToDownload = currentlyNotDownloadedFiles(
                            listToDownload, configFile.getStarWith(),
                            LogBD.TABLE_LOG_INCOMING );
                    
                    LOG.info( "Lista de archivos outgoings que no han sido descargados con anterioridad : "
                            + listToDownload );
                    
                    // VALIDACION QUE EXISTA ALGUNO
                    if ( listToDownload != null && listToDownload.size() > 0 ) {
                        errorFile = ConstantesUtil.EMPTY.toString();
                        for ( String filename : listToDownload ) {
                            try {
                                // DESCARGAR DESDE FTP TBK
                                String pathFtpOri = parametros.getFtpPathOut();
                                String pathDest = parametros.getPathAclInc();
                                
                                LOG.info( "Comienza descarga de archivo "
                                        + filename + " desde ftp : "
                                        + parametros.getDataFTP().getIpHost() );
                                ClientFTPSrv.getInstance().downloadFileSFTP(
                                        parametros.getDataFTP(), pathFtpOri,
                                        pathDest, filename );
                                
                                // INSERTA EN LA TBL_LOG_INC
                                save( filename, configFile.getStarWith() );
                                
                                // CUENTA LOS REGISTROS INSERTADOS
                                countDownloadProcess++;
                            }
                            catch ( AppException e ) {
                                LOG.error(
                                        "Ha ocurrido un error al intentar descargar el archivo : "
                                                + filename, e );
                                errorFile = errorFile.concat( filename )
                                        .concat(
                                                ConstantesUtil.SKIP_LINE
                                                        .toString() );
                            }
                        }
                    }
                    else {
                        LOG.warn( "No existen nuevos archivos outgoing que descargar desde SFTP de Transbank, todos han sido procesados anteriormente" );
                        MailSendSrv.getInstance().sendMail(
                                MsgErrorMail.ALERTA_FILE_PROCESSED.toString(),
                                MsgErrorMail.ALERTA_FILE_PROCESSED_TXT
                                        .toString() );
                    }
                }
                else {
                    LOG.warn( "No existen archivos Outgoing para descargar desde SFTP de Transbank" );
                    MailSendSrv.getInstance().sendMail(
                            MsgErrorMail.ALERTA_FILE_PROCESS.toString(),
                            MsgErrorMail.ALERTA_FILE_DOWNLOAD_OUTG_TBK
                                    .toString() );
                }
            }
            else {
                LOG.warn(MsgProceso.MSG_ERROR_FALTAN_PARAMETROS.toString());
                MailSendSrv.getInstance().sendMail(
                        MsgErrorMail.ALERTA_PRTS.toString(),
                        MsgErrorMail.ALERTA_PRTS_TXT.toString() );
            }
            
            // SE SUBIERON CORRECTAMETNE TODOS LOS REGISTROS
            if ( listToDownload != null
                    && countDownloadProcess == listToDownload.size() ) {
                flag = Boolean.TRUE;
            }
            else
                if ( errorFile != null ) {
                    MailSendSrv.getInstance().sendMail(
                            MsgErrorMail.ALERTA_FILE_LOAD.toString(),
                            MsgErrorMail.ALERTA_FILE_LOAD_TXT.toString()
                                    .concat( errorFile ) );
                }
            
            return flag;
            
        }
        else {
            String asunto = MsgProceso.MSG_PROCESO_DESACTIVADO_ASUNTO
                    .toString().replace( ":?:",
                            PROCESO_ACTUAL.getCodigoProceso() );
            String mensaje = MsgProceso.MSG_PROCESO_DESACTIVADO_BODY.toString()
                    .replace( ":?:", PROCESO_ACTUAL.getCodigoProceso() );
            MailSendSrv.getInstance().sendMail( asunto, mensaje );
        }
        return flag;
        
    }
    
    /**
     * Metodo crea un registro en la tabla log TBL_LOG_INC registrando
     * la descarga nombre del archivo.
     * 
     * @param fileName
     *            nombre del archivo.
     * @param starWith
     *            prefijo del archivo.
     * @throws AppException
     *             Exception App.
     */
    private void save( final String fileName, final String starWith )
            throws AppException {
        // INICIALIZA EL DAO
        CommonsDao commonsDAO = null;
        try {
            commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
            TblCronLogDTO cronLog = new TblCronLogDTO();
            cronLog.setFecha( DateUtils.getDateFileTbk( DateUtils
                    .getDateStringFromFilename( fileName, starWith ) ) );
            cronLog.setFilename( fileName );
            cronLog.setFileFlag( StatusProcessType.PROCESS_PENDING.getValue() );
            
            if ( commonsDAO.saveTblCronLog( cronLog, LogBD.TABLE_LOG_INCOMING ) ) {
                commonsDAO.endTx();
            }
            else {
                commonsDAO.rollBack();
            }
        }
        catch ( SQLException e ) {
            rollBackTransaccion( commonsDAO );
            throw new AppException( MsgErrorSQL.ERROR_LOAD_PROCESS.toString(),
                    e );
        }
        finally {
            try {
                if ( commonsDAO != null ) {
                    commonsDAO.close();
                }
            }
            catch ( SQLException e ) {
                rollBackTransaccion( commonsDAO );
                throw new AppException(
                        MsgErrorSQL.ERROR_LOAD_PROCESS.toString(), e );
            }
        }
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @throws AppException
     * @since 1.X
     */
    private ParamCronDTO getParametros() throws AppException {
        
        CommonsDao daoCommon = null;
        List<ParametroDTO> listaParametros = null;
        DataFTP dataFTP = null;
        ParamCronDTO parametros = null;
        try {
            LOG.info( "CARGA PARAMETROS PROCESO" );
            daoCommon = CommonsDaoFactory.getInstance().getNewEntidadDao();
            listaParametros = daoCommon
                    .getParametroCodGrupoDato( ConstantesPRTS.CRON_TBK
                            .toString() );
            
            if ( listaParametros != null ) {
                
                // PARAMETROS DE FTP TRANSBANK
                dataFTP = new DataFTP();
                dataFTP.setIpHost( CommonsUtils.getCodiDato( listaParametros,ConstantesPRTS.SFTP_IP_DOWNLOAD_OUTGOING.toString() ) );
                String port = CommonsUtils.getCodiDato(listaParametros, ConstantesPRTS.SFTP_PORT_DOWNLOAD_OUTGOING.toString()) ;
                dataFTP.setPort( Integer.parseInt((port != null && port.matches(ConstantesUtil.PATTERN_IS_NUMBER.toString()) ? port : ConstantesUtil.DEFAULT_PORT.toString())));
                dataFTP.setUser( CommonsUtils.getCodiDato( listaParametros, ConstantesPRTS.SFTP_USER_DOWNLOAD_OUTGOING.toString() ) );
                dataFTP.setPassword( CommonsUtils.getCodiDato( listaParametros, ConstantesPRTS.SFTP_PASS_DOWNLOAD_OUTGOING.toString() ) );
                
                parametros = new ParamCronDTO();
                
                // RUTA FTP DONDE BUSCA ARCHIVOS A DESCARGAR
                parametros
                        .setFtpPathOut( CommonsUtils.getCodiDato(
                                listaParametros,
                                ConstantesPRTS.PATH_OUT_DWNLD_OUTGOING.toString() ) );
                
                // RUTA SERVIDOR DONDE SE DEJARA EL OUTGOING DESCARGADO
                parametros
                        .setPathAclInc( CommonsUtils.getCodiDato(
                                listaParametros,
                                ConstantesPRTS.PATH_ACL_INC.toString() ) );
                
                // FORMATO DE ARCHIVO OUTGOING
                parametros.setFormatFilenameOut( CommonsUtils.getCodiDato(
                        listaParametros,
                        ConstantesPRTS.FORMAT_FILENAME_OUT.toString() ) );
                
                // FORMATO DE EXTENCION DE OUTGOING
                parametros.setFormatExtNameOut( CommonsUtils.getCodiDato(
                        listaParametros,
                        ConstantesPRTS.FORMAT_EXTNAME_OUT.toString() ) );
                
                // FORMATO DE EXTENSION DE ARCHIVO DE CONTROL
                parametros.setFormatExtNameOutCtr( CommonsUtils.getCodiDato(
                        listaParametros,
                        ConstantesPRTS.FORMAT_EXTNAME_OUT_CTR.toString() ) );
                                
                parametros.setDataFTP( dataFTP );
                
            }
            else {
                parametros = null;
            }
        }
        catch ( SQLException e ) {
            throw new AppException( MsgErrorSQL.ERROR_QUERY_PRTS.toString(), e );
        }
        finally {
            try {
                if ( daoCommon != null ) {
                    daoCommon.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException(
                        MsgErrorSQL.ERROR_QUERY_PRTS_CLOSE.toString(), e );
            }
        }
        
        return parametros;
    }
    
}
