package com.sbpay.sgi.cron.step;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.srv.process.ProcesarOutgoingVisaNacSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 16/04/2019, (ACL) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA.</B>
 */
public class StepProcessOutgoingVisaNac {
	
	/**
    * VARIABLES
    */
   private static final Logger LOG = Logger
           .getLogger( StepProcessOutgoingVisaNac.class );
   
   /**
    * 
    * 
    * <p>
    * Registro de versiones:
    * <ul>
    * <li>1.0 16/04/2019, (Everis Chile) - versión inicial
    * </ul>
    * <p>
    * 
    * @return
    * @throws Exception
    * @since 1.X
    */
   public void execute() throws Exception {
       try {
           LOG.info( "=======> Inicio prcesamiento Archivo Incoming(outgoing) visa nacional  : Step Procesar Outgoing visa nacional <======" );
           ProcesarOutgoingVisaNacSrv.getInstance().procesarArchivo();
           
       }
       catch ( AppException e ) {
           LOG.error( e.getMessage(),e );
           MailSendSrv.getInstance().sendMail(
                   MsgProceso.MSG_GENERIC_ERROR_MSJ.toString(),
                   ( e.getStackTraceDescripcion() != null ) ? e
                           .getStackTraceDescripcion() : e.getMessage() );
       }
       
   }
}
