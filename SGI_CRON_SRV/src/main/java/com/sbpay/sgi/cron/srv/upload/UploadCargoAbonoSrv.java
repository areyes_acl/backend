package com.sbpay.sgi.cron.srv.upload;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dto.DataFTP;
import com.sbpay.sgi.cron.dto.ParamUploadCargoAbonoDTO;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.dto.TblCronLogDTO;
import com.sbpay.sgi.cron.enums.ConstantesPRTS;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.DateFormatType;
import com.sbpay.sgi.cron.enums.FilterTypeSearch;
import com.sbpay.sgi.cron.enums.LogBD;
import com.sbpay.sgi.cron.enums.MsgErrorMail;
import com.sbpay.sgi.cron.enums.MsgErrorSQL;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.enums.StatusProcessType;
import com.sbpay.sgi.cron.srv.utils.ClientFTPSrv;
import com.sbpay.sgi.cron.srv.utils.CommonSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;
import com.sbpay.sgi.cron.utils.generals.DateUtils;
import com.sbpay.sgi.cron.validation.ProcessValidator;
import com.sbpay.sgi.cron.validation.impl.ProcessValidatorImpl;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 24/11/2015, (ACL-sbpay) - versiÃ³n inicial
 * </ul>
 * <p>
 * Clase que realiza la logica de la subida del archivo de cargos y
 * abonos al FTP de IC.
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public final class UploadCargoAbonoSrv extends CommonSrv {
    /** The Constant LOGGER. */
    private static final Logger LOG = Logger
            .getLogger( UploadCargoAbonoSrv.class );
    /** The single instance. */
    private static UploadCargoAbonoSrv singleINSTANCE = null;
    
    // PROCESO
    private final ProcessCodeType PROCESO_ACTUAL = ProcessCodeType.SUBIDA_CARGOS_Y_ABONOS;
    
    /**
     * Creates the instance.
     */
    private static void createInstance() {
        synchronized ( UploadCargoAbonoSrv.class ) {
            if ( singleINSTANCE == null ) {
                singleINSTANCE = new UploadCargoAbonoSrv();
                
            }
        }
    }
    
    /**
     * Patron singleton.
     * 
     * @return UploadIncomingSrv retorna instancia del servicio.
     */
    public static UploadCargoAbonoSrv getInstance() {
        if ( singleINSTANCE == null ) {
            createInstance();
        }
        return singleINSTANCE;
    }
    
    /**
     * Metodo de restricion de sobreescritura de la clase.
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        super.clone();
        throw new CloneNotSupportedException();
    }
    
    /**
     * Constructor Privado.
     */
    private UploadCargoAbonoSrv() {
        super();
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Metodo que realiza la subida de la Sol60 a Transbank
     * 
     * @return true false
     * @throws AppException
     * @since 1.X
     */
    public boolean uploadCargoAbono() throws AppException {
        boolean flag = false;
        List<String> fileDirectory = null;
        List<String> fileUploadList = null;
        ParamUploadCargoAbonoDTO parametros = null;
        String detalleArchivosNoSubidos = "";
        
        ProcessValidator validadorProceso = new ProcessValidatorImpl();
        
        if ( validadorProceso.isActiveProcess( PROCESO_ACTUAL ) ) {
            
            parametros = obtenerParametros();
            
            if ( validadorProceso.hasTheValidParametersForTheProcess(
                    PROCESO_ACTUAL, parametros ) ) {
                
                String pathSearch = parametros.getPathCarAbo();
                String startWith = parametros.getFormatFilenameCarAbo();
                String extFile = parametros.getFormatExtNameCarAbo();
                
                // Busca lista de archivos candidatos a procesar
                fileDirectory = getFileStartsWith( pathSearch, startWith,
                        extFile );
                
                LOG.info( "Lista de archivos que contiene formato de nombre valido:  "
                        + fileDirectory );
                if ( fileDirectory != null && fileDirectory.size() > 0 ) {
                    // Busca los archivos en BD que se procesaran
                    fileUploadList = getFileUpload( fileDirectory,
                            parametros.getFormatFilenameCarAbo() );
                    
                    LOG.info( "Lista de archivos que no han sido subidos con anterioridad:"
                            + fileUploadList );
                    if ( fileUploadList != null && fileUploadList.size() > 0 ) {
                        
                        String pathOrigen = parametros.getPathCarAbo();
                        String pathFtpDes = parametros.getPathSFTPEntrada();
                        
                        int count = 0;
                        // LISTA DE ARCHIVOS A SUBIR
                        for ( String filename : fileUploadList ) {
                            try {
                                if ( ClientFTPSrv.getInstance().uploadFileSFTP(
                                        parametros.getDataFTP(), pathOrigen,
                                        pathFtpDes, filename ) ) {
                                    
                                    // ACTUALIZA LOG AL SUBIR AL SFTP
                                    updateLog( filename,
                                            StatusProcessType.PROCESS_SUCESSFUL );
                                    count++;
                                    LOG.info( "ARCHIVO " + filename
                                            + " SUBIDO CORRECTAMENTE A IC" );
                                    
                                    // MUEVE ARCHIVO A BKP
                                    CommonsUtils.moveFile(
                                            parametros.getPathCarAbo().concat(
                                                    filename ),
                                            parametros.getPathCarAboBkp()
                                                    .concat( filename ) );
                                    LOG.info( "Archivo: " + filename
                                            + ", movido a carpeta de BKP " );
                                }
                                else {
                                    LOG.info( "Archivo: " + filename
                                            + ", no ha podido subirse a SFTP." );
                                    detalleArchivosNoSubidos = detalleArchivosNoSubidos
                                            .concat( "\n " ).concat( filename );
                                    
                                    // UPDATEA LOG A ERROR
                                    updateLog( filename,
                                            StatusProcessType.PROCESS_ERROR );
                                    
                                    // MUEVE ARCHIVO A ERROR
                                    CommonsUtils.moveFile(
                                            parametros.getPathCarAbo().concat(
                                                    filename ), parametros
                                                    .getPathCarAboError()
                                                    .concat( filename ) );
                                    LOG.info( "Archivo: " + filename
                                            + ", movido a carpeta de ERROR " );
                                }
                            }
                            catch ( Exception e ) {
                                LOG.error( e.getMessage(), e );
                                
                                // UPDATEA LOG A ERROR
                                updateLog( filename,
                                        StatusProcessType.PROCESS_ERROR );
                                
                                // MUEVE ARCHIVO A ERROR
                                CommonsUtils.moveFile(
                                        parametros.getPathCarAbo().concat(
                                                filename ),
                                        parametros.getPathCarAboError().concat(
                                                filename ) );
                                LOG.info( "Archivo: " + filename
                                        + ", movido a carpeta de ERROR " );
                                detalleArchivosNoSubidos = detalleArchivosNoSubidos
                                        .concat( "\n" ).concat( filename );
                            }
                        }
                        
                        // NO SE PUDO SUBIR NINGUN ARCHIVO
                        if ( count == 0 ) {
                            LOG.info( "Error al subir archivo SOL60, no se ha podido subir ningun archivo Sol 60 a SFTP" );
                            MailSendSrv
                                    .getInstance()
                                    .sendMailOk(
                                            MsgErrorMail.ALERTA_FILE_UPLOAD_CAR_ABO_TITLE
                                                    .toString(),
                                            "No se ha subido ningun archivo SOL60" );
                            flag = Boolean.FALSE;
                            
                        }
                        // NO SE SUBIERON TODOS LOS ARCHIVOS
                        else
                            if ( count > 0 && count < fileUploadList.size() ) {
                                LOG.info( "No se han subido todos los archivos del tipo solicitud60" );
                                MailSendSrv
                                        .getInstance()
                                        .sendMailOk(
                                                MsgErrorMail.ALERTA_FILE_UPLOAD_CAR_ABO_TITLE
                                                        .toString(),
                                                "Los siguientes archivos no han podido ser procesados : "
                                                        + detalleArchivosNoSubidos );
                                flag = Boolean.FALSE;
                            }
                            // NO SE SUBIERON TODOS LOS ARCHIVOS
                            else
                                if ( count > 0
                                        && count == fileUploadList.size() ) {
                                    LOG.info( "Se han subido todos los archivos del tipo solicitud60" );
                                    flag = Boolean.TRUE;
                                }
                        
                    }
                    else {
                        LOG.info( "Todo archivos Cargo y Abonos (SOL60) para subir se encuentran procesados" );
                        MailSendSrv
                                .getInstance()
                                .sendMailOk(
                                        MsgErrorMail.ALERTA_FILE_PROCESSED_UPLOAD_CAR_ABO
                                                .toString(),
                                        MsgErrorMail.ALERTA_FILE_PROCESSED_UPLOAD_TXT_CAR_ABO
                                                .toString() );
                    }
                }
                else {
                    LOG.info( "No existen archivos de cargos y abonos para subir" );
                    MailSendSrv.getInstance().sendMailOk(
                            MsgErrorMail.ALERTA_FILE_UPLOAD_CAR_ABO.toString(),
                            MsgErrorMail.ALERTA_FILE_UPLOAD_TXT_CAR_ABO
                                    .toString() );
                }
            }
            else {
                LOG.warn( MsgProceso.MSG_ERROR_FALTAN_PARAMETROS.toString() );
                MailSendSrv.getInstance().sendMailOk(
                        MsgErrorMail.ALERTA_PRTS_UPLOAD_CAR_ABO.toString(),
                        MsgErrorMail.ALERTA_PRTS_TXT_UPLOAD_CAR_ABO.toString() );
            }
            return flag;
        }
        else {
            String asunto = MsgProceso.MSG_PROCESO_DESACTIVADO_ASUNTO
                    .toString().replace( ":?:",
                            PROCESO_ACTUAL.getCodigoProceso() );
            String mensaje = MsgProceso.MSG_PROCESO_DESACTIVADO_BODY.toString()
                    .replace( ":?:", PROCESO_ACTUAL.getCodigoProceso() );
            MailSendSrv.getInstance().sendMail( asunto, mensaje );
        }
        return flag;
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Metodo busca los archivos que van a ser subidos al servidor FTP
     * de IC (Cargos y Abonos).
     * 
     * @param fileDirectory
     * @return
     * @throws AppException
     * @since 1.X
     */
    private List<String> getFileUpload( final List<String> fileDirectory,
            String starwith ) throws AppException {
        
        String dateFile = null;
        List<String> fileCarga = null;
        // UploadDao daoUpload = null;
        CommonsDao daoCommons = null;
        String fileAux = null;
        // CronLogDTO cronLogDTO = null;
        TblCronLogDTO dto = null;
        TblCronLogDTO dtoAux = new TblCronLogDTO();
        try {
            fileCarga = new ArrayList<String>();
            daoCommons = CommonsDaoFactory.getInstance().getNewEntidadDao();
            /* Lista de archivos que se pueden procesar. */
            for ( String fileName : fileDirectory ) {
                // formato PREFIJOmmdds.dat
                fileAux = fileName.replace( starwith, "" );
                // Rescata fecha archivo incoming
                dateFile = fileAux.substring( 0, 8 );
                dateFile = DateUtils.parseFormat( dateFile,
                        DateFormatType.FORMAT_YYYYMMDD,
                        DateFormatType.FORMAT_DDMMYY_WITH_SLASH_SEPARATOR );
                
                if ( dateFile != null ) {
                    dtoAux.setFecha( dateFile );
                    dto = daoCommons.getTblCronLogByType( dtoAux,
                            LogBD.TABLE_LOG_CARGO_ABONO.getTable(),
                            FilterTypeSearch.FILTER_BY_DATE );
                    if ( dto != null
                            && ConstantesUtil.ZERO.toString().equalsIgnoreCase(
                                    dto.getFileFlag() ) ) {
                        fileCarga.add( fileName );
                    }
                }
                else {
                    LOG.info( "Archivo no cumple con el formato para obtener la fecha al procesar los cargos y abonos "
                            + fileName );
                    throw new AppException(
                            "Archivo no cumple con el formato para obtener la fecha al procesar los cargos y abonos "
                                    + fileName );
                }
            }
            return fileCarga;
        }
        catch ( SQLException e ) {
            throw new AppException(
                    MsgErrorSQL.ERROR_QUERY_PROCESS_UPLOAD_CAR_ABO.toString(),
                    e );
        }
        finally {
            try {
                if ( daoCommons != null ) {
                    daoCommons.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException(
                        MsgErrorSQL.ERROR_QUERY_PROCESS_CLOSE_UPLOAD_CAR_ABO
                                .toString(),
                        e );
            }
        }
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param filename
     * @param status
     * @since 1.X
     */
    private void updateLog( String filename, StatusProcessType status ) {
        CommonsDao commons = null;
        
        try {
            commons = CommonsDaoFactory.getInstance().getNewEntidadDao();
            TblCronLogDTO dto = new TblCronLogDTO();
            dto.setFilename( filename );
            dto.setFileFlag( status.getValue() );
            
            if ( commons.updateTblCronLogByType( dto,
                    LogBD.TABLE_LOG_CARGO_ABONO.getTable(),
                    FilterTypeSearch.FILTER_BY_NAME ) ) {
                LOG.info( "SE UPDATEA CORRECTAMENTE TABLA LOG _PARA FILE  :"
                        + filename );
                commons.endTx();
            }
            else {
                LOG.warn( "NO SE UPDATEA TABLA LOG _PARA FILE  :" + filename );
                commons.rollBack();
            }
            
        }
        catch ( SQLException e ) {
            LOG.warn( "NO SE HA PODIDO REALIZAR UPDATE " + e );
        }
        finally {
            
            try {
                if ( commons != null ) {
                    commons.close();
                }
            }
            catch ( SQLException e ) {
                LOG.error( e.getMessage(), e );
            }
        }
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * Metodo busca los parametros para el proceso de subir
     * solicitud60
     * 
     * @throws AppException
     * @since 1.X
     */
    private ParamUploadCargoAbonoDTO obtenerParametros() throws AppException {
        CommonsDao daoCommon = null;
        List<ParametroDTO> paramDTOList = null;
        ParamUploadCargoAbonoDTO parametros = null;
        
        try {
            daoCommon = CommonsDaoFactory.getInstance().getNewEntidadDao();
            paramDTOList = daoCommon
                    .getParametroCodGrupoDato( ConstantesPRTS.CRON_TBK
                            .toString() );
            if ( paramDTOList != null ) {
                parametros = new ParamUploadCargoAbonoDTO();
                
                // DATOS PARA FTP SOL 60
                DataFTP dataFTP = new DataFTP();
                dataFTP.setIpHost( CommonsUtils.getCodiDato( paramDTOList,
                        ConstantesPRTS.SFTP_IP_UPLOAD_SOL60.toString() ) );
                String port = CommonsUtils.getCodiDato( paramDTOList,
                        ConstantesPRTS.SFTP_PORT_UPLOAD_SOL60.toString() );
                dataFTP.setPort( Integer.parseInt( ( port != null
                        && port.matches( ConstantesUtil.PATTERN_IS_NUMBER
                                .toString() ) ? port
                        : ConstantesUtil.DEFAULT_PORT.toString() ) ) );
                dataFTP.setUser( CommonsUtils.getCodiDato( paramDTOList,
                        ConstantesPRTS.SFTP_USER_UPLOAD_SOL60.toString() ) );
                dataFTP.setPassword( CommonsUtils.getCodiDato( paramDTOList,
                        ConstantesPRTS.SFTP_PASS_UPLOAD_SOL60.toString() ) );
                parametros.setDataFTP( dataFTP );
                
                // RUTA SERVIDOR SFTP TRANSANBANK
                parametros.setPathSFTPEntrada( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.PATH_IN_UPLD_SOL60.toString() ) );
                
                // PARAMETROS FORMATO DE SOLICITUD60
                parametros.setFormatExtNameCarAbo( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.FORMAT_EXTNAME_CAR_ABO.toString() ) );
                parametros.setFormatFilenameCarAbo( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.FORMAT_FILENAME_CAR_ABO.toString() ) );
                
                // PARAMETROS DE RUTAS
                parametros.setPathCarAbo( CommonsUtils.getCodiDato(
                        paramDTOList, ConstantesPRTS.PATH_CAR_ABO.toString() ) );
                parametros.setPathCarAboBkp( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.PATH_CAR_ABO_BKP.toString() ) );
                parametros.setPathCarAboError( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.PATH_CAR_ABO_ERROR.toString() ) );
                
            }
            else {
                parametros = null;
            }
        }
        catch ( SQLException e ) {
            throw new AppException(
                    MsgErrorSQL.ERROR_QUERY_PRTS_UPLOAD_CAR_ABO.toString(), e );
        }
        finally {
            try {
                if ( daoCommon != null ) {
                    daoCommon.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException(
                        MsgErrorSQL.ERROR_QUERY_PRTS_UPLOAD_CLOSE_CAR_ABO
                                .toString(),
                        e );
            }
        }
        return parametros;
    }
    
}
