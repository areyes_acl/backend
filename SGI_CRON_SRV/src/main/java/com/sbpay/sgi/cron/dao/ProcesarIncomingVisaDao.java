package com.sbpay.sgi.cron.dao;

import java.sql.SQLException;

import com.sbpay.sgi.cron.dao.Dao;
import com.sbpay.sgi.cron.dto.TransaccionOutVisa;
import com.sbpay.sgi.cron.dto.TransaccionOutVisaCobroCargo;
import com.sbpay.sgi.cron.dto.TransaccionOutVisaPago;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 25/04/2019, (ACL) -  versión inicial 
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SA.</B>
 */
public interface ProcesarIncomingVisaDao extends Dao {
    
    public boolean saveTransaction(TransaccionOutVisa transaccion, Integer sidOperador) throws Exception;
    
    public boolean saveTransactionPago(TransaccionOutVisaPago transaccion, Integer sidOperador) throws Exception;
    
    public boolean callSPCargaIncoming() throws SQLException;
    
    public boolean callSPCargaIncomingPago() throws SQLException;

	public boolean saveTransactionCargoAbono(TransaccionOutVisaCobroCargo transaccion, Integer sidOperador) throws Exception;

	public boolean callSPCobroCargoVisa() throws SQLException;

	
    
}