package com.sbpay.sgi.cron.srv.upload;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dto.ConfigFile;
import com.sbpay.sgi.cron.dto.DataFTP;
import com.sbpay.sgi.cron.dto.ParamUploadVisaDTO;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.dto.TblCronLogDTO;
import com.sbpay.sgi.cron.enums.ConstantesPRTS;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.FilterTypeSearch;
import com.sbpay.sgi.cron.enums.LogBD;
import com.sbpay.sgi.cron.enums.MsgErrorFile;
import com.sbpay.sgi.cron.enums.MsgErrorMail;
import com.sbpay.sgi.cron.enums.MsgErrorSQL;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.enums.StatusProcessType;
import com.sbpay.sgi.cron.srv.utils.ClientFTPSrv;
import com.sbpay.sgi.cron.srv.utils.CommonSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;
import com.sbpay.sgi.cron.utils.generals.DateUtils;
import com.sbpay.sgi.cron.validation.ProcessValidator;
import com.sbpay.sgi.cron.validation.impl.ProcessValidatorImpl;


/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public final class UploadVisaIntSrv extends CommonSrv {

  private static final Logger LOG = Logger.getLogger(UploadVisaIntSrv.class);
  private static UploadVisaIntSrv singleINSTANCE = null;
  private String msgfileNotUp = null;
  
  private final ProcessCodeType PROCESO_ACTUAL = ProcessCodeType.SUBIDA_INCOMING_VISA_INTERNACIONAL;


  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
   * </ul>
   * <p>
   * 
   * @since 1.X
   */
  private static void createInstance() {
    synchronized (UploadVisaIntSrv.class) {
      if (singleINSTANCE == null) {
        singleINSTANCE = new UploadVisaIntSrv();
      }
    }
  }

  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
   * </ul>
   * <p>
   * 
   * @return
   * @since 1.X
   */
  public static UploadVisaIntSrv getInstance() {
    if (singleINSTANCE == null) {
      createInstance();
    }
    return singleINSTANCE;
  }

  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
   * </ul>
   * <p>
   * 
   * @return
   * @throws CloneNotSupportedException
   * @see java.lang.Object#clone()
   * @since 1.X
   */
  @Override
  protected Object clone() throws CloneNotSupportedException {
    super.clone();
    throw new CloneNotSupportedException();
  }

  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
   * </ul>
   * <p>
   * 
   * @since 1.X
   */
  private UploadVisaIntSrv() {
    super();
  }

  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
   * </ul>
   * <p>
   * 
   * @return
   * @throws AppException
   * @since 1.X
   */
	public boolean uploadVisa() throws AppException {
		
		// VALIDA SI EL PROCESO ESTA O NO ACTIVADO
		ProcessValidator validadorProceso = new ProcessValidatorImpl();
	    if (!validadorProceso.isActiveProcess(PROCESO_ACTUAL)) {
	        String asunto = MsgProceso.MSG_PROCESO_DESACTIVADO_ASUNTO
                    .toString().replace( ":?:",
                            PROCESO_ACTUAL.getCodigoProceso() );
            String mensaje = MsgProceso.MSG_PROCESO_DESACTIVADO_BODY.toString()
                    .replace( ":?:", PROCESO_ACTUAL.getCodigoProceso() );
            MailSendSrv.getInstance().sendMail( asunto, mensaje );
            return false;
	    }
	    
	    LOG.info("Proceso está activo");
	    ParamUploadVisaDTO parametros = null;
		boolean flag = false;
		List<String> fileDirectory = null;
		List<String> fileUpload = null;

		parametros = getParametrosUploadCron();

		// VALIDACION DE PARAMETROS
		if (!validadorProceso.hasTheValidParametersForTheProcess( PROCESO_ACTUAL, parametros )) {
			LOG.info("Parametros de BD incompletos");
			MailSendSrv.getInstance().sendMail(MsgErrorMail.ALERTA_PRTS.toString(),
											   MsgErrorMail.ALERTA_PRTS_TXT.toString());
			return flag;
		}

		String pathSearch = parametros.getPathVisaInc();
		//String pathFTP = parametros.getFtpPathVisaOut();

		// BUSQUEDA DE ARCHIVOS QUE POSEAN NOMBRE VALIDO
		fileDirectory = getFileStartsWith(pathSearch, parametros.getFilenameVisa(), parametros.getExtVisaFile());
		
		LOG.info("Lista de archivos que contiene formato de nombre valido para subir:  " + fileDirectory);
		
		if (fileDirectory != null && fileDirectory.size() > 0) {
			ConfigFile config = new ConfigFile();
            config.setStarWith( parametros.getFilenameVisa() );
            config.setEndsWith( parametros.getExtVisaFile() );
            config.setStarWithCtr( parametros.getFilenameVisa() );
            config.setEndsWithCtr( parametros.getFormatExtNameIncVisaCrt() );
            
			// BUSCA LOS ARCHIVOS QUE TENGAN UN REGISTRO EN LA TABLA :  TBL_LOG_OUT_VISA
			fileUpload = getFileUpload(fileDirectory, parametros);
			
			// SE BUSCA ARCHIVOS DE CONTROL DE ESOS INCOMING
            List<String> controlFileList = retrieveFilenameCtrList(pathSearch, config);
			
			LOG.info("Lista de archivos que no han sido subidos con anterioridad:" + fileUpload);

			if (fileUpload != null && fileUpload.size() > 0) {
				// BUSCA LA LISTA DE ARCHIVOS EXISTENTES EN EL
                // SFTP DE VISA
                List<String> listaFtpTbk = ClientFTPSrv.getInstance().listFileSFTP( parametros.getDataFTP(), parametros.getFtpPathVisaOut() );
                
				int count = subirIncoming(parametros, fileUpload, listaFtpTbk, controlFileList);

				if (count == fileUpload.size()) {
					flag = true;
				}else{
					LOG.warn("SE EJECUTA PROCESO DE SUBIDA DE INCOMING VISA INTERNACIONAL A SFTP DE VISA INTERNACIONAL, PERSO EXISTEN ARCHIVOS QUE NO SE SUBIERON");
					MailSendSrv.getInstance().sendMail(MsgErrorMail.ALERTA_FILE_UPLOAD_INCOMING_VISA_INT.toString(),
							   MsgErrorMail.ALERTA_FILE_UPLOAD_INCOMING_VISA_INT_TXT.toString().replace("?", msgfileNotUp));
					
				}
			} else {
				LOG.info("No existen archivos Incoming VISA Internacional para subir a SFTP VISA Internacional");
				MailSendSrv.getInstance().sendMail(MsgErrorMail.ALERTA_FILE_PROCESSED_UPLOAD_VISA_INT.toString(),
												   MsgErrorMail.ALERTA_FILE_PROCESSED_UPLOAD_VISA_INT_TXT.toString());
			}
		} else {
			LOG.info("No existen archivos Incoming VISA Internacional para subir a SFTP VISA Internacional");
			MailSendSrv.getInstance().sendMail(MsgErrorMail.ALERTA_FILE_UPLOAD_VISA_INT.toString(),
											   MsgErrorMail.ALERTA_FILE_UPLOAD_VISA_INT_TXT.toString());
		}

		return flag;
	}

  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
   * </ul>
   * <p>
   * 
   * @param fileUpload
   * @param dataFTP
   * @param pathSearch
   * @param pathFTP
   * @param count
   * @return
   * @throws AppException
   * @since 1.X
   */
  private int subirIncoming(ParamUploadVisaDTO params,
          List<String> fileToUploadList, List<String> listaFtpTbk,
          List<String> controlFileList) throws AppException {
	  int count = 0;
      String filenameCtr = null;
      // RECORRE LA LISTA DE ARCHIVOS
      for ( String filename : fileToUploadList ) {
      	filenameCtr = "";
          try {
              // SI NO SE ENCUENTRA EL CTR NI EL INCOMING EN FTP SE
              // SUBE
              if ( listaFtpTbk != null
                      && !listaFtpTbk.contains( filename )
                      && !listaFtpTbk.contains( filename.replace(
                              params.getFormatExtNameIncVisaCrt(),
                              params.getExtVisaFile() ) ) ) {
                  
                  // Busco archivo de control
                  filenameCtr = buscarArchivoContrl( filename,
                          controlFileList, params.getExtVisaFile(),
                          params.getFormatExtNameIncVisaCrt() );
                 
                  // SUBO INCOMING A VISA
                  if ( ClientFTPSrv.getInstance().uploadFileSFTP(
                          params.getDataFTP(), params.getPathVisaInc(),
                          params.getFtpPathVisaOut(), filename ) ) {
                      LOG.info( "ARCHIVO " + filename
                              + " SUBIDO CORRECTAMENTE A FTP = "
                              + params.getDataFTP().getIpHost() );
                      
                    
                      if ( filenameCtr != null ) {
                          if ( ClientFTPSrv.getInstance().uploadFileSFTP(
                                  params.getDataFTP(),
                                  params.getPathVisaInc(),
                                  params.getFtpPathVisaOut(), filenameCtr ) ) {
                              LOG.info( "ARCHIVO " + filename
                                      + " SUBIDO CORRECTAMENTE A FTP = "
                                      + params.getDataFTP().getIpHost() );
                              
                              // MUEVE ARCHIVO INC A CARPETA BKP
                              CommonsUtils.moveFile( params.getPathVisaInc()
                                      + filename, params.getPathVisaBkp()
                                      + filename );
                              
                              // MUEVE ARCHIVO CTR A CARPETA BKP
                              CommonsUtils
                                      .moveFile( params.getPathVisaInc()
                                              + filenameCtr,
                                              params.getPathVisaBkp()
                                                      + filenameCtr );
                              
                              // UPDATE TABLA LOG
                              updateLog( filename,
                                      StatusProcessType.PROCESS_SUCESSFUL );
                              
                              count++;
                          }
                          else {
                              LOG.warn( "NO SE HA PODIDO SUBIR INCOMING" );
                          }
                          
                      }
                      else {
                          LOG.warn( "ARCHIVO : " + filename
                                  + " YA SE ENCUENTRA EN FTP VISA INTERNACIONAL" );
                      }
                  }
                  else {
                      LOG.warn( "NO SE HA PODIDO SUBIR ARCHIVO INCOMING A BKP" );
                  }
              }
          }
          catch ( Exception e ) {
              LOG.error( "NO SE PUDO SUBIR ARCHIVO " + filename
                      + " A FTP, SE MUEVE A FORLDER ERROR : "
                      + params.getPathVisaError() );
              updateLog( filename, StatusProcessType.PROCESS_ERROR );
             
              // MUEVE A ERROR INCOMING
              CommonsUtils.moveFile( params.getPathVisaInc() + filename,
                      params.getPathVisaError()+ filename );
              
              // MUEVE A ERROR CTR
              if( filenameCtr != null && !filenameCtr.isEmpty()){
              	CommonsUtils.moveFile( params.getPathVisaInc()+ filenameCtr,params.getPathVisaError() + filenameCtr );	
              }
              msgfileNotUp.concat( filename ).concat(
                      ConstantesUtil.SKIP_LINE.toString() );
          }
      }
      return count;
  }
  
  private String buscarArchivoContrl( String filename,
          List<String> controlFileList, String extName, String ctrExt ) {
      
      for ( String fileCTR : controlFileList ) {
          String fileAuxCTR = fileCTR.replace( ctrExt, "" );
          String fileAuxiNC = filename.replace( extName, "" );
          
          if ( fileAuxCTR.equals( fileAuxiNC ) ) {
              return fileCTR;
          }
      }
      
      return null;
  }



  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
   * </ul>
   * <p>
   * 
   * @param filename
   * @param status
   * @since 1.X
   */
  private void updateLog(String filename, StatusProcessType status) {
    CommonsDao commons = null;
    try {
      // SE SETEA EL DTO
      commons = CommonsDaoFactory.getInstance().getNewEntidadDao();
      TblCronLogDTO dto = new TblCronLogDTO();
      dto.setFilename(filename);
      dto.setFileFlag(status.getValue());

      // SE UPDATE
      if (commons.updateTblCronLogByType(dto, LogBD.TBL_LOG_OUT_VISA_INT.getTable(),
          FilterTypeSearch.FILTER_BY_NAME)) {
        LOG.info("SE UPDATEA CORRECTAMENTE TABLA TBL_LOG_OUT_VISA_INT PARA FILE  :" + filename);
        commons.endTx();
      } else {
        LOG.warn("NO SE UPDATEA TABLA TBL_LOG_OUT_VISA_INT PARA FILE  :" + filename);
        commons.rollBack();
      }
    } catch (SQLException e) {
      LOG.warn("NO SE HA PODIDO REALIZAR UPDATE " + e);
    } finally {

      if (commons != null) {
        try {
          commons.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }

  }



  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
   * </ul>
   * <p>
   * 
   * @param fileDirectory
   * @return
   * @throws AppException
   * @since 1.X
   */
  private List<String> getFileUpload(final List<String> fileDirectory, ParamUploadVisaDTO parametros)
      throws AppException {

    String dateFile = null;
    List<String> fileCarga = null;
    CommonsDao daoCommons = null;
    String fileAux = null;
    TblCronLogDTO dto = null;
    TblCronLogDTO dtoAux = new TblCronLogDTO();
    try {
      fileCarga = new ArrayList<String>();
      daoCommons = CommonsDaoFactory.getInstance().getNewEntidadDao();

      for (String fileName : fileDirectory) {
        fileAux = fileName.replace(parametros.getFilenameVisa(), "");
        dateFile = fileAux.substring(0, 8);
        dateFile = DateUtils.getDateFileTbk(fileAux);

        if (dateFile != null) {
          dtoAux.setFecha(dateFile);
          dto =
              daoCommons.getTblCronLogByType(dtoAux, LogBD.TBL_LOG_OUT_VISA_INT.getTable(),
                  FilterTypeSearch.FILTER_BY_DATE);
          if (dto != null && ConstantesUtil.ZERO.toString().equalsIgnoreCase(dto.getFileFlag())) {
            fileCarga.add(fileName);
          }
        } else {
          LOG.info("Archivo no cumple con el formato para obtener la fecha " + fileName);
          CommonsUtils.moveFile(parametros.getPathVisaInc() + fileName,
              parametros.getPathVisaError() + fileName);
          MailSendSrv.getInstance().sendMail(MsgErrorFile.ERROR_FILE_FORMAT.toString(),
              MsgErrorFile.ERROR_FILE_FORMAT_TXT.toString().concat(fileName));
        }
      }
      return fileCarga;
    } catch (SQLException e) {
      throw new AppException(MsgErrorSQL.ERROR_QUERY_PROCESS_CLOSE_UPLOAD.toString(), e);
    } finally {
      try {
        if (daoCommons != null) {
          daoCommons.close();
        }
      } catch (SQLException e) {
        throw new AppException(MsgErrorSQL.ERROR_QUERY_PROCESS_CLOSE_UPLOAD.toString(), e);
      }
    }
  }
  
  
	



  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
   * </ul>
   * <p>
   * 
   * @return
   * @throws AppException
   * @since 1.X
   */
  private ParamUploadVisaDTO getParametrosUploadCron() throws AppException {
    CommonsDao daoCommon = null;
    List<ParametroDTO> paramDTOList = null;
    ParamUploadVisaDTO parametros = null;
    try {
      daoCommon = CommonsDaoFactory.getInstance().getNewEntidadDao();
      paramDTOList = daoCommon.getParametroCodGrupoDato(ConstantesPRTS.CRON_VISA_INT.toString());
      if (paramDTOList != null) {

        // PARAMETROS DE FTP DE VISA
        DataFTP dataFTP = new DataFTP();
        dataFTP.setIpHost(CommonsUtils.getCodiDato(paramDTOList,
            ConstantesPRTS.SFTP_IP_UPLOAD_INCOMING_VISA_INT.toString()));
        dataFTP.setPort(Integer.parseInt(CommonsUtils.getCodiDato(paramDTOList,
            ConstantesPRTS.SFTP_PORT_UPLOAD_INCOMING_VISA_INT.toString())));
        dataFTP.setUser(CommonsUtils.getCodiDato(paramDTOList,
            ConstantesPRTS.SFTP_USER_UPLOAD_INCOMING_VISA_INT.toString()));
        dataFTP.setPassword(CommonsUtils.getCodiDato(paramDTOList,
            ConstantesPRTS.SFTP_PASS_UPLOAD_INCOMING_VISA_INT.toString()));

        // PARAMETROS DE PROCESO DE SUBIDA INCOMING A VISA
        parametros = new ParamUploadVisaDTO();

        // RUTA DE FOLDER EN SERVER sbpay DONDE SE ENCUENTRAN INCOMING DE VISA A SUBIR A FTP
        parametros.setPathVisaInc(CommonsUtils.getCodiDato(paramDTOList,
            ConstantesPRTS.PATH_VISA_INT_UP_INC.toString()));

        // RUTA EN FTP DE VISA DONDE SE DEJARAN LOS ARCHIVOS A SUBIR
        parametros.setFtpPathVisaOut(CommonsUtils.getCodiDato(paramDTOList,
            ConstantesPRTS.PATH_IN_UPLD_INCOMING_VISA_INT.toString()));

        // RUTA EN SERVIDOR sbpay DONDE SE DEJARAN LOS ARCHIVOS CORRECTAMENTE SUBIDOS A VISA
        parametros.setPathVisaBkp(CommonsUtils.getCodiDato(paramDTOList,
            ConstantesPRTS.PATH_OUT_VISA_INT_BKP.toString()));

        // RUTA EN SERVIDOR sbpay DONDE SE DEJARAN LOS ARCHIVOS QUE NO SE PUDIERON SUBIR A VISA
        parametros.setPathVisaError(CommonsUtils.getCodiDato(paramDTOList,
            ConstantesPRTS.PATH_OUT_VISA_INT_ERROR.toString()));

        // FORMATO DE NOMBRE DE ARCHIVO INCOMING DE VISA
        parametros.setFilenameVisa(CommonsUtils.getCodiDato(paramDTOList,
            ConstantesPRTS.FORMAT_FILENAME_INC_VISA_INT.toString()));

        // FORMATO DE EXTENSION DE ARCHIVO INCOMING DE VISA
        parametros.setExtVisaFile(CommonsUtils.getCodiDato(paramDTOList,
            ConstantesPRTS.FORMAT_EXTNAME_INC_VISA_INT.toString()));
        
        parametros.setFormatExtNameIncVisaCrt(CommonsUtils.getCodiDato(paramDTOList,
            ConstantesPRTS.FORMAT_EXTNAME_INC_VISA_INT_CTR.toString()));
        
        parametros.setDataFTP(dataFTP);
        
        //LOG.info("parametros: "+parametros);
        
      } else {
        parametros = null;
      }
    } catch (SQLException e) {
      throw new AppException(MsgErrorSQL.ERROR_QUERY_PRTS_UPLOAD.toString(), e);
    } finally {
      try {
        if (daoCommon != null) {
          daoCommon.close();
        }
      } catch (SQLException e) {
        throw new AppException(MsgErrorSQL.ERROR_MAIL_CLOSE_UPLOAD.toString(), e);
      }
    }
    return parametros;

  }

}
