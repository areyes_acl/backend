package com.sbpay.sgi.cron.srv.upload;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dto.ConfigFile;
import com.sbpay.sgi.cron.dto.DataFTP;
import com.sbpay.sgi.cron.dto.ParamUploadCntbleDTO;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.dto.TblCronLogDTO;
import com.sbpay.sgi.cron.enums.ConstantesPRTS;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.FilterTypeSearch;
import com.sbpay.sgi.cron.enums.LogBD;
import com.sbpay.sgi.cron.enums.MsgErrorFile;
import com.sbpay.sgi.cron.enums.MsgErrorMail;
import com.sbpay.sgi.cron.enums.MsgErrorSQL;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.enums.StatusProcessType;
import com.sbpay.sgi.cron.srv.utils.ClientFTPSrv;
import com.sbpay.sgi.cron.srv.utils.CommonSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;
import com.sbpay.sgi.cron.utils.generals.DateUtils;
import com.sbpay.sgi.cron.validation.ProcessValidator;
import com.sbpay.sgi.cron.validation.impl.ProcessValidatorImpl;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2018, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public final class UploadAsientosCntblAvanceSrv extends CommonSrv {
    
    /** The Constant LOGGER. */
    private static final Logger LOGGER = Logger
            .getLogger( UploadAsientosCntblAvanceSrv.class );
    /** The single instance. */
    private static UploadAsientosCntblAvanceSrv singleINSTANCE = null;
    
    private final ProcessCodeType PROCESO_ACTUAL = ProcessCodeType.SUBIDA_ARCHIVO_CONTABLE_AVA;
    
    /**
     * Creates the instance.
     */
    private static void createInstance() {
        synchronized ( UploadAsientosCntblAvanceSrv.class ) {
            if ( singleINSTANCE == null ) {
                singleINSTANCE = new UploadAsientosCntblAvanceSrv();
                
            }
        }
    }
    
    /**
     * Patron singleton.
     * 
     * @return UploadAsientosCntblAvanceSrv retorna instancia del servicio.
     */
    public static UploadAsientosCntblAvanceSrv getInstance() {
        if ( singleINSTANCE == null ) {
            createInstance();
        }
        return singleINSTANCE;
    }
    
    /**
     * Metodo de restricion de sobreescritura de la clase.
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        super.clone();
        throw new CloneNotSupportedException();
    }
    
    /**
     * Constructor Privado.
     */
    private UploadAsientosCntblAvanceSrv() {
        super();
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2018, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @throws AppException
     * @since 1.X
     */
    public boolean uploadAsientosContablesAvances() throws AppException {
        
        ProcessValidator validadorProceso = new ProcessValidatorImpl();
        
        if ( validadorProceso.isActiveProcess( PROCESO_ACTUAL ) ) {
            LOGGER.info( "Proceso está activo" );
            
            ParamUploadCntbleDTO parametros = getParametrosUploadAsientosAvance();
            
            boolean flag = false;
            List<String> fileDirectory = null;
            List<String> controlFileList = null;
            List<String> fileUpload = null;
            String filenameCtr = null;
            
            // VALIDA PARAMETROS PROCESO
            if ( !validadorProceso.hasTheValidParametersForTheProcess(
                    PROCESO_ACTUAL, parametros ) ) {
                LOGGER.info( MsgProceso.MSG_ERROR_FALTAN_PARAMETROS.toString() );
                MailSendSrv.getInstance().sendMail(
                        MsgErrorMail.ALERTA_PRTS_UPLOAD_CNTBL_AVA.toString(),
                        MsgErrorMail.ALERTA_PRTS_TXT_UPLOAD_CNTBL_AVA.toString() );
                return flag;
            }
            
            // BUSCA LISTA DE ARCHIVOS CANDIDATOS EN FOLDER sbpay
            fileDirectory = getFileStartsWith(
                    parametros.getsbpayServerPath(), parametros
                            .getFormatContable().getStarWith(), parametros
                            .getFormatContable().getEndsWith() );
            LOGGER.info( "Lista de archivos que contiene formato de nombre valido para subir:  "
                    + ((fileDirectory == null )? "0" : fileDirectory.size() ));
            
            // SE BUSCA LISTA DE ARCHIVOS DE CONTROL CONTABLE EN
            // FOLDER DE sbpay
            controlFileList = retrieveFilenameCtrList(
                    parametros.getsbpayServerPath(),
                    parametros.getFormatContable() );
            
            if ( fileDirectory != null && fileDirectory.size() > 0 ) {
                
                // FILTRA LA LISTA POR ARCHIVOS QUE NO TENGAN
                // REGISTROS EN LA TABLA : TBL_LOG_CONTABLE
                fileUpload = getFileUpload( fileDirectory, parametros );
                
                LOGGER.info( "Lista de archivos que se van a subir a FTP:"
                        + fileUpload );
                if ( fileUpload != null && fileUpload.size() > 0 ) {
                    int count = 0;
                    String msj = "";
                    for ( String filename : fileUpload ) {
                        
                        try {
                            // BUSCA ARCHIVO DE CONTROL DE CONTABLE
                            filenameCtr = buscarArchivoContrl( filename,
                                    controlFileList,
                                    parametros.getFormatContable() );    
                            
                            LOGGER.info("ARCHIVO CTR CONTABLE: "+filenameCtr);
                            
                            // SUBE ARCHIVO ID A SFTP DE ONEWORLD
                            if ( subirArchivo( parametros, filename ) ) {
                                
                                LOGGER.info( "Archivo: "
                                        + filename
                                        + ", se ha subido correctamente a servidor (SFTP) = "
                                        + parametros.getDataFTP().getIpHost() );
                                                                
                                // SUBE ARCHIVO IR A SFTP DE ONEWORL
                                if ( subirArchivo( parametros, filenameCtr ) ) {
                                    
                                    LOGGER.info( "Archivo: "
                                            + filenameCtr
                                            + ", se ha subido correctamente a servidor (SFTP) = "
                                            + parametros.getDataFTP()
                                                    .getIpHost() );
                                    
                                    // MUEVE ARCHIVOS A CARPETA
                                    // RESPALDO (BKP)
                                    respaldarArchivoPorEstado( filename,
                                            filenameCtr,
                                            parametros.getsbpayServerPath(),
                                            parametros.getsbpayServerPathBkp() );
                                    
                                    // UPDATEA FLAG A 1 EN TABLA
                                    // TBL_LOG_CONTABLE
                                    actualizarLog( filename,
                                            StatusProcessType.PROCESS_SUCESSFUL );
                                    count++;
                                }
                            }
                        }
                        catch ( Exception e ) {
                            LOGGER.info( "ARCHIVO "
                                    + filename
                                    + " NO HA PODIDO SER SUBIDO  A SFTP DE ONEW" );
                            
                            // UPDATEA FLAG A -1 EN TABLA
                            // TBL_LOG_CONTABLE CON ERROR
                            actualizarLog( filename,
                                    StatusProcessType.PROCESS_ERROR );
                            msj = msj
                                    .concat(
                                            ConstantesUtil.SKIP_LINE.toString() )
                                    .concat( "-)" ).concat( filename );
                            
                            // MUEVE ARCHIVOS A CARPETA RESPALDO
                            // (ERROR)
                            respaldarArchivoPorEstado( filename, filenameCtr,
                                    parametros.getsbpayServerPath(),
                                    parametros.getsbpayServerPathError() );
                            
                        }
                    }
                    if ( count == fileUpload.size() ) {
                        flag = true;
                    }
                    else {
                        LOGGER.info( "Algunos archivos de Asientos Contables de avances con tranferencia no pudieron ser cargados" );
                        MailSendSrv
                                .getInstance()
                                .sendMail(
                                        MsgErrorMail.ALERTA_FILE_UPLOAD_CNTBL_AVA
                                                .toString(),
                                        MsgErrorMail.ALERTA_UPLOAD_FILE_CNTBL_OWORLD_AVA
                                                .toString().concat( msj ) );
                    }
                    
                }
                else {
                    LOGGER.info( "Todo archivos de Asientos Contables para upload estan procesados" );
                    MailSendSrv
                            .getInstance()
                            .sendMail(
                                    MsgErrorMail.ALERTA_FILE_PROCESSED_UPLOAD
                                            .toString(),
                                    MsgErrorMail.ALERTA_FILE_PROCESSED_UPLOAD_TXT_CNTBL_AVA
                                            .toString() );
                }
            }
            else {
                LOGGER.info( "No existen archivos de Asientos Contables de avances con transferencia que subir" );
                MailSendSrv.getInstance().sendMail(
                        MsgErrorMail.ALERTA_FILE_UPLOAD.toString(),
                        MsgErrorMail.ALERTA_FILE_UPLOAD_TXT_CNTBL_AVA.toString() );
            }
            
            return flag;
            
        }
        else {
            String asunto = MsgProceso.MSG_PROCESO_DESACTIVADO_ASUNTO
                    .toString().replace( ":?:",
                            PROCESO_ACTUAL.getCodigoProceso() );
            String mensaje = MsgProceso.MSG_PROCESO_DESACTIVADO_BODY.toString()
                    .replace( ":?:", PROCESO_ACTUAL.getCodigoProceso() );
            MailSendSrv.getInstance().sendMail( asunto, mensaje );
        }
        return false;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2018, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param parametros
     * @param filename
     * @return
     * @throws AppException
     * @since 1.X
     */
    private Boolean subirArchivo( ParamUploadCntbleDTO parametros,
            String filename ) throws AppException {
        
        return ClientFTPSrv.getInstance().uploadFileSFTP(
                parametros.getDataFTP(), parametros.getsbpayServerPath(),
                parametros.getSftpPathIn(), filename );
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2018, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Metodo que respalda archivo de control y archivo contable en la
     * carpeta correspondiente, segun el estado (error o a backup).
     * 
     * @param filename
     * @param filenameCtr
     * @param folderTemp
     * @param folderDest
     * @since 1.X
     */
    private void respaldarArchivoPorEstado( String filename,
            String filenameCtr, String folderTemp, String folderDest ) {
        
        // MUEVE ARCHIVO CONTABLE
        CommonsUtils.moveFile( folderTemp + filename, folderDest + filename );
        LOGGER.info( "Archivo : " + filename
                + ", ha sido movido correctamente a la carpeta destino : "
                + folderDest );
        
        // MUEVE ARCHIVO CONTROL CONTABLE
        if ( filenameCtr != null ) {
            CommonsUtils.moveFile( folderTemp + filenameCtr, folderDest
                    + filenameCtr );
            LOGGER.info( "Archivo : " + filenameCtr
                    + ", ha sido movido correctamente a la carpeta destino : "
                    + folderDest );
        }
    }
    
    /**
     * Metodo busca los archivos que van a ser subidos al servidor FTP
     * tbk.
     * 
     * @param fileDirectory
     *            Archivos candidatos.
     * @return Lista de archivos que deben ser prosados.
     * @throws AppException
     *             App Exception.
     */
    private List<String> getFileUpload( final List<String> fileDirectory,
            ParamUploadCntbleDTO paramUploadCntbleDTO ) throws AppException {
        
        String dateFile = null;
        List<String> fileCarga = null;
        // UploadDao daoUpload = null;
        CommonsDao daoCommons = null;
        String fileAux = null;
        // CronLogDTO cronLogDTO = null;
        TblCronLogDTO dto = null;
        TblCronLogDTO dtoAux = new TblCronLogDTO();
        try {
            fileCarga = new ArrayList<String>();
            daoCommons = CommonsDaoFactory.getInstance().getNewEntidadDao();
            
            /* Lista de archivos que se pueden procesar. */
            for ( String fileName : fileDirectory ) {
                // formato PREFIJOmmdds.dat
                fileAux = fileName.replace( paramUploadCntbleDTO
                        .getFormatContable().getStarWith(), "" );
                
                // Rescata fecha archivo de asientos contables
                dateFile = DateUtils.getDateFileAsientosCntbl( fileAux );
                if ( dateFile != null ) {
                    dtoAux.setFecha( dateFile );
                    dto = daoCommons.getTblCronLogByType( dtoAux,
                            LogBD.TABLE_LOG_CONTABLE.getTable(),
                            FilterTypeSearch.FILTER_BY_DATE );
                    if ( dto != null
                            && dto.getFileFlag() != null
                            && StatusProcessType.PROCESS_PENDING.getValue()
                                    .equalsIgnoreCase( dto.getFileFlag() ) ) {
                        fileCarga.add( fileName );
                    }
                }
                else {
                    LOGGER.info( "Archivo no cumple con el formato para obtener la fecha al procesar los de Asientos Contables "
                            + fileName );
                    /*
                     * Se mueve archivo a path de error Asientos
                     * Contables
                     */
                    CommonsUtils.moveFile(
                            paramUploadCntbleDTO.getsbpayServerPath()
                                    + fileName,
                            paramUploadCntbleDTO.getsbpayServerPathError() );
                    /*
                     * Envia correo con el nombre del archivo mal
                     * formateado
                     */
                    MailSendSrv.getInstance().sendMail(
                            MsgErrorFile.ERROR_FILE_FORMAT_CNTBL.toString(),
                            MsgErrorFile.ERROR_FILE_FORMAT_TXT.toString()
                                    .concat( fileName ) );
                    
                }
            }
            return fileCarga;
        }
        catch ( SQLException e ) {
            throw new AppException(
                    MsgErrorSQL.ERROR_QUERY_PROCESS_UPLOAD_CNTBL.toString(), e );
        }
        finally {
            try {
                if ( daoCommons != null ) {
                    daoCommons.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException(
                        MsgErrorSQL.ERROR_QUERY_PROCESS_CLOSE_UPLOAD_CNTBL
                                .toString(),
                        e );
            }
        }
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2018, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param filename
     * @since 1.X
     */
    private void actualizarLog( String filename, StatusProcessType status ) {
        CommonsDao commons = null;
        try {
            commons = CommonsDaoFactory.getInstance().getNewEntidadDao();
            TblCronLogDTO dto = new TblCronLogDTO();
            dto.setFilename( filename );
            dto.setFileFlag( status.getValue() );
            if ( commons.updateTblCronLogByType( dto,
                    LogBD.TABLE_LOG_CONTABLE.getTable(),
                    FilterTypeSearch.FILTER_BY_NAME ) ) {
                LOGGER.info( "SE UPDATEA CORRECTAMENTE TABLA LOG _PARA FILE  :"
                        + filename );
                commons.endTx();
            }
            else {
                LOGGER.warn( "NO SE UPDATEA TABLA "
                        + LogBD.TABLE_LOG_CONTABLE.getTable() + " PARA FILE  :"
                        + filename );
                commons.rollBack();
            }
        }
        catch ( SQLException e ) {
            LOGGER.warn( "NO SE HA PODIDO REALIZAR UPDATE, PARA FILE  :"
                    + filename, e );
        }
        finally {
            
            if ( commons != null ) {
                try {
                    commons.close();
                }
                catch ( SQLException e ) {
                    e.printStackTrace();
                }
            }
        }
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/XX/2018, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @param filename
     * @param controlFileList
     * @return
     * @since 1.X
     */
    private String buscarArchivoContrl( String filename,
            List<String> controlFileList, ConfigFile config ) {
        
        for ( String fileCTR : controlFileList ) {
            String fileAuxCTR = fileCTR.replace( config.getStarWithCtr(), "" )
                    .replace( "." + config.getEndsWithCtr(), "" );
            String fileAuxiNC = filename.replace( config.getStarWith(), "" )
                    .replace( "." + config.getEndsWith(), "" );
            
            if ( fileAuxCTR.equals( fileAuxiNC ) ) {
                return fileCTR;
            }
        }
        
        return null;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/XX/2018, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * * Metodo busca los parametros del proceso Upload Asientos
     * Contable.
     * 
     * 
     * @return
     * @throws AppException
     * @since 1.X
     */
    private ParamUploadCntbleDTO getParametrosUploadAsientosAvance()
            throws AppException {
        CommonsDao daoCommon = null;
        ParamUploadCntbleDTO paramUploadCntbleDTO = null;
        List<ParametroDTO> paramDTOList = null;
        try {
            daoCommon = CommonsDaoFactory.getInstance().getNewEntidadDao();
            paramDTOList = daoCommon
                    .getParametroCodGrupoDato( ConstantesPRTS.CRON_TBK
                            .toString() );
            if ( paramDTOList != null ) {
                // SETEA DATOS DE FTP
                DataFTP dataFTP = new DataFTP();
                dataFTP.setIpHost( CommonsUtils.getCodiDato( paramDTOList,
                        ConstantesPRTS.SFTP_IP_UPLD_ACCOUNT_FILE_AVA
                                .toString() ) );
                try {
                    dataFTP.setPort( Integer.valueOf( CommonsUtils.getCodiDato(
                            paramDTOList,
                            ConstantesPRTS.SFTP_PORT_UPLD_ACCOUNT_FILE_AVA
                                    .toString() ) ) );
                }
                catch ( Exception e ) {
                    LOGGER.error( e.getMessage(), e );
                    dataFTP.setPort( 21 );
                }
                dataFTP.setUser( CommonsUtils.getCodiDato( paramDTOList,
                        ConstantesPRTS.SFTP_USER_UPLD_ACCOUNT_FILE_AVA
                                .toString() ) );
                dataFTP.setPassword( CommonsUtils.getCodiDato( paramDTOList,
                        ConstantesPRTS.SFTP_PASS_UPLD_ACCOUNT_FILE_AVA
                                .toString() ) );
                
                                
                // SETEA PARAMETROS DEL CRON
                paramUploadCntbleDTO = new ParamUploadCntbleDTO();
                
               
                
                // PATH DE FTP DONDE SE SUBIRA EL ARCHIVO DE ASIENTOS
                // CONTABLES
                paramUploadCntbleDTO.setSftpPathIn( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.PATH_IN_UPLD_ACCOUNT_FILE_AVA
                                .toString() ) );
                
                // PATH DE SERVIDOR DE sbpay DONDE SE BUSCARAN EL/LOS
                // ARCHIVO/S CONTABLE A SUBIR
                paramUploadCntbleDTO.setsbpayServerPath( CommonsUtils
                        .getCodiDato( paramDTOList,
                                ConstantesPRTS.PATH_TRX_CONTABLE_AVA.toString() ) );
                
                // PATH DE SERVIDOR DE sbpay DE ERROR
                paramUploadCntbleDTO.setsbpayServerPathError( CommonsUtils
                        .getCodiDato( paramDTOList,
                                ConstantesPRTS.PATH_TRX_CONTABLE_ERROR_AVA
                                        .toString() ) );
                
                // PATH DE SERVIDOR DE sbpay DE BACKUP
                paramUploadCntbleDTO
                        .setsbpayServerPathBkp( CommonsUtils
                                .getCodiDato( paramDTOList,
                                        ConstantesPRTS.PATH_TRX_CONTABLE_BKP_AVA
                                                .toString() ) );
                
                // FOMATO STARWITH DE NOMBRE DE ARCHIVO CONTABLE
                paramUploadCntbleDTO.getFormatContable().setStarWith(
                        CommonsUtils.getCodiDato( paramDTOList,
                                ConstantesPRTS.FORMAT_FILE_ACCOUNT_FILE_AVA
                                        .toString() ) ); // ID
                
                // FORMATO DE EXTENSION DE ARCHIVO CONTABLE
                paramUploadCntbleDTO.getFormatContable().setEndsWith(
                        CommonsUtils.getCodiDato( paramDTOList,
                                ConstantesPRTS.FORMAT_EXT_ACCONT_FILE_AVA
                                        .toString() ) );
                
                // FORMATO "STARWITH" DE ARCHIVO DE CONTROL CONTABLE
                paramUploadCntbleDTO.getFormatContable().setStarWithCtr(
                        CommonsUtils.getCodiDato( paramDTOList,
                                ConstantesPRTS.FORMAT_FILE_ACCOUNT_FILE_CTR_AVA
                                        .toString() ) );
                
                // FORMATO "ENDSWITH" DE ARCHIVO DE CONTROL CONTABLE
                paramUploadCntbleDTO.getFormatContable().setEndsWithCtr(
                        CommonsUtils.getCodiDato( paramDTOList,
                                ConstantesPRTS.FORMAT_EXT_ACCOUNT_FILE_CTR_AVA
                                        .toString() ) );
                
                paramUploadCntbleDTO.setDataFTP( dataFTP );
            }
            return paramUploadCntbleDTO;
            
        }
        catch ( SQLException e ) {
            LOGGER.error( MsgErrorSQL.ERROR_QUERY_PRTS_UPLOAD_CNTBL.toString(),
                    e );
            throw new AppException(
                    MsgErrorSQL.ERROR_QUERY_PRTS_UPLOAD_CNTBL.toString(), e );
        }
        finally {
            try {
                if ( daoCommon != null ) {
                    daoCommon.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException(
                        MsgErrorSQL.ERROR_QUERY_PRTS_UPLOAD_CLOSE_CNTBL
                                .toString(),
                        e );
            }
        }
    }
    
}
