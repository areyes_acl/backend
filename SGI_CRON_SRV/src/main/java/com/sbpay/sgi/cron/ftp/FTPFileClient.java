package com.sbpay.sgi.cron.ftp;

import java.util.List;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dto.DataFTP;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 18/1/2016, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public interface FTPFileClient {
    
    public Boolean upload( DataFTP dataFTP, String pathOrigen,
            String pathDestino, String filename ) throws AppException;
    
    public Boolean download( DataFTP dataFTP, String pathFtpOrigen,
            String pathDestino, String fileName ) throws AppException;
    
    public List<String> listFiles( DataFTP dataFtp, String folderPath )
            throws AppException;    
}
