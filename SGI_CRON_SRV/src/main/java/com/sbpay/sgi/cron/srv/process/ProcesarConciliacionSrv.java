package com.sbpay.sgi.cron.srv.process;

import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.TrxBiceDao;
import com.sbpay.sgi.cron.dao.TrxBiceDaoFactory;
import com.sbpay.sgi.cron.dao.TrxConciliacionAvancesDaoFactory;
import com.sbpay.sgi.cron.dao.TrxConciliarAvancesDao;
import com.sbpay.sgi.cron.dto.LogTransaccionBiceDTO;
import com.sbpay.sgi.cron.enums.MsgErrorProcessFileBice;
import com.sbpay.sgi.cron.enums.MsgErrorSQL;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.srv.utils.CommonSrv;

public class ProcesarConciliacionSrv extends CommonSrv {

	/** The Constant LOGGER. */
	private static final Logger LOG = Logger
			.getLogger(ProcesarConciliacionSrv.class.getName());
	/** The single instance. */
	private static ProcesarConciliacionSrv singleINSTANCE = null;

	private final ProcessCodeType PROCESO_ACTUAL = ProcessCodeType.PROCESAR_CONCILIACION;

	/**
	 * Creates the instance.
	 */
	private static void createInstance() {
		synchronized (ProcesarConciliacionSrv.class) {
			if (singleINSTANCE == null) {
				singleINSTANCE = new ProcesarConciliacionSrv();
			}
		}
	}

	/**
	 * Patron singleton.
	 * 
	 * @return ProcesarBiceSrv retorna instancia del servicio.
	 */
	public static ProcesarConciliacionSrv getInstance() {
		if (singleINSTANCE == null) {
			createInstance();
		}
		return singleINSTANCE;
	}

	/**
	 * Metodo de restricion de sobreescritura de la clase.
	 */
	@Override
	protected Object clone() throws CloneNotSupportedException {
		super.clone();
		throw new CloneNotSupportedException();
	}

	/**
	 * Constructor Privado.
	 */
	private ProcesarConciliacionSrv() {
		super();
	}

	public void processConciliacion() throws AppException {
		try {
			LOG.info("Ingresa a proceso de Conciliacion");
			saveData();

		} catch (AppException e) {
			// ERROR AL PROCESAR CONCILIACION
			LOG.error(MsgErrorProcessFileBice.ERROR_IN_PROCESS_CONC.toString(),
					e);
		}

	}

	private void saveData() throws AppException {
		TrxConciliarAvancesDao procesarDAO = null;
		try {
			LOG.info("GUARDAR DATOS");

			procesarDAO = TrxConciliacionAvancesDaoFactory.getInstance()
					.getNewEntidadDao();
			procesarDAO.callSPCargaConciliacionAvances();
		} catch (SQLException e) {
			LOG.info(e.getMessage());
			throw new AppException(MsgErrorSQL.ERROR_SAVE_CONCILIAR.toString(), e);
		} finally {
			try {
				if (procesarDAO != null) {
					procesarDAO.close();
				}
			} catch (SQLException e) {
				LOG.error(e.getMessage(), e);
			}

		}
	}

}
