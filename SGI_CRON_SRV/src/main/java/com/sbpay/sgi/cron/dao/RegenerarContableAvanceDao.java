package com.sbpay.sgi.cron.dao;

import java.sql.SQLException;
import java.util.List;






import com.sbpay.sgi.cron.dao.Dao;
import com.sbpay.sgi.cron.dto.ReContableAvanceLogDTO;
import com.sbpay.sgi.cron.dto.TmpExportContableDTO;

public interface RegenerarContableAvanceDao extends Dao{
	
	List<TmpExportContableDTO> obtenerInformacionCnblt(String fechaContable) throws SQLException;
	
	boolean cargarInformacionContableAvance(String fechaContable) throws SQLException;
	
	List<ReContableAvanceLogDTO> getDataRegenerar() throws SQLException;
	
	void updateDataContableAvances(Integer sidContable) throws SQLException;

	void clearTmpContable(String fechaContable)throws SQLException;
	
	void savehistoriaContable(Integer sidContable)throws SQLException;
	
	void updateTablelog(Integer sidContable)throws SQLException;
}
