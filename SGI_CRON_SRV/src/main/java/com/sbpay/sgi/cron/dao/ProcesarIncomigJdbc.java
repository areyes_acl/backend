package com.sbpay.sgi.cron.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import oracle.jdbc.OracleCallableStatement;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.dao.BaseDao;
import com.sbpay.sgi.cron.dto.Registro00DTO;
import com.sbpay.sgi.cron.dto.Registro00OutVisaCobroCargoDTO;
import com.sbpay.sgi.cron.dto.Registro01DTO;
import com.sbpay.sgi.cron.dto.Registro05DTO;
import com.sbpay.sgi.cron.dto.Registro07DTO;
import com.sbpay.sgi.cron.dto.Transaccion;
import com.sbpay.sgi.cron.dto.TransaccionOutVisaCobroCargo;
import com.sbpay.sgi.cron.enums.ConstantesBD;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.utils.generals.DateUtils;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 19/11/2015, (ACL) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SA.</B>
 */
public class ProcesarIncomigJdbc extends BaseDao<ProcesarIncomingDao> implements
        ProcesarIncomingDao {
    
    /** VARIABLE PARA EL LOGER */
    private static final Logger LOGGER = Logger
            .getLogger( ProcesarIncomigJdbc.class );
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 19/11/2015, (ACL & sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @param connection
     * @since 1.X
     */
    public ProcesarIncomigJdbc( Connection connection ) {
        super( connection );
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 19/11/2015, (ACL & sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @param connection
     * @since 1.X
     */
    @Override
    public boolean saveTransaction( Transaccion transaccion, Integer sidOperador)
            throws SQLException {
        PreparedStatement stmt = null;
        
        try {
            StringBuilder str = new StringBuilder()
                    .append( "INSERT INTO " )
                    .append( ConstantesBD.ESQUEMA.toString() )
                    .append( ".TMP_INCOMING  " )
                    .append( " (SID, MIT, NRO_TARJETA, codigo_procesamiento," )
                    .append(
                            " monto_transac, monto_conciliacion, monto_facturacion, tasa_convenio_conciliacion," )
                    .append(
                            " tasa_convenio_facturacion, fecha_hr_trasac, fecha_vencimiento_tarjeta, " )
                    .append(
                            " cod_datos_punto_servicio , nro_secuencia_tarjeta, codigo_funcion, cod_motivo_mensaje, " )
                    .append(
                            " mcc , montos_originales, datos_referencia_adquirente, cod_identificacion_adquirente, " )
                    .append(
                            " cod_ident_inst_envia , nro_referencia_recuperacion, codigo_autorizacion, codigo_servicio, " )
                    .append(
                            " ident_term_acep_tarjeta , cod_ident_acep_tarjeta, nombre_ubic_acep_tarjeta, datos_adicionales_1, " )
                    .append(
                            " cod_moneda_transac , cod_moneda_conciliacion, cod_moneda_facturacion, montos_adicionales, " )
                    .append(
                            " icc , datos_adicionales_2, ident_ciclo_duracion_transac, nro_mensaje, " )
                    .append(
                            " registro_de_datos , fecha_accion, cod_ident_inst_dest_transac, cod_ident_inst_orig_transac, " )
                    .append(
                            " datos_ref_emisor  , cod_ident_inst_recibe, monto_recargo_x_conver_moneda, datos_adicionales_3, " )
                    .append(
                            " datos_adicionales_4 , datos_adicionales_5, p0023, p0052, " )
                    .append( " p0148 , p0158, p0165, p1000, " )
                    .append( " p1001 , p1003, p1004, p1005, " )
                    .append( " p1006 , p1007, p1008, p1009, " )
                    .append( " p1010 , p1011, p1012, p0105, " )
                    .append( " p012 , p0122, p0191, p0201, " )
                    .append( " p0306 , p0005, p0025, p0026, " )
                    .append( " p0044 , p0057, p0071, p0137, " )
                    .append( " p0146 , p0149, p0159, p0170, " )
                    .append( " p0176 , p0177, p0189, p0228, " )
                    .append( " p0230 , p0262, p0264, p0280, " )
                    .append( " p0300 , p0301, p0302, p0358, " )
                    .append( " p0359 , p0370, p0372, p0374, " )
                    .append( " p0378 , p0380, p0381, p0384, " )
                    .append( " p0390 , p0391, p0392, p0393, " )
                    .append( " p0394 , p0395, p0396, p0400, " )
                    .append( " p0401 , p0402, OPERADOR) " )
                    .append( " values (SEQ_TMP_INCOMING.nextval, ?, ?, ? , " )
                    .append( " ?, ?, ?, ? ," ).append( " ?, ?, ?, ? ," )
                    .append( " ?, ?, ?, ? ," ).append( " ?, ?, ?, ? ," )
                    .append( " ?, ?, ?, ? ," ).append( " ?, ?, ?, ? ," )
                    .append( " ?, ?, ?, ? ," ).append( " ?, ?, ?, ? ," )
                    .append( " ?, ?, ?, ? ," ).append( " ?, ?, ?, ? ," )
                    .append( " ?, ?, ?, ? ," ).append( " ?, ?, ?, ? ," )
                    .append( " ?, ?, ?, ? ," ).append( " ?, ?, ?, ? ," )
                    .append( " ?, ?, ?, ? ," ).append( " ?, ?, ?, ? ," )
                    .append( " ?, ?, ?, ? ," ).append( " ?, ?, ?, ? ," )
                    .append( " ?, ?, ?, ? ," ).append( " ?, ?, ?, ? ," )
                    .append( " ?, ?, ?, ? ," ).append( " ?, ?, ?, ? ," )
                    .append( " ?, ?, ?, ? ," ).append( " ?, ?, ?, ? ," )
                    .append( " ?, ?, ?, ? ," ).append( " ?, ?, ?, ? ," )
                    .append( " ?, ?)" );
            
            this.getConnection().setAutoCommit( false );
            stmt = this.getConnection().prepareStatement( str.toString() );
            
            setStringNull(
                    ( ( Registro00DTO ) transaccion.getRegistro00() )
                            .getCodigoTransaccion(),
                    1, stmt );
            setStringNull(
                    ( ( Registro00DTO ) transaccion.getRegistro00() )
                            .getNumeroTarjeta(),
                    2, stmt );
            setStringNull(
                    ( ( Registro00DTO ) transaccion.getRegistro00() )
                            .getUsagCode(),
                    3, stmt );
            setStringNull(
                    ( ( Registro00DTO ) transaccion.getRegistro00() )
                            .getMontoFuen(),
                    4, stmt );
            setStringNull(
                    ( ( Registro00DTO ) transaccion.getRegistro00() )
                            .getMontoFuen(),
                    5, stmt );
            setStringNull(
                    ( ( Registro00DTO ) transaccion.getRegistro00() )
                            .getMontoFuen(),
                    6, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 7, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 8, stmt );
            setStringNull(
                    ( DateUtils.getStringDateWithValidationProcess( ( ( Registro00DTO ) transaccion
                            .getRegistro00() ).getFechaComp(), DateUtils.FECHA_NORMAL ) ), 9, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 10, stmt );
            setStringNull(
                    ( ( Registro00DTO ) transaccion.getRegistro00() )
                            .getPosEntryMode(),
                    11, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 12, stmt );
            setStringNull(
                    ( obtieneCodigoTransaccion(
                            ( ( Registro00DTO ) transaccion.getRegistro00() )
                                    .getCodRazon(),
                            ( ( Registro00DTO ) transaccion.getRegistro00() )
                                    .getTransCode() ) ), 13, stmt );
            setStringNull(
                    ( ( Registro00DTO ) transaccion.getRegistro00() )
                            .getCodRazon(),
                    14, stmt );
            setStringNull(
                    ( ( Registro00DTO ) transaccion.getRegistro00() )
                            .getRubroComer(),
                    15, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 16, stmt );
            setStringNull(
                    ( ( Registro00DTO ) transaccion.getRegistro00() )
                            .getNumeroReferencia(),
                    17, stmt );
            setStringNull(
                    ( ( Registro00DTO ) transaccion.getRegistro00() )
                            .getAcqMembId(),
                    18, stmt );
            setStringNull(
                    ( ( Registro01DTO ) transaccion.getRegistro01() )
                            .getRubroTransbank(),
                    19, stmt ); // REGISTRO01 - 30
            setStringNull(
                    ( ( Registro01DTO ) transaccion.getRegistro01() )
                            .getChargRefN(),
                    20, stmt ); // REGISTRO01 - 05
            setStringNull(
                    ( ( Registro00DTO ) transaccion.getRegistro00() )
                            .getCodAutor(),
                    21, stmt );
            setStringNull(
                    ( ( Registro00DTO ) transaccion.getRegistro00() )
                            .getPosTerminalCapability(),
                    22, stmt );
            setStringNull(
                    ( ( Registro01DTO ) transaccion.getRegistro01() )
                            .getTerminalId(),
                    23, stmt );// REGISTRO01
            // REGISTRO01
            setStringNull(
                    ( ( Registro01DTO ) transaccion.getRegistro01() )
                            .getCardAcceptorId(),
                    24, stmt );
            setStringNull(
                    ( ( Registro00DTO ) transaccion.getRegistro00() )
                            .getNombreUbicAcepTarjeta(),
                    25, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 26, stmt );
            setStringNull(
                    ( ( Registro00DTO ) transaccion.getRegistro00() )
                            .getMonedaFuen(),
                    27, stmt );
            setStringNull(
                    ( ( Registro00DTO ) transaccion.getRegistro00() )
                            .getMonedaFuen(),
                    28, stmt );
            setStringNull(
                    ( ( Registro00DTO ) transaccion.getRegistro00() )
                            .getMonedaFuen(),
                    29, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 30, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 31, stmt );
            // REGISTRO 05 COMPLETO
            setStringNull(
                    ( transaccion.getRegistro05() != null ) ? ( ( Registro05DTO ) transaccion.getRegistro05() ).getAll05Register()
                            : "", 32, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 33, stmt );
            // CORRELATIVO DE LECTURA EL NRO DE TRANSACCION QUE ESTOY
            // LEYENDO
            setStringNull(
                    String.valueOf( transaccion.getNumeroTransaccion() ), 34,
                    stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 35, stmt );
            setStringNull(
                    ( ( Registro00DTO ) transaccion.getRegistro00() )
                            .getFechaProc(),
                    36, stmt );
            setStringNull(
                    ( ( Registro00DTO ) transaccion.getRegistro00() )
                            .getCodIdentDestTransc(),
                    37, stmt );
            setStringNull(
                    ( ( Registro00DTO ) transaccion.getRegistro00() ).getOfic(),
                    38, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 39, stmt );
            // BIN 4 PRIMEROS DIGITOS de tarjeta
            setStringNull(
                    ( ( Registro00DTO ) transaccion.getRegistro00() )
                            .getBinTarjeta(),
                    40, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 41, stmt );
            String allRegister07 = ( transaccion.getRegistro07() != null ) ? ( ( Registro07DTO ) transaccion
                    .getRegistro07() ).getAllRegister07() : "";
            // 100 primeros bytes del registros completo 07
            setStringNull(
                    ( allRegister07.length() > 100 ) ? allRegister07.substring(
                            0, 100 ) : allRegister07, 42, stmt );
            // Restantes 70 bytes del registro 07
            setStringNull(
                    ( allRegister07.length() > 100 ) ? allRegister07.substring(
                            100, allRegister07.length() ) : "", 43, stmt );
           // setStringNull( ConstantesUtil.WHITE.toString(), 44, stmt );
            setStringNull( ( ( Registro01DTO ) transaccion.getRegistro01() ).getMensaje() , 44, stmt );
            // REG01 -> 13
            setStringNull(
                    ( ( Registro01DTO ) transaccion.getRegistro01() )
                            .getIndicatorTransaction(),
                    45, stmt );
            // REG01 -> 6s
            setStringNull(
                    ( ( Registro01DTO ) transaccion.getRegistro01() )
                            .getDocumInd(),
                    46, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 47, stmt );
            // SE AGREGA CAMPO 19 DEL REG 00 AL P0158
            setStringNull(
                    ( ( Registro00DTO ) transaccion.getRegistro00() )
                            .getRubroComer(),
                    48, stmt );
            // REG01 -> 24^28
            setStringNull(
                    ( ( Registro01DTO ) transaccion.getRegistro01() )
                            .getP0165(),
                    49, stmt );
            
            //p:1000 = fecha_de_compensacion Se setea segun condicion fecha compra > fecha actual (P1000)
            setStringNull(
                    ( DateUtils.getStringDateWithValidationProcess( ( ( Registro00DTO ) transaccion
                            .getRegistro00() ).getFechaDCompensacion(), DateUtils.FECHA_FUTURA ) ), 50,
                    stmt );
            setStringNull(
                    ( ( Registro00DTO ) transaccion.getRegistro00() )
                            .getTipoVenta(),
                    51, stmt );
            setStringNull(
                    ( ( Registro00DTO ) transaccion.getRegistro00() )
                            .getZipCodeComer(),
                    52, stmt );
            setStringNull(
                    ( ( Registro00DTO ) transaccion.getRegistro00() )
                            .getNumCuo(),
                    53, stmt );
            // REG01 -> 12
            setStringNull(
                    ( ( Registro01DTO ) transaccion.getRegistro01() )
                            .getValorCuota(),
                    54, stmt );
            // REG01 -> 31
            setStringNull(
                    ( ( Registro01DTO ) transaccion.getRegistro01() )
                            .getTasaEECC(),
                    55, stmt );
            setStringNull(
                    ( ( Registro01DTO ) transaccion.getRegistro01() )
                            .getComisionCic(),
                    56, stmt );// REG01 -> 15
            setStringNull(
                    ( ( Registro01DTO ) transaccion.getRegistro01() )
                            .getRubroTransbank(),
                    57, stmt );// REG01 -> 30
            setStringNull(
                    ( ( Registro01DTO ) transaccion.getRegistro01() )
                            .getOrigen(),
                    58, stmt );// REG01 -> 29
            setStringNull(
                    ( ( Registro00DTO ) transaccion.getRegistro00() )
                            .getCodMunic(),
                    59, stmt );
            // RUT sbpay OBTENER DESDE BD
            setStringNull( transaccion.getRutsbpay(), 60, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 61, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 62, stmt );
            // ¿¿CONSULTAR NO SE HA OBTENIDO RESPUESTA???
            setStringNull( ConstantesUtil.WHITE.toString(), 63, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 64, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 65, stmt );
            // ?¿?CONSULTAR NO SE HA OBTENIDO RESPUESTA¿?
            setStringNull( ConstantesUtil.WHITE.toString(), 66, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 67, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 68, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 69, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 70, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 71, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 72, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 73, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 74, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 75, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 76, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 77, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 78, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 79, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 80, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 81, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 82, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 83, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 84, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 85, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 86, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 87, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 88, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 89, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 90, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 91, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 92, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 93, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 94, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 95, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 96, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 97, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 98, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 99, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 100, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 101, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 102, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 103, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 104, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 105, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 106, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 107, stmt );
            setStringNull( ConstantesUtil.WHITE.toString(), 108, stmt );
            stmt.setInt(109, sidOperador);
            
            
            return ( stmt.executeUpdate() > 0 );
        }
        finally {
            if ( stmt != null ) {
                stmt.close();
            }
        }
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 - 2/12/2015, (ACL) - versión inicial
     * </ul>
     * <p>
     * VALIDA EL SI EL CODIGO DE RAZON ES MAYOR QUE 0
     * 
     * @param codRazon
     * @param transcode
     * @return
     * @since 1.X
     */
    private String obtieneCodigoTransaccion( String codRazon, String transcode ) {
        Integer iCodRazon = Integer.parseInt( codRazon );
        if ( iCodRazon > 0 ) {
            return "205";
        }
        
        return transcode;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2015, (Everis Chile) - versión inicial
     * </ul>
     * <p>
     * 
     * LLAMA AL PACKAGE PARA COPIAR EL CONTENIDO DESDE LA TMP A LA TBL
     * 
     * @return
     * @throws SQLException
     * @see com.sbpay.sgi.cron.dao.ProcesarIncomingDao#callSPCargaIncoming()
     * @since 1.X
     */
    @Override
    public boolean callSPCargaIncoming() throws SQLException {
        Connection dbConnection = null;
        OracleCallableStatement callableStatement = null;
        
        try {
            String callSPCargaIncomingSql = "{call SP_SIG_CARGAINCOMING(?,?)}";
            
            dbConnection = getConnection();
            callableStatement = ( OracleCallableStatement ) dbConnection
                    .prepareCall( callSPCargaIncomingSql );
            
            callableStatement.registerOutParameter( 1, java.sql.Types.VARCHAR );
            callableStatement.registerOutParameter( 2, java.sql.Types.NUMERIC );
            
            callableStatement.executeUpdate();
            
            String warning = callableStatement.getString( 1 );
            Integer codigoError = callableStatement.getInt( 2 );
            LOGGER.info( "******SE EJECUTA STORE PROCEDURE SP_SIG_CARGAINCOMING*****" );
            LOGGER.info( "WARNING : " + warning );
            LOGGER.info( "codigoError : " + codigoError );
            
        }
        finally {
            if ( callableStatement != null ) {
                callableStatement.close();
            }
        }
        return Boolean.TRUE;
    }
    
    @Override
	public boolean callSPCobroCargo() throws SQLException {
		Connection dbConnection = null;
		OracleCallableStatement callableStatement = null;

		try {
			String callSPCargaIncomingSql = "{call SP_SIG_COBRO_CARGO_VISA(?,?)}";

			dbConnection = getConnection();
			callableStatement = (OracleCallableStatement) dbConnection
					.prepareCall(callSPCargaIncomingSql);

			callableStatement.registerOutParameter(1, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(2, java.sql.Types.NUMERIC);

			callableStatement.executeUpdate();

			String warning = callableStatement.getString(1);
			Integer codigoError = callableStatement.getInt(2);
			LOGGER.info("******SE EJECUTA STORE PROCEDURE SP_SIG_COBRO_CARGO_VISA*****");
			LOGGER.info("WARNING : " + warning);
			LOGGER.info("codigoError : " + codigoError);

		} finally {
			if (callableStatement != null) {
				callableStatement.close();
			}
		}
		return Boolean.TRUE;
	}

    
    //
	@Override
	public boolean saveTransactionCargoAbono(TransaccionOutVisaCobroCargo transaccion, Integer sidOperador) throws SQLException {
		PreparedStatement stmt = null;

		try {
			StringBuilder str = new StringBuilder()
					.append("INSERT INTO ")
					.append(ConstantesBD.ESQUEMA.toString())
					.append(".TMP_COBRO_CARGO_VISA  ")
					.append(" (SID, "
							+ "MIT, "
							+ "CODIGO_FUNCION, "
							+ "DESTINATION_BIN,")
					.append(" SOURCE_BIN, "
							+ "REASON_CODE, "
							+ "COUNTRY_CODE, "
							+ "EVENT_DATE,")
					.append(" ACCOUNT_NUMBER, "
							+ "ACCOUNT_NUMBER_EXTENSION, "
							+ "DESTINATION_AMOUNT, ")
					.append(" DESTINATION_CURRENCY_CODE , "
							+ "SOURCE_AMOUNT, "
							+ "SOURCE_CURRENCY_CODE, "
							+ "MESSAGE_TEXT, ")
					.append(" SETTLEMENT_FLAG , "
							+ "TRANSACTION_IDENTIFIER, "
							+ "RESERVED, "
							+ "CENTRAL_PROCESSING_DATE, ")
					.append(" REIMBURSEMENT_ATTRIBUTE , "
							+ "OPERADOR, "
							+ "DATOS_ADICIONALES)")
					.append(" values (SEQ_TMP_COBRO_CARGO_VISA.nextval, ?, ?, ? , ")
					.append(" ?, ?, ?, ? ,").append(" ?, ?, ?, ? ,")
					.append(" ?, ?, ?, ? ,").append(" ?, ?, ?, ? ,")
					.append(" ?, ?)");

			this.getConnection().setAutoCommit(false);
			stmt = this.getConnection().prepareStatement(str.toString());
			
			LOGGER.info("QUERY DE Cobro Cargo : " + str);

			setStringNull(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getMit(), 1, stmt);
			setStringNull(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getCodigoFuncion(), 2, stmt);
			setStringNull(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getDestinationBin(), 3, stmt);
			setStringNull(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getSourceBin(), 4, stmt);
			setStringNull(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getReasonCode(), 5, stmt);
			setStringNull(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getCountryCode(), 6, stmt);
			
			setStringNull( DateUtils.getStringDateWithValidationProcess(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getEventDate(), DateUtils.FECHA_FUTURA), 7, stmt);
			
			setStringNull(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getAccountNumber(), 8, stmt);
			setStringNull(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getAccountNumberExtension(), 9, stmt);
			setStringNull(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getDestinationAmount(), 10, stmt);
			setStringNull(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getDestinationCurrencyCode(), 11, stmt);
			setStringNull(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getSourceAmount(), 12, stmt);
			setStringNull(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getSourceCurrencyCode(), 13, stmt);
			setStringNull(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getMessageText(), 14, stmt);
			setStringNull(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getSettlementFlag(), 15, stmt);
			setStringNull(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getTransactionIdentifier(), 16, stmt);
			setStringNull(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getReserved(), 17, stmt);
			
			setStringNull( DateUtils.transformJulianDateInGregorianDateStringCobroCargo(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getCentralProcessingDate()), 18, stmt);
			
			setStringNull(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getReimbursementAttribute(), 19, stmt);
			stmt.setInt(20, sidOperador);
			setStringNull(((Registro00OutVisaCobroCargoDTO) transaccion.getRegistro1020()).getDatosAdicionales(), 21, stmt);
					
			
			return (stmt.executeUpdate() > 0);
		} finally {
			if (stmt != null) {
				stmt.close();
			}
		}
	}
    
    
}
