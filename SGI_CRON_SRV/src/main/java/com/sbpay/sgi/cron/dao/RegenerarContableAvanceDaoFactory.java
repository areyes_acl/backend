package com.sbpay.sgi.cron.dao;

import java.sql.SQLException;

import com.sbpay.sgi.cron.dao.BaseDaoFactory;
import com.sbpay.sgi.cron.ds.ConnectionProvider;


public class RegenerarContableAvanceDaoFactory  extends BaseDaoFactory<RegenerarContableAvanceDao>{
	/** The single instance. */
	private static RegenerarContableAvanceDaoFactory singleINSTANCE = null;

	/**
	 * Creates the instance.
	 * 
	 * @throws SQLException
	 */
	private static void createInstance() throws SQLException {
		synchronized (RegenerarContableAvanceDaoFactory.class) {
			if (singleINSTANCE == null) {
				singleINSTANCE = new RegenerarContableAvanceDaoFactory();
			}
		}
	}

	/**
	 * Patron singleton.
	 * 
	 * @return RegenerarContableAvanceDaoFactory retorna instancia del servicio.
	 * @throws SQLException
	 */
	public static RegenerarContableAvanceDaoFactory getInstance() throws SQLException {
		if (singleINSTANCE == null) {
			createInstance();
		}
		return singleINSTANCE;
	}

	/**
	 * Metodo de restricion de sobreescritura de la clase.
	 */
	@Override
	protected Object clone() throws CloneNotSupportedException {
		super.clone();
		throw new CloneNotSupportedException();
	}

	/**
	 * Constructor default.
	 * 
	 * @throws SQLException Error de SQL.
	 */
	private RegenerarContableAvanceDaoFactory() throws SQLException {
		super();
	}

	@Override
	public RegenerarContableAvanceDao getNewEntidadDao() throws SQLException {
		return new RegenerarContableAvanceDaoJdbc(ConnectionProvider.getInstance()
				.getConnection());
	}	
}
