package com.sbpay.sgi.cron.srv.upload;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dto.DataFTP;
import com.sbpay.sgi.cron.dto.ParamUploadOutgoingICDTO;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.dto.TblCronLogDTO;
import com.sbpay.sgi.cron.enums.ConstantesPRTS;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.FilterTypeSearch;
import com.sbpay.sgi.cron.enums.LogBD;
import com.sbpay.sgi.cron.enums.MsgErrorMail;
import com.sbpay.sgi.cron.enums.MsgErrorSQL;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.srv.utils.ClientFTPSrv;
import com.sbpay.sgi.cron.srv.utils.CommonSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;
import com.sbpay.sgi.cron.utils.generals.DateUtils;
import com.sbpay.sgi.cron.validation.ProcessValidator;
import com.sbpay.sgi.cron.validation.impl.ProcessValidatorImpl;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 17/12/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * Clase que realiza la logica de la subida de un archivo OUTGOING PARA IC.
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public final class UploadICOutgoingTbkSrv extends CommonSrv {
  /** The Constant LOGGER. */
  private static final Logger LOG = Logger.getLogger(UploadICOutgoingTbkSrv.class);
  /** The single instance. */
  private static UploadICOutgoingTbkSrv singleINSTANCE = null;
  /** PROCESO **/
  private final ProcessCodeType PROCESO_ACTUAL = ProcessCodeType.SUBIDA_OUTGOING_A_IC;



  /**
   * Creates the instance.
   */
  private static void createInstance() {
    synchronized (UploadICOutgoingTbkSrv.class) {
      if (singleINSTANCE == null) {
        singleINSTANCE = new UploadICOutgoingTbkSrv();
      }
    }
  }

  /**
   * Patron singleton.
   * 
   * @return UploadOutgoingSrv retorna instancia del servicio.
   */
  public static UploadICOutgoingTbkSrv getInstance() {
    if (singleINSTANCE == null) {
      createInstance();
    }
    return singleINSTANCE;
  }

  /**
   * Metodo de restricion de sobreescritura de la clase.
   */
  @Override
  protected Object clone() throws CloneNotSupportedException {
    super.clone();
    throw new CloneNotSupportedException();
  }

  /**
   * Constructor Privado.
   */
  private UploadICOutgoingTbkSrv() {
    super();
  }


  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
   * </ul>
   * <p>
   * Metodo orquesta la subida de un archivo outgoing a IC, la logica de la subida del archivo se
   * maneja a traves de la tabla TBL_LOG_INC.
   * 
   * 
   * @return true: ok
   * @return false: error.
   * @throws AppException Exception App.
   * @since 1.X
   */
  public boolean uploadOutgoingIC() throws AppException {
    ParamUploadOutgoingICDTO params = null;
    boolean flag = false;
    List<String> fileList = null;
    List<String> fileUpload = null;
    
    ProcessValidator validadorProceso = new ProcessValidatorImpl();
    
    if (validadorProceso.isActiveProcess(PROCESO_ACTUAL)) {
    
    params = getParametros();

    // BUSQUEDA DE PARAMETROS DEL STEP UPLOAD OUTGOING IC
     if (validadorProceso.hasTheValidParametersForTheProcess(PROCESO_ACTUAL, params)) {
    	LOG.info("=== SE ENCONTRARON TODOS LOS PARAMETROS PARA EL PROCESO DE SUBIDA DE OUTGOING TBK A IC... ===");
      // SETEO PARAMETROS VAR LOCAL
      String pathOrigen = params.getPathAclInc();
      String pathFtpDes = params.getPathFtpIcEntrada();
      String startWith = params.getFormatFileNameOut();
      String extFile = params.getFormatExtNameOut();

      // BUSQUEDA DE ARCHIVOS QUE COMIENCEN CON PREFIJO: extFile
      fileList = getFileStartsWith(pathOrigen, startWith, extFile);
      LOG.info("Lista de archivos que contiene formato de nombre valido:  " + fileList);
      
      // VALIDA RESULTADO BUSQUEDA
      if (fileList != null && fileList.size() >= 0) {

        // BUSQUEDA DE ARCHIVOS A SUBIR A IC
        fileUpload = getFileUpload(fileList, startWith);
        LOG.info("Lista de archivos que no han sido subidos con anterioridad:" + fileUpload);
        
        if (fileUpload != null && fileUpload.size() > 0) {
        String msgListFilesNotUp = "";
          // SETEA PARAMETROS DEL FTP
          int count = 0;
          // LISTA DE ARCHIVOS A SUBIR
          for (String filename : fileUpload) {

            try {
              if (ClientFTPSrv.getInstance().uploadFileSFTP(params.getDataFTP(), pathOrigen,
                  pathFtpDes, filename)) {
                count++;
                LOG.info("ARCHIVO :" + filename + ", se ha subido correctamente a IC");
              }
            } catch (Exception e) {
              LOG.error("No se ha podido subir archivo : " + filename , e);
              e.printStackTrace();
              msgListFilesNotUp = msgListFilesNotUp.concat("-) " + filename).concat(
									ConstantesUtil.SKIP_LINE.toString());
              
            }
          }
          if (count == fileUpload.size()) {
            flag = true;
          
          }else{
        	  LOG.info("== WARN : Proceso de subida de outgoing a IC, se ha ejecutado pero algunos archivos no fueron subidos : " + msgListFilesNotUp);
        	  MailSendSrv.getInstance().sendMail(
                      MsgErrorMail.ALERTA_NOT_ALL_FILE_UPLOADED.toString(),
                      MsgErrorMail.MSG_NOT_ALL_FILE_UPLOADED.toString().concat(ConstantesUtil.SKIP_LINE.toString()).concat(msgListFilesNotUp));
        	  
          }
        } else {
          LOG.warn("Todo archivos outgoing para upload en IC estan procesados");
          MailSendSrv.getInstance().sendMail(
              MsgErrorMail.ALERTA_FILE_PROCESSED_UPLOAD_IC.toString(),
              MsgErrorMail.ALERTA_FILE_PROCESSED_UPLOAD_TXT_IC.toString());
        }
      } else {
        LOG.warn("No hay archivos outgoing transbank que subir al ftp de IC");
        MailSendSrv.getInstance().sendMail(MsgErrorMail.ALERTA_FILE_UPLOAD_IC.toString(),
            MsgErrorMail.ALERTA_FILE_UPLOAD_TXT_IC.toString());
      }
    } else {
        LOG.warn(MsgProceso.MSG_ERROR_FALTAN_PARAMETROS.toString());
      MailSendSrv.getInstance().sendMail(MsgErrorMail.ALERTA_PRTS_UPLOAD_IC.toString(),
          MsgErrorMail.ALERTA_PRTS_TXT_UPLOAD_IC.toString());
    }
    return flag;
    
    }else{
        String asunto = MsgProceso.MSG_PROCESO_DESACTIVADO_ASUNTO
                .toString().replace( ":?:",
                        PROCESO_ACTUAL.getCodigoProceso() );
        String mensaje = MsgProceso.MSG_PROCESO_DESACTIVADO_BODY.toString()
                .replace( ":?:", PROCESO_ACTUAL.getCodigoProceso() );
        MailSendSrv.getInstance().sendMail( asunto, mensaje );
    }
    return flag;
  }


  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
   * </ul>
   * <p>
   * Metodo valida contra la tabla TBL_LOG_INC si el archivo que se esta leyendo en la carpeta de
   * sbpay (incoming) ya fue subido a IC por FTP.
   * 
   * @param fileList Lista de archivos que esta en la carpeta a subir.
   * @return Lista de archivos que se deben subir.
   * @throws AppException
   * @since 1.X
   */
  private List<String> getFileUpload(final List<String> fileList, String starwith)
      throws AppException {
    String dateFile = null;
    List<String> fileUpload = null;
    CommonsDao daoCommons = null;
    String fileAux = null;
    TblCronLogDTO dto = null;
    TblCronLogDTO dtoAux = new TblCronLogDTO();
    try {
      fileUpload = new ArrayList<String>();
      daoCommons = CommonsDaoFactory.getInstance().getNewEntidadDao();
      // LISTA LOS ARCHIVOS CANDIDATOS A SUBIR.
      for (String fileName : fileList) {
        // FORMATO PREFIJOmmdds.dat
        fileAux = fileName.replace(starwith, "");
        // RESCATA FECHA ARCHIVO OUTGOING.
        dateFile = DateUtils.getDateFileTbk(fileAux);
        if (dateFile != null) {
          // SETEA LA FECHA A BUSCAR
          dtoAux.setFecha(dateFile);
          // BUSCA LA FECHA DEL ARCHIVO EN LA TABLA TBL_LOG_INC
          dto =
              daoCommons.getTblCronLogByType(dtoAux, LogBD.TABLE_LOG_INCOMING.getTable(),
                  FilterTypeSearch.FILTER_BY_DATE);
          // SI ENCUENTRA LA FECHA Y EL FLAG = 0, AGREGA A UNA LISTA
          if (dto != null && dto.getFileFlag().equalsIgnoreCase(ConstantesUtil.ZERO.toString())) {
            fileUpload.add(fileName);
          }
        } else {
          LOG.warn("Archivo no cumple con el formato para obtener la fecha al procesar los Outgoing "
              + fileName);
        }
      }
      return fileUpload;
    } catch (SQLException e) {
      throw new AppException(MsgErrorSQL.ERROR_QUERY_PROCESS_UPLOAD_IC.toString(), e);
    } finally {
      try {
        if (daoCommons != null) {
          daoCommons.close();
        }
      } catch (SQLException e) {
        throw new AppException(MsgErrorSQL.ERROR_QUERY_PROCESS_CLOSE_UPLOAD_IC.toString(), e);
      }
    }
  }
  
  

  



  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
   * </ul>
   * <p>
   * Metodo rescata los parametros a utilizar en la subida del outgoing para IC.
   * 
   * @return boolean true:parametros cargado, false:parametros con error.
   * @throws AppException
   * @since 1.X
   */
    private ParamUploadOutgoingICDTO getParametros() throws AppException {
        CommonsDao daoCommon = null;
        List<ParametroDTO> paramDTOList = null;
        ParamUploadOutgoingICDTO params = null;
        
        try {
            LOG.info( "CARGA PARAMETROS STEP UPLOAD OUTGOING TO IC" );
            daoCommon = CommonsDaoFactory.getInstance().getNewEntidadDao();
            paramDTOList = daoCommon
                    .getParametroCodGrupoDato( ConstantesPRTS.CRON_TBK
                            .toString() );
            if ( paramDTOList != null ) {
                
                // PARAMETROS DE FTP DE IC
                DataFTP dataFTP = new DataFTP();
                dataFTP.setIpHost( CommonsUtils.getCodiDato( paramDTOList,ConstantesPRTS.SFTP_IP_UPLOAD_OUTGOING_IC.toString() ) );
                String port = CommonsUtils.getCodiDato(paramDTOList, ConstantesPRTS.SFTP_PORT_UPLOAD_OUTGOING_IC.toString());
                dataFTP.setPort( Integer.parseInt((port != null && port.matches(ConstantesUtil.PATTERN_IS_NUMBER.toString()) ? port : ConstantesUtil.DEFAULT_PORT.toString())));
                dataFTP.setUser( CommonsUtils.getCodiDato( paramDTOList,ConstantesPRTS.SFTP_USER_UPLOAD_OUTGOING_IC.toString() ) );
                dataFTP.setPassword( CommonsUtils.getCodiDato( paramDTOList,ConstantesPRTS.SFTP_PASS_UPLOAD_OUTGOING_IC.toString() ) );
                
                // PARAMETROS DE PROCESO DE SUBIDA A IC
                params = new ParamUploadOutgoingICDTO();
                
                // RUTA DE FOLDER EN SFTP DE IC DONDE SE DEJARAN LOS OUTGOING TBK SUBIDOS
                params.setPathFtpIcEntrada( CommonsUtils.getCodiDato(paramDTOList,ConstantesPRTS.PATH_IN_UPLD_OUTGOING_IC.toString() ) );
                
                // RUTA DE FOLDER EN SERVER sbpay DONDE SE ENCUENTRAN
                // OUTGOING TBK
                params.setPathAclInc( CommonsUtils.getCodiDato( paramDTOList,ConstantesPRTS.PATH_ACL_INC.toString() ) );
                
                
                // FORMATO DE NOMBRE DE ARCHIVO OUTGOING TRANSBANK
                params.setFormatFileNameOut( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.FORMAT_FILENAME_OUT.toString() ) );
                
                // FORMATO DE EXTENCION DE ARCHIVO OUTGOING TRANSBANK
                params.setFormatExtNameOut( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.FORMAT_EXTNAME_OUT.toString() ) );
                
                // FORMATO DE EXTENCION DE ARCHIVO DE CONTROL CTR
                params.setFormatExtNameOutCtr( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.FORMAT_EXTNAME_OUT_CTR.toString() ) );
                
                params.setDataFTP( dataFTP );
                
                LOG.info( params );
                
            }
            
        }
        catch ( SQLException e ) {
            throw new AppException(
                    MsgErrorSQL.ERROR_QUERY_UP_OUT_PRTS.toString(), e );
        }
        finally {
            try {
                if ( daoCommon != null ) {
                    daoCommon.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException(
                        MsgErrorSQL.ERROR_QUERY_UP_OUT_PRTS_CLOSE.toString(), e );
            }
        }
        
        return params;
    }
}
