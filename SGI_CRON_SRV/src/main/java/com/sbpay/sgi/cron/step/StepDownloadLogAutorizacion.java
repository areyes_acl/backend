package com.sbpay.sgi.cron.step;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.enums.MsgExitoMail;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.srv.download.DownLoadLogAutorizacionSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 18/1/2016, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class StepDownloadLogAutorizacion {
    /** The Constant LOGGER. */
    private static final Logger LOG = Logger
            .getLogger( StepDownloadLogAutorizacion.class );
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 12/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws Exception
     * @since 1.X
     */
    public void execute() throws Exception {
        
        try {
            LOG.info( "========> Inicio Proceso:  Download Log Autorizacion !!!<=========" );
            if ( DownLoadLogAutorizacionSrv.getInstance().descargarLogAutorizacion() ) {
                LOG.info( "Envio Correo de Descarga Satisfactoria: Proceso DownLoad Log Autorizacion de trx" );
                MailSendSrv
                        .getInstance()
                        .sendMailOk(
                                MsgExitoMail.EXITO_PROCESS_DOWNLOAD_TITLE_LOG_AUTORIZACION
                                        .toString(),
                                MsgExitoMail.EXITO_PROCESS_DOWNLOAD_LOG_AUTORIZACION
                                        .toString() );
            }
            else {
                LOG.info( "=====>Finaliza Proceso :Descargar Log de Autorizacion de trx, se ha ejecutado con observaciones" );
                
            }
            
        }
        catch ( AppException e ) {
            LOG.error( e.getMessage(), e );
            MailSendSrv.getInstance().sendMail(
                    MsgProceso.MSG_GENERIC_ERROR_MSJ.toString(),
                    ( e.getStackTraceDescripcion() != null ) ? e
                            .getStackTraceDescripcion() : e.getMessage() );
        }
        
    }
    
}
