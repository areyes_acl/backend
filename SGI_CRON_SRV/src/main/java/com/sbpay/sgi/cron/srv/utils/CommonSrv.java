package com.sbpay.sgi.cron.srv.utils;

import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dao.Dao;
import com.sbpay.sgi.cron.dto.ConfigFile;
import com.sbpay.sgi.cron.dto.TblCronLogDTO;
import com.sbpay.sgi.cron.enums.FilterTypeSearch;
import com.sbpay.sgi.cron.enums.LogBD;
import com.sbpay.sgi.cron.enums.MsgErrorFile;
import com.sbpay.sgi.cron.enums.MsgErrorSQL;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;
import com.sbpay.sgi.cron.utils.generals.DateUtils;

/**
 * 
 * Clase encargada de validar
 * 
 * @author dmedina
 * @date 03-11-2015
 *
 */
public class CommonSrv {
	/**
	 * Clase para imprimir en el log.
	 */
	private static final Logger LOG = Logger.getLogger(CommonSrv.class
			.getName());

	/**
	 * Valida si un string puede ser convertivo a Float.
	 * 
	 * @param obj
	 *            Objeto a validar.
	 * @return True si es posible convertirlo en Float. False si no es posible
	 *         covertirlo en Float.
	 */
	protected boolean isNumber(final String obj) {
		try {
			if (obj == null || obj.isEmpty()) {
				return true;
			} else {
				Float.valueOf(obj);
				return true;
			}
		} catch (Exception ex) {
			LOG.debug(ex.getMessage(), ex);
			return false;
		}
	}

	/**
	 * Rescata valores Integer validando los valores nulos. Entresa NULL cuando
	 * se rescato un null de la columna.
	 * 
	 * @param index
	 *            Indice de la columna.
	 * @param rs
	 *            ResultSet con los datos.
	 * @return Integer con el valor de la columna
	 * @throws SQLException
	 *             Error al recuperar el dato.
	 */
	protected Integer getIntegerNull(final int index, final CallableStatement rs)
			throws SQLException {
		Integer aux = rs.getInt(index);
		if (rs.wasNull()) {
			return null;
		} else {
			return aux;
		}
	}

	/**
	 * Rescata valores string validando los valores nulos. Entresa NULL cuando
	 * se rescato un null de la columna.
	 * 
	 * @param index
	 *            Indice de la columna.
	 * @param cs
	 *            ResultSet con los datos.
	 * @return String con el valor de la columna
	 * @throws SQLException
	 *             Error al recuperar el dato.
	 */
	protected String getStringNull(final int index, final CallableStatement cs)
			throws SQLException {
		String obj = cs.getString(index);
		if (cs.wasNull()) {
			return "";
		} else {
			return obj;
		}
	}

	/**
	 * Rescata valores string validando los valores nulos. Entresa NULL cuando
	 * se rescato un null de la columna.
	 * 
	 * @param index
	 *            Indice de la columna.
	 * @param rs
	 *            .
	 * @return String con el valor de la columna
	 * @throws SQLException
	 *             Error al recuperar el dato.
	 */
	protected String getStringNull(final int index, final ResultSet rs)
			throws SQLException {
		String obj = rs.getString(index);
		if (rs.wasNull()) {
			return "";
		} else {
			return obj;
		}
	}

	/**
	 * Rescata valores string validando los valores nulos. Entresa NULL cuando
	 * se rescato un null de la columna.
	 * 
	 * @param index
	 *            Indice de la columna.
	 * @param cs
	 *            ResultSet con los datos.
	 * @return String con el valor de la columna
	 * @throws SQLException
	 *             Error al recuperar el dato.
	 */
	protected String getLongNull(final int index, final CallableStatement cs)
			throws SQLException {
		Long obj = cs.getLong(index);
		if (cs.wasNull()) {
			return "";
		} else {
			return String.valueOf(obj);
		}
	}

	/**
	 * Setea un String en una consulta.
	 * 
	 * @param value
	 *            Valor.
	 * @param index
	 *            Indice del parametro.
	 * @param cs
	 *            Consulta.
	 * @throws SQLException
	 *             Error SQL.
	 */
	protected void setStringNull(final String value, final int index,
			final CallableStatement cs) throws SQLException {
		if (value == null) {
			cs.setNull(index, Types.NULL);
		} else {
			cs.setString(index, value);
		}
	}

	/**
	 * Setea un Integer en una consulta.
	 * 
	 * @param value
	 *            Valor.
	 * @param index
	 *            Indice del parametro.
	 * @param cs
	 *            Consulta.
	 * @throws SQLException
	 *             Error SQL.
	 */
	protected void setIntegerNull(final Integer value, final int index,
			final CallableStatement cs) throws SQLException {
		if (value == null) {
			cs.setNull(index, Types.NULL);
		} else {
			cs.setInt(index, value);
		}
	}

	/**
	 * Setea un Long en una consulta.
	 * 
	 * @param value
	 *            Valor.
	 * @param index
	 *            Indice del parametro.
	 * @param cs
	 *            Consulta.
	 * @throws SQLException
	 *             Error SQL.
	 */
	protected void setLongNull(final Long value, final int index,
			final CallableStatement cs) throws SQLException {
		if (value == null) {
			cs.setNull(index, Types.NULL);
		} else {
			cs.setLong(index, value);
		}
	}

	/**
	 * Setea un Long en una consulta.
	 * 
	 * @param value
	 *            Valor.
	 * @param index
	 *            Indice del parametro.
	 * @param cs
	 *            Consulta.
	 * @throws SQLException
	 *             Error SQL.
	 */
	protected void setLongNull(final String value, final int index,
			final CallableStatement cs) throws SQLException {
		if (value == null) {
			cs.setNull(index, Types.NULL);
		} else if ("".equals(value)) {
			cs.setNull(index, Types.NULL);
		} else {
			cs.setLong(index, Long.valueOf(value));
		}
	}

	/**
	 * Setea un Date en una consulta.
	 * 
	 * @param value
	 *            Valor.
	 * @param index
	 *            Indice del parametro.
	 * @param cs
	 *            Consulta.
	 * @throws SQLException
	 *             Error SQL.
	 */
	protected void setDateNull(final Date value, final int index,
			final CallableStatement cs) throws SQLException {
		if (value == null) {
			cs.setNull(index, Types.NULL);
		} else {
			cs.setDate(index, new java.sql.Date(value.getTime()));
		}
	}

	/**
	 * Calculo del Digito verificador.
	 * 
	 * @param rut
	 *            RUT.
	 * @return Digito.
	 */
	protected String getDvRut(final Integer rut) {
		if (rut == null) {
			return "";
		}
		int mod = 0;
		int sum = 1;
		int iRut = rut;
		for (; iRut != 0; iRut /= 10) {
			sum = (sum + iRut % 10 * (9 - mod++ % 6)) % 11;
		}
		if (sum == 0) {
			return String.valueOf((char) 75);
		} else {
			return String.valueOf((char) (sum + 47));
		}
	}

	/**
	 * Cierra un port de servicio web.
	 * 
	 * @param port
	 *            Port a cerrar.
	 */
	protected void closeWs(final Closeable port) {
		try {
			port.close();
		} catch (IOException ex) {
			LOG.error("ERROR AL CERRA WS", ex);
		}
	}

	/**
	 * Previene la imprecion de "null".
	 * 
	 * @param value
	 *            valor.
	 * @return salida.
	 */
	protected String getStringNull(final String value) {
		if (value == null) {
			return "";
		} else {
			return value;
		}
	}

	/**
	 * Metodo realiza rollback de una transaccion de cualquier dao.
	 * 
	 * @param daoCarga
	 *            dao carga incoming.
	 * @throws AppException
	 *             Exception App.
	 */
	protected void rollBackTransaccion(final Object objectDAO)
			throws AppException {
		try {
			if (objectDAO != null) {
				((Dao) objectDAO).rollBack();
				LOG.info("SE APLICO ROLLBACK");
			}
		} catch (SQLException e) {
			throw new AppException(MsgErrorSQL.ERROR_ROLLBACK.toString(), e);
		}
	}

	/**
	 * Metodo valida que los archivos del directorio sean archivos y posean la
	 * extension (extControl) y comiencen con el prefijo (startWith) para
	 * agregar a un arreglo el archivo con extension (extFile).
	 * 
	 * @param pathSearch
	 *            path de busqueda.
	 * @param startWith
	 *            que el archivo comience con.
	 * @param extControl
	 *            extension de control.
	 * @param extFile
	 *            extension del archivo a procesar.
	 * @return lista de archivos a procesar.
	 * @throws AppException
	 *             Exception App.
	 */
	protected List<String> getFileStartsWith(final String pathSearch,
			final String startWith, final String extControl,
			final String extFile) throws AppException {
		File dir = null;
		File[] ficheros = null;
		List<String> fileDirectory = null;
		try {
			dir = new File(pathSearch);
			ficheros = dir.listFiles();
			if (ficheros != null && ficheros.length > 0) {
				fileDirectory = new ArrayList<String>();
				for (File file : ficheros) {
					if (file != null && file.isFile()
							&& file.getName().endsWith(extControl)) {
						if (file.getName().startsWith(startWith)) {
							fileDirectory.add(file.getName().replace(
									extControl, extFile));
						}
					}
				}
				if (fileDirectory.size() == 0) {
					fileDirectory = null;
				}
			}
			return fileDirectory;
		} catch (Exception e) {
			throw new AppException(MsgErrorFile.ERROR_READ_DIR.toString()
					.concat(pathSearch), e);
		}
	}

	/**
	 * Metodo valida que los archivos del directorio comiencen con el prefijo
	 * (startWith) y cumplan
	 * 
	 * @param pathSearch
	 *            path de busqueda.
	 * @param startWith
	 *            que el archivo comience con.
	 * @param extFile
	 *            extension del archivo a procesar.
	 * @return lista de archivos a procesar.
	 * @throws AppException
	 *             Exception App.
	 */
	protected List<String> getFileStartsWith(final String pathSearch,
			final String startWith, final String extFile) throws AppException {
		File dir = null;
		File[] ficheros = null;
		List<String> fileDirectory = null;
		try {
			dir = new File(pathSearch);
			ficheros = dir.listFiles();
			if (ficheros != null && ficheros.length > 0) {
				fileDirectory = new ArrayList<String>();
				for (File file : ficheros) {
					if (file != null && file.isFile()
							&& file.getName().endsWith(extFile)
							&& file.getName().startsWith(startWith)) {
						fileDirectory.add(file.getName());
					}
				}
				if (fileDirectory.size() == 0) {
					fileDirectory = null;
				}
			}
			return fileDirectory;
		} catch (Exception e) {
			throw new AppException(MsgErrorFile.ERROR_READ_DIR.toString()
					.concat(pathSearch), e);
		}
	}

	/**
	 * Metodo escribe contendo en un archivo.
	 * 
	 * @param contenido
	 *            variable contenido.
	 * @param file
	 *            file de archivo.
	 * @throws IOException
	 *             Excption IO.
	 */
	protected void writeFile(final String contenido, File file)
			throws IOException {
		BufferedWriter bw = null;
		bw = new BufferedWriter(new FileWriter(file));
		bw.append(contenido);
		bw.close();
	}

	/**
	 * Metodo recorre una lista de String y valida que comience con y finalice
	 * con para agregarlo a una nueva lista.
	 * 
	 * @param listFTP
	 *            lista de archivos desde el FTP.
	 * @param parametros
	 *            parametros de configuracion del archivo.
	 * @return Lista String que coinciden.
	 */
	protected List<String> retrieveFilenameListToDownload(
			final List<String> listFTP, final ConfigFile parametros) {
		List<String> filenamesToDownLoad = new ArrayList<String>();
		String starWith = parametros.getStarWith();
		String endsWith = parametros.getEndsWith();
		for (String filename : listFTP) {
			if (CommonsUtils.validateFilename(filename, starWith, endsWith)) {
				filenamesToDownLoad.add(filename);
			}

		}
		return filenamesToDownLoad;
	}

	/**
	 * Metodo recorre una lista de String y valida que comience con BICE
	 * 
	 * @param listFTP
	 *            lista de archivos desde el FTP.
	 * @param parametros
	 *            parametros de configuracion del archivo.
	 * @return Lista String que coinciden.
	 */
	protected List<String> retrieveFilenameListToDownloadBice(
			final List<String> listFTP, final ConfigFile parametros) {
		List<String> filenamesToDownLoad = new ArrayList<String>();
		String starWith = parametros.getStarWith();
		String endsWith = parametros.getEndsWith();
		String endsWithCtr = parametros.getEndsWithCtr();
		String name = starWith + "." + endsWithCtr;

		for (String filename : listFTP) {
			if (!filename.equals(name)) {
				if (CommonsUtils.validateFilenameBice(filename, starWith,
						endsWith)) {
					filenamesToDownLoad.add(filename);
				}
			}

		}

		return filenamesToDownLoad;
	}
	
	/**
	 * Metodo recorre una lista de String y valida que sea archivo de control para validar
	 * 
	 * @param listFTP
	 *            lista de archivos desde el FTP.
	 * @param parametros
	 *            parametros de configuracion del archivo.
	 * @return Lista String que coinciden.
	 */

	protected List<String> retrieveFilenameListToDownloadBiceCtr(
			final List<String> listFTP, final ConfigFile parametros) {
		List<String> filenamesToDownLoadCtr = new ArrayList<String>();
		String starWithCtrl = parametros.getStarWithCtr();
		String endsWithCtrl = parametros.getEndsWithCtr();

		for (String fileCtrl : listFTP) {
			if (CommonsUtils.validateFilenameBiceCtr(fileCtrl, starWithCtrl,
					endsWithCtrl)) {
				filenamesToDownLoadCtr.add(fileCtrl);
			}
		}

		return filenamesToDownLoadCtr;
	}

	/**
	 * Metodo recorre una lista de String y valida que comience con AVANCES
	 * 
	 * @param listFTP
	 *            lista de archivos desde el FTP.
	 * @param parametros
	 *            parametros de configuracion del archivo.
	 * @return Lista String que coinciden.
	 */
	protected List<String> retrieveFilenameListToDownloadAvances(
			final List<String> listFTP, final ConfigFile parametros) {
		List<String> filenamesToDownLoad = new ArrayList<String>();
		String starWith = parametros.getStarWith();
		for (String filename : listFTP) {
			if (CommonsUtils.validateFilenameAvances(filename, starWith)) {
				filenamesToDownLoad.add(filename);
			}

		}
		return filenamesToDownLoad;
	}

	/**
	 * Metodo recorre una lista de String y valida que comience con y finalice
	 * con para agregarlo a una nueva lista.
	 * 
	 * @param listFTP
	 *            lista de archivos desde el FTP.
	 * @param parametros
	 *            parametros de configuracion del archivo.
	 * @return Lista String que coinciden.
	 */
	protected List<String> retrieveFilenameCtrListToDownload(
			List<String> listFTP, final ConfigFile parametros) {
		List<String> filenamesToCtr = new ArrayList<String>();
		List<String> filenamesToCom = new ArrayList<String>();
		List<String> filenamesToDownLoad = new ArrayList<String>();
		String starWithCtrl = parametros.getStarWithCtr();
		String endsWithCtrl = parametros.getEndsWithCtr();
		String starWith = parametros.getStarWith();
		String endsWith = parametros.getEndsWith();
		String fileAux = null;
		for (String filename : listFTP) {
			if (CommonsUtils.validateFilename(filename, starWithCtrl,
					endsWithCtrl)) {
				filenamesToCtr.add(filename);
			}
			if (CommonsUtils.validateFilename(filename, starWith, endsWith)) {
				filenamesToCom.add(filename);
			}

		}
		for (String fileCtrl : filenamesToCtr) {
			fileAux = fileCtrl.replace(endsWithCtrl, endsWith);
			for (String file : filenamesToCom) {
				if (fileAux.equalsIgnoreCase(file)) {
					filenamesToDownLoad.add(file);
					break;
				}
			}
		}

		return filenamesToDownLoad;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versiÃ³n inicial
	 * </ul>
	 * <p>
	 * Metodo que obtiene una lista de los nombres de los archivos de control
	 * 
	 * 
	 * @param listFTP
	 * @param parametros
	 * @param endsWith
	 * @param endsWithCTR
	 * @param starWith
	 * @return
	 * @throws AppException
	 * @since 1.X
	 */
	protected List<String> retrieveFilenameCtrList(String pathSearch,
			final ConfigFile parametros) throws AppException {
		List<String> filenamesCtr = new ArrayList<String>();
		String starWithCtrl = parametros.getStarWithCtr();
		String endsWithCtrl = parametros.getEndsWithCtr();

		File dir = null;
		File[] ficheros = null;
		try {
			dir = new File(pathSearch);
			ficheros = dir.listFiles();
			if (ficheros != null && ficheros.length > 0) {
				for (File file : ficheros) {
					if (file != null && file.isFile()
							&& file.getName().endsWith(endsWithCtrl)) {
						if (file.getName().startsWith(starWithCtrl)) {
							filenamesCtr.add(file.getName());
						}
					}
				}
				if (filenamesCtr.size() == 0) {
					filenamesCtr = null;
				}
			}
			return filenamesCtr;
		} catch (Exception e) {

			throw new AppException(MsgErrorFile.ERROR_READ_DIR.toString()
					.concat(pathSearch), e);
		}

	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versiÃ³n inicial
	 * </ul>
	 * <p>
	 * Metodo que obtiene una lista de los nombres de los archivos de control
	 * 
	 * 
	 * @param listFTP
	 * @param parametros
	 * @param endsWith
	 * @param endsWithCTR
	 * @param starWith
	 * @return
	 * @throws AppException
	 * @since 1.X
	 */
	protected List<String> retrieveFilenameCtrList(List<String> list,
			final ConfigFile parametros) throws AppException {
		List<String> filenamesList = new ArrayList<String>();
		String starWith = parametros.getStarWith();
		String endsWith = parametros.getEndsWith();

		try {
			if (list != null && list.size() > 0) {
				for (String filename : list) {
					if (filename != null && filename.endsWith(endsWith)) {
						if (filename.startsWith(starWith)) {
							filenamesList.add(filename);
						}
					}
				}
				if (filenamesList.size() == 0) {
					filenamesList = null;
				}
			}
			return filenamesList;
		} catch (Exception e) {

			throw new AppException(e);
		}

	}

	/**
	 * Metodo consulta si el archivo candidato a descargar ya existe en la base
	 * de datos. (consulta por fecha)
	 * 
	 * @param listToDownload
	 *            lista de candidatos.
	 * @param starWith
	 *            comienza con prefijo.
	 * @param logBD
	 *            Variable de la tabla.
	 * @return Lista de String de archivos a procesar.
	 * @throws AppException
	 *             Exception App.
	 */
	protected List<String> currentlyNotDownloadedFiles(
			final List<String> listToDownload, final String starWith,
			final LogBD logBD) throws AppException {
		List<String> finalList = null;
		CommonsDao commonsDAO = null;
		try {
			commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
			finalList = new ArrayList<String>();
			TblCronLogDTO dto = new TblCronLogDTO();
			// RECORRE TODOS LOS FILENAME
			for (String filename : listToDownload) {
				String fecha = DateUtils.getDateFileTbk(DateUtils
						.getDateStringFromFilename(filename, starWith));

				dto.setFecha(fecha);
				TblCronLogDTO log = commonsDAO.getTblCronLogByType(dto,
						logBD.getTable(), FilterTypeSearch.FILTER_BY_DATE);
				if (log == null) {
					finalList.add(filename);
				}
			}

		} catch (SQLException e) {
			throw new AppException(MsgErrorSQL.ERROR_QUERY_TBL_LOG.toString()
					+ logBD.getTable(), e);
		} finally {
			try {
				if (commonsDAO != null) {
					commonsDAO.close();
				}
			} catch (SQLException e) {
				throw new AppException(
						MsgErrorSQL.ERROR_QUERY_TBL_LOG.toString()
								+ logBD.getTable(), e);
			}
		}
		return finalList;
	}
	
	/**
	 * Metodo consulta si el archivo candidato a descargar ya existe en la base
	 * de datos. (consulta por fecha) - visa igual que tbk pero nombres general visa
	 * 
	 * @param listToDownload
	 *            lista de candidatos.
	 * @param starWith
	 *            comienza con prefijo.
	 * @param logBD
	 *            Variable de la tabla.
	 * @return Lista de String de archivos a procesar.
	 * @throws AppException
	 *             Exception App.
	 */
	protected List<String> currentlyNotDownloadedFilesVisaNac(
			final List<String> listToDownload, final String starWith,
			final LogBD logBD) throws AppException {
		List<String> finalList = null;
		CommonsDao commonsDAO = null;
		try {
			commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
			finalList = new ArrayList<String>();
			TblCronLogDTO dto = new TblCronLogDTO();
			// RECORRE TODOS LOS FILENAME
			for (String filename : listToDownload) {
				String fecha = DateUtils.getDateFileVisaNac(DateUtils
						.getDateStringFromFilenameVisa(filename, starWith));
				
				dto.setFecha(fecha);
				TblCronLogDTO log = commonsDAO.getTblCronLogByType(dto,
						logBD.getTable(), FilterTypeSearch.FILTER_BY_DATE);
				if (log == null) {
					finalList.add(filename);
				}
			}

		} catch (SQLException e) {
			throw new AppException(MsgErrorSQL.ERROR_QUERY_TBL_LOG.toString()
					+ logBD.getTable(), e);
		} finally {
			try {
				if (commonsDAO != null) {
					commonsDAO.close();
				}
			} catch (SQLException e) {
				throw new AppException(
						MsgErrorSQL.ERROR_QUERY_TBL_LOG.toString()
								+ logBD.getTable(), e);
			}
		}
		return finalList;
	}
	
	
	protected List<String> currentlyNotDownloadedFilesVisaInt(
			final List<String> listToDownload, final String starWith,
			final LogBD logBD) throws AppException {
		List<String> finalList = null;
		CommonsDao commonsDAO = null;
		try {
			commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
			finalList = new ArrayList<String>();
			TblCronLogDTO dto = new TblCronLogDTO();
			// RECORRE TODOS LOS FILENAME
			for (String filename : listToDownload) {
				String fecha = DateUtils.getDateFileVisaInt(DateUtils
						.getDateStringFromFilenameVisa(filename, starWith));
				
				dto.setFecha(fecha);
				TblCronLogDTO log = commonsDAO.getTblCronLogByType(dto,
						logBD.getTable(), FilterTypeSearch.FILTER_BY_DATE);
				if (log == null) {
					finalList.add(filename);
				}
			}

		} catch (SQLException e) {
			throw new AppException(MsgErrorSQL.ERROR_QUERY_TBL_LOG.toString()
					+ logBD.getTable(), e);
		} finally {
			try {
				if (commonsDAO != null) {
					commonsDAO.close();
				}
			} catch (SQLException e) {
				throw new AppException(
						MsgErrorSQL.ERROR_QUERY_TBL_LOG.toString()
								+ logBD.getTable(), e);
			}
		}
		return finalList;
	}
	
	

	/**
	 * BICE VALIDA Metodo consulta si el archivo candidato a descargar ya existe
	 * en la base de datos. (consulta por fecha)
	 * 
	 * @param listToDownload
	 *            lista de candidatos.
	 * @param starWith
	 *            comienza con prefijo.
	 * @param logBD
	 *            Variable de la tabla.
	 * @return Lista de String de archivos a procesar.
	 * @throws AppException
	 *             Exception App.
	 */
	protected List<String> currentlyNotDownloadedFilesBice(
			final List<String> listToDownload, final String starWith,
			final LogBD logBD) throws AppException {
		List<String> finalList = null;
		CommonsDao commonsDAO = null;
		try {
			commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
			finalList = new ArrayList<String>();
			TblCronLogDTO dto = new TblCronLogDTO();
			// RECORRE TODOS LOS FILENAME
			// ESTO PUEDE CAMBIAR DEPENDIENDO EL FORMATO DEL NOMBRE DE ARCHIVO
			for (String filename : listToDownload) {
				String fecha = DateUtils.getDateFileBice(DateUtils
						.getDateStringFromFilenameBice(filename, starWith));
				dto.setFecha(fecha);
				TblCronLogDTO log = commonsDAO.getTblCronLogByType(dto,
						logBD.getTable(), FilterTypeSearch.FILTER_BY_DATE);
				if (log == null) {
					finalList.add(filename);
				}
			}

		} catch (SQLException e) {
			throw new AppException(MsgErrorSQL.ERROR_QUERY_TBL_LOG.toString()
					+ logBD.getTable(), e);
		} finally {
			try {
				if (commonsDAO != null) {
					commonsDAO.close();
				}
			} catch (SQLException e) {
				throw new AppException(
						MsgErrorSQL.ERROR_QUERY_TBL_LOG.toString()
								+ logBD.getTable(), e);
			}
		}
		return finalList;
	}

	/**
	 * AVANCES VALIDA Metodo consulta si el archivo candidato a descargar ya
	 * existe en la base de datos. (consulta por fecha)
	 * 
	 * @param listToDownload
	 *            lista de candidatos.
	 * @param starWith
	 *            comienza con prefijo.
	 * @param logBD
	 *            Variable de la tabla.
	 * @return Lista de String de archivos a procesar.
	 * @throws AppException
	 *             Exception App.
	 */
	protected List<String> currentlyNotDownloadedFilesAvances(
			final List<String> listToDownload, final String starWith,
			final LogBD logBD) throws AppException {
		List<String> finalList = null;
		CommonsDao commonsDAO = null;
		try {
			commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
			finalList = new ArrayList<String>();
			TblCronLogDTO dto = new TblCronLogDTO();
			// RECORRE TODOS LOS FILENAME
			// ESTO PUEDE CAMBIAR DEPENDIENDO EL FORMATO DEL NOMBRE DE ARCHIVO
			for (String filename : listToDownload) {
				String fecha = DateUtils.getDateFileAvances(DateUtils
						.getDateStringFromFilenameAvances(filename, starWith));
				dto.setFecha(fecha);
				TblCronLogDTO log = commonsDAO.getTblCronLogByType(dto,
						logBD.getTable(), FilterTypeSearch.FILTER_BY_DATE);
				if (log == null) {
					finalList.add(filename);
				}
			}

		} catch (SQLException e) {
			throw new AppException(MsgErrorSQL.ERROR_QUERY_TBL_LOG.toString()
					+ logBD.getTable(), e);
		} finally {
			try {
				if (commonsDAO != null) {
					commonsDAO.close();
				}
			} catch (SQLException e) {
				throw new AppException(
						MsgErrorSQL.ERROR_QUERY_TBL_LOG.toString()
								+ logBD.getTable(), e);
			}
		}
		return finalList;
	}

	/**
	 * Metodo consulta si el archivo candidato a descargar ya existe en la base
	 * de datos. (consulta por fecha) Alterado para validar la nomesclatura del
	 * formato nuevo del documento
	 * 
	 * @param listToDownload
	 *            lista de candidatos.
	 * @param starWith
	 *            comienza con prefijo.
	 * @param logBD
	 *            Variable de la tabla.
	 * @return Lista de String de archivos a procesar.
	 * @throws AppException
	 *             Exception App.
	 */
	protected List<String> currentlyNotDownloadedFilesCP(
			final List<String> listToDownload, final String starWith,
			final LogBD logBD) throws AppException {
		List<String> finalList = null;
		CommonsDao commonsDAO = null;
		try {
			commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
			finalList = new ArrayList<String>();
			TblCronLogDTO dto = new TblCronLogDTO();
			// RECORRE TODOS LOS FILENAME
			for (String filename : listToDownload) {
				String fecha = DateUtils.getDateFileTbkCP(DateUtils
						.getDateStringFromFilename(filename, starWith));
				dto.setFecha(fecha);
				TblCronLogDTO log = commonsDAO.getTblCronLogByType(dto,
						logBD.getTable(), FilterTypeSearch.FILTER_BY_DATE);
				if (log == null) {
					finalList.add(filename);
				}
			}

		} catch (SQLException e) {
			throw new AppException(MsgErrorSQL.ERROR_QUERY_TBL_LOG.toString()
					+ logBD.getTable(), e);
		} finally {
			try {
				if (commonsDAO != null) {
					commonsDAO.close();
				}
			} catch (SQLException e) {
				throw new AppException(
						MsgErrorSQL.ERROR_QUERY_TBL_LOG.toString()
								+ logBD.getTable(), e);
			}
		}
		return finalList;
	}
	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * Metodo que dado una lista de nombres de archivos con formato NOMBRE +
	 * mmdd + Extension, retorna el nombre del archivo que contiene la fecha
	 * mayor
	 * 
	 * @param list
	 *            : lista que contiene los nombres de archivos.
	 * @param config
	 *            : objeto que contiene el formato de los nombres de los
	 *            archivos
	 * 
	 * @return
	 * @since 1.X
	 */
	protected String getFilenameWithTheLastDate(List<String> list,
			ConfigFile config) throws AppException {
		String file = null;

		if (list == null || config == null) {
			throw new AppException(
					"No se ha ingresado una lista para extraer fecha o el objeto de configuracion no es valido.");

		} else {
			String fechaMax = null;
			for (String filename : list) {
				String fechaAux = DateUtils.getDateFileTbk(DateUtils
						.getDateStringFromFilename(filename,
								config.getStarWith()));

				// si es el primer elemnto
				if (fechaMax == null) {
					fechaMax = fechaAux;
					file = filename;
				} else {
					// si es la segunda iteracion
					if (DateUtils.compareDates(fechaMax, fechaAux) < 0) {
						fechaMax = fechaAux;
						file = filename;
					}

				}

			}
		}
		return file;

	}

}
