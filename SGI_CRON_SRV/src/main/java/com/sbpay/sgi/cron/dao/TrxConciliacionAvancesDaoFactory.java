package com.sbpay.sgi.cron.dao;
import java.sql.SQLException;

import com.sbpay.sgi.cron.dao.BaseDaoFactory;
import com.sbpay.sgi.cron.ds.ConnectionProvider;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/11/2018, (ACL) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SA.</B>
 */
public class TrxConciliacionAvancesDaoFactory  extends BaseDaoFactory<TrxConciliarAvancesDao>{
	/** The single instance. */
	private static TrxConciliacionAvancesDaoFactory singleINSTANCE = null;

	/**
	 * Creates the instance.
	 * 
	 * @throws SQLException
	 */
	private static void createInstance() throws SQLException {
		synchronized (TrxConciliacionAvancesDaoFactory.class) {
			if (singleINSTANCE == null) {
				singleINSTANCE = new TrxConciliacionAvancesDaoFactory();
			}
		}
	}

	/**
	 * Patron singleton.
	 * 
	 * @return ActivarPrepagoDaoFactory retorna instancia del servicio.
	 * @throws SQLException
	 */
	public static TrxConciliacionAvancesDaoFactory getInstance() throws SQLException {
		if (singleINSTANCE == null) {
			createInstance();
		}
		return singleINSTANCE;
	}

	/**
	 * Metodo de restricion de sobreescritura de la clase.
	 */
	@Override
	protected Object clone() throws CloneNotSupportedException {
		super.clone();
		throw new CloneNotSupportedException();
	}

	/**
	 * Constructor default.
	 * 
	 * @throws SQLException Error de SQL.
	 */
	private TrxConciliacionAvancesDaoFactory() throws SQLException {
		super();
	}

	/**
	 * Metodo obtiene la instancia mas la conexion.
	 */
	@Override
	public TrxConciliarAvancesDao getNewEntidadDao() throws SQLException {
		return new TrxConciliarAvancesDaoJdbc(ConnectionProvider.getInstance()
				.getConnection());
	}
}
