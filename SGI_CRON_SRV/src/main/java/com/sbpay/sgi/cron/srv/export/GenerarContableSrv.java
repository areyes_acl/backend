package com.sbpay.sgi.cron.srv.export;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dao.GenerarContableDao;
import com.sbpay.sgi.cron.dao.GenerarContableDaoFactory;
import com.sbpay.sgi.cron.dto.ParamExportCnblDTO;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.dto.TblCronLogDTO;
import com.sbpay.sgi.cron.dto.TmpExportContableDTO;
import com.sbpay.sgi.cron.enums.ConstantesOperador;
import com.sbpay.sgi.cron.enums.ConstantesPRTS;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.DateFormatType;
import com.sbpay.sgi.cron.enums.LogBD;
import com.sbpay.sgi.cron.enums.MsgErrorMail;
import com.sbpay.sgi.cron.enums.MsgGenerationCntbl;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.enums.StatusProcessType;
import com.sbpay.sgi.cron.srv.utils.CommonSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;
import com.sbpay.sgi.cron.utils.file.ContableExport;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;
import com.sbpay.sgi.cron.utils.generals.DateUtils;
import com.sbpay.sgi.cron.validation.ProcessValidator;
import com.sbpay.sgi.cron.validation.impl.ProcessValidatorImpl;

public class GenerarContableSrv  extends CommonSrv{
    
	/** The Constant LOGGER. */
	private static final Logger LOGGER = Logger.getLogger( GenerarContableSrv.class );
    /** The single instance. */
    private static GenerarContableSrv singleINSTANCE = null;
    /** variable parametros del Cron */
    
    private final ProcessCodeType PROCESO_ACTUAL = ProcessCodeType.GENERAR_ARCHIVO_CONTABLE;

    
    /**
     * Creates the instance.
     */
    private static void createInstance() {
        synchronized ( GenerarContableSrv.class ) {
            if ( singleINSTANCE == null ) {
                singleINSTANCE = new GenerarContableSrv();

            }
        }
    }
    
    /**
     * Patron singleton.
     * 
     * @return GenerarContableSrv retorna instancia del servicio.
     */
    public static GenerarContableSrv getInstance() {
        if ( singleINSTANCE == null ) {
            createInstance();
        }
        return singleINSTANCE;
    }
    
    /**
     * Metodo de restricion de sobreescritura de la clase.
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        super.clone();
        throw new CloneNotSupportedException();
    }
    
    /**
     * Constructor Privado.
     */
    private GenerarContableSrv() {
        super();
    }
    
    /**
     * 
     * @return
     * @throws AppException
     */
    public boolean generarArchivoContable() throws AppException {
    	GenerarContableDao generarContableDao = null;
    	ParamExportCnblDTO paramExportCnblDTO = null;
    	Boolean flag = Boolean.FALSE;
    	CommonsDao commonsDAO = null;
    	 
    	try{
    	    ProcessValidator validadorProceso = new ProcessValidatorImpl();

    	    if (validadorProceso.isActiveProcess(PROCESO_ACTUAL)) {

    		paramExportCnblDTO = getParametrosUploadCntbl();
        	if(!validadorProceso.hasTheValidParametersForTheProcess( PROCESO_ACTUAL, paramExportCnblDTO )){
        		LOGGER.warn(MsgGenerationCntbl.ERROR_PARAM_NOT_FOUND.toString());
                MailSendSrv.getInstance().sendMail(MsgErrorMail.ALERTA_PRTS.toString(), MsgErrorMail.ALERTA_PRTS_TXT.toString());
        	}
        	
        	LOGGER.info("=== SE ENCONTRARON TODOS LOS PARAMETROS NECESARIOS PARA EL PROCESO, SE INICIA GENERACION DE ASIENTOS CONTABLES... ===");
        	
        	generarContableDao = GenerarContableDaoFactory.getInstance().getNewEntidadDao();
        	commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
        	
        	// Se agrega ciclo para pasar cada uno de los operadores existentes a la generacion de contabilidad
        	List<Integer> operadores = commonsDAO.getAllOperadorProducto();
        	// Se limpia tabla CONTABLE
        	generarContableDao.clearTmpContable();
        	for (Integer sidOperador : operadores) {
        		// LLAMA AL SP_SIG_CONTABLE_MAIN
                if (generarContableDao.cargarInformacionContable(sidOperador)) {
                  LOGGER.info("SE HA LLAMADO AL PROCEDIMIENTO SP_SIG_CONTABLE_MAIN("+sidOperador+") CORRECTAMENTE");
                }
			}
        	
        	List<TmpExportContableDTO> lstTmpExportContableDTO = generarContableDao.obtenerInformacionCnblt();
			if (lstTmpExportContableDTO != null
					&& lstTmpExportContableDTO.size() > 0) {

				String cntblFilename = generateContableFilename(paramExportCnblDTO);
				String cntblFilenameCtr = generateControlFilename(paramExportCnblDTO);
				String ruta = paramExportCnblDTO.getPathSalida();
				ContableExport incomingOut = new ContableExport();
				incomingOut.exportContableFile(ruta, cntblFilename,cntblFilenameCtr,
						lstTmpExportContableDTO);
				saveLog(cntblFilename);
				generarContableDao.clearTmpContable();

	            flag = true;
			} else {
				LOGGER.warn(MsgGenerationCntbl.WARNING_DATA_NOT_FOUND
						.toString());
				flag = false;
			}
			
	    	return flag;
	    	
    	    }else{
    	        String asunto = MsgProceso.MSG_PROCESO_DESACTIVADO_ASUNTO
                        .toString().replace( ":?:",
                                PROCESO_ACTUAL.getCodigoProceso() );
                String mensaje = MsgProceso.MSG_PROCESO_DESACTIVADO_BODY.toString()
                        .replace( ":?:", PROCESO_ACTUAL.getCodigoProceso() );
                MailSendSrv.getInstance().sendMail( asunto, mensaje );
    	    }
    	}catch (SQLException e) {
			throw new AppException(MsgGenerationCntbl.ERROR_CREATE_CONTABLE_FILE.toString(),e);
		}finally {
			if (generarContableDao != null && commonsDAO != null) {
				try {
					generarContableDao.close();
					commonsDAO.close();
				} catch (SQLException e) {
					throw new AppException(e.getMessage(), e);
				}
			}
		}
        return flag;
    	

    }
	
   
	private String generateControlFilename(ParamExportCnblDTO parametros) {
		String date = DateUtils.getDateTodayInYYYYMMDD();
		StringBuilder str = new StringBuilder();
		str.append(parametros.getFormatFileNameOnewCtr()).append(date)
				.append(ConstantesUtil.POINT).append(parametros.getFormatExtNameOnewCtr());
		return str.toString();
	}

	/**
	 * 
	 * @param cntblFilename
	 * @throws AppException
	 */
	private void saveLog(String cntblFilename) throws AppException {
		CommonsDao commonsDAO = null;

		TblCronLogDTO log = new TblCronLogDTO();
		log.setFilename(cntblFilename);
		log.setFileFlag(StatusProcessType.PROCESS_PENDING.getValue());
		log.setFecha(DateUtils.parseFormat(DateUtils.getTodayInYYMMDDFormat(),
				DateFormatType.FORMAT_YYMMDD,
				DateFormatType.FORMAT_DDMMYY_WITH_SLASH_SEPARATOR));
		try {
			commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
			commonsDAO.saveTblCronLog(log, LogBD.TABLE_LOG_CONTABLE);
			commonsDAO.endTx();
		} catch (SQLException e) {
			try {
				if (commonsDAO != null) {
					commonsDAO.rollBack();
					LOGGER.warn("Se ha ejecutado rollback");
				}
			} catch (SQLException e1) {
				throw new AppException(e.getMessage(), e1);
			}
			throw new AppException(e.getMessage(), e);
		} finally {
			if (commonsDAO != null) {
				try {
					commonsDAO.close();
				} catch (SQLException e) {
					throw new AppException(e.getMessage(), e);
				}
			}
		}
	}
  
  /**
   * 
   * @param parametros
   * @return
   */
  private String generateContableFilename(final ParamExportCnblDTO parametros) {
	String date = DateUtils.getDateTodayInYYYYMMDD();
	StringBuilder str = new StringBuilder();
	str.append(parametros.getNomArchivo()).append(date).append(ConstantesUtil.POINT)
			.append(parametros.getExtension());
	return str.toString();
  }
  
  
  
  
  /**
   * Metodo busca los parametros del proceso que genera el archivo Contable.
   * 
   * @return Dto con los parametros utilizados.
   * @throws SQLException Exception SQL.
   */
  private ParamExportCnblDTO getParametrosUploadCntbl() throws SQLException {
      CommonsDao daoCommon = null;
      List<ParametroDTO> paramDTOList = null;
      ParamExportCnblDTO paramPldCnblDTO = null;
      try {
          daoCommon = CommonsDaoFactory.getInstance().getNewEntidadDao();
          paramDTOList = daoCommon.getParametroCodGrupoDato( ConstantesPRTS.CRON_TBK.toString() );
          if ( paramDTOList != null) {
        	  paramPldCnblDTO = new ParamExportCnblDTO();
              paramPldCnblDTO.setPathSalida( CommonsUtils.getCodiDato( paramDTOList, ConstantesPRTS.PATH_TRX_CONTABLE.toString() ) );   
              paramPldCnblDTO.setNomArchivo( CommonsUtils.getCodiDato( paramDTOList, ConstantesPRTS.FORMAT_FILE_ACCOUNT_FILE.toString() ) );
              paramPldCnblDTO.setExtension( CommonsUtils.getCodiDato( paramDTOList, ConstantesPRTS.FORMAT_EXT_ACCONT_FILE.toString() ) );
              paramPldCnblDTO.setFormatFileNameOnewCtr( CommonsUtils.getCodiDato( paramDTOList, ConstantesPRTS.FORMAT_FILE_ACCOUNT_FILE_CTR.toString() ) );  
              paramPldCnblDTO.setFormatExtNameOnewCtr( CommonsUtils.getCodiDato( paramDTOList, ConstantesPRTS.FORMAT_EXT_ACCOUNT_FILE_CTR.toString() ) );  
          }
          else {
          	paramPldCnblDTO = null;
          }
          return paramPldCnblDTO;
		} finally {

			if (daoCommon != null) {
				daoCommon.close();
			}

		}
  }

}
