package com.sbpay.sgi.cron.srv.download;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dto.ConfigFile;
import com.sbpay.sgi.cron.dto.DataFTP;
import com.sbpay.sgi.cron.dto.ParamDownloadBOLDTO;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.dto.TblCronLogDTO;
import com.sbpay.sgi.cron.enums.ConstantesPRTS;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.LogBD;
import com.sbpay.sgi.cron.enums.MsgDownloadBol;
import com.sbpay.sgi.cron.enums.MsgDownloadComisionProcess;
import com.sbpay.sgi.cron.enums.MsgErrorSQL;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.enums.StatusProcessType;
import com.sbpay.sgi.cron.srv.utils.ClientFTPSrv;
import com.sbpay.sgi.cron.srv.utils.CommonSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;
import com.sbpay.sgi.cron.utils.generals.DateUtils;
import com.sbpay.sgi.cron.validation.BolFileValidator;
import com.sbpay.sgi.cron.validation.ProcessValidator;
import com.sbpay.sgi.cron.validation.impl.BolFileValidatorImpl;
import com.sbpay.sgi.cron.validation.impl.ProcessValidatorImpl;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 12/01/2016, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * Servicio para descargar BOL desde ftp definido
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public final class DownloadBOLSrv extends CommonSrv {
    
    /** The Constant LOGGER. */
    private static final Logger LOG = Logger.getLogger( DownloadBOLSrv.class
            .getName() );
    /** The single instance. */
    private static DownloadBOLSrv singleINSTANCE = null;
    
    private final ProcessCodeType PROCESO_ACTUAL = ProcessCodeType.DESCARGAR_ARCHIVO_BOL;
    
    /**
     * Creates the instance.
     */
    private static void createInstance() {
        synchronized ( DownloadBOLSrv.class ) {
            if ( singleINSTANCE == null ) {
                singleINSTANCE = new DownloadBOLSrv();
            }
        }
    }
    
    /**
     * Patron singleton.
     * 
     * @return DownLoadBOLSrv retorna instancia del servicio.
     */
    public static DownloadBOLSrv getInstance() {
        if ( singleINSTANCE == null ) {
            createInstance();
        }
        return singleINSTANCE;
    }
    
    /**
     * Metodo de restricion de sobreescritura de la clase.
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        super.clone();
        throw new CloneNotSupportedException();
    }
    
    /**
     * Constructor Privado.
     */
    private DownloadBOLSrv() {
        super();
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @throws AppException
     * @since 1.X
     */
    public void descargarBOL() throws AppException {
        
        ProcessValidator validadorProceso = new ProcessValidatorImpl();
        
        if ( validadorProceso.isActiveProcess( PROCESO_ACTUAL ) ) {
            
            // BUSCAR LOS PARAMETROS NECESARIOS PARA PROCESO
            ParamDownloadBOLDTO parametros = obtenerParametrosProceso();
            
            if ( validadorProceso.hasTheValidParametersForTheProcess(
                    PROCESO_ACTUAL, parametros ) ) {
                
                String nombreArchivo = extraerNombreUltimoArchivoBOLDesdeSFTP( parametros );
                LOG.info( "nombre de arhivo mayor : " + nombreArchivo );
                
                BolFileValidator bolValidate = new BolFileValidatorImpl();
                bolValidate.validarArchivoBolADescargar( nombreArchivo,
                        parametros.getFormatFileNameBol() );
                
                descargarArchivo( parametros, nombreArchivo );
                generarArchivoControl( parametros, nombreArchivo );
                
                LOG.info( "Se ha descargado el archivo BOL correctamente." );
                
            }
            else {
                throw new AppException(
                        MsgDownloadBol.INVALID_PARAMS.toString() );
            }
            
        }
        else {
            String asunto = MsgProceso.MSG_PROCESO_DESACTIVADO_ASUNTO
                    .toString().replace( ":?:",
                            PROCESO_ACTUAL.getCodigoProceso() );
            String mensaje = MsgProceso.MSG_PROCESO_DESACTIVADO_BODY.toString()
                    .replace( ":?:", PROCESO_ACTUAL.getCodigoProceso() );
                        
            MailSendSrv.getInstance().sendMail( asunto, mensaje );
            
        }
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param parametros
     * @param nombreArchivo
     * @throws AppException
     * @since 1.X
     */
    private void generarArchivoControl( ParamDownloadBOLDTO parametros,
            String nombreArchivo ) throws AppException {
        String ruta = parametros.getPathBol();
        String controlFilename = nombreArchivo.replace(
                parametros.getFormatExtNameBol(),
                parametros.getFormatExtNameBolCtr() );
        
        LOG.info( "=========> SE GENERA ARCHIVO DE CONTROL BOL<==========" );
        String pathControlFile = ruta.concat( controlFilename );
        File controlFile = new File( pathControlFile );
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter( new FileWriter( controlFile ) );
            bw.close();
        }
        catch ( IOException e ) {
            throw new AppException(
                    MsgDownloadBol.ERROR_GENERATE_CONTROL_FILE.toString() );
        }
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param parametros
     * @param nombreArchivo
     * @throws AppException
     * @since 1.X
     */
    private void descargarArchivo( ParamDownloadBOLDTO parametros,
            String nombreArchivo ) throws AppException {
        // DESCARGAR
        String pathOrigenFTP = parametros.getPathServerFTP();
        String pathDestinoLocalServer = parametros.getPathBol();
        
        // SE EXTRAE LA FECHA DEL ARCHIVO
        String fecha = DateUtils.getDateFileTbk( DateUtils
                .getDateStringFromFilename( nombreArchivo,
                        parametros.getFormatFileNameBol() ) );
        
        LOG.info( "Comienza descarga de archivo " + nombreArchivo
                + " desde ftp : " + parametros.getDataFTP().getIpHost() );
        
        ClientFTPSrv.getInstance().downloadFileSFTP( parametros.getDataFTP(),
                pathOrigenFTP, pathDestinoLocalServer, nombreArchivo );
        
        TblCronLogDTO logDTO = new TblCronLogDTO();
        logDTO.setFecha( fecha );
        logDTO.setFilename( nombreArchivo );
        logDTO.setFileFlag( StatusProcessType.PROCESS_PENDING.getValue() );
        
        save( logDTO );
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Metodo registra en la tabla de log TBL_LOG_COMISION la descarga
     * del archivo de comisiones.
     * 
     * @param dto
     * @throws AppException
     * @since 1.X
     */
    private void save( TblCronLogDTO dto ) throws AppException {
        // INICIALIZA EL DAO
        CommonsDao commonsDAO = null;
        try {
            commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
            if ( commonsDAO.saveTblCronLogBOL( dto, LogBD.TABLE_LOG_BOL ) ) {
                commonsDAO.endTx();
            }
            else {
                commonsDAO.rollBack();
            }
        }
        catch ( SQLException e ) {
            
            throw new AppException(
                    MsgDownloadComisionProcess.ERROR_SAVE_LOG.toString(), e );
        }
        finally {
            if ( commonsDAO != null ) {
                try {
                    commonsDAO.close();
                }
                catch ( SQLException e ) {
                    throw new AppException(
                            MsgDownloadComisionProcess.ERROR_SAVE_LOG
                                    .toString(),
                            e );
                }
            }
        }
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Se extrae le nombre del ultimo archivo BOL descargado desde el
     * SFTP de transbank
     * 
     * @return
     * @throws AppException
     * @since 1.X
     */
    private String extraerNombreUltimoArchivoBOLDesdeSFTP(
            ParamDownloadBOLDTO parametros ) throws AppException {
        
        List<String> listaSFTP = ClientFTPSrv.getInstance().listFileSFTP(
                parametros.getDataFTP(), parametros.getPathServerFTP() );
        
        LOG.info( "listaSFTP : " + listaSFTP );
        
        ConfigFile config = new ConfigFile( parametros.getFormatFileNameBol(),
                parametros.getFormatExtNameBol(),
                parametros.getFormatFileNameBol(),
                parametros.getFormatExtNameBolCtr() );
        
        // RECUPERA LOS NOMBRES DE LOS ARCHIVOS BOL CON FORMATO DE
        // NOMBRE VALIDO
        listaSFTP = retrieveFilenameCtrList( listaSFTP, config );
        
        LOG.info( "listaFTPFiltradaPorNombre : " + listaSFTP );
        
        if(listaSFTP == null){
            throw new AppException( "No se ha encontrado ningun archivo con el formato BOL valido en carpeta de SFTP." );
        }
        
        // RECUPERA EL NOMBRE DEL ARCHIVO MAYOR DE SFTP
        String nombreArchivoReciente = getFilenameWithTheLastDate( listaSFTP,
                config );
        
        return nombreArchivoReciente;
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Metodo que obtiene los parametros necesarios para el proceso de
     * descarga de comisiones
     * 
     * @return
     * @throws AppException
     * @since 1.X
     */
    private ParamDownloadBOLDTO obtenerParametrosProceso() throws AppException {
        ParamDownloadBOLDTO parametros = null;
        List<ParametroDTO> paramDTOList = null;
        CommonsDao commonsDAO = null;
        DataFTP dataFTP = null;
        try {
            // INICIA EL DAO
            commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
            
            // SE OBTIENEN TODOS LOS CODIGOS
            paramDTOList = commonsDAO
                    .getParametroCodGrupoDato( ConstantesPRTS.CRON_TBK
                            .toString() );
            
            // SI EXISTEN PARAMETROS EN BD SE EXTRAEN
            if ( paramDTOList != null ) {
                
                // PARAMETROS DE FTP TRANSBANK
                dataFTP = new DataFTP();
                dataFTP.setIpHost( CommonsUtils.getCodiDato( paramDTOList,
                        ConstantesPRTS.SFTP_IP_DOWNLOAD_BOL_FILE.toString() ) );
                String port = CommonsUtils.getCodiDato(paramDTOList, ConstantesPRTS.SFTP_PORT_DOWNLOAD_BOL_FILE.toString()) ;
                dataFTP.setPort( Integer.parseInt((port != null && port.matches(ConstantesUtil.PATTERN_IS_NUMBER.toString()) ? port : ConstantesUtil.DEFAULT_PORT.toString())));
                
                dataFTP.setUser( CommonsUtils.getCodiDato( paramDTOList,
                        ConstantesPRTS.SFTP_USER_DOWNLOAD_BOL_FILE.toString() ) );
                dataFTP.setPassword( CommonsUtils.getCodiDato( paramDTOList,
                        ConstantesPRTS.SFTP_PASS_DOWNLOAD_BOL_FILE.toString() ) );
                
                // PARAMETROS BOL
                parametros = new ParamDownloadBOLDTO();
                
                // FORMATO DE EXTENSION DE ARCHIVO BOL
                parametros.setFormatFileNameBol( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.FORMAT_FILENAME_BOL.toString() ) );
                parametros.setFormatExtNameBol( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.FORMAT_EXTNAME_BOL.toString() ) );
                parametros.setFormatExtNameBolCtr( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.FORMAT_EXTNAME_BOL_CTR.toString() ) );
                parametros.setPathBol( CommonsUtils.getCodiDato( paramDTOList,
                        ConstantesPRTS.PATH_BOL.toString() ) );
                parametros.setPathServerFTP( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.PATH_OUT_DWNLD_BOL_FILE
                                .toString() ) );
                
                parametros.setDataFTP( dataFTP );
                
            }
            return parametros;
        }
        catch ( Exception e ) {
            e.printStackTrace();
            throw new AppException( MsgErrorSQL.ERROR_QUERY_PRTS.toString(), e );
        }
        finally {
            try {
                if ( commonsDAO != null ) {
                    commonsDAO.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException(
                        MsgErrorSQL.ERROR_QUERY_PRTS_CLOSE.toString(), e );
            }
        }
    }
    
}
