package com.sbpay.sgi.cron.dao;

import java.sql.SQLException;
import java.util.List;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.Dao;
import com.sbpay.sgi.cron.dto.TmpExportItzAvanceDTO;

public interface GenerarItzAvanceDao extends Dao{

	List<TmpExportItzAvanceDTO> obtenerInformacion() throws SQLException, AppException;

}
