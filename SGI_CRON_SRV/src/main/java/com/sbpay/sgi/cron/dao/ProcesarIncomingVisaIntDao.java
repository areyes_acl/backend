package com.sbpay.sgi.cron.dao;

import java.sql.SQLException;

import com.sbpay.sgi.cron.dao.Dao;
import com.sbpay.sgi.cron.dto.TransaccionOutVisaInt;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 25/04/2019, (ACL) -  versión inicial 
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SA.</B>
 */
public interface ProcesarIncomingVisaIntDao extends Dao {
    
    public boolean saveTransaction(TransaccionOutVisaInt transaccion, Integer sidOperador)throws SQLException;
    
    public boolean callSPCargaIncoming() throws SQLException;
    
}