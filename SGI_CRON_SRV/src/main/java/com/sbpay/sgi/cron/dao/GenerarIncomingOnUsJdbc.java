package com.sbpay.sgi.cron.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.dao.BaseDao;
import com.sbpay.sgi.cron.dto.TransaccionOnUsDTO;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 7/12/2015, (ACL) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SA.</B>
 */
public class GenerarIncomingOnUsJdbc extends BaseDao<GenerarIncomingOnUsDAO> implements
        GenerarIncomingOnUsDAO {
    
    /** VARIABLE PARA EL LOGER */
    private static final Logger LOGGER = Logger
            .getLogger( GenerarIncomingOnUsJdbc.class );
	
    
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 28/01/2015, (ACL & sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @param connection
     * @since 1.X
     */
    public GenerarIncomingOnUsJdbc( Connection connection ) {
        super( connection );
        
    }


	@Override
	public boolean callSPCargaOutgoingOnUsTransbank() throws SQLException {
		CallableStatement callableStatement = null;
		LOGGER.info("******SE EJECUTA STORE PROCEDURE CALL SP_SIG_CARGAOUT_ONUS_TBK_VISA*****");
		try {
			callableStatement = this.getConnection().prepareCall(
					CALL_SP_SIG_CARGAOUT_ONUS_TBK_VISA);
			callableStatement.registerOutParameter(1, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(2, java.sql.Types.NUMERIC);		
			callableStatement.execute();
			String warning = callableStatement.getString(1);
			Integer codigoError = callableStatement.getInt(2);			
			LOGGER.info("WARNING : " + warning);
			LOGGER.info("codigoError : " + codigoError);
		} finally {

			if (callableStatement != null) {
				callableStatement.close();
			}

		}

		return Boolean.TRUE;
	}


	@Override
	public List<TransaccionOnUsDTO> loadTransactions() throws SQLException {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<TransaccionOnUsDTO> transactionList = new ArrayList<TransaccionOnUsDTO>();
		TransaccionOnUsDTO tr = null;
		try {

			stmt = this.getConnection().prepareStatement(
					SELECT_ALL_FROM_TMP_OUTGOING_ONUS_TRANSBANK);

			// SE EJECUTA CONSULTA
			rs = stmt.executeQuery();

			// LEE LAS COLUMNAS DE CADA REGISTRO
			while (rs.next()) {
				tr = parseTransactions(rs);
				transactionList.add(tr);
			}
			return transactionList;
		} finally {

			if (stmt != null) {
				stmt.close();
			}
			if (rs != null) {
				rs.close();
			}
		}
	}


	private TransaccionOnUsDTO parseTransactions(final ResultSet rs) throws SQLException {
		TransaccionOnUsDTO tr = new TransaccionOnUsDTO();
        tr.setSid(rs.getLong("SID"));
        tr.setCiudadComercio(rs.getString("CIUDAD_COMERCIO"));
        tr.setCodigoTransaccion(rs.getString("CODIGO_TRANSACCION"));
        tr.setEstadoProceso(rs.getInt("ESTADO_PROCESO"));
        tr.setNumeroTarjeta(rs.getString("NUMERO_TARJETA"));
        tr.setFechaCompra(rs.getString("FECHA_COMPRA"));
        tr.setFechaAutorizacion(rs.getString("FECHA_AUTORIZACION"));
        tr.setFechaPosteo(rs.getString("FECHA_POSTEO"));
        tr.setTipoVenta(rs.getString("TIPO_VENTA"));
        tr.setNumCuotas(rs.getString("NUM_CUOTAS"));
        tr.setNumMicrofilm(rs.getString("NUM_MICROFILM"));
        tr.setNumComercio(rs.getString("NUM_COMERCIO"));
        tr.setMontoTransac(rs.getString("MONTO_TRANSACCION"));
        tr.setValorCuota(rs.getString("VALOR_CUOTA"));
        tr.setNombreComercio(rs.getString("NOMBRE_COMERCIO"));
        tr.setCiudadComercio(rs.getString("CIUDAD_COMERCIO"));
        tr.setRubroComercio(rs.getString("RUBRO_COMERCIO"));
        tr.setCodAutor(rs.getString("COD_AUTOR"));
        return tr;
	}


	@Override
	public void clearTemporalOutgoingOnUsTransbank() throws SQLException {
		CallableStatement callableStatement = null;
		LOGGER.info("******SE EJECUTA STORE PROCEDURE CALL SP SP_SIG_CLEAN_OUT_TBK_ONUS_VISA *****");
		try {
			callableStatement = this.getConnection().prepareCall(
					CALL_SP_SIG_CLEAROUT_ONUS_TBK_VISA);
			callableStatement.registerOutParameter(1, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(2, java.sql.Types.NUMERIC);		
			callableStatement.execute();
			String warning = callableStatement.getString(1);
			Integer codigoError = callableStatement.getInt(2);			
			LOGGER.info("WARNING : " + warning);
			LOGGER.info("codigoError : " + codigoError);
		} finally {

			if (callableStatement != null) {
				callableStatement.close();
			}

		}


	}     
}
