package com.sbpay.sgi.cron.dao;

import java.sql.SQLException;

import com.sbpay.sgi.cron.dao.Dao;
import com.sbpay.sgi.cron.dto.TransaccionOnUsDTO;


/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 24/11/2015, (ACL) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SA.</B>
 */
public interface TrxOnUsDao extends Dao {

	boolean saveTransaction(TransaccionOnUsDTO instance) throws SQLException;

	boolean callSPCargaLogTrxOnUs() throws SQLException;
    



    
}
