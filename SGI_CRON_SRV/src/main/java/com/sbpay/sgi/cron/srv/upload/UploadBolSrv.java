package com.sbpay.sgi.cron.srv.upload;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dto.BolLogDTO;
import com.sbpay.sgi.cron.dto.ConfigFile;
import com.sbpay.sgi.cron.dto.DataFTP;
import com.sbpay.sgi.cron.dto.ParamUploadBolDTO;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.enums.ConstantesPRTS;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.MsgErrorSQL;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.enums.MsgUploadBol;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.enums.StatusProcessType;
import com.sbpay.sgi.cron.srv.utils.ClientFTPSrv;
import com.sbpay.sgi.cron.srv.utils.CommonSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;
import com.sbpay.sgi.cron.validation.ProcessValidator;
import com.sbpay.sgi.cron.validation.impl.ProcessValidatorImpl;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * Clase encargada de realizar la subida del archivo BOL a transbank
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public final class UploadBolSrv extends CommonSrv {
    /** The Constant LOGGER. */
    private static final Logger LOG = Logger.getLogger(UploadBolSrv.class);
    /** The single instance. */
    private static UploadBolSrv singleINSTANCE = null;
    private final ProcessCodeType PROCESO_ACTUAL = ProcessCodeType.SUBIDA_ARCHIVO_BOL;

    /**
     * Creates the instance.
     */
    private static void createInstance() {
	synchronized (UploadBolSrv.class) {
	    if (singleINSTANCE == null) {
		singleINSTANCE = new UploadBolSrv();

	    }
	}
    }

    /**
     * Patron singleton.
     * 
     * @return UploadIncomingSrv retorna instancia del servicio.
     */
    public static UploadBolSrv getInstance() {
	if (singleINSTANCE == null) {
	    createInstance();
	}
	return singleINSTANCE;
    }

    /**
     * Metodo de restricion de sobreescritura de la clase.
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
	super.clone();
	throw new CloneNotSupportedException();
    }

    /**
     * Constructor Privado.
     */
    private UploadBolSrv() {
	super();
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * Metodo principal del servicio, que se encarga de realizar al subida del
     * Archivo BOL al servidor SFPT de Transbank
     * 
     * @return
     * @throws AppException
     * @throws SQLException
     * @since 1.X
     */
    public void uploadBol() throws AppException {
        
        ParamUploadBolDTO parametros = null;
        List<String> fileListInDirectory = null;
        List<String> fileListFiltered = null;
        
        ProcessValidator validadorProceso = new ProcessValidatorImpl();
        
        if ( validadorProceso.isActiveProcess( PROCESO_ACTUAL ) ) {
            LOG.info( "=== Proceso subida de Bol activo ===" );
            parametros = obtenerParametros();
            
            if ( validadorProceso.hasTheValidParametersForTheProcess(
                    PROCESO_ACTUAL, parametros ) ) {
                LOG.info( "===  Se han encontrado todos los parametros ====" );
                ConfigFile config = getConfigFile( parametros );
                
                LOG.info( "config :" + config );
                
                // BUSCA FILENAMELIST EN LOCALSERVER CON NOMBRES
                // VALIDOS
                fileListInDirectory = getFileStartsWith(
                        parametros.getPathLocalServer(), config.getStarWith(),
                        config.getEndsWith() );
                
                LOG.info( "Lista de archivos con formato valido:  "
                        + fileListInDirectory );
                
                // SI EXISTEN ARCHIVOS LOS FILTRA POR PENDIENDES DE
                // SUBIR
                if ( fileListInDirectory != null
                        && fileListInDirectory.size() > 0 ) {
                    
                    fileListFiltered = filtrarListaDeNombresDeArchivosPorPendientesDeSubir(
                            fileListInDirectory, config.getStarWith() );
                    
                    LOG.info( "Lista de archivos pendientes de subir:  "
                            + fileListInDirectory );
                    
                    // SI EXISTEN ARCHIVOS PENDIENTES ELIMINA LOS QUE
                    // YA EXISTEN EN SFTP (CASO BORDE)
                    if ( fileListFiltered != null
                            && fileListFiltered.size() > 0 ) {
                        
                        fileListFiltered = filtraListaPorArchivosExistentesEnSFTPDestino(
                                parametros, fileListFiltered );
                        
                        if ( fileListFiltered != null
                                && fileListFiltered.size() > 0 ) {
                            
                            LOG.info( "Lista de archivos pendientes de subir:  "
                                    + fileListFiltered );
                            
                            subirArchivos( fileListFiltered, parametros );
                        }
                    }
                    else {
                        throw new AppException(
                                MsgUploadBol.NO_PENDING_FILES.toString() );
                    }
                }
            }
            else {
                throw new AppException( MsgUploadBol.NO_PARAMS.toString() );
                
            }
            
        }
        else {
            String asunto = MsgProceso.MSG_PROCESO_DESACTIVADO_ASUNTO
                    .toString().replace( ":?:",
                            PROCESO_ACTUAL.getCodigoProceso() );
            String mensaje = MsgProceso.MSG_PROCESO_DESACTIVADO_BODY.toString()
                    .replace( ":?:", PROCESO_ACTUAL.getCodigoProceso() );
            MailSendSrv.getInstance().sendMail( asunto, mensaje );
        }
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param fileListFiltered
     * @param config
     * 
     * @since 1.X
     */
    private List<String> obtenerArchivosControl(ParamUploadBolDTO parametros,
	    ConfigFile config) {
	List<String> listaArchivosControl = null;
	try {
	    listaArchivosControl = retrieveFilenameCtrList(
		    parametros.getPathLocalServer(), config);
	} catch (AppException e) {
	    e.printStackTrace();
	}
	return listaArchivosControl;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * Metodo que realiza la subida a SFTP de destino
     *
     * 
     * 
     * @param fileListFiltered
     * @throws AppException
     * @since 1.X
     */
    private void subirArchivos(List<String> fileListFiltered,
	    ParamUploadBolDTO parametros) throws AppException {

	List<String> controlFileList = obtenerArchivosControl(
		parametros,
		new ConfigFile(parametros.getFormatFilename(), parametros
			.getFormatExtName(), parametros.getFormatFilename(),
			parametros.getFormatExtNameCtrl()));

	LOG.info("Lista de archivos de control :" + controlFileList);

	for (String filename : fileListFiltered) {

	    try {
		// OBTENER ARCHIVO DE CONTROL DEL BOL (siempre será 1 : el
		// último)
		if (controlFileList != null) {
		    String controlFilename = obtenerNombreArchivoControlAsociadoABol(
			    filename, controlFileList, parametros);
		    // SUBE ARCHIVO DE CONTROL Y ARCHIVO BOL

		    LOG.info("nombre archivo control : " + controlFilename);
		    if (ClientFTPSrv.getInstance().uploadFileSFTP(
			    parametros.getDataFTP(),
			    parametros.getPathLocalServer(),
			    parametros.getPathFtpUpload(), filename)
			    && ClientFTPSrv.getInstance().uploadFileSFTP(
				    parametros.getDataFTP(),
				    parametros.getPathLocalServer(),
				    parametros.getPathFtpUpload(),
				    controlFilename)) {

			// SUBIDA OK UPDATEA ARCHIVO
			updatearRegistroPorNombre(filename,
				StatusProcessType.PROCESS_SUCESSFUL);

			// MUEVE A BKP BOL
			CommonsUtils.moveFile(parametros.getPathLocalServer()
				.concat(filename), parametros
				.getPathLocalServerBkp().concat(filename));

			// MUEVE A BKP ARCHIVO CONTROL
			CommonsUtils.moveFile(parametros.getPathLocalServer()
				.concat(controlFilename), parametros
				.getPathLocalServerBkp()
				.concat(controlFilename));

			LOG.info("ARCHIVO " + filename + "y archivo de control"
				+ " SUBIDO CORRECTAMENTE A FTP TBK= "
				+ parametros.getDataFTP().getIpHost());
		    } else {
			LOG.info("ARCHIVO " + filename + "y archivo de control"
				+ " No se ha sudido a A FTP TBK= ");

			// ERROR
			updatearRegistroPorNombre(filename,
				StatusProcessType.PROCESS_ERROR);

			// ERROR ARCHIVO BOL
			CommonsUtils.moveFile(parametros.getPathLocalServer()
				.concat(filename), parametros
				.getPathLocalServerError().concat(filename));

			// ERROR ARCHIVO DE CONTROL
			CommonsUtils.moveFile(
				parametros.getPathLocalServer().concat(
					controlFilename),
				parametros.getPathLocalServerError().concat(
					controlFilename));
		    }
		} else {
		    throw new AppException(
			    MsgUploadBol.NO_CONTROL_FILE.toString());
		}
	    } catch (Exception e) {
		throw new AppException(e);
	    }
	}

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param filename
     * @param controlFileList
     * @param parametros
     * @return
     * @since 1.X
     */
    private String obtenerNombreArchivoControlAsociadoABol(String filename,
	    List<String> controlFileList, ParamUploadBolDTO parametros) {

	filename = filename.replace("." + parametros.getFormatExtName(), "");

	for (String fileCtr : controlFileList) {
	    if (fileCtr.startsWith(filename)) {
		return fileCtr;
	    }
	}
	return filename;

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Mètodo que updatea un file por nombre en la tabla TBL_LOG_BOL de acuerdo
     * a
     * 
     * @param filename
     * @throws SQLException
     * @since 1.X
     */
    private void updatearRegistroPorNombre(String filename,
	    StatusProcessType status) throws SQLException {

	CommonsDao commonsDao = CommonsDaoFactory.getInstance()
		.getNewEntidadDao();

	commonsDao.updateLogBol(filename, status);
	commonsDao.endTx();
	commonsDao.close();

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * Metodo que filtra la lista de nombres de archivos, que son candidatos a
     * ser subidos a sftp, eliminando de esta los que ya existen en el sftp
     * 
     * @param parametros
     * @param fileUpload
     * @return
     * @throws AppException
     * @since 1.X
     */
    private List<String> filtraListaPorArchivosExistentesEnSFTPDestino(
	    ParamUploadBolDTO parametros, List<String> fileUpload)
	    throws AppException {
	// [BOL4903281.DAT,BOL4903281.DAT]
	List<String> filenamesList = new ArrayList<String>();

	// BUSCA LA LISTA DE TODOS LOS ARCHIVOS EXISTENTES EN EL SFTP
	List<String> listaArchivosEnSFTP = ClientFTPSrv.getInstance()
		.listFileSFTP(parametros.getDataFTP(),
			parametros.getPathFtpUpload());

	// SI EXISTE UNA LISTA SE COMPARA SI CONTIENE LOS ARCHIVOS CANDIDATOS
	if (listaArchivosEnSFTP != null && listaArchivosEnSFTP.size() > 0
		&& fileUpload != null && fileUpload.size() > 0) {

	    for (String fileLocal : fileUpload) {
		if (!listaArchivosEnSFTP.contains(fileLocal)) {
		    filenamesList.add(fileLocal);
		}
	    }

	} else {
	    filenamesList.addAll(fileUpload);
	}

	return filenamesList;
    }



    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * Metodo
     * 
     * @param fileDirectory
     * @param starWith
     * @return
     * @throws AppException
     * @since 1.X
     */
    private List<String> filtrarListaDeNombresDeArchivosPorPendientesDeSubir(
	    final List<String> fileDirectory, String starWith)
	    throws AppException {

	List<String> listFilesFiltered = null;
	CommonsDao daoCommons = null;

	try {
	    listFilesFiltered = new ArrayList<String>();
	    daoCommons = CommonsDaoFactory.getInstance().getNewEntidadDao();

	    // Lista de archivos que se pueden procesar.
	    for (String fileName : fileDirectory) {

		BolLogDTO log = daoCommons.getBolLogRegisterByName(fileName);

		if (log != null
			&& log.getFileFlag() != null
			&& StatusProcessType.PROCESS_PENDING.getValue()
				.equalsIgnoreCase(log.getFileFlag())) {
		    listFilesFiltered.add(fileName);

		} else {
		    LOG.info("Archivo no cumple con el formato para obtener la fecha al leer outgoing en TBL_LOG_VISA "
			    + fileName);
		}
	    }
	    return listFilesFiltered;
	} catch (SQLException e) {
	    throw new AppException(
		    MsgErrorSQL.ERROR_QUERY_PROCESS_UPLOAD.toString(), e);
	} finally {
	    try {
		if (daoCommons != null) {
		    daoCommons.close();
		}
	    } catch (SQLException e) {
		throw new AppException(
			MsgErrorSQL.ERROR_QUERY_PROCESS_CLOSE_UPLOAD.toString(),
			e);
	    }
	}
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * Metodo que busca los paramrtros
     * 
     * @return
     * @throws AppException
     * @since 1.X
     */
    private ParamUploadBolDTO obtenerParametros() throws AppException {
        
        CommonsDao daoCommon = null;
        List<ParametroDTO> paramDTOList = null;
        ParamUploadBolDTO params = null;
        
        try {
            daoCommon = CommonsDaoFactory.getInstance().getNewEntidadDao();
            paramDTOList = daoCommon
                    .getParametroCodGrupoDato( ConstantesPRTS.CRON_TBK
                            .toString() );
            
            if ( paramDTOList != null ) {
                
                // PARAMETROS DE CONEXION A FTP TBK
                DataFTP dataFtp = new DataFTP();
                dataFtp.setIpHost( CommonsUtils.getCodiDato( paramDTOList, ConstantesPRTS.SFTP_IP_UPLOAD_BOL_FILE.toString() ) );
                String port = CommonsUtils.getCodiDato(paramDTOList, ConstantesPRTS.SFTP_PORT_UPLOAD_BOL_FILE.toString()) ;
                dataFtp.setPort( Integer.parseInt((port != null && port.matches(ConstantesUtil.PATTERN_IS_NUMBER.toString()) ? port : ConstantesUtil.DEFAULT_PORT.toString())));
                dataFtp.setUser( CommonsUtils.getCodiDato( paramDTOList, ConstantesPRTS.SFTP_USER_UPLOAD_BOL_FILE.toString() ) );
                dataFtp.setPassword( CommonsUtils.getCodiDato( paramDTOList, ConstantesPRTS.SFTP_PASS_UPLOAD_BOL_FILE.toString() ) );
                
                // PARAMETROS DE PROCESO
                params = new ParamUploadBolDTO();
                
                params.setPathFtpUpload( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.PATH_IN_UPLD_BOL_FILE.toString() ) );
                
                // RUTA DE FOLDER EN SERVIDOR sbpay DONDE ESTAN LOS
                // BOL
                params.setPathLocalServer( CommonsUtils.getCodiDato(
                        paramDTOList, ConstantesPRTS.PATH_BOL.toString() ) );
                
                // RUTA EN SERVER sbpay DONDE SE DEJARAN LOS BKP
                params.setPathLocalServerBkp( CommonsUtils.getCodiDato(
                        paramDTOList, ConstantesPRTS.PATH_BOL_BKP.toString() ) );
                
                // FORMATO DE NOMBRE DE ARCHIVO BOL A SUBIR
                params.setPathLocalServerError( CommonsUtils.getCodiDato(
                        paramDTOList, ConstantesPRTS.PATH_BOL_ERROR.toString() ) );
                
                // FORMATO COMIENZA CON DE BOL
                params.setFormatFilename( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.FORMAT_FILENAME_BOL.toString() ) );
                
                // FORMATO EXTENSION DE ARCHIVO
                params.setFormatExtName( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.FORMAT_EXTNAME_BOL.toString() ) );
                
                // FORMATO DE EXTENSION DE ARCHIVO DE CONTROL
                params.setFormatExtNameCtrl( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.FORMAT_EXTNAME_BOL_CTR.toString() ) );
                
                params.setDataFTP( dataFtp );
                
                LOG.debug( "== PARAMETROS  : " + params );
                
            }
        }
        catch ( SQLException e ) {
            throw new AppException(
                    MsgErrorSQL.ERROR_QUERY_PRTS_UPLOAD.toString(), e );
        }
        finally {
            try {
                if ( daoCommon != null ) {
                    daoCommon.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException(
                        MsgErrorSQL.ERROR_QUERY_PRTS_UPLOAD_CLOSE.toString(), e );
            }
        }
        
        return params;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * Metodo que setea un archivo de configuraciòn
     * 
     * @param parametros
     * @return
     * @since 1.X
     */
    public ConfigFile getConfigFile(ParamUploadBolDTO parametros) {
	ConfigFile config = new ConfigFile(parametros.getFormatFilename(),
		parametros.getFormatExtName(), parametros.getFormatFilename(),
		parametros.getFormatExtNameCtrl());
	return config;

    }
    
}
