package com.sbpay.sgi.cron.step;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.enums.MsgExitoMail;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.srv.download.DownLoadOutgoingVisaNacSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 09/04/2019, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class StepDownloadOutgoingVisaNac {
	/** The Constant LOGGER. */
	private static final Logger LOG = Logger
			.getLogger(StepDownloadOutgoingVisaNac.class);

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 09/04/2019, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @throws Exception
	 * @since 1.x
	 */
	public void execute() throws Exception {
		try {
			LOG.info("=====> Inicio Proceso:  Descargar outgoing visa nacional desde SFTP <======");
			if (DownLoadOutgoingVisaNacSrv.getInstance()
					.descargarOutgoingVisaNac()) {
				LOG.info("Envio Correo de Descarga Satisfactoria: Descargar outgoing visa nacional desde SFTP");
				MailSendSrv
						.getInstance()
						.sendMailOk(
								MsgExitoMail.EXITO_DOWNLOAD_OUTGOING_VISA_NAC_TITLE
										.toString(),
								MsgExitoMail.EXITO_DOWNLOAD_OUTGOING_VISA_NAC_CAUSE
										.toString());
				LOG.info(" ======> Finaliza Proceso :DownLoad SFTP, no se ejecuta correctamente ======");
			} else {
				LOG.info(" ======> Finaliza Proceso :DownLoad Outgoing visa nacional desde SFTP, con observaciones. ======");
			}
		} catch (AppException e) {
			LOG.error(e.getMessage(), e);
			MailSendSrv.getInstance().sendMail(
					MsgProceso.MSG_GENERIC_ERROR_MSJ.toString(),
					(e.getStackTraceDescripcion() != null) ? e
							.getStackTraceDescripcion() : e.getMessage());
		}
	}

}
