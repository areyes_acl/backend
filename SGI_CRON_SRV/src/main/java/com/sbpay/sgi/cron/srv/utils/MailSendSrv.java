package com.sbpay.sgi.cron.srv.utils;

import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dto.ParamMailDTO;
import com.sbpay.sgi.cron.dto.ParametroUserDTO;
import com.sbpay.sgi.cron.enums.ConstantesPRTS;
import com.sbpay.sgi.cron.enums.MsgErrorMail;
import com.sbpay.sgi.cron.enums.MsgErrorSQL;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;

/**
 * 
 * Clase encargada del envio de correo.
 * 
 * @author dmedina
 * @date 03-11-2015
 *
 */
public final class MailSendSrv extends CommonSrv {
    /** The Constant LOGGER. */
	private static final Logger LOG = Logger
			.getLogger(MailSendSrv.class.getName());
    
    /** The single instance. */
    private static MailSendSrv singleINSTANCE = null;
    
    /**
     * Creates the instance.
     */
    private static void createInstance() {
        synchronized ( MailSendSrv.class ) {
            if ( singleINSTANCE == null ) {
                singleINSTANCE = new MailSendSrv();
            }
        }
    }
    
    /**
     * Patron singleton.
     * 
     * @return MailSendSrv retorna instancia del servicio.
     */
    public static MailSendSrv getInstance() {
        if ( singleINSTANCE == null ) {
            createInstance();
        }
        return singleINSTANCE;
    }
    
    /**
     * Metodo de restricion de sobreescritura de la clase.
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        super.clone();
        throw new CloneNotSupportedException();
    }
    
    /**
     * Constructor Privado.
     */
    private MailSendSrv() {
        super();
    }
    
    /**
     * Metodo envia correo electronico informando algun estado no
     * desao del cron de transbank.
     * 
     * @param cause
     *            titulo del correo.
     * @param contenido
     *            contenido del correo.
     */
    public void sendMail( final String cause, final String contenido ) {
        Properties props = null;
        Session session = null;
        Message message = null;
        String asunto = "[:ambiente:] :titulo:";
        
        try {
            ParamMailDTO pMailDto = getParametrosMail();
            LOG.info( "Parametros : " + pMailDto );
            props = setPropertiesSendMail( pMailDto );
            session = setSessionSendMail( props,
                    Integer.parseInt( pMailDto.getMailSecurity() ), pMailDto );
            
            asunto = asunto.replace( ":ambiente:", pMailDto.getEnvironment() );
            asunto = asunto.replace( ":titulo:", cause );
            
            message = setMessageMail( session, pMailDto, asunto, contenido );
            
            Transport.send( message );
            LOG.info( "SE ENVIA MAIL CORRECTAMENTE" );
        }
        catch ( MessagingException e ) {
			LOG.error(MsgErrorMail.ERROR_MSG_SEND.toString(), e);
        }
        catch ( Exception e ) {
        	LOG.error(MsgErrorMail.ERROR_MSG_SEND.toString(), e);
            
        }
        
    }
    
    /**
     * 
     * @param session
     * @param pMailDto
     * @param titulo
     * @param contenido
     * @return
     * @throws MessagingException
     */
    private Message setMessageMail( final Session session,
            final ParamMailDTO pMailDto, final String titulo,
            final String contenido ) throws MessagingException {
        Message message = new MimeMessage( session );
        
        message.setFrom( new InternetAddress( pMailDto.getMailUser() ) );
        
        message.setRecipients( Message.RecipientType.TO,
                InternetAddress.parse( pMailDto.getMailDest() ) );
        message.setSubject( titulo );
        message.setText( contenido );
        
        return message;
    }
    
    /**
     * 
     * @param props
     *            propiedades de la seguridad del mail
     * @param mailSecurity
     * @param pMailDto
     * @return
     */
    private Session setSessionSendMail( final Properties props,
            final int mailSecurity, final ParamMailDTO pMailDto ) {
        Session session = null;
        switch ( mailSecurity ) {
            case 1:
                // SSL
                session = Session.getDefaultInstance( props,
                        new javax.mail.Authenticator() {
                            protected PasswordAuthentication getPasswordAuthentication() {
                                return new PasswordAuthentication( pMailDto
                                        .getMailUser(), pMailDto.getMailPass() );
                            }
                        } );
                break;
            case 2:
                // TLS
                session = Session.getInstance( props,
                        new javax.mail.Authenticator() {
                            protected PasswordAuthentication getPasswordAuthentication() {
                                return new PasswordAuthentication( pMailDto
                                        .getMailUser(), pMailDto.getMailPass() );
                            }
                        } );
                break;
            case 0:
                session = Session.getInstance( props);
                break;
            default:
                session = null;
                break;
        }
        return session;
    }
    
    /**
     * Metodo setea las propiedades del mail segun el tipo de
     * seguridad.
     * 
     * @param paramMailDto
     *            DTO de parametros de configuracion del envio de
     *            correo.
     * @return Properties propiedades de configuracion del correo.
     */
    private Properties setPropertiesSendMail( final ParamMailDTO pMailDto ) {
        Properties props = new Properties();
        switch ( Integer.parseInt( pMailDto.getMailSecurity() ) ) {
            case 1:
                // SSL
                props.put( "mail.smtp.host", pMailDto.getMailSmtp() );
                props.put( "mail.smtp.socketFactory.port",
                        Integer.toString( pMailDto.getMailPort() ) );
                props.put( "mail.smtp.socketFactory.class",
                        "javax.net.ssl.SSLSocketFactory" );
                props.put( "mail.smtp.auth", "true" );
                props.put( "mail.smtp.port",
                        Integer.toString( pMailDto.getMailPort() ) );
                break;
            case 2:
                // TLS
                props.put( "mail.smtp.auth", "true" );
                props.put( "mail.smtp.starttls.enable", "true" );
                props.put( "mail.smtp.host", pMailDto.getMailSmtp() );
                props.put( "mail.smtp.port",
                        Integer.toString( pMailDto.getMailPort() ) );
                break;
            case 0:
              props.put( "mail.smtp.auth", "false" );
              props.put( "mail.smtp.starttls.enable", "true" );
              props.put( "mail.smtp.host", pMailDto.getMailSmtp() );
              props.put( "mail.smtp.port",
                      Integer.toString( pMailDto.getMailPort() ) );
              
                break;
            default:
                props = null;
                break;
        }
        return props;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 - 25/11/2015, (ACL) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @throws AppException
     * @since 1.X
     */
    private ParamMailDTO getParametrosMail() throws AppException {
        CommonsDao daoCommon = null;
        List<ParametroUserDTO> paramDTOList = null;
        ParamMailDTO paramMailDto = null;
        try {
            daoCommon = CommonsDaoFactory.getInstance().getNewEntidadDao();
            paramDTOList = daoCommon
                    .getParametroCodGrupoDatoUser( ConstantesPRTS.MAIL_CONFIG
                            .toString() );
            if ( paramDTOList != null) {
                paramMailDto = new ParamMailDTO();
                paramMailDto.setMailSmtp( CommonsUtils.getCodiDatoUser( paramDTOList,
                        ConstantesPRTS.MAIL_SMTP.toString() ) );
                
                paramMailDto.setMailPort( Integer.parseInt( CommonsUtils.getCodiDatoUser(
                        paramDTOList, ConstantesPRTS.MAIL_PORT.toString() ) ) );
                paramMailDto.setMailUser( CommonsUtils.getCodiDatoUser( paramDTOList,
                        ConstantesPRTS.MAIL_USER.toString() ) );
                paramMailDto.setMailPass( CommonsUtils.getCodiDatoUser( paramDTOList,
                        ConstantesPRTS.MAIL_PASS.toString() ) );
                paramMailDto.setMailSecurity( CommonsUtils.getCodiDatoUser( paramDTOList,
                        ConstantesPRTS.MAIL_SECURITY.toString() ) );
                paramMailDto.setMailAuth( CommonsUtils.getCodiDatoUser( paramDTOList,
                        ConstantesPRTS.MAIL_AUTH.toString() ) );
                paramMailDto.setMailDest( CommonsUtils.getCodiDatoUser( paramDTOList,
                        ConstantesPRTS.MAIL_DEST.toString() ) );
                paramMailDto.setMailDestOk( CommonsUtils.getCodiDatoUser( paramDTOList,
                        ConstantesPRTS.MAIL_DEST_OK.toString() ) );
                paramMailDto.setEnvironment( CommonsUtils.getCodiDatoUser( paramDTOList,
                        ConstantesPRTS.MAIL_ENVIRONMENT.toString() ));
            }
            else {
                throw new AppException(
                        MsgErrorSQL.ERROR_MAIL_PARAMETERS_NOT_FOUND.toString() );
            }
            return paramMailDto;
        }
        catch ( SQLException e ) {
            throw new AppException( MsgErrorSQL.ERROR_MAIL_PRTS.toString(), e );
        }
        finally {
            try {
                if ( daoCommon != null ) {
                    daoCommon.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException(
                        MsgErrorSQL.ERROR_MAIL_CLOSE.toString(), e );
            }
        }
    }  
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 - 25/11/2015, (ACL) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @throws AppException
     * @since 1.X
     */
    private ParamMailDTO getParametrosMailVenc() throws AppException {
        CommonsDao daoCommon = null;
        List<ParametroUserDTO> paramDTOList = null;
        ParamMailDTO paramMailDto = null;
        try {
            daoCommon = CommonsDaoFactory.getInstance().getNewEntidadDao();
            paramDTOList = daoCommon
                    .getParametroCodGrupoDatoUser( ConstantesPRTS.MAIL_CONFIG
                            .toString() );
            if ( paramDTOList != null) {
                paramMailDto = new ParamMailDTO();
                paramMailDto.setMailSmtp( CommonsUtils.getCodiDatoUser( paramDTOList,
                        ConstantesPRTS.MAIL_SMTP.toString() ) );
                
                paramMailDto.setMailPort( Integer.parseInt( CommonsUtils.getCodiDatoUser(
                        paramDTOList, ConstantesPRTS.MAIL_PORT.toString() ) ) );
                paramMailDto.setMailUser( CommonsUtils.getCodiDatoUser( paramDTOList,
                        ConstantesPRTS.MAIL_USER.toString() ) );
                paramMailDto.setMailPass( CommonsUtils.getCodiDatoUser( paramDTOList,
                        ConstantesPRTS.MAIL_PASS.toString() ) );
                paramMailDto.setMailSecurity( CommonsUtils.getCodiDatoUser( paramDTOList,
                        ConstantesPRTS.MAIL_SECURITY.toString() ) );
                paramMailDto.setMailAuth( CommonsUtils.getCodiDatoUser( paramDTOList,
                        ConstantesPRTS.MAIL_AUTH.toString() ) );
                paramMailDto.setMailDest( CommonsUtils.getCodiDatoUser( paramDTOList,
                        ConstantesPRTS.MAIL_DEST.toString() ) );
                paramMailDto.setEnvironment( CommonsUtils.getCodiDatoUser( paramDTOList,
                        ConstantesPRTS.MAIL_ENVIRONMENT.toString() ) );
            }
            else {
                throw new AppException(
                        MsgErrorSQL.ERROR_MAIL_PARAMETERS_NOT_FOUND.toString() );
            }
            return paramMailDto;
        }
        catch ( SQLException e ) {
            throw new AppException( MsgErrorSQL.ERROR_MAIL_PRTS.toString(), e );
        }
        finally {
            try {
                if ( daoCommon != null ) {
                    daoCommon.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException(
                        MsgErrorSQL.ERROR_MAIL_CLOSE.toString(), e );
            }
        }
    }
    
    /**
     * 
     * @param cause
     * @param contenido
     */
    public void sendMailOk( final String cause, final String contenido ) {
        Properties props = null;
        Session session = null;
        Message message = null;
        String asunto = "[:ambiente:] :titulo:";
        try {
            ParamMailDTO pMailDto = getParametrosMail();
            props = setPropertiesSendMail( pMailDto );
            session = setSessionSendMail( props,
                    Integer.parseInt( pMailDto.getMailSecurity() ), pMailDto );
            
            asunto = asunto.replace( ":ambiente:", pMailDto.getEnvironment() );
            asunto = asunto.replace( ":titulo:", cause );
            
            message = setMessageMailOk( session, pMailDto, asunto, contenido );
            
            Transport.send( message );
            LOG.info( "SE ENVIA MAIL CORRECTAMENTE" );
        }
        catch ( MessagingException e ) {
			LOG.error(MsgErrorMail.ERROR_MSG_SEND.toString(), e);
			
        }
        catch ( Exception e ) {
        	LOG.error(MsgErrorMail.ERROR_MSG_SEND.toString(), e);            
        }
        
    }
    
    /**
     * 
     * @param cause
     * @param contenido
     */
    public void sendMailOkHTML( final String cause, final String contenido ) {
        Properties props = null;
        Session session = null;
        Message message = null;
        String asunto = "[:ambiente:] :titulo:";
        try {
            ParamMailDTO pMailDto = getParametrosMailVenc();
            props = setPropertiesSendMail( pMailDto );
            session = setSessionSendMail( props,
                    Integer.parseInt( pMailDto.getMailSecurity() ), pMailDto );
            
            asunto = asunto.replace( ":ambiente:", pMailDto.getEnvironment() );
            asunto = asunto.replace( ":titulo:", cause );
            
            message = setMessageMailOk( session, pMailDto, asunto, contenido );
            message.setContent(contenido, "text/html; charset=utf-8");
            Transport.send( message );
        }
        catch ( MessagingException e ) {
			LOG.error(MsgErrorMail.ERROR_MSG_SEND.toString(), e);
        }
        catch ( Exception e ) {
        	LOG.error(MsgErrorMail.ERROR_MSG_SEND.toString(), e);            
        }
        
    }
    
    /**
     * 
     * @param session
     * @param pMailDto
     * @param titulo
     * @param contenido
     * @return
     * @throws MessagingException
     */
    private Message setMessageMailOk( final Session session,
            final ParamMailDTO pMailDto, final String titulo,
            final String contenido ) throws MessagingException {
        Message message = new MimeMessage( session );
        
        message.setFrom( new InternetAddress( pMailDto.getMailUser() ) );
        
        message.setRecipients( Message.RecipientType.TO,
                InternetAddress.parse( pMailDto.getMailDestOk() ) );
        message.setSubject( titulo );
        message.setText( contenido );
        
        return message;
    }
    
}
