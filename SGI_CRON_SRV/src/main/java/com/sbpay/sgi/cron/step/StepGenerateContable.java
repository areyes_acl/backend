package com.sbpay.sgi.cron.step;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.enums.MsgGenerationCntbl;
import com.sbpay.sgi.cron.srv.export.GenerarContableSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 12/02/2016, (ACL) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SA.</B>
 */
public class StepGenerateContable {
    
    /**
     * VARIABLES
     */
    private static final Logger LOG = Logger
            .getLogger( StepGenerateContable.class );
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (Everis Chile) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @throws Exception
     * @since 1.X
     */
	public void execute() {
		LOG.info("Inicio proceso de Generacion de archivo contable : StepGenerateContable ");

		try {
			if (GenerarContableSrv.getInstance().generarArchivoContable()) {
				MailSendSrv.getInstance().sendMailOk(MsgGenerationCntbl.SUCCESFUL_GENERATE_CNTBL_TITTLE.toString(),
													 MsgGenerationCntbl.SUCCESFUL_GENERATE_CNTBL_MSG.toString());
			} else {
				LOG.info("Generar/Exportar Asientos contables  no realizados, Finaliza!");
			}
		} catch (AppException e) {
			LOG.error(e.getMessage(),e);
			MailSendSrv.getInstance().sendMail(MsgGenerationCntbl.ERROR_GEN_CNTBL_FILE.toString(),
  		          (e.getStackTraceDescripcion()!= null)?e.getStackTraceDescripcion(): e.getMessage());
		}

	}
    
}
