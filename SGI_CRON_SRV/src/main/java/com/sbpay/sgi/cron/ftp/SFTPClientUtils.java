package com.sbpay.sgi.cron.ftp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dto.DataFTP;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.MsgErrorSFTP;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.jcraft.jsch.UserInfo;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 18/1/2016, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class SFTPClientUtils implements FTPFileClient {
    
	 private static final Logger LOGGER = Logger.getLogger( SFTPClientUtils.class );
    
	 /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 16/12/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @param dataFTP
     *            Contiene los datos necesarios para subir archivo a
     *            ftp
     * @param ftp
     * @throws AppException
     * @since 1.X
     */
    public Boolean upload( DataFTP dataFTP, String pathOrigen,
            String pathDestino, String filename ) throws AppException {
        ChannelSftp sftp = null;
        FileInputStream in = null;
        try {
            sftp = getConexionSFTP( dataFTP );
            sftp.cd( pathDestino );
            File file = new File( pathOrigen.concat( filename ) );
            in = new FileInputStream( file );
            sftp.put( in, file.getName() );
            return Boolean.TRUE;
        }
        catch ( SftpException e ) {
            throw new AppException(
                    MsgErrorSFTP.ERROR_UPLOAD_READ_FILE_SFTP.toString(), e );
        }
        catch ( IOException e ) {
            throw new AppException(
                    MsgErrorSFTP.ERROR_UPLOAD_READ_FILE_SFTP.toString(), e );
        }
        finally {
            try {
                if ( in != null ) {
                    in.close();
                }
                
                closeConectionSFTP( sftp );
            }
            catch ( IOException e ) {
                throw new AppException(MsgErrorSFTP.ERROR_CLOSE_FILE_SFTP.toString(), e );
            }
        }
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 17/12/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * Metodo que realiza la descarga de un archivo desde ftp
     * 
     * @param ftpConection
     *            Objeto que maneja la conexión al ftp
     * @param pathOrigen
     *            Ruta del archivo en el ftp
     * @param pathDestino
     *            Ruta del archivo donde se descargara
     * @return
     * @throws AppException
     * @since 1.X
     */
    public Boolean download( DataFTP dataFTP, String pathFtpOrigen,
            String pathDestino, String filename ) throws AppException {
        
        Boolean flag = false;
        File file = null;
        FileOutputStream fileOutputStream = null;
        ChannelSftp sftp = null;
        try {
            sftp = getConexionSFTP( dataFTP );
            sftp.cd( pathFtpOrigen );
            
            file = new File( pathDestino + filename );
            file.setExecutable( true );
            file.setReadable( true );
            file.setWritable( true );
            fileOutputStream = new FileOutputStream( file );
            sftp.get( filename, fileOutputStream );
            fileOutputStream.close();
            
        }
        catch ( SftpException e ) {
            throw new AppException( MsgErrorSFTP.ERROR_DOWNLOAD_FILE_SFTP.toString(),
                    e );
        }
        catch ( IOException e ) {
            throw new AppException( MsgErrorSFTP.ERROR_DOWNLOAD_FILE_SFTP.toString(),e );
        }
        finally {
            try {
                if ( fileOutputStream != null ) {
                    fileOutputStream.close();
                }
                
                if ( file != null ) {
                    file = null;
                }
                
                closeConectionSFTP( sftp );
                
            }
            catch ( IOException e ) {
                flag = Boolean.FALSE;
                throw new AppException(MsgErrorSFTP.ERROR_CLOSE_FILE_SFTP.toString(), e );
            }
        }
        return flag;
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 21/12/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * Metodo que regresa una lista de
     * 
     * @return
     * @throws IOException
     * @throws AppException
     * @since 1.X
     */
    public List<String> listFiles( DataFTP dataFtp, String folderPath )
            throws AppException {
        List<String> listaArchivos = new ArrayList<String>();
        ChannelSftp sftp = null;
        try {
            sftp = getConexionSFTP( dataFtp );
            sftp.cd( folderPath );
            Vector<ChannelSftp.LsEntry> list = sftp.ls( ConstantesUtil.ASTERISK.toString() );
            for ( ChannelSftp.LsEntry entry : list ) {
                if ( !entry.getAttrs().isDir() ) {
                    listaArchivos.add( entry.getFilename() );
                }
                
            }
        }
        catch ( SftpException e ) {
            throw new AppException(MsgErrorSFTP.ERROR_LIST_SFTP.toString(),e );
        }
        finally {
            closeConectionSFTP( sftp );
        }
        
        return listaArchivos;
    }
    
    /**
     * Metodo permite la conexion al FTP de transbank
     * 
     * @return Cliente conectado al FTP.
     * @throws AppException
     *             Exception de la App.
     */
    private ChannelSftp getConexionSFTP( DataFTP dataFTP ) throws AppException {
        ChannelSftp sftp = null;
        JSch jsch = null;
        Session session = null;
        try {
            jsch = new JSch();
            session = jsch.getSession( dataFTP.getUser(), dataFTP.getIpHost(),
                    dataFTP.getPort() );
            
            UserInfo ui = new SUserInfo( dataFTP.getPassword(), null );
            session.setUserInfo( ui );
            session.setPassword( dataFTP.getPassword() );
            session.connect();
            
            sftp = ( ChannelSftp ) session.openChannel( "sftp" );
            sftp.connect();
            
            if ( sftp.isConnected() ) {
            	LOGGER.info("ESTA CONECTADO A SFTP " );
                return sftp;
            }
            return null;
            
        }
        catch ( JSchException e ) {
            throw new AppException(
                    MsgErrorSFTP.ERROR_CONNECTION_SFTP.toString(), e );
        }
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 21/12/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @param ftpClient
     * @throws IOException
     * @since 1.X
     */
    private void closeConectionSFTP( ChannelSftp sftp ) throws AppException {
        try {
            // Verifica si el control de la conexion esta ok
        	LOGGER.info( " Se cierra conexion a SFTP " );
            if ( sftp != null ) {
                sftp.exit();
                sftp.disconnect();
                sftp.getSession().disconnect();
            }
            
        }
        catch ( JSchException e ) {
            throw new AppException( e.getMessage(), e );
        }
        
    }
    
}
