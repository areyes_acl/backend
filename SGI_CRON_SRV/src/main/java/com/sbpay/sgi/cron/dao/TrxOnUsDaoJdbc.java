package com.sbpay.sgi.cron.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import oracle.jdbc.OracleCallableStatement;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.dao.BaseDao;
import com.sbpay.sgi.cron.dto.TransaccionOnUsDTO;
import com.sbpay.sgi.cron.enums.ConstantesBD;

public class TrxOnUsDaoJdbc extends BaseDao<TrxOnUsDao> implements TrxOnUsDao {
    
    /** VARIABLE PARA EL LOGER */
    private static final Logger LOGGER = Logger.getLogger( TrxOnUsDaoJdbc.class );
    
    /**
     * Constructor con conexion.
     * 
     * @param connection
     *            Conexion JDBC.
     */
    public TrxOnUsDaoJdbc( Connection connection ) {
        super( connection );
    }

	@Override
	public boolean saveTransaction(final TransaccionOnUsDTO transaccion)
			throws SQLException {
	    PreparedStatement stmt = null;
	    try {
	      StringBuilder str =
	              new StringBuilder()
	                  .append("INSERT INTO ")
	                  .append(ConstantesBD.ESQUEMA.toString())
	                  .append(".TMP_TRANSAC_ONUS ")
	                  .append("(SID,CODIGO_TRANSACCION,NUMERO_TARJETA,FECHA_COMPRA,FECHA_AUTORIZACION,FECHA_POSTEO,")
	                  .append("TIPO_VENTA,NUM_CUOTAS,NUM_MICROFILM,NUM_COMERCIO,MONTO_TRANSACCION,VALOR_CUOTA,")
	                  .append("NOMBRE_COMERCIO,CIUDAD_COMERCIO,RUBRO_COMERCIO,COD_AUTOR)")
	                  .append(" values (SEQ_TMP_TRANSAC_ONUS.nextval , ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

	          stmt = this.getConnection().prepareStatement(str.toString());

	          setStringNull(transaccion.getCodigoTransaccion(), 1, stmt);
	          setStringNull(transaccion.getNumeroTarjeta(), 2, stmt);
	          setStringNull(transaccion.getFechaCompra(), 3, stmt);
	          setStringNull(transaccion.getFechaAutorizacion(), 4, stmt);
	          setStringNull(transaccion.getFechaPosteo(), 5, stmt);
	          setStringNull(transaccion.getTipoVenta(), 6, stmt);
	          setStringNull(transaccion.getNumCuotas(), 7, stmt);
	          setStringNull(transaccion.getNumMicrofilm(), 8, stmt);
	          setStringNull(transaccion.getNumComercio(), 9, stmt);
	          setStringNull(transaccion.getMontoTransac(), 10, stmt);
	          setStringNull(transaccion.getValorCuota(), 11, stmt);
	          setStringNull(transaccion.getNombreComercio(), 12, stmt);
	          setStringNull(transaccion.getCiudadComercio(), 13, stmt);
	          setStringNull(transaccion.getRubroComercio(), 14, stmt);
	          setStringNull(transaccion.getCodAutor(), 15, stmt);


	          return (stmt.executeUpdate() > 0);
	    } finally {
	      if (stmt != null) {
	        stmt.close();
	      }
	    }
	}

	@Override
	public boolean callSPCargaLogTrxOnUs() throws SQLException {
	    Connection dbConnection = null;
	    OracleCallableStatement callableStatement = null;

	    try {
	      String callSPCargaIncomingSql = "{call SP_SIG_CARGA_LOG_TRX_ONUS(?,?)}";

	      dbConnection = getConnection();
	      callableStatement =
	          (OracleCallableStatement) dbConnection.prepareCall(callSPCargaIncomingSql);

	      callableStatement.registerOutParameter(1, java.sql.Types.VARCHAR);
	      callableStatement.registerOutParameter(2, java.sql.Types.NUMERIC);

	      callableStatement.executeUpdate();

	      String warning = callableStatement.getString(1);
	      Integer codigoError = callableStatement.getInt(2);
	      LOGGER.info("****** SE EJECUTA STORE PROCEDURE SP_SIG_CARGA_LOG_TRX_ONUS *****");
	      LOGGER.info("WARNING : " + warning);
	      LOGGER.info("codigoError : " + codigoError);
	      LOGGER.info("*********** STORE PROCEDURE SP_SIG_CARGA_LOG_TRX_ONUS *********** ");

	    } finally {
	      if (callableStatement != null) {
	        callableStatement.close();
	      }
	    }
	    return Boolean.TRUE;
		
	}
    
 

}
