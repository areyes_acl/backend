package com.sbpay.sgi.cron.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.dao.BaseDao;
import com.sbpay.sgi.cron.dto.TmpRechazosDTO;
import com.sbpay.sgi.cron.enums.ConstantesBD;

public class RechazarDaoJdbc extends BaseDao<RechazarDao> implements
		RechazarDao {
    /** VARIABLE PARA EL LOGER */
	private static final Logger LOG = Logger.getLogger(RechazarDaoJdbc.class);
	/**
	 * Constructor con conexion.
	 * 
	 * @param connection Conexion JDBC.
	 */
	public RechazarDaoJdbc(Connection connection) {
		super(connection);
	}


	@Override
	public boolean saveRchTransaction(TmpRechazosDTO dto)
			throws SQLException {
		PreparedStatement stmt = null;
		boolean flag = false;
		String operador;
		try {
			StringBuilder str = new StringBuilder()
			.append( "INSERT INTO " )
            .append( ConstantesBD.ESQUEMA.toString() )
            .append( ".TMP_RECHAZOS  " )
            .append(" (SID, MERCHANT_CODE_ORIGINAL, MERCHANT_CODE_IC,  ")
            .append(" MERCHANT_NAME, REFERENCE_NRO, TRANSACTION_CODE, ")
            .append(" AUTH_NRO, TRANSACTION_DATE, CARD_NRO, DEBIT_CREDIT, ")
            .append(" CURRENCY, GROSS_AMOUNT, REJECT_REASON, OPERADOR) ")
            .append(" VALUES (SEQ_TMP_INCOMING.nextval, ")
            .append(" ?,?,?,?,?,?,?,?,?,?,?,?,?)");
			
			LOG.debug("SQL save rechazo -> " + str.toString());
			LOG.debug(" TmpRechazosDTO " +dto.toString());
            stmt = this.getConnection().prepareStatement( str.toString() );
            setStringNull( dto.getMerchantCodeOriginal(), 1, stmt );//MERCHANT_CODE_ORIGINAL
            setStringNull( dto.getMerchantCodeIc(), 2, stmt );//MERCHANT_CODE_IC
			setStringNull(dto.getMerchantName(), 3, stmt);//MERCHANT_NAME
			setStringNull(dto.getReferenceNro(), 4, stmt);//REFERENCE_NRO
			setStringNull(dto.getTransactionCode(), 5, stmt);//TRANSACTION_CODE
			setStringNull(dto.getAuthNro(), 6, stmt);// AUTH_NRO
			setStringNull(dto.getTransactionDate(), 7, stmt);// TRANSACTION_DATE
			setStringNull(dto.getCardNro(), 8, stmt);//CARD_NRO
			setStringNull(dto.getDebitCredit(), 9, stmt);//DEBIT_CREDIT
			setStringNull(dto.getCurrency(), 10, stmt);//CURRENCY
			setStringNull(dto.getGrossAmount(), 11, stmt);//GROSS_AMOUNT
			setStringNull(dto.getRejectReason(), 12, stmt);//REJECT_REASON
			
			operador = dto.getOperador();
			if(operador.equals("2")){
				operador = "3";
			}
			else if(operador.equals("3")){
				operador = "1";
			}
			else if(operador.equals("1")){
				operador = "2";
			}			
			setStringNull(operador, 13, stmt);//OPERADOR
			flag = stmt.executeUpdate() > 0; 
			
			return ( flag );
		} finally {
			if (stmt != null) {
				stmt.close();
			}
		}

	}



	@Override
	public void callSPCargaRechazos() throws SQLException {

		LOG.info("******SE EJECUTA STORE PROCEDURE "+CALL_SP_SIG_CMPRECHAZOINCOMING+"*****");
		CallableStatement cs = null;

		try {

			cs = this.getConnection().prepareCall(
					CALL_SP_SIG_CMPRECHAZOINCOMING);
			cs.registerOutParameter(1, java.sql.Types.VARCHAR);
			cs.registerOutParameter(2, java.sql.Types.NUMERIC);
			cs.execute();

			String warning = cs.getString(1);
			Integer codigoError = cs.getInt(2);

			LOG.info("WARNING : " + warning);
			LOG.info("codigoError : " + codigoError);
		} finally {
			if (cs != null) {
				cs.close();
			}
		}
	}
}