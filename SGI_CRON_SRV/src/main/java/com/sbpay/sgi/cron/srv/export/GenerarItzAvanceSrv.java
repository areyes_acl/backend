package com.sbpay.sgi.cron.srv.export;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dao.GenerarItzAvanceDao;
import com.sbpay.sgi.cron.dao.GenerarItzAvanceDaoFactory;
import com.sbpay.sgi.cron.dto.ParamExportCnblDTO;
import com.sbpay.sgi.cron.dto.ParamExportItzAvanceDTO;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.dto.TblCronLogDTO;
import com.sbpay.sgi.cron.dto.TmpExportContableDTO;
import com.sbpay.sgi.cron.dto.TmpExportItzAvanceDTO;
import com.sbpay.sgi.cron.enums.ConstantesPRTS;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.DateFormatType;
import com.sbpay.sgi.cron.enums.LogBD;
import com.sbpay.sgi.cron.enums.MsgErrorMail;
import com.sbpay.sgi.cron.enums.MsgGenerationItzAvance;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.enums.StatusProcessType;
import com.sbpay.sgi.cron.srv.utils.CommonSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;
import com.sbpay.sgi.cron.utils.file.AvancesExport;
import com.sbpay.sgi.cron.utils.file.ContableExport;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;
import com.sbpay.sgi.cron.utils.generals.DateUtils;
import com.sbpay.sgi.cron.validation.ProcessValidator;
import com.sbpay.sgi.cron.validation.impl.ProcessValidatorImpl;

public class GenerarItzAvanceSrv extends CommonSrv {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = Logger
			.getLogger(GenerarItzAvanceSrv.class);
	/** The single instance. */
	private static GenerarItzAvanceSrv singleINSTANCE = null;
	/** variable parametros del Cron */

	private final ProcessCodeType PROCESO_ACTUAL = ProcessCodeType.GENERAR_ARCHIVO_AVA;

	/**
	 * Creates the instance.
	 */
	private static void createInstance() {
		synchronized (GenerarItzAvanceSrv.class) {
			if (singleINSTANCE == null) {
				singleINSTANCE = new GenerarItzAvanceSrv();

			}
		}
	}

	/**
	 * Patron singleton.
	 * 
	 * @return GenerarContableSrv retorna instancia del servicio.
	 */
	public static GenerarItzAvanceSrv getInstance() {
		if (singleINSTANCE == null) {
			createInstance();
		}
		return singleINSTANCE;
	}

	/**
	 * Metodo de restricion de sobreescritura de la clase.
	 */
	@Override
	protected Object clone() throws CloneNotSupportedException {
		super.clone();
		throw new CloneNotSupportedException();
	}

	/**
	 * Constructor Privado.
	 */
	private GenerarItzAvanceSrv() {
		super();
	}

	/**
	 * 
	 * @return
	 * @throws AppException
	 */
	public boolean generarArchivoAvances() throws AppException {
		Boolean flag = Boolean.FALSE;
		GenerarItzAvanceDao generarAvanceDao = null;
		ParamExportItzAvanceDTO paramExportItzAvanceDTO = null;

		try {
			ProcessValidator validadorProceso = new ProcessValidatorImpl();

			if (validadorProceso.isActiveProcess(PROCESO_ACTUAL)) {

				paramExportItzAvanceDTO = getParametrosUploadItzAvances();

				String Filename = generateContableFilename(paramExportItzAvanceDTO);

				if (!validadorProceso.hasTheValidParametersForTheProcess(
						PROCESO_ACTUAL, paramExportItzAvanceDTO)) {
					LOGGER.warn(MsgGenerationItzAvance.ERROR_PARAM_NOT_FOUND
							.toString());
					MailSendSrv.getInstance().sendMail(
							MsgErrorMail.ALERTA_PRTS.toString(),
							MsgErrorMail.ALERTA_PRTS_TXT.toString());
				}

				LOGGER.info("=== SE ENCONTRARON TODOS LOS PARAMETROS NECESARIOS PARA EL PROCESO, SE INICIA GENERACION DE INTERFAZ DE AVANCES CON TRANSFERENCIA... ===");

				generarAvanceDao = GenerarItzAvanceDaoFactory.getInstance()
						.getNewEntidadDao();

				List<TmpExportItzAvanceDTO> lstTmpExportAvanceDTO = generarAvanceDao
						.obtenerInformacion();

				if (lstTmpExportAvanceDTO != null
						&& lstTmpExportAvanceDTO.size() > 0) {

					String avanceFilename = generateContableFilename(paramExportItzAvanceDTO);
					String ruta = paramExportItzAvanceDTO.getPathSalida();

					AvancesExport exportOut = new AvancesExport();

					exportOut.exportAvanceFile(ruta, avanceFilename,
							lstTmpExportAvanceDTO);

					LOGGER.info("=== SE REALIZA LA INTERFAZ DE AVANCES CON TRANSFERENCIA CON NOMBRE: "
							+ avanceFilename + " EN RUTA: " + ruta + " ===");

					flag = true;
				} else {
					LOGGER.warn(MsgGenerationItzAvance.WARNING_DATA_NOT_FOUND
							.toString());

					MailSendSrv
							.getInstance()
							.sendMail(
									MsgGenerationItzAvance.ERROR_GEN_ITZ_FILE
											.toString(),
									MsgGenerationItzAvance.WARNING_DATA_NOT_FOUND
											.toString());
					flag = false;
				}

				return flag;

			} else {
				MailSendSrv.getInstance().sendMail(
						MsgProceso.MSG_PROCESO_DESACTIVADO_ASUNTO.toString()
								.replace(":?:",
										PROCESO_ACTUAL.getCodigoProceso()),
						MsgProceso.MSG_PROCESO_DESACTIVADO_BODY.toString()
								.replace(":?:",
										PROCESO_ACTUAL.getCodigoProceso()));
			}

		} catch (SQLException e) {
			throw new AppException(
					MsgGenerationItzAvance.ERROR_CREATE_ITZ_FILE.toString(), e);
		} finally {
			if (generarAvanceDao != null) {
				try {
					generarAvanceDao.close();
				} catch (SQLException e) {
					throw new AppException(e.getMessage(), e);
				}
			}
		}
		return flag;

	}

	/**
	 * 
	 * @param parametros
	 * @return
	 */
	private String generateContableFilename(
			final ParamExportItzAvanceDTO parametros) {
		String date = DateUtils.getDateTodayInYYYYMMDDAnterior();
		StringBuilder str = new StringBuilder();
		str.append(date).append(ConstantesUtil.UNDERSCORE)
				.append(parametros.getNomArchivo());
		return str.toString();
	}

	/**
	 * Metodo busca los parametros del proceso que genera interfaz de avances.
	 * 
	 * @return Dto con los parametros utilizados.
	 * @throws SQLException
	 *             Exception SQL.
	 */
	private ParamExportItzAvanceDTO getParametrosUploadItzAvances()
			throws SQLException {
		CommonsDao daoCommon = null;
		List<ParametroDTO> paramDTOList = null;
		ParamExportItzAvanceDTO paramItzAvanceDTO = null;
		try {
			daoCommon = CommonsDaoFactory.getInstance().getNewEntidadDao();
			paramDTOList = daoCommon
					.getParametroCodGrupoDato(ConstantesPRTS.CRON_TBK
							.toString());
			if (paramDTOList != null) {
				paramItzAvanceDTO = new ParamExportItzAvanceDTO();
				paramItzAvanceDTO.setPathSalida(CommonsUtils.getCodiDato(
						paramDTOList,
						ConstantesPRTS.PATH_TRX_FILE_AVA.toString()));
				paramItzAvanceDTO.setNomArchivo(CommonsUtils.getCodiDato(
						paramDTOList,
						ConstantesPRTS.FORMAT_FILE_FILE_AVA.toString()));
			} else {
				paramItzAvanceDTO = null;
			}
			return paramItzAvanceDTO;
		} finally {

			if (daoCommon != null) {
				daoCommon.close();
			}

		}
	}

}
