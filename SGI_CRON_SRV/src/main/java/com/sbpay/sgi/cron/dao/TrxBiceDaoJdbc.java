package com.sbpay.sgi.cron.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import oracle.jdbc.OracleCallableStatement;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.dao.BaseDao;
import com.sbpay.sgi.cron.dto.TransaccionBiceDTO;
import com.sbpay.sgi.cron.enums.ConstantesBD;

public class TrxBiceDaoJdbc extends BaseDao<TrxBiceDao> implements TrxBiceDao {
    
    /** VARIABLE PARA EL LOGER */
    private static final Logger LOGGER = Logger.getLogger( TrxBiceDaoJdbc.class );
    
    /**
     * Constructor con conexion.
     * 
     * @param connection
     *            Conexion JDBC.
     */
    public TrxBiceDaoJdbc( Connection connection ) {
        super( connection );
    }

	@Override
	public boolean saveTransaction(final TransaccionBiceDTO transaccion)
			throws SQLException {
	    PreparedStatement stmt = null;
	    
	    try {
	      StringBuilder str =
	              new StringBuilder()
	                  .append("INSERT INTO ")
	                  .append(ConstantesBD.ESQUEMA.toString())
	                  .append(".TMP_TRANSAC_AVA_BICE ")
	                  .append("(SID,TIPO_REGISTRO,NUM_OPER_PROG,COD_SERVICIO,RUT_ORDENANTE,NUM_CTA_ORDENANTE,")
	                  .append("ID_OPER_CLIENTE,COD_ESTADO,GLS_ESTADO,FECHA_INSTRUCCION,FECHA_CONTABLE,COD_BANCO_BENEFICIARIO,")
	                  .append("TIPO_CTA_BENEFICIARIO,CTA_BENEFICIARIO,RUT_BENEFICIARIO,NOM_BENEFICIARIO,MONTO)")
	                  .append(" values (SEQ_TMP_TRANSAC_AVA_BICE.nextval , ?, ?, ?, ?, ?, ?, ?, ?, To_date(?, 'DD-MM-RR HH24:MI:SS'), To_date(?, 'DD-MM-RR'), ?, ?, ?, ?, ?, ?)");

	          stmt = this.getConnection().prepareStatement(str.toString());
	          	          
	          setStringNull(transaccion.getTipoRegistro(), 1, stmt);
	          setStringNull(transaccion.getNumOperProg(), 2, stmt);
	          setStringNull(transaccion.getCodServicio(), 3, stmt);
	          setStringNull(transaccion.getRutOrdenante(), 4, stmt);
	          setStringNull(transaccion.getNumCtaOrdenante(), 5, stmt);
	          setStringNull(transaccion.getIdOperCliente(), 6, stmt);
	          setStringNull(transaccion.getCodEstado(), 7, stmt);
	          setStringNull(transaccion.getGlosaEstado(), 8, stmt);
	          setStringNull(transaccion.getFechaInstruccion(), 9, stmt);
	          setStringNull(transaccion.getFechaContable(), 10, stmt);
	          setStringNull(transaccion.getCodBancoBeneficiario(), 11, stmt);
	          setStringNull(transaccion.getTipoCtaBeneficiario(), 12, stmt);
	          setStringNull(transaccion.getCtaBeneficiario(), 13, stmt);
	          setStringNull(transaccion.getRutBeneficiario(), 14, stmt);
	          setStringNull(transaccion.getNomBeneficiario(), 15, stmt);
	          setStringNull(transaccion.getMonto(), 16, stmt);
	          
	          return (stmt.executeUpdate() > 0);
	    } finally {
	      if (stmt != null) {
	        stmt.close();
	      }
	    }
	}

	@Override
	public boolean callSPCargaLogTrxBice() throws SQLException {
	    Connection dbConnection = null;
	    OracleCallableStatement callableStatement = null;

	    try {
	      String callSPCargaIncomingSql = "{call SP_SIG_CARGA_LOG_TRX_AVA_BICE(?,?)}";

	      dbConnection = getConnection();
	      callableStatement =
	          (OracleCallableStatement) dbConnection.prepareCall(callSPCargaIncomingSql);

	      callableStatement.registerOutParameter(1, java.sql.Types.VARCHAR);
	      callableStatement.registerOutParameter(2, java.sql.Types.NUMERIC);

	      callableStatement.executeUpdate();

	      String warning = callableStatement.getString(1);
	      Integer codigoError = callableStatement.getInt(2);
	      LOGGER.info("****** SE EJECUTA STORE PROCEDURE SP_SIG_CARGA_LOG_TRX_AVA_BICE *****");
	      LOGGER.info("WARNING : " + warning);
	      LOGGER.info("codigoError : " + codigoError);
	      LOGGER.info("*********** STORE PROCEDURE SP_SIG_CARGA_LOG_TRX_AVA_BICE *********** ");

	    } finally {
	      if (callableStatement != null) {
	        callableStatement.close();
	      }
	    }
	    return Boolean.TRUE;
		
	}
    
 

}
