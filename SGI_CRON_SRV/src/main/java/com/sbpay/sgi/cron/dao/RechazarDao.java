package com.sbpay.sgi.cron.dao;

import java.sql.SQLException;

import com.sbpay.sgi.cron.dao.Dao;
import com.sbpay.sgi.cron.dto.TmpRechazosDTO;

public interface RechazarDao extends Dao {
	
    static final String MERCHANT_NAME = "ABCVISA";
    static final String TRANSACTION_CODE = "";
    static final String CALL_SP_SIG_CMPRECHAZOINCOMING = "{call SP_SIG_CMPRECHAZOINCOMING(?,?)}";


	boolean saveRchTransaction(final TmpRechazosDTO instance) throws SQLException;

	void callSPCargaRechazos() throws SQLException;



}
