package com.sbpay.sgi.cron.step;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.enums.MsgExitoMail;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.srv.export.GenerarIncomingOnUsSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;

public class StepGenerarIncomingOnUs {
    
    /** The Constant LOGGER. */
    private static final Logger LOG = Logger
            .getLogger( StepGenerarIncomingOnUs.class );
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 28/01/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @throws Exception
     * @since 1.X
     */
    public void execute() throws Exception {
        
        try {
            LOG.info( "Inicio Proceso de Generar/Exportar Incoming On Us : StepGenerarIncomingOnUs " );
            if ( GenerarIncomingOnUsSrv.getInstance().generarIncomingOnUsTbk() ) {
                LOG.info( "Envio Correo de Generar/Exportar Incoming On Us  Satisfactorio: Proceso Generar Incoming On Us" );
                MailSendSrv.getInstance().sendMailOk(
                        MsgExitoMail.EXITO_PROCESS_INC_ONUS_TITLE.toString(),
                        MsgExitoMail.EXITO_PROCESS_INC_ONUS.toString() );
            }
            else {
                LOG.info( "Generar/Exportar Incoming On Us no realizados, Finaliza! (total o parcialmente consultar log)" );
            }            
        }
        catch ( AppException e ) {
            LOG.error( e.getMessage(), e );
            MailSendSrv.getInstance().sendMail(
                    MsgProceso.MSG_GENERIC_ERROR_MSJ.toString(),
                    ( e.getStackTraceDescripcion() != null ) ? e
                            .getStackTraceDescripcion() : e.getMessage() );
        }
        
    }
    
}
