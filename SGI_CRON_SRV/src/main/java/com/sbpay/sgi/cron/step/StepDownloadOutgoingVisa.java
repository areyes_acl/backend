package com.sbpay.sgi.cron.step;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.enums.MsgExitoMail;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.srv.download.DownLoadOutgoingVisaSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;

public class StepDownloadOutgoingVisa {
    
    private static final Logger LOG = Logger
            .getLogger( StepDownloadOutgoingVisa.class );
    
    public void execute() throws Exception {
        
        try {
            LOG.info( "\n ======> Inicio Proceso:  Download outging Visa Internacional <=======" );
            if ( DownLoadOutgoingVisaSrv.getInstance()
                    .descargarArchivoVisaInt() ) {
                LOG.info( "Envio Correo de Descarga Satisfactoria: Proceso DownLoad Outgoing Visa Internacional" );
                MailSendSrv
                        .getInstance()
                        .sendMailOk(
                                MsgExitoMail.EXITO_DOWNLOAD_OUTGOING_VISA_TITLE
                                        .toString(),
                                MsgExitoMail.EXITO_DOWNLOAD_OUTGOING_VISA_CAUSE
                                        .toString() );
                
                LOG.info( "======> Fin Proceso: Download outging Visa Internacional , correctamente <=======" );
            }
            else {
                LOG.warn( "======> Fin Proceso: Download outging Visa Internacional , con observaciones <=======" );
                
            }
            
        }
        catch ( AppException e ) {
            LOG.info( "======> Error al ejecutar proceso <=======" );
            LOG.error( e.getMessage(), e );
            MailSendSrv.getInstance().sendMail(
                    MsgProceso.MSG_GENERIC_ERROR_MSJ.toString(),
                    ( e.getStackTraceDescripcion() != null ) ? e
                            .getStackTraceDescripcion() : e.getMessage() );
        }
        
    }
}
