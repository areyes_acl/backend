package com.sbpay.sgi.cron.dao;

import java.sql.SQLException;
import java.util.List;








import com.sbpay.sgi.cron.dao.Dao;
import com.sbpay.sgi.cron.dto.DetalleContableBean;
import com.sbpay.sgi.cron.dto.SolicitudReporteContableDTO;

public interface GenerarReporteContableDao extends Dao{
	
	List<SolicitudReporteContableDTO> obtenerInformacionCnbltSolicitud() throws SQLException;
	
	void updateDataSolicitudContableBloqueo() throws SQLException;
	
	List<DetalleContableBean> obtenerDetalleContable(String fechaInicio, String fechaTermino, String tipoMov, int operador, int producto) throws SQLException;
	
	void updateDataSolicitudContableStateOK(Integer sid, Integer cantidad) throws SQLException;
}
