package com.sbpay.sgi.cron.validation.impl;

import java.sql.SQLException;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dto.TblCronLogDTO;
import com.sbpay.sgi.cron.enums.LogBD;
import com.sbpay.sgi.cron.enums.MsgDownloadBol;
import com.sbpay.sgi.cron.utils.generals.DateUtils;
import com.sbpay.sgi.cron.validation.BolFileValidator;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class BolFileValidatorImpl implements BolFileValidator {

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * Metodo que realiza una validaciòn de la descarga de comisiones.
     * 
     * Si la fecha del ultimo archivo en SFTP > fecha ultimo archivo en bd en
     * BD, se descarga = true
     * 
     * Si la fecha del ultimo archivo en SFTP <= fecha ultimo archivo en bd
     * existe inconsistencia y no se descarga = false
     * 
     * Si no existe registro en base de datos se retorna verdadero
     * 
     * si no existe ninguna fecha ingresada = true
     * 
     * @return
     * @throws AppException
     * @since 1.X
     */
    public void validarArchivoBolADescargar(String nombreArchivo,
	    String starWith) throws AppException {
	CommonsDao commonsDAO = null;

	try {
	    // SI NO EXISTE NINGUN ARCHIVO A DESCARGAR
	    if (nombreArchivo == null) {
		throw new AppException(MsgDownloadBol.NO_VALID_FILES.toString());
	    }

	    // SE EXTRAE LA FECHA DEL ARCHIVO
	    String fecha = DateUtils.getDateFileTbk(DateUtils
		    .getDateStringFromFilename(nombreArchivo, starWith));

	    // ULTIMO EN BD
	    commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();

	    TblCronLogDTO cronLog = new TblCronLogDTO();
	    cronLog = commonsDAO.getRegisterWithMaxDate(LogBD.TABLE_LOG_BOL);

	    if (cronLog != null) {

		// SE COMPARAN FECHA BD CON FECHA SFTP
		Integer diferencia = DateUtils.compareDates(fecha,
			cronLog.getFecha());

		// DIFERENCIA NO SE PUDO CALCULAR DIFERENCIA
		if (diferencia == null) {
		    throw new AppException(
			    MsgDownloadBol.CANT_CALCULATE_DIFERENCE_BETWEENN_DATES
				    .toString());
		}

		// SI SON IGUALES LAS FECHAS
		if (diferencia == 0) {
		    throw new AppException(
			    MsgDownloadBol.NO_EXIST_NEW_FILES.toString());

		    // SI ULTIMO EN SFTP < ULTIMO EN BD
		} else if (diferencia < 0) {
		    throw new AppException(
			    MsgDownloadBol.INVALID_DATE_BETWEEN_FILE_AND_REGISTER
				    .toString());

		}
	    }

	} catch (SQLException e) {
	    throw new AppException(
		    MsgDownloadBol.ERROR_RETRIEVE_LOG_REGISTER.toString(), e);
	} finally {
	    if (commonsDAO != null) {
		try {
		    commonsDAO.close();
		} catch (SQLException e) {
		    throw new AppException(
			    MsgDownloadBol.ERROR_CLOSE_CONECTION.toString(), e);
		}
	    }
	}

    }

  

}
