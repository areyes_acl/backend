package com.sbpay.sgi.cron.step;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.enums.MsgErrorMail;
import com.sbpay.sgi.cron.enums.MsgExitoMail;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.srv.upload.UploadICOutgoingTbkSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 7/1/2016, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class StepUploadICOutgoingTbk {
    /** The Constant LOGGER. */
    private static final Logger LOG = Logger
            .getLogger( StepUploadICOutgoingTbk.class );
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 8/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws Exception
     * @since 1.X
     */
    public void execute() throws Exception {
        try {
            LOG.info( "=====> Inicio Proceso: Upload de archivo Outgoing Transbank a SFTP IC <====" );
            if ( UploadICOutgoingTbkSrv.getInstance().uploadOutgoingIC() ) {
                LOG.info( "Envio de email satisfactorio: Proceso Upload Outgoing tbk a sftp IC" );
                MailSendSrv.getInstance().sendMailOk(
                        MsgExitoMail.EXITO_PROCESS_UPLOAD_IC_TITLE.toString(),
                        MsgExitoMail.EXITO_PROCESS_UPLOAD_IC_BODY.toString() );
                
            }
            else {
                LOG.info( MsgErrorMail.ERROR_PROCESS_UPLOAD_IC_BODY.toString() );
                LOG.info( "=====> Fin del proceso de DE Upload Outgoing a a ftp de IC  <====" );
            }
        }
        catch ( AppException e ) {
        	 LOG.error( e.getMessage(), e );
        	 MailSendSrv.getInstance().sendMail(
                     MsgProceso.MSG_GENERIC_ERROR_MSJ.toString(),
                     ( e.getStackTraceDescripcion() != null ) ? e
                             .getStackTraceDescripcion() : e.getMessage() );
        }
    }
}
