package com.sbpay.sgi.cron.dao;

import java.sql.SQLException;

import com.sbpay.sgi.cron.dao.BaseDaoFactory;
import com.sbpay.sgi.cron.ds.ConnectionProvider;

public final class CommonsDaoFactory extends BaseDaoFactory<CommonsDao> {

	/** The single instance. */
	private static CommonsDaoFactory singleINSTANCE = null;

	/**
	 * Creates the instance.
	 * 
	 * @throws SQLException
	 */
	private static void createInstance() throws SQLException {
		synchronized (CommonsDaoFactory.class) {
			if (singleINSTANCE == null) {
				singleINSTANCE = new CommonsDaoFactory();
			}
		}
	}

	/**
	 * Patron singleton.
	 * 
	 * @return ActivarPrepagoDaoFactory retorna instancia del servicio.
	 * @throws SQLException
	 */
	public static CommonsDaoFactory getInstance() throws SQLException {
		if (singleINSTANCE == null) {
			createInstance();
		}
		return singleINSTANCE;
	}

	/**
	 * Metodo de restricion de sobreescritura de la clase.
	 */
	@Override
	protected Object clone() throws CloneNotSupportedException {
		super.clone();
		throw new CloneNotSupportedException();
	}

	/**
	 * Constructor default.
	 * 
	 * @throws SQLException Error de SQL.
	 */
	private CommonsDaoFactory() throws SQLException {
		super();
	}

	/**
	 * Metodo obtiene la instancia mas la conexion.
	 */
	@Override
	public CommonsDao getNewEntidadDao() throws SQLException {
		return new CommonsDaoJdbc(ConnectionProvider.getInstance().getConnection());
	}
}