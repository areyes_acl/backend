package com.sbpay.sgi.cron.job;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;

import com.sbpay.sgi.cron.step.StepDownloadBOL;
import com.sbpay.sgi.cron.step.StepUploadBOL;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 7/1/2016, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * JOB ENCARGADO DE GESTIONAR EL BOL
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class FiveJob implements Job {

    /** LOGGER CONSTANT **/
    private static final Logger LOG = Logger.getLogger(FiveJob.class);

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 7/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @param context
     * @since 1.X
     */

    @Override
    public void execute(JobExecutionContext context) {

	LOG.info("========== EJECUCION DEL QUINTO JOB =============");
	descargarArchivoBOL();
	subirArchivoBOL();
	LOG.info("========== FIN EJECUCION DEL QUINTO JOB =============");

    }
    
   

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 7/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws Exception
     * @since 1.X
     */
    public void descargarArchivoBOL() {
	try {
	    StepDownloadBOL step = new StepDownloadBOL();
	    step.execute();

	} catch (Exception e) {
	    LOG.error(e.getMessage(), e);
	}
    }

 
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 7/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws Exception
     * @since 1.X
     */
  	public void subirArchivoBOL() {
	try {
	    // LLAMA A LA TERCERA TAREA
	    StepUploadBOL step = new StepUploadBOL();
	    step.execute();
	} catch (Exception e) {
	    LOG.error(e.getMessage(), e);
	}

    }

}
