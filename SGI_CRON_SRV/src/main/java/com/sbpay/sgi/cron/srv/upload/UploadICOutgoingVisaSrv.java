package com.sbpay.sgi.cron.srv.upload;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dto.DataFTP;
import com.sbpay.sgi.cron.dto.ParamUploadIncomingVisaDTO;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.dto.TblCronLogDTO;
import com.sbpay.sgi.cron.enums.ConstantesPRTS;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.FilterTypeSearch;
import com.sbpay.sgi.cron.enums.LogBD;
import com.sbpay.sgi.cron.enums.MsgErrorMail;
import com.sbpay.sgi.cron.enums.MsgErrorSQL;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.srv.utils.ClientFTPSrv;
import com.sbpay.sgi.cron.srv.utils.CommonSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;
import com.sbpay.sgi.cron.utils.generals.DateUtils;
import com.sbpay.sgi.cron.validation.ProcessValidator;
import com.sbpay.sgi.cron.validation.impl.ProcessValidatorImpl;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 24/11/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * Clase que realiza la logica de la subida del archivo OUTGOING VISA a IC
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public final class UploadICOutgoingVisaSrv extends CommonSrv {
  /** The Constant LOGGER. */
  private static final Logger LOG = Logger.getLogger(UploadICOutgoingVisaSrv.class);
  /** The single instance. */
  private static UploadICOutgoingVisaSrv singleINSTANCE = null;
  
  /** PROCESO */
  private final ProcessCodeType PROCESO_ACTUAL = ProcessCodeType.SUBIDA_OUTGOING_VISA_A_IC;

  /**
   * Creates the instance.
   */
  private static void createInstance() {
    synchronized (UploadICOutgoingVisaSrv.class) {
      if (singleINSTANCE == null) {
        singleINSTANCE = new UploadICOutgoingVisaSrv();

      }
    }
  }

  /**
   * Patron singleton.
   * 
   * @return UploadIncomingSrv retorna instancia del servicio.
   */
  public static UploadICOutgoingVisaSrv getInstance() {
    if (singleINSTANCE == null) {
      createInstance();
    }
    return singleINSTANCE;
  }

  /**
   * Metodo de restricion de sobreescritura de la clase.
   */
  @Override
  protected Object clone() throws CloneNotSupportedException {
    super.clone();
    throw new CloneNotSupportedException();
  }

  /**
   * Constructor Privado.
   */
  private UploadICOutgoingVisaSrv() {
    super();
  }


	/**
	 * <
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * Metodo orquesta el uso de la subida del archivo Outgoing VISA a IC
	 * 
	 * @return
	 * @throws AppException
	 * @since 1.X
	 */
	public boolean upload() throws AppException {
	    ParamUploadIncomingVisaDTO parametros = null;
        boolean flag = false;
        List<String> fileDirectory = null;
        List<String> fileUpload = null;
        String errorFile = null;
	    
	    ProcessValidator validadorProceso = new ProcessValidatorImpl();

	    if (!validadorProceso.isActiveProcess(PROCESO_ACTUAL)) {
	        String asunto = MsgProceso.MSG_PROCESO_DESACTIVADO_ASUNTO
                    .toString().replace( ":?:",
                            PROCESO_ACTUAL.getCodigoProceso() );
            String mensaje = MsgProceso.MSG_PROCESO_DESACTIVADO_BODY.toString()
                    .replace( ":?:", PROCESO_ACTUAL.getCodigoProceso() );
            MailSendSrv.getInstance().sendMail( asunto, mensaje );
            return flag;
	    }
	    
		// BUSCA LOS PARAMETROS BD UTILIZADOS EN EL CRON
		parametros = getParametrosUploadCron();

		if (validadorProceso.hasTheValidParametersForTheProcess( PROCESO_ACTUAL, parametros )) {
			LOG.info("===  SE HAN ENCONTRADO TODOS LOS PARAMETROS, COMIENZA EL PROCESO DE SUBIDA VISA A IC====");
			String pathSearch = parametros.getPathVisaInc();
			String startWith = parametros.getFilenameOutVisa();
			String extFile = parametros.getExtFileOutVisa();
			String startWithToIc = parametros.getFilenameOutVisaToIc();
			String extFileToIc = parametros.getExtFileOutVisaToIc();

			// BUSCA LISTA DE ARCHIVOS CANTIDATOS A PROCESAR QUE CONTENGAN
			// NOMBRE VALIDO
			fileDirectory = getFileStartsWith(pathSearch, startWith, extFile);
			LOG.info("Lista de archivos que contiene formato de nombre valido:  " + fileDirectory);

			if (fileDirectory != null && fileDirectory.size() > 0) {
				// BUSCA ARCHIVOS QUE SE SUBIRAN A IC, FILTRANDO POR FECHA EN
				// TABLA TBL_LOG_VISA
				fileUpload = getFileUpload(fileDirectory, startWith);
				 LOG.info("Lista de archivos que no han sido subidos con anterioridad:" + fileUpload);
				 
				if (fileUpload != null && fileUpload.size() > 0) {
					
						errorFile = ConstantesUtil.EMPTY.toString();
						int count = 0;
						errorFile = ConstantesUtil.EMPTY.toString();
						
						for (String filenameDest : fileUpload) {
							try {
								//copia el archivo, sube el local y luego borra
				            	String medio = filenameDest.replace(startWith, "").substring(4,8);
				            	String toFile = startWithToIc+medio+extFileToIc;
				            	CommonsUtils.copiarArchivo(pathSearch+filenameDest, pathSearch+toFile);
				            	filenameDest=toFile;
								if (ClientFTPSrv.getInstance().uploadFileSFTP(parametros.getDataFTP(),parametros.getPathVisaInc(),parametros.getFtpPathIcEntrada(),filenameDest)) {
									LOG.info("ARCHIVO "+ filenameDest+ " SUBIDO CORRECTAMENTE A FTP IC = "+ parametros.getDataFTP().getIpHost());
									count++;
									CommonsUtils.borrarArchivo(filenameDest);
								} else {
									errorFile = errorFile.concat(filenameDest).concat(ConstantesUtil.SKIP_LINE.toString());
								}
							} catch (Exception e) {
								LOG.info("Ha ocurrido un problema al subir archivo  : "+ filenameDest + ", hacia FTP IC" + e);
								errorFile = errorFile.concat(filenameDest).concat(ConstantesUtil.SKIP_LINE.toString());
							}
						}

						if (count == fileUpload.size()) {
							flag = true;
						} else {
							LOG.info("Algunos archivos outgoing Visa no pudieron ser cargados en IC : \n"
											+ errorFile);
							MailSendSrv
									.getInstance()
									.sendMail(
											MsgErrorMail.ALERTA_FILE_UPLOAD_OUT_VISA_IC_TITLE
													.toString(),
											MsgErrorMail.ALERTA_FILE_UPLOAD_OUT_VISA_IC_BODY
													.toString().concat(errorFile));
						}

				} else {
					LOG.info("No existen archivos outgoing Visa que subir a IC");
					MailSendSrv.getInstance().sendMail(
							MsgErrorMail.ALERTA_FILE_UPLOAD.toString(),
							MsgErrorMail.ALERTA_FILE_UPLOAD_OUT_VISA_IC_WARN
									.toString());
				}

			} else {
				LOG.info("No existen archivos outgoing Visa que subir a IC");
				MailSendSrv.getInstance().sendMail(
						MsgErrorMail.ALERTA_FILE_UPLOAD.toString(),
						MsgErrorMail.ALERTA_FILE_UPLOAD_OUT_VISA_IC_WARN
								.toString());
			}

		} else {
		    LOG.info(MsgProceso.MSG_ERROR_FALTAN_PARAMETROS.toString());
			MailSendSrv.getInstance().sendMail(
					MsgErrorMail.ALERTA_PRTS_UPLOAD.toString(),
					MsgErrorMail.ALERTA_PRTS_TXT_UPLOAD.toString());
		}

		return flag;
	}



  /**
   * 
   * @param dataFTP
   * @param fileUpload
   * @return
 * @throws AppException 
   */
  private List<String> filtraSiExisteArchivoEnIC( ParamUploadIncomingVisaDTO parametros,
		List<String> fileUpload) throws AppException {
	  List<String> filenamesList = null;
	  
	  // BUSCA LA LISTA DE ARCHIVOS EXISTENTES EN EL
      // SFTP DE TRANSBANK
		List<String> listaFtpTbk = ClientFTPSrv.getInstance().listFileSFTP( parametros.getDataFTP(), parametros.getFtpPathIcEntrada());
		
		if( listaFtpTbk != null && listaFtpTbk.size() > 0  && fileUpload != null && fileUpload.size() > 0 ){
			filenamesList = new ArrayList<String>();
			for (String fileFTP : listaFtpTbk) {
				if(!fileUpload.contains(fileFTP)){
					filenamesList.add(fileFTP);
				}
			}
			
		}
	
	return filenamesList;
}

/**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
   * </ul>
   * <p>
   * 
   * Metodo busca los archivos que van a ser subidos al servidor FTP tbk
   * 
   * @param fileDirectory
   * @return
   * @throws AppException
   * @since 1.X
   */
  private List<String> getFileUpload(final List<String> fileDirectory, String starWith)
      throws AppException {

    String dateFile = null;
    List<String> fileCarga = null;
    CommonsDao daoCommons = null;
    String fileAux = null;
    TblCronLogDTO dto = null;
    TblCronLogDTO dtoAux = new TblCronLogDTO();
    try {
      fileCarga = new ArrayList<String>();
      daoCommons = CommonsDaoFactory.getInstance().getNewEntidadDao();

      // Lista de archivos que se pueden procesar.
      for (String fileName : fileDirectory) {
        // Formato PREFIJOmmdds.dat
        fileAux = fileName.replace(starWith, "");

        // Rescata fecha archivo incoming
        dateFile = DateUtils.getDateFileVisaInt(fileAux);
        if (dateFile != null) {
          dtoAux.setFecha(dateFile);
          dto =
              daoCommons.getTblCronLogByType(dtoAux, LogBD.TABLE_LOG_VISA_INT.getTable(),
                  FilterTypeSearch.FILTER_BY_DATE);
          if (dto != null) {
            fileCarga.add(fileName);
          }
        } else {
          LOG.info("Archivo no cumple con el formato para obtener la fecha al leer outgoing en visa internacional "
              + fileName);
        }
      }
      return fileCarga;
    } catch (SQLException e) {
      throw new AppException(MsgErrorSQL.ERROR_QUERY_PROCESS_UPLOAD.toString(), e);
    } finally {
      try {
        if (daoCommons != null) {
          daoCommons.close();
        }
      } catch (SQLException e) {
        throw new AppException(MsgErrorSQL.ERROR_QUERY_PROCESS_CLOSE_UPLOAD.toString(), e);
      }
    }
  }


  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
   * </ul>
   * <p>
   * Metodo busca los parametros del proceso Upload Incoming.
   * 
   * @return Dto con los parametros utilizados.
   * @throws AppException
   * @since 1.X
   */
  private ParamUploadIncomingVisaDTO getParametrosUploadCron() throws AppException {
    CommonsDao daoCommon = null;
    List<ParametroDTO> paramDTOList = null;
    ParamUploadIncomingVisaDTO params = null;
    try {
      daoCommon = CommonsDaoFactory.getInstance().getNewEntidadDao();
      paramDTOList = daoCommon.getParametroCodGrupoDato(ConstantesPRTS.CRON_VISA_INT.toString());

      if (paramDTOList != null) {

        // PARAMETROS DE CONEXION A FTP IC
        DataFTP dataFtp = new DataFTP();
        dataFtp.setIpHost(CommonsUtils.getCodiDato(paramDTOList,ConstantesPRTS.SFTP_IP_UPLOAD_VISA_INT_IC.toString()));
        String port = CommonsUtils.getCodiDato(paramDTOList,ConstantesPRTS.SFTP_PORT_UPLOAD_VISA_INT_IC.toString());
        dataFtp.setPort( Integer.parseInt((port != null && port.matches(ConstantesUtil.PATTERN_IS_NUMBER.toString()) ? port : ConstantesUtil.DEFAULT_PORT.toString())));
        dataFtp.setUser(CommonsUtils.getCodiDato(paramDTOList, ConstantesPRTS.SFTP_USER_UPLOAD_VISA_INT_IC.toString()));
        dataFtp.setPassword(CommonsUtils.getCodiDato(paramDTOList, ConstantesPRTS.SFTP_PASS_UPLOAD_VISA_INT_IC.toString()));

        // PARAMETROS DE PROCESO UPLOAD INCOMING VISA A IC
        params = new ParamUploadIncomingVisaDTO();
        
        // RUTA DE FOLDER FTP DONDE SE DEJARAN LOS ARCHIVOS OUTGOING DE VISA
        params.setFtpPathIcEntrada(CommonsUtils.getCodiDato(paramDTOList, ConstantesPRTS.PATH_IN_UPLD_VISA_INT_IC.toString()));

        // RUTA DE FOLDER EN SERVIDOR sbpay DONDE SE DEJARAN LOS OUTGOING VISA A SUBIR A IC
        params.setPathVisaInc(CommonsUtils.getCodiDato(paramDTOList, ConstantesPRTS.PATH_ACL_INC_VISA_INT.toString()));

        // FORMATO DE NOMBRE DE ARCHIVO OUTGOING A SUBIR A IC
        params.setFilenameOutVisa(CommonsUtils.getCodiDato(paramDTOList, ConstantesPRTS.FORMAT_FILENAME_OUT_VISA_INT.toString()));
        params.setExtFileOutVisa(CommonsUtils.getCodiDato(paramDTOList, ConstantesPRTS.FORMAT_EXTNAME_OUT_VISA_INT.toString()));
        params.setExtFileCtrOutVisa(CommonsUtils.getCodiDato(paramDTOList, ConstantesPRTS.FORMAT_EXTNAME_OUT_CTR_VISA_INT.toString()));
        params.setFilenameOutVisaToIc(CommonsUtils.getCodiDato(paramDTOList, ConstantesPRTS.FORMAT_FILENAME_OUT_VISA_INT_TO_IC.toString()));
        params.setExtFileOutVisaToIc(CommonsUtils.getCodiDato(paramDTOList, ConstantesPRTS.FORMAT_EXTNAME_OUT_VISA_INT_TO_IC.toString()));
        params.setExtFileCtrOutVisaToIc(CommonsUtils.getCodiDato(paramDTOList, ConstantesPRTS.FORMAT_EXTNAME_OUT_CTR_VISA_INT_TO_IC.toString()));
        
        params.setDataFTP(dataFtp);
        
        LOG.debug("== PARAMETROS  : " + params);

      }
    } catch (SQLException e) {
      throw new AppException(MsgErrorSQL.ERROR_QUERY_PRTS_UPLOAD.toString(), e);
    } finally {
      try {
        if (daoCommon != null) {
          daoCommon.close();
        }
      } catch (SQLException e) {
        throw new AppException(MsgErrorSQL.ERROR_QUERY_PRTS_UPLOAD_CLOSE.toString(), e);
      }
    }

    return params;
  }
 
}
