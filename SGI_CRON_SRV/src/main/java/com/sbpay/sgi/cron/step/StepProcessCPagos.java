package com.sbpay.sgi.cron.step;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.srv.process.ProcesarCompscnsPagoSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 14/01/2016, (ACL) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA.</B>
 */
public class StepProcessCPagos {
    
    /**
     * VARIABLES
     */
    private static final Logger LOG = Logger
            .getLogger( StepProcessCPagos.class );
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2015, (sbpay Chile) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @throws Exception
     * @since 1.X
     */
    public void execute() throws Exception {
        try {
            LOG.info( "\n=======> Inicio procesamiento Archivo Compensacion de Pagos : StepProcessCPagos <=======" );
            ProcesarCompscnsPagoSrv.getInstance().procesarCPagos();
            LOG.info( "=======> Fin procesamiento Archivo Compensacion de Pagos : StepProcessCPagos" );
            
        }
        catch ( AppException e ) {
            LOG.error( e.getMessage(), e );
            MailSendSrv.getInstance().sendMail(
                    MsgProceso.MSG_GENERIC_ERROR_MSJ.toString(),
                    ( e.getStackTraceDescripcion() != null ) ? e
                            .getStackTraceDescripcion() : e.getMessage() );
        }
        
    }
    
}
