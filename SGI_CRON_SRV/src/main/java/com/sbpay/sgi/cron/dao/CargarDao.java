package com.sbpay.sgi.cron.dao;

import java.sql.SQLException;

import com.sbpay.sgi.cron.dao.Dao;
import com.sbpay.sgi.cron.dto.CronLogDTO;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 24/11/2015, (ACL) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SA.</B>
 */
public interface CargarDao extends Dao {
    
    public CronLogDTO getCronLogByDate( final String date ) throws SQLException;
    
    public boolean saveLogInc( CronLogDTO cronLogDTO )
            throws SQLException;
    
    public boolean updateFlag( String flag, String incomingName )
            throws SQLException;
    
    public CronLogDTO getByIncomingName( String flag ) throws SQLException;


    
}
