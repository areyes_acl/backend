package com.sbpay.sgi.cron.dao;

import java.sql.SQLException;

import com.sbpay.sgi.cron.dao.BaseDaoFactory;
import com.sbpay.sgi.cron.ds.ConnectionProvider;

public class GenerarContableDaoFactory extends BaseDaoFactory<GenerarContableDao> {
	
	/** The single instance. */
	private static GenerarContableDaoFactory singleINSTANCE = null;

	/**
	 * Creates the instance.
	 * 
	 * @throws SQLException
	 */
	private static void createInstance() throws SQLException {
		synchronized (GenerarContableDaoFactory.class) {
			if (singleINSTANCE == null) {
				singleINSTANCE = new GenerarContableDaoFactory();
			}
		}
	}

	/**
	 * Patron singleton.
	 * 
	 * @return GenerarContableDaoFactory retorna instancia del servicio.
	 * @throws SQLException
	 */
	public static GenerarContableDaoFactory getInstance() throws SQLException {
		if (singleINSTANCE == null) {
			createInstance();
		}
		return singleINSTANCE;
	}

	/**
	 * Metodo de restricion de sobreescritura de la clase.
	 */
	@Override
	protected Object clone() throws CloneNotSupportedException {
		super.clone();
		throw new CloneNotSupportedException();
	}

	/**
	 * Constructor default.
	 * 
	 * @throws SQLException Error de SQL.
	 */
	private GenerarContableDaoFactory() throws SQLException {
		super();
	}

	@Override
	public GenerarContableDaoJdbc getNewEntidadDao() throws SQLException {
		return new GenerarContableDaoJdbc(ConnectionProvider.getInstance()
				.getConnection());
	}

}
