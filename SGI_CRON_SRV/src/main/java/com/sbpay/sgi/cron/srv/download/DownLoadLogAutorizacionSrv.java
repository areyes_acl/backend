package com.sbpay.sgi.cron.srv.download;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dto.ConfigFile;
import com.sbpay.sgi.cron.dto.DataFTP;
import com.sbpay.sgi.cron.dto.ParamDownloadLogAutorizacionDTO;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.dto.TblCronLogDTO;
import com.sbpay.sgi.cron.enums.ConstantesPRTS;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.LogBD;
import com.sbpay.sgi.cron.enums.MsgErrorMail;
import com.sbpay.sgi.cron.enums.MsgErrorSQL;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.enums.StatusProcessType;
import com.sbpay.sgi.cron.srv.utils.ClientFTPSrv;
import com.sbpay.sgi.cron.srv.utils.CommonSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;
import com.sbpay.sgi.cron.utils.generals.DateUtils;
import com.sbpay.sgi.cron.validation.ProcessValidator;
import com.sbpay.sgi.cron.validation.impl.ProcessValidatorImpl;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 19/01/2016, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * Este proceso se encarga de descargar el archivo de Log de Autorizaciones desde IC, 
 * partiendo con los que comiencen un prefijo, posteriormente valida en la BD en la 
 * tabla TBL_LOG_TRANSAC_AUTOR  que no este descargado. 
 * (Solo descarga el archivo no existe archivo de control)	
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public final class DownLoadLogAutorizacionSrv extends CommonSrv {
	/** The Constant LOGGER. */
	private static final Logger LOG = Logger.getLogger(DownLoadLogAutorizacionSrv.class
			.getName());
	/** The single instance. */
	private static DownLoadLogAutorizacionSrv singleINSTANCE = null;
	
	/** PROCESO **/
	private final ProcessCodeType PROCESO_ACTUAL = ProcessCodeType.DESCARGAR_LOG_DE_TRANSACCIONES_AUTORIZADAS;

	/**
	 * Creates the instance.
	 */
	private static void createInstance() {
		synchronized (DownLoadLogAutorizacionSrv.class) {
			if (singleINSTANCE == null) {
				singleINSTANCE = new DownLoadLogAutorizacionSrv();
			}
		}
	}

	/**
	 * Patron singleton.
	 * 
	 * @return DownLoadLogAutorizacionSrv retorna instancia del servicio.
	 */
	public static DownLoadLogAutorizacionSrv getInstance() {
		if (singleINSTANCE == null) {
			createInstance();
		}
		return singleINSTANCE;
	}

	/**
	 * Metodo de restricion de sobreescritura de la clase.
	 */
	@Override
	protected Object clone() throws CloneNotSupportedException {
		super.clone();
		throw new CloneNotSupportedException();
	}

	/**
	 * Constructor Privado.
	 */
	private DownLoadLogAutorizacionSrv() {
		super();
	}

	/**
	 * Metodo principal orquesta la descarga del log de transacciones
	 * desde el SFTP de IC.
	 * @return Boolean true: descarga OK; false: descarga Fallida.
	 * @throws AppException Exception App.
	 */
	public boolean descargarLogAutorizacion() throws AppException{
		Boolean flag = Boolean.FALSE;
		List<String> listFTP = null;
		List<String> listToDownload = null;
		int countDownloadProcess = -1;
		String errorFile = null;
		
		ProcessValidator validadorProceso = new ProcessValidatorImpl();
		
		if (validadorProceso.isActiveProcess(PROCESO_ACTUAL)) {
		
		// BUSCAR LOS PARAMETROS NECESARIOS PARA EL FILE
		ParamDownloadLogAutorizacionDTO parametros = getParametros();
		
		if (validadorProceso.hasTheValidParametersForTheProcess( PROCESO_ACTUAL, parametros )) {
			LOG.info("===>Se han encontrado todos los parametros para comenzar el proceso :"+PROCESO_ACTUAL.getCodigoProceso()+" <==");
			// OBTENER NOMBRES ARCHIVOS A DESCARGAR
			listFTP = ClientFTPSrv.getInstance()
					.listFileSFTP(
							parametros.getDataFTP(),
							parametros.getPathFtpIcSalida());
			
			// SETEA EL OBJETO DE CONFIG FILE
			ConfigFile config = new ConfigFile();
			config.setStarWith(parametros.getFormatFileAbcLog());
			config.setEndsWith(parametros.getFormatExtNameAbcLog());
			
			// VALIDAR FORMATOS NOMBRES ARCHIVOS
			listToDownload = retrieveFilenameListToDownload(listFTP, config);
			
			// SI EXISTE ALGUN ARCHIVO VALIDO CONTINUA PROCESO
			if (listToDownload != null && listToDownload.size() > 0) {
				// VALIDAR ARCHIVOS QUE NO SE HAYAN DESCARGADO ANTES
				listToDownload = currentlyNotDownloadedFiles(listToDownload, parametros.getFormatFileAbcLog(), LogBD.TBL_LOG_TRANSAC_AUTOR);
				
				// VALIDACION QUE EXISTA ALGUNO
				if (listToDownload != null && listToDownload.size() > 0) {
					errorFile = ConstantesUtil.EMPTY.toString();
					countDownloadProcess = 0;
					for (String filename : listToDownload) {
						try {
							// DESCARGAR
							String pathFtpOri = parametros.getPathFtpIcSalida();
							String pathDest = parametros.getPathAbcLogTrx();

							LOG.info( "Comienza descarga de archivo "+filename+" desde ftp : "+ parametros.getDataFTP().getIpHost() );
							ClientFTPSrv.getInstance().downloadFileSFTP(
									parametros.getDataFTP(), pathFtpOri,
									pathDest, filename);

							// INSERTA EN LA TBL_LOG_TRANSAC_AUTOR
							save(filename, parametros.getFormatFileAbcLog());

							// CUENTA LOS REGISTROS INSERTADOS
							countDownloadProcess++;
						} catch (AppException e) {
							LOG.error("Ha ocurrido un error al intentar descargar el archivo : " + filename, e);
							errorFile = errorFile.concat(filename).concat(
									ConstantesUtil.SKIP_LINE.toString());
						}
					}
				} else {
					LOG.info("Todos los archivos de Log de Autorizacion han sido procesados");
					MailSendSrv.getInstance().sendMail(
							MsgErrorMail.ALERTA_FILE_PROCESSED_AUTO.toString(),
							MsgErrorMail.ALERTA_FILE_PROCESSED_AUTO_TXT.toString());
				}

			} else {
				LOG.info("No existen archivos de log de autorizacion que procesar");
				MailSendSrv.getInstance().sendMail(
						MsgErrorMail.ALERTA_FILE_PROCESS_AUTO.toString(),
						MsgErrorMail.ALERTA_FILE_PROCESS_AUTO_TXT.toString());
			}
		} else {
		    LOG.warn(MsgProceso.MSG_ERROR_FALTAN_PARAMETROS.toString());
			MailSendSrv.getInstance().sendMail(
					MsgErrorMail.ALERTA_PRTS.toString(),
					MsgErrorMail.ALERTA_PRTS_TXT.toString());
		}
		
		// SE SUBIERON CORRECTAMETNE TODOS LOS REGISTROS
		if (listToDownload != null
				&& countDownloadProcess == listToDownload.size()) {
			flag = Boolean.TRUE;
		} else if (errorFile != null) {
			MailSendSrv.getInstance().sendMail(
					MsgErrorMail.ALERTA_FILE_AUTO_LOAD.toString(),
					MsgErrorMail.ALERTA_FILE_LOAD_AUTO_TXT.toString().concat(
							errorFile));
		}
		return flag;
		
		}else{
		    String asunto = MsgProceso.MSG_PROCESO_DESACTIVADO_ASUNTO
                    .toString().replace( ":?:",
                            PROCESO_ACTUAL.getCodigoProceso() );
            String mensaje = MsgProceso.MSG_PROCESO_DESACTIVADO_BODY.toString()
                    .replace( ":?:", PROCESO_ACTUAL.getCodigoProceso() );
            MailSendSrv.getInstance().sendMail( asunto, mensaje );
		}
        return flag;
	}

	/**
	 * Metodo guarda el registro de la descarga de un archivo de un archivo 
	 * de log de autorizacion.
	 * @param fileName nombre del archivo.
	 * @param starWith comienza con.
	 * @throws AppException Exception App.
	 */
	private void save(final String fileName, final String starWith) throws AppException {
        // INICIALIZA EL DAO
        CommonsDao commonsDAO = null;
        try {
            commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
            TblCronLogDTO cronLog = new TblCronLogDTO();
            cronLog.setFecha( DateUtils.getDateFileTbk( DateUtils
                    .getDateStringFromFilename( fileName, starWith ) ) );
            cronLog.setFilename( fileName );
            cronLog.setFileFlag( StatusProcessType.PROCESS_PENDING.getValue() );                        
            if ( commonsDAO.saveTblCronLog( cronLog, LogBD.TABLE_LOG_TRANSAC_AUTOR ) ) {
                commonsDAO.endTx();
            }
            else {
                commonsDAO.rollBack();
            }
        }
        catch ( SQLException e ) {
			rollBackTransaccion(commonsDAO);
			throw new AppException(MsgErrorSQL.ERROR_SAVE_TBL_LOG.toString()
					+ LogBD.TABLE_LOG_TRANSAC_AUTOR.getTable(), e);
		} finally {
			try {
				if (commonsDAO != null) {
					commonsDAO.close();
				}
			} catch (SQLException e) {
				rollBackTransaccion(commonsDAO);
				throw new AppException(
						MsgErrorSQL.ERROR_SAVE_TBL_LOG.toString()
								+ LogBD.TABLE_LOG_TRANSAC_AUTOR.getTable(), e);
			}
		}
		
	}
	
	
	



	/**
	 * Metodo setea los parametros de base de datos del proceso de descarga de
	 * log de autorizacion (FTP:IC)
	 * @return DTO parametros.
	 * @throws AppException Exception App.
	 */
	private ParamDownloadLogAutorizacionDTO getParametros() throws AppException {
		ParamDownloadLogAutorizacionDTO parametros = null;
		List<ParametroDTO> paramDTOList = null;
		CommonsDao commonsDAO = null;
		DataFTP dataFTP = null;
		try {
			// INICIA EL DAO
			commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
			// SE OBTIENEN TODOS LOS CODIGOS
			paramDTOList = commonsDAO
					.getParametroCodGrupoDato(ConstantesPRTS.CRON_TBK
							.toString());
			LOG.info("LISTA DE PARAMETROS : " + paramDTOList.size());

			// SI EXISTEN PARAMETROS EN BD SE EXTRAEN
			if (paramDTOList != null) {

				// PARAMETROS DE FTP
				dataFTP = new DataFTP();
				dataFTP.setIpHost(CommonsUtils.getCodiDato(paramDTOList,
						ConstantesPRTS.SFTP_IP_DOWNLOAD_LOG_TRX_AUTH.toString()));
				String port = CommonsUtils.getCodiDato(paramDTOList, ConstantesPRTS.SFTP_PORT_DOWNLOAD_LOG_TRX_AUTH.toString()) ;
                dataFTP.setPort( Integer.parseInt((port != null && port.matches(ConstantesUtil.PATTERN_IS_NUMBER.toString()) ? port : ConstantesUtil.DEFAULT_PORT.toString())));
				dataFTP.setUser(CommonsUtils.getCodiDato(paramDTOList,
						ConstantesPRTS.SFTP_USER_DOWNLOAD_LOG_TRX_AUTH.toString()));
				dataFTP.setPassword(CommonsUtils.getCodiDato(paramDTOList,
						ConstantesPRTS.SFTP_PASS_DOWNLOAD_LOG_TRX_AUTH.toString()));

				// PARAMETROS DE COMISIONES
				parametros = new ParamDownloadLogAutorizacionDTO();
				// FORMATO DEL NOMBRE DEL ARCHIVO LOG AUTORIZACION DE ENTRADA DESDE IC
				parametros.setFormatFileAbcLog(CommonsUtils.getCodiDato(
						paramDTOList,
						ConstantesPRTS.FORMAT_FILE_ABC_LOG.toString()));
				//FORMATO EXTENSION ARCHIVO LOG AUTORIZACION
				parametros.setFormatExtNameAbcLog(CommonsUtils
						.getCodiDato(paramDTOList,
								ConstantesPRTS.FORMAT_EXTNAME_ABC_LOG
										.toString()));
				// RUTA SERVIDOR DONDE SE DEJARA EL ARCHIVO LOG DE AUTORIZACION DESCARGADO (DE ENTRADA CRON)
				parametros.setPathAbcLogTrx(CommonsUtils.getCodiDato(
						paramDTOList, ConstantesPRTS.PATH_ABC_LOG_TRX.toString()));

				// RUTA FTP IC DONDE BUSCA ARCHIVOS A DESCARGAR (SALIDA IC)
				parametros.setPathFtpIcSalida(CommonsUtils.getCodiDato(
						paramDTOList,
						ConstantesPRTS.PATH_OUT_DWNLD_LOG_TRX.toString()));
				parametros.setDataFTP(dataFTP);
				
			}
			return parametros;
		} catch (SQLException e) {
			throw new AppException(MsgErrorSQL.ERROR_QUERY_PRTS.toString(), e);
		} finally {
			try {
				if (commonsDAO != null) {
					commonsDAO.close();
				}
			} catch (SQLException e) {
				throw new AppException(
						MsgErrorSQL.ERROR_QUERY_PRTS_CLOSE.toString(), e);
			}
		}
	}

	
}
