package com.sbpay.sgi.cron.srv.utils;

import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dao.GenerarCorreoVencDAO;
import com.sbpay.sgi.cron.dao.GenerarCorreoVencDAOFactory;
import com.sbpay.sgi.cron.dto.ParamCorreoVencimientoDTO;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.enums.ConstantesPRTS;
import com.sbpay.sgi.cron.enums.MsgDownloadBol;
import com.sbpay.sgi.cron.enums.MsgErrorSQL;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.validation.ProcessValidator;
import com.sbpay.sgi.cron.validation.impl.ProcessValidatorImpl;

public class BodyMailSrv extends CommonSrv {
    
    private static final Logger LOGGER = Logger.getLogger( BodyMailSrv.class
            .getName() );
    private final ProcessCodeType PROCESO_ACTUAL = ProcessCodeType.GENERAR_CORREO_DE_AVISO_VENCIMIENTO;
    
    private static BodyMailSrv singleINSTANCE = null;
    
    private String bodyHtml = "<html>"
            + "<h4>Los siguientes valores representan las transacciones que estan por vencer : </h4>"
            + "<br>" + "<table border='1'>" + "<tr>"
            + "<th>Numero de tarjeta</th>" + "<th>Monto de transaccion</th>"
            + "<th>Fecha de vencimiento</th>" + "<th>Codigo de la razon</th>"
            + "<th>Descripcion de la razon</th>" + "</tr>";
    
    private BodyMailSrv() {
        super();
    }
    
    private static void createInstance() {
        synchronized ( BodyMailSrv.class ) {
            if ( singleINSTANCE == null ) {
                singleINSTANCE = new BodyMailSrv();
            }
        }
    }
    
    public static BodyMailSrv getInstance() {
        if ( singleINSTANCE == null ) {
            createInstance();
        }
        return singleINSTANCE;
    }
    
    @Override
    protected Object clone() throws CloneNotSupportedException {
        super.clone();
        throw new CloneNotSupportedException();
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @throws AppException
     * @since 1.X
     */
    public String crearCuerpoCorreo() throws AppException {
        
        ProcessValidator validadorProceso = new ProcessValidatorImpl();
        
        if ( validadorProceso.isActiveProcess( PROCESO_ACTUAL ) ) {
            LOGGER.info( "Proceso está activo" );
            
            List<ParamCorreoVencimientoDTO> listaVencimiento = obtenerListaVencimiento();
            
            if ( listaVencimiento == null ) {
                return null;
            }
            
            if ( listaVencimiento.size() == 0 ) {
                return null;
            }
            
            Locale locale = new Locale( "es" );
            NumberFormat currencyFormatter = NumberFormat
                    .getNumberInstance( locale );
            String cuerpoCorreo = bodyHtml;
            for ( ParamCorreoVencimientoDTO correoVencimientoDTO : listaVencimiento ) {
                cuerpoCorreo += "<tr>"
                        + "<td>"
                        + correoVencimientoDTO.getNumeroTC()
                        + "</td>"
                        + "<td>"
                        + currencyFormatter.format( correoVencimientoDTO
                                .getMonto() ) + "</td>" + "<td>"
                        + correoVencimientoDTO.getFecVencimiento() + "</td>"
                        + "<td>" + correoVencimientoDTO.getCodMotivo()
                        + "</td>" + "<td>" + correoVencimientoDTO.getMotivo()
                        + "</td>" + "</tr>";
            }
            cuerpoCorreo += "</table>" + "</html>";
            LOGGER.debug( "Cuerpo del correo " + cuerpoCorreo );
            return cuerpoCorreo;
            
        }
        else {
            throw new AppException(
                    MsgDownloadBol.WARN_PROCESS_DEACTIVATED.toString() );
            
        }
    }
    
    private List<ParamCorreoVencimientoDTO> obtenerListaVencimiento()
            throws AppException {
        GenerarCorreoVencDAO generarCorreoVencDAO = null;
        CommonsDao commonsDao = null;
        List<ParamCorreoVencimientoDTO> listaCorreoVencimientoDTOs = null;
        ParametroDTO parametroDTO = null;
        try {
            commonsDao = CommonsDaoFactory.getInstance().getNewEntidadDao();
            parametroDTO = commonsDao
                    .getParametroCodDato( ConstantesPRTS.DAYS_TRANSAC
                            .toString() );
            generarCorreoVencDAO = GenerarCorreoVencDAOFactory.getInstance()
                    .getNewEntidadDao();
            listaCorreoVencimientoDTOs = generarCorreoVencDAO
                    .obtenerTransVenc( Integer.parseInt( parametroDTO
                            .getValor() ) );
            
        }
        catch ( SQLException e ) {
            LOGGER.debug( e.getErrorCode(), e );
            throw new AppException( MsgErrorSQL.ERROR_MAIL_CLOSE.toString(), e );
        }
        finally {
            if ( commonsDao != null ) {
                try {
                    commonsDao.close();
                }
                catch ( SQLException e ) {
                    
                    LOGGER.error( e.getMessage(), e );
                }
            }
            
            if ( generarCorreoVencDAO != null ) {
                try {
                    generarCorreoVencDAO.close();
                }
                catch ( SQLException e ) {
                    LOGGER.error( e.getMessage(), e );
                }
            }
        }
        
        return listaCorreoVencimientoDTOs;
    }
}
