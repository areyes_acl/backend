package com.sbpay.sgi.cron.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.BaseDao;
import com.sbpay.sgi.cron.dto.CompensacionPagoDTO;
import com.sbpay.sgi.cron.enums.MsgErrorProcessFileCPago;

public class CompscnPagoJdbc extends BaseDao<CompscnPagoDao> implements CompscnPagoDao{
	
	  /** VARIABLE PARA EL LOGER */
    private static final Logger LOGGER = Logger.getLogger( CommonsDaoJdbc.class );

    private static final String CALL_SP_SIG_GUARDAR_CPAGO = "{call SP_SIG_GUARDAR_CPAGO(?,?,?,?,?,?,?,?)}";
    
    /**
     * Constructor con conexion.
     * 
     * @param connection
     *            Conexion JDBC.
     */
    public CompscnPagoJdbc( Connection connection ) {
        super( connection );
    }

	@Override
	public Boolean guardarCompscnPagoDao(CompensacionPagoDTO compensacionPagoDTO) throws AppException {
		// TODO Auto-generated method stub
	        CallableStatement callableStatement = null;
	        try {
	            LOGGER.debug("Ha comenzado el proceso de guardado de compensacion de pago");
	            callableStatement = this.getConnection().prepareCall( CALL_SP_SIG_GUARDAR_CPAGO );
	            callableStatement.setString( "FECHA", compensacionPagoDTO.getFecPago());
	            callableStatement.setInt( "CANTIDAD	", compensacionPagoDTO.getCantidad() );
	            callableStatement.setDouble( "MONTO_BRUTO", compensacionPagoDTO.getMontoPago());
	            callableStatement.setDouble( "NETA", compensacionPagoDTO.getNeta());
	            callableStatement.setDouble( "IVA", compensacionPagoDTO.getIva() );
	            callableStatement.setDouble( "TOTAL", compensacionPagoDTO.getTotal() );
	            callableStatement.setDouble( "NETO", compensacionPagoDTO.getNeto());
	            callableStatement.setInt( "TIPO	", compensacionPagoDTO.getTipoTransaccion() );
	            
	            callableStatement.execute();
	            
	            return Boolean.TRUE;
	        }
	        catch ( SQLException e ) {
	            throw new AppException( MsgErrorProcessFileCPago.ERROR_CALL_SP_SIG_GUARDAR_CPAGO.toString(), e );
	        }
	        finally {
	            try {
	                if ( callableStatement != null ) callableStatement.close();
	            }
	            catch ( SQLException e ) {
	                throw new AppException( MsgErrorProcessFileCPago.ERROR_CALL_SP_SIG_GUARDAR_CPAGO.toString(), e );
	            }
		}
	}
}
