package com.sbpay.sgi.cron.srv.process;

import java.io.File;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dao.ProcesarIncomingDao;
import com.sbpay.sgi.cron.dao.ProcesarIncomingDaoFactory;
import com.sbpay.sgi.cron.dto.IncomingDTO;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.dto.ParamsProcessOutVisaDTO;
import com.sbpay.sgi.cron.dto.TblCronLogDTO;
import com.sbpay.sgi.cron.dto.Transaccion;
import com.sbpay.sgi.cron.enums.ConstantesOperador;
import com.sbpay.sgi.cron.enums.ConstantesPRTS;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.FilterTypeSearch;
import com.sbpay.sgi.cron.enums.LogBD;
import com.sbpay.sgi.cron.enums.MsgErrorMail;
import com.sbpay.sgi.cron.enums.MsgErrorProcessFile;
import com.sbpay.sgi.cron.enums.MsgErrorSQL;
import com.sbpay.sgi.cron.enums.MsgExitoMail;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.enums.StatusProcessType;
import com.sbpay.sgi.cron.srv.utils.CommonSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;
import com.sbpay.sgi.cron.utils.file.IncomingFileUtils;
import com.sbpay.sgi.cron.utils.file.IncomingReader;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;
import com.sbpay.sgi.cron.validation.ProcessValidator;
import com.sbpay.sgi.cron.validation.impl.ProcessValidatorImpl;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 16/11/2015, (ACL) - versión inicial
 * </ul>
 * <p>
 * 
 * @author afreire
 * <p>
 * Este proceso se encarga de leer el archivo outgoing de visa y cargar las 
 * transacciones informadas en el Sistema, la lectura se realiza al buscar 
 * en una ruta local del servidor todos los archivos que posean el prefijo y 
 * terminen con una extension definida por la tabla de parametros TBL_PRTS a 
 * continuación se valida en la tabla TBL_LOG_VISA que el archivo no este 
 * procesado.
 * <B>Todos los derechos reservados por ACL SA.</B>
 */
public class ProcesarOutgoingVisaSrv extends CommonSrv {

  /** VARIABLE PARA EL LOGER */
  private static final Logger LOGGER = Logger.getLogger(ProcesarOutgoingVisaSrv.class);

  /** VARIABLE PARA UTILIZAR AL GUARDAR TRANSACCION */
  private String rutsbpay;

  /** The single instance. */
  private static ProcesarOutgoingVisaSrv singleINSTANCE = null;
  
  /** Proceso */
  private final ProcessCodeType PROCESO_ACTUAL = ProcessCodeType.PROCESAR_OUGOING_VISA;

  /**
   * Creates the instance.
   */
  private static void createInstance() {
    synchronized (ProcesarOutgoingVisaSrv.class) {
      if (singleINSTANCE == null) {
        singleINSTANCE = new ProcesarOutgoingVisaSrv();
      }
    }
  }

  /**
   * Patron singleton.
   * 
   * @return ActivarPrepagoSrv retorna instancia del servicio.
   */
  public static ProcesarOutgoingVisaSrv getInstance() {
    if (singleINSTANCE == null) {
      createInstance();
    }
    return singleINSTANCE;
  }

  /**
   * Metodo de restricion de sobreescritura de la clase.
   */
  @Override
  protected Object clone() throws CloneNotSupportedException {
    super.clone();
    throw new CloneNotSupportedException();
  }

  /**
   * Constructor Privado.
   */
  private ProcesarOutgoingVisaSrv() {
    super();
  }

  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
   * </ul>
   * <p>
   * 
   * @throws AppException
   * @since 1.X
   */
  public void procesar() throws AppException {
      
      ProcessValidator validadorProceso = new ProcessValidatorImpl();
      if (!validadorProceso.isActiveProcess(PROCESO_ACTUAL)) {
          LOGGER.info("Proceso : "+PROCESO_ACTUAL.getCodigoProceso()+ ", No se encuentra activo por BD (TBL_CRON_CONFIG)");
          String asunto = MsgProceso.MSG_PROCESO_DESACTIVADO_ASUNTO
                  .toString().replace( ":?:",
                          PROCESO_ACTUAL.getCodigoProceso() );
          String mensaje = MsgProceso.MSG_PROCESO_DESACTIVADO_BODY.toString()
                  .replace( ":?:", PROCESO_ACTUAL.getCodigoProceso() );
          MailSendSrv.getInstance().sendMail( asunto, mensaje );
          return;
      }
      
    // OBTIENE LOS PARAMETROS DESDE TBL_PRTS
    ParamsProcessOutVisaDTO params = getParametros();
    
    if (validadorProceso.hasTheValidParametersForTheProcess( PROCESO_ACTUAL, params )) {
      Integer countFileProcessed = 0;
      List<File> listOfFiles = null;

      // RECORRE LOS ARCHIVOS DEL FOLDER BUSCANDO ARCHIVOS INCOMING QUE TENGAN NOMBRES VALIDOS
      listOfFiles =CommonsUtils.getFilesInFolder(params.getPathVisaInc(), params.getFormatFilenameOutVisa(),params.getFormatExtNameOutVisa());

      LOGGER.info("Lista de archivos :  "
          + ((listOfFiles != null) ? "" + listOfFiles.size() : "No se encontraron archivos"));

      // SI NO EXISTE NINGUN ARCHIVO EN LA CARPETA TERMINA EL PROCESO
      if (listOfFiles == null) {
        LOGGER.info(MsgErrorProcessFile.WARNING_NO_FILES_FOUNDS.toString());
        return;
      }

      for (File file : listOfFiles) {
        LOGGER.info("NOMBRE ARCHIVO : " + file.getName());

        if (esValidoIncomingFile(file, params.getFormatFilenameOutVisa())) {
          try {
            LOGGER.info("Archivo Incoming VISA válido : Se comienza a procesar...");

            // 1- PROCESA ARCHIVO INCOMING
            IncomingFileUtils reader = new IncomingReader();
            IncomingDTO incoming = reader.readIncomingFile(file, params.getFormatFilenameOutVisa());

            // 2 - SE GUARDAN LAS TRANSACCIONES DE UN INCOMING
            saveData(incoming);

            // 3 - SI PROCESA EXITOSAMENTE SE MUEVE INCOMING A LA CARPETA BACKUP
            CommonsUtils.moveFile(file.getAbsolutePath(), params.getPathVisaIncBkp() + file.getName());
            countFileProcessed++;
          } catch (AppException e) {

            // ERROR AL PROCESAR INCOMING VISA SE ENVIA EMAIL
            LOGGER.info(MsgErrorProcessFile.ERROR_IN_PROCESS_OUTGOING_VISA.toString() + "-"
                + e);
            MailSendSrv.getInstance().sendMail(
                MsgErrorProcessFile.ERROR_IN_PROCESS_OUTGOING_VISA.toString(), e.getMessage());
            //

            // MUEVE ARCHIVO A LA CARPETA DE ERROR
            CommonsUtils.moveFile(file.getAbsolutePath(),
                params.getPathVisaIncError() + file.getName());
          }
        }
      }

      // SI NO SE PROCESA NINGUN FILE ESCRIBE ESCRIBE LOG
      String msj = "";
      if (countFileProcessed == 0) {
    	  LOGGER.info(MsgErrorProcessFile.WARNING_NO_FILE_PROCESSING_TITLE + "  - "
            + MsgErrorProcessFile.WARNING_NO_FILE_PROCESSING_CAUSE);
    	  msj = " Sin embargo, " + MsgErrorProcessFile.WARNING_NO_FILE_PROCESSING_CAUSE.toString().toLowerCase()+".";
      }

      // SI SE PROCESARON TODOS LOS ARCHIVOS EXISTENTES SE ENVIA CORREO
      if (listOfFiles.size() == countFileProcessed) {
        LOGGER.info("== Envio Correo de Proceso  Satisfactorio: Procesamiento Archivo Outgoing Visa ===");
        if(msj != null && !msj.isEmpty()){
        	  MailSendSrv.getInstance().sendMailOk(
        	            MsgExitoMail.EXITO_PROCESS_OUTGOING_VISA_TITLE.toString(),
        	            MsgExitoMail.EXITO_PROCESS_OUTGOING_VISA_CAUSE.toString().concat(ConstantesUtil.SKIP_LINE.toString().concat(msj)));
        }else{
        	  MailSendSrv.getInstance().sendMailOk(
        	            MsgExitoMail.EXITO_PROCESS_OUTGOING_VISA_TITLE.toString(),
        	            MsgExitoMail.EXITO_PROCESS_OUTGOING_VISA_CAUSE.toString());
        }
      
      }
    } else {
      LOGGER.info("Parametros de Base de Datos Incompletos");
      MailSendSrv.getInstance().sendMail(MsgErrorMail.ALERTA_PRTS.toString(),
          MsgErrorMail.ALERTA_PRTS_TXT.toString());
    }

  }

  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
   * </ul>
   * <p>
   * Metodo que valida si el incoming: -) Viene nulo -) Es un archivo -) Está o no ingresado en bd
   * en la tabla log si es asi se verifica su codigo si es 1- ya se encuentra procesado si es -1
   * fallo en procesamientos anteriores, y si es 0 se debe procesar.
   * 
   * @param filename
   * @return
   * @throws AppException
   * @since 1.X
   */
  private boolean esValidoIncomingFile(File file, String starWith) throws AppException {
    CommonsDao commonsDAO = null;
    try {

      if (file == null) {
    	  LOGGER.info("El archivo a leer viene nulo");
        return false;
      }

      if (!file.isFile()) {
    	  LOGGER.info("El file : " + file.getName() + ", no es un archivo incoming ");
        return false;
      }

      commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();

      // BUSCA REGISTRO LOG POR NOMBRE EN LOG_VISA
      LOGGER.info("Nombre Incoming: " + file.getName());
      TblCronLogDTO filter = new TblCronLogDTO();
      filter.setFilename(file.getName());

      TblCronLogDTO cronlog =
          commonsDAO.getTblCronLogByType(filter, LogBD.TABLE_LOG_VISA.getTable(),
              FilterTypeSearch.FILTER_BY_NAME);
      LOGGER.info("=====> Valor de TBL_LOG_VISA: " + cronlog);

      if (cronlog == null) {
        LOGGER.info(MsgErrorProcessFile.ERROR_INCOMING_VISA_NAME_NOT_FOUND.toString());
        return Boolean.FALSE;
      }
      // SI ES DISTINTO DE 0 NO ES VÁLIDO
      else if (!"0".equalsIgnoreCase(cronlog.getFileFlag())) {
        LOGGER.info(MsgErrorProcessFile.ERROR_INVALID_INCOMING_VISA_NO_OK_STATE.toString());
        return Boolean.FALSE;
      }

    } catch (SQLException e) {
      throw new AppException(MsgErrorSQL.ERROR_FIND_INCOMING_VISA_BY_NAME.toString(), e);
    } finally {

      try {
        if (commonsDAO != null) {
          commonsDAO.close();
        }
      } catch (SQLException e) {
        throw new AppException(MsgErrorSQL.ERROR_CLOSE_CONECTION.toString(), e);
      }
    }

    return Boolean.TRUE;

  }

  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
   * </ul>
   * <p>
   * Metodo encargado de recorrer las transacciones encontradas en el incoming y enviarlas una a una
   * al DAO para que sean almacenadas
   * 
   * @param incoming
   * @throws AppException
   * @since 1.X
   */
  private void saveData(IncomingDTO incoming) throws AppException {
    Integer flag = 0;
    ProcesarIncomingDao procesarDAO = null;
    CommonsDao commonsDAO = null;

    try {
      procesarDAO = ProcesarIncomingDaoFactory.getInstance().getNewEntidadDao();
      commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
      
      Integer sidOperador = commonsDAO.getOperador(ConstantesOperador.VISA_INT.toString());
      LOGGER.info( "sidOperador: "+sidOperador);

      // RECORRE TODAS LAS TRANSACCIONES DEL INCOMING
      for (Transaccion instance : incoming.getListaTransacciones()) {
        // ALMACENA RUTsbpay
        instance.setRutsbpay(rutsbpay);

        // GUARDA EN TMP_INCOMING, SI HAY ERROR SUMA -1
        if (!procesarDAO.saveTransaction(instance, sidOperador)) {
          flag = flag - 1;
        }
      }

      // COMMMIT DE TRANSACCIONES SAVE ACUMULADAS
      if (flag == 0) {
        procesarDAO.endTx();
        LOGGER.info("COMMITEA SAVE TRANSACCIONES");


        // SE UPDATEA EL FLAG A OK
        updateLog(StatusProcessType.PROCESS_SUCESSFUL.getValue(), incoming);

        // SE LLAMA AL SP CARGA INCOMING
        procesarDAO.callSPCargaIncoming();
        procesarDAO.endTx();
      } else {
        // SE REALIZA ROLLBACK
        rollBackTransaccion(procesarDAO);

      }
    } catch (SQLException e) {

      // SE UPDATEA EL FLAG A ERROR
      updateLog(StatusProcessType.PROCESS_ERROR.getValue(), incoming);


      throw new AppException(MsgErrorSQL.ERROR_SAVE_INCOMING.toString(), e);


    } finally {
      try {
        if (procesarDAO != null) {
          procesarDAO.close();
        }

      } catch (SQLException e) {
        LOGGER.error(e.getMessage(), e);
      }

    }

  }

  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
   * </ul>
   * <p>
   * 
   * Metodo que updatea la tabla de log de acuerdo si se ejecuto o no el procesamiento del archivo
   * incoming
   * 
   * @since 1.X
   */
  private void updateLog(String flagProcesso, IncomingDTO incoming) {

    CommonsDao commonsDAO = null;
    TblCronLogDTO tblCronLogDTO = null;

    try {
      LOGGER.info(" UPDATE FLAG -  FILENAME : " + incoming.getIncomingName());
      commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
      tblCronLogDTO = new TblCronLogDTO();
      tblCronLogDTO.setFilename(incoming.getIncomingName());
      tblCronLogDTO.setFileFlag(flagProcesso);

      // SE REALIZA EL UPDATE
      if (commonsDAO.updateTblCronLogByType(tblCronLogDTO, LogBD.TABLE_LOG_VISA.getTable(),
          FilterTypeSearch.FILTER_BY_NAME)) {
        LOGGER.info("COMMITEA UPDATE");
        commonsDAO.endTx();


      } else {
        // ROLLBACK CARGA UPDATE FLAG
        rollBackTransaccion(commonsDAO);
      }


    } catch (SQLException e) {
      LOGGER.info(e.getErrorCode() + " - " + e.getMessage());
    } catch (AppException e) {
      LOGGER.info(e.getStackTraceDescripcion());
    } finally {

      if (commonsDAO != null) {
        try {
          commonsDAO.close();
        } catch (SQLException e) {
          LOGGER.info(e.getErrorCode() + " - " + e.getMessage());
        }
      }

    }
  }
  
	

  /**
   * 
   * 
   * <p>
   * Registro de versiones:
   * <ul>
   * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
   * </ul>
   * <p>
   * 
   * Metodo setea los parametros de sistema del cron transbank.
   * 
   * @return
   * @throws AppException
   * @since 1.X
   */
  private ParamsProcessOutVisaDTO getParametros() throws AppException {
    ParamsProcessOutVisaDTO parametros = null;
    List<ParametroDTO> paramDTOList = null;
    CommonsDao commonsDAO = null;
    try {
      commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
      // SE OBTIENEN TODOS LOS CODIGOS
      paramDTOList = commonsDAO.getParametroCodGrupoDato(ConstantesPRTS.CRON_TBK.toString());

      // SI EXISTEN PARAMETROS EN BD SE EXTRAEN
      if (paramDTOList != null) {
        parametros = new ParamsProcessOutVisaDTO();
        // RUTA SERVIDOR DONDE SE LEERA EL ARCHIVO OUTGOING DE VISA PARA PROCESAR
        parametros.setPathVisaInc(CommonsUtils.getCodiDato(paramDTOList,
            ConstantesPRTS.PATH_VISA_INC.toString()));
		// RUTA SERVIDOR DONDE SE DEJARA EL ARCHIVO OUTGOING DE VISA PROCESADO CON EXITO ( BKP)     
        parametros.setPathVisaIncBkp(CommonsUtils.getCodiDato(paramDTOList,
            ConstantesPRTS.PATH_VISA_INC_BKP.toString()));
        // RUTA SERVIDOR DONDE SE DEJARA EL ARCHIVO OUTGOING DE VISA PROCESADO CON ERROR
        parametros.setPathVisaIncError(CommonsUtils.getCodiDato(paramDTOList,
            ConstantesPRTS.PATH_VISA_INC_ERROR.toString()));
        // FORMATO DE NOMBRE OUTGOING VISA
        parametros.setFormatFilenameOutVisa(CommonsUtils.getCodiDato(paramDTOList,
            ConstantesPRTS.FORMAT_FILENAME_OUT_VISA.toString()));
        // FORMATO DE LA EXTENSION DEL ARCHIVO VISA DE TRANSBANK
        parametros.setFormatExtNameOutVisa(CommonsUtils.getCodiDato(paramDTOList,
            ConstantesPRTS.FORMAT_EXTNAME_OUT_VISA.toString()));
        rutsbpay = CommonsUtils.getCodiDato(paramDTOList, ConstantesPRTS.RUT_COM_VI.toString());


      }
      LOGGER.info("Parametros  BD : " + parametros);
      return parametros;
    } catch (SQLException e) {
      throw new AppException(MsgErrorSQL.ERROR_QUERY_PRTS.toString(), e);
    } finally {
      try {
        if (commonsDAO != null) {
          commonsDAO.close();
        }
      } catch (SQLException e) {
        throw new AppException(MsgErrorSQL.ERROR_QUERY_PRTS_CLOSE.toString(), e);
      }
    }
  }

}
