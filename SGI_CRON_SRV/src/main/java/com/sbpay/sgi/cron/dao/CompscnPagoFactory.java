package com.sbpay.sgi.cron.dao;

import java.sql.SQLException;

import com.sbpay.sgi.cron.dao.BaseDaoFactory;
import com.sbpay.sgi.cron.ds.ConnectionProvider;

public class CompscnPagoFactory extends BaseDaoFactory<CompscnPagoDao>{

	/** The single instance. */
	private static CompscnPagoFactory singleINSTANCE = null;

	/**
	 * Creates the instance.
	 * 
	 * @throws SQLException
	 */
	private static void createInstance() throws SQLException {
		synchronized (CompscnPagoFactory.class) {
			if (singleINSTANCE == null) {
				singleINSTANCE = new CompscnPagoFactory();
			}
		}
	}

	/**
	 * Patron singleton.
	 * 
	 * @return ActivarPrepagoDaoFactory retorna instancia del servicio.
	 * @throws SQLException
	 */
	public static CompscnPagoFactory getInstance() throws SQLException {
		if (singleINSTANCE == null) {
			createInstance();
		}
		return singleINSTANCE;
	}

	/**
	 * Metodo de restricion de sobreescritura de la clase.
	 */
	@Override
	protected Object clone() throws CloneNotSupportedException {
		super.clone();
		throw new CloneNotSupportedException();
	}

	/**
	 * Constructor default.
	 * 
	 * @throws SQLException Error de SQL.
	 */
	private CompscnPagoFactory() throws SQLException {
		super();
	}

	/**
	 * Metodo obtiene la instancia mas la conexion.
	 */
	@Override
	public CompscnPagoJdbc getNewEntidadDao() throws SQLException {
		return new CompscnPagoJdbc(ConnectionProvider.getInstance()
				.getConnection());
	}
}
