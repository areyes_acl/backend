package com.sbpay.sgi.cron.dao;

import java.sql.SQLException;

import com.sbpay.sgi.cron.dao.BaseDaoFactory;
import com.sbpay.sgi.cron.ds.ConnectionProvider;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 10/12/2015, (ACL) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SA.</B>
 */
public class GenerarCargoAbonoDaoFactory extends
        BaseDaoFactory<GenerarCargoAbonoDAO> {
    
    /** The single instance. */
    private static GenerarCargoAbonoDaoFactory singleINSTANCE = null;
    
    /**
     * Creates the instance.
     * 
     * @throws SQLException
     */
    private static void createInstance() throws SQLException {
        synchronized ( GenerarCargoAbonoDaoFactory.class ) {
            if ( singleINSTANCE == null ) {
                singleINSTANCE = new GenerarCargoAbonoDaoFactory();
            }
        }
    }
    
    /**
     * Patron singleton.
     * 
     * @return ActivarPrepagoDaoFactory retorna instancia del
     *         servicio.
     * @throws SQLException
     */
    public static GenerarCargoAbonoDaoFactory getInstance() throws SQLException {
        if ( singleINSTANCE == null ) {
            createInstance();
        }
        return singleINSTANCE;
    }
    
    /**
     * Metodo de restricion de sobreescritura de la clase.
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        super.clone();
        throw new CloneNotSupportedException();
    }
    
    /**
     * Constructor default.
     * 
     * @throws SQLException
     *             Error de SQL.
     */
    private GenerarCargoAbonoDaoFactory() throws SQLException {
        super();
    }
    
    /**
     * Metodo obtiene la instancia mas la conexion.
     */
    @Override
    public GenerarCargoAbonoDAO getNewEntidadDao() throws SQLException {
        return new GenerarCargoAbonoJdbc( ConnectionProvider.getInstance()
                .getConnection() );
    }
    
}
