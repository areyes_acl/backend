package com.sbpay.sgi.cron.step;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.enums.MsgGenerationItzAvance;
import com.sbpay.sgi.cron.srv.export.GenerarItzAvanceSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 xx/xx/2018, (ACL) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SA.</B>
 */
public class StepGenerateItzAvance {
	 /**
     * VARIABLES
     */
    private static final Logger LOG = Logger
            .getLogger( StepGenerateItzAvance.class );
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2018, (ACL Chile) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @throws Exception
     * @since 1.X
     */
    public void execute() {
    	LOG.info("Inicio proceso de Generacion de archivo de avances : StepGenerateItzAvance ");
    	try {
			if (GenerarItzAvanceSrv.getInstance().generarArchivoAvances()) {
				MailSendSrv.getInstance().sendMailOk(MsgGenerationItzAvance.SUCCESFUL_GENERATE_ITZ_TITTLE.toString(),
													 MsgGenerationItzAvance.SUCCESFUL_GENERATE_ITZ_MSG.toString());
			} else {
				LOG.info("Generar/Exportar Interfaz de avances no realizados, Finaliza!");
			}
		} catch (AppException e) {
			LOG.error(e.getMessage(),e);
			MailSendSrv.getInstance().sendMail(MsgGenerationItzAvance.ERROR_GEN_ITZ_FILE.toString(),
  		          (e.getStackTraceDescripcion()!= null)?e.getStackTraceDescripcion(): e.getMessage());
		}
    }
}
