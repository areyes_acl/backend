package com.sbpay.sgi.cron.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.BaseDao;
import com.sbpay.sgi.cron.dto.ComisionesPagoDTO;
import com.sbpay.sgi.cron.enums.MsgErrorCargoAbono;

public class ComisionesDaoJdbc extends BaseDao<ComisionesDao> implements ComisionesDao {
    
    /** VARIABLE PARA EL LOGER */
    private static final Logger LOGGER = Logger.getLogger( ComisionesDaoJdbc.class );

    private static final String CALL_SP_SIG_GUARDARCOMISIONES = "{call SP_SIG_GUARDAR_COM(?,?,?,?,?,?)}";
    
    /**
     * Constructor con conexion.
     * 
     * @param connection
     *            Conexion JDBC.
     */
    public ComisionesDaoJdbc( Connection connection ) {
        super( connection );
    }

	@Override
	public Boolean guardarComision(ComisionesPagoDTO comisionesPagoDTO)throws Exception {
		// TODO Auto-generated method stub
        CallableStatement callableStatement = null;
        
        
        
        try {
            
            callableStatement = this.getConnection().prepareCall( CALL_SP_SIG_GUARDARCOMISIONES );
            callableStatement.setString( "FECHA", comisionesPagoDTO.getFecha());
            callableStatement.setString( "DETALLE", comisionesPagoDTO.getDetalle() );
            callableStatement.setInt( "COMISION", comisionesPagoDTO.getComision());
            callableStatement.setInt( "MONTO_VENTA", comisionesPagoDTO.getMontoVenta());
            callableStatement.setInt( "MONTO_CNC", comisionesPagoDTO.getMontoCNC() );
            callableStatement.setString( "SIGNO_VENTA", comisionesPagoDTO.getSignoVenta() );
            
            callableStatement.execute();
            
            return Boolean.TRUE;
        }
        catch ( SQLException e ) {
            throw new AppException(
                    MsgErrorCargoAbono.ERROR_CALL_SP_CARGASALIDACARGOABONO
                            .toString(),
                    e );
        }
        finally {
            try {
                if ( callableStatement != null ) {
                    callableStatement.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException(
                        MsgErrorCargoAbono.ERROR_CALL_SP_CAR_ABO_TBK_CLOSE_CONECTION
                                .toString(), e );
            }
        }
	}
    
 

}
