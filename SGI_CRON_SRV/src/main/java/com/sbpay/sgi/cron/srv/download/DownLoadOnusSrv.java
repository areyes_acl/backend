package com.sbpay.sgi.cron.srv.download;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dto.ConfigFile;
import com.sbpay.sgi.cron.dto.DataFTP;
import com.sbpay.sgi.cron.dto.ParamDownloadOnusDTO;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.dto.TblCronLogDTO;
import com.sbpay.sgi.cron.enums.ConstantesPRTS;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.LogBD;
import com.sbpay.sgi.cron.enums.MsgErrorMail;
import com.sbpay.sgi.cron.enums.MsgErrorSQL;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.enums.StatusProcessType;
import com.sbpay.sgi.cron.srv.utils.ClientFTPSrv;
import com.sbpay.sgi.cron.srv.utils.CommonSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;
import com.sbpay.sgi.cron.utils.generals.DateUtils;
import com.sbpay.sgi.cron.validation.ProcessValidator;
import com.sbpay.sgi.cron.validation.impl.ProcessValidatorImpl;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 14/01/2016, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * Clase encarga de descargar el archivo de de Transacciones On Us de IC  
 * validando que este en el ruta del FTP de salida de IC y que el archivo 
 * comience con "prefijo" y no este descargado ya antes esto se valida en 
 * la tabla TBL_LOG_ONUS, si no esta descargado se descarga y escribe en 
 * la dicha tabla.
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public final class DownLoadOnusSrv extends CommonSrv {
	/** The Constant LOGGER. */
	private static final Logger LOG = Logger.getLogger(DownLoadOnusSrv.class
			.getName());
	/** The single instance. */
	private static DownLoadOnusSrv singleINSTANCE = null;
	
	/** PROCESO **/
    private final ProcessCodeType PROCESO_ACTUAL = ProcessCodeType.DESCARGAR_ONUS;

	/**
	 * Creates the instance.
	 */
	private static void createInstance() {
		synchronized (DownLoadOnusSrv.class) {
			if (singleINSTANCE == null) {
				singleINSTANCE = new DownLoadOnusSrv();
			}
		}
	}

	/**
	 * Patron singleton.
	 * 
	 * @return DownLoadOnusSrv retorna instancia del servicio.
	 */
	public static DownLoadOnusSrv getInstance() {
		if (singleINSTANCE == null) {
			createInstance();
		}
		return singleINSTANCE;
	}

	/**
	 * Metodo de restricion de sobreescritura de la clase.
	 */
	@Override
	protected Object clone() throws CloneNotSupportedException {
		super.clone();
		throw new CloneNotSupportedException();
	}

	/**
	 * Constructor Privado.
	 */
	private DownLoadOnusSrv() {
		super();
	}

	/**
	 * Metodo descarga el archivo de transacciones ONUS desde el FTP
	 * de IC.
	 * @return boolean true: descarga OK; false: descarga fallida.
	 * @throws AppException Exception App.
	 */
	public boolean descargarOnus() throws AppException {
		Boolean flag = Boolean.FALSE;
		List<String> listFTP = null;
		List<String> listToDownload = null;
		int countDownloadProcess = -1;
		String errorFile = null;
		
		ProcessValidator validadorProceso = new ProcessValidatorImpl();

	    if (validadorProceso.isActiveProcess(PROCESO_ACTUAL)) {
	        
		// BUSCAR LOS PARAMETROS NECESARIOS PARA EL FILE
		ParamDownloadOnusDTO parametros = cargaParametros();
		
	    if (validadorProceso.hasTheValidParametersForTheProcess(PROCESO_ACTUAL, parametros)) {
			LOG.info( "SE HAN ECONTRADO TODOS LOS PARAMETROS PARA INCIAR EL PROCESO...");
			
			// OBTENER NOMBRES ARCHIVOS A DESCARGAR
			listFTP = ClientFTPSrv.getInstance().listFileSFTP(parametros.getDataFTP(),parametros.getPathFtpIcSalida());
			
			ConfigFile configFile = new ConfigFile();
			configFile.setStarWith(parametros.getFormatFileNameOnus());
			configFile.setEndsWith(parametros.getFormatExtNameOnus());
						
			// VALIDAR FORMATOS NOMBRES ARCHIVOS
			listToDownload = retrieveFilenameListToDownload(listFTP, configFile);
			// SI EXISTE ALGUN ARCHIVO VALIDO CONTINUA PROCESO
			if (listToDownload != null && listToDownload.size() > 0) {
				// VALIDAR QUE NO SE HAYAN DESCARGADO ANTES
				listToDownload = currentlyNotDownloadedFiles(listToDownload,parametros.getFormatFileNameOnus(),LogBD.TABLE_LOG_ONUS);
				
				// VALIDACION QUE EXISTA ALGUNO
				if (listToDownload != null && listToDownload.size() > 0) {
					errorFile = ConstantesUtil.EMPTY.toString();
					countDownloadProcess = 0;
					for (String filename : listToDownload) {
						try {
							// DESCARGAR
							String pathFtpOri = parametros.getPathFtpIcSalida();
							String pathDest = parametros.getPathAbcOnUs();
							
							LOG.info( "Comienza descarga de archivo: "+filename+", desde ftp : "+ parametros.getDataFTP().getIpHost() );
							ClientFTPSrv.getInstance().downloadFileSFTP(
									parametros.getDataFTP(), pathFtpOri,
									pathDest, filename);

							// INSERTA EN LA TBL_LOG_ONUS
							save(filename, parametros.getFormatFileNameOnus());

							// CUENTA LOS REGISTROS INSERTADOS
							countDownloadProcess++;
						} catch (AppException e) {
							LOG.error("Ha ocurrido un error al intentar descargar el archivo : " + filename, e);
							errorFile = errorFile.concat(filename).concat(
									ConstantesUtil.SKIP_LINE.toString());
						}
					}
				} else {
					LOG.info("Todos los archivos onus de IC han sido ya descargados, ver TBL_LOG_ONUS");
					MailSendSrv.getInstance().sendMail(
							MsgErrorMail.ALERTA_FILE_PROCESSED_ONUS.toString(),
							MsgErrorMail.ALERTA_FILE_PROCESSED_ONUS_TXT.toString());
				}

			} else {
				LOG.info("No existen archivos ONUS que procesar");
				MailSendSrv.getInstance().sendMail(
						MsgErrorMail.ALERTA_FILE_PROCESS_ONUS.toString(),
						MsgErrorMail.ALERTA_FILE_PROCESS_ONUS_TXT.toString());
			}
		} else {
		    LOG.warn(MsgProceso.MSG_ERROR_FALTAN_PARAMETROS.toString());
			MailSendSrv.getInstance().sendMail(
					MsgErrorMail.ALERTA_PRTS.toString(),
					MsgErrorMail.ALERTA_PRTS_TXT.toString());
		}
		
		// SE SUBIERON CORRECTAMETNE TODOS LOS REGISTROS
		if (listToDownload != null
				&& countDownloadProcess == listToDownload.size()) {
			flag = Boolean.TRUE;
		} else if (errorFile != null) {
			MailSendSrv.getInstance().sendMail(
					MsgErrorMail.ALERTA_FILE_ONUS_LOAD.toString(),
					MsgErrorMail.ALERTA_FILE_LOAD_ONUS_TXT.toString().concat(
							errorFile));
		}
		return flag;
		
	    }else{
	        String asunto = MsgProceso.MSG_PROCESO_DESACTIVADO_ASUNTO
                    .toString().replace( ":?:",
                            PROCESO_ACTUAL.getCodigoProceso() );
            String mensaje = MsgProceso.MSG_PROCESO_DESACTIVADO_BODY.toString()
                    .replace( ":?:", PROCESO_ACTUAL.getCodigoProceso() );
            MailSendSrv.getInstance().sendMail( asunto, mensaje );
	    }
        return flag;
	}

	/**
	 * Metodo guarda un registro en la tabla de log TBL_LOG_ONUS para registrar
	 * los archivos que han sido descargado.
	 * @param fileName nombre del archivo descargado.
	 * @param starWith comienza con.
	 * @throws AppException Exception App.
	 */
	private void save(final String fileName, final String starWith) throws AppException {
        // INICIALIZA EL DAO
        CommonsDao commonsDAO = null;
        try {
            commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
            TblCronLogDTO cronLog = new TblCronLogDTO();
            cronLog.setFecha( DateUtils.getDateFileTbk( DateUtils
                    .getDateStringFromFilename( fileName, starWith ) ) );
            cronLog.setFilename( fileName );
            cronLog.setFileFlag( StatusProcessType.PROCESS_PENDING.getValue() );    
            
            if ( commonsDAO.saveTblCronLog( cronLog, LogBD.TABLE_LOG_ONUS ) ) {
                commonsDAO.endTx();
            }
            else {
                commonsDAO.rollBack();
            }
        }
        catch ( SQLException e ) {
        	rollBackTransaccion(commonsDAO);
			throw new AppException(MsgErrorSQL.ERROR_SAVE_TBL_LOG.toString()
					+ LogBD.TABLE_LOG_ONUS.getTable(), e);
		} finally {
			try {
				if (commonsDAO != null) {
					commonsDAO.close();
				}
			} catch (SQLException e) {
				rollBackTransaccion(commonsDAO);
				throw new AppException(MsgErrorSQL.ERROR_SAVE_TBL_LOG.toString()
						+ LogBD.TABLE_LOG_ONUS.getTable(), e);
			}
		}
		
	}
	
	
	



	/**
	 * Metodo carga los parametros del paso de desarga Onus FTP IC.
	 * @return Dto de parametros.
	 * @throws AppException Exception App.
	 */
	private ParamDownloadOnusDTO cargaParametros() throws AppException {
		ParamDownloadOnusDTO parametros = null;
		List<ParametroDTO> paramDTOList = null;
		CommonsDao commonsDAO = null;
		DataFTP dataFTP = null;
		try {
			// INICIA EL DAO
			commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
			// SE OBTIENEN TODOS LOS CODIGOS
			paramDTOList = commonsDAO
					.getParametroCodGrupoDato(ConstantesPRTS.CRON_TBK
							.toString());

			// SI EXISTEN PARAMETROS EN BD SE EXTRAEN
			if (paramDTOList != null) {

				// PARAMETROS DE FTP IC
				dataFTP = new DataFTP();
				dataFTP.setIpHost(CommonsUtils.getCodiDato(paramDTOList,
						ConstantesPRTS.SFTP_IP_DOWNLOAD_ONUS.toString()));
				String port = CommonsUtils.getCodiDato(paramDTOList, ConstantesPRTS.SFTP_PORT_DOWNLOAD_ONUS.toString()) ;
	            dataFTP.setPort( Integer.parseInt((port != null && port.matches(ConstantesUtil.PATTERN_IS_NUMBER.toString()) ? port : ConstantesUtil.DEFAULT_PORT.toString())));
				
				dataFTP.setUser(CommonsUtils.getCodiDato(paramDTOList,
						ConstantesPRTS.SFTP_USER_DOWNLOAD_ONUS.toString()));
				dataFTP.setPassword(CommonsUtils.getCodiDato(paramDTOList,
						ConstantesPRTS.SFTP_PASS_DOWNLOAD_ONUS.toString()));

				// PARAMETROS DE TRANSACCIONES ON US
				parametros = new ParamDownloadOnusDTO();

				// FORMATO DEL NOMBRE DEL ARCHIVO ON US DE ENTRADA DESDE IC
				parametros.setFormatFileNameOnus(CommonsUtils.getCodiDato(paramDTOList,ConstantesPRTS.FORMAT_FILENAME_ONUS.toString()));
				
				//FORMATO EXTENSION ARCHIVO ON US
				parametros.setFormatExtNameOnus(CommonsUtils
						.getCodiDato(paramDTOList,
								ConstantesPRTS.FORMAT_EXTNAME_ONUS
										.toString()));
				
				// RUTA SERVIDOR DONDE SE DEJARA EL ARCHIVO ON US DESCARGADO (DE ENTRADA)
				parametros.setPathAbcOnUs(CommonsUtils.getCodiDato(
						paramDTOList, ConstantesPRTS.PATH_ABC_ONUS.toString()));

				// RUTA FTP IC DONDE BUSCA ARCHIVOS A DESCARGAR (SALIDA)
				parametros.setPathFtpIcSalida(CommonsUtils.getCodiDato(
						paramDTOList,
						ConstantesPRTS.PATH_OUT_DWNLD_ONUS.toString()));
				parametros.setDataFTP(dataFTP);
			}
			return parametros;
		} catch (SQLException e) {
			throw new AppException(MsgErrorSQL.ERROR_QUERY_PRTS.toString(), e);
		} finally {
			try {
				if (commonsDAO != null) {
					commonsDAO.close();
				}
			} catch (SQLException e) {
				throw new AppException(
						MsgErrorSQL.ERROR_QUERY_PRTS_CLOSE.toString(), e);
			}
		}
	}

}
