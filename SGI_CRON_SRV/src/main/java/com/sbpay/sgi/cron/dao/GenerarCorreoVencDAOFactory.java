package com.sbpay.sgi.cron.dao;

import java.sql.SQLException;

import com.sbpay.sgi.cron.dao.BaseDaoFactory;
import com.sbpay.sgi.cron.ds.ConnectionProvider;

public class GenerarCorreoVencDAOFactory extends BaseDaoFactory<GenerarCorreoVencDAO>{

	private static GenerarCorreoVencDAOFactory singleINSTANCE = null;
	
	/**
	 * Uso de singleton, cual verifica que la instancia creada sea reutilizada.
	 * @throws SQLException
	 */
	private static void createInstance()throws SQLException{
		synchronized (GenerarCorreoVencDAOFactory.class) {
			if(singleINSTANCE==null)singleINSTANCE = new GenerarCorreoVencDAOFactory();
		}
	}
	
	/**
	 * Obtiene la instancia con el valor de la clase, de no ser asì crea la instancia.
	 * @return GenerarCorreoVencDAOFactory
	 * @throws SQLException
	 */
	public static GenerarCorreoVencDAOFactory getInstance() throws SQLException{
		if(singleINSTANCE==null)createInstance();
		return singleINSTANCE;
	}
	
	/**
     * Metodo de restricion de sobreescritura de la clase.
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        super.clone();
        throw new CloneNotSupportedException();
    }
    
    /**
     * Constructor default.
     * 
     * @throws SQLException
     *             Error de SQL.
     */
    private GenerarCorreoVencDAOFactory() throws SQLException {
        super();
    }
    
	@Override
	public GenerarCorreoVencDAO getNewEntidadDao() throws SQLException {
		// TODO Auto-generated method stub
		return new GenerarCorreoVencDAOJdbc(ConnectionProvider.getInstance().getConnection());
	}

}
