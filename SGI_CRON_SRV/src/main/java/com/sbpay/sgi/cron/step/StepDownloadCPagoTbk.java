package com.sbpay.sgi.cron.step;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.enums.MsgExitoMail;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.srv.download.DownLoadCPagoSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;

public class StepDownloadCPagoTbk {
	
	private static final Logger LOG = Logger.getLogger( StepDownloadCPagoTbk.class );
	
	   public void execute() throws Exception {
	        
	        try {
	        	LOG.info( "==========> Inicio Proceso:  Descarga archivo de Compensacion de pagos desde SFTP Transbank <===========" );
	            if ( DownLoadCPagoSrv.getInstance().descargarArchivoCPago() ) {
	                LOG.info( "Envio Correo de Descarga : Proceso descargar archivo compensación de pago" );
	                MailSendSrv
	                        .getInstance()
	                        .sendMailOk(
	                                MsgExitoMail.EXITO_PROCESS_DOWNLOAD_CPAGO_TITLE
	                                        .toString(),
	                                MsgExitoMail.EXITO_PROCESS_DOWNLOAD_CPAGO_BODY
	                                        .toString() );
	                LOG.info( "==========> Fin de Proceso:  Descarga archivo de Compensacion de pagos desde SFTP Transbank, correctamente <===========" );
	            }
	            else {
	            	LOG.warn( "===> Fin proceso:  Descarga archivo de Compensacion de pagos desde SFTP Transbank, con observaciones <=======" );
	                
	            }
	            
	        }
	        catch ( AppException e ) {
	        	LOG.error( e.getMessage(), e );
	        	 MailSendSrv.getInstance().sendMail(
	                        MsgProceso.MSG_GENERIC_ERROR_MSJ.toString(),
	                        (e.getStackTraceDescripcion() != null) ? e
	                            .getStackTraceDescripcion() : e.getMessage());
	        }
	        
	    }
}