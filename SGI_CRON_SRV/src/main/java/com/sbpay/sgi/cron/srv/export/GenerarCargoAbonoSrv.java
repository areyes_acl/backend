package com.sbpay.sgi.cron.srv.export;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dao.GenerarCargoAbonoDAO;
import com.sbpay.sgi.cron.dao.GenerarCargoAbonoDaoFactory;
import com.sbpay.sgi.cron.dto.ParamCargoAbonoDTO;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.dto.TblCronLogDTO;
import com.sbpay.sgi.cron.dto.TmpSalidaCargoAbonoDTO;
import com.sbpay.sgi.cron.enums.ConstantesPRTS;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.DateFormatType;
import com.sbpay.sgi.cron.enums.LogBD;
import com.sbpay.sgi.cron.enums.MsgErrorCargoAbono;
import com.sbpay.sgi.cron.enums.MsgErrorMail;
import com.sbpay.sgi.cron.enums.MsgErrorSQL;
import com.sbpay.sgi.cron.enums.MsgGenerationIncoming;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.enums.StatusProcessType;
import com.sbpay.sgi.cron.srv.utils.CommonSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;
import com.sbpay.sgi.cron.utils.generals.DateUtils;
import com.sbpay.sgi.cron.validation.ProcessValidator;
import com.sbpay.sgi.cron.validation.impl.ProcessValidatorImpl;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 15/12/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * Clase que genera el archivo de salida de cargo y abonos de sbpay.
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public final class GenerarCargoAbonoSrv extends CommonSrv {
    /** The Constant LOGGER. */
    private static final Logger LOG = Logger
            .getLogger( GenerarCargoAbonoSrv.class.getName() );
    /** The single instance. */
    private static GenerarCargoAbonoSrv singleINSTANCE = null;
    
    /** variable parametros del Cron */
    private ParamCargoAbonoDTO paramDto = null;
    
    /** PROCESO */ 
    private final ProcessCodeType PROCESO_ACTUAL = ProcessCodeType.GENERAR_CARGOS_Y_ABONOS;
    
    /**
     * Creates the instance.
     */
    private static void createInstance() {
        synchronized ( GenerarCargoAbonoSrv.class ) {
            if ( singleINSTANCE == null ) {
                singleINSTANCE = new GenerarCargoAbonoSrv();
            }
        }
    }
    
    /**
     * Patron singleton.
     * 
     * @return GenerarCargoAbonoSrv retorna instancia del servicio.
     */
    public static GenerarCargoAbonoSrv getInstance() {
        if ( singleINSTANCE == null ) {
            createInstance();
        }
        return singleINSTANCE;
    }
    
    /**
     * Metodo de restricion de sobreescritura de la clase.
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        super.clone();
        throw new CloneNotSupportedException();
    }
    
    /**
     * Constructor Privado.
     */
    private GenerarCargoAbonoSrv() {
        super();
    }
    
    /**
     * Metodo genera el archivo de cargos y abonos de ABC-DIN, para a
     * contunuacion escribir en la tabla de log de cargos y rechazos
     * (TBL_LOG_CAR_ABO).
     * 
     * @return true: proceso OK; false: Ejeucion parcial y fallida.
     * @throws AppException
     */
    public boolean generarCargoAbonoTbk() throws AppException {
 
        paramDto = new ParamCargoAbonoDTO();
        LOG.info( "INICIO GENERACION CARGO ABONO" );
        List<TmpSalidaCargoAbonoDTO> list = null;
        CommonsDao dao = null;
        TblCronLogDTO dto = null;
        boolean flag = false;
        GenerarCargoAbonoDAO abonoCargoDAO = null;
        
        try {
            
            ProcessValidator validadorProceso = new ProcessValidatorImpl();
            
            if (validadorProceso.isActiveProcess(PROCESO_ACTUAL)) {
            getParametros();
          
            if ( validadorProceso.hasTheValidParametersForTheProcess( PROCESO_ACTUAL, paramDto )) {
            	abonoCargoDAO = GenerarCargoAbonoDaoFactory.getInstance().getNewEntidadDao();
                String cargoAbonoFilename = generateCargoAbonoFilename( paramDto );
                // SE DEJA POR SI ES QUE QUIEREN ARCHIVO DE CONTROL AL GENERAR CARGO Y ABONO
//                String controlFilename = generateControlFilename( paramDto );
                String controlFilename = null;
                String ruta = paramDto.getPathCarAbo();
                
                // LLAMA AL SP SP_SIG_CARGASALIDACARGOABONO
                if ( abonoCargoDAO.callSPCargaSalidaCargoAbono() ) {
                    LOG.info( "SE HA LLAMADO AL PROCEDIMIENTO CORRECTAMENTE" );
                    list = abonoCargoDAO.loadCargoAbono();
                    // ESCRIBE EN LOG ERROR NO SE ENCONTRARON
                    // RESULTADOS
                    if ( list != null && list.size() == 0 ) {
                        LOG.warn( MsgErrorCargoAbono.WARNING_TRANSACTIONS_NOT_FOUND
                                .toString() );
                    }
                    else {
                        // EXPORTA EL ARCHIVO CARGO ABONO
                        if ( exportCargoAbonoFile( cargoAbonoFilename,
                                controlFilename, ruta, list ) ) {
                            if ( abonoCargoDAO.callSPModEstSalidaCargoAbono() ) {
                                LOG.info( "SE CAMBIA ESTADO TRANSACCIONES" );
                                try {
                                    dao = CommonsDaoFactory.getInstance()
                                            .getNewEntidadDao();
                                    dto = setCronDto( cargoAbonoFilename );
                                    if ( dto != null ) {
                                        dao.saveTblCronLog( dto,
                                                LogBD.TABLE_LOG_CARGO_ABONO );
                                        dao.endTx();
                                        flag = true;
                                    }
                                    else {
                                        LOG.warn( "No fue posible realizar el update al archivo: "
                                                + cargoAbonoFilename );
                                        rollBackTransaccion( dao );
                                    }
                                }
                                catch ( SQLException e ) {
                                    rollBackTransaccion( dao );
                                    throw new AppException(
                                            MsgErrorCargoAbono.ERROR_INSERT_TBL_CRON_IC
                                                    .toString(), e );
                                }
                            }
                            else {
                                LOG.warn( "No fue posible modificar el estado de salida de la tabla de  cargo y abono" );
                                MailSendSrv
                                        .getInstance()
                                        .sendMail(
                                                MsgErrorMail.ALERTA_ABO_CAR_PROCESS
                                                        .toString(),
                                                MsgErrorMail.ALERTA_ABO_CAR_PROCESS_TXT
                                                        .toString() );
                            }
                        }
                        else {
                            LOG.info( "No existen transacciones de cargos y abonos que procesar" );
                        }
                    }
                }
            }
            else {
                LOG.warn( "Parametros de Base de Datos Incompletos" );
                MailSendSrv.getInstance().sendMail(
                        MsgErrorMail.ALERTA_ABO_CAR_PRTS.toString(),
                        MsgErrorMail.ALERTA_ABO_CAR_PRTS_TXT.toString() );
            }
            return flag;
            
        }else{
            String asunto = MsgProceso.MSG_PROCESO_DESACTIVADO_ASUNTO
                    .toString().replace( ":?:",
                            PROCESO_ACTUAL.getCodigoProceso() );
            String mensaje = MsgProceso.MSG_PROCESO_DESACTIVADO_BODY.toString()
                    .replace( ":?:", PROCESO_ACTUAL.getCodigoProceso() );
            MailSendSrv.getInstance().sendMail( asunto, mensaje );
        }
            
        }
        catch ( SQLException e1 ) {
            throw new AppException(
                    MsgErrorCargoAbono.ERROR_INIT_DAO.toString(), e1 );
        }
        finally {
            try {
                if ( abonoCargoDAO != null ) {
                    abonoCargoDAO.close();
                }
                if ( dao != null ) {
                    dao.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException(
                        MsgErrorCargoAbono.ERROR_INSERT_TBL_CRON_IC_CLOSE_CONECTION
                                .toString(), e );
            }
            
        }
        return flag;
        
    }
    
    /**
     * Metodo setea el DTO a guardar en la tabla de log de cargos y
     * abonos.
     * 
     * @param file
     *            nombre del archivo a procesar.
     * @return dto a guardar en la tabla de log.
     */
    private TblCronLogDTO setCronDto( String file ) {
        TblCronLogDTO dto = null;
        LOG.info( "METODO: setCronLog Cargo Abono" );
        String dateFile = DateUtils.getToDayDDMMYY();
        if ( dateFile != null ) {
            dto = new TblCronLogDTO();
            dto.setFecha( dateFile );
            dto.setFileFlag( StatusProcessType.PROCESS_PENDING.getValue());
            dto.setFilename( file );
        }
        return dto;
    }
    
    
//    /**
//     * Metodo genera el nombre del archivo de control del proceso de
//     * cargo y abonos.
//     * 
//     * @param paramDto
//     *            dto con los parametros del cron.
//     * @return String nombre del archivo de control.
//     */
//    private String generateControlFilename( final ParamCargoAbonoDTO paramDto ) {
//        String date = DateUtils.getTodayInYYMMDDFormat();
//        date = DateUtils.parseFormat( date, DateFormatType.FORMAT_YYMMDD,
//                DateFormatType.FORMAT_YYYYMMDD );
//        StringBuilder str = new StringBuilder();
//        str.append( paramDto.getFormatFileNameCarAbo() ).append( date )
//                .append( ConstantesUtil.POINT.toString() )
//                .append( paramDto.getFormatExtNameCarAboCrt() );
//        
//        return str.toString();
//    }
    
    /**
     * Metodo genera el nombre del archivo del proceso de cargo y
     * abonos.
     * 
     * @param paramDto
     *            dto con los parametros del cron.
     * @return String nombre del archivo de cargo y abonos.
     */
    private String generateCargoAbonoFilename( final ParamCargoAbonoDTO paramDto ) {
        String date = DateUtils.getTodayInYYMMDDFormat();
        date = DateUtils.parseFormat( date, DateFormatType.FORMAT_YYMMDD,
                DateFormatType.FORMAT_YYYYMMDD );
        StringBuilder str = new StringBuilder();
        str.append( paramDto.getFormatFileNameCarAbo() ).append( date )
                .append( ConstantesUtil.UNDERSCORE.toString() )
                .append( ConstantesUtil.ONE.toString() )
                .append( ConstantesUtil.POINT.toString() )
                .append( paramDto.getFormatExtNameCarAbo() );
        
        return str.toString();
    }
    
    /**
     * Metodo exportar el archivo de cargo y abono a partir de las
     * tabla TMP_SALIDA_CARGO_ABONO.
     * 
     * @param cargoAbonoFilename
     *            nombre del archivo de cargo y abono.
     * @param controlFilename
     *            nombre del archivo de control cargo y abono.
     * @param ruta
     *            ruta en el directorio de archivos de cargos y
     *            abonos.
     * @param list
     *            lista transacciones de cargos y abonos
     * @return boolean true: ok; false: error.
     * @throws AppException
     */
    private boolean exportCargoAbonoFile( final String cargoAbonoFilename,
            final String controlFilename, final String ruta,
            final List<TmpSalidaCargoAbonoDTO> list ) throws AppException {
        String rutaFile = ruta.concat( cargoAbonoFilename );
        File cargoAbonoFile = new File( rutaFile );
        try {
            generaArchivoCargoAbono( cargoAbonoFile, list );
            if(controlFilename != null){
              generaArchivoControl( ruta, controlFilename );
            }
            
            return Boolean.TRUE;
        }
        catch ( IOException e ) {
            throw new AppException(
                    MsgGenerationIncoming.ERROR_CREATE_INCOMING_FILE.toString(),
                    e );
        }
        
    }
    
    /**
     * Metodo genera el archivo de control de cargos y abono.
     * 
     * @param ruta
     *            ruta donde dejar el archivo.
     * @param controlFilename
     *            nombre del archivo
     * @throws IOException
     *             Exception de entrada y salida.
     */
    private void generaArchivoControl( final String ruta,
            final String controlFilename ) throws IOException {
        LOG.info( "=========> SE GENERA ARCHIVO DE CONTROL <==========" );
        String pathControlFile = ruta.concat( controlFilename );
        File controlFile = new File( pathControlFile );
        BufferedWriter bw = new BufferedWriter( new FileWriter( controlFile ) );
        bw.close();
        
    }
    
    /**
     * Metodo Genera el archivo de cargo de abono.
     * 
     * @param cargoAbonoFile
     *            variable File del archivo cargo abono.
     * @param list
     *            lista de transacciones de cargo y abonos.
     * @throws IOException
     *             Exception de entrada y salida.
     */
    private void generaArchivoCargoAbono( File cargoAbonoFile,
            final List<TmpSalidaCargoAbonoDTO> list ) throws IOException {
        LOG.info( "=========> SE GENERA ARCHIVO CARGO ABONO <==========" );
        StringBuilder contenido = new StringBuilder();
        int cont = 0;
        
        // GENERA EL CONTENIDO DE TRANSACCIONES
        for ( TmpSalidaCargoAbonoDTO dto : list ) {
            // SI ES ULTIMO REGISTRO
            if ( ( cont + 1 ) == list.size() ) {
                // contenido.append( getCargoAbonoFinalToString( dto )
                // );
            }
            else {
                contenido.append( getCargoAbonoToString( dto ) );
                cont++;
            }
        }
        
        // ESCRIBE EN ARCHIVO CARGO ABONO
        writeFile( contenido.toString(), cargoAbonoFile );
        
    }
    
//    /**
//     * Metodo setea la ultima linea del archivo cargo abono
//     * 
//     * @param dto
//     *            variable del archivo.
//     * @return cadena con el contenido.
//     */
//    private Object getCargoAbonoFinalToString( TmpSalidaCargoAbonoDTO dto ) {
//        StringBuilder str = new StringBuilder();
//        String espacio = ConstantesUtil.WHITE.toString();
//        str.append( CommonsUtils.concatStringRight( dto.getFilter1(), espacio,
//                600 ) );
//        return str.toString();
//    }
    
    /**
     * Metodo setea la lineas de contendio del archivo cargo y abonos.
     * 
     * @param dto
     *            parametros del archivo
     * @return cadena del archivo linea a linea 600 caracteres.
     */
    private String getCargoAbonoToString( final TmpSalidaCargoAbonoDTO dto ) {
        
        StringBuilder str = new StringBuilder();
        String cero = ConstantesUtil.ZERO.toString();
        String espacio = ConstantesUtil.WHITE.toString();
        
        // TIPO_REGISTRO
        str.append(CommonsUtils.concatStringLeft(getStringNull( dto.getTipoRegistro() ), cero, 2 ) )
                
        // COD_EMISOR
        .append(CommonsUtils.concatStringLeft(getStringNull( dto.getCodEmisor() ), cero, 3 ) )
                
        // TIPO_SOLICITUD
         .append(CommonsUtils.concatStringLeft(getStringNull( dto.getTipoSolicitud() ), cero, 2 ) )
                
         // COD_TARJETA
         .append(CommonsUtils.concatStringRight(getStringNull( dto.getCodTarjeta() ), espacio, 19 ) )
                
         // DESCRIPCION
         .append(CommonsUtils.concatStringRight( getStringNull( dto.getDescripcionTblCargosAbonos() ), espacio, 30 ) )
                
         // NRO_CUOTAS
         .append(CommonsUtils.concatStringLeft(getStringNull( dto.getNroCuotas() ), cero, 2 ) )
         
         // MONTO TRANSACCION CARGO ABONO
         .append(CommonsUtils.concatStringLeft(getStringNull( dto.getMontoTransacCargoAbono() ), cero, 11 ) )
                
         // FECHA_EFECTIVA
         .append(CommonsUtils.concatStringLeft(getStringNull( dto.getFechaEfectiva() ), espacio, 8 ) )
                
         // SUCURSAL RECAUDADORA
         .append(CommonsUtils.concatStringLeft( getStringNull( dto.getSucursalRecaudadora() ), cero, 3 ) )
         
         // TRANSAC_CODE
         .append(CommonsUtils.concatStringLeft(getStringNull( dto.getTransactionCode() ), cero, 6 ) )
                
         // HORA TRANSACCION
         .append(CommonsUtils.concatStringLeft(getStringNull( dto.getHoraTransaccion() ), cero, 6 ) )
                
         // MERCHANT NUMBER (COMERCIO)
         .append(CommonsUtils.concatStringRight(getStringNull( dto.getMerchantNumber() ), cero,9 ) )
                
         // MODO DE PAGO
          .append(CommonsUtils.concatStringLeft(getStringNull( dto.getPaymentMode() ), cero, 1 ) )
          
         // DETALLE DE PAGO
          .append(CommonsUtils.concatStringLeft( getStringNull( dto.getPaymentDetails() ), espacio, 25 ) )
                
          // MONEDA TRX
          .append(CommonsUtils.concatStringLeft(getStringNull( dto.getTransactionCurrency() ), espacio, 3 ) )
                
          // PAGO DIFERIDO
          .append(CommonsUtils.concatStringLeft(getStringNull( dto.getPagoDiferido() ), cero, 2 ) )
          
          // TASA DE INTERES
          .append(CommonsUtils.concatStringLeft(getStringNull( dto.getTasaInteres() ), cero, 6 ) )
          
          // FILLER 1
          .append(CommonsUtils.concatStringLeft(getStringNull( dto.getFilter1() ), espacio, 461 ) )
          
          // FILLER 2
          .append(CommonsUtils.concatStringLeft(getStringNull( dto.getFilter2() ), espacio, 1 ) )
          
          // SALTO DE LINEA
          .append( ConstantesUtil.SKIP_LINE.toString() );
        
        return str.toString();
    }
    
    /**
     * Metodo setea los parametros del proceso de generacion del
     * archivo de cargo y abono.
     * 
     * @throws AppException
     *             Exception App.
     */
    private void getParametros() throws AppException {
        
        CommonsDao daoCommon = null;
        List<ParametroDTO> paramDTOList = null;
        try {
            LOG.info( "CARGA PARAMETROS CARGO Y ABONOS" );
            daoCommon = CommonsDaoFactory.getInstance().getNewEntidadDao();
            paramDTOList = daoCommon
                    .getParametroCodGrupoDato( ConstantesPRTS.CRON_TBK
                            .toString() );
            if ( paramDTOList != null && paramDTOList.size() > 0 ) {
              
                paramDto = new ParamCargoAbonoDTO();
                // RUTA SERVIDOR sbpay DONDE SE DEJARAN LOS CARGOS Y ABONOS
                paramDto.setPathCarAbo( CommonsUtils.getCodiDato( paramDTOList,
                    ConstantesPRTS.PATH_CAR_ABO.toString() ) );
                
                // FORMATO DE NOMBRE DE ARCHIVO DE CARGOS Y ABONOS GENERADO
                paramDto.setFormatFileNameCarAbo( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.FORMAT_FILENAME_CAR_ABO.toString() ) );
        
                // FORMATO DE EXTENSION DE ARCHIVO DE CARGOS Y ABONOS
                paramDto.setFormatExtNameCarAbo( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.FORMAT_EXTNAME_CAR_ABO.toString() ) );
               
            }    else {
                paramDto = null;
            }
            
        }
        catch ( SQLException e ) {
            throw new AppException(
                    MsgErrorSQL.ERROR_QUERY_ABO_CARG_PRTS.toString(), e );
        }
        finally {
            try {
                if ( daoCommon != null ) {
                    daoCommon.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException(
                        MsgErrorSQL.ERROR_QUERY_ABO_CARG_PRTS_CLOSE.toString(),
                        e );
            }
        }
    }
    
    
    
    
}
