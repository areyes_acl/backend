package com.sbpay.sgi.cron.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.dao.BaseDao;
import com.sbpay.sgi.cron.dto.CronLogDTO;
import com.sbpay.sgi.cron.enums.ConstantesBD;

public class CargarDaoJdbc extends BaseDao<CargarDao> implements CargarDao {
    
    /** VARIABLE PARA EL LOGER */
    private static final Logger LOGGER = Logger.getLogger( CargarDaoJdbc.class );
    
    /**
     * Constructor con conexion.
     * 
     * @param connection
     *            Conexion JDBC.
     */
    public CargarDaoJdbc( Connection connection ) {
        super( connection );
    }
    
    @Override
    public CronLogDTO getCronLogByDate( final String date ) throws SQLException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        CronLogDTO cronLogDTO = null;
        try {
            StringBuilder str = new StringBuilder(
                    "select FECHA,  INCOMING_NAME, INCOMING_FLAG, INCOMING_TS, " )
                    .append( " OUTGOING_NAME, OUTGOING_FLAG, OUTGOING_TS  " )
                    .append( " from " )
                    .append( ConstantesBD.ESQUEMA.toString() )
                    .append(
                            ".TBL_CRON_LOG where TRUNC(FECHA) = to_date(?,'DD/MM/YY') " );
            LOGGER.info("CONSULTA SQL -> " +  str.toString() );
            stmt = this.getConnection().prepareStatement( str.toString() );
            stmt.setString( 1, date );
            rs = stmt.executeQuery();
            
            if ( rs.next() ) {
                cronLogDTO = new CronLogDTO();
                cronLogDTO.setFecha( rs.getString( "FECHA" ) );
                cronLogDTO.setIncomingName( rs.getString( "INCOMING_NAME" ) );
                cronLogDTO.setIncomingFlag( rs.getString( "INCOMING_FLAG" ) );
                cronLogDTO.setIncomingTs( rs.getString( "INCOMING_TS" ) );
                cronLogDTO.setOutgoingName( rs.getString( "OUTGOING_NAME" ) );
                cronLogDTO.setOutgoingFlag( rs.getString( "OUTGOING_FLAG" ) );
                cronLogDTO.setOutgoingTs( rs.getString( "OUTGOING_TS" ) );
                LOGGER.info(cronLogDTO.toString());
                
            }
            return cronLogDTO;
        }
        finally {
            if ( stmt != null ) {
                stmt.close();
            }
            if ( rs != null ) {
                rs.close();
            }
        }
    }
    
    @Override
    public boolean saveLogInc( CronLogDTO dto ) throws SQLException {
        PreparedStatement stmt = null;
        try {
			final StringBuilder str = new StringBuilder()
					.append("INSERT INTO ")
					.append(ConstantesBD.ESQUEMA.toString()).append(".")
					.append(ConstantesBD.TABLE_LOG_INCOMING.toString())
					.append(" (SID,FECHA,")
					.append(" FILE_NAME,FILE_FLAG,FILE_TS,")
					.append(" FILE_NAME_INT, FILE_FLAG_INT,FILE_TS_INT)")
					.append(" values (?,to_date(?,'DD/MM/RR'),?,?,SYSDATE,")
					.append(" null,null,null) ");
            stmt = this.getConnection().prepareStatement( str.toString() );
            setLongNull( Long.toString( dto.getSid() ), 1, stmt );
            setStringNull( dto.getFecha(), 2, stmt );
            setStringNull( dto.getIncomingName(), 3, stmt );
            setStringNull( dto.getIncomingFlag(), 4, stmt );
            //LOGGER.info( "SQL --> " + str.toString() );
            return ( stmt.executeUpdate() > 0 );
        }
        finally {
            if ( stmt != null ) {
                stmt.close();
            }
        }
    }
    /**
     * 	"FECHA" DATE, 
	"FILE_NAME" VARCHAR2(50 BYTE), 
	"FILE_FLAG" VARCHAR2(1 BYTE), 
	"FILE_TS" TIMESTAMP (6),
	"FILE_NAME_INT" VARCHAR2(50 BYTE), 
	"FILE_FLAG_INT" VARCHAR2(1 BYTE), 
	"FILE_TS_INT" TIMESTAMP (6)
   );
     */
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 - 24/11/2015, (ACL) - versión inicial
     * </ul>
     * <p>
     * 
     * @param flag
     * @param filename
     * @return
     * @throws SQLException
     * @since 1.X
     */
    @Override
    public boolean updateFlag( String flag, String filename )
            throws SQLException {
        PreparedStatement stmt = null;
        try {
            StringBuilder str = new StringBuilder().append( "UPDATE " )
                    .append( ConstantesBD.ESQUEMA.toString() )
                    .append( ".TBL_CRON_LOG SET" )
                    .append( " INCOMING_FLAG = ? " )
                    .append( " WHERE INCOMING_NAME=?" );
            this.getConnection().setAutoCommit( false );
            stmt = this.getConnection().prepareStatement( str.toString() );
            setStringNull( flag, 1, stmt );
            setStringNull( filename, 2, stmt );
            //LOGGER.info( "SQL  updateFlag--> " + str.toString() );
            return ( stmt.executeUpdate() > 0 );
        }
        finally {
            if ( stmt != null ) {
                stmt.close();
            }
        }
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2015, (Everis Chile) -  versión inicial 
     * </ul>
     * <p>
     *   
     * @param incomingName
     * @return
     * @throws SQLException
     * @see com.sbpay.sgi.cron.dao.CargarDao#getByIncomingName(java.lang.String)
     * @since 1.X
     */
    @Override
    public CronLogDTO getByIncomingName( String incomingName )
            throws SQLException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        CronLogDTO cronLogDTO = null;
        try {
            StringBuilder str = new StringBuilder(
                    "select FECHA,  INCOMING_NAME, INCOMING_FLAG, INCOMING_TS, " )
                    .append( " OUTGOING_NAME, OUTGOING_FLAG, OUTGOING_TS  " )
                    .append( " from " )
                    .append( ConstantesBD.ESQUEMA.toString() )
                    .append( ".TBL_CRON_LOG where INCOMING_NAME = ? " );
            stmt = this.getConnection().prepareStatement( str.toString() );
            stmt.setString( 1, incomingName );
            LOGGER.info( "Query---> :  " + str.toString() );
            rs = stmt.executeQuery();
            
            if ( rs.next() ) {
                cronLogDTO = new CronLogDTO();
                cronLogDTO.setFecha( rs.getString( "FECHA" ) );
                cronLogDTO.setIncomingName( rs.getString( "INCOMING_NAME" ) );
                cronLogDTO.setIncomingFlag( rs.getString( "INCOMING_FLAG" ) );
                cronLogDTO.setIncomingTs( rs.getString( "INCOMING_TS" ) );
                cronLogDTO.setOutgoingName( rs.getString( "OUTGOING_NAME" ) );
                cronLogDTO.setOutgoingFlag( rs.getString( "OUTGOING_FLAG" ) );
                cronLogDTO.setOutgoingTs( rs.getString( "OUTGOING_TS" ) );
                
            }
            return cronLogDTO;
        }
        finally {
            if ( stmt != null ) {
                stmt.close();
            }
            if ( rs != null ) {
                rs.close();
            }
        }
    }

}
