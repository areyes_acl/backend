package com.sbpay.sgi.cron.srv.download;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dto.ConfigFile;
import com.sbpay.sgi.cron.dto.DataFTP;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.dto.ParamsReadCPagoDTO;
import com.sbpay.sgi.cron.dto.TblCronLogDTO;
import com.sbpay.sgi.cron.enums.ConstantesPRTS;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.LogBD;
import com.sbpay.sgi.cron.enums.MsgErrorMail;
import com.sbpay.sgi.cron.enums.MsgErrorSQL;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.enums.StatusProcessType;
import com.sbpay.sgi.cron.srv.utils.ClientFTPSrv;
import com.sbpay.sgi.cron.srv.utils.CommonSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;
import com.sbpay.sgi.cron.utils.generals.DateUtils;
import com.sbpay.sgi.cron.validation.ProcessValidator;
import com.sbpay.sgi.cron.validation.impl.ProcessValidatorImpl;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class DownLoadCPagoSrv extends CommonSrv{

	/** The Constant LOGGER. */
	private static final Logger LOG = Logger.getLogger(DownLoadCPagoSrv.class);
	
	/** PROCESO ACTUAL **/
	private final ProcessCodeType PROCESO_ACTUAL = ProcessCodeType.DESCARGAR_COMPENSACION_DE_PAGOS;

	/** The single instance. */
	private static DownLoadCPagoSrv singleINSTANCE = null;

	private DownLoadCPagoSrv(){
		super();
	}
	
	/**
	 * Creates the instance.
	 */
	private static void createInstance() {
		synchronized (DownLoadOnusSrv.class) {
			if (singleINSTANCE == null) singleINSTANCE = new DownLoadCPagoSrv();
		}
	}

	/**
	 * Patron singleton.
	 * 
	 * @return DownLoadOnusSrv retorna instancia del servicio.
	 */
	public static DownLoadCPagoSrv getInstance() {
		if (singleINSTANCE == null) createInstance();
		return singleINSTANCE;
	}
	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
	 * </ul>
	 * <p>
	 * Metodo principal que orquesata la descargar del archivo comisiones de pago
	 * 
	 * @return
	 * @throws AppException
	 * @since 1.X
	 */ 
	public boolean descargarArchivoCPago() throws AppException {
	        
	        boolean flag = Boolean.FALSE;
	        ParamsReadCPagoDTO parametros = null;
	        List<String> listFTP = null;
	        List<String> listToDownload = null;
	        int countDownloadProcess = 0;
	        String errorFile = null;
	        
	        ProcessValidator validadorProceso = new ProcessValidatorImpl();
	        
	        if (validadorProceso.isActiveProcess(PROCESO_ACTUAL)) {
	            
	        // BUSCA LOS PARAMETROS DE BD DEL CRON
	        parametros = getParametros();
	            
			if (validadorProceso.hasTheValidParametersForTheProcess( PROCESO_ACTUAL, parametros )) {
			  LOG.info("PARAMETROS VALIDOS PARA INICIAR LA DESCARGA DE ARCHIVO C. PAGO");
				// OBTENER NOMBRES ARCHIVOS A DESCARGAR
				listFTP = ClientFTPSrv.getInstance().listFileSFTP(parametros.getDataFTP(), parametros.getPathFTPPathCPago());

				// VALIDAR FORMATOS NOMBRES ARCHIVOS
				ConfigFile configFile = new ConfigFile();
				configFile.setEndsWith("");
				configFile.setStarWith(parametros.getFileNameCPago());
				configFile.setStarWithCtr(parametros.getFileNameCPago());
				
				listToDownload = retrieveFilenameListToDownload(listFTP, configFile);
				
				LOG.info("Lista de archivos validos en el SFTP Transbank : " + listToDownload);
				// SI EXISTE ALGUN ARCHIVO VALIDO CONTINUA PROCESO
				if (listToDownload != null && listToDownload.size() > 0) {

					// VALIDAR QUE NO SE HAYAN DESCARGADO ANTES
					listToDownload = currentlyNotDownloadedFilesCP(listToDownload, parametros.getFileNameCPago(), LogBD.TABLE_LOG_CPAGO);
					
					LOG.info("Lista de archivos que no han sido ya descargados SFTP Transbank :\n " + listToDownload);
					// VALIDACION QUE EXISTA ALGUNO
					if (listToDownload != null && listToDownload.size() > 0) {
						errorFile = ConstantesUtil.EMPTY.toString();
						for (String filename : listToDownload) {
							try {
								// DESCARGAR
								String pathFtpOri = parametros.getPathFTPPathCPago();
								String pathDest = parametros.getPathCPAGO();
								
								LOG.info( "Comienza descarga de archivo "+filename+" desde ftp : "+ parametros.getDataFTP().getIpHost() );
								ClientFTPSrv.getInstance().downloadFileSFTP(
								    parametros.getDataFTP(), pathFtpOri,
										pathDest, filename);

								// INSERTA EN LA TABLA TBL_LOG_CPAGO
								save(filename, parametros.getFileNameCPago());

								// CUENTA LOS REGISTROS INSERTADOS
								countDownloadProcess++;
							} catch (AppException e) {
								LOG.error("Ha ocurrido un error al intentar descargar el archivo : " + filename, e);
								errorFile = errorFile.concat(filename).concat(
										ConstantesUtil.SKIP_LINE.toString());
							}
						}
					} else {
						LOG.info("No existen archivos para procesar, todos fueron ya procesados");
						MailSendSrv.getInstance().sendMail(
								MsgErrorMail.ALERTA_DOWNLOAD_ALL_FILES_ALREADY_PROCESSED_CPAGO_TITLE.toString(),
								MsgErrorMail.ALERTA_DOWNLOAD_ALL_FILES_ALREADY_PROCESSED_CPAGO_BODY.toString());
					}
				} else {
				  LOG.info("No existen archivos compensación de pago que cumplan con el formato del nombre, para procesar");
					MailSendSrv.getInstance().sendMail(
							MsgErrorMail.ALERTA_DOWNLOAD_CPAGO_NO_FILES_FOUND_TITLE.toString(),
							MsgErrorMail.ALERTA_DOWNLOAD_CPAGO_NO_FILES_FOUND_BODY.toString());
				}
			} else {
			    LOG.warn(MsgProceso.MSG_ERROR_FALTAN_PARAMETROS.toString());
				MailSendSrv.getInstance().sendMail(
						MsgErrorMail.ALERTA_PRTS.toString(),
						MsgErrorMail.ALERTA_PRTS_TXT.toString());
			}

			// SE SUBIERON CORRECTAMETNE TODOS LOS REGISTROS
			if (listToDownload != null
					&& countDownloadProcess == listToDownload.size()) {
				flag = Boolean.TRUE;
			} else if (errorFile != null) {
				MailSendSrv.getInstance().sendMail(
						MsgErrorMail.ALERTA_DOWNLOAD_CPAGO_PROBLEM_TITLE.toString(),
						MsgErrorMail.ALERTA_DOWNLOAD_CPAGO_PROBLEM_BODY.toString().concat(
								errorFile));
			}

			return flag;
			
	        }else{
	            String asunto = MsgProceso.MSG_PROCESO_DESACTIVADO_ASUNTO
	                    .toString().replace( ":?:",
	                            PROCESO_ACTUAL.getCodigoProceso() );
	            String mensaje = MsgProceso.MSG_PROCESO_DESACTIVADO_BODY.toString()
	                    .replace( ":?:", PROCESO_ACTUAL.getCodigoProceso() );
	            MailSendSrv.getInstance().sendMail( asunto, mensaje );
	        }
            return flag;
	        
	    }
	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
	 * </ul>
	 * <p>
	 * Metodo encargado de registrar en la Tabla TBL_LOG_CPAGO
	 * los archivos descargados desde ftp de transbank
	 * 
	 * @param fileName
	 * @param starWith
	 * @throws AppException
	 * @since 1.X
	 */
	private void save(final String fileName, final String starWith) throws AppException {
        // INICIALIZA EL DAO
        CommonsDao commonsDAO = null;
        try {
            commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
            TblCronLogDTO cronLog = new TblCronLogDTO();
            cronLog.setFecha( DateUtils.getDateFileTbkCP( DateUtils.getDateStringFromFilename( fileName, starWith ) ) );
            cronLog.setFilename( fileName );
            cronLog.setFileFlag( StatusProcessType.PROCESS_PENDING.getValue() );

            if ( commonsDAO.saveTblCronLog( cronLog, LogBD.TABLE_LOG_CPAGO ) ) {
                commonsDAO.endTx();
            }
            else {
                commonsDAO.rollBack();
            }
        }
        catch ( SQLException e ) {
        	rollBackTransaccion(commonsDAO);
            throw new AppException(
            		MsgErrorSQL.ERROR_LOAD_PROCESS.toString(), e );
		} finally {
			try {
				if (commonsDAO != null) {
					commonsDAO.close();
				}
			} catch (SQLException e) {
				rollBackTransaccion(commonsDAO);
				throw new AppException(
						MsgErrorSQL.ERROR_LOAD_PROCESS.toString(), e);
			}
		}
		
	}

	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
	 * </ul>
	 * <p>
	 * Metodo que obtiene los parametros necesarios para el proceso 
	 * de descarga de compensacion de pagos
	 * 
	 * @return
	 * @throws AppException
	 * @since 1.X
	 */
	private ParamsReadCPagoDTO getParametros() throws AppException {

        CommonsDao daoCommon = null;
        List<ParametroDTO> paramDTOList = null;
        DataFTP dataFTP = null;
        ParamsReadCPagoDTO paramCPagoDTO = null;
        try {
            LOG.info( "CARGA PARAMETROS CRON PROCESO DOWNLOAD CPAGO" );
            daoCommon = CommonsDaoFactory.getInstance().getNewEntidadDao();
            paramDTOList = daoCommon.getParametroCodGrupoDato( ConstantesPRTS.CRON_TBK.toString() );
            if ( paramDTOList != null  ) {
            	
                // PARAMETROS DE FTP TRANSBANK
                dataFTP = new DataFTP();
                dataFTP.setIpHost( CommonsUtils.getCodiDato( paramDTOList, ConstantesPRTS.SFTP_IP_DOWNLOAD_CPAGO.toString() ) );
                String port = CommonsUtils.getCodiDato(paramDTOList, ConstantesPRTS.SFTP_PORT_DOWNLOAD_CPAGO.toString()) ;
                dataFTP.setPort( Integer.parseInt((port != null && port.matches(ConstantesUtil.PATTERN_IS_NUMBER.toString()) ? port : ConstantesUtil.DEFAULT_PORT.toString())));
                dataFTP.setUser( CommonsUtils.getCodiDato( paramDTOList, ConstantesPRTS.SFTP_USER_DOWNLOAD_CPAGO.toString() ) );
                dataFTP.setPassword( CommonsUtils.getCodiDato( paramDTOList, ConstantesPRTS.SFTP_PASS_DOWNLOAD_CPAGO.toString() ) );

                // PARAMETROS PROCESO COMPENSACION DE PAGOS
                paramCPagoDTO = new ParamsReadCPagoDTO();
                
                // RUTA DE FOLDER DONDE SE ENCUENTRAN LAS CPAGO EN EL SFTP DE TRANSBANK A DESCARGAR
                paramCPagoDTO.setPathFTPPathCPago(CommonsUtils.getCodiDato( paramDTOList, ConstantesPRTS.PATH_OUT_DWNLD_CPAGO.toString() ) );
                
                // RUTA DE FOLDER EN EL SERVER DE sbpay DONDE QUEDARAN LOS ARCHIVOS CPAGO DESCARGADOS
                paramCPagoDTO.setPathCPAGO(CommonsUtils.getCodiDato( paramDTOList, ConstantesPRTS.PATH_CPAGO.toString()));
                                
                // FORMATO DE EXTENSION DE ARCHIVO CPAGO
                paramCPagoDTO.setFileNameCPago(CommonsUtils.getCodiDato( paramDTOList, ConstantesPRTS.FORMAT_FILENAME_CPAGO.toString()));
               
                // DATOS FTP
                paramCPagoDTO.setDataFTP(dataFTP);
                LOG.info("PARAMETROD  : "+ paramCPagoDTO);
            }
            return paramCPagoDTO;
        }
        catch ( Exception e ) {
            throw new AppException( MsgErrorSQL.ERROR_QUERY_PRTS.toString(), e );
        }
        finally {
            try {
                if ( daoCommon != null ) {
                    daoCommon.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException(
                        MsgErrorSQL.ERROR_QUERY_PRTS_CLOSE.toString(), e );
            }
        }
	}
	
	
 
}
