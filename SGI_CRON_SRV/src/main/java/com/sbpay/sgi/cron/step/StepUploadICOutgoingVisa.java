package com.sbpay.sgi.cron.step;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.enums.MsgErrorMail;
import com.sbpay.sgi.cron.enums.MsgExitoMail;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.srv.upload.UploadICOutgoingVisaSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 09/02/2016, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * Step configura la subida del Outgoing de Visa a IC
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class StepUploadICOutgoingVisa {
    /** The Constant LOGGER. */
    private static final Logger LOG = Logger
            .getLogger( StepUploadICOutgoingVisa.class );
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 8/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * Metodo ejecuta el servicio que sube via FTP el Outgoing de Visa a IC
     * @throws Exception
     * @since 1.X
     */
    public void execute() throws Exception {
        try {
            LOG.info( "Inicio Proceso: Upload Outgoing Visa Internacional a FTP IC [StepUploadOutgoingVisa] " );
            if ( UploadICOutgoingVisaSrv.getInstance().upload()) {
                LOG.info( "Envio Correo de Upload  Satisfactorio: Proceso Upload Outgoing Visa A FTP IC" );
                MailSendSrv.getInstance().sendMailOk(
                        MsgExitoMail.EXITO_PROCESS_UPLOAD_OUT_VISA_IC_TITLE.toString(),
                        MsgExitoMail.EXITO_PROCESS_UPLOAD_OUT_VISA_BODY.toString() );
                
            }
            else {
                LOG.info( MsgErrorMail.ERROR_PROCESS_UPLOAD_OUT_VISA_BODY.toString() );
                
            }
        }
        catch ( AppException e ) {
            LOG.error( e.getMessage(), e );
            MailSendSrv.getInstance().sendMail(
                    MsgProceso.MSG_GENERIC_ERROR_MSJ.toString(),
                    ( e.getStackTraceDescripcion() != null ) ? e
                            .getStackTraceDescripcion() : e.getMessage() );
            throw new Exception( e.getMessage(), e );
        }
    }
}
