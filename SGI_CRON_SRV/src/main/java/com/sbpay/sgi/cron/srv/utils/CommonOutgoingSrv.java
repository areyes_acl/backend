package com.sbpay.sgi.cron.srv.utils;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dao.ProcesarIncomingVisaDaoFactory;
import com.sbpay.sgi.cron.dao.User;
import com.sbpay.sgi.cron.dto.CronTaskParam;
import com.sbpay.sgi.cron.dto.TblCronLogDTO;
import com.sbpay.sgi.cron.enums.ConstantesOperador;
import com.sbpay.sgi.cron.enums.FilterTypeSearch;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.enums.StatusProcessType;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;

/**
 * 
 * Clase encargada de validar
 * 
 * @author dmedina
 * @date 03-11-2015
 *
 */
public class CommonOutgoingSrv {
	/**
	 * Clase para imprimir en el log.
	 */
	private static final Logger LOGGER = Logger.getLogger(CommonOutgoingSrv.class.getName());
    
	

	public static void errorProcessOutgoing(File file, AppException e, String ERROR_IN_PROCESS_OUTGOING, String PatchAclIncError, String TABLE_LOG_INCOMING) {
		String filename = file.getName();
		// ERROR AL PROCESAR INCOMING SE ENVIA EMAIL
        LOGGER.error(ERROR_IN_PROCESS_OUTGOING, e );
        MailSendSrv.getInstance().sendMail( ERROR_IN_PROCESS_OUTGOING.toString(), e.getMessage() );
        // MUEVE ARCHIVO A LA CARPETA DE ERROR
        CommonsUtils.moveFile( file.getAbsolutePath(), PatchAclIncError + filename );
        // SE UPDATEA EL FLAG A PROCESADO CON ERROR
        TblCronLogDTO tblCronLogDTO = new TblCronLogDTO();
        tblCronLogDTO.setFilename( file.getName() );
        tblCronLogDTO.setFileFlag( StatusProcessType.PROCESS_ERROR.getValue() );
        try{
         LOGGER.info("actualizando el log de procesamiento");
	    CommonsDao commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
        if (commonsDAO.updateTblCronLogByType(tblCronLogDTO, TABLE_LOG_INCOMING, FilterTypeSearch.FILTER_BY_NAME)) {
        	LOGGER.info("SE ACTUALIZA CORRECTAMENTE TABLA "+TABLE_LOG_INCOMING+" PARA FILE  :" + filename);
              commonsDAO.endTx();
              commonsDAO.close();
            } else {
            	LOGGER.warn("NO SE ACTUALIZA TABLA "+TABLE_LOG_INCOMING+" PARA FILE  :" + filename);
              commonsDAO.rollBack();
            }
		}catch(SQLException s){
			LOGGER.error(s);
		}
        
	}
	
	public static void updateTblCronConfig(String paramName, int est) {
	
        try{
         LOGGER.info("actualizando tabla TblCronConfig");
	    CommonsDao commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
        if (commonsDAO.updateTblCronConfig(paramName, est)) {
        	LOGGER.info("SE ACTUALIZA CORRECTAMENTE TABLA TblCronConfig");
              commonsDAO.endTx();
              commonsDAO.close();
            } else {
            	LOGGER.warn("NO SE ACTUALIZA TABLA TblCronConfig");
              commonsDAO.rollBack();
            }
		}catch(SQLException s){
			LOGGER.error(s);
		}
        
	}
	
	public static void updateValor(int sid, String valor){
		
		try{
			LOGGER.info("actualizar valor de los parametros del cron");
			CommonsDao commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
			
			if(commonsDAO.updateValorParams(sid, valor)){
				LOGGER.info("Se actualizan los parametros del cron");
	            commonsDAO.endTx();
			}
			else{
				LOGGER.warn("No se han podido actualziar los parametros del cron");
	            commonsDAO.rollBack();
			}
			commonsDAO.close();
			
		}
		catch(SQLException e){
			LOGGER.error(e);
		}
		
		
	}
	
	public static boolean getUsuarioServ(String username, String pass)
	{
		boolean var = false;
		 try{
	         LOGGER.info("Buscando usuario");
		    CommonsDao commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
	        if (commonsDAO.getUsuario(username, pass)) {
	        	LOGGER.info("Se encontró el usuario");
	              var = true;
	            } else {
	            	LOGGER.warn("Usuario no encontrado");
	            }
	        commonsDAO.close();
			}catch(SQLException s){
				LOGGER.error(s);
			}
		 return var;
	}
	
	
	public static Integer getIDOperador(String id){
		CommonsDao commonsDAO;
		Integer sidOperador = 0;
		try {
			commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
			 sidOperador = commonsDAO.getIDOperador(id);
			 commonsDAO.close();
		} catch (SQLException e) {
			LOGGER.error("Error al recuperar el ID del operador : "+e);
		}
	
	return sidOperador;
    }
	
	//Método para devolver los parámetros de una tarea determinada
	public static List<CronTaskParam> datosParametros(int tarea)
	{
		CommonsDao commonsDAO;
		List<CronTaskParam> result= new ArrayList<CronTaskParam>();
		
		try{
			commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();	
			result = commonsDAO.datosParametros(tarea);
			commonsDAO.close();
		}catch(SQLException e){
			LOGGER.error("Error al recuperar los parámetros"+e);
			
		}
		
		return result;
		
	}
	
}


