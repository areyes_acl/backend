package com.sbpay.sgi.cron.step;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.enums.MsgExitoMail;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.srv.download.DownLoadComisionesSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 7/1/2016, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class StepDownloadComisionesTbk {
    /** The Constant LOGGER. */
    private static final Logger LOG = Logger
            .getLogger( StepDownloadComisionesTbk.class );
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 12/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws Exception
     * @since 1.X
     */
    public void execute() throws Exception {
        
        try {
            LOG.info( "=======> Inicio Proceso:  Download Comisiones desde SFTP Transbank <========" );
            if ( DownLoadComisionesSrv.getInstance().descargarComisiones() ) {
                LOG.info( "Envio Correo de Descarga Satisfactoria: Proceso DownLoad Comisiones" );
                MailSendSrv
                        .getInstance()
                        .sendMailOk(
                                MsgExitoMail.EXITO_PROCESS_DOWNLOAD_COM_TITLE
                                        .toString(),
                                MsgExitoMail.EXITO_PROCESS_DOWNLOAD_COM_BODY
                                        .toString() );
                LOG.info( "=======> Finaliza de proceso:  Download Comisiones desde SFTP Transbank <========" );
            }
            else {
                LOG.warn( "====>Finaliza Proceso :DownLoad Comisiones, sin embargo se ha ejecutado con observaciones... ver log" );
                
            }
            
        }
        catch ( AppException e ) {
            LOG.error( e.getMessage(), e );
            MailSendSrv.getInstance().sendMail(
                    MsgProceso.MSG_GENERIC_ERROR_MSJ.toString(),
                    ( e.getStackTraceDescripcion() != null ) ? e
                            .getStackTraceDescripcion() : e.getMessage() );
        }
        
    }
    
}
