package com.sbpay.sgi.cron.srv.download;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dto.DataFTP;
import com.sbpay.sgi.cron.dto.ParamDownloadComisionesDTO;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.dto.TblCronLogDTO;
import com.sbpay.sgi.cron.enums.ConstantesPRTS;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.FilterTypeSearch;
import com.sbpay.sgi.cron.enums.LogBD;
import com.sbpay.sgi.cron.enums.MsgDownloadComisionProcess;
import com.sbpay.sgi.cron.enums.MsgErrorMail;
import com.sbpay.sgi.cron.enums.MsgErrorSQL;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.enums.StatusProcessType;
import com.sbpay.sgi.cron.srv.utils.ClientFTPSrv;
import com.sbpay.sgi.cron.srv.utils.CommonSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;
import com.sbpay.sgi.cron.validation.ProcessValidator;
import com.sbpay.sgi.cron.validation.impl.ProcessValidatorImpl;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 12/01/2016, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * Clase encargada de descargar el archivo las comisiones. (archivo mensual)
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public final class DownLoadComisionesSrv extends CommonSrv {
    /** The Constant LOGGER. */
    private static final Logger LOG = Logger
            .getLogger( DownLoadComisionesSrv.class.getName() );
    /** The single instance. */
    private static DownLoadComisionesSrv singleINSTANCE = null;
    
    /** Proceso **/
    private final ProcessCodeType PROCESO_ACTUAL = ProcessCodeType.DESCARGAR_COMISIONES;
    
    
    /**
     * Creates the instance.
     */
    private static void createInstance() {
        synchronized ( DownLoadComisionesSrv.class ) {
            if ( singleINSTANCE == null ) {
                singleINSTANCE = new DownLoadComisionesSrv();
            }
        }
    }
    
    /**
     * Patron singleton.
     * 
     * @return DownLoadComisionesSrv retorna instancia del servicio.
     */
    public static DownLoadComisionesSrv getInstance() {
        if ( singleINSTANCE == null ) {
            createInstance();
        }
        return singleINSTANCE;
    }
    
    /**
     * Metodo de restricion de sobreescritura de la clase.
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        super.clone();
        throw new CloneNotSupportedException();
    }
    
    /**
     * Constructor Privado.
     */
    private DownLoadComisionesSrv() {
        super();
    }

    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
     * </ul>
     * <p>
     *  Metodo encargado de administrar la descarga de comisiones desde el FTP
     * de Transbank.(el archivo es mensual)
     * 
     * @return boolean true: Ok descarga; false: Falla descarga.
     * @throws AppException Exception App.
     * @throws AppException
     * @since 1.X
     */
	public boolean descargarComisiones() throws AppException {
        Boolean flag = Boolean.FALSE;
        List<String> listFTP = null;
        List<String> listToDownload = null;
        int countDownloadProcess = -1;
        String fecha = null;
        TblCronLogDTO dto = null;
        ProcessValidator validadorProceso = new ProcessValidatorImpl();
        
        LOG.info( "==== Inicio de Descargar Comisiones CEC ====" );
        
        // VALIDA SI PROCESO SE ENCUENTRA ACTIVADO/DESACTIVADO
        if ( validadorProceso.isActiveProcess( PROCESO_ACTUAL ) ) {
            
		// BUSCAR LOS PARAMETROS NECESARIOS PARA EL FILE
        ParamDownloadComisionesDTO parametros = getParametros();
       
        if( validadorProceso.hasTheValidParametersForTheProcess( PROCESO_ACTUAL, parametros )){
          		
            // OBTENER LISTA DE NOMBRES DE ARCHIVOS EN CASILLA TRANSBANK
            listFTP = ClientFTPSrv.getInstance()
                    .listFileSFTP( parametros.getDataFTP(),
                            parametros.getPathFtpTbkComision() );
            
            LOG.info( "Lista de archivos : "+ listFTP  );
            
            // SE OBTIENEN SOLO LA LISTA DE ARCHIVOS QUE CUMPLEN CON EL FORMATO DEL NOMBRE
            listToDownload = retrieveFilenameListToDownload( listFTP,
                    parametros);
            
            LOG.info( "Lista de archivos listToDownload : "+ listToDownload  );
            
    	
    		if (listToDownload != null && listToDownload.size() > 0) {
    			countDownloadProcess = 0;
    			fecha = getDateFileComisionProcess();
    			for ( String filename : listToDownload ) {
    				dto = new TblCronLogDTO();
                    try {
                        
                        if(!archivoYaDescargado(filename)){
                        
                        // DESCARGAR
                        String pathFtpOri = parametros.getPathFtpTbkComision();
                        String pathLocalDest = parametros.getPathTbkComision();
                        
                        LOG.info( "Comienza descarga de archivo "+filename+" desde ftp : "+ parametros.getDataFTP().getIpHost() );
                        ClientFTPSrv.getInstance().downloadFileSFTP(
                                parametros.getDataFTP(), pathFtpOri,
                                pathLocalDest,filename );
                        dto.setFecha(fecha);
                        dto.setFilename(filename);
                        dto.setFileFlag(StatusProcessType.PROCESS_PENDING.getValue());
                        // INSERTA EN LA TBL_LOG_COMISION
                        save(dto);
                        
                        // CUENTA LOS REGISTROS INSERTADOS
                        countDownloadProcess++;
                        
                      }else{
                          LOG.info( "Archivo De comisiones :" +filename+ " - ya ha sido descargado" );
                      }
                        
                        
                    }
                    catch ( AppException e ) {
                                LOG.error(
                                        "Ha ocurrido un error al intentar descargar el archivo : "
                                                + filename, e );
                    }
                    
                }
    		} else {
    	        MailSendSrv.getInstance().sendMail(
                        MsgDownloadComisionProcess.PROCESS_FINISH_WITH_WARN.toString(),
                        MsgDownloadComisionProcess.ALL_FILES_HAS_NOT_VALID_FORMAT
                                .toString() );
                LOG.info( MsgDownloadComisionProcess.ALL_FILES_HAS_NOT_VALID_FORMAT
                        .toString() );
    		}
    		
    		// SE SUBIERON CORRECTAMENTE TODOS LOS REGISTROS
            if ( listToDownload != null
                    && countDownloadProcess == listToDownload.size() ) {
                flag = Boolean.TRUE;
            }		
        }else{
            LOG.warn(MsgProceso.MSG_ERROR_FALTAN_PARAMETROS.toString());
            MailSendSrv.getInstance().sendMail(
                  MsgErrorMail.ALERTA_PRTS.toString(),
                  MsgErrorMail.ALERTA_PRTS_TXT.toString());
        }
		return flag;
		
        }else{
            String asunto = MsgProceso.MSG_PROCESO_DESACTIVADO_ASUNTO
                    .toString().replace( ":?:",
                            PROCESO_ACTUAL.getCodigoProceso() );
            String mensaje = MsgProceso.MSG_PROCESO_DESACTIVADO_BODY.toString()
                    .replace( ":?:", PROCESO_ACTUAL.getCodigoProceso() );
            MailSendSrv.getInstance().sendMail( asunto, mensaje );
        }
        return flag;
	}

  
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
	 * </ul>
	 * <p>
	 * 
	 * @param filename
	 * @return
	 * @throws AppException 
	 * @since 1.X
	 */
    private boolean archivoYaDescargado( String filename ) throws AppException {
        CommonsDao commonsDAO = null;
        try {
            commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
            TblCronLogDTO logTbl = new TblCronLogDTO();
            logTbl.setFilename( filename );
            
            logTbl = commonsDAO.getTblCronLogByType( logTbl,
                    LogBD.TABLE_LOG_COMISION.getTable(),
                    FilterTypeSearch.FILTER_BY_NAME );
            
            if ( logTbl != null ) {
                return Boolean.TRUE;
            }
        }
        catch ( SQLException e ) {
            
            throw new AppException(
                    MsgDownloadComisionProcess.ERROR_SEARCH_LOG.toString(), e );
        }
        finally {
            if ( commonsDAO != null ) {
                try {
                    commonsDAO.close();
                }
                catch ( SQLException e ) {
                    throw new AppException(
                            MsgDownloadComisionProcess.ERROR_SEARCH_LOG
                                    .toString(),
                            e );
                }
            }
        }
        return Boolean.FALSE;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * Metodo registra en la tabla de log TBL_LOG_COMISION la descarga
     * del archivo de comisiones.
     * 
     * @param dto
     *            DTO de la tabla de comisiones.
     * @throws AppException
     *             Exception App.
     * 
     * @param dto
     * @throws AppException
     * @since 1.X
     */
	private void save(TblCronLogDTO dto) throws AppException {
        // INICIALIZA EL DAO
        CommonsDao commonsDAO = null;
        try {
            commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();   
            if ( commonsDAO.saveTblCronLog( dto, LogBD.TABLE_LOG_COMISION ) ) {
                commonsDAO.endTx();
            }
            else {
                commonsDAO.rollBack();
            }
        }
        catch ( SQLException e ) {
            
            throw new AppException(
                    MsgDownloadComisionProcess.ERROR_SAVE_LOG.toString(), e );
		} finally {
			if (commonsDAO != null) {
				try {
					commonsDAO.close();
				} catch (SQLException e) {
					throw new AppException(
		                    MsgDownloadComisionProcess.ERROR_SAVE_LOG.toString(), e );
				}
			}
		}
	}

	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
	 * </ul>
	 * <p>
	 * 
     * Metodo busca en una lista de archivos los que comiencien con y terminen
     * con su respectivo archivo de control. 
     * @param listFTP lista de archivos.
     * @param parametros parametros de busqueda.
     * @return Lista final resultado del filtro.
     * @since 1.X
	 */
	private List<String> retrieveFilenameListToDownload(List<String> listFTP,
			ParamDownloadComisionesDTO parametros) {
		List<String> filenamesToCtr = new ArrayList<String>();
		List<String> filenamesToCom = new ArrayList<String>();
		List<String> filenamesToDownLoad = new ArrayList<String>();
		String starWithCtrl = parametros.getFormatFileControlTbkComision();
		String endsWithCtrl = parametros.getFormatExtNameControlTbkComision();
		String starWith = parametros.getFormatFileTbkComision();
		String endsWith = parametros.getFormatExtNameTbkComision();
		String fileAux = null;
		for (String filename : listFTP) {
//			if (CommonsUtils.validateFilename(filename, starWithCtrl,
//					endsWithCtrl)) {
//				filenamesToCtr.add(filename);
//			}
			if (CommonsUtils.validateFilename(filename, starWith, endsWith)) {
				filenamesToCom.add(filename);			
			}

		}
//		for (String fileCtrl : filenamesToCtr) {
//			fileAux = fileCtrl.replace(endsWithCtrl, endsWith);
//			for (String file : filenamesToCom) {
//				if (fileAux.equalsIgnoreCase(file)) {
//					filenamesToDownLoad.add(file);
//					break;
//				}
//			}
//		}

		return filenamesToCom;
	}
	
    

	
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
     * </ul>
     * <p>
     * 
     * Fecha de descarga del archivo sysdate
     * @return fecha DD/MM/YY.
     * @since 1.X
     */
    private String getDateFileComisionProcess() {
        
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy");
        StringBuilder str = new StringBuilder().append(sdf.format(cal.getTime()));
        return str.toString();
    }
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
     * </ul>
     * <p>
     * Metodo que obtiene los parametros necesarios para el proceso de descarga de comisiones
     * @return
     * @throws AppException
     * @since 1.X
     */
	private ParamDownloadComisionesDTO getParametros() throws AppException {
		ParamDownloadComisionesDTO parametros = null;
        List<ParametroDTO> paramDTOList = null;
        CommonsDao commonsDAO = null;
        DataFTP dataFTP = null;
        try {
            // INICIA EL DAO
            commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
            // SE OBTIENEN TODOS LOS CODIGOS
            paramDTOList = commonsDAO
                    .getParametroCodGrupoDato( ConstantesPRTS.CRON_TBK
                            .toString() );
            
            // SI EXISTEN PARAMETROS EN BD SE EXTRAEN
            if ( paramDTOList != null ) {
                
                // PARAMETROS DE SFTP TRANSBANK
                dataFTP = new DataFTP();
                dataFTP.setIpHost( CommonsUtils.getCodiDato( paramDTOList,ConstantesPRTS.SFTP_IP_DOWNLOAD_COMISSION.toString() ) );
                String port = CommonsUtils.getCodiDato(paramDTOList, ConstantesPRTS.SFTP_PORT_DOWNLOAD_COMISSION.toString());
                dataFTP.setPort( Integer.parseInt((port != null && port.matches(ConstantesUtil.PATTERN_IS_NUMBER.toString()) ? port : ConstantesUtil.DEFAULT_PORT.toString())));
                dataFTP.setUser( CommonsUtils.getCodiDato( paramDTOList,ConstantesPRTS.SFTP_USER_DOWNLOAD_COMISSION.toString() ) );
                dataFTP.setPassword( CommonsUtils.getCodiDato( paramDTOList,ConstantesPRTS.SFTP_PASS_DOWNLOAD_COMISSION.toString() ) );
                
                // PARAMETROS PROCESO COMISIONES
                parametros = new ParamDownloadComisionesDTO();
                
               // RUTA SFTP DE TRANSBANK DONDE ESTARAN LAS COMISIONES A DESCARGAR
                parametros.setPathFtpTbkComision( CommonsUtils.getCodiDato(paramDTOList,ConstantesPRTS.PATH_OUT_DWNLD_COMISSION.toString() ) );
                
                // RUTA SERVIDOR sbpay DONDE SE DESCARGARAN LAS COMISIONES
                parametros.setPathTbkComision( CommonsUtils.getCodiDato(paramDTOList, ConstantesPRTS.PATH_TBK_COM.toString() ) );
                
                // FORMATO DE ARCHIVO COMISIONES
                parametros.setFormatFileTbkComision( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.FORMAT_FILE_TBK_COM.toString() ) );
                
                // FORMATO DE EXTENSION DE ARCHIVO COMISIONES
                parametros.setFormatExtNameTbkComision( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.FORMAT_EXTNAME_TBK_COM.toString() ) );
                
                // FORMATO DE ARCHIVO DE CONTROL DE COMISIONES
                parametros.setFormatFileControlTbkComision( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.FORMAT_FILE_TBK_COM_CTR.toString() ) );
                
                // FORMATO DE EXTENSION DE ARCHIVO DE CONTROL DE COMISIONES
                parametros.setFormatExtNameControlTbkComision( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.FORMAT_EXTNAME_TBK_COM_CTR.toString() ) );
                parametros.setDataFTP( dataFTP );
                
            }
            LOG.info( "Parametros  BD para el proceso de DownloadComisiones desde TBK: " + parametros );
            return parametros;
        }
        catch ( Exception e ) {
            throw new AppException( MsgErrorSQL.ERROR_QUERY_PRTS.toString(), e );
        }
        finally {
            try {
                if ( commonsDAO != null ) {
                    commonsDAO.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException(
                        MsgErrorSQL.ERROR_QUERY_PRTS_CLOSE.toString(), e );
            }
        }
    }
	
	

  
	
	
}
