package com.sbpay.sgi.cron.job;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;

import com.sbpay.sgi.cron.step.StepGenerateContable;
import com.sbpay.sgi.cron.step.StepUploadAsientosCntbl;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 7/1/2016, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * JOB ENCARGADO DE LA CONTABILIDAD
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class FourJob implements Job {

	/** LOGGER CONSTANT **/
	private static final Logger LOG = Logger.getLogger(FourJob.class);

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 7/1/2016, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @param context
	 * @since 1.Xs
	 */

	@Override
	public void execute(JobExecutionContext context) {

		LOG.info("========== EJECUCION DEL CUARTO JOB =============");
		generarContable();
		subirAsientosContables();
		LOG.info("========== FIN EJECUCION DEL CUARTO JOB =============");

	}
	
	

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 7/1/2016, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @throws Exception
	 * @since 1.X
	 */
	public void generarContable() {
		try {
			StepGenerateContable stepGenerateContable = new StepGenerateContable();
			stepGenerateContable.execute();

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
	
	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 7/1/2016, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @throws Exception
	 * @since 1.X
	 */
	public void subirAsientosContables() {
		try {
			// LLAMA A LA TERCERA TAREA
			StepUploadAsientosCntbl uploadContable = new StepUploadAsientosCntbl();
			uploadContable.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

	}
	
	
}
