package com.sbpay.sgi.cron.dao;

import java.sql.SQLException;

import com.sbpay.sgi.cron.dao.BaseDaoFactory;
import com.sbpay.sgi.cron.ds.ConnectionProvider;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 19/11/2015, (ACL) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SA.</B>
 */
public class ProcesarIncomingDaoFactory extends
        BaseDaoFactory<ProcesarIncomingDao> {
    
    /** The single instance. */
    private static ProcesarIncomingDaoFactory singleINSTANCE = null;
    
    /**
     * Creates the instance.
     * 
     * @throws SQLException
     */
    private static void createInstance() throws SQLException {
        synchronized ( ProcesarIncomingDaoFactory.class ) {
            if ( singleINSTANCE == null ) {
                singleINSTANCE = new ProcesarIncomingDaoFactory();
            }
        }
    }
    
    /**
     * Patron singleton.
     * 
     * @return ProcesarIncomingDaoFactory retorna instancia del
     *         servicio.
     * @throws SQLException
     */
    public static ProcesarIncomingDaoFactory getInstance() throws SQLException {
        if ( singleINSTANCE == null ) {
            createInstance();
        }
        return singleINSTANCE;
    }
    
    /**
     * Metodo de restricion de sobreescritura de la clase.
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        super.clone();
        throw new CloneNotSupportedException();
    }
    
    /**
     * Constructor default.
     * 
     * @throws SQLException
     *             Error de SQL.
     */
    private ProcesarIncomingDaoFactory() throws SQLException {
        super();
    }
    
    /**
     * Metodo obtiene la instancia mas la conexion.
     */
    @Override
    public ProcesarIncomingDao getNewEntidadDao() throws SQLException {
        return new ProcesarIncomigJdbc( ConnectionProvider.getInstance(
                 ).getConnection() );
    }
    
}
