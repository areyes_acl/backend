package com.sbpay.sgi.cron.dao;

import java.sql.SQLException;

import com.sbpay.sgi.cron.dao.BaseDaoFactory;
import com.sbpay.sgi.cron.ds.ConnectionProvider;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 17/04/2019, (ACL) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SA.</B>
 */
public class ProcesarIncomingVisaNacDaoFactory extends
        BaseDaoFactory<ProcesarIncomingVisaNacDao> {
    
    /** The single instance. */
    private static ProcesarIncomingVisaNacDaoFactory singleINSTANCE = null;
    
    /**
     * Creates the instance.
     * 
     * @throws SQLException
     */
    private static void createInstance() throws SQLException {
        synchronized ( ProcesarIncomingVisaNacDaoFactory.class ) {
            if ( singleINSTANCE == null ) {
                singleINSTANCE = new ProcesarIncomingVisaNacDaoFactory();
            }
        }
    }
    
    /**
     * Patron singleton.
     * 
     * @return ProcesarIncomingVisaNacDaoFactory retorna instancia del
     *         servicio.
     * @throws SQLException
     */
    public static ProcesarIncomingVisaNacDaoFactory getInstance() throws SQLException {
        if ( singleINSTANCE == null ) {
            createInstance();
        }
        return singleINSTANCE;
    }
    
    /**
     * Metodo de restricion de sobreescritura de la clase.
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        super.clone();
        throw new CloneNotSupportedException();
    }
    
    /**
     * Constructor default.
     * 
     * @throws SQLException
     *             Error de SQL.
     */
    private ProcesarIncomingVisaNacDaoFactory() throws SQLException {
        super();
    }
    
    
    /**
     * Metodo obtiene la instancia mas la conexion.
     */
    @Override
    public ProcesarIncomingVisaNacDao getNewEntidadDao() throws SQLException {
        return new ProcesarIncomigVisaNacJdbc( ConnectionProvider.getInstance(
                 ).getConnection() );
    }
    
}
