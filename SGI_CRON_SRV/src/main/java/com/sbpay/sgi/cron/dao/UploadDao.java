package com.sbpay.sgi.cron.dao;

import java.sql.SQLException;

import com.sbpay.sgi.cron.dao.Dao;
import com.sbpay.sgi.cron.dto.CronLogDTO;
import com.sbpay.sgi.cron.dto.TblCronIcOutDTO;

public interface UploadDao extends Dao {

	CronLogDTO getCronLogByDateProcess(final String dateFile) throws SQLException;

	boolean updateCronLong(final CronLogDTO dateFile) throws SQLException;

	/**
	 * 
	 * @param dateFile
	 * @return
	 * @throws SQLException
	 */
	TblCronIcOutDTO getCronLogIcByDateProcess(final String dateFile)
			throws SQLException;

	boolean updateCronIcOut(final TblCronIcOutDTO dto) throws SQLException;




}
