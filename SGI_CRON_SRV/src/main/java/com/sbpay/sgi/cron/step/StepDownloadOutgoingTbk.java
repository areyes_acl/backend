package com.sbpay.sgi.cron.step;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.enums.MsgExitoMail;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.srv.download.DownLoadOutgoingTbkSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 7/1/2016, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class StepDownloadOutgoingTbk {
    /** The Constant LOGGER. */
    private static final Logger LOG = Logger
            .getLogger( StepDownloadOutgoingTbk.class );
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 7/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws Exception
     * @since 1.X
     */
    public void execute() throws Exception {
        
        try {
            LOG.info( "=====> Inicio Proceso:  Descargar outgoing desde SFTP Transbank <======" );
            if ( DownLoadOutgoingTbkSrv.getInstance().descargarOutgoing() ) {
                LOG.info( "Envio Correo de Descarga Satisfactoria: Descargar outgoing desde SFTP Transbank" );
                MailSendSrv
                        .getInstance()
                        .sendMailOk(
                                MsgExitoMail.EXITO_PROCESS_DOWNLOAD_TITLE
                                        .toString(),
                                MsgExitoMail.EXITO_PROCESS_DOWNLOAD_OUTGOING
                                        .toString() );
                LOG.info( " ======> Finaliza Proceso :DownLoad SFTP Transbank, no se ejecuta correctamente ======" );
            }
            else {
                LOG.info( " ======> Finaliza Proceso :DownLoad Outgoing desde SFTP Transbank, con observaciones. ======" );
            }
        }
        catch ( AppException e ) {
            LOG.error(e.getMessage(), e);
            MailSendSrv.getInstance().sendMail(
                    MsgProceso.MSG_GENERIC_ERROR_MSJ.toString(),
                    (e.getStackTraceDescripcion() != null) ? e
                        .getStackTraceDescripcion() : e.getMessage());
        }
        
    }
    
}
