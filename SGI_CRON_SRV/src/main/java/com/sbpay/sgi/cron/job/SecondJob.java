package com.sbpay.sgi.cron.job;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;

import com.sbpay.sgi.cron.step.StepDownloadAvances;
import com.sbpay.sgi.cron.step.StepDownloadAvancesBice;
import com.sbpay.sgi.cron.step.StepDownloadLogAutorizacion;
import com.sbpay.sgi.cron.step.StepDownloadOnus;
import com.sbpay.sgi.cron.step.StepDownloadRechazos;
import com.sbpay.sgi.cron.step.StepGenerateItzAvance;
import com.sbpay.sgi.cron.step.StepProcessAvances;
import com.sbpay.sgi.cron.step.StepProcessBice;
import com.sbpay.sgi.cron.step.StepProcessCPagos;
import com.sbpay.sgi.cron.step.StepProcessComisiones;
import com.sbpay.sgi.cron.step.StepProcessLogAutorizacion;
import com.sbpay.sgi.cron.step.StepProcessOnus;
import com.sbpay.sgi.cron.step.StepProcessOutgoing;
import com.sbpay.sgi.cron.step.StepProcessOutgoingVisaInt;
import com.sbpay.sgi.cron.step.StepProcessOutgoingVisaNac;
import com.sbpay.sgi.cron.step.StepRechazosTbk;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 7/1/2016, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class SecondJob implements Job {

	/** LOGGER CONSTANT **/
	private static final Logger LOG = Logger.getLogger(SecondJob.class);

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 7/1/2016, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @param context
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
	 * @since 1.X
	 */
	@Override
	public void execute(JobExecutionContext context) {
		LOG.info("========== EJECUCION DEL SEGUNDO JOB =============");
		
		descargarRechazos();
		descargarLogAutorizacion();
		procesarLogAutorizacion();
		procesarOutgoingTransbank();
		procesarOutgoingVisaNac();
		procesarOutgoingVisaInt();
 
		//generar interfaz de canal/avances
		generarItzAvances();
		// TAREA NUEVAS
		descargarBice();
		descargarAvances();
		descargarOnUs();
		procesarBice();
		procesarAvances();
		
		procesarOnUs();
		//FIN TAREAS NUEVAS

		procesarRechazosTransbank();
		procesarComisiones();
		procesarCPagos();

		LOG.info("========== FIN EJECUCION DEL SEGUNDO JOB =============");

	}
	
	
	/**
     * 
     */
	public void procesarLogAutorizacion() {
		try {
			// SE LLAMA A LA TAREA PROCESAR OUTGOING VISA INTERNACIONAL
			StepProcessLogAutorizacion processLog = new StepProcessLogAutorizacion();
			processLog.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

	}
	
	public void procesarCPagos() {
		try {
			// LLAMA A PROCESAR COMPENSACION DE PAGOS
			StepProcessCPagos process = new StepProcessCPagos();
			process.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

	}
	
	
	public void procesarComisiones() {
		try {
			// LLAMA A PROCESAR COMISIONES
			StepProcessComisiones process = new StepProcessComisiones();
			process.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

	}
	

	public void procesarOnUs() {
		try {
			// LLAMA A PROCESAR ON US
			StepProcessOnus process = new StepProcessOnus();
			process.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

	}

	
	public void descargarLogAutorizacion() {
		try {
			// LLAMA A DESCARGA LOG AUTORIZACION
			StepDownloadLogAutorizacion download = new StepDownloadLogAutorizacion();
			download.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

	}
	
	
	public void descargarOnUs() {
			try {
				// LLAMA A DESCARGA ON US
				StepDownloadOnus downloadOnUs = new StepDownloadOnus();
				downloadOnUs.execute();
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}
	
		}
	

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 7/1/2016, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @since 1.X
	 */
	public void procesarRechazosTransbank() {

		try {
			// LLAMA A LA TERCERA TAREA
			StepRechazosTbk rechazosTbk = new StepRechazosTbk();
			rechazosTbk.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
	
	

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 7/1/2016, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @since 1.X
	 */
	public void procesarOutgoingTransbank() {

		try {
			// LLAMA A LA SEGUNDA TAREA
			StepProcessOutgoing processOutg = new StepProcessOutgoing();
			processOutg.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
	

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 7/1/2016, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @since 1.X
	 */
	public void descargarRechazos() {

		try {
			// LLAMA A LA PRIMERA TAREA
			StepDownloadRechazos downloadRch = new StepDownloadRechazos();
			downloadRch.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
	
	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 DD/10/2018, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @since 1.X
	 */
	public void descargarBice() {
		try {
			LOG.info("========== DESCARGA BICE =========");
			StepDownloadAvancesBice stepDownload = new StepDownloadAvancesBice();
			stepDownload.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
	
	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 DD/10/2018, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @since 1.X
	 */
	public void descargarAvances() {
		try {
			LOG.info("========== DESCARGA AVANCES =========");
			StepDownloadAvances stepDownload = new StepDownloadAvances();
			stepDownload.execute();

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 DD/10/2018, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @since 1.X
	 */
	public void procesarBice() {
		try {
			LOG.info("========== PROCESAR BICE =========");
			StepProcessBice stepDownload = new StepProcessBice();
			stepDownload.execute();

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}

	
	/**
	 * 
	 * NO, I WONT'T GIVE UP, NO I WON'T GIVE IN
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 DD/10/2018, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @since 1.X
	 */
	public void procesarAvances() {
		try {
			LOG.info("========== PROCESAR AVANCES =========");
			StepProcessAvances stepProcessAvances = new StepProcessAvances();
			stepProcessAvances.execute();

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
	
	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/12/2018, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @since 1.X
	 */
	public void generarItzAvances() {
		try {
			// LLAMA A PROCESAR
			StepGenerateItzAvance process = new StepGenerateItzAvance();
			process.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

	}
	
	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 16/04/2019, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @since 1.X
	 */
	public void procesarOutgoingVisaNac() {

		try {
			// LLAMA A LA SEGUNDA TAREA
			StepProcessOutgoingVisaNac processOutg = new StepProcessOutgoingVisaNac();
			processOutg.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
	
	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 25/04/2019, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @since 1.X
	 */
	public void procesarOutgoingVisaInt() {

		try {
			// LLAMA A LA SEGUNDA TAREA
			StepProcessOutgoingVisaInt processOutg = new StepProcessOutgoingVisaInt();
			processOutg.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
	

}
