package com.sbpay.sgi.cron.srv.export;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dao.GenerarIncomingOnUsDAO;
import com.sbpay.sgi.cron.dao.GenerarIncomingOnUsDaoFactory;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.dto.ParamsGenerateIncomingOnUsDTO;
import com.sbpay.sgi.cron.dto.TblCronLogDTO;
import com.sbpay.sgi.cron.dto.TransaccionOnUsDTO;
import com.sbpay.sgi.cron.enums.ConstantesPRTS;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.DateFormatType;
import com.sbpay.sgi.cron.enums.LogBD;
import com.sbpay.sgi.cron.enums.MsgErrorMail;
import com.sbpay.sgi.cron.enums.MsgErrorSQL;
import com.sbpay.sgi.cron.enums.MsgGenerationIncomingOnUs;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.enums.StatusProcessType;
import com.sbpay.sgi.cron.srv.utils.CommonSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;
import com.sbpay.sgi.cron.utils.file.TransaccionOnUsExport;
import com.sbpay.sgi.cron.utils.file.TransaccionOnUsFileUtil;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;
import com.sbpay.sgi.cron.utils.generals.DateUtils;
import com.sbpay.sgi.cron.validation.ProcessValidator;
import com.sbpay.sgi.cron.validation.impl.ProcessValidatorImpl;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 28/01/2016, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * Clase procesa el On Us Outgoing y genera el archivo On Us Outgoing.
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public final class GenerarIncomingOnUsSrv extends CommonSrv {
	/** The Constant LOGGER. */
	private static final Logger LOG = Logger
			.getLogger(GenerarIncomingOnUsSrv.class.getName());
	/** The single instance. */
	private static GenerarIncomingOnUsSrv singleINSTANCE = null;
	
	/** */
	private final ProcessCodeType PROCESO_ACTUAL = ProcessCodeType.GENERAR_INCOMING_ONUS;

	/**
	 * Creates the instance.
	 */
	private static void createInstance() {
		synchronized (GenerarIncomingOnUsSrv.class) {
			if (singleINSTANCE == null) {
				singleINSTANCE = new GenerarIncomingOnUsSrv();
			}
		}
	}

	/**
	 * Patron singleton.
	 * 
	 * @return GenerarIncomingOnUsSrv retorna instancia del servicio.
	 */
	public static GenerarIncomingOnUsSrv getInstance() {
		if (singleINSTANCE == null) {
			createInstance();
		}
		return singleINSTANCE;
	}

	/**
	 * Metodo de restricion de sobreescritura de la clase.
	 */
	@Override
	protected Object clone() throws CloneNotSupportedException {
		super.clone();
		throw new CloneNotSupportedException();
	}

	/**
	 * Constructor Privado.
	 */
	private GenerarIncomingOnUsSrv() {
		super();
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
	 * </ul>
	 * <p>
	 * 
	 * @return
	 * @throws AppException
	 * @since 1.X
	 */
	public boolean generarIncomingOnUsTbk() throws AppException {
		GenerarIncomingOnUsDAO generarIncomingOnUsDAO = null;
		boolean flag = Boolean.FALSE;
		LOG.info("=========INICIA PROCESO DE GENERACION DE INCOMING ON US ============");
		try {
		    ProcessValidator validadorProceso = new ProcessValidatorImpl();

		    if (!validadorProceso.isActiveProcess(PROCESO_ACTUAL)) {
		        LOG.info("Proceso :"+PROCESO_ACTUAL.getCodigoProceso()+", no se encuentra activo.");
		        String asunto = MsgProceso.MSG_PROCESO_DESACTIVADO_ASUNTO
	                    .toString().replace( ":?:",
	                            PROCESO_ACTUAL.getCodigoProceso() );
	            String mensaje = MsgProceso.MSG_PROCESO_DESACTIVADO_BODY.toString()
	                    .replace( ":?:", PROCESO_ACTUAL.getCodigoProceso() );
	            MailSendSrv.getInstance().sendMail( asunto, mensaje );
	            return flag;
		    }
		    
			
			// FORMA LOS NOMBRES DE LOS ARCHIVOS
			ParamsGenerateIncomingOnUsDTO parametros = getParametros();
			if (validadorProceso.hasTheValidParametersForTheProcess( PROCESO_ACTUAL, parametros )) {
			    
			    generarIncomingOnUsDAO = GenerarIncomingOnUsDaoFactory
	                    .getInstance().getNewEntidadDao();

				// LLAMA AL SP_CARGA_OUTGOING_ONUS_TRANSBANK
				if (generarIncomingOnUsDAO.callSPCargaOutgoingOnUsTransbank()) {
					LOG.info("SE HA LLAMADO AL PROCEDIMIENTO CORRECTAMENTE");
				}
		        // OBTIENE LA LISTA DE TRANSACCIONES DESDE BD
	            List<TransaccionOnUsDTO> listaTrasacciones = generarIncomingOnUsDAO
	                    .loadTransactions();
	            
	            // ESCRIBE EN LOG ERROR NO SE ENCONTRARON RESULTADOS
	            if ( listaTrasacciones != null && listaTrasacciones.size() == 0 ) {
	                LOG.warn( MsgGenerationIncomingOnUs.WARNING_TRANSACTIONS_NOT_FOUND
	                        .toString() );
	            }

	            String incomingFilename = generateIncomingOnUsFilename( parametros );
	            String controlFilename = generateControlOnUsFilename( parametros );
	            String ruta = parametros.getPathAbcOnUsOut();
	            // GENERA ARCHIVO INCOMING Y ARCHIVO DE CONTROL CON LAS
	            // TRANSACCIONES ENCONTRADAS
	            TransaccionOnUsFileUtil incomingOut = new TransaccionOnUsExport();
	            incomingOut.exportIncomingOnUsFile( ruta, incomingFilename,
	                    controlFilename, listaTrasacciones );
	            
	            // BORRA TABLA XXXXXXXX UNA VEZ QUE EL
	            // PROCESO SE EJECUTA CORRECTAMENTE
	            generarIncomingOnUsDAO.clearTemporalOutgoingOnUsTransbank();

				// SE DEJA REGISTRO EN LA TABLA TBL_LOG_OUT_ONUS
				saveLog(incomingFilename);
				flag = true;
			} else {
				LOG.info("Parametros de Base de Datos Incompletos");
				MailSendSrv.getInstance().sendMail(
						MsgErrorMail.ALERTA_PRTS.toString(),
						MsgErrorMail.ALERTA_PRTS_TXT.toString());
			}
		} catch (Exception e) {
		    e.printStackTrace();
			throw new AppException(
					MsgGenerationIncomingOnUs.ERROR_SQL_ON_US_OUT_TBK
							.toString(),
					e);
		} finally {
			try {
				if (generarIncomingOnUsDAO != null) {
					generarIncomingOnUsDAO.close();
				}
			} catch (SQLException e) {
				throw new AppException(
						MsgErrorSQL.ERROR_DAO_ONUS_CLOSE.toString(), e);
			}
		}
		return flag;
	}

	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
	 * </ul>
	 * <p>
	 * 
	 * @param incomingFilename
	 * @throws SQLException
	 * @since 1.X
	 */
	private void saveLog(String incomingFilename) throws SQLException {
		CommonsDao commonsDAO = null;
		
		TblCronLogDTO log = new TblCronLogDTO();
		log.setFilename(incomingFilename);
		log.setFileFlag(StatusProcessType.PROCESS_PENDING.getValue());
		log.setFecha(DateUtils.parseFormat(DateUtils.getTodayInYYMMDDFormat(),
				DateFormatType.FORMAT_YYMMDD,
				DateFormatType.FORMAT_DDMMYY_WITH_SLASH_SEPARATOR));
		try {
			commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
			commonsDAO.saveTblCronLog(log, LogBD.TABLE_LOG_OUTGOING_ONUS);
			commonsDAO.endTx();
		} catch (SQLException e) {
			try {
				if (commonsDAO != null) {
					commonsDAO.rollBack();
				} else {
					LOG.info("No se ha ejecutado rollback");
				}
			} catch (SQLException e1) {
				throw new SQLException(e.getMessage(), e1);
			}
		} finally {
			if (commonsDAO != null) {
				commonsDAO.close();
			}
		}
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
	 * </ul>
	 * <p>
	 * 
	 * @param parametros
	 * @return
	 * @since 1.X
	 */
	private String generateControlOnUsFilename(
			final ParamsGenerateIncomingOnUsDTO parametros) {
		String date = DateUtils.getDateTodayInSpecialFormat();
		StringBuilder str = new StringBuilder();
		str.append(parametros.getFormatFilenameIncOnUs()).append(date)
				.append(ConstantesUtil.POINT)
				.append(parametros.getFormatExtNameIncCrtOnUs());
		return str.toString();
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
	 * </ul>
	 * <p>
	 * 
	 * @param parametros
	 * @return
	 * @since 1.X
	 */
	private String generateIncomingOnUsFilename(
			final ParamsGenerateIncomingOnUsDTO parametros) {
		String date = DateUtils.getDateTodayInSpecialFormat();
		StringBuilder str = new StringBuilder();
		str.append(parametros.getFormatFilenameIncOnUs()).append(date)
				.append(ConstantesUtil.POINT)
				.append(parametros.getFormatExtNameIncOnUs());
		return str.toString();
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
	 * </ul>
	 * <p>
	 * 
	 * @return
	 * @throws SQLException
	 * @since 1.X
	 */
  private ParamsGenerateIncomingOnUsDTO getParametros() throws SQLException {
    CommonsDao daoCommon = null;
    List<ParametroDTO> paramDTOList = null;
    ParamsGenerateIncomingOnUsDTO paramDto = null;

    try {

      daoCommon = CommonsDaoFactory.getInstance().getNewEntidadDao();
      paramDTOList = daoCommon.getParametroCodGrupoDato(ConstantesPRTS.CRON_TBK.toString());
      if (paramDTOList != null && paramDTOList.size() > 0) {
        // PARAMETROS DE PRTRS
        paramDto = new ParamsGenerateIncomingOnUsDTO();

        // RUTA DE FOLDER DONDE SE DEJARA EL ARCHIVO INCOMING ON US GENERADO
        paramDto.setPathAbcOnUsOut(CommonsUtils.getCodiDato(paramDTOList,
            ConstantesPRTS.PATH_ABC_ONUS_OUT.toString()));

        // FORMATO DE EXTENSION DE ARCHIVO INCOMING ONUS
        paramDto.setFormatExtNameIncOnUs(CommonsUtils.getCodiDato(paramDTOList,
            ConstantesPRTS.FORMAT_EXTNAME_ONUS_OUT.toString()));

        // FORMATO DE EXTENSION DE ARCHIVO DE CONTROL ONUSS
        paramDto.setFormatExtNameIncCrtOnUs(CommonsUtils.getCodiDato(paramDTOList,
            ConstantesPRTS.FORMAT_EXTNAME_ONUS_OUT_CTR.toString()));

        // FORMATO DE STARWITH DEL NOMBRE DEL ARCHIVO INCOMING ON US
        paramDto.setFormatFilenameIncOnUs(CommonsUtils.getCodiDato(paramDTOList,
            ConstantesPRTS.FORMAT_FILENAME_ONUS_OUT.toString()));

      }
      LOG.info("CARGO PARAMETROS GENERAR/EXPORTAR INCOMING ON US");
      return paramDto;
    } finally {
      if (daoCommon != null) {
        daoCommon.close();
      }
    }
  }
  
  
  
}
