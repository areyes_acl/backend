package com.sbpay.sgi.cron.srv.process;

import java.io.File;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dao.ProcesarIncomingDao;
import com.sbpay.sgi.cron.dao.ProcesarIncomingDaoFactory;
import com.sbpay.sgi.cron.dto.IncomingDTO;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.dto.ParamsFileReaderProcessDTO;
import com.sbpay.sgi.cron.dto.TblCronLogDTO;
import com.sbpay.sgi.cron.dto.Transaccion;
import com.sbpay.sgi.cron.dto.TransaccionOutVisaCobroCargo;
import com.sbpay.sgi.cron.enums.ConstantesOperador;
import com.sbpay.sgi.cron.enums.ConstantesPRTS;
import com.sbpay.sgi.cron.enums.FilterTypeSearch;
import com.sbpay.sgi.cron.enums.LogBD;
import com.sbpay.sgi.cron.enums.MsgErrorMail;
import com.sbpay.sgi.cron.enums.MsgErrorProcessFile;
import com.sbpay.sgi.cron.enums.MsgErrorSQL;
import com.sbpay.sgi.cron.enums.MsgExitoMail;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.enums.StatusProcessType;
import com.sbpay.sgi.cron.srv.utils.CommonOutgoingSrv;
import com.sbpay.sgi.cron.srv.utils.CommonSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;
import com.sbpay.sgi.cron.utils.file.IncomingFileUtils;
import com.sbpay.sgi.cron.utils.file.IncomingReader;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;
import com.sbpay.sgi.cron.validation.ProcessValidator;
import com.sbpay.sgi.cron.validation.impl.ProcessValidatorImpl;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 16/11/2015, (ACL) - versión inicial
 * </ul>
 * <p>
 * 
 * @author afreire
 *         <p>
 *         <B>Todos los derechos reservados por ACL SA.</B>
 */
public class ProcesarOutgoingSrv extends CommonSrv {
    
    /** VARIABLE PARA EL LOGER */
    private static final Logger LOGGER = Logger
            .getLogger( ProcesarOutgoingSrv.class );
   
    private final ProcessCodeType PROCESO_ACTUAL = ProcessCodeType.PROCESAR_OUTGOING;

    
    /** VARIABLE PARA UTILIZAR AL GUARDAR TRANSACCION */
    private String rutsbpay;
    
    /** The single instance. */
    private static ProcesarOutgoingSrv singleINSTANCE = null;
    
    /**
     * Creates the instance.
     */
    private static void createInstance() {
        synchronized ( ProcesarOutgoingSrv.class ) {
            if ( singleINSTANCE == null ) {
                singleINSTANCE = new ProcesarOutgoingSrv();
            }
        }
    }
    
    /**
     * Patron singleton.
     * 
     * @return ActivarPrepagoSrv retorna instancia del servicio.
     */
    public static ProcesarOutgoingSrv getInstance() {
        if ( singleINSTANCE == null ) {
            createInstance();
        }
        return singleINSTANCE;
    }
    
    /**
     * Metodo de restricion de sobreescritura de la clase.
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        super.clone();
        throw new CloneNotSupportedException();
    }
    
    /**
     * Constructor Privado.
     */
    private ProcesarOutgoingSrv() {
        super();
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2015, (Everis Chile) - versión inicial
     * </ul>
     * <p>
     * Metodo principal que contrala toda la lógica del procesamiento
     * de un incoming
     * 
     * @throws AppException
     * @throws SQLException 
     * 
     * 
     * @since 1.X
     */
    public void procesarArchivo() throws AppException {
        
        ProcessValidator validadorProceso = new ProcessValidatorImpl();
        
        if (validadorProceso.isActiveProcess(PROCESO_ACTUAL)) {
        /*
         * OBTIENE LOS PARAMETROS UTILIZADOS PARA PROCESAR EL ARCHIVO
         * pathAclInc Y formatFileNameInc
         */
        ParamsFileReaderProcessDTO params = getParametros();
        Integer countFileProcessed = 0;
        List<File> listOfFiles = null;
        
        
        if(validadorProceso.hasTheValidParametersForTheProcess( PROCESO_ACTUAL, params )){
        	LOGGER.debug("==> SE HAN ENCONTRADO TODOS LOS PARAMETROS, SE INICIA PROCESO <==");
        	 rutsbpay = params.getRutsbpay();
        /*
         * RECORRE LOS ARCHIVOS DEL FOLDER BUSCANDO ARCHIVOS INCOMING
         * SE VALIDA SI EL INCOMING TIENE REGISTRO EN TBL_LOG_INC Y SI
         * ESTA O NO PROCESADO (FLAG 1 : PROCESADO, FLAG -1: ERROR,
         * FLAG : 0 )
         */
        listOfFiles = CommonsUtils.getFilesInFolder( params.getPathAclInc(),
                params.getFormatFilenameOut(), params.getFormatExtNameOut() );
        
        LOGGER.debug( "Lista de archivos :  "
                + ( ( listOfFiles != null ) ? "" + listOfFiles.size()
                        : "No se encontraron archivos" ) );
        
        // SI NO EXISTE NINGUN ARCHIVO EN LA CARPETA TERMINA EL PROCESO
        if ( listOfFiles == null ) {
            LOGGER.warn( MsgErrorProcessFile.WARNING_NO_FILES_FOUNDS.toString() );
            return;
        }
        
        for ( File file : listOfFiles ) {
            if ( file.isFile()
                    && file.getName()
                            .startsWith( params.getFormatFilenameOut() )
                    && esValidoIncoming( file.getName() ) ) {
                try {
                    LOGGER.info( "Archivo Outgoing válido : Se comienza a procesar..." );
                    // 1- PROCESA ARCHIVO INCOMING
                    IncomingFileUtils reader = new IncomingReader();
                    IncomingDTO incoming = reader.readIncomingFile( file,
                            params.getFormatFilenameOut() );
                    
                    // 2 - SE GUARDAN LAS TRANSACCIONES DE UN INCOMING
                    // EN BD
                    saveData( incoming );
                    
                    // 3 - SE MUEVE INCOMING A LA CARPETA BACKUP
                    CommonsUtils.moveFile( file.getAbsolutePath(),
                            params.getPathAclIncBkp() + file.getName() );
                    countFileProcessed++;
                    
                    // SE UPDATEA EL FLAG A PROCESADO OK
                    String filename = file.getName();
                    TblCronLogDTO tblCronLogDTO = new TblCronLogDTO();
                    tblCronLogDTO.setFilename( file.getName() );
                    tblCronLogDTO.setFileFlag( StatusProcessType.PROCESS_SUCESSFUL.getValue() );
                    try{
                     LOGGER.info("actualizando el log de procesamiento");
            	    CommonsDao commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
                    if (commonsDAO.updateTblCronLogByType(tblCronLogDTO, LogBD.TABLE_LOG_INCOMING.getTable(), FilterTypeSearch.FILTER_BY_NAME)) {
                    	LOGGER.info("SE ACTUALIZA CORRECTAMENTE TABLA "+LogBD.TABLE_LOG_INCOMING.getTable()+" PARA FILE  :" + filename);
                          commonsDAO.endTx();
                        } else {
                        	LOGGER.warn("NO SE ACTUALIZA TABLA "+LogBD.TABLE_LOG_INCOMING.getTable()+" PARA FILE  :" + filename);
                          commonsDAO.rollBack();
                        }
            		}catch(SQLException s){
            			LOGGER.error(s);
            		}
                }
                catch ( AppException e ) {
                	CommonOutgoingSrv.errorProcessOutgoing(file, e, MsgErrorProcessFile.ERROR_IN_PROCESS_OUTGOING.toString(), params.getPatchAclIncError(), LogBD.TABLE_LOG_INCOMING.getTable());
                }
            }
        }
        
        LOGGER.info("======== TOTAL DE ARCHIVOS PROCESADOS  =  "+countFileProcessed+  "  ==========");
        String msj = "";
        
        // SI NO SE PROCESA NINGUN FILE ESCRIBE ESCRIBE LOG
        if ( countFileProcessed == 0 ) {
            LOGGER.warn( MsgErrorProcessFile.WARNING_NO_FILE_PROCESSING_TITLE
                    + "  - "
                    + MsgErrorProcessFile.WARNING_NO_FILE_PROCESSING_CAUSE );
            msj  = " Sin embargo" + MsgErrorProcessFile.WARNING_NO_FILES_FOUNDS.toString() ;
        }
        
        // SI SE PROCESARON TODOS LOS ARCHIVOS EXISTENTES SE ENVIA MAIL CORRECTO
        if ( listOfFiles.size() == countFileProcessed ) {
            LOGGER.info( "Envio Correo de Proceso  Satisfactorio: Procesamiento Archivo Outgoing Transbank " );
            MailSendSrv.getInstance().sendMailOk(
                    MsgExitoMail.EXITO_PROCESS_OUTGOING_TITLE.toString(),
                    MsgExitoMail.EXITO_PROCESS_OUTGOING_BODY.toString().concat("\n").concat(msj) );
        }
        
      }else{
    	  LOGGER.error("Parametros de Base de Datos Incompletos para el proceso : ProcessOutgoingTbk");
    				MailSendSrv.getInstance().sendMail(
    						MsgErrorMail.ALERTA_PRTS.toString(),
    						MsgErrorMail.ALERTA_PRTS_TXT.toString());
      }
            
        }
        else {
            String asunto = MsgProceso.MSG_PROCESO_DESACTIVADO_ASUNTO
                    .toString().replace( ":?:",
                            PROCESO_ACTUAL.getCodigoProceso() );
            String mensaje = MsgProceso.MSG_PROCESO_DESACTIVADO_BODY.toString()
                    .replace( ":?:", PROCESO_ACTUAL.getCodigoProceso() );
            MailSendSrv.getInstance().sendMail( asunto, mensaje );
        }
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 - 24/11/2015, (ACL) - versión inicial
     * </ul>
     * <p>
     * 
     * Metodo que valida si el incoming está o no ingresado en bd en
     * la tabla Cron_log si es asi se verifica su codigo si es 1- ya
     * se encuentra procesado si es -1 fallo en procesamientos
     * anteriores, y si es 0 se debe procesar.
     * 
     * @throws AppException
     * 
     * 
     * @since 1.X
     */
    private boolean esValidoIncoming( String filename ) throws AppException {
        CommonsDao commonsDAO = null;
        try {
            commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
            
            // BUSCA REGISTRO LOG POR NOMBRE
            LOGGER.info( "Nombre Incoming: " + filename );
            TblCronLogDTO filter = new TblCronLogDTO();
            filter.setFilename( filename );
            
            TblCronLogDTO cronlog = commonsDAO.getTblCronLogByType( filter,
                    LogBD.TABLE_LOG_INCOMING.getTable(),
                    FilterTypeSearch.FILTER_BY_NAME );
            LOGGER.info( "=====> Valor de TBL_LOG_INC: " + cronlog );
            
            if ( cronlog == null ) {
                LOGGER.error( MsgErrorProcessFile.ERROR_INCOMING_NAME_NOT_FOUND
                        .toString() );
                return Boolean.FALSE;
            }
            // SI ES DISTINTO DE 0 NO ES VÁLIDO
            else
                if ( !"0".equalsIgnoreCase( cronlog.getFileFlag() ) ) {
                    LOGGER.error( MsgErrorProcessFile.ERROR_INVALID_INCOMING_STATE
                            .toString() );
                    return Boolean.FALSE;
                }
            
        }
        catch ( SQLException e ) {
            throw new AppException(
                    MsgErrorSQL.ERROR_FIND_INCOMING_BY_NAME.toString(), e );
        }
        finally {
            
            try {
                if ( commonsDAO != null)  {
                    commonsDAO.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException(
                        MsgErrorSQL.ERROR_FIND_INCOMING_BY_NAME.toString(), e );
            }
        }
        
        return Boolean.TRUE;
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 - 16/11/2015, (ACL) - versión inicialfS
     * </ul>
     * <p>
     * Metodo encargado de recorrer las transacciones encontradas en
     * el incoming y enviarlas una a una al DAO para que sean
     * almacenadas
     * 
     * @throws AppException
     *
     * 
     * @since 1.X
     */
    private void saveData( IncomingDTO incoming ) throws AppException {
        Integer flag = 0;
        ProcesarIncomingDao procesarDAO = null;
        CommonsDao commonsDAO = null;
        TblCronLogDTO tblCronLogDTO = null;
        try {
            procesarDAO = ProcesarIncomingDaoFactory.getInstance()
                    .getNewEntidadDao();
            commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
            
            
            Integer sidOperador = commonsDAO.getOperador(ConstantesOperador.TBK.toString());
            
                    
                // RECORRE TODAS LAS TRANSACCIONES DEL INCOMING
                for ( Transaccion instance : incoming.getListaTransacciones() ) {
                    // ALMACENA RUT sbpay
                    instance.setRutsbpay( rutsbpay );
                    
                    // GUARDA EN TMP_INCOMING, SI HAY ERROR SUMA -1
                    if ( !procesarDAO.saveTransaction( instance, sidOperador)) {
                        flag = flag - 1;
                    }
                }
                
                for(TransaccionOutVisaCobroCargo instance : incoming.getListaCobroCargo()){
                	
                	LOGGER.info("Entro al listado de Cobro Cargo");
                	if( !procesarDAO.saveTransactionCargoAbono(instance, sidOperador)){
                		flag = flag - 1;
                	}
                	
                }

            // COMMMIT DE TRANSACCIONES SAVE ACUMULADAS
            if ( flag == 0 ) {
                procesarDAO.endTx();
                LOGGER.info( "COMMITEA SAVE TRANSACCIONES" );
                
                // UNA VEZ GUARDADAS LAS TRASSACCIONES SE ACTUALIZA INCOMING EN CRON LOG
                LOGGER.info( " UPDATE FLAG -  FILENAME : "+ incoming.getIncomingName() );
                tblCronLogDTO = new TblCronLogDTO();
                tblCronLogDTO.setFilename( incoming.getIncomingName() );
                tblCronLogDTO.setFileFlag( StatusProcessType.PROCESS_SUCESSFUL.getValue() );
                
                // SE REALIZA EL UPDATE
                if ( commonsDAO.updateTblCronLogByType( tblCronLogDTO,LogBD.TABLE_LOG_INCOMING.getTable(),FilterTypeSearch.FILTER_BY_NAME ) ) {
                    LOGGER.info( "COMMITEA UPDATE" );
                    commonsDAO.endTx();
                    // SE LLAMA AL SP CARGA INCOMING
                    procesarDAO.callSPCargaIncoming();
                    procesarDAO.callSPCobroCargo();
                }
                else {
                    // ROLLBACK CARGA UPDATE FLAG
                    rollBackTransaccion( commonsDAO );
                }
            }
            else {
                // SE REALIZA ROLLBACK
                rollBackTransaccion( procesarDAO );
                throw new AppException( MsgErrorSQL.ERROR_SAVE_INCOMING.toString());
            }
        }
        catch ( SQLException e ) {
            throw new AppException( MsgErrorSQL.ERROR_SAVE_INCOMING.toString(),e );
        }
        finally {
            try {
                if ( procesarDAO != null ) {
                    procesarDAO.close();
                }
                if ( commonsDAO != null ) {
                    commonsDAO.close();
                }
            }
            catch ( SQLException e ) {
                LOGGER.error(e.getMessage(), e);
            }
            
        }
        
    }
   
    
    
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 - 24/11/2015, (ACL) - versión inicial
     * </ul>
     * <p>
     * 
     * Metodo setea los parametros de sistema del cron transbank.
     * 
     * @return
     * @throws AppException
     * @since 1.X
     */
    private ParamsFileReaderProcessDTO getParametros() throws AppException {
        ParamsFileReaderProcessDTO parametros = null;
        List<ParametroDTO> paramDTOList = null;
        CommonsDao commonsDAO = null;
        try {
            commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
            // SE OBTIENEN TODOS LOS CODIGOS
            paramDTOList = commonsDAO
                    .getParametroCodGrupoDato( ConstantesPRTS.CRON_TBK
                            .toString() );
            
            // SI EXISTEN PARAMETROS EN BD SE EXTRAEN
            if ( paramDTOList != null ) {
                parametros = new ParamsFileReaderProcessDTO();
				// RUTA SERVIDOR DONDE SE DEJARA EL ARCHIVO OUTGOING DE TRANSBANK PROCESADOS BKP     
                parametros.setPathAclIncBkp( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.PATH_ACL_INC_BKP.toString() ) );
                // RUTA SERVIDOR DONDE SE DEJARA EL ARCHIVO OUTGOING DE TRANSBANK PROCESADO CON ERROR
                parametros.setPatchAclIncError( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.PATH_ACL_INC_ERROR.toString() ) );
                // RUTA SERVIDOR DONDE SE LEERA EL ARCHIVO OUTGOING DE TRANSBANK PARA PROCESAR
                parametros.setPathAclInc( CommonsUtils.getCodiDato(
                        paramDTOList, ConstantesPRTS.PATH_ACL_INC.toString() ) );
                // FORMATO DEL NOMBRE DEL ARCHIVO OUTGOING DE TRANSBANK                
                parametros.setFormatFilenameOut( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.FORMAT_FILENAME_OUT.toString() ) );
                // RUT ABC DIN
                parametros.setRutsbpay( CommonsUtils.getCodiDato(
                        paramDTOList, ConstantesPRTS.RUT_COM_VI.toString() ) );
                // FORMATO DE LA EXTENSION DEL ARCHIVO OUTGOING DE TRANSBANK
                parametros.setFormatExtNameOut( CommonsUtils.getCodiDato(
                        paramDTOList,
                        ConstantesPRTS.FORMAT_EXTNAME_OUT.toString() ) );
                
            }
            LOGGER.debug( "Parametros  BD : " + parametros );
            return parametros;
        }
        catch ( SQLException e ) {
            throw new AppException( MsgErrorSQL.ERROR_QUERY_PRTS.toString(), e );
        }
        finally {
            try {
                if ( commonsDAO != null ) {
                    commonsDAO.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException(
                        MsgErrorSQL.ERROR_QUERY_PRTS_CLOSE.toString(), e );
            }
        }
    }
    
}
