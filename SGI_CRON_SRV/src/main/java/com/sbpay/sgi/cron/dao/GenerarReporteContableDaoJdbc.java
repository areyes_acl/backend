package com.sbpay.sgi.cron.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;










import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.dao.BaseDao;
import com.sbpay.sgi.cron.dto.DetalleContableBean;
import com.sbpay.sgi.cron.dto.SolicitudReporteContableDTO;
import com.sbpay.sgi.cron.enums.ConstantesBD;

public class GenerarReporteContableDaoJdbc extends BaseDao<GenerarReporteContableDao> implements GenerarReporteContableDao {

	/** VARIABLE PARA EL LOGER */
	private static final Logger LOGGER = Logger.getLogger(GenerarReporteContableDaoJdbc.class);

	private static final String SP_SIG_DETALLE_CONTABLE = "{call SP_SIG_DETALLE_CONTABLE(?, ?, ?, ?, ?, ?,?, ?)}"; 
	static final String UPDATE_SOLICITUD_OK = "UPDATE TBL_EXPDC_SOL SET TOTAL_REGISTROS = ?, ID_ESTADO = (SELECT EST.SID FROM TBL_EXPDC_EST EST WHERE EST.NOM = 'OK') WHERE SID = ?";
	
	
	static final String SELECT_ALL_SOLICITUD_PENDIENTE = "SELECT SOL.SID, OPE.ID_OPERADOR, OPE.ID_PRODUCTO, SOL.TIPO_MOV, to_char(SOL.FECHA_INI, 'DD/MM/YY') AS FECHA_INI, to_char(SOL.FECHA_FIN, 'DD/MM/YY') AS FECHA_FIN, SOL.FICHERO FROM "+ConstantesBD.ESQUEMA.toString()+".TBL_EXPDC_SOL SOL INNER JOIN "+ConstantesBD.ESQUEMA.toString()+".TBL_EXPDC_EST EST ON (EST.SID = SOL.ID_ESTADO) INNER JOIN "+ConstantesBD.ESQUEMA.toString()+".TBL_OPERADOR_PRODUCTO OPE ON (OPE.SID = SOL.ID_OPE_PROD) WHERE EST.NOM = 'PEN' AND SOL.BLOCKED IS NULL";
	static final String UPDATE_ALL_SOLICITUD_PENDIENTE = "UPDATE "+ConstantesBD.ESQUEMA.toString()+".TBL_EXPDC_SOL SET BLOCKED = 1 WHERE ID_ESTADO = (SELECT EST.SID FROM "+ConstantesBD.ESQUEMA.toString()+".TBL_EXPDC_EST EST WHERE EST.NOM = 'PEN') AND BLOCKED IS NULL";

	public GenerarReporteContableDaoJdbc(Connection connection) {
		super(connection);
	}
	
	@Override
	public List<SolicitudReporteContableDTO> obtenerInformacionCnbltSolicitud() throws SQLException {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<SolicitudReporteContableDTO> transactionList = new ArrayList<SolicitudReporteContableDTO>();
		SolicitudReporteContableDTO tr = null;
		try {

			stmt = this.getConnection().prepareStatement(SELECT_ALL_SOLICITUD_PENDIENTE);
			LOGGER.info(SELECT_ALL_SOLICITUD_PENDIENTE);

			// SE EJECUTA CONSULTA
			rs = stmt.executeQuery();

			// LEE LAS COLUMNAS DE CADA REGISTRO
			while (rs.next()) {
				tr = parseTransactions(rs);
				transactionList.add(tr);
			}
			return transactionList;
		} finally {

			if (stmt != null) {
				stmt.close();
			}
			if (rs != null) {
				rs.close();
			}
		}
	}


	private SolicitudReporteContableDTO parseTransactions(final ResultSet rs) throws SQLException {
		SolicitudReporteContableDTO tr = new SolicitudReporteContableDTO();
        tr.setSid(rs.getInt("SID"));
        tr.setIdOperador(rs.getInt("ID_OPERADOR"));
        tr.setIdProducto(rs.getInt("ID_PRODUCTO"));
        tr.setTipoMov(rs.getString("TIPO_MOV"));
        tr.setFechaIni(rs.getString("FECHA_INI"));
        tr.setFechaFin(rs.getString("FECHA_FIN"));
        tr.setFichero(rs.getString("FICHERO"));
        return tr;
	}
	
	
	@Override
	public void updateDataSolicitudContableBloqueo() throws SQLException {
		PreparedStatement stmt = null;
		try {

			stmt = this.getConnection().prepareStatement(UPDATE_ALL_SOLICITUD_PENDIENTE);
			LOGGER.info(UPDATE_ALL_SOLICITUD_PENDIENTE);

			// SE EJECUTA CONSULTA
			stmt.executeUpdate();
			
		} finally {

			if (stmt != null) {
				stmt.close();
			}
		}
	}

	@Override
	public void updateDataSolicitudContableStateOK(Integer sidContable, Integer cantidad) throws SQLException {
		PreparedStatement stmt = null;
		try {

			stmt = this.getConnection().prepareStatement(UPDATE_SOLICITUD_OK);
			setIntNull(cantidad.toString(), 1, stmt);
			setIntNull(sidContable.toString(), 2, stmt);
			LOGGER.info(UPDATE_SOLICITUD_OK);

			// SE EJECUTA CONSULTA
			stmt.executeUpdate();
			
		} finally {

			if (stmt != null) {
				stmt.close();
			}
		}
	}

	@Override
	public List<DetalleContableBean> obtenerDetalleContable(String fechaInicio, String fechaTermino, String tipoMov, int operador, int producto) throws SQLException {
		CallableStatement callableStatement = null;
		List<DetalleContableBean> listaDetalle = null;
		ResultSet rs = null;
		LOGGER.info("===== Se ejecuta el procedimiento : SP_SIG_DETALLE_CONTABLE =====");
		try {
			callableStatement = this.getConnection().prepareCall( SP_SIG_DETALLE_CONTABLE);
			callableStatement.setString("fecha_inicio", fechaInicio);
			callableStatement.setString("fecha_termino", fechaTermino);
			callableStatement.setString("tipo_movimiento", tipoMov);
			callableStatement.setInt("filtro_operador", operador);
			callableStatement.setInt("producto_id", producto);
			callableStatement.registerOutParameter("warning", OracleTypes.VARCHAR);
		    callableStatement.registerOutParameter("cod_error", OracleTypes.NUMBER);
		    callableStatement.registerOutParameter("prfCursor", OracleTypes.CURSOR);

			
			callableStatement.execute();
			int codigoError = callableStatement.getInt("cod_error");
		    String warning = callableStatement.getString("warning");
		    rs = (ResultSet) ((callableStatement).getObject("prfCursor"));
		    
			LOGGER.info("WARNING : " + warning);
			LOGGER.info("codigoError : " + codigoError);
			rs = (ResultSet) ((callableStatement).getObject("prfCursor"));
			listaDetalle = new ArrayList<DetalleContableBean>();
			
			if (codigoError == 0 && rs != null ) {
				// BUSCA DATOS DEL CURSOR
				while (rs.next()) {
				    DetalleContableBean detalle = new DetalleContableBean();
				    detalle.setFechaContable(rs.getString("FECHA_CONTABLE"));
				    detalle.setTipoMovimiento(rs.getString("TIPO_MOV"));
				    detalle.setCuentaDebito(rs.getString("DEBE"));
				    detalle.setCuentaCredito(rs.getString("HABER"));
				    detalle.setMonto(rs.getString("MONTO_TRANSAC"));
				    detalle.setSubledger(rs.getString("SUBLEDGER"));
				    detalle.setNumeroTarjeta(rs.getString("NRO_TARJETA"));
				    detalle.setMicrofilm(rs.getString("MICROFILM"));
				    detalle.setTipoVenta(rs.getString("TIPO_VENTA"));
				    detalle.setCantidadCuotas(rs.getString("CUOTAS"));
				    detalle.setNombreArchivo(rs.getString("NOMBRE_ARCHIVO_CONTABLE"));

				    listaDetalle.add(detalle);
				}

			    } else {
				   throw new SQLException(warning);
			    }
			
		} finally {

			if (callableStatement != null) {
				callableStatement.close();
			}
			if (rs != null) {
				rs.close();
			}

		}
		return listaDetalle;
	}
	
	
}
