package com.sbpay.sgi.cron.srv.export;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dao.GenerarIncomingDAO;
import com.sbpay.sgi.cron.dao.GenerarIncomingDaoFactory;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.dto.ParamsGenerateIncomingVisaDTO;
import com.sbpay.sgi.cron.dto.TblCronLogDTO;
import com.sbpay.sgi.cron.dto.Transaccion;
import com.sbpay.sgi.cron.enums.ConstantesOperador;
import com.sbpay.sgi.cron.enums.ConstantesPRTS;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.DateFormatType;
import com.sbpay.sgi.cron.enums.LogBD;
import com.sbpay.sgi.cron.enums.MsgErrorMail;
import com.sbpay.sgi.cron.enums.MsgErrorSQL;
import com.sbpay.sgi.cron.enums.MsgGenerationIncomingOnUs;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.enums.StatusProcessType;
import com.sbpay.sgi.cron.srv.utils.CommonSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;
import com.sbpay.sgi.cron.utils.file.IncomingExportVisaNac;
import com.sbpay.sgi.cron.utils.file.IncomingFileUtils;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;
import com.sbpay.sgi.cron.utils.generals.DateUtils;
import com.sbpay.sgi.cron.validation.ProcessValidator;
import com.sbpay.sgi.cron.validation.impl.ProcessValidatorImpl;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 08/02/2016, (ACL) - versión inicial
 * </ul>
 * <p>
 * 
 * Clase encargada de generar el outgoing visa
 * <p>
 * <B>Todos los derechos reservados por ACL SA.</B>
 */
public class GenerarIncomingVisaSrv extends CommonSrv {
    
    /** VARIABLE PARA EL LOGER */
    private static final Logger LOGGER = Logger
            .getLogger( GenerarIncomingVisaSrv.class );

	private static final String FLAG_MONEDA_CLP = "1";
	
	/** PROCESO */
	private final ProcessCodeType PROCESO_ACTUAL = ProcessCodeType.GENERAR_INCOMING_VISA_NAC;
    
    /** The single instance. */
    private static GenerarIncomingVisaSrv singleINSTANCE = null;
    
    /**
     * Creates the instance.
     */
    private static void createInstance() {
        synchronized ( GenerarIncomingVisaSrv.class ) {
            if ( singleINSTANCE == null ) {
                singleINSTANCE = new GenerarIncomingVisaSrv();
            }
        }
    }
    
    /**
     * Patron singleton.
     * 
     * @return ActivarPrepagoSrv retorna instancia del servicio.
     */
    public static GenerarIncomingVisaSrv getInstance() {
        if ( singleINSTANCE == null ) {
            createInstance();
        }
        return singleINSTANCE;
    }
    
    /**
     * Metodo de restricion de sobreescritura de la clase.
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        super.clone();
        throw new CloneNotSupportedException();
    }
    
    /**
     * Constructor Privado.
     * 
     * @throws SQLException
     */
    private GenerarIncomingVisaSrv() {
        super();   
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 - 10/12/2015, (ACL) - versión inicial
     * </ul>
     * <p>
     * Metodo encargado de realizar todo el proceso de generacion de
     * incoming a transabank, en las carpetas correspondientes, el
     * cual tanto en caso de error como en caso exitoso envia un email
     * 
     * 
     * @since 1.X
     */
	public boolean generaArhivoIncomingVisa() throws AppException {
    	GenerarIncomingDAO generarIncomingDAO = null;
		boolean flag = Boolean.FALSE;
		try {

		    ProcessValidator validadorProceso = new ProcessValidatorImpl();
		    if (!validadorProceso.isActiveProcess(PROCESO_ACTUAL)) {
		        LOGGER.info("Proceso : "+PROCESO_ACTUAL.getCodigoProceso()+ ", no se encuentra activado en BD (TBL_CRON_CONFIG).");
		        String asunto = MsgProceso.MSG_PROCESO_DESACTIVADO_ASUNTO
	                    .toString().replace( ":?:",
	                            PROCESO_ACTUAL.getCodigoProceso() );
	            String mensaje = MsgProceso.MSG_PROCESO_DESACTIVADO_BODY.toString()
	                    .replace( ":?:", PROCESO_ACTUAL.getCodigoProceso() );
	            MailSendSrv.getInstance().sendMail( asunto, mensaje );
	            return flag;
		    }
		    
		    // CARGA PARAMETROS
			ParamsGenerateIncomingVisaDTO parametros = getParametros();
			
			if (validadorProceso.hasTheValidParametersForTheProcess( PROCESO_ACTUAL, parametros )) {
			  generarIncomingDAO = GenerarIncomingDaoFactory
                  .getInstance().getNewEntidadDao();
			  
			  CommonsDao commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
			  Integer sidOperador = commonsDAO.getOperador(ConstantesOperador.VISA_NAC.toString());
			  String binOperador = commonsDAO.getBinOperador(ConstantesOperador.VISA_NAC.toString());
			  
				// LLAMA AL SP_SIG_CARGAOUTGOING_VISA
				if (generarIncomingDAO.callSPCargaOutgoingVisa(sidOperador)) {
					LOGGER.info("SE HA LLAMADO AL PROCEDIMIENTO CORRECTAMENTE");
				}
				
		        // OBTIENE LA LISTA DE TRANSACCIONES DESDE BD
	            List<Transaccion> listaTrasacciones = generarIncomingDAO
	                    .loadTransactions();
	            
	            // ESCRIBE EN LOG ERROR NO SE ENCONTRARON RESULTADOS
	            if ( listaTrasacciones != null && listaTrasacciones.size() == 0 ) {
	            	LOGGER.warn( MsgGenerationIncomingOnUs.WARNING_TRANSACTIONS_NOT_FOUND
	                        .toString() );
	            }

	            String incomingFilename = generateIncomingFilename( parametros );
	            String controlFilename = generateControlFilename( parametros );
	            String ruta = parametros.getPathOutVisa();
	            // GENERA ARCHIVO INCOMING Y ARCHIVO DE CONTROL CON LAS
	            // TRANSACCIONES ENCONTRADAS
	            IncomingFileUtils incomingOut = new IncomingExportVisaNac(binOperador);
	            incomingOut.exportIncomingFile(ruta, incomingFilename, controlFilename, listaTrasacciones);
	            
	            // BORRA TABLA TMP_OUTGOING_TRANSBANK UNA VEZ QUE EL
	            // PROCESO SE EJECUTA CORRECTAMENTE
	            generarIncomingDAO.clearTemporalOutgoingTransbank();

				// SE DEJA REGISTRO EN LA TABLA TBL_LOG_
				saveLog(incomingFilename);
				flag = Boolean.TRUE;
			} else {
				LOGGER.info("Parametros de Base de Datos Incompletos");
				MailSendSrv.getInstance().sendMail(
						MsgErrorMail.ALERTA_PRTS.toString(),
						MsgErrorMail.ALERTA_PRTS_TXT.toString());
			}
		} catch (Exception e) {
			throw new AppException(e.getMessage(),
					e);
		} finally {
			try {
				if (generarIncomingDAO != null) {
					generarIncomingDAO.close();
				}
			} catch (SQLException e) {
				throw new AppException(
						MsgErrorSQL.ERROR_GEN_INC_VISA_CLOSE.toString(), e);
			}
		}
		return flag;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 18/12/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @param incomingFilename
     * @throws AppException
     * 
     * @since 1.X
     */
    private void saveLog( String incomingFilename ) throws AppException {
        CommonsDao commonsDAO = null;
        TblCronLogDTO log = new TblCronLogDTO();
        log.setFilename( incomingFilename );
        log.setFileFlag( StatusProcessType.PROCESS_PENDING.getValue() );
        log.setFecha( DateUtils.parseFormat(
                DateUtils.getTodayInYYMMDDFormat(),
                DateFormatType.FORMAT_YYMMDD,
                DateFormatType.FORMAT_DDMMYY_WITH_SLASH_SEPARATOR ) );
        try {
            commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
            commonsDAO.saveTblCronLog( log, LogBD.TBL_LOG_OUT_VISA );
            commonsDAO.endTx();
        }
        catch ( SQLException e ) {
            try {
                if ( commonsDAO != null ) {
                    commonsDAO.rollBack();
                }else{
                    LOGGER.info( "No se ha ejecutado rollback" );
                }
            }
            catch ( SQLException e1 ) {
                throw new AppException(
                        "Ha ocurrido un error al realizar rollback", e1 );
            }
            throw new AppException( e.getMessage(), e );
        }
        finally {
            try {
                if ( commonsDAO != null ) {
                    commonsDAO.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException(
                        "Ha ocurrido un error al realizar rollback", e );
            }
            
        }
        
    }
    
    
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 - 7/12/2015, (ACL) - versión inicial
     * </ul>
     * <p>
     * 
     * @param parametros
     * 
     * @return
     * @throws AppException
     * @since 1.X
     */
    private String generateIncomingFilename(
            ParamsGenerateIncomingVisaDTO parametros ) throws AppException {
        
        String date = DateUtils.getDateTodayInYYYYMMDD();
        StringBuilder str = new StringBuilder();
        str.append( parametros.getFormatFilenameIncVisa() ).append( date )
                .append( ConstantesUtil.POINT )
                .append( parametros.getFormatExtNameIncVisa() );
        
        return str.toString();
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 - 11/12/2015, (ACL) - versión inicial
     * </ul>
     * <p>
     * Metodo que genera el nombre del archivo de control
     * 
     * 
     * @return
     * @since 1.X
     */
    private String generateControlFilename( ParamsGenerateIncomingVisaDTO parametros ) {
        String date = DateUtils.getDateTodayInYYYYMMDD();
        StringBuilder str = new StringBuilder();
        str.append( parametros.getFormatFilenameIncVisa() ).append( date )
                .append( ConstantesUtil.POINT )
                .append( parametros.getFormatExtNameIncVisaCrt() );
        
        return str.toString();
    }
    
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 - 24/11/2015, (ACL) - versión inicial
     * </ul>
     * <p>
     * 
     * Metodo setea los parametros de sistema del cron transbank.
     * 
     * @return
     * @throws AppException
     * @since 1.X
     */
  private ParamsGenerateIncomingVisaDTO getParametros() throws AppException {
    ParamsGenerateIncomingVisaDTO parametros = null;
    List<ParametroDTO> paramDTOList = null;
    CommonsDao commonsDAO = null;
    try {
      // INICIA EL DAO
      commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
      // SE OBTIENEN TODOS LOS CODIGOS
      paramDTOList = commonsDAO.getParametroCodGrupoDato(ConstantesPRTS.CRON_VISA_NAC.toString());

      if (paramDTOList != null) {
        // PARAMETROS DE BD TBL_PRTS
        parametros = new ParamsGenerateIncomingVisaDTO();
        
        // RUTA DEL SERVIDOR DE sbpay DONDE SE DEJARA EL INCOMING VISA GENERADO
        parametros.setPathOutVisa(CommonsUtils.getCodiDato(paramDTOList,
            ConstantesPRTS.PATH_VISA_UP_INC.toString()));
        
        // FORMATO DE NOMBRE DEL INCOMING VISA GENERADO, PREFIJO STARWITH
        parametros.setFormatFilenameIncVisa(CommonsUtils.getCodiDato(paramDTOList,
            ConstantesPRTS.FORMAT_FILENAME_INC_VISA.toString()));
        
        // FORMATO DE EXTENSION DE ARCHIVO INCOMING VISA GENERADO , SUFIJO ENDSWITH
        parametros.setFormatExtNameIncVisa(CommonsUtils.getCodiDato(paramDTOList,
            ConstantesPRTS.FORMAT_EXTNAME_INC_VISA.toString()));
        
        // FORMATO DE EXTENSION DE ARCHIVO DE CONTROL GENERADO, SUFIJO  ENDSWITH
        parametros.setFormatExtNameIncVisaCrt(CommonsUtils.getCodiDato(paramDTOList,
            ConstantesPRTS.FORMAT_EXTNAME_INC_VISA_CTR.toString()));
        
       
        LOGGER.info(parametros);
      }

      return parametros;
    } catch (SQLException e) {
      throw new AppException(MsgErrorSQL.ERROR_QUERY_PRTS.toString(), e);
    } finally {
      try {
        if (commonsDAO != null) {
          commonsDAO.close();
        }
      } catch (SQLException e) {
        throw new AppException(MsgErrorSQL.ERROR_QUERY_PRTS_CLOSE.toString(), e);
      }
    }
  }
  
      
}
