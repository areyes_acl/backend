package com.sbpay.sgi.cron.step;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.srv.download.DownloadRechazosSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 18/12/2015, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class StepDownloadRechazos {
    
    /**
     * VARIABLES
     */
    private static final Logger LOG = Logger
            .getLogger( StepDownloadRechazos.class );
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2015, (Everis Chile) - versión inicial
     * </ul>
     * <p>
     * 
     * @param contribution
     * @param chunkContext
     * @return
     * @throws Exception
     * @see org.springframework.batch.core.step.tasklet.Tasklet#execute(org.springframework.batch.core.StepContribution,
     *      org.springframework.batch.core.scope.context.ChunkContext)
     * @since 1.X
     */
    public void execute() throws Exception {
        
        LOG.info( "======>Inicio proceso de Descarga Rechazos : StepDownloadRechazos<=====" );
        try {
            if ( DownloadRechazosSrv.getInstance().descargarRechazos() ) {
                LOG.info( "======>El proceso (StepDownloadRechazos) de descarga de rechazos ha finalizado correctamente<=====" );
            }
            else {
                LOG.info( "======> Ha concluido el proceso de descarga de rechazos con observaciones (ver logs)!" );
            }
        }
        catch ( AppException e ) {
            LOG.error( e.getMessage(), e );
            MailSendSrv.getInstance().sendMail(
                    MsgProceso.MSG_GENERIC_ERROR_MSJ.toString(),
                    ( e.getStackTraceDescripcion() != null ) ? e
                            .getStackTraceDescripcion() : e.getMessage() );
        }
        
    }
    
}
