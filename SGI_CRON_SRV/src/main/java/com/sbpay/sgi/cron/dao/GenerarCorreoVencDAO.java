package com.sbpay.sgi.cron.dao;

import java.sql.SQLException;
import java.util.List;

import com.sbpay.sgi.cron.dao.Dao;
import com.sbpay.sgi.cron.dto.ParamCorreoVencimientoDTO;

public interface GenerarCorreoVencDAO extends Dao{

	public List<ParamCorreoVencimientoDTO> obtenerTransVenc(Integer dias) throws SQLException;
}
