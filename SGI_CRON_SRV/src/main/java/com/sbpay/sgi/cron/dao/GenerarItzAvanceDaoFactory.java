package com.sbpay.sgi.cron.dao;

import java.sql.SQLException;

import com.sbpay.sgi.cron.dao.BaseDaoFactory;
import com.sbpay.sgi.cron.ds.ConnectionProviderGenerarAvance;
import com.sbpay.sgi.cron.ds.ConnectionProvider;



public class GenerarItzAvanceDaoFactory  extends BaseDaoFactory<GenerarItzAvanceDao>{
	/** The single instance. */
	private static GenerarItzAvanceDaoFactory singleINSTANCE = null;

	/**
	 * Creates the instance.
	 * 
	 * @throws SQLException
	 */
	private static void createInstance() throws SQLException {
		synchronized (GenerarItzAvanceDaoFactory.class) {
			if (singleINSTANCE == null) {
				singleINSTANCE = new GenerarItzAvanceDaoFactory();
			}
		}
	}

	/**
	 * Patron singleton.
	 * 
	 * @return GenerarItzAvanceDaoFactory retorna instancia del servicio.
	 * @throws SQLException
	 */
	public static GenerarItzAvanceDaoFactory getInstance() throws SQLException {
		if (singleINSTANCE == null) {
			createInstance();
		}
		return singleINSTANCE;
	}

	/**
	 * Metodo de restricion de sobreescritura de la clase.
	 */
	@Override
	protected Object clone() throws CloneNotSupportedException {
		super.clone();
		throw new CloneNotSupportedException();
	}

	/**
	 * Constructor default.
	 * 
	 * @throws SQLException Error de SQL.
	 */
	private GenerarItzAvanceDaoFactory() throws SQLException {
		super();
	}
	
	@Override
	public GenerarItzAvanceDaoJdbc getNewEntidadDao() throws SQLException {
		//JNDI sbpay
		return new GenerarItzAvanceDaoJdbc(ConnectionProviderGenerarAvance.getInstance()
				.getConnection());
		
		//PRUEBA LOCAL 
		/*return new GenerarItzAvanceDaoJdbc(ConnectionProvider.getInstance()
				.getConnection());*/
	}
	
}
