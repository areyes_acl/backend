package com.sbpay.sgi.cron.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.dao.BaseDao;
import com.sbpay.sgi.cron.dto.ReContableAvanceLogDTO;
import com.sbpay.sgi.cron.dto.TmpExportContableDTO;
import com.sbpay.sgi.cron.enums.ConstantesBD;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.LogBD;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;

public class RegenerarContableAvanceDaoJdbc extends
		BaseDao<RegenerarContableAvanceDao> implements
		RegenerarContableAvanceDao {

	/** VARIABLE PARA EL LOGER */
	private static final Logger LOGGER = Logger
			.getLogger(RegenerarContableAvanceDaoJdbc.class);

	private static final String SP_SIG_OBTENER_INFO_RECONTABLE = "{call SP_SIG_OBTENER_INFO_RECONTABLE(?,?,?,?)}";

	private static final String SP_SIG_RECONTABLE_MAIN_AVA = "{call SP_SIG_RECONTABLE_MAIN_AVA(?,?,?)}";

	private static final String SP_SIG_CLEAN_TMP_RECONTABLE = "{call SP_SIG_CLEAN_TMP_RECONTABLE(?,?,?)}";

	private static final String SP_SIG_UPDATE_CC_AVANCE = "{call SP_SIG_UPDATE_CC_AVANCE(?,?,?)}";
	
	private static final String SP_SIG_INSERT_HIS_CONTABLE = "{call SP_SIG_INSERT_HIS_CONTABLE(?,?,?,?,?,?)}";
	
	private static final String SP_SIG_UPDATE_LOG_CONTABLE = "{call SP_SIG_UPDATE_LOG_CONTABLE(?,?,?)}";

	public RegenerarContableAvanceDaoJdbc(Connection connection) {
		super(connection);
	}

	@Override
	public List<TmpExportContableDTO> obtenerInformacionCnblt(String fechaContable)
			throws SQLException {

		OracleCallableStatement callableStatement = null;
		List<TmpExportContableDTO> lstContableDTO = new ArrayList<TmpExportContableDTO>();
		TmpExportContableDTO paramExportContableDTO;
		try {

			LOGGER.info("********* Class: RegenerarContableAvanceDaoJdbc: SE EJECUTA SP : SP_SIG_OBTENER_INFO_RECONTABLE *******_");
			callableStatement = (OracleCallableStatement) this.getConnection()
					.prepareCall(SP_SIG_OBTENER_INFO_RECONTABLE);

			callableStatement.registerOutParameter("warning",
					OracleTypes.VARCHAR);
			callableStatement.registerOutParameter("cod_error",
					OracleTypes.NUMBER);
			callableStatement.registerOutParameter("prfCursor",
					OracleTypes.CURSOR);
			callableStatement.setString("V_FECHA", fechaContable);
			
			callableStatement.execute();

			ResultSet resultados = (ResultSet) ((callableStatement)
					.getObject("prfCursor"));
			int codigoError = callableStatement.getInt("cod_error");

			if (codigoError == 0)
				while (resultados.next()) {
					paramExportContableDTO = new TmpExportContableDTO();
					paramExportContableDTO.setTransaccion(resultados
							.getString("TRANSACCION"));
					paramExportContableDTO.setMovOrg(resultados
							.getString("MOV_ORG"));
					paramExportContableDTO.setProducto(resultados
							.getString("PRODUCTO"));
					paramExportContableDTO.setSucMov(resultados
							.getString("SUC_MOV"));
					paramExportContableDTO.setSucPro(resultados
							.getString("SUC_PRO"));
					paramExportContableDTO.setSucPago(resultados
							.getString("SUC_PAGO"));
					paramExportContableDTO.setComercio(resultados
							.getLong("COMERCIO"));
					paramExportContableDTO
							.setFecha(resultados.getString("FEC"));
					paramExportContableDTO.setIdentificador(resultados
							.getString("IDENTIFICADOR"));
					paramExportContableDTO.setCompania(resultados
							.getString("COMPANIA"));
					paramExportContableDTO.setRutEntidad(resultados
							.getString("RUT_ENTIDAD"));
					paramExportContableDTO.setMov(CommonsUtils
							.getStringNull(resultados.getString("MOVIMIENTO")));
					paramExportContableDTO.setMonto(resultados
							.getString("MONTO"));
					paramExportContableDTO.setPortafolio(resultados
							.getString("PORTAFOLIO"));
					paramExportContableDTO.setCuenta(resultados
							.getString("CUENTA"));
					paramExportContableDTO.setRutCliente(resultados
							.getString("RUTCLIENTE"));
					lstContableDTO.add(paramExportContableDTO);
				}
			else {
				LOGGER.error("Method: obtenerInformacionCnblt - Problemas al leer informacion contable desde la base de datos.");
			}
		} finally {
			if (callableStatement != null) {
				callableStatement.close();
			}
		}
		return lstContableDTO;
	}

	@Override
	public boolean cargarInformacionContableAvance(String fechaContable) throws SQLException {
		CallableStatement callableStatement = null;

		LOGGER.info("****** Class: RegenerarContableAvanceDaoJdbc: SE EJECUTA STORE PROCEDURE SP_SIG_RECONTABLE_MAIN_AVA*****");

		try {
			callableStatement = this.getConnection().prepareCall(
					SP_SIG_RECONTABLE_MAIN_AVA);

			callableStatement.registerOutParameter(1, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(2, java.sql.Types.NUMERIC);
			callableStatement.setString(3, fechaContable);
			callableStatement.execute();

			String warning = callableStatement.getString(1);
			Integer codigoError = callableStatement.getInt(2);
			LOGGER.info("WARNING : " + warning);
			LOGGER.info("codigoError : " + codigoError);

			if (codigoError != 0) {
				LOGGER.warn("Cod error " + codigoError + ", mensaje:" + warning);
				return Boolean.FALSE;
			}
			return Boolean.TRUE;
		} finally {

			if (callableStatement != null) {
				callableStatement.close();
			}

		}

	}

	@Override
	public void clearTmpContable(String fechaContable) throws SQLException {
		CallableStatement callableStatement = null;
		LOGGER.info("****** Class: RegenerarContableAvanceDaoJdbc: SE EJECUTA STORE PROCEDURE CALL SP SP_SIG_CLEAN_TMP_RECONTABLE *****");
		try {
			callableStatement = this.getConnection().prepareCall(
					SP_SIG_CLEAN_TMP_RECONTABLE);
			callableStatement.registerOutParameter(1, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(2, java.sql.Types.NUMERIC);
			callableStatement.setString(3, fechaContable);
			
			callableStatement.execute();
			String warning = callableStatement.getString(1);
			Integer codigoError = callableStatement.getInt(2);
			LOGGER.info("WARNING : " + warning);
			LOGGER.info("codigoError : " + codigoError);
		} finally {

			if (callableStatement != null) {
				callableStatement.close();
			}

		}

	}

	@Override
	public List<ReContableAvanceLogDTO> getDataRegenerar() throws SQLException {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<ReContableAvanceLogDTO> lstContableAvanceDTO = null;

		final StringBuilder str = new StringBuilder();

		try {
			str.append("SELECT TC.SID AS sidContable, "
					+ " TO_CHAR(TC.FECHA, 'YYYYMMDD') AS fecha, "
					+ " TC.FILE_NAME AS nameFile, "
					+ " TC.FILE_FLAG AS flag "
					+ " FROM TBL_LOG_CONTABLE TC " + " WHERE TC.FILE_FLAG = 2 ");

			stmt = this.getConnection().prepareStatement(str.toString());
			rs = stmt.executeQuery();

			lstContableAvanceDTO = new ArrayList<ReContableAvanceLogDTO>();

			while (rs.next()) {
				ReContableAvanceLogDTO contableAvanceLogDTO = new ReContableAvanceLogDTO();
				contableAvanceLogDTO.setSidContable(rs.getInt("sidContable"));
				contableAvanceLogDTO.setFecha(rs.getString("fecha"));
				contableAvanceLogDTO.setFilename(rs.getString("nameFile"));
				contableAvanceLogDTO.setFlag(rs.getString("flag"));
				lstContableAvanceDTO.add(contableAvanceLogDTO);
			}
			return lstContableAvanceDTO;
		} finally {
			if (stmt != null) {
				stmt.close();
			}

			if (rs != null) {
				rs.close();
			}

		}
	}
	

	@Override
	public void updateDataContableAvances(Integer sidContable)
			throws SQLException {
		CallableStatement callableStatement = null;
		LOGGER.info("****** Class: RegenerarContableAvanceDaoJdbc: SE EJECUTA STORE PROCEDURE CALL SP SP_SIG_UPDATE_CC_AVANCE *****");
		try {
			callableStatement = this.getConnection().prepareCall(
					SP_SIG_UPDATE_CC_AVANCE);
			callableStatement.registerOutParameter(1, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(2, java.sql.Types.NUMERIC);
			callableStatement.setInt(3, sidContable);
			
			callableStatement.execute();
			String warning = callableStatement.getString(1);
			Integer codigoError = callableStatement.getInt(2);
			
			LOGGER.info("WARNING : " + warning);
			LOGGER.info("codigoError : " + codigoError);
		} finally {

			if (callableStatement != null) {
				callableStatement.close();
			}

		}
	}
	
	@Override
	public void savehistoriaContable(Integer sidContable)
			throws SQLException {
		CallableStatement callableStatement = null;
		LOGGER.info("****** Class: RegenerarContableAvanceDaoJdbc: SE EJECUTA STORE PROCEDURE CALL SP SP_SIG_INSERT_HIS_CONTABLE *****");
		try {
			callableStatement = this.getConnection().prepareCall(
					SP_SIG_INSERT_HIS_CONTABLE);
			
			callableStatement.registerOutParameter("FLAG", OracleTypes.VARCHAR);
			callableStatement.registerOutParameter("ERROR_OUT", OracleTypes.VARCHAR);
			
			callableStatement.setInt("VSID_CONTABLE", sidContable);
			callableStatement.setInt("VSID_USER", 1);
			callableStatement.setString("VMOTIVO", "Se regenera de forma automatica");
			callableStatement.setString("VTIPO", "CRON");
			
			callableStatement.execute();
			
			String warning = callableStatement.getString("ERROR_OUT");
			String codigoError=  callableStatement.getString("FLAG");
			
			LOGGER.info("WARNING : " + warning);
			LOGGER.info("codigoError : " + codigoError);
		} finally {

			if (callableStatement != null) {
				callableStatement.close();
			}

		}
	}
	
	@Override
	public void updateTablelog(Integer sidContable)
			throws SQLException {
		CallableStatement callableStatement = null;
		LOGGER.info("****** Class: RegenerarContableAvanceDaoJdbc: SE EJECUTA STORE PROCEDURE CALL SP SP_SIG_UPDATE_LOG_CONTABLE *****");
		try {
			callableStatement = this.getConnection().prepareCall(
					SP_SIG_UPDATE_LOG_CONTABLE);
			callableStatement.registerOutParameter(1, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(2, java.sql.Types.NUMERIC);
			callableStatement.setInt(3, sidContable);
			
			callableStatement.execute();
			String warning = callableStatement.getString(1);
			Integer codigoError = callableStatement.getInt(2);
			
			LOGGER.info("WARNING : " + warning);
			LOGGER.info("codigoError : " + codigoError);
		} finally {

			if (callableStatement != null) {
				callableStatement.close();
			}

		}
	}
	
	
}
