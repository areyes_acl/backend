package com.sbpay.sgi.cron.step;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.enums.MsgExitoMail;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.srv.upload.ReuploadAsientosCntblAvanceSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;

public class StepReuploadAsientosCntblAvance {
    /** The Constant LOGGER. */
    private static final Logger LOG = Logger.getLogger( StepReuploadAsientosCntblAvance.class );
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/XX/2019, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws Exception
     * @since 1.X
     */
    public void execute() throws Exception {
    	 try {
             //LOG.info( "Inicio Upload Asientos contables " );
             if ( ReuploadAsientosCntblAvanceSrv.getInstance().reuploadAsientosContablesAvances() ) {
             	LOG.info( MsgExitoMail.EXITO_PROCESS_UPLOAD_CNTBL_AVA.toString());
             }
             
         }
         catch ( AppException e ) {
             LOG.error( e.getMessage(), e );
             MailSendSrv.getInstance().sendMail(
                     MsgProceso.MSG_GENERIC_ERROR_MSJ.toString(),
                     ( e.getStackTraceDescripcion() != null ) ? e
                             .getStackTraceDescripcion() : e.getMessage() );
         }
    	
    }
}
