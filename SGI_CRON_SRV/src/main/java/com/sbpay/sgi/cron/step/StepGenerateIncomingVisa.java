package com.sbpay.sgi.cron.step;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.enums.MsgExitoMail;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.srv.export.GenerarIncomingVisaSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 08/02/2016, (ACL) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SA.</B>
 */
public class StepGenerateIncomingVisa {
    
    /**
     * VARIABLES
     */
    private static final Logger LOG = Logger
            .getLogger( StepGenerateIncomingVisa.class );
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws Exception
     * @since 1.X
     */
    public void execute() throws Exception {
        try {
            
            LOG.info( "=======> Inicio proceso de Generacion de Incoming VISA Nacional : StepGenerateIncomingVisa <===========" );
            if ( GenerarIncomingVisaSrv.getInstance()
                    .generaArhivoIncomingVisa() ) {
                LOG.info( "Envio Correo de Generar/Exportar Incoming VISA Nacional Satisfactorio: Proceso Generar Incoming On Us" );
                MailSendSrv.getInstance().sendMailOk(
                        MsgExitoMail.EXITO_GENERATE_INC_VISA_TITLE.toString(),
                        MsgExitoMail.EXITO_GENERATE_INC_VISA_BODY.toString() );
            }
            else {
                LOG.info( "Generar/Exportar Incoming VISA Finaliza! (total o parcialmente consultar log)" );
            }
        }
        catch ( AppException e ) {
            LOG.error( e.getMessage(), e );
            MailSendSrv.getInstance().sendMail(
                    MsgProceso.MSG_GENERIC_ERROR_MSJ.toString(),
                    ( e.getStackTraceDescripcion() != null ) ? e
                            .getStackTraceDescripcion() : e.getMessage() );
        }
        
    }
    
}
