package com.sbpay.sgi.cron.dao;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.Dao;
import com.sbpay.sgi.cron.dto.CompensacionPagoDTO;

public interface CompscnPagoDao extends Dao{

	public Boolean guardarCompscnPagoDao(CompensacionPagoDTO compensacionPagoDTO)throws AppException ;
	
}
