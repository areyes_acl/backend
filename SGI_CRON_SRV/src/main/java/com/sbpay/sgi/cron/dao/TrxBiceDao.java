package com.sbpay.sgi.cron.dao;

import java.sql.SQLException;

import com.sbpay.sgi.cron.dao.Dao;
import com.sbpay.sgi.cron.dto.TransaccionBiceDTO;


/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/10/2018, (ACL) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SA.</B>
 */
public interface TrxBiceDao extends Dao {

	boolean saveTransaction(TransaccionBiceDTO instance) throws SQLException;

	boolean callSPCargaLogTrxBice() throws SQLException;
    



    
}
