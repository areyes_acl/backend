package com.sbpay.sgi.cron.job;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;

import com.sbpay.sgi.cron.step.StepDownloadCPagoTbk;
import com.sbpay.sgi.cron.step.StepDownloadComisionesTbk;
import com.sbpay.sgi.cron.step.StepDownloadOutgoingTbk;
import com.sbpay.sgi.cron.step.StepDownloadOutgoingVisa;
import com.sbpay.sgi.cron.step.StepDownloadOutgoingVisaNac;
import com.sbpay.sgi.cron.step.StepUploadICOutgoingTbk;
import com.sbpay.sgi.cron.step.StepUploadICOutgoingVisa;
import com.sbpay.sgi.cron.step.StepUploadICOutgoingVisaNac;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 7/1/2016, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class FirstJob implements Job {
	/** LOGGER CONSTANT **/
	private static final Logger LOG = Logger.getLogger(FirstJob.class);

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @param context
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
	 * @since 1.X
	 */
	@Override
	public void execute(JobExecutionContext context) {

		LOG.info("========== EJECUCION DEL PRIMER JOB =============");
	
		descargarOutgoingTransbank();
		descargarComisionesTransbank();
		descargarCompensacionPagos();		
		subirOutgoingNacIC();
		
		descargarOutgoingVisaNac();
		subirOutgoingVisaNacIc();

		descargarOutgoingVisaInt();
		subirOutgoingVisaIntIC();
		
		
		LOG.info("========== FIN EJECUCION DEL PRIMER JOB =========");

	}
	
	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @since 1.X
	 */
	
	public void descargarOutgoingTransbank() {
		try {
			// LLAMA EL DOWNLOAD TRANSBANK
			StepDownloadOutgoingTbk stepDownload = new StepDownloadOutgoingTbk();
			stepDownload.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
	
	
	
	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @since 1.X
	 */
	public void subirOutgoingVisaIntIC() {
		try {
			StepUploadICOutgoingVisa stepUpload = new StepUploadICOutgoingVisa();
			stepUpload.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

	}
	
	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @since 1.X
	 */
	public void descargarOutgoingVisaInt() {
		try {
			// LLAMA EL DOWNLOAD VISA INTERNACIONAL
			StepDownloadOutgoingVisa stepDownload = new StepDownloadOutgoingVisa();
			stepDownload.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

	}
	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @since 1.X
	 */
	public void descargarCompensacionPagos() {
		try {
			// LLAMA EL DOWNLOAD COMPENSACION PAGOS TRANSBANK
			StepDownloadCPagoTbk stepDownload = new StepDownloadCPagoTbk();
			stepDownload.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @since 1.X
	 */
	public void descargarComisionesTransbank() {
		try {
			// LLAMA EL DOWNLOAD COMISIONES TRANSBANK
			StepDownloadComisionesTbk stepDownload = new StepDownloadComisionesTbk();
			stepDownload.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 7/1/2016, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo encarcargo de llamar a la tarea uploadOutgoing
	 * 
	 * @since 1.X
	 */
	public void subirOutgoingNacIC() {

		try {
			StepUploadICOutgoingTbk stepUpload = new StepUploadICOutgoingTbk();
			stepUpload.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

	}
	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 09/04/2019, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @since 1.0
	 */
	public void descargarOutgoingVisaNac() {
		try {
			// LLAMA EL DOWNLOAD 
			StepDownloadOutgoingVisaNac stepDownload = new StepDownloadOutgoingVisaNac();
			stepDownload.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
	
	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 09/04/2019, (ACL-sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo encarcargo de llamar a la tarea StepUploadICOutgoingVisaNac
	 * 
	 * @since 1.X
	 */
	public void subirOutgoingVisaNacIc() {

		try {
			StepUploadICOutgoingVisaNac stepUpload = new StepUploadICOutgoingVisaNac();
			stepUpload.execute();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

	}
	
}
