package com.sbpay.sgi.cron.srv.upload;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dto.DataFTP;
import com.sbpay.sgi.cron.dto.ParamUploadOnusTbkDTO;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.dto.TblCronLogDTO;
import com.sbpay.sgi.cron.enums.ConstantesPRTS;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.FilterTypeSearch;
import com.sbpay.sgi.cron.enums.LogBD;
import com.sbpay.sgi.cron.enums.MsgErrorMail;
import com.sbpay.sgi.cron.enums.MsgErrorSQL;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.enums.StatusProcessType;
import com.sbpay.sgi.cron.srv.utils.ClientFTPSrv;
import com.sbpay.sgi.cron.srv.utils.CommonSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;
import com.sbpay.sgi.cron.utils.generals.DateUtils;
import com.sbpay.sgi.cron.validation.ProcessValidator;
import com.sbpay.sgi.cron.validation.impl.ProcessValidatorImpl;

public final class UploadOnusTbkSrv extends CommonSrv {
	
	private static final Logger LOG = Logger.getLogger(UploadOnusTbkSrv.class);
	
	private static UploadOnusTbkSrv singleINSTANCE = null;
	
    private final ProcessCodeType PROCESO_ACTUAL = ProcessCodeType.SUBIDA_ONUS_A_TRANSBANK;
		
	private static void createInstance() {
		synchronized (UploadOnusTbkSrv.class) {
			if(singleINSTANCE == null) {
				singleINSTANCE = new UploadOnusTbkSrv();
			}
		}
	}
	
	public static UploadOnusTbkSrv getInstance() {
		if (singleINSTANCE == null){
			createInstance();
		}
		return singleINSTANCE;
	}
	
	@Override
	protected Object clone() throws CloneNotSupportedException {
		super.clone();
		throw new CloneNotSupportedException();
	}
	
	private UploadOnusTbkSrv() {
		super();
	}
	
	/**
	 * 
	 * Metodo encargado de subir el archivo ONUS a 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * @return
	 * @throws AppException
	 * 
	 */
	public boolean uploadOnusTbk() throws AppException {
		ParamUploadOnusTbkDTO parametros = null;
		boolean flag = false;
		List<String> fileDirectory = null;
		List<String> fileUpload = null;
		String msj = "";
		
		ProcessValidator validadorProceso = new ProcessValidatorImpl();

        if ( !validadorProceso.isActiveProcess( PROCESO_ACTUAL ) ) {
            String asunto = MsgProceso.MSG_PROCESO_DESACTIVADO_ASUNTO
                    .toString().replace( ":?:",
                            PROCESO_ACTUAL.getCodigoProceso() );
            String mensaje = MsgProceso.MSG_PROCESO_DESACTIVADO_BODY.toString()
                    .replace( ":?:", PROCESO_ACTUAL.getCodigoProceso() );
            MailSendSrv.getInstance().sendMail( asunto, mensaje );
            return false;
        }
		
		parametros = getParametrosUploadCron();		
		
		if(validadorProceso.hasTheValidParametersForTheProcess( PROCESO_ACTUAL, parametros )){
			LOG.info("== SE ENCUENTRAN CARGADOS TODOS LOS PARAMETROS PARA EL PROCESO, SE INICIA EL PROCESO DE SUBIDA DE ONUS A TRANSBANK===");

			String fileExt = parametros.getExtOnusTbkFile();
			String starWith = parametros.getNameOnusTbkFile();
			String pathInc = parametros.getPathOnusTbkInc();
			
			// VALIDA NOMBRES
			fileDirectory = getFileStartsWith(pathInc, starWith, fileExt);
			LOG.info("Lista de archivos que contiene formato de nombre valido para subir:  " + fileDirectory);
			
			if (fileDirectory != null && fileDirectory.size() > 0){
				
				// VALIDA QUE NO HAYA SIDO SUBIDO ANTES
				fileUpload = getFileUpload(fileDirectory, parametros);
				 LOG.info("Lista de archivos que no han sido subidos con anterioridad:" + fileUpload);
				
				if(fileUpload != null && fileUpload.size() > 0) {
					
					String pathSearch = parametros.getPathOnusTbkInc();
					String pathFtp = parametros.getFtpPathIn();
					
					int count = 0;
					
					for(String nombrefileDest : fileUpload) {
						try{
							if(ClientFTPSrv.getInstance().uploadFileSFTP(parametros.getDataFTP(), pathSearch, pathFtp, nombrefileDest)) {
								CommonsUtils.moveFile(parametros.getPathOnusTbkInc() + nombrefileDest, parametros.getPathBackup() + nombrefileDest);
								
								
								if(updateUploadOnusTbk(nombrefileDest,starWith, StatusProcessType.PROCESS_SUCESSFUL)){
									LOG.info("ARCHIVO " + nombrefileDest + "SUBIDO CORRECTAMENTE A FTP TRANSBANK");
								}
								count++;
							}
						}catch(AppException e){		
							updateUploadOnusTbk(nombrefileDest,starWith, StatusProcessType.PROCESS_ERROR);
							CommonsUtils.moveFile(parametros.getPathOnusTbkInc() + nombrefileDest, parametros.getPathError() + nombrefileDest);
							msj = ConstantesUtil.SKIP_LINE.toString().concat("-) ").concat(msj).concat(nombrefileDest).concat(ConstantesUtil.SKIP_LINE.toString());
							LOG.info("ARCHIVO " + nombrefileDest + "NO SE HA SUBIDO CORRECTAMENTE A FTP TRANSBANK, SE CONTINUA CON SIGUIENTE");
							LOG.error(e.getMessage(), e);
						}
					}
					
					if(count == fileUpload.size()) {
						flag = true;
					}else{
						MailSendSrv.getInstance().sendMail(MsgErrorMail.ALERTA_TITLE_UPLOAD_TO_TBK_ONUS.toString(),
														   MsgErrorMail.ALERTA_FILES_NOT_UPLOADED.toString().replace("?", msj));
					}
				} else {
					LOG.info("Todos los archivos On Us para transferencia a TBK estan procesados");
					MailSendSrv.getInstance().sendMail(
							MsgErrorMail.ALERTA_TITLE_UPLOAD_TO_TBK_ONUS.toString(),
							MsgErrorMail.ALERTA_ALL_ALREADY_PROCESS_UPLOAD_TO_TBK_ONUS_TXT.toString());
				}
			} else {
				LOG.info("No existen archivos para subir");
				MailSendSrv.getInstance().sendMail(
						MsgErrorMail.ALERTA_TITLE_UPLOAD_TO_TBK_ONUS.toString(),
						MsgErrorMail.ALERTA_NOT_FILES_FOUND_UPLOAD_TO_TBK_ONUS.toString());
			}
		} else {
			LOG.info("Parametros de BD incompletos");
			MailSendSrv.getInstance().sendMail(
					MsgErrorMail.ALERTA_TITLE_UPLOAD_TO_TBK_ONUS.toString(),
					MsgErrorMail.ALERTA_PRTS_TXT.toString());
		}
		return flag;		
	}

	 
	/**
	 * 
	 * @param nombrefileDest
	 * @return
	 * @throws AppException
	 */
	private boolean updateUploadOnusTbk(final String nombrefileDest,String starWith, StatusProcessType status) throws AppException {
		CommonsDao daoCommons = null;
		TblCronLogDTO dto = null;
		boolean flag = false;
		try {
			dto = setCronOnusTbk(nombrefileDest,starWith,status);					
			LOG.info("nombre file dest "+nombrefileDest);

			daoCommons = CommonsDaoFactory.getInstance().getNewEntidadDao();
			if (dto != null && daoCommons.updateTblCronLogByType(dto, LogBD.TABLE_LOG_OUTGOING_ONUS.getTable(), FilterTypeSearch.FILTER_BY_DATE)) {
				daoCommons.endTx();
				flag = true;
			} else {
				LOG.info("No fue posible actualizar en la base de datos el archivo ".concat(nombrefileDest));
				rollBackTransaccion(daoCommons);
				MailSendSrv.getInstance().sendMail(
						MsgErrorSQL.ERROR_UPDATE_ONUS.toString(),
						MsgErrorSQL.ERROR_UPDATE_UPLOAD_TXT.toString());
			}
			return flag;
		} catch (SQLException e){
			rollBackTransaccion(daoCommons);
			throw new AppException(MsgErrorSQL.ERROR_QUERY_PROCESS_CLOSE_UPLOAD.toString(), e);
		} finally {
			try {
				if (daoCommons != null) {
					daoCommons.close();
				} 
			} catch (SQLException e) {
					rollBackTransaccion(daoCommons);
					throw new AppException(MsgErrorSQL.ERROR_QUERY_PROCESS_UPLOAD.toString(), e);
				}
			}
		}
	
	
	/**
	 * 
	 * @param nombrefileDest
	 * @param starWith
	 * @return
	 */
	private TblCronLogDTO setCronOnusTbk(String nombrefileDest, String starWith, StatusProcessType status) {
		TblCronLogDTO dto = null;
		String fileAux = null;
		String dateAux = null;
		fileAux = nombrefileDest.replace(starWith, "");
		fileAux = fileAux.substring(0,8);
		dateAux = DateUtils.getDateFileTbk(fileAux);
		
		if (dateAux != null) {
			dto = new TblCronLogDTO();
			dto.setFecha(dateAux);
			dto.setFileFlag(status.getValue());
		}
		return dto;
	}

	
	/**
	 * 
	 * @param fileDirectory
	 * @return
	 * @throws AppException
	 */
	private List<String> getFileUpload(final List<String> fileDirectory, ParamUploadOnusTbkDTO parametros) throws AppException {
		
		String dateFile = null;
		List<String> fileCarga = null;
		CommonsDao daoCommons = null;
		String fileAux = null;
		TblCronLogDTO dto = null;
		TblCronLogDTO dtoAux = new TblCronLogDTO();
		try {
			fileCarga = new ArrayList<String>();
			daoCommons = CommonsDaoFactory.getInstance().getNewEntidadDao();
			for(String fileName : fileDirectory) {
				fileAux = fileName.replace(parametros.getNameOnusTbkFile(), "");
				
				dateFile = fileAux.substring(0,8);
				dateFile = DateUtils.getDateFileTbk(fileAux);
				if(dateFile != null) {
					dtoAux.setFecha(dateFile);
					dto = daoCommons.getTblCronLogByType(dtoAux, LogBD.TABLE_LOG_OUTGOING_ONUS.getTable(), FilterTypeSearch.FILTER_BY_DATE);
					if (dto != null && ConstantesUtil.ZERO.toString().equalsIgnoreCase(dto.getFileFlag())){
						fileCarga.add(fileName);
					}
				}
				else {
					LOG.info("Archivo no cumple con el formato para obtener la fecha :" + fileName);
					CommonsUtils.moveFile(parametros.getPathOnusTbkInc() + fileName, parametros.getPathError() + fileName);
				}
			}
			return fileCarga;
		}
		catch (SQLException e) {
			throw new AppException(MsgErrorSQL.ERROR_QUERY_PROCESS_CLOSE_UPLOAD.toString(), e);
		}
		finally {
			try {
				if (daoCommons != null) {
					daoCommons.close();
				}
			}
			catch (SQLException e) {
				throw new AppException(MsgErrorSQL.ERROR_QUERY_PROCESS_CLOSE_UPLOAD.toString(), e);
			}
		}
	}
	
	
	
	  
	/**
	 * 
	 * @return
	 * @throws AppException
	 */
	private ParamUploadOnusTbkDTO getParametrosUploadCron()  throws AppException {
		ParamUploadOnusTbkDTO pUpLoadDto = null;
        CommonsDao daoCommon = null;
        List<ParametroDTO> paramDTOList = null;
        try {
            daoCommon = CommonsDaoFactory.getInstance().getNewEntidadDao();
            paramDTOList = daoCommon
                    .getParametroCodGrupoDato( ConstantesPRTS.CRON_TBK
                            .toString() );
            if ( paramDTOList != null) {
        		// PARAMETROS DE FTP DE TRANSBANK
        		DataFTP dataFTP = new DataFTP();
        		dataFTP.setIpHost( CommonsUtils.getCodiDato( paramDTOList, ConstantesPRTS.SFTP_IP_UPLOAD_INCOMING_ONUS.toString()));
        		dataFTP.setPort( Integer.valueOf(CommonsUtils.getCodiDato( paramDTOList, ConstantesPRTS.SFTP_PORT_UPLOAD_INCOMING_ONUS.toString())));
        		dataFTP.setUser( CommonsUtils.getCodiDato( paramDTOList, ConstantesPRTS.SFTP_USER_UPLOAD_INCOMING_ONUS.toString()));
        		dataFTP.setPassword( CommonsUtils.getCodiDato( paramDTOList, ConstantesPRTS.SFTP_PASS_UPLOAD_INCOMING_ONUS.toString()));
                
        		// PARAMETROS DEL PROCESO
                pUpLoadDto = new ParamUploadOnusTbkDTO();
                
                // EXTENSION DEL ARCHIVO ONUS
                pUpLoadDto.setExtOnusTbkFile(CommonsUtils.getCodiDato( paramDTOList, ConstantesPRTS.FORMAT_EXTNAME_ONUS_OUT.toString()));
                
                // NOMBRE STARWITH DEL ARCHIVO ONUS
                pUpLoadDto.setNameOnusTbkFile(CommonsUtils.getCodiDato( paramDTOList, ConstantesPRTS.FORMAT_FILENAME_ONUS.toString()));
                
                // RUTA EN SERVIDOR sbpay DONDE SE BUSCARA EL ARCHIVO ONUS
                pUpLoadDto.setPathOnusTbkInc(CommonsUtils.getCodiDato( paramDTOList, ConstantesPRTS.PATH_ABC_ONUS_OUT.toString()));
                
                // RUTA EN SERVIDOR sbpay DONDE SE DEJARA EL ARCHIVO ONUS EN CASO DE SUBIR
                pUpLoadDto.setPathBackup(CommonsUtils.getCodiDato( paramDTOList, ConstantesPRTS.PATH_ABC_ONUS_OUT_BKP.toString()));
                
                // RUTA EN SERVIDOR sbpay DONDE SE DEJARA EL ARCHIVO ONUS EN CASO DE NO SUBIR
                pUpLoadDto.setPathError(CommonsUtils.getCodiDato( paramDTOList, ConstantesPRTS.PATH_ABC_ONUS_OUT_ERROR.toString()));
                
                // RUTA FTP DONDE SE DEJARA EL ARCHIVO ONUS AL SUBIR A TBK
                pUpLoadDto.setFtpPathIn(CommonsUtils.getCodiDato( paramDTOList, ConstantesPRTS.PATH_IN_UPLD_INCOMING_ONUS.toString()));  
                
                // PARAMETROS DE FTP
                pUpLoadDto.setDataFTP(dataFTP);
                LOG.info(pUpLoadDto);
            }
            else {
                pUpLoadDto = null;
            }
        }
        catch ( SQLException e ) {
            throw new AppException(
                    MsgErrorSQL.ERROR_QUERY_PRTS_UPLOAD_CAR_ABO.toString(), e );
        }
        finally {
            try {
                if ( daoCommon != null ) {
                    daoCommon.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException(
                        MsgErrorSQL.ERROR_QUERY_PRTS_UPLOAD_CLOSE_CAR_ABO.toString(), e );
            }
        }
        return pUpLoadDto;
    }
	
	
}
