package com.sbpay.sgi.cron.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.dao.BaseDao;
import com.sbpay.sgi.cron.dto.TmpExportContableDTO;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;

public class GenerarContableDaoJdbc extends BaseDao<GenerarContableDao>
		implements GenerarContableDao {
	
    /** VARIABLE PARA EL LOGER */
    private static final Logger LOGGER = Logger.getLogger( GenerarContableDaoJdbc.class );

    private static final String SP_SIG_OBTENER_INFO_CONTABLE = "{call SP_SIG_OBTENER_INFO_CONTABLE(?,?,?)}";

	private static final String SP_SIG_CONTABLE_MAIN = "{call SP_SIG_CONTABLE_MAIN(?,?,?)}";

	private static final String CALL_SP_SIG_CLEAN_TMP_CONTABLE =  "{call SP_SIG_CLEAN_TMP_CONTABLE(?,?)}";;


	public GenerarContableDaoJdbc(Connection connection) {
		super(connection);
	}

	@Override
	public List<TmpExportContableDTO> obtenerInformacionCnblt() throws SQLException {

        OracleCallableStatement callableStatement = null;
        List<TmpExportContableDTO> lstContableDTO = new ArrayList<TmpExportContableDTO>();
        TmpExportContableDTO paramExportContableDTO;
        try {
            
        	LOGGER.info("********* SE EJECUTA SP : SP_SIG_OBTENER_INFO_CONTABLE *******_");
            callableStatement = ( OracleCallableStatement ) this.getConnection().prepareCall( SP_SIG_OBTENER_INFO_CONTABLE );

			callableStatement.registerOutParameter("warning", OracleTypes.VARCHAR);
			callableStatement.registerOutParameter("cod_error", OracleTypes.NUMBER);
			callableStatement.registerOutParameter("prfCursor", OracleTypes.CURSOR);
            
            callableStatement.execute();
            
            ResultSet resultados = (ResultSet) ((callableStatement).getObject("prfCursor"));
			int codigoError = callableStatement.getInt("cod_error");
			
			if(codigoError==0)
				while (resultados.next()) {
					paramExportContableDTO = new TmpExportContableDTO();
					paramExportContableDTO.setTransaccion(resultados.getString("TRANSACCION"));
					paramExportContableDTO.setMovOrg(resultados.getString("MOV_ORG"));
					paramExportContableDTO.setProducto(resultados.getString("PRODUCTO"));
					paramExportContableDTO.setSucMov(resultados.getString("SUC_MOV"));
					paramExportContableDTO.setSucPro(resultados.getString("SUC_PRO"));
					paramExportContableDTO.setSucPago(resultados.getString("SUC_PAGO"));
					paramExportContableDTO.setComercio(resultados.getLong("COMERCIO"));
					paramExportContableDTO.setFecha(resultados.getString("FEC"));
					paramExportContableDTO.setIdentificador(resultados.getString("IDENTIFICADOR"));
					paramExportContableDTO.setCompania(resultados.getString("COMPANIA"));
					paramExportContableDTO.setRutEntidad(resultados.getString("RUT_ENTIDAD"));
					paramExportContableDTO.setMov(CommonsUtils
							.getStringNull(resultados.getString("MOVIMIENTO")));
					paramExportContableDTO.setMonto(resultados.getString("MONTO"));
					paramExportContableDTO.setPortafolio(resultados.getString("PORTAFOLIO"));
					paramExportContableDTO.setCuenta(resultados.getString("CUENTA"));
					paramExportContableDTO.setRutCliente(resultados.getString("RUTCLIENTE"));
					lstContableDTO.add(paramExportContableDTO);
				}
			else{
				LOGGER.error("Method: obtenerInformacionCnblt - Problemas al leer informacion contable desde la base de datos.");
			}
        }
        finally {
            if ( callableStatement != null ) {
                callableStatement.close();
            }
        }
		return lstContableDTO;
	}

	@Override
	public boolean cargarInformacionContable(Integer operador) throws SQLException {
		CallableStatement callableStatement = null;

		LOGGER.info("******SE EJECUTA STORE PROCEDURE SP_SIG_CONTABLE_MAIN*****");
		LOGGER.info("operador DAO: "+operador);

		try {
			callableStatement = this.getConnection().prepareCall(
					SP_SIG_CONTABLE_MAIN);

			callableStatement.registerOutParameter(1, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(2, java.sql.Types.NUMERIC);
			callableStatement.setInt(3, operador);


			callableStatement.execute();

			String warning = callableStatement.getString(1);
			Integer codigoError = callableStatement.getInt(2);
			LOGGER.info("WARNING : " + warning);
			LOGGER.info("codigoError : " + codigoError);
		
			if (codigoError != 0) {
				LOGGER.warn( "Cod error " + codigoError + ", mensaje:" + warning);
				return Boolean.FALSE;
			}
			return Boolean.TRUE;	
		} finally {

			if (callableStatement != null) {
				callableStatement.close();
			}

		}

	
	}

	@Override
	public void clearTmpContable() throws SQLException {
		CallableStatement callableStatement = null;
		LOGGER.info("******SE EJECUTA STORE PROCEDURE CALL SP SP_SIG_CLEAN_TMP_CONTABLE *****");
		try {
			callableStatement = this.getConnection().prepareCall(
					CALL_SP_SIG_CLEAN_TMP_CONTABLE);
			callableStatement.registerOutParameter(1, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(2, java.sql.Types.NUMERIC);		
			callableStatement.execute();
			String warning = callableStatement.getString(1);
			Integer codigoError = callableStatement.getInt(2);			
			LOGGER.info("WARNING : " + warning);
			LOGGER.info("codigoError : " + codigoError);
		} finally {

			if (callableStatement != null) {
				callableStatement.close();
			}

		}
		
	}
}
