package com.sbpay.sgi.cron.step;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.enums.MsgExitoMail;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.srv.upload.UploadCargoAbonoSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;

public class StepUploadCargoAbono {
    
    /** The Constant LOGGER. */
    private static final Logger LOG = Logger
            .getLogger( StepUploadCargoAbono.class );
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 7/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws Exception
     * @since 1.X
     */
    public void execute() throws Exception {
        try {
            LOG.info( "Inicio Upload Cargo y Abono IC : StepUploadCargoAbono " );
            if ( UploadCargoAbonoSrv.getInstance().uploadCargoAbono() ) {
                
                MailSendSrv
                        .getInstance()
                        .sendMailOk(
                                MsgExitoMail.EXITO_PROCESS_UPLOAD_TITLE_CAR_ABO
                                        .toString(),
                                MsgExitoMail.EXITO_PROCESS_UPLOAD_CAR_ABO
                                        .toString() );
                LOG.info( "Envio Correo de Upload  Satisfactoria: Proceso Upload Cargo y Abono IC" );
                
            }
            else {
                LOG.info( "Upload Cargo y Abono IC Ejecutado con observaciones Finaliza!" );
                
            }
        }
        catch ( AppException e ) {
            LOG.error( e.getMessage(), e );
            MailSendSrv.getInstance().sendMail(
                    MsgProceso.MSG_GENERIC_ERROR_MSJ.toString(),
                    ( e.getStackTraceDescripcion() != null ) ? e
                            .getStackTraceDescripcion() : e.getMessage() );
        }
    }
    
}
