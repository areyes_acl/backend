package com.sbpay.sgi.cron.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.sbpay.sgi.cron.dao.BaseDao;
import com.sbpay.sgi.cron.dto.CronLogDTO;
import com.sbpay.sgi.cron.dto.TblCronIcOutDTO;
import com.sbpay.sgi.cron.enums.ConstantesBD;

public class UploadDaoJdbc extends BaseDao<UploadDao> implements UploadDao {

	/**
	 * Constructor con conexion.
	 * 
	 * @param connection  Conexion JDBC.
	 */
	public UploadDaoJdbc(Connection connection) {
		super(connection);
	}

	@Override
	public CronLogDTO getCronLogByDateProcess(final String dateFile)
			throws SQLException {
		 PreparedStatement stmt = null;
	        ResultSet rs = null;
	        CronLogDTO cronLogDTO = null;
	        try {
	            StringBuilder str = new StringBuilder(
	                    "select FECHA,  INCOMING_NAME, INCOMING_FLAG, INCOMING_TS, ")
	            		.append(" OUTGOING_NAME, OUTGOING_FLAG, OUTGOING_TS  ")
	            		.append(" from ").append(ConstantesBD.ESQUEMA.toString())
	            		.append(".TBL_CRON_LOG where TRUNC(FECHA) = ")
	            		.append(" to_date(?,'DD/MM/YY') AND INCOMING_FLAG='1'")
	            		.append(" AND OUTGOING_NAME is NULL AND OUTGOING_FLAG")
	            		.append(" IS NULL ");
	            stmt = this.getConnection().prepareStatement(str.toString());
	            stmt.setString(1, dateFile);
	            rs = stmt.executeQuery();

	            if (rs.next()) {
	            	cronLogDTO = new CronLogDTO();
	            	cronLogDTO.setFecha(rs.getString("FECHA"));
	            	cronLogDTO.setIncomingName(rs.getString("INCOMING_NAME"));
	            	cronLogDTO.setIncomingFlag(rs.getString("INCOMING_FLAG"));
	            	cronLogDTO.setIncomingTs(rs.getString("INCOMING_TS"));
	            	cronLogDTO.setOutgoingName(rs.getString("OUTGOING_NAME"));
	            	cronLogDTO.setOutgoingFlag(rs.getString("OUTGOING_FLAG"));
	            	cronLogDTO.setOutgoingTs(rs.getString("OUTGOING_TS"));

	            }
	            return cronLogDTO;
	        } finally {
	            if (stmt != null) {
	                stmt.close();
	            }
	            if (rs != null) {
	                rs.close();
	            }
	        }
	}

	@Override
	public boolean updateCronLong(CronLogDTO dto) throws SQLException {
		PreparedStatement stmt = null;
        try {
            final StringBuilder str = new StringBuilder()
                    .append("UPDATE ").append(ConstantesBD.ESQUEMA.toString())
                    .append(".TBL_CRON_LOG SET OUTGOING_NAME = ?, ")
                    .append(" OUTGOING_FLAG = ?, OUTGOING_TS = SYSDATE ")
                    .append(" WHERE TRUNC(FECHA) = to_date(?,'DD/MM/RR') ");
            stmt = this.getConnection().prepareStatement(str.toString());
            setStringNull(dto.getOutgoingName(), 1, stmt);
            setStringNull(dto.getOutgoingFlag(), 2, stmt);
            setStringNull(dto.getFecha(), 3, stmt);
            return (stmt.executeUpdate() > 0);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
	}

	@Override
	public TblCronIcOutDTO getCronLogIcByDateProcess(final String dateFile)
			throws SQLException {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		TblCronIcOutDTO dto = null;
		try {
			StringBuilder str = new StringBuilder(
					"select FECHA,  FILE_FLAG, FILE_NAME, FILE_TS, SID ")
					.append(" from ").append(ConstantesBD.ESQUEMA.toString())
					.append(".TBL_CRON_IC_OUT where TRUNC(FECHA) = ")
					.append(" to_date(?,'DD/MM/YY') AND FILE_FLAG='0'");
			stmt = this.getConnection().prepareStatement(str.toString());
			stmt.setString(1, dateFile);
			rs = stmt.executeQuery();

			if (rs.next()) {
				dto = new TblCronIcOutDTO();
				dto.setFecha(rs.getString("FECHA"));
				dto.setFileFlag(rs.getString("FILE_FLAG"));
				dto.setFileName(rs.getString("FILE_NAME"));
				dto.setFileTs(rs.getString("FILE_TS"));
				dto.setSid(rs.getLong("SID"));
			}
			return dto;
		} finally {
			if (stmt != null) {
				stmt.close();
			}
			if (rs != null) {
				rs.close();
			}
		}
	}

	@Override
	public boolean updateCronIcOut(final TblCronIcOutDTO dto) throws SQLException {
		PreparedStatement stmt = null;
        try {
            final StringBuilder str = new StringBuilder()
                    .append("UPDATE ").append(ConstantesBD.ESQUEMA.toString())
                    .append(".TBL_CRON_IC_OUT SET FILE_NAME = ?, ")
                    .append(" FILE_FLAG = ?, OUTGOING_TS = SYSDATE ")
                    .append(" WHERE TRUNC(FECHA) = to_date(?,'DD/MM/RR') ")
                    .append(" AND FILE_FLAG = '0' ");
            stmt = this.getConnection().prepareStatement(str.toString());
            setStringNull(dto.getFileName(), 1, stmt);
            setStringNull(dto.getFileFlag(), 2, stmt);
            setStringNull(dto.getFecha(), 3, stmt);
            return (stmt.executeUpdate() > 0);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
	}


}