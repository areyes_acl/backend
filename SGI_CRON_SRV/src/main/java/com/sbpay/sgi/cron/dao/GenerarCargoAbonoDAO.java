package com.sbpay.sgi.cron.dao;

import java.sql.SQLException;
import java.util.List;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.Dao;
import com.sbpay.sgi.cron.dto.TmpSalidaCargoAbonoDTO;


/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 9/12/2015, (ACL) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SA.</B>
 */
public interface GenerarCargoAbonoDAO extends Dao {
    
	public List<TmpSalidaCargoAbonoDTO> loadCargoAbono() throws AppException;

	public boolean callSPCargaSalidaCargoAbono() throws AppException;

	public boolean callSPModEstSalidaCargoAbono() throws AppException;

	public boolean setTblCronIc(final String cargoAbonoFilename)
			throws SQLException;
    
}
