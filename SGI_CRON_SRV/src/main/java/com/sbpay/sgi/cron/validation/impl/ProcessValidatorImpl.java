package com.sbpay.sgi.cron.validation.impl;

import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dto.CronConfigDTO;
import com.sbpay.sgi.cron.dto.ParamCargoAbonoDTO;
import com.sbpay.sgi.cron.dto.ParamCronAvancesDTO;
import com.sbpay.sgi.cron.dto.ParamCronBiceDTO;
import com.sbpay.sgi.cron.dto.ParamCronDTO;
import com.sbpay.sgi.cron.dto.ParamDownloadBOLDTO;
import com.sbpay.sgi.cron.dto.ParamDownloadComisionesDTO;
import com.sbpay.sgi.cron.dto.ParamDownloadLogAutorizacionDTO;
import com.sbpay.sgi.cron.dto.ParamDownloadOnusDTO;
import com.sbpay.sgi.cron.dto.ParamDownloadVisaDTO;
import com.sbpay.sgi.cron.dto.ParamDownloadVisaNacDTO;
import com.sbpay.sgi.cron.dto.ParamExportCnblDTO;
import com.sbpay.sgi.cron.dto.ParamExportDetContDTO;
import com.sbpay.sgi.cron.dto.ParamExportItzAvanceDTO;
import com.sbpay.sgi.cron.dto.ParamExportReCnblDTO;
import com.sbpay.sgi.cron.dto.ParamProcessLogAutorizacion;
import com.sbpay.sgi.cron.dto.ParamRechazoDTO;
import com.sbpay.sgi.cron.dto.ParamUploadBolDTO;
import com.sbpay.sgi.cron.dto.ParamUploadCargoAbonoDTO;
import com.sbpay.sgi.cron.dto.ParamUploadCntbleDTO;
import com.sbpay.sgi.cron.dto.ParamUploadIncomingDTO;
import com.sbpay.sgi.cron.dto.ParamUploadIncomingVisaDTO;
import com.sbpay.sgi.cron.dto.ParamUploadOnusTbkDTO;
import com.sbpay.sgi.cron.dto.ParamUploadOutgoingICDTO;
import com.sbpay.sgi.cron.dto.ParamUploadOutgoingVisaNacICDTO;
import com.sbpay.sgi.cron.dto.ParamUploadVisaDTO;
import com.sbpay.sgi.cron.dto.ParamsDownloadRechazosDTO;
import com.sbpay.sgi.cron.dto.ParamsFileReaderProcessDTO;
import com.sbpay.sgi.cron.dto.ParamsFileReaderProcessOutVisaIntDTO;
import com.sbpay.sgi.cron.dto.ParamsFileReaderProcessOutVisaNacDTO;
import com.sbpay.sgi.cron.dto.ParamsGenerateComisionesDTO;
import com.sbpay.sgi.cron.dto.ParamsGenerateIncomingDTO;
import com.sbpay.sgi.cron.dto.ParamsGenerateIncomingOnUsDTO;
import com.sbpay.sgi.cron.dto.ParamsGenerateIncomingVisaDTO;
import com.sbpay.sgi.cron.dto.ParamsProcessAvancesDTO;
import com.sbpay.sgi.cron.dto.ParamsProcessBiceDTO;
import com.sbpay.sgi.cron.dto.ParamsProcessOnusDTO;
import com.sbpay.sgi.cron.dto.ParamsProcessOutVisaDTO;
import com.sbpay.sgi.cron.dto.ParamsReadCPagoDTO;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.validation.ProcessValidator;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class ProcessValidatorImpl implements ProcessValidator {
	private static final Logger LOGGER = Logger
			.getLogger(ProcessValidatorImpl.class);

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que verifica si un proceso del cron se encuentra activado por base
	 * de datos o se encuantra desactivado
	 * 
	 * @return
	 * @throws AppException
	 * @since 1.X
	 */
	@Override
	public Boolean isActiveProcess(ProcessCodeType proceso) throws AppException {
		Boolean activo = Boolean.FALSE;
		CommonsDao common = null;

		try {
			common = CommonsDaoFactory.getInstance().getNewEntidadDao();
			CronConfigDTO config = common.isProcessActive(proceso);

			if (config == null) {
				throw new AppException(
						"No se ha podido comprobar si el proceso : "
								+ proceso.getCodigoProceso() + " está activo");
			}

			if (config.getEstado() == 1) {
				activo = Boolean.TRUE;
				LOGGER.info("Proceso: " + proceso.getCodigoProceso()
						+ ", está activo. "
						+ MsgProceso.MSG_CAMBIAR_CONFIG.toString());
			} else {
				activo = Boolean.FALSE;
				LOGGER.warn("Proceso: " + proceso.getCodigoProceso()
						+ ", no está activo."
						+ MsgProceso.MSG_CAMBIAR_CONFIG.toString());
			}
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
		} finally {

			try {
				if (common != null) {
					common.close();
				}
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
			}

		}
		return activo;

	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que valida que hayan encontrado todos los paramsetros necesarios
	 * para el proceso
	 * 
	 *
	 * @param listaParametros
	 * @return
	 * @since 1.X
	 */
	public Boolean hasTheValidParametersForTheProcess(ProcessCodeType proceso,
			Object params) {

		Boolean isValid = Boolean.FALSE;

		switch (proceso) {

		case DESCARGAR_OUTGOING:
			isValid = validaDescargaOutgoing(params);
			break;	

		case DESCARGAR_COMISIONES:
			isValid = validaParametrosDescargaComisiones(params);
			break;

		case DESCARGAR_COMPENSACION_DE_PAGOS:
			isValid = validaParametrosCompensacionPago(params);
			break;

		case DESCARGAR_OUTGOING_VISA:
			isValid = validarParametrosDescargOutgoingVisa(params);
			break;

		case SUBIDA_OUTGOING_A_IC:
			isValid = validaParametrosSubidaOutgoingIc(params);
			break;
			
		case SUBIDA_OUTGOING_VISA_A_IC:
			isValid = validarParametroSubirIncomingVisaAIc(params);
			break;

		case DESCARGAR_RECHAZOS:
			isValid = validaParametrosDescargaRechazos(params);
			break;

		case DESCARGAR_ONUS:
			isValid = validaParametrosDescargaOnus(params);
			break;

		case DESCARGAR_LOG_DE_TRANSACCIONES_AUTORIZADAS:
			isValid = validarParametrosDescargaLogTrxAutorizadas(params);
			break;

		case PROCESAR_LOG_DE_TRANSACCIONES_AUTORIZADAS:
			isValid = validarParametrosProcesarLogTrxAutorizadas(params);
			break;

		case PROCESAR_OUTGOING:
			isValid = validarParametrosProcesamientoDeOutgoing(params);
			break;

		case PROCESAR_OUGOING_VISA:
			isValid = ValidarParametrosProcesoDeOutgoingVisa(params);
			break;

		case PROCESAR_RECHAZOS:
			isValid = validarParametrosProcesamientoRechazo(params);
			break;

		case PROCESAR_ONUS:
			isValid = validaParametrosProcesoOnus(params);
			break;

		case PROCESAR_COMISIONES:
			isValid = validaParametrosProcesarComisiones(params);
			break;

		case PROCESAR_COMPENSACION_DE_PAGOS:
			isValid = validarParametrosProcesoCPago(params);
			break;

		case GENERAR_INCOMING:
			isValid = validarParametrosProcesoGenerarIncomingTbk(params);
			break;
			
		case GENERAR_INCOMING_ONUS:
			isValid = validarParametrosGeneracionIncomingONUS(params);
			break;

		case GENERAR_INCOMING_VISA_NAC:
			isValid = validarParametrosProcesoGenerarIncomingVisa(params);
			break;

		case GENERAR_INCOMING_VISA_INTERNACIONAL:
			isValid = validarParametrosProcesoGenerarIncomingVisa(params);
			break;
			
		case GENERAR_CARGOS_Y_ABONOS:
			isValid = validaParametrosGeneracionSolicitud60(params);
			break;

		case SUBIDA_INCOMING_A_TRANSBANK:
			isValid = validaParametrosSubidaIncoming(params);
			break;

		case SUBIDA_ONUS_A_TRANSBANK:
			isValid = validarParametrosProcesoSubidaOnus(params);
			break;
			
		case SUBIDA_INCOMING_VISA_NAC:
			isValid = validaParametrosSubirVisa(params);
			break;
			
		case SUBIDA_INCOMING_VISA_INTERNACIONAL:
			isValid = validaParametrosSubirVisa(params);
			break;

		case SUBIDA_CARGOS_Y_ABONOS:
			isValid = validaParametrosSolicitud60(params);
			break;

		case GENERAR_ARCHIVO_CONTABLE:
			isValid = validaParametrosGeneracionContable(params);
			break;

		case SUBIDA_ARCHIVO_CONTABLE:
			isValid = validaParametrosSubidaArchivoContable(params);
			break;

		case SUBIDA_ARCHIVO_BOL:
			isValid = validaparametrosSubidaBol(params);
			break;

		case DESCARGAR_ARCHIVO_BOL:
			isValid = validaDescargaBol(params);
			break;

		case DESCARGAR_AVANCES_BICE:
			isValid = validaDescargaAvancesBice(params);
			break;

		case PROCESAR_BICE:
			isValid = validaParametrosProcesoBice(params);
			break;

		case DESCARGAR_AVANCES:
			isValid = validaDescargaAvances(params);
			break;

		case PROCESAR_AVANCES:
			isValid = validaParametrosProcesoAvances(params);
			break;

		case GENERAR_ARCHIVO_CONTABLE_AVA:
			isValid = validaParametrosGeneracionContableAvance(params);
			break;

		case SUBIDA_ARCHIVO_CONTABLE_AVA:
			isValid = validaParametrosSubidaArchivoContableAvance(params);
			break;

		case GENERAR_ARCHIVO_AVA:
			isValid = validaParametrosGeneracionAvance(params);
			break;

		case REGENERAR_ARCHIVO_CONTABLE_AVA:
			isValid = validaParametrosRegeneracionContableAvance(params);
			break;

		case RESUBIDA_ARCHIVO_CONTABLE_AVA:
			isValid = validaParametrosResubidaArchivoContableAvance(params);
			break;
		
		case GENERAR_REPORTE_ARCHIVO_CONTABLE:
			isValid = validaParametrosGenerarReporteArchivoContable(params);
			break;
		case DESCARGAR_OUTGOING_VISA_NAC:
			isValid = validarParametrosDescargOutgoingVisaNacional(params);
			break;
			
		case SUBIDA_OUTGOING_VISA_NAC_A_IC:
			isValid = validarParametroSubirIncomingVisaNacAIc(params);
			break;
			
		case PROCESAR_OUTGOING_VISA_NAC:
			isValid = validarParametrosProcesamientoDeOutgoingVisaNac(params);
			break;

		case PROCESAR_OUTGOING_VISA_INT:
			isValid = validarParametrosProcesamientoDeOutgoingVisaInt(params);
			break;
			
		default:
			isValid = Boolean.FALSE;
			break;

		}

		return isValid;

	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * </ul>
	 * <p>
	 * Metodo que valida que hayan encontrado todos los parametros necesarios
	 * para el proceso
	 * 
	 * SFTP_IP_DOWNLOAD_AVANCES,SFTP_PORT_DOWNLOAD_AVANCES,
	 * SFTP_USER_DOWNLOAD_AVANCES,SFTP_PASS_DOWNLOAD_AVANCES
	 * PATH_OUT_DWNLD_AVANCES
	 * ,FORMAT_FILENAME_AVANCES,FORMAT_FILENAME_CORRELATIVO_AVANCES
	 * ,PATH_ACL_AVANCES
	 * 
	 * @param listaParametros
	 * @return
	 * @since 1.X
	 */
	private boolean validaDescargaAvances(Object params) {

		ParamCronAvancesDTO parametros = (ParamCronAvancesDTO) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {

			// VALIDA PARAMETROS FTP NULO
			if (parametros.getDataFTP() == null) {
				return false;
			} else {
				// VALIDA PARAMETROS FTP NULOS
				if (parametros.getDataFTP().getIpHost() == null
						|| parametros.getDataFTP().getPort() == null
						|| parametros.getDataFTP().getUser() == null
						|| parametros.getDataFTP().getPassword() == null) {
					return false;
				} else {
					// VALIDA PARAMETROS FTP VACIOS
					if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
							parametros.getDataFTP().getIpHost())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											String.valueOf(parametros
													.getDataFTP().getPort()))
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP().getUser())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP()
													.getPassword())) {
						return false;
					}
				}
			}

			// VALIDA PARAMETROS PROCESO NULOS
			if (parametros.getFormatFilenameAvances() == null
					|| parametros.getFtpPathAvances() == null
					|| parametros.getPathAclAvances() == null) {
				return false;
			} else {

				// VALIDA PARAMETROS PROCESO VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getFormatFilenameAvances())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFtpPathAvances())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathAclAvances())) {
					return false;
				}

			}
		}
		return true;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 DD/08/2018, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que valida que hayan encontrado todos los parametros necesarios
	 * para el proceso
	 * 
	 * SFTP_IP_DOWNLOAD_AVA_BICE, SFTP_PORT_DOWNLOAD_AVA_BICE,
	 * SFTP_USER_DOWNLOAD_AVA_BICE, SFTP_PASS_DOWNLOAD_AVA_BICE,
	 * PATH_OUT_DWNLD_AVA_BICE, PATH_ACL_AVA_BICE, FORMAT_FILENAME_AVA_BICE
	 * FORMAT_EXTNAME_AVA_BICE,
	 * 
	 * @param listaParametros
	 * @return
	 * @since 1.X
	 */
	private boolean validaDescargaAvancesBice(Object params) {

		ParamCronBiceDTO parametros = (ParamCronBiceDTO) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {

			// VALIDA PARAMETROS FTP NULO
			if (parametros.getDataFTP() == null) {
				return false;
			} else {
				// VALIDA PARAMETROS FTP NULOS
				if (parametros.getDataFTP().getIpHost() == null
						|| parametros.getDataFTP().getPort() == null
						|| parametros.getDataFTP().getUser() == null
						|| parametros.getDataFTP().getPassword() == null) {
					return false;
				} else {
					// VALIDA PARAMETROS FTP VACIOS
					if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
							parametros.getDataFTP().getIpHost())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											String.valueOf(parametros
													.getDataFTP().getPort()))
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP().getUser())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP()
													.getPassword())) {
						return false;
					}
				}
			}

			// VALIDA PARAMETROS PROCESO NULOS
			if (parametros.getFormatFilenameBice() == null
					|| parametros.getFtpPathBice() == null
					|| parametros.getPathAclBice() == null) {
				return false;
			} else {

				// VALIDA PARAMETROS PROCESO VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getFormatFilenameBice())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFtpPathBice())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathAclBice())) {
					return false;
				}

			}
		}
		return true;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @param params
	 * @return
	 * @since 1.X
	 */
	private Boolean validaParametrosSolicitud60(Object params) {

		ParamUploadCargoAbonoDTO parametros = (ParamUploadCargoAbonoDTO) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {

			// VALIDA PARAMETROS FTP NULO
			if (parametros.getDataFTP() == null) {
				return false;
			} else {
				// VALIDA PARAMETROS FTP NULOS
				if (parametros.getDataFTP().getIpHost() == null
						|| parametros.getDataFTP().getPort() == null
						|| parametros.getDataFTP().getUser() == null
						|| parametros.getDataFTP().getPassword() == null) {
					return false;
				} else {
					// VALIDA PARAMETROS FTP VACIOS
					if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
							parametros.getDataFTP().getIpHost())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											String.valueOf(parametros
													.getDataFTP().getPort()))
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP().getUser())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP()
													.getPassword())) {
						return false;
					}
				}
			}

			// VALIDA PARAMETROS PROCESO NULOS
			if (parametros.getFormatExtNameCarAbo() == null
					|| parametros.getFormatFilenameCarAbo() == null
					|| parametros.getPathCarAbo() == null
					|| parametros.getPathCarAboBkp() == null
					|| parametros.getPathCarAboError() == null
					|| parametros.getPathSFTPEntrada() == null) {
				return false;
			} else {

				// VALIDA PARAMETROS PROCESO VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getFormatExtNameCarAbo())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatFilenameCarAbo())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathCarAbo())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathCarAboBkp())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathCarAboError())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathSFTPEntrada())) {
					return false;
				}

			}
		}
		return true;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo para realizar la validacion de los parametros del archivo BOL
	 * 
	 * @param params
	 * @since 1.X
	 */
	private Boolean validaDescargaBol(Object params) {
		ParamDownloadBOLDTO parametros = (ParamDownloadBOLDTO) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {

			// VALIDA PARAMETROS FTP NULO
			if (parametros.getDataFTP() == null) {
				return false;
			} else {
				// VALIDA PARAMETROS FTP NULOS
				if (parametros.getDataFTP().getIpHost() == null
						|| parametros.getDataFTP().getPort() == null
						|| parametros.getDataFTP().getUser() == null
						|| parametros.getDataFTP().getPassword() == null) {
					return false;
				} else {
					// VALIDA PARAMETROS FTP VACIOS
					if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
							parametros.getDataFTP().getIpHost())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											String.valueOf(parametros
													.getDataFTP().getPort()))
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP().getUser())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP()
													.getPassword())) {
						return false;
					}
				}
			}

			// VALIDA PARAMETROS PROCESO NULOS
			if (parametros.getFormatExtNameBol() == null
					|| parametros.getFormatExtNameBolCtr() == null
					|| parametros.getFormatFileNameBol() == null
					|| parametros.getPathBol() == null
					|| parametros.getPathServerFTP() == null) {
				return false;
			} else {

				// VALIDA PARAMETROS PROCESO VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getFormatExtNameBol())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatExtNameBolCtr())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatFileNameBol())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathBol())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathServerFTP())) {
					return false;
				}

			}
		}
		return true;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que valida que hayan encontrado todos los parametros necesarios
	 * para el proceso
	 * 
	 * FTP_IP, FTP_PORT, FTP_USER, FTP_PASS, FTP_PATH_OUT, PATH_ACL_INC,
	 * FORMAT_FILENAME_OUT FORMAT_EXTNAME_OUT, FORMAT_EXTNAME_OUT_CTR
	 * 
	 * @param listaParametros
	 * @return
	 * @since 1.X
	 */
	private boolean validaDescargaOutgoing(Object params) {

		ParamCronDTO parametros = (ParamCronDTO) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {

			// VALIDA PARAMETROS FTP NULO
			if (parametros.getDataFTP() == null) {
				return false;
			} else {
				// VALIDA PARAMETROS FTP NULOS
				if (parametros.getDataFTP().getIpHost() == null
						|| parametros.getDataFTP().getPort() == null
						|| parametros.getDataFTP().getUser() == null
						|| parametros.getDataFTP().getPassword() == null) {
					return false;
				} else {
					// VALIDA PARAMETROS FTP VACIOS
					if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
							parametros.getDataFTP().getIpHost())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											String.valueOf(parametros
													.getDataFTP().getPort()))
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP().getUser())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP()
													.getPassword())) {
						return false;
					}
				}
			}

			// VALIDA PARAMETROS PROCESO NULOS
			if (parametros.getFormatExtNameOut() == null
					|| parametros.getFormatExtNameOutCtr() == null
					|| parametros.getFormatFilenameOut() == null
					|| parametros.getFtpPathOut() == null
					|| parametros.getPathAclInc() == null) {
				return false;
			} else {

				// VALIDA PARAMETROS PROCESO VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getFormatExtNameOut())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatExtNameOutCtr())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatFilenameOut())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFtpPathOut())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathAclInc())) {
					return false;
				}

			}
		}
		return true;
	}
		

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que valida que hayan encontrado todos los parametros necesarios
	 * para el proceso
	 * 
	 * FTP_IP, FTP_PORT, FTP_USER, FTP_PASS, FTP_PATH_TBK_COM, PATH_TBK_COM
	 * ,FORMAT_FILE_TBK_COM, FORMAT_EXTNAME_TBK_COM, FORMAT_FILE_TBK_COM_CTR,
	 * FORMAT_EXTNAME_TBK_COM_CTR
	 * 
	 * @param listaParametros
	 * @return
	 * @since 1.X
	 */
	private boolean validaParametrosDescargaComisiones(Object params) {

		ParamDownloadComisionesDTO parametros = (ParamDownloadComisionesDTO) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {

			// VALIDA PARAMETROS FTP NULO
			if (parametros.getDataFTP() == null) {
				return false;
			} else {
				// VALIDA PARAMETROS FTP NULOS
				if (parametros.getDataFTP().getIpHost() == null
						|| parametros.getDataFTP().getPort() == null
						|| parametros.getDataFTP().getUser() == null
						|| parametros.getDataFTP().getPassword() == null) {
					return false;
				} else {
					// VALIDA PARAMETROS FTP VACIOS
					if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
							parametros.getDataFTP().getIpHost())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											String.valueOf(parametros
													.getDataFTP().getPort()))
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP().getUser())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP()
													.getPassword())) {
						return false;
					}
				}
			}

			// VALIDA PARAMETROS PROCESO NULOS
			if (parametros.getFormatExtNameControlTbkComision() == null
					|| parametros.getFormatExtNameTbkComision() == null
					|| parametros.getFormatFileControlTbkComision() == null
					|| parametros.getFormatFileTbkComision() == null
					|| parametros.getPathFtpTbkComision() == null
					|| parametros.getPathTbkComision() == null) {
				return false;
			} else {

				// VALIDA PARAMETROS PROCESO VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getFormatExtNameControlTbkComision())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatExtNameTbkComision())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatFileControlTbkComision())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatFileTbkComision())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathFtpTbkComision())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathTbkComision())) {
					return false;
				}

			}
		}
		return true;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que valida que hayan encontrado todos los parametros necesarios
	 * para el proceso
	 * 
	 * FTP_IP, FTP_PORT, FTP_USER, FTP_PASS, FTP_PATH_OUT, PATH_CPAGO,
	 * FORMAT_EXTNAME_CPAGO FORMAT_FILENAME_CPAGO
	 * 
	 * @param listaParametros
	 * @return
	 * @since 1.X
	 */
	private boolean validaParametrosCompensacionPago(Object params) {

		ParamsReadCPagoDTO parametros = (ParamsReadCPagoDTO) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {

			// VALIDA PARAMETROS FTP NULO
			if (parametros.getDataFTP() == null) {
				return false;
			} else {
				// VALIDA PARAMETROS FTP NULOS
				if (parametros.getDataFTP().getIpHost() == null
						|| parametros.getDataFTP().getPort() == null
						|| parametros.getDataFTP().getUser() == null
						|| parametros.getDataFTP().getPassword() == null) {
					return false;
				} else {
					// VALIDA PARAMETROS FTP VACIOS
					if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
							parametros.getDataFTP().getIpHost())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											String.valueOf(parametros
													.getDataFTP().getPort()))
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP().getUser())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP()
													.getPassword())) {
						return false;
					}
				}
			}

			// VALIDA PARAMETROS PROCESO NULOS
			if (parametros.getFileNameCPago() == null
					|| parametros.getPathCPAGO() == null
					|| parametros.getPathFTPPathCPago() == null) {
				return false;
			} else {

				// VALIDA PARAMETROS PROCESO VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getFileNameCPago())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathCPAGO())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathFTPPathCPago())) {
					return false;
				}

			}
		}
		return true;
	}
	
	

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que valida que hayan encontrado todos los parametros necesarios
	 * para el proceso
	 * 
	 *
	 * 
	 * @param listaParametros
	 * @return
	 * @since 1.X
	 */
	private boolean validaParametrosSubidaOutgoingIc(Object params) {

		ParamUploadOutgoingICDTO parametros = (ParamUploadOutgoingICDTO) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {

			// VALIDA PARAMETROS FTP NULO
			if (parametros.getDataFTP() == null) {
				return false;
			} else {
				// VALIDA PARAMETROS FTP NULOS
				if (parametros.getDataFTP().getIpHost() == null
						|| parametros.getDataFTP().getPort() == null
						|| parametros.getDataFTP().getUser() == null
						|| parametros.getDataFTP().getPassword() == null) {
					return false;
				} else {
					// VALIDA PARAMETROS FTP VACIOS
					if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
							parametros.getDataFTP().getIpHost())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											String.valueOf(parametros
													.getDataFTP().getPort()))
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP().getUser())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP()
													.getPassword())) {
						return false;
					}
				}
			}

			// VALIDA PARAMETROS PROCESO NULOS
			if (parametros.getFormatExtNameOut() == null
					|| parametros.getFormatExtNameOutCtr() == null
					|| parametros.getFormatFileNameOut() == null
					|| parametros.getPathAclInc() == null
					|| parametros.getPathFtpIcEntrada() == null) {
				return false;
			} else {

				// VALIDA PARAMETROS PROCESO VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getFormatExtNameOut())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatExtNameOutCtr())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatFileNameOut())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathAclInc())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathFtpIcEntrada())) {
					return false;
				}

			}
		}
		return true;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que valida que hayan encontrado todos los parametros necesarios
	 * para el proceso
	 * 
	 * FTP_IP, FTP_PORT, FTP_USER, FTP_PASS, FTP_PATH_OUT, PATH_ACL_INC,
	 * FORMAT_FILENAME_OUT FORMAT_EXTNAME_OUT, FORMAT_EXTNAME_OUT_CTR
	 * 
	 * @param listaParametros
	 * @return
	 * @since 1.X
	 */
	private boolean validaParametrosDescargaRechazos(Object params) {

		ParamsDownloadRechazosDTO parametros = (ParamsDownloadRechazosDTO) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {

			// VALIDA PARAMETROS FTP NULO
			if (parametros.getDataFTP() == null) {
				return false;
			} else {
				// VALIDA PARAMETROS FTP NULOS
				if (parametros.getDataFTP().getIpHost() == null
						|| parametros.getDataFTP().getPort() == null
						|| parametros.getDataFTP().getUser() == null
						|| parametros.getDataFTP().getPassword() == null) {
					return false;
				} else {
					// VALIDA PARAMETROS FTP VACIOS
					if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
							parametros.getDataFTP().getIpHost())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											String.valueOf(parametros
													.getDataFTP().getPort()))
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP().getUser())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP()
													.getPassword())) {
						return false;
					}
				}
			}

			// VALIDA PARAMETROS PROCESO NULOS
			if (parametros.getFormatExtNameRch() == null
					|| parametros.getFormatFileAbcRch() == null
					|| parametros.getPathAbcRch() == null
					|| parametros.getPathFtpIcSalida() == null) {
				return false;
			} else {

				// VALIDA PARAMETROS PROCESO VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getFormatExtNameRch())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatFileAbcRch())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathAbcRch())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathFtpIcSalida())) {
					return false;
				}

			}
		}
		return true;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que valida que hayan encontrado todos los parametros necesarios
	 * para el proceso
	 * 
	 * FTP_IP_IC FTP_PORT_IC FTP_USER_IC FTP_PASS_IC PATH_FTP_IC_SALIDA
	 * FORMAT_FILENAME_ONUS FORMAT_EXTNAME_ONUS PATH_ABC_ONUS
	 * 
	 * @param listaParametros
	 * @return
	 * @since 1.X
	 */
	private boolean validaParametrosDescargaOnus(Object params) {

		ParamDownloadOnusDTO parametros = (ParamDownloadOnusDTO) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {

			// VALIDA PARAMETROS FTP NULO
			if (parametros.getDataFTP() == null) {
				return false;
			} else {
				// VALIDA PARAMETROS FTP NULOS
				if (parametros.getDataFTP().getIpHost() == null
						|| parametros.getDataFTP().getPort() == null
						|| parametros.getDataFTP().getUser() == null
						|| parametros.getDataFTP().getPassword() == null) {
					return false;
				} else {
					// VALIDA PARAMETROS FTP VACIOS
					if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
							parametros.getDataFTP().getIpHost())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											String.valueOf(parametros
													.getDataFTP().getPort()))
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP().getUser())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP()
													.getPassword())) {
						return false;
					}
				}
			}

			// VALIDA PARAMETROS PROCESO NULOS
			if (parametros.getFormatFileNameOnus() == null
					|| parametros.getFormatExtNameOnus() == null
					|| parametros.getPathAbcOnUs() == null
					|| parametros.getPathFtpIcSalida() == null) {
				return false;
			} else {

				// VALIDA PARAMETROS PROCESO VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getFormatFileNameOnus())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatExtNameOnus())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathAbcOnUs())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathFtpIcSalida())) {
					return false;
				}

			}
		}
		return true;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que valida que hayan encontrado todos los parametros necesarios
	 * para el proceso
	 * 
	 * FTP_IP_IC FTP_PORT_IC FTP_USER_IC FTP_PASS_IC PATH_FTP_IC_SALIDA
	 * FORMAT_FILE_ABC_LOG FORMAT_EXTNAME_ABC_LOG PATH_ABC_LOG_TRX
	 * 
	 * @param listaParametros
	 * @return
	 * @since 1.X
	 */
	private boolean validarParametrosDescargaLogTrxAutorizadas(Object params) {
		ParamDownloadLogAutorizacionDTO parametros = (ParamDownloadLogAutorizacionDTO) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {

			// VALIDA PARAMETROS FTP NULO
			if (parametros.getDataFTP() == null) {
				return false;
			} else {
				// VALIDA PARAMETROS FTP NULOS
				if (parametros.getDataFTP().getIpHost() == null
						|| parametros.getDataFTP().getPort() == null
						|| parametros.getDataFTP().getUser() == null
						|| parametros.getDataFTP().getPassword() == null) {
					return false;
				} else {
					// VALIDA PARAMETROS FTP VACIOS
					if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
							parametros.getDataFTP().getIpHost())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											String.valueOf(parametros
													.getDataFTP().getPort()))
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP().getUser())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP()
													.getPassword())) {
						return false;
					}
				}
			}

			// VALIDA PARAMETROS PROCESO NULOS
			if (parametros.getFormatFileAbcLog() == null
					|| parametros.getFormatExtNameAbcLog() == null
					|| parametros.getPathAbcLogTrx() == null
					|| parametros.getPathFtpIcSalida() == null) {
				return false;
			} else {

				// VALIDA PARAMETROS PROCESO VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getFormatFileAbcLog())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatExtNameAbcLog())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathAbcLogTrx())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathFtpIcSalida())) {
					return false;
				}

			}
		}
		return true;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que valida que hayan encontrado todos los parametros necesarios
	 * para el proceso
	 * 
	 * FORMAT_FILE_ABC_LOG FORMAT_EXTNAME_ABC_LOG PATH_ABC_LOG_TRX
	 * PATH_ABC_LOG_TRX_BKP PATH_ABC_LOG_TRX_ERROR
	 * 
	 * @param listaParametros
	 * @return
	 * @since 1.X
	 */
	private boolean validarParametrosProcesarLogTrxAutorizadas(Object params) {

		ParamProcessLogAutorizacion parametros = (ParamProcessLogAutorizacion) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {

			// VALIDA PARAMETROS PROCESO NULOS
			if (parametros.getStarWith() == null
					|| parametros.getEndsWith() == null
					|| parametros.getPathAbcLogTrx() == null
					|| parametros.getPathBkp() == null
					|| parametros.getPathError() == null) {
				return false;
			} else {

				// VALIDA PARAMETROS PROCESO VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getStarWith())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getEndsWith())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathAbcLogTrx())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathBkp())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathError())) {
					return false;
				}

			}

		}
		return true;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que valida que hayan encontrado todos los parametros necesarios
	 * para el proceso
	 * 
	 * PATH_ACL_INC_BKP PATH_ACL_INC_ERROR PATH_ACL_INC FORMAT_FILENAME_OUT
	 * RUT_COM_VI FORMAT_EXTNAME_OUT
	 * 
	 * @param listaParametros
	 * @return
	 * @since 1.X
	 */
	private boolean validarParametrosProcesamientoDeOutgoing(Object params) {
		ParamsFileReaderProcessDTO parametros = (ParamsFileReaderProcessDTO) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {

			// VALIDA PARAMETROS PROCESO NULOS
			if (parametros.getFormatExtNameOut() == null
					|| parametros.getFormatFilenameOut() == null
					|| parametros.getPatchAclIncError() == null
					|| parametros.getPathAclInc() == null
					|| parametros.getRutsbpay() == null
					|| parametros.getPathAclIncBkp() == null) {
				return false;
			} else {

				// VALIDA PARAMETROS PROCESO VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getFormatExtNameOut())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatFilenameOut())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPatchAclIncError())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathAclInc())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getRutsbpay())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathAclIncBkp())) {
					return false;
				}

			}
		}
		return true;
	}
	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 17/04/2019, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que valida que hayan encontrado todos los parametros necesarios
	 * para el proceso
	 * 
	 * PATH_ACL_INC_VISA_NAC_BKP PATH_ACL_INC_VISA_NAC_ERROR PATH_ACL_INC_VISA_NAC FORMAT_FILENAME_OUT_VISA_NAC
	 * RUT_COM_VI_VISA_NAC FORMAT_EXTNAME_OUT_VISA_NAC
	 * 
	 * @param listaParametros
	 * @return
	 * @since 1.X
	 */
	private boolean validarParametrosProcesamientoDeOutgoingVisaNac(Object params) {
		ParamsFileReaderProcessOutVisaNacDTO parametros = (ParamsFileReaderProcessOutVisaNacDTO) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {

			// VALIDA PARAMETROS PROCESO NULOS
			if (parametros.getFormatExtNameOut() == null
					|| parametros.getFormatFilenameOut() == null
					|| parametros.getPatchAclIncError() == null
					|| parametros.getPathAclInc() == null
					|| parametros.getRutsbpay() == null
					|| parametros.getPathAclIncBkp() == null) {
				return false;
			} else {

				// VALIDA PARAMETROS PROCESO VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getFormatExtNameOut())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatFilenameOut())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPatchAclIncError())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathAclInc())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getRutsbpay())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathAclIncBkp())) {
					return false;
				}

			}
		}
		return true;
	}
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 25/04/2019, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que valida que hayan encontrado todos los parametros necesarios
	 * para el proceso
	 * 
	 * PATH_ACL_INC_VISA_INT_BKP PATH_ACL_INC_VISA_INT_ERROR PATH_ACL_INC_VISA_INT FORMAT_FILENAME_OUT_VISA_INT
	 * RUT_COM_VI_VISA_INT FORMAT_EXTNAME_OUT_VISA_INT
	 * 
	 * @param listaParametros
	 * @return
	 * @since 1.X
	 */
	private boolean validarParametrosProcesamientoDeOutgoingVisaInt(Object params) {
		ParamsFileReaderProcessOutVisaIntDTO parametros = (ParamsFileReaderProcessOutVisaIntDTO) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {

			// VALIDA PARAMETROS PROCESO NULOS
			if (parametros.getFormatExtNameOut() == null
					|| parametros.getFormatFilenameOut() == null
					|| parametros.getPatchAclIncError() == null
					|| parametros.getPathAclInc() == null
					|| parametros.getRutsbpay() == null
					|| parametros.getPathAclIncBkp() == null) {
				return false;
			} else {

				// VALIDA PARAMETROS PROCESO VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getFormatExtNameOut())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatFilenameOut())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPatchAclIncError())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathAclInc())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getRutsbpay())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathAclIncBkp())) {
					return false;
				}

			}
		}
		return true;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que valida que hayan encontrado todos los parametros necesarios
	 * para el proceso
	 * 
	 * 
	 * @param listaParametros
	 * @return
	 * @since 1.X
	 */
	private Boolean validarParametrosProcesamientoRechazo(Object params) {
		ParamRechazoDTO parametros = (ParamRechazoDTO) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {

			// VALIDA PARAMETROS PROCESO NULOS
			if (parametros.getFormatExtNameRch() == null
					|| parametros.getFormatFileNameAbcRch() == null
					|| parametros.getPathAbcRch() == null
					|| parametros.getPathAbcRchBkp() == null
					|| parametros.getPathAbcRchError() == null) {
				return false;
			} else {

				// VALIDA PARAMETROS PROCESO VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getFormatExtNameRch())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatFileNameAbcRch())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathAbcRch())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathAbcRchBkp())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathAbcRchError())) {
					return false;
				}

			}
		}
		return true;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que valida que hayan encontrado todos los parametros necesarios
	 * para el proceso
	 * 
	 * FORMAT_FILENAME_ONUS FORMAT_EXTNAME_ONUS PATH_ABC_ONUS PATH_ABC_ONUS_BKP
	 * PATH_ABC_ONUS_ERROR
	 * 
	 * @param listaParametros
	 * @return
	 * @since 1.X
	 */
	private boolean validaParametrosProcesoOnus(Object params) {

		ParamsProcessOnusDTO parametros = (ParamsProcessOnusDTO) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {

			// VALIDA PARAMETROS PROCESO NULOS
			if (parametros.getFormatExtNameOnUs() == null
					|| parametros.getFormatFileNameOnUs() == null
					|| parametros.getPathAbcOnUs() == null
					|| parametros.getPathAbcOnUsBkp() == null
					|| parametros.getPathAbcOnUsError() == null) {
				return false;
			} else {

				// VALIDA PARAMETROS PROCESO VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getFormatExtNameOnUs())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatFileNameOnUs())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathAbcOnUs())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathAbcOnUsBkp())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathAbcOnUsError())) {
					return false;
				}

			}

		}
		return true;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que valida que hayan encontrado todos los parametros necesarios
	 * para el proceso
	 * 
	 *
	 * 
	 * @param listaParametros
	 * @return
	 * @since 1.X
	 */
	private boolean validaParametrosProcesarComisiones(Object params) {

		ParamsGenerateComisionesDTO parametros = (ParamsGenerateComisionesDTO) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {

			// VALIDA PARAMETROS PROCESO NULOS
			if (parametros.getFormatExtNameTbkComision() == null
					|| parametros.getFormatFileTbkComision() == null
					|| parametros.getPathTbkComBkp() == null
					|| parametros.getPathTbkComErr() == null
					|| parametros.getPathTbkComision() == null) {
				return false;
			} else {

				// VALIDA PARAMETROS PROCESO VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getFormatExtNameTbkComision())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatFileTbkComision())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathTbkComBkp())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathTbkComErr())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathTbkComision())) {
					return false;
				}

			}
		}
		return true;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que valida que hayan encontrado todos los parametros necesarios
	 * para el proceso
	 * 
	 * 
	 * @param listaParametros
	 * @return
	 * @since 1.X
	 */
	private boolean validarParametrosProcesoCPago(Object params) {

		ParamsReadCPagoDTO parametros = (ParamsReadCPagoDTO) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {

			// VALIDA PARAMETROS PROCESO NULOS
			if (parametros.getFileNameCPago() == null
					|| parametros.getPathCPAGO() == null
					|| parametros.getPathCPagoBKP() == null
					|| parametros.getPathCPagoError() == null) {
				return false;
			} else {

				// VALIDA PARAMETROS PROCESO VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getFileNameCPago())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathCPAGO())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathCPagoBKP())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathCPagoError())) {
					return false;
				}

			}
		}
		return true;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @param parametros
	 * @return
	 * @since 1.X
	 */
	private boolean validarParametrosProcesoGenerarIncomingTbk(Object params) {
		ParamsGenerateIncomingDTO parametros = (ParamsGenerateIncomingDTO) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {
			// VALIDA PARAMETROS PROCESO NULOS
			if (parametros.getFormatExtNameInc() == null
					|| parametros.getFormatExtNameIncCrt() == null
					|| parametros.getFormatFilenameInc() == null
					|| parametros.getPathABCIncoming() == null) {
				return false;
			} else {

				// VALIDA PARAMETROS PROCESO VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getFormatExtNameInc())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatExtNameIncCrt())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatFilenameInc())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathABCIncoming())) {
					return false;
				}

			}
		}
		return true;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @param parametros
	 * @return
	 * @since 1.X
	 */
	private boolean validaParametrosGeneracionSolicitud60(Object params) {
		ParamCargoAbonoDTO parametros = (ParamCargoAbonoDTO) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {
			// VALIDA PARAMETROS PROCESO NULOS
			if (parametros.getFormatExtNameCarAbo() == null
					|| parametros.getFormatFileNameCarAbo() == null
					|| parametros.getPathCarAbo() == null) {
				return false;
			} else {

				// VALIDA PARAMETROS PROCESO VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getFormatExtNameCarAbo())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatFileNameCarAbo())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathCarAbo())) {
					return false;
				}

			}
		}
		return true;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @param parametros
	 * @return
	 * @since 1.X
	 */
	private boolean validaParametrosSubidaIncoming(Object params) {
		ParamUploadIncomingDTO parametros = (ParamUploadIncomingDTO) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;

		} else {

			// VALIDA PARAMETROS FTP NULO
			if (parametros.getDataFTP() == null) {
				return false;
			} else {
				// VALIDA PARAMETROS FTP NULOS
				if (parametros.getDataFTP().getIpHost() == null
						|| parametros.getDataFTP().getPort() == null
						|| parametros.getDataFTP().getUser() == null
						|| parametros.getDataFTP().getPassword() == null) {
					return false;
				} else {
					// VALIDA PARAMETROS FTP VACIOS
					if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
							parametros.getDataFTP().getIpHost())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											String.valueOf(parametros
													.getDataFTP().getPort()))
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP().getUser())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP()
													.getPassword())) {
						return false;
					}
				}
			}

			// VALIDA PARAMETROS PROCESO NULOS
			if (parametros.getFormatFileInc() == null
					|| parametros.getFormatExtNameInc() == null
					|| parametros.getFormatExtNameIncCtr() == null
					|| parametros.getFormatFileInc() == null
					|| parametros.getFtpPathInc() == null
					|| parametros.getPathAbcInc() == null
					|| parametros.getPathAbcIncBkp() == null
					|| parametros.getPathAbcIncError() == null) {
				return false;
			} else {

				// VALIDA PARAMETROS PROCESO VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getFormatFileInc())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatExtNameInc())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatExtNameIncCtr())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatFileInc())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFtpPathInc())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathAbcInc())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathAbcIncBkp())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathAbcIncError())) {
					return false;
				}

			}
		}
		return true;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que valida que hayan encontrado todos los parametros necesarios
	 * para el proceso
	 * 
	 * 
	 * 
	 * @param listaParametros
	 * @return
	 * @since 1.X
	 */
	private boolean validaParametrosGeneracionContable(Object params) {
		ParamExportCnblDTO parametros = (ParamExportCnblDTO) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {

			// VALIDA PARAMETROS PROCESO NULOS
			if (parametros.getExtension() == null
					|| parametros.getNomArchivo() == null
					|| parametros.getPathSalida() == null
					|| parametros.getFormatExtNameOnewCtr() == null
					|| parametros.getFormatFileNameOnewCtr() == null) {
				return false;
			} else {

				// VALIDA PARAMETROS PROCESO VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getExtension())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getNomArchivo())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathSalida())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatExtNameOnewCtr())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatFileNameOnewCtr())) {
					return false;
				}

			}
		}
		return true;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2018, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que valida que hayan encontrado todos los parametros necesarios
	 * para el proceso
	 * 
	 * 
	 * 
	 * @param listaParametros
	 * @return
	 * @since 1.X
	 */
	private boolean validaParametrosGeneracionContableAvance(Object params) {
		ParamExportCnblDTO parametros = (ParamExportCnblDTO) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {

			// VALIDA PARAMETROS PROCESO NULOS
			if (parametros.getExtension() == null
					|| parametros.getNomArchivo() == null
					|| parametros.getPathSalida() == null
					|| parametros.getFormatExtNameOnewCtr() == null
					|| parametros.getFormatFileNameOnewCtr() == null) {
				return false;
			} else {

				// VALIDA PARAMETROS PROCESO VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getExtension())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getNomArchivo())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathSalida())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatExtNameOnewCtr())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatFileNameOnewCtr())) {
					return false;
				}

			}
		}
		return true;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2019, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que valida que hayan encontrado todos los parametros necesarios
	 * para el proceso
	 * 
	 * 
	 * 
	 * @param listaParametros
	 * @return
	 * @since 1.X
	 */
	private boolean validaParametrosRegeneracionContableAvance(Object params) {
		ParamExportReCnblDTO parametros = (ParamExportReCnblDTO) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {

			// VALIDA PARAMETROS PROCESO NULOS
			if (parametros.getExtension() == null
					|| parametros.getNomArchivo() == null
					|| parametros.getPathSalida() == null
					|| parametros.getFormatExtNameOnewCtr() == null
					|| parametros.getFormatFileNameOnewCtr() == null
					|| parametros.getPathError() == null) {
				return false;
			} else {

				// VALIDA PARAMETROS PROCESO VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getExtension())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getNomArchivo())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathSalida())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatExtNameOnewCtr())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatFileNameOnewCtr())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathError())) {
					return false;
				}

			}
		}
		return true;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que valida que hayan encontrado todos los parametros necesarios
	 * para el proceso
	 * 
	 * FTP_IP_ONEW FTP_PORT_ONEW FTP_USER_ONEW FTP_PASS_ONEW
	 * PATH_FTP_ONEW_ENTRADA PATH_TRX_CONTABLE PATH_TRX_CONTABLE_ERROR
	 * FORMAT_FILENAME_ONEW FORMAT_EXTNAME_ONEWº
	 * 
	 * @param listaParametros
	 * @return
	 * @since 1.X
	 */
	private boolean validaParametrosSubidaArchivoContable(Object params) {

		ParamUploadCntbleDTO parametros = (ParamUploadCntbleDTO) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {

			// VALIDA PARAMETROS FTP NULO
			if (parametros.getDataFTP() == null) {
				return false;
			} else {
				// VALIDA PARAMETROS FTP NULOS
				if (parametros.getDataFTP().getIpHost() == null
						|| parametros.getDataFTP().getPort() == null
						|| parametros.getDataFTP().getUser() == null
						|| parametros.getDataFTP().getPassword() == null) {
					return false;
				} else {
					// VALIDA PARAMETROS FTP VACIOS
					if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
							parametros.getDataFTP().getIpHost())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											String.valueOf(parametros
													.getDataFTP().getPort()))
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP().getUser())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP()
													.getPassword())) {
						return false;
					}
				}
			}

			// VALIDA PARAMETROS RUTAS NULAS
			if (parametros.getsbpayServerPath() == null
					|| parametros.getsbpayServerPathBkp() == null
					|| parametros.getsbpayServerPathError() == null
					|| parametros.getSftpPathIn() == null) {
				return false;
			}

			// VALIDA PARAMETROS DE RUTAS VACIOS
			if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
					parametros.getsbpayServerPath())
					|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
							parametros.getsbpayServerPathBkp())
					|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
							parametros.getsbpayServerPathError())
					|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
							parametros.getSftpPathIn())) {
				return false;
			}

			// VALIDA PARAMETROS ARCHIVO NULOS
			if (parametros.getFormatContable() == null) {

				return false;

			} else {
				// PARAMETROS NULOS
				if (parametros.getFormatContable().getStarWith() == null
						|| parametros.getFormatContable().getEndsWith() == null
						|| parametros.getFormatContable().getStarWithCtr() == null
						|| parametros.getFormatContable().getEndsWithCtr() == null) {
					return false;
				}

				// PARAMETROS VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getFormatContable().getStarWith())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatContable().getEndsWith())
						|| ConstantesUtil.EMPTY.toString()
								.equalsIgnoreCase(
										parametros.getFormatContable()
												.getStarWithCtr())
						|| ConstantesUtil.EMPTY.toString()
								.equalsIgnoreCase(
										parametros.getFormatContable()
												.getEndsWithCtr())) {
					return false;
				}
			}

		}
		return true;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2018, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que valida que hayan encontrado todos los parametros necesarios
	 * para el proceso
	 * 
	 * FTP_IP_ONEW FTP_PORT_ONEW FTP_USER_ONEW FTP_PASS_ONEW
	 * PATH_FTP_ONEW_ENTRADA PATH_TRX_CONTABLE PATH_TRX_CONTABLE_ERROR
	 * FORMAT_FILENAME_ONEW FORMAT_EXTNAME_ONEWº
	 * 
	 * @param listaParametros
	 * @return
	 * @since 1.X
	 */
	private boolean validaParametrosSubidaArchivoContableAvance(Object params) {

		ParamUploadCntbleDTO parametros = (ParamUploadCntbleDTO) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {

			// VALIDA PARAMETROS FTP NULO
			if (parametros.getDataFTP() == null) {
				return false;
			} else {
				// VALIDA PARAMETROS FTP NULOS
				if (parametros.getDataFTP().getIpHost() == null
						|| parametros.getDataFTP().getPort() == null
						|| parametros.getDataFTP().getUser() == null
						|| parametros.getDataFTP().getPassword() == null) {
					return false;
				} else {
					// VALIDA PARAMETROS FTP VACIOS
					if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
							parametros.getDataFTP().getIpHost())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											String.valueOf(parametros
													.getDataFTP().getPort()))
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP().getUser())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP()
													.getPassword())) {
						return false;
					}
				}
			}

			// VALIDA PARAMETROS RUTAS NULAS
			if (parametros.getsbpayServerPath() == null
					|| parametros.getsbpayServerPathBkp() == null
					|| parametros.getsbpayServerPathError() == null
					|| parametros.getSftpPathIn() == null) {
				return false;
			}

			// VALIDA PARAMETROS DE RUTAS VACIOS
			if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
					parametros.getsbpayServerPath())
					|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
							parametros.getsbpayServerPathBkp())
					|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
							parametros.getsbpayServerPathError())
					|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
							parametros.getSftpPathIn())) {
				return false;
			}

			// VALIDA PARAMETROS ARCHIVO NULOS
			if (parametros.getFormatContable() == null) {

				return false;

			} else {
				// PARAMETROS NULOS
				if (parametros.getFormatContable().getStarWith() == null
						|| parametros.getFormatContable().getEndsWith() == null
						|| parametros.getFormatContable().getStarWithCtr() == null
						|| parametros.getFormatContable().getEndsWithCtr() == null) {
					return false;
				}

				// PARAMETROS VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getFormatContable().getStarWith())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatContable().getEndsWith())
						|| ConstantesUtil.EMPTY.toString()
								.equalsIgnoreCase(
										parametros.getFormatContable()
												.getStarWithCtr())
						|| ConstantesUtil.EMPTY.toString()
								.equalsIgnoreCase(
										parametros.getFormatContable()
												.getEndsWithCtr())) {
					return false;
				}
			}

		}
		return true;
	}

	
	private boolean validaParametrosGenerarReporteArchivoContable(Object params) {

		ParamExportDetContDTO parametros = (ParamExportDetContDTO) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {

			// VALIDA PARAMETROS FTP NULO
			if (parametros.getPathSalida() == null) {
				return false;
			} 
		}
		return true;
	}
	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2019, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que valida que hayan encontrado todos los parametros necesarios
	 * para el proceso
	 * 
	 * FTP_IP_ONEW FTP_PORT_ONEW FTP_USER_ONEW FTP_PASS_ONEW
	 * PATH_FTP_ONEW_ENTRADA PATH_TRX_CONTABLE PATH_TRX_CONTABLE_ERROR
	 * FORMAT_FILENAME_ONEW FORMAT_EXTNAME_ONEWº
	 * 
	 * @param listaParametros
	 * @return
	 * @since 1.X
	 */
	private boolean validaParametrosResubidaArchivoContableAvance(Object params) {

		ParamUploadCntbleDTO parametros = (ParamUploadCntbleDTO) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {

			// VALIDA PARAMETROS FTP NULO
			if (parametros.getDataFTP() == null) {
				return false;
			} else {
				// VALIDA PARAMETROS FTP NULOS
				if (parametros.getDataFTP().getIpHost() == null
						|| parametros.getDataFTP().getPort() == null
						|| parametros.getDataFTP().getUser() == null
						|| parametros.getDataFTP().getPassword() == null) {
					return false;
				} else {
					// VALIDA PARAMETROS FTP VACIOS
					if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
							parametros.getDataFTP().getIpHost())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											String.valueOf(parametros
													.getDataFTP().getPort()))
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP().getUser())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP()
													.getPassword())) {
						return false;
					}
				}
			}

			// VALIDA PARAMETROS RUTAS NULAS
			if (parametros.getsbpayServerPath() == null
					|| parametros.getsbpayServerPathBkp() == null
					|| parametros.getsbpayServerPathError() == null
					|| parametros.getSftpPathIn() == null) {
				return false;
			}

			// VALIDA PARAMETROS DE RUTAS VACIOS
			if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
					parametros.getsbpayServerPath())
					|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
							parametros.getsbpayServerPathBkp())
					|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
							parametros.getsbpayServerPathError())
					|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
							parametros.getSftpPathIn())) {
				return false;
			}

			// VALIDA PARAMETROS ARCHIVO NULOS
			if (parametros.getFormatContable() == null) {

				return false;

			} else {
				// PARAMETROS NULOS
				if (parametros.getFormatContable().getStarWith() == null
						|| parametros.getFormatContable().getEndsWith() == null
						|| parametros.getFormatContable().getStarWithCtr() == null
						|| parametros.getFormatContable().getEndsWithCtr() == null) {
					return false;
				}

				// PARAMETROS VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getFormatContable().getStarWith())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatContable().getEndsWith())
						|| ConstantesUtil.EMPTY.toString()
								.equalsIgnoreCase(
										parametros.getFormatContable()
												.getStarWithCtr())
						|| ConstantesUtil.EMPTY.toString()
								.equalsIgnoreCase(
										parametros.getFormatContable()
												.getEndsWithCtr())) {
					return false;
				}
			}

		}
		return true;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 09/04/2019, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que valida que hayan encontrado todos los parametros necesarios
	 * para el proceso
	 * 
	 * FTP_IP, FTP_PORT, FTP_USER, FTP_PASS, FTP_PATH_OUT, PATH_ACL_INC,
	 * FORMAT_FILENAME_OUT FORMAT_EXTNAME_OUT, FORMAT_EXTNAME_OUT_CTR
	 * 
	 * @param listaParametros
	 * @return
	 * @since 1.X
	 */
	private boolean validarParametrosDescargOutgoingVisa(Object params) {

		ParamDownloadVisaDTO parametros = (ParamDownloadVisaDTO) params;
		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {

			// VALIDA PARAMETROS FTP NULO
			if (parametros.getDataFTP() == null) {
				return false;
			} else {
				// VALIDA PARAMETROS FTP NULOS
				if (parametros.getDataFTP().getIpHost() == null
						|| parametros.getDataFTP().getPort() == null
						|| parametros.getDataFTP().getUser() == null
						|| parametros.getDataFTP().getPassword() == null) {
					return false;
				} else {
					// VALIDA PARAMETROS FTP VACIOS
					if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
							parametros.getDataFTP().getIpHost())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											String.valueOf(parametros
													.getDataFTP().getPort()))
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP().getUser())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP()
													.getPassword())) {
						return false;
					}
				}
			}

			// VALIDA PARAMETROS PROCESO NULOS
			if (parametros.getExtNameVISA() == null
					|| parametros.getExtNameVISACTR() == null
					|| parametros.getFileNameVISA() == null
					|| parametros.getFtpPathOutVisa() == null
					|| parametros.getPathVisaInc() == null) {
				return false;
			} else {

				// VALIDA PARAMETROS PROCESO VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getExtNameVISA())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getExtNameVISACTR())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFileNameVISA())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFtpPathOutVisa())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathVisaInc())) {
					return false;
				}

			}
		}
		return true;
	}
	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 09/04/2019, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que valida que hayan encontrado todos los parametros necesarios
	 * para el proceso
	 * 
	 * SFTP_IP_DOWNLOAD_OUTGOING_VISA_NAC, SFTP_PORT_DOWNLOAD_OUTGOING_VISA_NAC, SFTP_USER_DOWNLOAD_OUTGOING_VISA_NAC, SFTP_PASS_DOWNLOAD_OUTGOING_VISA_NAC, 
	 * PATH_OUT_DWNLD_OUTGOING_VISA_NAC, PATH_ACL_INC_VISA_NAC,
	 * FORMAT_FILENAME_OUT_VISA_NAC FORMAT_EXTNAME_OUT_VISA_NAC, FORMAT_EXTNAME_OUT_CTR_VISA_NAC
	 * 
	 * @param listaParametros
	 * @return
	 * @since 1.X
	 */
	private boolean validarParametrosDescargOutgoingVisaNacional(Object params) {

		ParamDownloadVisaNacDTO parametros = (ParamDownloadVisaNacDTO) params;
		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {

			// VALIDA PARAMETROS FTP NULO
			if (parametros.getDataFTP() == null) {
				return false;
			} else {
				// VALIDA PARAMETROS FTP NULOS
				if (parametros.getDataFTP().getIpHost() == null
						|| parametros.getDataFTP().getPort() == null
						|| parametros.getDataFTP().getUser() == null
						|| parametros.getDataFTP().getPassword() == null) {
					return false;
				} else {
					// VALIDA PARAMETROS FTP VACIOS
					if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
							parametros.getDataFTP().getIpHost())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											String.valueOf(parametros
													.getDataFTP().getPort()))
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP().getUser())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP()
													.getPassword())) {
						return false;
					}
				}
			}

			// VALIDA PARAMETROS PROCESO NULOS
			if (parametros.getExtNameVISA() == null
					|| parametros.getExtNameVISACTR() == null
					|| parametros.getFileNameVISA() == null
					|| parametros.getFtpPathOutVisa() == null
					|| parametros.getPathVisaInc() == null) {
				return false;
			} else {

				// VALIDA PARAMETROS PROCESO VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getExtNameVISA())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getExtNameVISACTR())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFileNameVISA())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFtpPathOutVisa())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathVisaInc())) {
					return false;
				}

			}
		}
		return true;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que valida que hayan encontrado todos los parametros necesarios
	 * para el proceso
	 * 
	 * FTP_IP, FTP_PORT, FTP_USER, FTP_PASS, FTP_PATH_OUT, PATH_ACL_INC,
	 * FORMAT_FILENAME_OUT FORMAT_EXTNAME_OUT, FORMAT_EXTNAME_OUT_CTR
	 * 
	 * @param listaParametros
	 * @return
	 * @since 1.X
	 */
	private boolean validarParametroSubirIncomingVisaAIc(Object params) {

		ParamUploadIncomingVisaDTO parametros = (ParamUploadIncomingVisaDTO) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {

			// VALIDA PARAMETROS FTP NULO
			if (parametros.getDataFTP() == null) {
				return false;
			} else {
				// VALIDA PARAMETROS FTP NULOS
				if (parametros.getDataFTP().getIpHost() == null
						|| parametros.getDataFTP().getPort() == null
						|| parametros.getDataFTP().getUser() == null
						|| parametros.getDataFTP().getPassword() == null) {
					return false;
				} else {
					// VALIDA PARAMETROS FTP VACIOS
					if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
							parametros.getDataFTP().getIpHost())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											String.valueOf(parametros
													.getDataFTP().getPort()))
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP().getUser())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP()
													.getPassword())) {
						return false;
					}
				}
			}

			// VALIDA PARAMETROS PROCESO NULOS
			if (parametros.getPathVisaInc() == null
					|| parametros.getFtpPathIcEntrada() == null
					|| parametros.getFilenameOutVisa() == null
					|| parametros.getExtFileOutVisa() == null) {
				return false;
			} else {

				// VALIDA PARAMETROS PROCESO VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getPathVisaInc())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFtpPathIcEntrada())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFilenameOutVisa())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getExtFileOutVisa())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getExtFileCtrOutVisa())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFilenameOutVisaToIc())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getExtFileOutVisaToIc())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getExtFileCtrOutVisaToIc())
						) {
					return false;
				}

			}
		}
		return true;
	}
	
	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 04/09/2019, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que valida que hayan encontrado todos los parametros necesarios
	 * para el proceso
	 * 
	 * SFTP_IP_UPLOAD_VISA_NAC_IC, SFTP_PORT_UPLOAD_VISA_NAC_IC, SFTP_USER_UPLOAD_VISA_NAC_IC, SFTP_PASS_UPLOAD_VISA_NAC_IC, PATH_IN_UPLD_VISA_NAC_IC, 
	 * PATH_ACL_INC_VISA_NAC,
	 * FORMAT_FILENAME_OUT_VISA_NAC FORMAT_EXTNAME_OUT_VISA_NAC
	 * 
	 * @param listaParametros
	 * @return
	 * @since 1.X
	 */
	private boolean validarParametroSubirIncomingVisaNacAIc(Object params) {

		ParamUploadOutgoingVisaNacICDTO parametros = (ParamUploadOutgoingVisaNacICDTO) params;
		
		// VALIDA OBJETO PARAMETRO NULO
				if (parametros == null) {
					return false;
				} else {

					// VALIDA PARAMETROS FTP NULO
					if (parametros.getDataFTP() == null) {
						return false;
					} else {
						// VALIDA PARAMETROS FTP NULOS
						if (parametros.getDataFTP().getIpHost() == null
								|| parametros.getDataFTP().getPort() == null
								|| parametros.getDataFTP().getUser() == null
								|| parametros.getDataFTP().getPassword() == null) {
							return false;
						} else {
							// VALIDA PARAMETROS FTP VACIOS
							if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
									parametros.getDataFTP().getIpHost())
									|| ConstantesUtil.EMPTY.toString()
											.equalsIgnoreCase(
													String.valueOf(parametros
															.getDataFTP().getPort()))
									|| ConstantesUtil.EMPTY.toString()
											.equalsIgnoreCase(
													parametros.getDataFTP().getUser())
									|| ConstantesUtil.EMPTY.toString()
											.equalsIgnoreCase(
													parametros.getDataFTP()
															.getPassword())) {
								return false;
							}
						}
					}

					// VALIDA PARAMETROS PROCESO NULOS
					if (parametros.getFormatFileNameOut() == null
							|| parametros.getFormatExtNameOutCtr() == null
							|| parametros.getFormatFileNameOut() == null
							|| parametros.getPathAclInc() == null
							|| parametros.getPathFtpIcEntrada() == null
							|| parametros.getFormatFileNameOutToIc() == null
							|| parametros.getFormatExtNameOutCtrToIc() == null
							|| parametros.getFormatFileNameOutToIc() == null) {
						return false;
					} else {

						// VALIDA PARAMETROS PROCESO VACIOS
						if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatExtNameOut())
								|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
										parametros.getFormatExtNameOutCtr())
								|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
										parametros.getFormatFileNameOut())
								|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
										parametros.getPathAclInc())
								|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
										parametros.getPathFtpIcEntrada())
								|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
										parametros.getFormatExtNameOutToIc())
								|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatExtNameOutCtrToIc())
								|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatFileNameOutToIc())
								) {
							return false;
						}

					}
				}

		return true;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que valida que hayan encontrado todos los parametros necesarios
	 * para el proceso
	 * 
	 * 
	 * @param listaParametros
	 * @return
	 * @since 1.X
	 */
	private boolean ValidarParametrosProcesoDeOutgoingVisa(Object params) {
		ParamsProcessOutVisaDTO parametros = (ParamsProcessOutVisaDTO) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {

			// VALIDA PARAMETROS PROCESO NULOS
			if (parametros.getFormatExtNameOutVisa() == null
					|| parametros.getFormatFilenameOutVisa() == null
					|| parametros.getPathVisaInc() == null
					|| parametros.getPathVisaIncBkp() == null
					|| parametros.getPathVisaIncError() == null) {
				return false;
			} else {

				// VALIDA PARAMETROS PROCESO VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getFormatExtNameOutVisa())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatFilenameOutVisa())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathVisaInc())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathVisaIncBkp())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathVisaIncError())) {
					return false;
				}

			}
		}
		return true;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @param parametros
	 * @return
	 * @since 1.X
	 */
	private boolean validarParametrosGeneracionIncomingONUS(Object params) {

		ParamsGenerateIncomingOnUsDTO parametros = (ParamsGenerateIncomingOnUsDTO) params;
		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {
			// VALIDA PARAMETROS PROCESO NULOS
			if (parametros.getPathAbcOnUsOut() == null
					|| parametros.getFormatExtNameIncCrtOnUs() == null
					|| parametros.getFormatExtNameIncOnUs() == null
					|| parametros.getFormatFilenameIncOnUs() == null) {
				return false;
			} else {

				// VALIDA PARAMETROS PROCESO VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getPathAbcOnUsOut())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatExtNameIncCrtOnUs())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatExtNameIncOnUs())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatFilenameIncOnUs())) {
					return false;
				}

			}
		}
		return true;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @param parametros
	 * @return
	 * @since 1.X
	 */
	private boolean validarParametrosProcesoGenerarIncomingVisa(Object params) {

		ParamsGenerateIncomingVisaDTO parametros = (ParamsGenerateIncomingVisaDTO) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {
			// VALIDA PARAMETROS PROCESO NULOS
			if (parametros.getFormatExtNameIncVisa() == null
					|| parametros.getFormatExtNameIncVisaCrt() == null
					|| parametros.getFormatFilenameIncVisa() == null
					|| parametros.getPathOutVisa() == null) {
				return false;
			} else {

				// VALIDA PARAMETROS PROCESO VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getFormatExtNameIncVisa())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatExtNameIncVisaCrt())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatFilenameIncVisa())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathOutVisa())) {
					return false;
				}

			}
		}
		return true;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que valida que hayan encontrado todos los parametros necesarios
	 * para el proceso
	 * 
	 * FTP_IP_IC FTP_PORT_IC FTP_USER_IC FTP_PASS_IC PATH_FTP_IC_SALIDA
	 * FORMAT_FILENAME_ONUS FORMAT_EXTNAME_ONUS PATH_ABC_ONUS
	 * 
	 * @param listaParametros
	 * @return
	 * @since 1.X
	 */
	private boolean validarParametrosProcesoSubidaOnus(Object params) {

		ParamUploadOnusTbkDTO parametros = (ParamUploadOnusTbkDTO) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {

			// VALIDA PARAMETROS FTP NULO
			if (parametros.getDataFTP() == null) {
				return false;
			} else {
				// VALIDA PARAMETROS FTP NULOS
				if (parametros.getDataFTP().getIpHost() == null
						|| parametros.getDataFTP().getPort() == null
						|| parametros.getDataFTP().getUser() == null
						|| parametros.getDataFTP().getPassword() == null) {
					return false;
				} else {
					// VALIDA PARAMETROS FTP VACIOS
					if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
							parametros.getDataFTP().getIpHost())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											String.valueOf(parametros
													.getDataFTP().getPort()))
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP().getUser())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP()
													.getPassword())) {
						return false;
					}
				}
			}

			// VALIDA PARAMETROS PROCESO NULOS
			if (parametros.getExtOnusTbkFile() == null
					|| parametros.getNameOnusTbkFile() == null
					|| parametros.getPathOnusTbkInc() == null
					|| parametros.getFtpPathIn() == null
					|| parametros.getPathBackup() == null
					|| parametros.getPathError() == null) {
				return false;
			} else {

				// VALIDA PARAMETROS PROCESO VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getExtOnusTbkFile())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getNameOnusTbkFile())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathOnusTbkInc())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFtpPathIn())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathBackup())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathError())) {
					return false;
				}

			}
		}
		return true;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @param params
	 * @return
	 * @since 1.X
	 */
	private Boolean validaparametrosSubidaBol(Object params) {

		ParamUploadBolDTO parametros = (ParamUploadBolDTO) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {

			// VALIDA PARAMETROS FTP NULO
			if (parametros.getDataFTP() == null) {
				return false;
			} else {
				// VALIDA PARAMETROS FTP NULOS
				if (parametros.getDataFTP().getIpHost() == null
						|| parametros.getDataFTP().getPort() == null
						|| parametros.getDataFTP().getUser() == null
						|| parametros.getDataFTP().getPassword() == null) {
					return false;
				} else {
					// VALIDA PARAMETROS FTP VACIOS
					if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
							parametros.getDataFTP().getIpHost())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											String.valueOf(parametros
													.getDataFTP().getPort()))
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP().getUser())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP()
													.getPassword())) {
						return false;
					}
				}
			}

			// VALIDA PARAMETROS PROCESO NULOS
			if (parametros.getFormatExtName() == null
					|| parametros.getFormatExtNameCtrl() == null
					|| parametros.getFormatFilename() == null
					|| parametros.getPathFtpUpload() == null
					|| parametros.getPathLocalServer() == null
					|| parametros.getPathLocalServerBkp() == null
					|| parametros.getPathLocalServerError() == null) {
				return false;
			} else {

				// VALIDA PARAMETROS PROCESO VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getFormatExtName())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatExtNameCtrl())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFormatFilename())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathFtpUpload())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathLocalServer())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathLocalServerBkp())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathLocalServerError())) {
					return false;
				}

			}
		}
		return true;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que valida que hayan encontrado todos los parametros necesarios
	 * para el proceso
	 * 
	 * 
	 * @param listaParametros
	 * @return
	 * @since 1.X
	 */
	private boolean validaParametrosSubirVisa(Object params) {
		ParamUploadVisaDTO parametros = (ParamUploadVisaDTO) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {

			// VALIDA PARAMETROS FTP NULO
			if (parametros.getDataFTP() == null) {
				return false;
			} else {
				// VALIDA PARAMETROS FTP NULOS
				if (parametros.getDataFTP().getIpHost() == null
						|| parametros.getDataFTP().getPort() == null
						|| parametros.getDataFTP().getUser() == null
						|| parametros.getDataFTP().getPassword() == null) {
					return false;
				} else {
					// VALIDA PARAMETROS FTP VACIOS
					if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
							parametros.getDataFTP().getIpHost())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											String.valueOf(parametros
													.getDataFTP().getPort()))
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP().getUser())
							|| ConstantesUtil.EMPTY.toString()
									.equalsIgnoreCase(
											parametros.getDataFTP()
													.getPassword())) {
						return false;
					}
				}
			}

			// VALIDA PARAMETROS PROCESO NULOS
			if (parametros.getExtVisaFile() == null
					|| parametros.getFilenameVisa() == null
					|| parametros.getFtpPathVisaOut() == null
					|| parametros.getPathVisaBkp() == null
					|| parametros.getPathVisaError() == null
					|| parametros.getPathVisaInc() == null) {
				return false;
			} else {

				// VALIDA PARAMETROS PROCESO VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getExtVisaFile())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFilenameVisa())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getFtpPathVisaOut())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathVisaBkp())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathVisaError())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathVisaInc())) {
					return false;
				}

			}
		}
		return true;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/10/2018, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que valida que hayan encontrado todos los parametros necesarios
	 * para el proceso
	 * 
	 * FORMAT_FILENAME_BICE PATH_ABC_BICE PATH_ACL_AVA_BICE
	 * PATH_ABC_AVA_BICE_BKP PATH_ABC_AVA_BICE_ERROR
	 * 
	 * @param listaParametros
	 * @return
	 * @since 1.X
	 */
	private boolean validaParametrosProcesoBice(Object params) {

		ParamsProcessBiceDTO parametros = (ParamsProcessBiceDTO) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {

			// VALIDA PARAMETROS PROCESO NULOS
			if (parametros.getFormatFileNameBice() == null
					|| parametros.getPathAbcBice() == null
					|| parametros.getPathAbcBiceBkp() == null
					|| parametros.getPathAbcBiceError() == null) {
				return false;
			} else {

				// VALIDA PARAMETROS PROCESO VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getFormatFileNameBice())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathAbcBice())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathAbcBiceBkp())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathAbcBiceError())) {
					return false;
				}

			}

		}
		return true;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/10/2018, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que valida que hayan encontrado todos los parametros necesarios
	 * para el proceso
	 * 
	 * FORMAT_FILENAME_BICE PATH_ABC_BICE PATH_ACL_AVANCES PATH_ABC_AVANCES_BKP
	 * PATH_ABC_AVANCES_ERROR
	 * 
	 * @param listaParametros
	 * @return
	 * @since 1.X
	 */
	private boolean validaParametrosProcesoAvances(Object params) {

		ParamsProcessAvancesDTO parametros = (ParamsProcessAvancesDTO) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {

			// VALIDA PARAMETROS PROCESO NULOS
			if (parametros.getFormatFileNameAvances() == null
					|| parametros.getPathAbcAvances() == null
					|| parametros.getPathAbcAvancesBkp() == null
					|| parametros.getPathAbcAvancesError() == null) {
				return false;
			} else {

				// VALIDA PARAMETROS PROCESO VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getFormatFileNameAvances())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathAbcAvances())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathAbcAvancesBkp())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathAbcAvancesError())) {
					return false;
				}

			}

		}
		return true;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 XX/YY/2018, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo que valida que hayan encontrado todos los parametros necesarios
	 * para el proceso
	 * 
	 * 
	 * 
	 * @param listaParametros
	 * @return
	 * @since 1.X
	 */
	private boolean validaParametrosGeneracionAvance(Object params) {
		ParamExportItzAvanceDTO parametros = (ParamExportItzAvanceDTO) params;

		// VALIDA OBJETO PARAMETRO NULO
		if (parametros == null) {
			return false;
		} else {

			// VALIDA PARAMETROS PROCESO NULOS
			if (parametros.getNomArchivo() == null
					|| parametros.getPathSalida() == null) {
				return false;
			} else {

				// VALIDA PARAMETROS PROCESO VACIOS
				if (ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
						parametros.getNomArchivo())
						|| ConstantesUtil.EMPTY.toString().equalsIgnoreCase(
								parametros.getPathSalida())) {
					return false;
				}

			}
		}
		return true;
	}

}
