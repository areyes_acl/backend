package com.sbpay.sgi.cron.step;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.enums.MsgExitoMail;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.srv.download.DownLoadAvancesSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 DD/08/2018, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class StepDownloadAvances {
	 /** The Constant LOGGER. */
	private static final Logger LOG = Logger
			.getLogger(StepDownloadAvances.class);
	
	/**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 DD/10/2018, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws Exception
     * @since 1.X
     */
    public void execute() throws Exception {
        
        try {
            LOG.info( "=====> Inicio Proceso:  Descargar archivo desde SFTP  <======" );
            if ( DownLoadAvancesSrv.getInstance().descargarAvances() ) {
                LOG.info( "Envio Correo de Descarga Satisfactoria: Descargar archivo desde SFTP" );
                MailSendSrv
                        .getInstance()
                        .sendMailOk(
                                MsgExitoMail.EXITO_PROCESS_DOWNLOAD_TITLE_AVANCES
                                        .toString(),
                                MsgExitoMail.EXITO_PROCESS_DOWNLOAD_AVANCES
                                        .toString() );
                LOG.info( " ======> Finaliza Proceso :DownLoad SFTP , Se ejecuta correctamente ======" );
            }
            else {
                LOG.info( " ======> Finaliza Proceso :DownLoad archivo desde SFTP , con observaciones. ======" );
            }
        }
        catch ( AppException e ) {
            LOG.error(e.getMessage(), e);
            MailSendSrv.getInstance().sendMail(
                    MsgProceso.MSG_GENERIC_ERROR_MSJ.toString(),
                    (e.getStackTraceDescripcion() != null) ? e
                        .getStackTraceDescripcion() : e.getMessage());
        }
        
    }
}
