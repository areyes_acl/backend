package com.sbpay.sgi.cron.srv.download;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dto.ConfigFile;
import com.sbpay.sgi.cron.dto.DataFTP;
import com.sbpay.sgi.cron.dto.ParamCronBiceDTO;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.dto.TblCronLogDTO;
import com.sbpay.sgi.cron.enums.ConstantesPRTS;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.LogBD;
import com.sbpay.sgi.cron.enums.MsgErrorMail;
import com.sbpay.sgi.cron.enums.MsgErrorSQL;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.enums.StatusProcessType;
import com.sbpay.sgi.cron.srv.utils.ClientFTPSrv;
import com.sbpay.sgi.cron.srv.utils.CommonSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;
import com.sbpay.sgi.cron.utils.generals.DateUtils;
import com.sbpay.sgi.cron.validation.ProcessValidator;
import com.sbpay.sgi.cron.validation.impl.ProcessValidatorImpl;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 DD/08/2018, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * Clase encargada de descargar el archivo de avances de BICE y dejarlo en la
 * carpeta de BICE/BKP de ABC-DIN.
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public final class DownLoadAvancesBiceSrv extends CommonSrv {
	/** The Constant LOGGER. */
	private static final Logger LOG = Logger
			.getLogger(DownLoadAvancesBiceSrv.class.getName());

	/** The single instance. */
	private static DownLoadAvancesBiceSrv singleINSTANCE = null;

	private final ProcessCodeType PROCESO_ACTUAL = ProcessCodeType.DESCARGAR_AVANCES_BICE;

	/**
	 * Creates the instance.
	 */
	private static void createInstance() {
		synchronized (DownLoadAvancesBiceSrv.class) {
			if (singleINSTANCE == null) {
				singleINSTANCE = new DownLoadAvancesBiceSrv();
			}
		}
	}

	/**
	 * Patron singleton.
	 * 
	 * @return DownLoadAvancesBiceSrv retorna instancia del servicio.
	 */
	public static DownLoadAvancesBiceSrv getInstance() {
		if (singleINSTANCE == null) {
			createInstance();
		}
		return singleINSTANCE;
	}

	/**
	 * Metodo de restricion de sobreescritura de la clase.
	 */
	@Override
	protected Object clone() throws CloneNotSupportedException {
		super.clone();
		throw new CloneNotSupportedException();
	}

	/**
	 * Constructor Privado.
	 */
	private DownLoadAvancesBiceSrv() {
		super();
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 DD/08/2018, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo principal controla la logica de carga del archivo avances de BICE
	 * a sbpay
	 * 
	 * @return flag de carga realziada true: OK false: ERROR
	 * @throws AppException
	 *
	 * @throws AppException
	 * @since 1.X
	 */
	public boolean descargarAvances() throws AppException {

		boolean flag = Boolean.FALSE;
		ParamCronBiceDTO parametros = null;
		List<String> listFTP = null;
		List<String> listToDownload = null;
		List<String> listToDownloadCtr = null;
		int countDownloadProcess = 0;
		String errorFile = null;
		String formatoFile = "";

		ProcessValidator validadorProceso = new ProcessValidatorImpl();

		// VALIDA SI PROCESO SE ENCUENTRA ACTIVADO/DESACTIVADO
		if (validadorProceso.isActiveProcess(PROCESO_ACTUAL)) {
			parametros = getParametros();

			// VALIDA PARAMETROS DE PROCESO
			if (validadorProceso.hasTheValidParametersForTheProcess(
					PROCESO_ACTUAL, parametros)) {
				LOG.info("Se encontraron todos los parametros necesarios para iniciar el proceso ");

				// OBTENER NOMBRES ARCHIVOS A DESCARGAR
				listFTP = ClientFTPSrv.getInstance().listFileSFTP(
						parametros.getDataFTP(), parametros.getFtpPathBice());

				LOG.info("lista desde SFTP" + listFTP);

				formatoFile = DateUtils.getDateTodayInYYYYMMDDAnterior()
						.concat(ConstantesUtil.UNDERSCORE.toString())
						.concat(parametros.getFormatFilenameBiceCorre())
						.concat(ConstantesUtil.UNDERSCORE.toString())
						.concat(parametros.getFormatFilenameBice());

				// VALIDAR FORMATOS NOMBRES ARCHIVOS
				ConfigFile configFile = new ConfigFile();
				configFile.setStarWith(formatoFile);
				configFile.setEndsWith("");
				configFile.setStarWithCtr(formatoFile);
				configFile.setEndsWithCtr(parametros.getFormatExtnameBiceCtr());

				// FILTRA POR ARCHIVO POR NOMBRE DE ARCHIVO
				listToDownload = retrieveFilenameListToDownloadBice(listFTP,
						configFile);

				// FILTRA ARCHIVO DE CONTROL PARA VALIDAR QUE EXISTA
				listToDownloadCtr = retrieveFilenameListToDownloadBiceCtr(
						listFTP, configFile);
				
				LOG.info("Fecha de archivo a buscar "+ DateUtils.getDateTodayInYYYYMMDDAnterior());
				LOG.info("Lista de archivos a descargar: " + listToDownload);
				LOG.info("archivos de control: " + listToDownloadCtr);

				// SI EXISTE ALGUN ARCHIVO VALIDO CONTINUA PROCESO DEBE EXITIR
				// UN ARCHIVO DE CONTROL
				if (listToDownloadCtr.size() > 0 && listToDownloadCtr != null) {
					if (listToDownload != null && listToDownload.size() > 0) {

						// FILTRAR ARCHIVOS QUE NO HAN SIDO DESCARGADOS
						// ANTERIORMENTE
						listToDownload = currentlyNotDownloadedFilesBice(
								listToDownload, configFile.getStarWith(),
								LogBD.TABLE_LOG_AVANCES_BICE);

						LOG.info("Lista de archivos de avances que no han sido descargados con anterioridad : "
								+ listToDownload);

						// VALIDACION QUE EXISTA ALGUNO
						if (listToDownload != null && listToDownload.size() > 0) {
							errorFile = ConstantesUtil.EMPTY.toString();
							for (String filename : listToDownload) {
								try {
									// DESCARGAR DESDE FTP BICE
									String pathFtpOri = parametros
											.getFtpPathBice();
									String pathDest = parametros
											.getPathAclBice();

									LOG.info("Comienza descarga de archivo "
											+ filename
											+ " desde ftp : "
											+ parametros.getDataFTP()
													.getIpHost());

									ClientFTPSrv.getInstance()
											.downloadFileSFTP(
													parametros.getDataFTP(),
													pathFtpOri, pathDest,
													filename);

									// INSERTA EN LA TBL_LOG_AVANCES_BICE
									save(filename, configFile.getStarWith());

									// CUENTA LOS REGISTROS INSERTADOS
									countDownloadProcess++;
								} catch (AppException e) {
									LOG.error(
											"Ha ocurrido un error al intentar descargar el archivo : "
													+ filename, e);
									errorFile = errorFile.concat(filename)
											.concat(ConstantesUtil.SKIP_LINE
													.toString());
								}
							}
						} else {
							LOG.warn("No existen nuevos archivos de avances que descargar desde SFTP de BICE, todos han sido procesados anteriormente");
							MailSendSrv
									.getInstance()
									.sendMail(
											MsgErrorMail.ALERTA_FILE_PROCESSED_BICE
													.toString(),
											MsgErrorMail.ALERTA_FILE_PROCESSED_DAT_BICE
													.toString());
						}
					} else {
						LOG.warn("No existen archivos de avances para descargar desde SFTP de BICE");
						MailSendSrv
								.getInstance()
								.sendMail(
										MsgErrorMail.ALERTA_FILE_PROCESS_BICE
												.toString(),
										MsgErrorMail.ALERTA_FILE_DOWNLOAD_BICE
												.toString());
					}
				} else {
					LOG.warn("No existen archivo de control de avances para descargar desde SFTP de BICE");
					MailSendSrv.getInstance().sendMail(
							MsgErrorMail.ALERTA_FILE_PROCESS_BICE_CTR.toString(),
							MsgErrorMail.ALERTA_FILE_PROCESS_BICE_CTR.toString());
				}
			} else {
				LOG.warn(MsgProceso.MSG_ERROR_FALTAN_PARAMETROS.toString()
						+ "-- " + listToDownload);
				MailSendSrv.getInstance().sendMail(
						MsgErrorMail.ALERTA_PRTS.toString(),
						MsgErrorMail.ALERTA_PRTS_TXT.toString());
			}

			// SE SUBIERON CORRECTAMETNE TODOS LOS REGISTROS
			if (listToDownload != null
					&& countDownloadProcess == listToDownload.size()) {
				flag = Boolean.TRUE;
			} else if (errorFile != null) {
				MailSendSrv.getInstance().sendMail(
						MsgErrorMail.ALERTA_FILE_LOAD_BICE.toString(),
						MsgErrorMail.ALERTA_FILE_LOAD_DAT_BICE.toString()
								.concat(errorFile));
			}

			return flag;

		} else {
			String asunto = MsgProceso.MSG_PROCESO_DESACTIVADO_ASUNTO
					.toString().replace(":?:",
							PROCESO_ACTUAL.getCodigoProceso());
			String mensaje = MsgProceso.MSG_PROCESO_DESACTIVADO_BODY.toString()
					.replace(":?:", PROCESO_ACTUAL.getCodigoProceso());
			MailSendSrv.getInstance().sendMail(asunto, mensaje);
		}
		return flag;

	}

	/**
	 * Metodo crea un registro en la tabla log TBL_LOG_AVANCES_BICE registrando
	 * la descarga nombre del archivo. BICE
	 * 
	 * @param fileName
	 *            nombre del archivo.
	 * @param starWith
	 *            prefijo del archivo.
	 * @throws AppException
	 *             Exception App.
	 */
	private void save(final String fileName, final String starWith)
			throws AppException {
		// INICIALIZA EL DAO
		CommonsDao commonsDAO = null;
		try {
			commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
			TblCronLogDTO cronLog = new TblCronLogDTO();
			cronLog.setFecha(DateUtils.getDateFileBice(DateUtils
					.getDateStringFromFilenameBice(fileName, starWith)));

			LOG.info("fecha obtenida : "
					+ cronLog.getFecha()
					+ " data util: "
					+ DateUtils.getDateStringFromFilenameBice(fileName,
							starWith));

			cronLog.setFilename(fileName);
			cronLog.setFileFlag(StatusProcessType.PROCESS_PENDING.getValue());

			if (commonsDAO
					.saveTblCronLog(cronLog, LogBD.TABLE_LOG_AVANCES_BICE)) {
				commonsDAO.endTx();
			} else {
				commonsDAO.rollBack();
			}
		} catch (SQLException e) {
			rollBackTransaccion(commonsDAO);
			throw new AppException(
					MsgErrorSQL.ERROR_LOAD_PROCESS_BICE.toString(), e);
		} finally {
			try {
				if (commonsDAO != null) {
					commonsDAO.close();
				}
			} catch (SQLException e) {
				rollBackTransaccion(commonsDAO);
				throw new AppException(
						MsgErrorSQL.ERROR_LOAD_PROCESS_BICE.toString(), e);
			}
		}

	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 DD/08/2018, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @return
	 * @throws AppException
	 * @since 1.X
	 */
	private ParamCronBiceDTO getParametros() throws AppException {

		CommonsDao daoCommon = null;
		List<ParametroDTO> listaParametros = null;
		DataFTP dataFTP = null;
		ParamCronBiceDTO parametros = null;
		try {
			LOG.info("CARGA PARAMETROS PROCESO");
			daoCommon = CommonsDaoFactory.getInstance().getNewEntidadDao();
			listaParametros = daoCommon
					.getParametroCodGrupoDato(ConstantesPRTS.CRON_TBK
							.toString());

			if (listaParametros != null) {

				// PARAMETROS DE FTP BICE
				dataFTP = new DataFTP();
				dataFTP.setIpHost(CommonsUtils.getCodiDato(listaParametros,
						ConstantesPRTS.SFTP_IP_DOWNLOAD_AVA_BICE.toString()));
				String port = CommonsUtils.getCodiDato(listaParametros,
						ConstantesPRTS.SFTP_PORT_DOWNLOAD_AVA_BICE.toString());
				dataFTP.setPort(Integer.parseInt((port != null
						&& port.matches(ConstantesUtil.PATTERN_IS_NUMBER
								.toString()) ? port
						: ConstantesUtil.DEFAULT_PORT.toString())));
				dataFTP.setUser(CommonsUtils.getCodiDato(listaParametros,
						ConstantesPRTS.SFTP_USER_DOWNLOAD_AVA_BICE.toString()));
				dataFTP.setPassword(CommonsUtils.getCodiDato(listaParametros,
						ConstantesPRTS.SFTP_PASS_DOWNLOAD_AVA_BICE.toString()));

				parametros = new ParamCronBiceDTO();

				// RUTA FTP DONDE BUSCA ARCHIVOS A DESCARGAR
				parametros.setFtpPathBice(CommonsUtils.getCodiDato(
						listaParametros,
						ConstantesPRTS.PATH_OUT_DWNLD_AVA_BICE.toString()));

				// RUTA SERVIDOR DONDE SE DEJARA EL ARCHIVO DE AVANCES
				// DESCARGADO
				parametros.setPathAclBice(CommonsUtils.getCodiDato(
						listaParametros,
						ConstantesPRTS.PATH_ACL_AVA_BICE.toString()));

				// FORMATO DE ARCHIVO BICE
				parametros.setFormatFilenameBice(CommonsUtils.getCodiDato(
						listaParametros,
						ConstantesPRTS.FORMAT_FILENAME_AVA_BICE.toString()));

				// FORMATO DE ARCHIVO BICE CORRELATIVO
				parametros.setFormatFilenameBiceCorre(CommonsUtils.getCodiDato(
						listaParametros,
						ConstantesPRTS.FORMAT_FILENAME_CORRELATIVO_AVA_BICE
								.toString()));

				// FORMATO EXTENSION DE CONTROL
				parametros.setFormatExtnameBiceCtr(CommonsUtils.getCodiDato(
						listaParametros,
						ConstantesPRTS.FORMAT_EXTNAME_AVA_BICE_CTR.toString()));

				parametros.setDataFTP(dataFTP);

			} else {
				parametros = null;
			}
		} catch (SQLException e) {
			throw new AppException(MsgErrorSQL.ERROR_QUERY_PRTS.toString(), e);
		} finally {
			try {
				if (daoCommon != null) {
					daoCommon.close();
				}
			} catch (SQLException e) {
				throw new AppException(
						MsgErrorSQL.ERROR_QUERY_PRTS_CLOSE.toString(), e);
			}
		}

		return parametros;
	}

}
