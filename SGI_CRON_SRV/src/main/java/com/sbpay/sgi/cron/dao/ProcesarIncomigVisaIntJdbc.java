package com.sbpay.sgi.cron.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import oracle.jdbc.OracleCallableStatement;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.dao.BaseDao;
import com.sbpay.sgi.cron.dto.Registro00OutVisaIntDTO;
import com.sbpay.sgi.cron.dto.Registro01OutVisaIntDTO;
import com.sbpay.sgi.cron.dto.Registro03OutVisaIntDTO;
import com.sbpay.sgi.cron.dto.Registro04OutVisaIntDTO;
import com.sbpay.sgi.cron.dto.Registro05OutVisaIntDTO;
import com.sbpay.sgi.cron.dto.Registro06OutVisaIntDTO;
import com.sbpay.sgi.cron.dto.Registro0DOutVisaIntDTO;
import com.sbpay.sgi.cron.dto.Registro2DOutVisaIntDTO;
import com.sbpay.sgi.cron.dto.Registro07OutVisaIntDTO;
import com.sbpay.sgi.cron.dto.TransaccionOutVisaInt;
import com.sbpay.sgi.cron.enums.ConstantesBD;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.utils.generals.DateUtils;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 25/04/2019, (ACL) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SA.</B>
 */
public class ProcesarIncomigVisaIntJdbc extends
		BaseDao<ProcesarIncomingVisaIntDao> implements
		ProcesarIncomingVisaIntDao {

	/** VARIABLE PARA EL LOGER */
	private static final Logger LOGGER = Logger
			.getLogger(ProcesarIncomigVisaIntJdbc.class);

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 25/04/2019, (ACL & sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @param connection
	 * @since 1.X
	 */
	public ProcesarIncomigVisaIntJdbc(Connection connection) {
		super(connection);

	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 25/04/2019, (ACL & sbpay) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @param connection
	 * @since 1.X
	 */
	@Override
	public boolean saveTransaction(TransaccionOutVisaInt transaccion,
			Integer sidOperador) throws SQLException {
		PreparedStatement stmt = null;

		try {
			StringBuilder str = new StringBuilder()
					.append("INSERT INTO ")
					.append(ConstantesBD.ESQUEMA.toString())
					.append(".TMP_INCOMING  ")
					.append(" (SID, MIT, NRO_TARJETA, codigo_procesamiento,")
					.append(" monto_transac, monto_conciliacion, monto_facturacion, tasa_convenio_conciliacion,")
					.append(" tasa_convenio_facturacion, fecha_hr_trasac, fecha_vencimiento_tarjeta, ")
					.append(" cod_datos_punto_servicio , nro_secuencia_tarjeta, codigo_funcion, cod_motivo_mensaje, ")
					.append(" mcc , montos_originales, datos_referencia_adquirente, cod_identificacion_adquirente, ")
					.append(" cod_ident_inst_envia , nro_referencia_recuperacion, codigo_autorizacion, codigo_servicio, ")
					.append(" ident_term_acep_tarjeta , cod_ident_acep_tarjeta, nombre_ubic_acep_tarjeta, datos_adicionales_1, ")
					.append(" cod_moneda_transac , cod_moneda_conciliacion, cod_moneda_facturacion, montos_adicionales, ")
					.append(" icc , datos_adicionales_2, ident_ciclo_duracion_transac, nro_mensaje, ")
					.append(" registro_de_datos , fecha_accion, cod_ident_inst_dest_transac, cod_ident_inst_orig_transac, ")
					.append(" datos_ref_emisor  , cod_ident_inst_recibe, monto_recargo_x_conver_moneda, datos_adicionales_3, ")
					.append(" datos_adicionales_4 , datos_adicionales_5, p0023, p0052, ")
					.append(" p0148 , p0158, p0165, p1000, ")
					.append(" p1001 , p1003, p1004, p1005, ")
					.append(" p1006 , p1007, p1008, p1009, ")
					.append(" p1010 , p1011, p1012, p0105, ")
					.append(" p012 , p0122, p0191, p0201, ")
					.append(" p0306 , p0005, p0025, p0026, ")
					.append(" p0044 , p0057, p0071, p0137, ")
					.append(" p0146 , p0149, p0159, p0170, ")
					.append(" p0176 , p0177, p0189, p0228, ")
					.append(" p0230 , p0262, p0264, p0280, ")
					.append(" p0300 , p0301, p0302, p0358, ")
					.append(" p0359 , p0370, p0372, p0374, ")
					.append(" p0378 , p0380, p0381, p0384, ")
					.append(" p0390 , p0391, p0392, p0393, ")
					.append(" p0394 , p0395, p0396, p0400, ")
					.append(" p0401 , p0402, OPERADOR, datos_adicionales_6, datos_adicionales_7, datos_adicionales_8) ")
					.append(" values (SEQ_TMP_INCOMING.nextval, ?, ?, ? , ")
					.append(" ?, ?, ?, ? ,").append(" ?, ?, ?, ? ,")
					.append(" ?, ?, ?, ? ,").append(" ?, ?, ?, ? ,")
					.append(" ?, ?, ?, ? ,").append(" ?, ?, ?, ? ,")
					.append(" ?, ?, ?, ? ,").append(" ?, ?, ?, ? ,")
					.append(" ?, ?, ?, ? ,").append(" ?, ?, ?, ? ,")
					.append(" ?, ?, ?, ? ,").append(" ?, ?, ?, ? ,")
					.append(" ?, ?, ?, ? ,").append(" ?, ?, ?, ? ,")
					.append(" ?, ?, ?, ? ,").append(" ?, ?, ?, ? ,")
					.append(" ?, ?, ?, ? ,").append(" ?, ?, ?, ? ,")
					.append(" ?, ?, ?, ? ,").append(" ?, ?, ?, ? ,")
					.append(" ?, ?, ?, ? ,").append(" ?, ?, ?, ? ,")
					.append(" ?, ?, ?, ? ,").append(" ?, ?, ?, ? ,")
					.append(" ?, ?, ?, ? ,").append(" ?, ?, ?, ? ,")
					.append(" ?, ?, ?, ?, ?)");

			this.getConnection().setAutoCommit(false);
			stmt = this.getConnection().prepareStatement(str.toString());

			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getCodigoTransaccion(),
					1, stmt);
			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getNumeroTarjeta(),
					2, stmt);
			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getNumeroReferencia(),
					3, stmt);
			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getMontoFuen(),
					4, stmt);
			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00()).getMontoDest(), // se
																							// modifica
					5, stmt);
			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00()).getMontoDest(), // se
																							// modifica
					6, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 7, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 8, stmt);
			setStringNull((DateUtils.getStringDateWithValidationProcess(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getFechaComp(), DateUtils.FECHA_NORMAL)), 9, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 10, stmt);
			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getPosEntryMode(),
					11, stmt);

			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getNumberExt(),
					12, stmt); // se agrega (21-23) gsetNumberExt

			setStringNull((obtieneCodigoTransaccion(((Registro00OutVisaIntDTO) transaccion .getRegistro00()).getCodRazon(),((Registro00OutVisaIntDTO) transaccion.getRegistro00()).getTransCode())), 13,stmt);
			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getCodRazon(),
					14, stmt);
			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getRubroComer(),
					15, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 16, stmt);
			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getNumeroReferencia(),
					17, stmt);
			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getAcqMembId(),
					18, stmt);
			
			
			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getBinAdquiriente(),
					19, stmt); // REGISTRO01 - 30 //bin adquiriente se extraen 6 primeros digitos a getNumeroReferencia
	
			
			setStringNull(
					((Registro01OutVisaIntDTO) transaccion.getRegistro01())
							.getChargRefN(),
					20, stmt); // REGISTRO01 - 05
			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getCodAutor(),
					21, stmt);
			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getPosTerminalCapability(),
					22, stmt);
			setStringNull(
					((Registro01OutVisaIntDTO) transaccion.getRegistro01())
							.getTerminalId(),
					23, stmt);// REGISTRO01
			// REGISTRO01
			setStringNull(((Registro01OutVisaIntDTO) transaccion.getRegistro01()).getCardAcceptorId(),24, stmt);
			
			setStringNull(((Registro00OutVisaIntDTO) transaccion.getRegistro00()).getNombreUbicAcepTarjeta(),25, stmt);
			
			//DATOS_ADICIONALES_1 - ALL EL REGISTRO 03
			if( transaccion.getRegistro03() != null){
				setStringNull(((Registro03OutVisaIntDTO) transaccion.getRegistro03()).getAll(), 26, stmt);
			}else {
				setStringNull(ConstantesUtil.EMPTY.toString(), 26, stmt);
			}
			
			
			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getMonedaFuen(),
					27, stmt);
			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getMonedaDest(), // se modfica getMonedaDest
					28, stmt);
			
			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getMonedaDest(), // se modfica getMonedaDest
					29, stmt);
			
			setStringNull(ConstantesUtil.EMPTY.toString(), 30, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 31, stmt);
			// REGISTRO 05 COMPLETO
			setStringNull(
					(transaccion.getRegistro05() != null) ? ((Registro05OutVisaIntDTO) transaccion.getRegistro05()).getAll()
							: "", 32, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 33, stmt);
			// CORRELATIVO DE LECTURA EL NRO DE TRANSACCION QUE ESTOY
			// LEYENDO
			setStringNull(String.valueOf(transaccion.getNumeroTransaccion()),
					34, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 35, stmt);
			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getFechaProc(),
					36, stmt);
			
			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
							.getCodIdentDestTransc(), // se modifca para recibir 5 oarametros pata formar separados por slash
					37, stmt);
			
			setStringNull(ConstantesUtil.EMPTY.toString(), 38, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 39, stmt);
			// BIN 4 PRIMEROS DIGITOS de tarjeta
			setStringNull(((Registro00OutVisaIntDTO) transaccion.getRegistro00()).getBinTarjeta(),40, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 41, stmt);
			String allRegister07 = (transaccion.getRegistro07() != null) ? ((Registro07OutVisaIntDTO) transaccion
					.getRegistro07()).getAll() : "";
			// 100 primeros bytes del registros completo 07
			setStringNull( (allRegister07.length() > 100) ? allRegister07.substring(0, 100) : allRegister07, 42, stmt);
			// Restantes 70 bytes del registro 07
			setStringNull((allRegister07.length() > 100) ? allRegister07.substring(100, allRegister07.length()) : "", 43, stmt);
			
			//DATOS_ADICIONALES_5 - ALL REGISTRO 04
			if(transaccion.getRegistro04() != null){
				setStringNull(((Registro04OutVisaIntDTO) transaccion.getRegistro04()).getAll(), 44, stmt);
			}else{
				setStringNull(ConstantesUtil.EMPTY.toString(), 44, stmt);
			}
			
			
			//P0023
			setStringNull(
					((Registro01OutVisaIntDTO) transaccion.getRegistro01())
							.getIndicatorTransaction(),
					45, stmt); 
			
			//P0052
			setStringNull(
					((Registro01OutVisaIntDTO) transaccion.getRegistro01())
							.getDocumInd(),
					46, stmt);
			
			//P0148
			setStringNull(
					((Registro00OutVisaIntDTO) transaccion.getRegistro00())
						.getPais(),
						47, stmt);

			       //P0158
			       setStringNull(((Registro01OutVisaIntDTO) transaccion.getRegistro01()).getValorCuota(),48, stmt);
				   
			       //P0165
				   setStringNull( ConstantesUtil.EMPTY.toString(), 49, stmt );
				   
				   //P1000 CUANDO SE PROCESE EL TC46 PUEDE CAMBIAR A LA FECHA 66–72 (TC46 Vol5) CCYYDDD Juliano
				   setStringNull( ConstantesUtil.EMPTY.toString(), 50, stmt );
				   
				   //P1001
				   setStringNull("1", 51, stmt );
				   
				   setStringNull( ConstantesUtil.EMPTY.toString(), 52, stmt );
				   setStringNull( ConstantesUtil.EMPTY.toString(), 53, stmt );
				   setStringNull( ConstantesUtil.EMPTY.toString(), 54, stmt );
				   setStringNull( ConstantesUtil.EMPTY.toString(), 55, stmt );
				   setStringNull( ConstantesUtil.EMPTY.toString(), 56, stmt );
				   setStringNull( ConstantesUtil.EMPTY.toString(), 57, stmt );
				   setStringNull( ConstantesUtil.EMPTY.toString(), 58, stmt );
				   setStringNull( ConstantesUtil.EMPTY.toString(), 59, stmt );
			
				   setStringNull( ConstantesUtil.EMPTY.toString(), 60, stmt );
		            setStringNull( ConstantesUtil.EMPTY.toString(), 61, stmt );
			setStringNull(ConstantesUtil.EMPTY.toString(), 62, stmt);
			
			setStringNull(ConstantesUtil.EMPTY.toString(), 63, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 64, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 65, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 66, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 67, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 68, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 69, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 70, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 71, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 72, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 73, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 74, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 75, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 76, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 77, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 78, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 79, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 80, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 81, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 82, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 83, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 84, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 85, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 86, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 87, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 88, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 89, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 90, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 91, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 92, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 93, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 94, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 95, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 96, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 97, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 98, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 99, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 100, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 101, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 102, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 103, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 104, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 105, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 106, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 107, stmt);
			setStringNull(ConstantesUtil.EMPTY.toString(), 108, stmt);
			stmt.setInt(109, sidOperador);
			if(transaccion.getRegistro06() != null){
				setStringNull(((Registro06OutVisaIntDTO) transaccion.getRegistro06()).getAll(), 110, stmt);
			}else{
				setStringNull(ConstantesUtil.EMPTY.toString(), 110, stmt);
			}
			
if(transaccion.getRegistro0D() != null){
	setStringNull(((Registro0DOutVisaIntDTO) transaccion.getRegistro0D()).getAll(), 111, stmt);		
			}else{
				setStringNull(ConstantesUtil.EMPTY.toString(), 111, stmt);
			}
			
if(transaccion.getRegistro2D() != null){
	setStringNull(((Registro2DOutVisaIntDTO) transaccion.getRegistro2D()).getAll(), 112, stmt);	
			}else{
				setStringNull(ConstantesUtil.EMPTY.toString(), 112, stmt);
			}
			
			
			return (stmt.executeUpdate() > 0);
		} finally {
			if (stmt != null) {
				stmt.close();
			}
		}
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 - 25/04/2019, (ACL) - versión inicial
	 * </ul>
	 * <p>
	 * VALIDA EL SI EL CODIGO DE RAZON ES MAYOR QUE 0
	 * 
	 * @param codRazon
	 * @param transcode
	 * @return
	 * @since 1.X
	 */
	private String obtieneCodigoTransaccion(String codRazon, String transcode) {
		Integer iCodRazon = Integer.parseInt(codRazon);
		if (iCodRazon > 0) {
			return "205";
		}

		return transcode;
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 25/04/2019, (Everis Chile) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * LLAMA AL PACKAGE PARA COPIAR EL CONTENIDO DESDE LA TMP A LA TBL
	 * 
	 * @return
	 * @throws SQLException
	 * @see com.sbpay.sgi.cron.dao.ProcesarIncomingDao#callSPCargaIncoming()
	 * @since 1.X
	 */
	@Override
	public boolean callSPCargaIncoming() throws SQLException {
		Connection dbConnection = null;
		OracleCallableStatement callableStatement = null;

		try {
			String callSPCargaIncomingSql = "{call SP_SIG_CARGAINCOMING(?,?)}";

			dbConnection = getConnection();
			callableStatement = (OracleCallableStatement) dbConnection
					.prepareCall(callSPCargaIncomingSql);

			callableStatement.registerOutParameter(1, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(2, java.sql.Types.NUMERIC);

			callableStatement.executeUpdate();

			String warning = callableStatement.getString(1);
			Integer codigoError = callableStatement.getInt(2);
			LOGGER.info("******SE EJECUTA STORE PROCEDURE SP_SIG_CARGAINCOMING*****");
			LOGGER.info("WARNING : " + warning);
			LOGGER.info("codigoError : " + codigoError);

		} finally {
			if (callableStatement != null) {
				callableStatement.close();
			}
		}
		return Boolean.TRUE;
	}

}
