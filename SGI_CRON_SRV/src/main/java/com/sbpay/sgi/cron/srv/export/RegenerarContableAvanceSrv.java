package com.sbpay.sgi.cron.srv.export;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dao.RegenerarContableAvanceDao;
import com.sbpay.sgi.cron.dao.RegenerarContableAvanceDaoFactory;
import com.sbpay.sgi.cron.dto.ParamExportReCnblDTO;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.dto.ReContableAvanceLogDTO;
import com.sbpay.sgi.cron.dto.TmpExportContableDTO;
import com.sbpay.sgi.cron.enums.ConstantesPRTS;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.MsgErrorMail;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.enums.MsgRegenerationCntblAvances;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.srv.utils.CommonSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;
import com.sbpay.sgi.cron.utils.file.ContableExport;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;
import com.sbpay.sgi.cron.validation.ProcessValidator;
import com.sbpay.sgi.cron.validation.impl.ProcessValidatorImpl;

public class RegenerarContableAvanceSrv extends CommonSrv {
	/** The Constant LOGGER. */
	private static final Logger LOGGER = Logger
			.getLogger(RegenerarContableAvanceSrv.class);
	/** The single instance. */
	private static RegenerarContableAvanceSrv singleINSTANCE = null;
	/** variable parametros del Cron */

	private final ProcessCodeType PROCESO_ACTUAL = ProcessCodeType.REGENERAR_ARCHIVO_CONTABLE_AVA;

	/**
	 * Creates the instance.
	 */
	private static void createInstance() {
		synchronized (RegenerarContableAvanceSrv.class) {
			if (singleINSTANCE == null) {
				singleINSTANCE = new RegenerarContableAvanceSrv();

			}
		}
	}

	/**
	 * Patron singleton.
	 * 
	 * @return RegenerarContableAvanceSrv retorna instancia del servicio.
	 */
	public static RegenerarContableAvanceSrv getInstance() {
		if (singleINSTANCE == null) {
			createInstance();
		}
		return singleINSTANCE;
	}

	/**
	 * Metodo de restricion de sobreescritura de la clase.
	 */
	@Override
	protected Object clone() throws CloneNotSupportedException {
		super.clone();
		throw new CloneNotSupportedException();
	}

	/**
	 * Constructor Privado.
	 */
	private RegenerarContableAvanceSrv() {
		super();
	}

	/**
	 * 
	 * @return
	 * @throws AppException
	 */
	public boolean regenerarArchivoContableAvances() throws AppException {
		Boolean flag = Boolean.FALSE;
		RegenerarContableAvanceDao regenerarContableAvanceDao = null;
		ParamExportReCnblDTO ParamExportReCnblDTO = null;

		try {
			ProcessValidator validadorProceso = new ProcessValidatorImpl();

			if (validadorProceso.isActiveProcess(PROCESO_ACTUAL)) {

				ParamExportReCnblDTO = getParametrosUploadCntbl();
				if (!validadorProceso.hasTheValidParametersForTheProcess(
						PROCESO_ACTUAL, ParamExportReCnblDTO)) {
					LOGGER.warn(MsgRegenerationCntblAvances.ERROR_PARAM_NOT_FOUND
							.toString());
					MailSendSrv.getInstance().sendMail(
							MsgErrorMail.ALERTA_PRTS.toString(),
							MsgErrorMail.ALERTA_PRTS_TXT.toString());
				}

				//LOGGER.info("=== SE ENCONTRARON TODOS LOS PARAMETROS NECESARIOS PARA EL PROCESO, SE INICIA REGENERACION DE ASIENTOS CONTABLES DE AVANCES CON TRANSFERENCIA... ===");

				regenerarContableAvanceDao = RegenerarContableAvanceDaoFactory
						.getInstance().getNewEntidadDao();

				// METODO QUE BUSQUE ARCHIVOS A REGENERAR
				List<ReContableAvanceLogDTO> lstArchivosReprocesar = regenerarContableAvanceDao
						.getDataRegenerar();

				// se validaq ue exitan archivo a regenerar
				if (lstArchivosReprocesar != null
						&& lstArchivosReprocesar.size() > 0) {

					//LOGGER.info("SI EXISTEN ARCHIVOS PENDIENTES PARA REGENERAR");
					// ACTUALIZA REGISTROS EN BD A REGENERAR EN TBL_CONC_AVANCE
					for (ReContableAvanceLogDTO lista : lstArchivosReprocesar) {
						regenerarContableAvanceDao
								.updateDataContableAvances(lista
										.getSidContable());

						// LLAMA AL SP_SIG_RECONTABLE_MAIN_AVA
						if (regenerarContableAvanceDao
								.cargarInformacionContableAvance(lista
										.getFecha())) {
							//LOGGER.info("SE HA LLAMADO AL PROCEDIMIENTO SP_SIG_RECONTABLE_MAIN_AVA CORRECTAMENTE");
						}

						// crea lista contable envia fecha
						List<TmpExportContableDTO> lstTmpExportContableDTO = regenerarContableAvanceDao
								.obtenerInformacionCnblt(lista.getFecha());

						// valida lista
						if (lstTmpExportContableDTO != null
								&& lstTmpExportContableDTO.size() > 0) {

							// generar archivos
							String cntblFilename = generateContableFilename(
									ParamExportReCnblDTO, lista.getFecha());
							String cntblFilenameCtr = generateControlFilename(
									ParamExportReCnblDTO, lista.getFecha());
							String ruta = ParamExportReCnblDTO.getPathSalida();
							String rutaError = ParamExportReCnblDTO
									.getPathError();

							ContableExport salida = new ContableExport();

							// salida de archivos, si existe elimina y crea el
							// otro
							salida.exportContableFileReg(ruta, cntblFilename,
									cntblFilenameCtr, lstTmpExportContableDTO);

							// elimina de carpeta error si existe
							salida.exportContableFileRegDelete(rutaError,
									cntblFilename, cntblFilenameCtr);

							// inserta en tabla de historia contable para
							// avances
							regenerarContableAvanceDao
									.savehistoriaContable(lista
											.getSidContable());

							// limpiar tbl_contable
							regenerarContableAvanceDao.clearTmpContable(lista
									.getFecha());

							// despues de update, obtener, cargar y crear se
							// actualiza tbl_log_contable a estado 3
							regenerarContableAvanceDao.updateTablelog(lista
									.getSidContable());

							flag = true;
						} else {
							LOGGER.warn(MsgRegenerationCntblAvances.WARNING_DATA_NOT_FOUND
									.toString());

							String asunto = MsgRegenerationCntblAvances.ERROR_GEN_CNTBL_FILE
									.toString();
							String mensaje = MsgRegenerationCntblAvances.WARNING_DATA_NOT_FOUND
									.toString();

							MailSendSrv.getInstance().sendMail(asunto, mensaje);

							flag = false;
						}

					}

				} else {
					/*LOGGER.warn(MsgRegenerationCntblAvances.WARNING_DATA_NOT_FOUND_LOG
							.toString());
					
					MailSendSrv.getInstance().sendMail(
							MsgRegenerationCntblAvances.ERROR_GEN_CNTBL_FILE.toString(),
							MsgRegenerationCntblAvances.WARNING_DATA_NOT_FOUND_LOG.toString());*/

					flag = false;
				}

				return flag;

			} else {
				String asunto = MsgProceso.MSG_PROCESO_DESACTIVADO_ASUNTO
						.toString().replace(":?:",
								PROCESO_ACTUAL.getCodigoProceso());
				String mensaje = MsgProceso.MSG_PROCESO_DESACTIVADO_BODY
						.toString().replace(":?:",
								PROCESO_ACTUAL.getCodigoProceso());
				MailSendSrv.getInstance().sendMail(asunto, mensaje);
			}

		} catch (SQLException e) {
			String asunto = MsgRegenerationCntblAvances.ERROR_GEN_CNTBL_FILE
					.toString();
			String mensaje = e.toString();

			MailSendSrv.getInstance().sendMail(asunto, mensaje);

			throw new AppException(
					MsgRegenerationCntblAvances.ERROR_CREATE_CONTABLE_FILE
							.toString(),
					e);

		} finally {
			if (regenerarContableAvanceDao != null) {
				try {
					regenerarContableAvanceDao.close();
				} catch (SQLException e) {
					String asunto = MsgRegenerationCntblAvances.ERROR_GEN_CNTBL_FILE
							.toString();
					String mensaje = e.toString();

					MailSendSrv.getInstance().sendMail(asunto, mensaje);
					throw new AppException(e.getMessage(), e);
				}
			}
		}
		return flag;

	}

	/**
	 * 
	 * @param parametros
	 * @return
	 */
	private String generateControlFilename(ParamExportReCnblDTO parametros,
			String date) {
		StringBuilder str = new StringBuilder();
		str.append(parametros.getFormatFileNameOnewCtr()).append(date)
				.append(ConstantesUtil.POINT)
				.append(parametros.getFormatExtNameOnewCtr());
		return str.toString();
	}

	/**
	 * 
	 * @param parametros
	 * @return
	 */
	private String generateContableFilename(
			final ParamExportReCnblDTO parametros, String date) {
		StringBuilder str = new StringBuilder();
		str.append(parametros.getNomArchivo()).append(date)
				.append(ConstantesUtil.POINT).append(parametros.getExtension());
		return str.toString();
	}

	/**
	 * Metodo busca los parametros del proceso que genera el archivo Contable.
	 * 
	 * @return Dto con los parametros utilizados.
	 * @throws SQLException
	 *             Exception SQL.
	 */
	private ParamExportReCnblDTO getParametrosUploadCntbl() throws SQLException {
		CommonsDao daoCommon = null;
		List<ParametroDTO> paramDTOList = null;
		ParamExportReCnblDTO paramPldCnblDTO = null;
		try {
			daoCommon = CommonsDaoFactory.getInstance().getNewEntidadDao();
			paramDTOList = daoCommon
					.getParametroCodGrupoDato(ConstantesPRTS.CRON_TBK
							.toString());
			if (paramDTOList != null) {
				paramPldCnblDTO = new ParamExportReCnblDTO();
				paramPldCnblDTO.setPathSalida(CommonsUtils.getCodiDato(
						paramDTOList,
						ConstantesPRTS.PATH_TRX_REG_CONTABLE_AVA.toString()));
				paramPldCnblDTO.setNomArchivo(CommonsUtils.getCodiDato(
						paramDTOList,
						ConstantesPRTS.FORMAT_FILE_REG_ACCOUNT_FILE_AVA
								.toString()));
				paramPldCnblDTO.setExtension(CommonsUtils.getCodiDato(
						paramDTOList,
						ConstantesPRTS.FORMAT_EXT_REG_ACCONT_FILE_AVA
								.toString()));
				paramPldCnblDTO
						.setFormatFileNameOnewCtr(CommonsUtils
								.getCodiDato(
										paramDTOList,
										ConstantesPRTS.FORMAT_FILE_REG_ACCOUNT_FILE_CTR_AVA
												.toString()));
				paramPldCnblDTO
						.setFormatExtNameOnewCtr(CommonsUtils
								.getCodiDato(
										paramDTOList,
										ConstantesPRTS.FORMAT_EXT_REG_ACCOUNT_FILE_CTR_AVA
												.toString()));
				paramPldCnblDTO.setPathError(CommonsUtils.getCodiDato(
						paramDTOList,
						ConstantesPRTS.PATH_TRX_REG_CONTABLE_ERROR_AVA
								.toString()));

			} else {
				paramPldCnblDTO = null;
			}
			return paramPldCnblDTO;
		} finally {

			if (daoCommon != null) {
				daoCommon.close();
			}

		}
	}
}
