package com.sbpay.sgi.cron.srv.process;

import java.io.File;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.ComisionesDao;
import com.sbpay.sgi.cron.dao.ComisionesDaoFactory;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dto.ComisionesPagoDTO;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.dto.ParamsGenerateComisionesDTO;
import com.sbpay.sgi.cron.dto.TblCronLogDTO;
import com.sbpay.sgi.cron.enums.ConstantesPRTS;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.FilterTypeSearch;
import com.sbpay.sgi.cron.enums.LogBD;
import com.sbpay.sgi.cron.enums.MsgErrorMail;
import com.sbpay.sgi.cron.enums.MsgErrorProcessFileComision;
import com.sbpay.sgi.cron.enums.MsgErrorSQL;
import com.sbpay.sgi.cron.enums.MsgExitoMail;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.enums.StatusProcessType;
import com.sbpay.sgi.cron.srv.utils.CommonSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;
import com.sbpay.sgi.cron.utils.file.ComisionesFileUtil;
import com.sbpay.sgi.cron.utils.file.ComisionesReader;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;
import com.sbpay.sgi.cron.validation.ProcessValidator;
import com.sbpay.sgi.cron.validation.impl.ProcessValidatorImpl;

public class ProcesarComisionesSrv extends CommonSrv {

	private static final Logger LOGGER = Logger.getLogger(ProcesarComisionesSrv.class);

	private static ProcesarComisionesSrv singleINSTANCE = null;
	
	private final ProcessCodeType PROCESO_ACTUAL = ProcessCodeType.PROCESAR_COMISIONES;

	private static void createInstance() {
		synchronized (ProcesarComisionesSrv.class) {
			if (singleINSTANCE == null)
				singleINSTANCE = new ProcesarComisionesSrv();
		}
	}

	public static ProcesarComisionesSrv getInstance() {
		if (singleINSTANCE == null)
			createInstance();
		return singleINSTANCE;
	}

	/**
	 * Metodo de restricion de sobreescritura de la clase.
	 */
	@Override
	protected Object clone() throws CloneNotSupportedException {
		super.clone();
		throw new CloneNotSupportedException();
	}

	private ProcesarComisionesSrv() {
		super();
	}

	/**
	 * Metodo orquesta el procesamiento de comisiones desde un archivo
	 * descargado de transbank.
	 * @throws Exception 
	 */
	public void procesarComisiones() throws Exception {
	    
	    ProcessValidator validadorProceso = new ProcessValidatorImpl();

	    if (validadorProceso.isActiveProcess(PROCESO_ACTUAL)) {
	    
		// BUSCAR LOS PARAMETROS NECESARIOS PARA EL FILE
		ComisionesFileUtil comisionesFileUtil = new ComisionesReader();
		ParamsGenerateComisionesDTO parametros = getParametros();
		List<File> listOfFiles = null;
		int countFileProcessed = 0;
				
		if(validadorProceso.hasTheValidParametersForTheProcess( PROCESO_ACTUAL, parametros )){
		/*
		 * RECORRE LOS ARCHIVOS DEL FOLDER BUSCANDO ARCHIVOS COMISIONES SE
		 * VALIDA SI TIENE REGISTRO EN TBL_LOG_COMISION Y SI ESTA O NO PROCESADO
		 * (FLAG 1 : PROCESADO, FLAG -1: ERROR, FLAG : 0 )
		 */

		listOfFiles = CommonsUtils.getFilesInFolder( parametros.getPathTbkComision(), parametros.getFormatFileTbkComision(), parametros.getFormatExtNameTbkComision());

		LOGGER.debug("Lista de archivos :  " + ((listOfFiles != null) ? "" + listOfFiles.size() : "No se encontraron archivos"));

		// SI NO EXISTE NINGUN ARCHIVO EN LA CARPETA TERMINA EL
		// PROCESO
		if (listOfFiles == null) {
			LOGGER.warn(MsgErrorProcessFileComision.WARNING_NO_FILES_FOUNDS.toString());
			return;
		}

		for (File file : listOfFiles) {
			if (file.isFile() && file.getName().startsWith(parametros.getFormatFileTbkComision())
					&& isValidCommission(file.getName()) && comisionesFileUtil.validarArchivo(file)) {
				try {
					LOGGER.info("Archivo Comisiones válido : Se comienza a procesar...");
					// 1 - PROCESA ARCHIVO COMISIONES Y LUEGO GUARDA LOS REGISTROS Y ACTUALIZA EL LOG
					actualizarLog(comisionesFileUtil.readComisionesLine(file), file.getName());
					
					// 2 - SE MUEVE INCOMING A LA CARPETA BACKUP
				    CommonsUtils.moveFile( file.getAbsolutePath(),parametros.getPathTbkComBkp() + file.getName() );
				    LOGGER.info("Archivo Comisiones procesado, se mueve a backup ...");
				    
					countFileProcessed++;
				} catch (AppException e) {
					// ERROR AL PROCESAR INCOMING SE ENVIA EMAIL
					LOGGER.error(MsgErrorProcessFileComision.ERROR_IN_PROCESS.toString(), e);
					MailSendSrv.getInstance().sendMail(MsgErrorProcessFileComision.ERROR_IN_PROCESS.toString(), e.getStackTraceDescripcion());
					// MUEVE ARCHIVO A LA CARPETA DE ERROR
					CommonsUtils.moveFile(file.getAbsolutePath(), parametros.getPathTbkComErr() + file.getName());
					LOGGER.info("Archivo Comisiones no se ha proceso, se mueve a carpeta de error ...");
				}
			}else{
				LOGGER.info("Archivo Comisiones no cumple parametros isValidCommission comisionesFileUtil.validarArchivo");
				CommonsUtils.moveFile(file.getAbsolutePath(), parametros.getPathTbkComErr() + file.getName());
				LOGGER.info("Archivo Comisiones no se ha proceso, se mueve a carpeta de error ...");
			}
		}
		
		String msj = "";
		// SI NO SE PROCESA NINGUN FILE ESCRIBE ESCRIBE LOG
		if (countFileProcessed == 0) {
			LOGGER.warn(MsgErrorProcessFileComision.WARNING_NO_FILE_PROCESSING_TITLE
					+ "  - "
					+ MsgErrorProcessFileComision.WARNING_NO_FILE_PROCESSING_CAUSE);
			msj = "Sin embargo, " + MsgErrorProcessFileComision.WARNING_NO_FILE_PROCESSING_CAUSE.toString().toLowerCase();
			
		}

		// SI SE PROCESARON TODOS LOS ARCHIVOS EXISTENTES SE ENVIA
		// MAIL CORRECTO
		if (listOfFiles.size() == countFileProcessed) {
			LOGGER.info("Envio Correo de Proceso  Satisfactorio: Procesamiento Archivo Comisiones Transbank ");
			if(!msj.isEmpty()){
				MailSendSrv.getInstance().sendMailOk( MsgExitoMail.EXITO_PROCESS_COMISION_TITLE.toString(), MsgExitoMail.EXITO_PROCESS_COMISION_BODY.toString().concat(ConstantesUtil.SKIP_LINE.toString()).concat(msj));
			}else{
				MailSendSrv.getInstance().sendMailOk( MsgExitoMail.EXITO_PROCESS_COMISION_TITLE.toString(), MsgExitoMail.EXITO_PROCESS_COMISION_BODY.toString());
			}
		}
		
		}else{
			LOGGER.error("Parametros de Base de Datos Incompletos Proceso : Procesar comisiones");
			MailSendSrv.getInstance().sendMail(
					MsgErrorMail.ALERTA_RCH_PRTS.toString(),
					MsgErrorMail.ALERTA_RCH_PRTS_TXT.toString());
		}
	    }else{
	        String asunto = MsgProceso.MSG_PROCESO_DESACTIVADO_ASUNTO
                    .toString().replace( ":?:",
                            PROCESO_ACTUAL.getCodigoProceso() );
            String mensaje = MsgProceso.MSG_PROCESO_DESACTIVADO_BODY.toString()
                    .replace( ":?:", PROCESO_ACTUAL.getCodigoProceso() );
            MailSendSrv.getInstance().sendMail( asunto, mensaje );

	    }
	    

	}

	/**
	 * 
	 * @param object
	 * @param fileName
	 * @throws Exception 
	 */
	private void actualizarLog(List<ComisionesPagoDTO> listaComisionesPagoDTO, String fileName) throws Exception {
		CommonsDao commonsDAO = null;
		ComisionesDao comisionesDao = null;
		TblCronLogDTO tblCronLogDTO = null;
		try {

			commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
			comisionesDao = ComisionesDaoFactory.getInstance().getNewEntidadDao();
			
			tblCronLogDTO = new TblCronLogDTO();
			tblCronLogDTO.setFilename(fileName);
			tblCronLogDTO.setFileFlag(StatusProcessType.PROCESS_SUCESSFUL.getValue());
			// SE REALIZA EL UPDATE
			if (commonsDAO.updateTblCronLogByType(tblCronLogDTO, LogBD.TABLE_LOG_COMISION.getTable(), FilterTypeSearch.FILTER_BY_NAME)) {
				LOGGER.info( "******SE EJECUTA STORE PROCEDURE SP_SIG_GUARDARCOMISIONES *****" );
				for(ComisionesPagoDTO comisionesPagoDTO : listaComisionesPagoDTO){
					comisionesDao.guardarComision(comisionesPagoDTO);
				}
				comisionesDao.endTx();
				commonsDAO.endTx();
			} else {
				// ROLLBACK CARGA UPDATE FLAG
				rollBackTransaccion(comisionesDao);
				rollBackTransaccion(commonsDAO);
			}
		} catch (SQLException e) {
			rollBackTransaccion(commonsDAO);
			throw new AppException(MsgErrorSQL.ERROR_UPDATE_TBL_LOG.toString()
					+ LogBD.TABLE_LOG_COMISION.getTable(), e);
		} finally {
			try {
				if (commonsDAO != null) {
					commonsDAO.close();
				}
				if (comisionesDao != null) {
					comisionesDao.close();
				}
			} catch (SQLException e) {
				rollBackTransaccion(commonsDAO);
				throw new AppException(
						MsgErrorSQL.ERROR_UPDATE_TBL_LOG.toString()
								+ LogBD.TABLE_LOG_COMISION.getTable(), e);
			}
		}
	}

	private ParamsGenerateComisionesDTO getParametros() throws AppException {
		ParamsGenerateComisionesDTO parametros = null;
		List<ParametroDTO> paramDTOList = null;
		CommonsDao commonsDAO = null;

		try {
			// INICIA EL DAO
			commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
			// SE OBTIENEN TODOS LOS CODIGOS
			paramDTOList = commonsDAO.getParametroCodGrupoDato(ConstantesPRTS.CRON_TBK.toString());
			LOGGER.info("LISTA DE PARAMETROS : " + paramDTOList.size());

			// SI EXISTEN PARAMETROS EN BD SE EXTRAEN
			if (paramDTOList != null) {

				// PARAMETROS DE COMISIONES
				parametros = new ParamsGenerateComisionesDTO();

				parametros.setFormatFileTbkComision(CommonsUtils.getCodiDato(paramDTOList, ConstantesPRTS.FORMAT_FILE_TBK_COM.toString()));
				parametros.setFormatExtNameTbkComision(CommonsUtils.getCodiDato(paramDTOList, ConstantesPRTS.FORMAT_EXTNAME_TBK_COM.toString()));
				parametros.setPathTbkComision(CommonsUtils.getCodiDato(paramDTOList, ConstantesPRTS.PATH_TBK_COM.toString()));
				parametros.setPathTbkComBkp(CommonsUtils.getCodiDato(paramDTOList, ConstantesPRTS.PATH_TBK_COM_BKP.toString()));
				parametros.setPathTbkComErr(CommonsUtils.getCodiDato(paramDTOList, ConstantesPRTS.PATH_TBK_COM_ERR.toString()));

			}
			LOGGER.info("Parametros  BD : " + parametros);
			return parametros;
		} catch (SQLException e) {
			throw new AppException(MsgErrorSQL.ERROR_QUERY_PRTS.toString(), e);
		} finally {
			try {
				if (commonsDAO != null) {
					commonsDAO.close();
				}
			} catch (SQLException e) {
				throw new AppException(
						MsgErrorSQL.ERROR_QUERY_PRTS_CLOSE.toString(), e);
			}
		}
	}

	/**
	 * Metodo valida si el archivo mensual de comisiones fue descargado.
	 * 
	 * @param filename
	 *            nombre del archivo de comisiones.
	 * @return true: si el archivo no ha sido descargado false: archivo
	 *         descargado.
	 * @throws AppException
	 */
	private boolean isValidCommission(String filename) throws AppException {
		CommonsDao commonsDAO = null;
		try {
			commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();

			// BUSCA REGISTRO LOG POR NOMBRE
			LOGGER.info("Nombre Comision: " + filename);
			TblCronLogDTO filter = new TblCronLogDTO();
			filter.setFilename(filename);

			TblCronLogDTO cronlog = commonsDAO.getTblCronLogByType(filter, LogBD.TABLE_LOG_COMISION.getTable(), FilterTypeSearch.FILTER_BY_NAME);
			LOGGER.info("=====> Valor de TABLE_LOG_COMISION: " + cronlog);

			if (cronlog == null) {
				LOGGER.error(MsgErrorProcessFileComision.ERROR_COMISION_NAME_NOT_FOUND.toString());
				return Boolean.FALSE;
			}
			// SI ES DISTINTO DE 0 NO ES VÁLIDO
			else if (!StatusProcessType.PROCESS_PENDING.getValue().equalsIgnoreCase(cronlog.getFileFlag())) {
				LOGGER.info(MsgErrorProcessFileComision.ERROR_INVALID_COMISION_STATE.toString());
				return Boolean.FALSE;
			}

		} catch (SQLException e) {
			throw new AppException(
					MsgErrorSQL.ERROR_FIND_COMISIONES_BY_NAME.toString(), e);
		} finally {

			try {
				if (commonsDAO != null) {
					commonsDAO.close();
				}
			} catch (SQLException e) {
				throw new AppException(
						MsgErrorSQL.ERROR_FIND_COMISIONES_BY_NAME.toString(), e);
			}
		}

		return Boolean.TRUE;
	}
	
	
	
}
