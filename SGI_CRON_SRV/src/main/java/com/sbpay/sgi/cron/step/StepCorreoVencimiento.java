package com.sbpay.sgi.cron.step;


import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.enums.MsgExitoMail;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.srv.utils.BodyMailSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;

public class StepCorreoVencimiento {
	
    private static final Logger LOG = Logger.getLogger( StepCorreoVencimiento.class );
    
    /**
     * 
     * Ejecuta la obtención de la información de transacciones que estan a punto de caducar o cadan el mismo día.
     * @throws Exception
     * @since 1.X
     */
    public void execute() throws AppException {
        try {
            String cuerpoCorreo = BodyMailSrv.getInstance().crearCuerpoCorreo();
            LOG.info( "Inicio Proceso:  Correo de vencimiento transacciones" );
            if ( cuerpoCorreo != null ) {
                LOG.info( "Envio Correo de Descarga Satisfactoria: Proceso DownLoad Comisiones" );
                MailSendSrv.getInstance().sendMailOkHTML(MsgExitoMail.EXITO_PROCESS_CORREO_VENCIMIENTO.toString(),cuerpoCorreo );
            }
            else {
                LOG.info( "Finaliza Proceso :Carga de vencimmiento de transacciones, no se realiza" );
            }
        }
        catch ( AppException e ) {
            LOG.error( e.getMessage(), e );
            MailSendSrv.getInstance().sendMail(
                    MsgProceso.MSG_GENERIC_ERROR_MSJ.toString(),
                    ( e.getStackTraceDescripcion() != null ) ? e
                            .getStackTraceDescripcion() : e.getMessage() );
        }
        
    }
}
