package com.sbpay.sgi.cron.step;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.enums.MsgExitoMail;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.srv.upload.UploadAsientosCntblAvanceSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;


public class StepUploadAsientosCntblAvance {
    
    /** The Constant LOGGER. */
    private static final Logger LOG = Logger.getLogger( StepUploadAsientosCntblAvance.class );
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/12/2018, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws Exception
     * @since 1.X
     */
    public void execute() throws Exception {
        try {
            LOG.info( "Inicio Upload Asientos contables " );
            if ( UploadAsientosCntblAvanceSrv.getInstance().uploadAsientosContablesAvances() ) {
                /*MailSendSrv.getInstance().sendMailOk( MsgExitoMail.EXITO_PROCESS_UPLOAD_TITLE_CNTBL_AVA.toString(), MsgExitoMail.EXITO_PROCESS_UPLOAD_CNTBL_AVA.toString() );*/
            	LOG.info( MsgExitoMail.EXITO_PROCESS_UPLOAD_CNTBL_AVA.toString());
            }
            else {
                LOG.info( "Upload Asientos Contables de avances no ejecutado correctamente!" );
            }
        }
        catch ( AppException e ) {
            LOG.error( e.getMessage(), e );
            MailSendSrv.getInstance().sendMail(
                    MsgProceso.MSG_GENERIC_ERROR_MSJ.toString(),
                    ( e.getStackTraceDescripcion() != null ) ? e
                            .getStackTraceDescripcion() : e.getMessage() );
        }
    }
}
