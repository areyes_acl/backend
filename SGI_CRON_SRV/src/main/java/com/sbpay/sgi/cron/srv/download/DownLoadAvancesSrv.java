package com.sbpay.sgi.cron.srv.download;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dto.ConfigFile;
import com.sbpay.sgi.cron.dto.DataFTP;
import com.sbpay.sgi.cron.dto.ParamCronAvancesDTO;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.dto.TblCronLogDTO;
import com.sbpay.sgi.cron.enums.ConstantesPRTS;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.LogBD;
import com.sbpay.sgi.cron.enums.MsgErrorMail;
import com.sbpay.sgi.cron.enums.MsgErrorSQL;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.enums.StatusProcessType;
import com.sbpay.sgi.cron.srv.utils.ClientFTPSrv;
import com.sbpay.sgi.cron.srv.utils.CommonSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;
import com.sbpay.sgi.cron.utils.generals.DateUtils;
import com.sbpay.sgi.cron.validation.ProcessValidator;
import com.sbpay.sgi.cron.validation.impl.ProcessValidatorImpl;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 DD/10/2018, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * Clase encargada de descargar el archivo de avances  y dejarlo en la
 * carpeta de AVANCES/DESTINO de ABC-DIN.
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public final class DownLoadAvancesSrv extends CommonSrv {
	/** The Constant LOGGER. */
	private static final Logger LOG = Logger
			.getLogger(DownLoadAvancesSrv.class.getName());

	/** The single instance. */
	private static DownLoadAvancesSrv singleINSTANCE = null;

	private final ProcessCodeType PROCESO_ACTUAL = ProcessCodeType.DESCARGAR_AVANCES;

	/**
	 * Creates the instance.
	 */
	private static void createInstance() {
		synchronized (DownLoadAvancesSrv.class) {
			if (singleINSTANCE == null) {
				singleINSTANCE = new DownLoadAvancesSrv();
			}
		}
	}

	/**
	 * Patron singleton.
	 * 
	 * @return DownLoadAvancesSrv retorna instancia del servicio.
	 */
	public static DownLoadAvancesSrv getInstance() {
		if (singleINSTANCE == null) {
			createInstance();
		}
		return singleINSTANCE;
	}

	/**
	 * Metodo de restricion de sobreescritura de la clase.
	 */
	@Override
	protected Object clone() throws CloneNotSupportedException {
		super.clone();
		throw new CloneNotSupportedException();
	}

	/**
	 * Constructor Privado.
	 */
	public DownLoadAvancesSrv() {
		super();
	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 DD/10/2018, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * Metodo principal controla la logica de carga del archivo avances de avances
	 * a sbpay
	 * 
	 * @return flag de carga realziada true: OK false: ERROR
	 * @throws AppException
	 *
	 * @throws AppException
	 * @since 1.X
	 */
	public boolean descargarAvances() throws AppException {

		boolean flag = Boolean.FALSE;
		ParamCronAvancesDTO parametros = null;
		List<String> listFTP = null;
		List<String> listToDownload = null;
		int countDownloadProcess = 0;
		String errorFile = null;
		String formatoFile = "";

		ProcessValidator validadorProceso = new ProcessValidatorImpl();

		// VALIDA SI PROCESO SE ENCUENTRA ACTIVADO/DESACTIVADO
		if (validadorProceso.isActiveProcess(PROCESO_ACTUAL)) {
			parametros = getParametros();
			
			// VALIDA PARAMETROS DE PROCESO
			if (validadorProceso.hasTheValidParametersForTheProcess(
					PROCESO_ACTUAL, parametros)) {
				LOG.info("Se encontraron todos los parametros necesarios para iniciar el proceso ");

				// OBTENER NOMBRES ARCHIVOS A DESCARGAR
				listFTP = ClientFTPSrv.getInstance().listFileSFTP(
						parametros.getDataFTP(), parametros.getFtpPathAvances());

				LOG.info("lista desde SFTP" + listFTP);

				formatoFile = DateUtils.getDateTodayInYYYYMMDDAnterior()
						.concat(ConstantesUtil.UNDERSCORE.toString())
						.concat(parametros.getFormatFilenameAvancesCorre())
						.concat(ConstantesUtil.UNDERSCORE.toString())
						.concat(parametros.getFormatFilenameAvances());

				// VALIDAR FORMATOS NOMBRES ARCHIVOS
				ConfigFile configFile = new ConfigFile();
				configFile.setStarWith(formatoFile);

				// FILTRA POR ARCHIVO POR NOMBRE DE
				// ARCHIVOS
				listToDownload = retrieveFilenameListToDownloadAvances(listFTP,
						configFile);
				
				LOG.info("Fecha de archivo a buscar "+ DateUtils.getDateTodayInYYYYMMDDAnterior());
				LOG.info("Lista de archivos a descargar: " + listToDownload);

				// SI EXISTE ALGUN ARCHIVO VALIDO CONTINUA PROCESO
				if (listToDownload != null && listToDownload.size() > 0) {

					// FILTRAR ARCHIVOS QUE NO HAN SIDO DESCARGADOS
					// ANTERIORMENTE
					listToDownload = currentlyNotDownloadedFilesAvances(
							listToDownload, configFile.getStarWith(),
							LogBD.TABLE_LOG_AVANCES);

					LOG.info("Lista de archivos de avances que no han sido descargados con anterioridad : "
							+ listToDownload);

					// VALIDACION QUE EXISTA ALGUNO
					if (listToDownload != null && listToDownload.size() > 0) {
						errorFile = ConstantesUtil.EMPTY.toString();
						for (String filename : listToDownload) {
							try {
								// DESCARGAR DESDE FTP
								String pathFtpOri = parametros.getFtpPathAvances();
								String pathDest = parametros.getPathAclAvances();

								LOG.info("Comienza descarga de archivo "
										+ filename + " desde ftp : "
										+ parametros.getDataFTP().getIpHost());

								ClientFTPSrv.getInstance().downloadFileSFTP(
										parametros.getDataFTP(), pathFtpOri,
										pathDest, filename);

								// INSERTA EN LA TBL_LOG_AVANCES
								save(filename, configFile.getStarWith());

								// CUENTA LOS REGISTROS INSERTADOS
								countDownloadProcess++;
							} catch (AppException e) {
								LOG.error(
										"Ha ocurrido un error al intentar descargar el archivo : "
												+ filename, e);
								errorFile = errorFile.concat(filename).concat(
										ConstantesUtil.SKIP_LINE.toString());
							}
						}
					} else {
						LOG.warn("No existen nuevos archivos de avances que descargar desde SFTP, todos han sido procesados anteriormente");
						MailSendSrv
								.getInstance()
								.sendMail(
										MsgErrorMail.ALERTA_FILE_PROCESSED_AVANCES
												.toString(),
										MsgErrorMail.ALERTA_FILE_PROCESSED_DAT_AVANCES
												.toString());
					}
				} else {
					LOG.warn("No existen archivos de avances para descargar desde SFTP");
					MailSendSrv.getInstance().sendMail(
							MsgErrorMail.ALERTA_FILE_PROCESS_AVANCES.toString(),
							MsgErrorMail.ALERTA_FILE_DOWNLOAD_AVANCES.toString());
				}
			} else {
				LOG.warn(MsgProceso.MSG_ERROR_FALTAN_PARAMETROS.toString()
						+ "-- " + listToDownload);
				MailSendSrv.getInstance().sendMail(
						MsgErrorMail.ALERTA_PRTS.toString(),
						MsgErrorMail.ALERTA_PRTS_TXT.toString());
			}

			// SE SUBIERON CORRECTAMETNE TODOS LOS REGISTROS
			if (listToDownload != null
					&& countDownloadProcess == listToDownload.size()) {
				flag = Boolean.TRUE;
			} else if (errorFile != null) {
				MailSendSrv.getInstance().sendMail(
						MsgErrorMail.ALERTA_FILE_LOAD_AVANCES.toString(),
						MsgErrorMail.ALERTA_FILE_LOAD_DAT_AVANCES.toString()
								.concat(errorFile));
			}

			return flag;

		} else {
			String asunto = MsgProceso.MSG_PROCESO_DESACTIVADO_ASUNTO
					.toString().replace(":?:",
							PROCESO_ACTUAL.getCodigoProceso());
			String mensaje = MsgProceso.MSG_PROCESO_DESACTIVADO_BODY.toString()
					.replace(":?:", PROCESO_ACTUAL.getCodigoProceso());
			MailSendSrv.getInstance().sendMail(asunto, mensaje);
		}
		return flag;

	}

	/**
	 * Metodo crea un registro en la tabla log TABLE_LOG_AVANCES registrando
	 * la descarga nombre del archivo.AVANCES
	 * 
	 * @param fileName
	 *            nombre del archivo.
	 * @param starWith
	 *            prefijo del archivo.
	 * @throws AppException
	 *             Exception App.
	 */
	public void save(final String fileName, final String starWith)
			throws AppException {
		// INICIALIZA EL DAO
		CommonsDao commonsDAO = null;
		try {
			commonsDAO = CommonsDaoFactory.getInstance().getNewEntidadDao();
			TblCronLogDTO cronLog = new TblCronLogDTO();
			cronLog.setFecha(DateUtils.getDateFileAvances(DateUtils
					.getDateStringFromFilenameAvances(fileName, starWith)));

			LOG.info("fecha obtenida : " + cronLog.getFecha() + " data util: "
					+ DateUtils.getDateStringFromFilenameAvances(fileName, starWith));

			cronLog.setFilename(fileName);
			cronLog.setFileFlag(StatusProcessType.PROCESS_PENDING.getValue());			
			
			if (commonsDAO
					.saveTblCronLog(cronLog, LogBD.TABLE_LOG_AVANCES)) {
				commonsDAO.endTx();
			} else {
				commonsDAO.rollBack();
			}
		} catch (SQLException e) {
			rollBackTransaccion(commonsDAO);
			throw new AppException(
					MsgErrorSQL.ERROR_LOAD_PROCESS_AVANCES.toString(), e);
		} finally {
			try {
				if (commonsDAO != null) {
					commonsDAO.close();
				}
			} catch (SQLException e) {
				rollBackTransaccion(commonsDAO);
				throw new AppException(
						MsgErrorSQL.ERROR_LOAD_PROCESS_AVANCES.toString(), e);
			}
		}

	}

	/**
	 * 
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 DD/10/2018, (ACL SPA) - versión inicial
	 * </ul>
	 * <p>
	 * 
	 * @return
	 * @throws AppException
	 * @since 1.X
	 */
	private ParamCronAvancesDTO getParametros() throws AppException {

		CommonsDao daoCommon = null;
		List<ParametroDTO> listaParametros = null;
		DataFTP dataFTP = null;
		ParamCronAvancesDTO parametros = null;
		try {
			LOG.info("CARGA PARAMETROS PROCESO");
			daoCommon = CommonsDaoFactory.getInstance().getNewEntidadDao();
			listaParametros = daoCommon
					.getParametroCodGrupoDato(ConstantesPRTS.CRON_TBK
							.toString());
			
			if (listaParametros != null) {

				// PARAMETROS DE FTP 
				dataFTP = new DataFTP();
				dataFTP.setIpHost(CommonsUtils.getCodiDato(listaParametros,
						ConstantesPRTS.SFTP_IP_DOWNLOAD_AVANCES.toString()));
				String port = CommonsUtils.getCodiDato(listaParametros,
						ConstantesPRTS.SFTP_PORT_DOWNLOAD_AVANCES.toString());
				dataFTP.setPort(Integer.parseInt((port != null
						&& port.matches(ConstantesUtil.PATTERN_IS_NUMBER
								.toString()) ? port
						: ConstantesUtil.DEFAULT_PORT.toString())));
				dataFTP.setUser(CommonsUtils.getCodiDato(listaParametros,
						ConstantesPRTS.SFTP_USER_DOWNLOAD_AVANCES.toString()));
				dataFTP.setPassword(CommonsUtils.getCodiDato(listaParametros,
						ConstantesPRTS.SFTP_PASS_DOWNLOAD_AVANCES.toString()));

				parametros = new ParamCronAvancesDTO();

				// RUTA FTP DONDE BUSCA ARCHIVOS A DESCARGAR
				parametros.setFtpPathAvances(CommonsUtils.getCodiDato(
						listaParametros,
						ConstantesPRTS.PATH_OUT_DWNLD_AVANCES.toString()));

				// RUTA SERVIDOR DONDE SE DEJARA EL ARCHIVO DE AVANCES
				// DESCARGADO
				parametros.setPathAclAvances(CommonsUtils.getCodiDato(
						listaParametros,
						ConstantesPRTS.PATH_ACL_AVANCES.toString()));

				// FORMATO DE ARCHIVO AVANCES
				parametros.setFormatFilenameAvances(CommonsUtils.getCodiDato(
						listaParametros,
						ConstantesPRTS.FORMAT_FILENAME_AVANCES.toString()));

				// FORMATO DE ARCHIVO AVANCES CORRELATIVO
				parametros.setFormatFilenameAvancesCorre(CommonsUtils.getCodiDato(
						listaParametros,
						ConstantesPRTS.FORMAT_FILENAME_CORRELATIVO_AVANCES
								.toString()));

				parametros.setDataFTP(dataFTP);

			} else {
				parametros = null;
			}
		} catch (SQLException e) {
			throw new AppException(MsgErrorSQL.ERROR_QUERY_PRTS.toString(), e);
		} finally {
			try {
				if (daoCommon != null) {
					daoCommon.close();
				}
			} catch (SQLException e) {
				throw new AppException(
						MsgErrorSQL.ERROR_QUERY_PRTS_CLOSE.toString(), e);
			}
		}

		return parametros;
	}

}
