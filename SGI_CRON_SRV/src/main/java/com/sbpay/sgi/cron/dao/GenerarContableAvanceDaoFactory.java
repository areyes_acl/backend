package com.sbpay.sgi.cron.dao;

import java.sql.SQLException;

import com.sbpay.sgi.cron.dao.BaseDaoFactory;
import com.sbpay.sgi.cron.ds.ConnectionProvider;


public class GenerarContableAvanceDaoFactory  extends BaseDaoFactory<GenerarContableAvanceDao>{
	/** The single instance. */
	private static GenerarContableAvanceDaoFactory singleINSTANCE = null;

	/**
	 * Creates the instance.
	 * 
	 * @throws SQLException
	 */
	private static void createInstance() throws SQLException {
		synchronized (GenerarContableAvanceDaoFactory.class) {
			if (singleINSTANCE == null) {
				singleINSTANCE = new GenerarContableAvanceDaoFactory();
			}
		}
	}

	/**
	 * Patron singleton.
	 * 
	 * @return GenerarContableAvanceDaoFactory retorna instancia del servicio.
	 * @throws SQLException
	 */
	public static GenerarContableAvanceDaoFactory getInstance() throws SQLException {
		if (singleINSTANCE == null) {
			createInstance();
		}
		return singleINSTANCE;
	}

	/**
	 * Metodo de restricion de sobreescritura de la clase.
	 */
	@Override
	protected Object clone() throws CloneNotSupportedException {
		super.clone();
		throw new CloneNotSupportedException();
	}

	/**
	 * Constructor default.
	 * 
	 * @throws SQLException Error de SQL.
	 */
	private GenerarContableAvanceDaoFactory() throws SQLException {
		super();
	}
	
	@Override
	public GenerarContableAvanceDaoJdbc getNewEntidadDao() throws SQLException {
		return new GenerarContableAvanceDaoJdbc(ConnectionProvider.getInstance()
				.getConnection());
	}
	
}
