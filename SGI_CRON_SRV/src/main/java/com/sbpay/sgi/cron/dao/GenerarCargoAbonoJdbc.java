package com.sbpay.sgi.cron.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.BaseDao;
import com.sbpay.sgi.cron.dto.TmpSalidaCargoAbonoDTO;
import com.sbpay.sgi.cron.enums.ConstantesBD;
import com.sbpay.sgi.cron.enums.MsgErrorCargoAbono;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 7/12/2015, (ACL) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SA.</B>
 */
public class GenerarCargoAbonoJdbc extends BaseDao<GenerarCargoAbonoDAO>
        implements GenerarCargoAbonoDAO {
    
    /** VARIABLE PARA EL LOGER */
    private static final Logger LOGGER = Logger
            .getLogger( GenerarCargoAbonoJdbc.class );
    
    private static final String SELECT_ALL_FROM_TMP_SALIDA_CARGO_ABONO = "SELECT SID,TIPO_REGISTRO,COD_EMISOR,TIPO_SOLICITUD,COD_TARJETA,DESCRIPCION_TBL_CARGOS_ABONOS,NRO_CUOTAS,MONTO_TRANSAC_CARGO_ABONO,FECHA_EFECTIVA,SUCURSAL_RECAUDADORA,TRANSACTION_CODE,HORA_TRANSACCION,MERCHANT_NUMBER,PAYMENT_MODE,PAYMENT_DETAILS,TRANSACTION_CURRENCY,TASA_INTERES,FILTER1,FILTER2,PAGO_DIFERIDO,TIPO_MONEDA FROM "+ConstantesBD.ESQUEMA.toString()+".TMP_SALIDA_CARGO_ABONO";
    private static final String CALL_SP_SIG_MODESTSALIDACARGOABONO = "{call SP_SIG_MODESTSALIDACARGOABONO(?,?,?)}";
    private static final String CALL_SP_SIG_CARGASALIDACARGOABONO = "{call SP_SIG_CARGASALIDACARGOABONO(?,?,?)}";
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 7/12/2015, (ACL & sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @param connection
     * @since 1.X
     */
    public GenerarCargoAbonoJdbc( Connection connection ) {
        super( connection );
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2015, (Everis Chile) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @throws AppException
     * @throws SQLException
     * @see com.sbpay.sgi.cron.dao.CargarIncomingDAO#loadIncoming()
     * @since 1.X
     */
    @Override
    public List<TmpSalidaCargoAbonoDTO> loadCargoAbono() throws AppException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<TmpSalidaCargoAbonoDTO> cargoAbonoList = new ArrayList<TmpSalidaCargoAbonoDTO>();
        TmpSalidaCargoAbonoDTO dto = null;
        try {
            
            LOGGER.info( "CONSULTA SQL -> "
                    + SELECT_ALL_FROM_TMP_SALIDA_CARGO_ABONO );
            stmt = this.getConnection().prepareStatement(
                    SELECT_ALL_FROM_TMP_SALIDA_CARGO_ABONO );
            
            // SE EJECUTA CONSULTA
            rs = stmt.executeQuery();
            
            // LEE LAS COLUMNAS DE CADA REGISTRO
            while ( rs.next() ) {
                dto = parseCargoAbono( rs );
                cargoAbonoList.add( dto );
            }
            return cargoAbonoList;
        }
        catch ( SQLException e ) {
            throw new AppException( e.getMessage(), e );
        }
        finally {
            
            try {
                if ( stmt != null ) {
                    stmt.close();
                }
                if ( rs != null ) {
                    rs.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException( e.getMessage(), e );
                
            }
            
        }
        
    }
    
    private TmpSalidaCargoAbonoDTO parseCargoAbono( final ResultSet rs )
            throws SQLException {
        TmpSalidaCargoAbonoDTO dto = null;
        dto = new TmpSalidaCargoAbonoDTO();
        dto.setSid( rs.getLong( "SID" ) );
        dto.setCodEmisor( rs.getString( "TIPO_REGISTRO" ) );
        dto.setTipoRegistro( rs.getString( "TIPO_REGISTRO" ) );
        dto.setCodEmisor( rs.getString( "COD_EMISOR" ) );
        dto.setTipoSolicitud( rs.getString( "TIPO_SOLICITUD" ) );
        dto.setCodTarjeta( rs.getString( "COD_TARJETA" ) );
        dto.setDescripcionTblCargosAbonos( rs
                .getString( "DESCRIPCION_TBL_CARGOS_ABONOS" ) );
        dto.setNroCuotas( rs.getString( "NRO_CUOTAS" ) );
        dto.setMontoTransacCargoAbono( rs
                .getString( "MONTO_TRANSAC_CARGO_ABONO" ) );
        dto.setFechaEfectiva( rs.getString( "FECHA_EFECTIVA" ) );
        dto.setSucursalRecaudadora( rs.getString( "SUCURSAL_RECAUDADORA" ) );
        dto.setTransactionCode( rs.getString( "TRANSACTION_CODE" ) );
        dto.setHoraTransaccion( rs.getString( "HORA_TRANSACCION" ) );
        dto.setMerchantNumber( rs.getString( "MERCHANT_NUMBER" ) );
        dto.setPaymentMode( rs.getString( "PAYMENT_MODE" ) );
        dto.setPaymentDetails( rs.getString( "PAYMENT_DETAILS" ) );
        dto.setTransactionCurrency( rs.getString( "TRANSACTION_CURRENCY" ) );
        dto.setTasaInteres( rs.getString( "TASA_INTERES" ) );
        dto.setFilter1( rs.getString( "FILTER1" ) );
        dto.setFilter2( rs.getString( "FILTER2" ) );
        dto.setPagoDiferido( rs.getString( "PAGO_DIFERIDO" ) );
        dto.setTipoMoneda( rs.getString( "TIPO_MONEDA" ));
        return dto;
    }
    
    @Override
    public boolean callSPCargaSalidaCargoAbono() throws AppException {
        CallableStatement callableStatement = null;
        
        LOGGER.info( "******SE EJECUTA STORE PROCEDURE SP_SIG_CARGASALIDACARGOABONO *****" );
        
        try {
            
            callableStatement = this.getConnection().prepareCall(
                    CALL_SP_SIG_CARGASALIDACARGOABONO );
            callableStatement.registerOutParameter( 1, java.sql.Types.VARCHAR );
            callableStatement.registerOutParameter( 2, java.sql.Types.NUMERIC );
            callableStatement.registerOutParameter( 3, java.sql.Types.NUMERIC );
            
            callableStatement.execute();
            
            String warning = callableStatement.getString( 1 );
            Integer codigoError = callableStatement.getInt( 2 );
            Integer l_ind_Ok_Ko = callableStatement.getInt( 3 );
            LOGGER.info( "WARNING : " + warning );
            LOGGER.info( "codigoError : " + codigoError );
            LOGGER.info( "l_ind_Ok_Ko : " + l_ind_Ok_Ko );
            if ( l_ind_Ok_Ko != 0 && l_ind_Ok_Ko != 1 ) {
                LOGGER.warn( "Advertencia: la llamada al proceso SP_SIG_CARGASALIDACARGOABONO no a finalizado correctamente: error "
                        + codigoError + ", mensaje:" + warning );
                return Boolean.FALSE;
            }
            if ( l_ind_Ok_Ko == 1 ) {
                LOGGER.info( "la llamada al proceso SP_SIG_CARGASALIDACARGOABONO no encontro Abonos o Cargos "
                        + codigoError + ", mensaje:" + warning );
                return Boolean.FALSE;
            }
            return Boolean.TRUE;
        }
        catch ( SQLException e ) {
            throw new AppException(
                    MsgErrorCargoAbono.ERROR_CALL_SP_CARGASALIDACARGOABONO
                            .toString(),
                    e );
        }
        finally {
            try {
                if ( callableStatement != null ) {
                    callableStatement.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException(
                        MsgErrorCargoAbono.ERROR_CALL_SP_CAR_ABO_TBK_CLOSE_CONECTION
                                .toString(), e );
            }
        }
    }
    
    @Override
    public boolean callSPModEstSalidaCargoAbono() throws AppException {
        CallableStatement callableStatement = null;
        
        LOGGER.info( "******SE EJECUTA STORE PROCEDURE SP_SIG_MODESTSALIDACARGOABONO *****" );
        
        try {
            
            callableStatement = this.getConnection().prepareCall(
                    CALL_SP_SIG_MODESTSALIDACARGOABONO );
            callableStatement.registerOutParameter( 1, java.sql.Types.VARCHAR );
            callableStatement.registerOutParameter( 2, java.sql.Types.NUMERIC );
            callableStatement.registerOutParameter( 3, java.sql.Types.NUMERIC );
            
            callableStatement.execute();
            
            String warning = callableStatement.getString( 1 );
            Integer codigoError = callableStatement.getInt( 2 );
            Integer l_ind_Ok_Ko = callableStatement.getInt( 3 );
            LOGGER.info( "WARNING : " + warning );
            LOGGER.info( "codigoError : " + codigoError );
            LOGGER.info( "l_ind_Ok_Ko : " + l_ind_Ok_Ko );
            if ( l_ind_Ok_Ko != 0 ) {
                LOGGER.warn( "Advertencia: la llamada al proceso SP_SIG_MODESTSALIDACARGOABONO no a finalizado correctamente: error "
                        + codigoError + ", mensaje:" + warning );
                return Boolean.FALSE;
            }
            return Boolean.TRUE;
        }
        catch ( SQLException e ) {
            throw new AppException(
                    MsgErrorCargoAbono.ERROR_CALL_SP_CARGASALIDACARGOABONO
                            .toString(),
                    e );
        }
        finally {
            try {
                if ( callableStatement != null ) {
                    callableStatement.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException(
                        MsgErrorCargoAbono.ERROR_CALL_SP_CAR_ABO_TBK_CLOSE_CONECTION
                                .toString(), e );
            }
        }
    }
    
    @Override
    public boolean setTblCronIc( final String cargoAbonoFilename )
            throws SQLException {
        PreparedStatement stmt = null;
        try {
            final StringBuilder str = new StringBuilder()
                    .append( "INSERT INTO " )
                    .append( ConstantesBD.ESQUEMA.toString() )
                    .append( ".TBL_CRON_IC (SID,FECHA," )
                    .append( " FILE_NAME,FILE_FLAG,FILE_TS)" )
                    .append( " values (SEQ_TBL_CRON_IC.nextval," )
                    .append( " TRUNC(SYSDATE),?,?,SYSDATE)" );
            stmt = this.getConnection().prepareStatement( str.toString() );
            setStringNull( cargoAbonoFilename, 1, stmt );
            setStringNull( "0", 2, stmt );
            
            //LOGGER.info( "SQL --> " + str.toString() );
            return ( stmt.executeUpdate() > 0 );
            
        }
        finally {
            if ( stmt != null ) {
                stmt.close();
            }
        }
    }
}
