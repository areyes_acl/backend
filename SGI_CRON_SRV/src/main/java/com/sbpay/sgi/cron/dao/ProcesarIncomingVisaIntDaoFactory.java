package com.sbpay.sgi.cron.dao;

import java.sql.SQLException;

import com.sbpay.sgi.cron.dao.BaseDaoFactory;
import com.sbpay.sgi.cron.ds.ConnectionProvider;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 25/04/2019, (ACL) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SA.</B>
 */
public class ProcesarIncomingVisaIntDaoFactory extends
        BaseDaoFactory<ProcesarIncomingVisaIntDao> {
    
    /** The single instance. */
    private static ProcesarIncomingVisaIntDaoFactory singleINSTANCE = null;
    
    /**
     * Creates the instance.
     * 
     * @throws SQLException
     */
    private static void createInstance() throws SQLException {
        synchronized ( ProcesarIncomingVisaIntDaoFactory.class ) {
            if ( singleINSTANCE == null ) {
                singleINSTANCE = new ProcesarIncomingVisaIntDaoFactory();
            }
        }
    }
    
    /**
     * Patron singleton.
     * 
     * @return ProcesarIncomingVisaIntDaoFactory retorna instancia del
     *         servicio.
     * @throws SQLException
     */
    public static ProcesarIncomingVisaIntDaoFactory getInstance() throws SQLException {
        if ( singleINSTANCE == null ) {
            createInstance();
        }
        return singleINSTANCE;
    }
    
    /**
     * Metodo de restricion de sobreescritura de la clase.
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        super.clone();
        throw new CloneNotSupportedException();
    }
    
    /**
     * Constructor default.
     * 
     * @throws SQLException
     *             Error de SQL.
     */
    private ProcesarIncomingVisaIntDaoFactory() throws SQLException {
        super();
    }
    
    
    /**
     * Metodo obtiene la instancia mas la conexion.
     */
    @Override
    public ProcesarIncomingVisaIntDao getNewEntidadDao() throws SQLException {
        return new ProcesarIncomigVisaIntJdbc( ConnectionProvider.getInstance(
                 ).getConnection() );
    }
    
}
