package com.sbpay.sgi.cron.srv.export;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.CommonsDao;
import com.sbpay.sgi.cron.dao.CommonsDaoFactory;
import com.sbpay.sgi.cron.dao.GenerarReporteContableDao;
import com.sbpay.sgi.cron.dao.GenerarReporteContableDaoFactory;
import com.sbpay.sgi.cron.dto.DetalleContableBean;
import com.sbpay.sgi.cron.dto.ParamExportDetContDTO;
import com.sbpay.sgi.cron.dto.ParametroDTO;
import com.sbpay.sgi.cron.dto.SolicitudReporteContableDTO;
import com.sbpay.sgi.cron.enums.ConstantesPRTS;
import com.sbpay.sgi.cron.enums.MsgErrorMail;
import com.sbpay.sgi.cron.enums.MsgGenerarReporteDetalleContable;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.enums.MsgRegenerationCntblAvances;
import com.sbpay.sgi.cron.enums.ProcessCodeType;
import com.sbpay.sgi.cron.srv.utils.CommonSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;
import com.sbpay.sgi.cron.utils.file.ContableExport;
import com.sbpay.sgi.cron.utils.generals.CommonsUtils;
import com.sbpay.sgi.cron.validation.ProcessValidator;
import com.sbpay.sgi.cron.validation.impl.ProcessValidatorImpl;

public class GenerarReporteDetalleContableSrv extends CommonSrv {
	/** The Constant LOGGER. */
	private static final Logger LOGGER = Logger
			.getLogger(GenerarReporteDetalleContableSrv.class);
	/** The single instance. */
	private static GenerarReporteDetalleContableSrv singleINSTANCE = null;
	/** variable parametros del Cron */

	private final ProcessCodeType PROCESO_ACTUAL = ProcessCodeType.GENERAR_REPORTE_ARCHIVO_CONTABLE;

	/**
	 * Creates the instance.
	 */
	private static void createInstance() {
		synchronized (GenerarReporteDetalleContableSrv.class) {
			if (singleINSTANCE == null) {
				singleINSTANCE = new GenerarReporteDetalleContableSrv();

			}
		}
	}

	/**
	 * Patron singleton.
	 * 
	 * @return RegenerarContableAvanceSrv retorna instancia del servicio.
	 */
	public static GenerarReporteDetalleContableSrv getInstance() {
		if (singleINSTANCE == null) {
			createInstance();
		}
		return singleINSTANCE;
	}

	/**
	 * Metodo de restricion de sobreescritura de la clase.
	 */
	@Override
	protected Object clone() throws CloneNotSupportedException {
		super.clone();
		throw new CloneNotSupportedException();
	}

	/**
	 * Constructor Privado.
	 */
	private GenerarReporteDetalleContableSrv() {
		super();
	}

	/**
	 * 
	 * @return
	 * @throws AppException
	 */
	public boolean generarReporteDetalleContable() throws AppException {
		Boolean flag = Boolean.FALSE;
		GenerarReporteContableDao regenerarContableAvanceDao = null;
		ParamExportDetContDTO ParamExportReCnblDTO = null;

		try {
			ProcessValidator validadorProceso = new ProcessValidatorImpl();

			if (validadorProceso.isActiveProcess(PROCESO_ACTUAL)) {

				ParamExportReCnblDTO = getParametros();
				if (!validadorProceso.hasTheValidParametersForTheProcess(PROCESO_ACTUAL, ParamExportReCnblDTO)) {
					LOGGER.warn(MsgRegenerationCntblAvances.ERROR_PARAM_NOT_FOUND.toString());
					MailSendSrv.getInstance().sendMail(MsgErrorMail.ALERTA_PRTS.toString(), MsgErrorMail.ALERTA_PRTS_TXT.toString());
				}

				regenerarContableAvanceDao = GenerarReporteContableDaoFactory.getInstance().getNewEntidadDao();

				// METODO QUE BUSQUE ARCHIVOS A REGENERAR
				List<SolicitudReporteContableDTO> lstArchivosReprocesar = regenerarContableAvanceDao.obtenerInformacionCnbltSolicitud();
				int cantidad = lstArchivosReprocesar.size();
				if( cantidad > 0){
					LOGGER.info("Cantidad de registros "+ lstArchivosReprocesar.size());
					LOGGER.info("Bloqueando las trx encontradas "+ lstArchivosReprocesar.size());
					// Actualizando las trx tomadas
					regenerarContableAvanceDao.updateDataSolicitudContableBloqueo();
					regenerarContableAvanceDao.endTx();
					for (SolicitudReporteContableDTO lista : lstArchivosReprocesar) {
						List<DetalleContableBean> listaDetalle = regenerarContableAvanceDao.obtenerDetalleContable(lista.getFechaIni(), lista.getFechaFin(), lista.getTipoMov(), lista.getIdOperador(), lista.getIdProducto());
						int cantidadDetalle = listaDetalle.size();
						LOGGER.info("busqueda = "+ cantidadDetalle);
						LOGGER.info("Generando fichero");
						String ruta = ParamExportReCnblDTO.getPathSalida();
						ContableExport salida = new ContableExport();
						salida.exportContableFileReporte(ruta, lista.getFichero(), listaDetalle, ParamExportReCnblDTO);
						LOGGER.info("Cambiando el estado de la solicitud a OK");
						// Actualizando las trx tomadas
						regenerarContableAvanceDao.updateDataSolicitudContableStateOK(lista.getSid(), cantidadDetalle);
						regenerarContableAvanceDao.endTx();
						flag = true;
					}
					
				}else{
					LOGGER.info("No se encontraron solicitudes a procesar");
				}

			} else {
				String asunto = MsgProceso.MSG_PROCESO_DESACTIVADO_ASUNTO
						.toString().replace(":?:",
								PROCESO_ACTUAL.getCodigoProceso());
				String mensaje = MsgProceso.MSG_PROCESO_DESACTIVADO_BODY
						.toString().replace(":?:",
								PROCESO_ACTUAL.getCodigoProceso());
				MailSendSrv.getInstance().sendMail(asunto, mensaje);
			}

		} catch (SQLException e) {
			String asunto = MsgGenerarReporteDetalleContable.ERROR_GEN_CNTBL_FILE
					.toString();
			String mensaje = e.toString();

			MailSendSrv.getInstance().sendMail(asunto, mensaje);

			throw new AppException(
					MsgGenerarReporteDetalleContable.ERROR_CREATE_CONTABLE_FILE
							.toString(),
					e);

		} finally {
			if (regenerarContableAvanceDao != null) {
				try {
					regenerarContableAvanceDao.close();
				} catch (SQLException e) {
					String asunto = MsgGenerarReporteDetalleContable.ERROR_GEN_CNTBL_FILE
							.toString();
					String mensaje = e.toString();

					MailSendSrv.getInstance().sendMail(asunto, mensaje);
					throw new AppException(e.getMessage(), e);
				}
			}
		}
		return flag;

	}


	/**
	 * Metodo busca los parametros del proceso que genera el archivo Contable.
	 * 
	 * @return Dto con los parametros utilizados.
	 * @throws SQLException
	 *             Exception SQL.
	 */
	private ParamExportDetContDTO getParametros() throws SQLException {
		CommonsDao daoCommon = null;
		List<ParametroDTO> paramDTOList = null;
		ParamExportDetContDTO paramPldCnblDTO = null;
		try {
			daoCommon = CommonsDaoFactory.getInstance().getNewEntidadDao();
			paramDTOList = daoCommon.getParametroCodGrupoDato(ConstantesPRTS.CRON_TBK.toString());
			if (paramDTOList != null) {
				paramPldCnblDTO = new ParamExportDetContDTO();
				paramPldCnblDTO.setPathSalida(CommonsUtils.getCodiDato(paramDTOList, ConstantesPRTS.PATH_TRX_GEN_REPORTE_CONTABLE.toString()));
				paramPldCnblDTO.setCodDESCUENTO_TBK(CommonsUtils.getCodiDato(paramDTOList, ConstantesPRTS.COD_DESCUENTO.toString()));
				paramPldCnblDTO.setCodPAGO_TBK(CommonsUtils.getCodiDato(paramDTOList, ConstantesPRTS.COD_PAGO.toString()));
				paramPldCnblDTO.setCodDESCUENTO_VN(CommonsUtils.getCodiDato(paramDTOList, ConstantesPRTS.COD_DESCUENTO_VN.toString()));
				paramPldCnblDTO.setCodPAGO_VN(CommonsUtils.getCodiDato(paramDTOList, ConstantesPRTS.COD_PAGO_VN.toString()));
				paramPldCnblDTO.setCodDESCUENTO_VI(CommonsUtils.getCodiDato(paramDTOList, ConstantesPRTS.COD_DESCUENTO_VI.toString()));
				paramPldCnblDTO.setCodPAGO_VI(CommonsUtils.getCodiDato(paramDTOList, ConstantesPRTS.COD_PAGO_VI.toString()));
			} else {
				paramPldCnblDTO = null;
			}
			return paramPldCnblDTO;
		} finally {

			if (daoCommon != null) {
				daoCommon.close();
			}

		}
	}
}
