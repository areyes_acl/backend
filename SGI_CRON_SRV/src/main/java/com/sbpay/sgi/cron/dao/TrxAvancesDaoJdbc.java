package com.sbpay.sgi.cron.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import oracle.jdbc.OracleCallableStatement;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.dao.BaseDao;
import com.sbpay.sgi.cron.dto.TransaccionAvancesDTO;
import com.sbpay.sgi.cron.enums.ConstantesBD;

public class TrxAvancesDaoJdbc extends BaseDao<TrxAvancesDao> implements TrxAvancesDao {
    
    /** VARIABLE PARA EL LOGER */
    private static final Logger LOGGER = Logger.getLogger( TrxAvancesDaoJdbc.class );
    
    /**
     * Constructor con conexion.
     * 
     * @param connection
     *            Conexion JDBC.
     */
    public TrxAvancesDaoJdbc( Connection connection ) {
        super( connection );
    }

	@Override
	public boolean saveTransaction(final TransaccionAvancesDTO transaccion)
			throws SQLException {
	    PreparedStatement stmt = null;
	    
	    try {
	      StringBuilder str =
	              new StringBuilder()
	                  .append("INSERT INTO ")
	                  .append(ConstantesBD.ESQUEMA.toString())
	                  .append(".TMP_TRANSAC_AVANCES ")
	                  .append("(SID, FECHA_TRANSFERENCIA, COD_AUTORIZACION, NUM_TRX, NUM_TARJETA, MONTO, ESTADO, NUM_CANAL, SEGURO_MONTO)")
	                  .append(" values (SEQ_TMP_TRANSAC_AVANCES.nextval, To_date(?, 'DD-MM-RR'), ?, ?, ?, ?, ?, ?, ?)");

	          stmt = this.getConnection().prepareStatement(str.toString());
	          	          
	          setStringNull(transaccion.getFechaTransferencia(), 1, stmt);
	          setStringNull(transaccion.getCodAutorizacion(), 2, stmt);
	          setStringNull(transaccion.getNumTrx(), 3, stmt);
	          setStringNull(transaccion.getNumTarjeta(), 4, stmt);
	          setStringNull(transaccion.getMonto(), 5, stmt);
	          setStringNull(transaccion.getEstado(), 6, stmt);
	          setStringNull(transaccion.getNumCanal(), 7, stmt);
	          setStringNull(transaccion.getMontoSeguro(), 8, stmt);
	          
	          return (stmt.executeUpdate() > 0);
	    } finally {
	      if (stmt != null) {
	        stmt.close();
	      }
	    }
	}

	@Override
	public boolean callSPCargaLogTrxAvances() throws SQLException {
	    Connection dbConnection = null;
	    OracleCallableStatement callableStatement = null;

	    try {
	      String callSPCargaIncomingSql = "{call SP_SIG_CARGA_LOG_TRX_AVANCES(?,?)}";

	      dbConnection = getConnection();
	      callableStatement =
	          (OracleCallableStatement) dbConnection.prepareCall(callSPCargaIncomingSql);

	      callableStatement.registerOutParameter(1, java.sql.Types.VARCHAR);
	      callableStatement.registerOutParameter(2, java.sql.Types.NUMERIC);

	      callableStatement.executeUpdate();

	      String warning = callableStatement.getString(1);
	      Integer codigoError = callableStatement.getInt(2);
	      LOGGER.info("****** SE EJECUTA STORE PROCEDURE SP_SIG_CARGA_LOG_TRX_AVANCES *****");
	      LOGGER.info("WARNING : " + warning);
	      LOGGER.info("codigoError : " + codigoError);
	      LOGGER.info("*********** STORE PROCEDURE SP_SIG_CARGA_LOG_TRX_AVANCES *********** ");

	    } finally {
	      if (callableStatement != null) {
	        callableStatement.close();
	      }
	    }
	    return Boolean.TRUE;
		
	}
    
 

}
