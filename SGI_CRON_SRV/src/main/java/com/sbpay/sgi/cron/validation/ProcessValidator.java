package com.sbpay.sgi.cron.validation;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.enums.ProcessCodeType;


/**
 * 
 */
public interface ProcessValidator {
    
    public Boolean isActiveProcess(ProcessCodeType proceso) throws AppException;
    
    public Boolean hasTheValidParametersForTheProcess(ProcessCodeType proceso, Object parameters);

}
