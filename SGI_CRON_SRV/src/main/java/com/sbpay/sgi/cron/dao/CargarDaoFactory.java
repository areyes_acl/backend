package com.sbpay.sgi.cron.dao;

import java.sql.SQLException;

import com.sbpay.sgi.cron.dao.BaseDaoFactory;
import com.sbpay.sgi.cron.ds.ConnectionProvider;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 19/11/2015, (ACL) - versi�n inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SA.</B>
 */
public final class CargarDaoFactory extends BaseDaoFactory<CargarDao> {

	/** The single instance. */
	private static CargarDaoFactory singleINSTANCE = null;

	/**
	 * Creates the instance.
	 * 
	 * @throws SQLException
	 */
	private static void createInstance() throws SQLException {
		synchronized (CargarDaoFactory.class) {
			if (singleINSTANCE == null) {
				singleINSTANCE = new CargarDaoFactory();
			}
		}
	}

	/**
	 * Patron singleton.
	 * 
	 * @return ActivarPrepagoDaoFactory retorna instancia del servicio.
	 * @throws SQLException
	 */
	public static CargarDaoFactory getInstance() throws SQLException {
		if (singleINSTANCE == null) {
			createInstance();
		}
		return singleINSTANCE;
	}

	/**
	 * Metodo de restricion de sobreescritura de la clase.
	 */
	@Override
	protected Object clone() throws CloneNotSupportedException {
		super.clone();
		throw new CloneNotSupportedException();
	}

	/**
	 * Constructor default.
	 * 
	 * @throws SQLException Error de SQL.
	 */
	private CargarDaoFactory() throws SQLException {
		super();
	}

	/**
	 * Metodo obtiene la instancia mas la conexion.
	 */
	@Override
	public CargarDao getNewEntidadDao() throws SQLException {
		return new CargarDaoJdbc(ConnectionProvider.getInstance()
				.getConnection());
	}
}