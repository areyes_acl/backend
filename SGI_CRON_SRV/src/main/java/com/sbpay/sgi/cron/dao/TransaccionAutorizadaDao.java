package com.sbpay.sgi.cron.dao;

import java.sql.SQLException;

import com.sbpay.sgi.cron.dao.Dao;
import com.sbpay.sgi.cron.dto.TransacionAutorizada;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 19/11/2015, (ACL) -  versión inicial 
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SA.</B>
 */ 
public interface TransaccionAutorizadaDao extends Dao {
    
    public boolean saveTransaction(TransacionAutorizada transaccion)throws SQLException;
    
    public boolean callSPCargaLogTrxAutor() throws SQLException;
    
}
