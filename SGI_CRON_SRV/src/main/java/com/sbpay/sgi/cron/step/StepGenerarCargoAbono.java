package com.sbpay.sgi.cron.step;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.enums.MsgExitoMail;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.srv.export.GenerarCargoAbonoSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;

public class StepGenerarCargoAbono {
    
    /** The Constant LOGGER. */
    private static final Logger LOG = Logger
            .getLogger( StepGenerarCargoAbono.class );
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 7/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @throws Exception
     * @since 1.X
     */
    public void execute() throws Exception {
        
        try {
            LOG.info( "========== Inicio Proceso de Generar Cargos y Abonos : StepGenerarCargoAbono ========= " );
            if ( GenerarCargoAbonoSrv.getInstance().generarCargoAbonoTbk() ) {
                LOG.info( "Envio Correo de Cargos y Abonos  Satisfactorio: Proceso Cargo y Abono Tbk" );
                MailSendSrv.getInstance().sendMailOk(
                        MsgExitoMail.EXITO_PROCESS_CAR_ABO_TITLE.toString(),
                        MsgExitoMail.EXITO_PROCESS_CAR_ABO.toString() );
            }
            else {
                LOG.info( "Cargo y Abonos no realizados, Finaliza! (total o parcialmente consultar log)" );
                
            }
            
        }
        catch ( AppException e ) {
            LOG.error( e.getMessage(), e );
            MailSendSrv.getInstance().sendMail(
                    MsgProceso.MSG_GENERIC_ERROR_MSJ.toString(),
                    ( e.getStackTraceDescripcion() != null ) ? e
                            .getStackTraceDescripcion() : e.getMessage() );
        }
        
    }
    
}
