package com.sbpay.sgi.cron.step;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.srv.download.DownloadBOLSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 12/02/2016, (ACL) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SA.</B>
 */
public class StepDownloadBOL {

    /**
     * VARIABLES
     */
    private static final Logger LOG = Logger.getLogger(StepDownloadBOL.class);

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @throws Exception
     * @since 1.X
     */
    public void execute() {
	LOG.info("==== Inicio proceso : StepDownloadBOL === ");

	try {
	    DownloadBOLSrv.getInstance().descargarBOL();
	} catch (AppException e) {
	   LOG.error(e.getMessage(),e);
	    MailSendSrv.getInstance().sendMail(
		    MsgProceso.MSG_GENERIC_ERROR_MSJ.toString(),
		    (e.getStackTraceDescripcion() != null) ? e
			    .getStackTraceDescripcion() : e.getMessage());
	}

    }

}
