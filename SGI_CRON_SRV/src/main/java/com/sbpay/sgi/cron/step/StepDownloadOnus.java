package com.sbpay.sgi.cron.step;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.enums.MsgExitoMail;
import com.sbpay.sgi.cron.enums.MsgProceso;
import com.sbpay.sgi.cron.srv.download.DownLoadOnusSrv;
import com.sbpay.sgi.cron.srv.utils.MailSendSrv;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 7/1/2016, (ACL-sbpay) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL & sbpay.</B>
 */
public class StepDownloadOnus {
    /** The Constant LOGGER. */
    private static final Logger LOG = Logger
            .getLogger( StepDownloadOnus.class );
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 12/1/2016, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws Exception
     * @since 1.X
     */
    public void execute() throws Exception {
        
        try {
            LOG.info( "======>Inicio Proceso:  Download Onus file<========" );
            if ( DownLoadOnusSrv.getInstance().descargarOnus() ) {
                LOG.info( "Envio Correo de Descarga Satisfactoria Proceso : DownLoad Onus file" );
                MailSendSrv
                        .getInstance()
                        .sendMailOk(
                                MsgExitoMail.EXITO_PROCESS_DOWNLOAD_ONUS_IC_TITLE
                                        .toString(),
                                MsgExitoMail.EXITO_PROCESS_DOWNLOAD_ONUS_IC_BODY
                                        .toString() );
            }
            else {
                LOG.info( "=====>Finaliza Proceso :DownLoad Onus, pero se ejecuta con observaciones... favor revisar Log" );
                
            }
            
        }
        catch ( AppException e ) {
            LOG.error( e.getMessage(), e );
            MailSendSrv.getInstance().sendMail(
                    MsgProceso.MSG_GENERIC_ERROR_MSJ.toString(),
                    ( e.getStackTraceDescripcion() != null ) ? e
                            .getStackTraceDescripcion() : e.getMessage() );
        }
        
    }
    
}
