package com.sbpay.sgi.cron.dao;

import java.sql.SQLException;
import java.util.List;

import com.sbpay.sgi.cron.dao.Dao;
import com.sbpay.sgi.cron.dto.TmpExportContableDTO;

public interface GenerarContableAvanceDao extends Dao{

	List<TmpExportContableDTO> obtenerInformacionCnblt() throws SQLException;
	
	boolean cargarInformacionContableAvance() throws SQLException;

	void clearTmpContable()throws SQLException;
}
