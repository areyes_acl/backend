package com.sbpay.sgi.cron.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.sbpay.sgi.cron.commons.AppException;
import com.sbpay.sgi.cron.dao.BaseDao;
import com.sbpay.sgi.cron.dto.Registro00DTO;
import com.sbpay.sgi.cron.dto.Registro01DTO;
import com.sbpay.sgi.cron.dto.Registro05DTO;
import com.sbpay.sgi.cron.dto.Registro1020DTO;
import com.sbpay.sgi.cron.dto.Registro07DTO;
import com.sbpay.sgi.cron.dto.RegistroDTO;
import com.sbpay.sgi.cron.dto.Transaccion;
import com.sbpay.sgi.cron.enums.ConstantesBD;
import com.sbpay.sgi.cron.enums.ConstantesUtil;
import com.sbpay.sgi.cron.enums.MsgGenerationIncoming;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 7/12/2015, (ACL) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SA.</B>
 */
public class GenerarIncomingJdbc extends BaseDao<GenerarIncomingDAO> implements
        GenerarIncomingDAO {
    
    /** VARIABLE PARA EL LOGER */
    private static final Logger LOGGER = Logger
            .getLogger( GenerarIncomingJdbc.class );
    
    private static final String SELECT_ALL_FROM_TMP_OUTGOING_TRANSBANK = "SELECT SID, MIT, NRO_TARJETA, CODIGO_PROCESAMIENTO, MONTO_TRANSAC,MONTO_CONCILIACION, MONTO_FACTURACION,TASA_CONVENIO_CONCILIACION, TASA_CONVENIO_FACTURACION, FECHA_HR_TRASAC, FECHA_VENCIMIENTO_TARJETA, COD_DATOS_PUNTO_SERVICIO, NRO_SECUENCIA_TARJETA, CODIGO_FUNCION, COD_MOTIVO_MENSAJE, MCC, MONTOS_ORIGINALES, DATOS_REFERENCIA_ADQUIRENTE, COD_IDENTIFICACION_ADQUIRENTE, COD_IDENT_INST_ENVIA, NRO_REFERENCIA_RECUPERACION, CODIGO_AUTORIZACION, CODIGO_SERVICIO, IDENT_TERM_ACEP_TARJETA, COD_IDENT_ACEP_TARJETA, NOMBRE_UBIC_ACEP_TARJETA, DATOS_ADICIONALES_1, COD_MONEDA_TRANSAC, COD_MONEDA_CONCILIACION, COD_MONEDA_FACTURACION, MONTOS_ADICIONALES, ICC, DATOS_ADICIONALES_2, IDENT_CICLO_DURACION_TRANSAC, NRO_MENSAJE, REGISTRO_DE_DATOS, FECHA_ACCION, COD_IDENT_INST_DEST_TRANSAC, COD_IDENT_INST_ORIG_TRANSAC, DATOS_REF_EMISOR, COD_IDENT_INST_RECIBE, MONTO_RECARGO_X_CONVER_MONEDA, DATOS_ADICIONALES_3, DATOS_ADICIONALES_4, DATOS_ADICIONALES_5, P0023, P0052, P0105, P0122, P0148, P0149, P0158, P0165, P0191, P0228, P0262, P0301,P0306, P1000, P1001, P1003, P1004, P1005, P1006, P1007, P1008, P1009, P1010, P1011, P1012 FROM "+ConstantesBD.ESQUEMA.toString()+".TMP_OUTGOING_TRANSBANK";
    private static final String TRANS_CODE_REGISTER_01 = "01";
    private static final String CALL_SP_SIG_CARGAOUTGOING_TBK_VISA = "{call SP_SIG_CARGAOUTGOING_TBK_VISA(?,?,?)}";
    private static final String CALL_SP_SIG_CARGAOUTGOING_VISA = "{call SP_SIG_CARGAOUTGOING_VISA(?,?,?,?)}";
    private static final String TRUNCATE_TMP_OUTGOING_TRANSBANK = "TRUNCATE TABLE TMP_OUTGOING_TRANSBANK";
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 7/12/2015, (ACL & sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @param connection
     * @since 1.X
     */
    public GenerarIncomingJdbc( Connection connection ) {
        super( connection );
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2015, (Everis Chile) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @throws AppException
     * @throws SQLException
     * @see com.sbpay.sgi.cron.dao.CargarIncomingDAO#loadIncoming()
     * @since 1.X
     */
    @Override
    public List<Transaccion> loadTransactions() throws AppException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<Transaccion> transactionList = new ArrayList<Transaccion>();
        try {
            
        	if(LOGGER.isDebugEnabled()){
        		LOGGER.debug( "CONSULTA SQL -> "+ SELECT_ALL_FROM_TMP_OUTGOING_TRANSBANK );
        	}
            stmt = this.getConnection().prepareStatement(
                    SELECT_ALL_FROM_TMP_OUTGOING_TRANSBANK );
            
            // SE EJECUTA CONSULTA
            rs = stmt.executeQuery();
            
            // LEE LAS COLUMNAS DE CADA REGISTRO
            while ( rs.next() ) {
                Transaccion tr = parseTransactions( rs );
                tr.setNumeroTransaccion( Integer.valueOf( rs
                        .getString( "NRO_MENSAJE" ) ) );
                tr.setRutsbpay( rs.getString( "P1011" ) );
                transactionList.add( tr );
            }
        }
        catch ( SQLException e ) {
            throw new AppException( e.getMessage(), e );
        }
        finally {
            
            try {
                if ( stmt != null ) {
                    stmt.close();
                }
                if ( rs != null ) {
                    rs.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException( e.getMessage(), e );
                
            }
            
        }
        
        return transactionList;
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 - 9/12/2015, (ACL) - versión inicial
     * </ul>
     * <p>
     * 
     * @param rs
     * @return
     * @throws SQLException
     * @since 1.X
     */
    private Transaccion parseTransactions( ResultSet rs ) throws SQLException {
        Transaccion tr = new Transaccion();
        String mit = rs.getString("MIT");
        if(mit.equals("10") || mit.equals("20")){
        	tr.setRegistro1020( getRegistro1020( rs ) );
        	tr.setRegistro00( getRegistro00Reduce( rs ) );
        }
        else{
            tr.setRegistro00( getRegistro00( rs ) );
            tr.setRegistro01( getRegistro01( rs ) );
            tr.setRegistro05( getRegistro05( rs ) );
            tr.setRegistro07( getRegistro07( rs ) );
        }
        
        return tr;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 - 11/12/2015, (ACL) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @throws SQLException
     * @since 1.X
     */
    private Registro00DTO getRegistro00( ResultSet rs ) throws SQLException {
        Registro00DTO registro00 = new Registro00DTO();
        
        // VALORES PARA REGISTRO 00
        registro00.setCodigoTransaccion( rs.getString( "MIT" ) );
        registro00.setNumeroTarjeta( rs.getString( "NRO_TARJETA" ) );
        registro00.setUsagCode( rs.getString( "CODIGO_PROCESAMIENTO" ) );
        registro00.setMontoFuen( rs.getString( "MONTO_TRANSAC" ) );
        registro00.setFechaComp( rs.getString( "FECHA_HR_TRASAC" ) );
        registro00.setPosEntryMode( rs.getString( "COD_DATOS_PUNTO_SERVICIO" ) );
        registro00.setTransCode( rs.getString( "CODIGO_FUNCION" ) );
        registro00.setCodRazon( rs.getString( "COD_MOTIVO_MENSAJE" ) );
        String codIdent = rs.getString( "COD_IDENT_INST_DEST_TRANSAC" );
        // VARIABLES QUE VIENEN COMPUESTAS BY SLASH
        String[] settleFlagAndCardHolderMethod = codIdent
                .split( ConstantesUtil.SLASH.toString() );
        registro00.setSettleFlag( settleFlagAndCardHolderMethod[0] );
        registro00.setCardHolderIdMethod( settleFlagAndCardHolderMethod[1] );
        registro00.setRubroComer( rs.getString( "MCC" ) );
        registro00.setNumeroReferencia( rs
                .getString( "DATOS_REFERENCIA_ADQUIRENTE" ) );
        registro00
                .setAcqMembId( rs.getString( "COD_IDENTIFICACION_ADQUIRENTE" ) );
        registro00.setCodAutor( rs.getString( "CODIGO_AUTORIZACION" ) );
        registro00.setPosTerminalCapability( rs.getString( "CODIGO_SERVICIO" ) );
        registro00.setMonedaFuen( rs.getString( "COD_MONEDA_TRANSAC" ) );
        registro00.setFechaProc( rs.getString( "FECHA_ACCION" ) );
        registro00.setFechaDCompensacion( rs.getString( "P1000" ) );
        registro00.setOfic( rs.getString( "COD_IDENT_INST_ORIG_TRANSAC" ) );
        registro00.setNombreUbicAcepTarjeta( rs
                .getString( "NOMBRE_UBIC_ACEP_TARJETA" ) );
        registro00.setZipCodeComer( rs.getString( "P1003" ) );
        registro00.setCodMunic( rs.getString( "P1010" ) );
        registro00.setNumeroMensaje( rs.getString( "NRO_MENSAJE" ) );
        
        return registro00;
        
    }
    
    private Registro00DTO getRegistro00Reduce( ResultSet rs ) throws SQLException {
        Registro00DTO registro00 = new Registro00DTO();
        
        // VALORES PARA REGISTRO 00
        registro00.setCodigoTransaccion( rs.getString( "MIT" ) );
        registro00.setTransCode( rs.getString( "CODIGO_FUNCION" ) );
        registro00.setNumeroMensaje( rs.getString( "NRO_MENSAJE" ) );
        
        return registro00;
        
    }
    
    private Registro1020DTO getRegistro1020( ResultSet rs ) throws SQLException {
    	Registro1020DTO registro05 = new Registro1020DTO();
    	registro05.setMit(rs.getString( "MIT" ) );
    	registro05.setCodfuncion(rs.getString( "CODIGO_FUNCION" ) );
    	registro05.setMensaje(rs.getString( "NOMBRE_UBIC_ACEP_TARJETA" ) );
    	registro05.setCodrazon(rs.getString( "CODIGO_PROCESAMIENTO" ));
    	registro05.setDatosAdicionales1(rs.getString( "DATOS_ADICIONALES_1" )); 
    	registro05.setNroMensaje(rs.getString( "NRO_MENSAJE" ));
        return registro05;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 - 11/12/2015, (ACL) - versión inicial
     * </ul>
     * <p>
     * 
     * @param rs
     * @return
     * @throws SQLException
     * @since 1.X
     */
    private Registro01DTO getRegistro01( ResultSet rs ) throws SQLException {
        Registro01DTO registro01 = new Registro01DTO();
        // VALORES PARA REGISTRO 01
        ( ( RegistroDTO ) registro01 )
                .setTipoTransaccion( rs.getString( "MIT" ) );
        ( ( RegistroDTO ) registro01 ).setTransCode( TRANS_CODE_REGISTER_01 );
        registro01.setRubroTransbank( rs.getString( "COD_IDENT_INST_ENVIA" ) );
        registro01.setChargRefN( rs.getString( "NRO_REFERENCIA_RECUPERACION" ) );
        registro01.setTerminalId( rs.getString( "IDENT_TERM_ACEP_TARJETA" ) );
        registro01.setCardAcceptorId( rs.getString( "COD_IDENT_ACEP_TARJETA" ) );
        registro01.setIndicatorTransaction( rs.getString( "P0023" ) );
        registro01.setDocumInd( rs.getString( "P0052" ) );
        registro01.setIndicadorLiquidacion( rs.getString( "P0165" ) );
        registro01.setValorCuota( rs.getString( "P1005" ) );
        registro01.setIndicatorTransaction( rs.getString( "P0023" ) );
        registro01.setComisionCic( rs.getString( "P1007" ) );
        registro01.setP0165( rs.getString( "P0165" ) );
        registro01.setOrigen( rs.getString( "P1009" ) );
        registro01.setRubroTransbank( rs.getString( "P1008" ) );
        registro01.setTasaEECC( rs.getString( "P1006" ) );
        
        return registro01;
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 - 11/12/2015, (ACL) - versión inicial
     * </ul>
     * <p>
     * 
     * @param rs
     * @return
     * @throws SQLException
     * @since 1.X
     */
    private Registro05DTO getRegistro05( ResultSet rs ) throws SQLException {
        Registro05DTO registro05 = null;
        String registro05Total = rs.getString( "DATOS_ADICIONALES_2" );
        // VALORES PARA REGISTRO 05
        if ( registro05Total != null
                && !registro05Total.equalsIgnoreCase( "null" )
                && registro05Total.length() != 0 ) {
            registro05 = new Registro05DTO();
            registro05.setAll05Register( registro05Total );
            
        }
        
        return registro05;
    }
    
    /**
     * <s
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 - 11/12/2015, (ACL) - versión inicial
     * </ul>
     * <p>
     * 
     * @param rs
     * @return
     * @throws SQLException
     * @since 1.X
     */
    private Registro07DTO getRegistro07( ResultSet rs ) throws SQLException {
        Registro07DTO registro07 = null;
        /*
         * VALORES PARA REGISTRO 07 SE UNEN 2 CAMPOS DATOS ADICIONALES
         * 3 + DATOS ADICIONALES 4
         */
        String datosAdd3 = rs.getString( "DATOS_ADICIONALES_3" );
        String datosAdd4 = rs.getString( "DATOS_ADICIONALES_4" );
        // PUEDE NO CONTENER EL REGISTRO 07
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("datosAdd3 :" + datosAdd3);
			LOGGER.debug("datosAdd4 :" + datosAdd4);
		}
        if ( datosAdd3 != null && datosAdd4 != null
                && !datosAdd3.equalsIgnoreCase( "null" )
                && !datosAdd4.equalsIgnoreCase( "null" )
                && datosAdd3.length() != 0 && datosAdd4.length() != 0 ) {
            registro07 = new Registro07DTO();
            registro07.setAllRegister07( datosAdd3.concat( datosAdd4 ) );
            return registro07;
        }
        
        if(LOGGER.isDebugEnabled()){
        	LOGGER.debug( "REGISTRO 07 :" + registro07 );
        }
        
        return registro07;
        
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2015, (Everis Chile) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @see com.sbpay.sgi.cron.dao.GenerarIncomingDAO#callSPCargaOutgoingTransbank()
     * @since 1.X
     */
    @Override
    public boolean callSPCargaOutgoingTransbank() throws AppException {
        CallableStatement callableStatement = null;
        
        LOGGER.info( "******SE EJECUTA STORE PROCEDURE SP_SIG_CARGAOUTGOING_TBK*****" );
        
        try {
            callableStatement = this.getConnection().prepareCall(
                    CALL_SP_SIG_CARGAOUTGOING_TBK_VISA );
            
            callableStatement.registerOutParameter( 1, java.sql.Types.VARCHAR );
            callableStatement.registerOutParameter( 2, java.sql.Types.NUMERIC );
            callableStatement.registerOutParameter( 3, java.sql.Types.NUMERIC );
            
            callableStatement.execute();
            
            String warning = callableStatement.getString( 1 );
            Integer codigoError = callableStatement.getInt( 2 );
            Integer l_ind_Ok_Ko = callableStatement.getInt( 3 );
            LOGGER.info( "WARNING : " + warning );
            LOGGER.info( "codigoError : " + codigoError );
            LOGGER.info( "l_ind_Ok_Ko : " + l_ind_Ok_Ko );
            if ( l_ind_Ok_Ko != 0 ) {
                LOGGER.warn( MsgGenerationIncoming.ERROR_CALL_SP_OUT_TRBK
                        + ": error " + codigoError + ", mensaje:" + warning );
            }
            
        }
        catch ( SQLException e ) {
            throw new AppException(
                    MsgGenerationIncoming.ERROR_CALL_SP_OUT_TRBK.toString(), e );
        }
        finally {
            
            try {
                if ( callableStatement != null ) {
                    callableStatement.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException(
                        MsgGenerationIncoming.ERROR_CALL_SP_OUT_TBK_CLOSE_CONECTION
                                .toString(), e );
            }
            
        }
        
        return Boolean.TRUE;
    }
    
    
    @Override
    public boolean callSPCargaOutgoingVisa(final int operador) throws AppException {
        CallableStatement callableStatement = null;
        
        LOGGER.info( "******SE EJECUTA STORE PROCEDURE SP_SIG_CARGAOUTGOING_VISA*****" );
        
        try {
            callableStatement = this.getConnection().prepareCall(
            		CALL_SP_SIG_CARGAOUTGOING_VISA );
            
            callableStatement.registerOutParameter( 1, java.sql.Types.VARCHAR );
            callableStatement.registerOutParameter( 2, java.sql.Types.NUMERIC );
            callableStatement.registerOutParameter( 3, java.sql.Types.NUMERIC );
            callableStatement.setInt(4, operador);
            
            callableStatement.execute();
            
            String warning = callableStatement.getString( 1 );
            Integer codigoError = callableStatement.getInt( 2 );
            Integer l_ind_Ok_Ko = callableStatement.getInt( 3 );
            LOGGER.info( "WARNING : " + warning );
            LOGGER.info( "codigoError : " + codigoError );
            LOGGER.info( "l_ind_Ok_Ko : " + l_ind_Ok_Ko );
            if ( l_ind_Ok_Ko != 0 ) {
                LOGGER.warn( MsgGenerationIncoming.ERROR_CALL_SP_OUT_VISA
                        + ": error " + codigoError + ", mensaje:" + warning );
            }
            
        }
        catch ( SQLException e ) {
            throw new AppException(
                    MsgGenerationIncoming.ERROR_CALL_SP_OUT_VISA.toString(), e );
        }
        finally {
            
            try {
                if ( callableStatement != null ) {
                    callableStatement.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException(
                        MsgGenerationIncoming.ERROR_CALL_SP_OUT_TBK_CLOSE_CONECTION
                                .toString(), e );
            }
            
        }
        
        return Boolean.TRUE;
    }
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 17/12/2015, (ACL-sbpay) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws AppException
     * @see com.sbpay.sgi.cron.dao.GenerarIncomingDAO#truncateTmpOutgoingTransbank()
     * @since 1.X
     */
    @Override
    public void clearTemporalOutgoingTransbank() throws AppException {
        PreparedStatement stmt = null;
        
        try {
            if(LOGGER.isDebugEnabled()){
            	LOGGER.debug( "CONSULTA SQL -> "+ TRUNCATE_TMP_OUTGOING_TRANSBANK );
            }
            stmt = this.getConnection().prepareStatement(
                    TRUNCATE_TMP_OUTGOING_TRANSBANK );
            
            // SE EJECUTA CONSULTA
            stmt.executeUpdate();
            
            // LOGGER
            LOGGER.info( "Se ha vaciado la tabla TMP_OUTGOING_TRANSBANK correctamente" );
            
        }
        catch ( SQLException e ) {
            throw new AppException( e.getMessage(), e );
        }
        finally {
            
            try {
                if ( stmt != null ) {
                    stmt.close();
                }
            }
            catch ( SQLException e ) {
                throw new AppException( e.getMessage(), e );
                
            }
            
        }
    }
}
